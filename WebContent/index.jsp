<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Dyn Apps</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<link rel="shortcut icon" href="usr/audi/login/image/favicon.png">
		<link rel="stylesheet" href="usr/audi/login/css/style.css">
		<script src="usr/audi/login/js/script.js"></script>
		<script language="javascript">
			function setFocus()
			{
				document.getElementById("login").focus();
			}
		</script>
	</head>
	<body class="fondo">
		<div id="loginContainer">
			<div id="loginContent">
				<div id="loginBox">
					<img id="loginLogo" src="usr/audi/login/image/Daeji.png">
					<!-- <img id="logo" src="image/logo-audi.png"> -->
					<form action="DynAppsSrv" method="post">
						<% if(request.getAttribute("MSG_ERROR")!=null){ %>
								<font color="red"><%=request.getAttribute("MSG_ERROR")%></font></br>
						<% } %>
						<label>Usuario</label>
						<input type="text" name="login" value="<%=(request.getParameter("login")!=null?request.getParameter("login"):"")%>" id="login" placeholder="Escriba su nombre de usuario" autocomplete="off" onblur="loginValidation(this, 0)" required>
						<div class="validationMessage">
							<div class="triangle"></div>
							<div class="innervalidationMessage">Por favor escriba su nombre de usuario</div>
						</div>
						<label>Contrase&ntilde;a</label>
						<input type="password" name="password" placeholder="Escriba su contrase&ntilde;a" autocomplete="off" onblur="loginValidation(this, 1)" required>
						<div class="validationMessage">
							<div class="triangle"></div>
							<div class="innervalidationMessage">Por favor escriba su contrase&ntilde;a</div>
						</div>
						<!-- a class="passwordRecovery" href="javascript:void(0)">Recuperar contrase&ntilde;a</a-->
						<input type="submit" value="INGRESAR">
						<input type="hidden" name="PARAM_CTL_PANTALLA" value="0">
						<input type="hidden" name="PARAM_CTL_ID_TABLA" value="0">
						<input type="hidden" name="PARAM_CTL_OPERACION" value="C">
						<input type="hidden" name="PARAM_CTL_CVE_NAVEGACION" value="NAVEGA_CAT_CTL_MENU">
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
