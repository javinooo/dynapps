
var tableToExcel2 = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()
 
	function Export(tableName, bgcolor, txcolor) {
		var $newTable = $( "#" + tableName ).clone();
		$newTable.appendTo("body");
		var newIdT = 'newid' + tableName;
		$newTable.attr('id',newIdT);
		$newTable.find('.msg').remove();
		$newTable.find('.va').css('vertical-align', 'middle');
		$newTable.find('.data td').each(function (i, el) {
			var $input = $(this).find('input');
			var inputval = $input.val();
			if(inputval){
				$(this).css('background-color', bgcolor);
				$(this).css('color', txcolor);
				$(this).text(inputval);
				$(this).attr('align', 'center');
			}
			$input.remove();
		});
		tableToExcel2(newIdT, 'Matrix');
		$newTable.remove();
	}

	function fileUpload(filename) {
		alert('entra a fileupload: ' + filename);
		var $ID_EMPRESA		= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
		var $fileToLoad 	= $('#' + filename);
		
			$.ajax({
            type: "POST",
            url: "AdmContenidosSrv",
			async: false,
            data: {FILE_TO_LOAD:$fileToLoad,ID_EMPRESA:$ID_EMPRESA},
            dataType: "json",
            success: function(data) {

					console.log(' EXITO');
            },
            error: function(data){

					console.log('error handing here'); 
            }
        });

	}

    // Funcion para exportar a excel para IE
    function exportToExcel(NomTabla) {
            //alert('Entra a la funcion');
            var oExcel = new ActiveXObject("Excel.Application");
            //alert('paso 1');
            var oBook = oExcel.Workbooks.Add;
            //alert('paso 2');
            var oSheet = oBook.Worksheets(1);
            //alert('antes de iterar');
            for (var y = 0; y < document.getElementById(NomTabla).rows.length; y++)
            // detailsTable is the table where the content to be exported is
            {
                for (var x = 0; x < document.getElementById(NomTabla).rows(y).cells.length; x++) {
                    oSheet.Cells(y + 1, x + 1) = document.getElementById(NomTabla).rows(y).cells(x).innerText;
                }
            }
            oExcel.Visible = true;
            oExcel.UserControl = true;
        }
        
        
        function test() { 
            if (!window['ActiveXObject']) { 
                alert('Error: ActiveX not supported'); 
                
            }else{
                alert('ActiveX yes supported');
            }
        }          
        

    // para exportar a excel con Chrome y Firefox
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64;charset=UTF-8,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
	//$("#" + table + " th:first-child").remove();
	//$("#" + table + " td:first-child").remove();
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()

function upperCase(x)
{
	var y = document.getElementById(x);
	var texto = y.value;
	document.getElementById(x).value= texto.toUpperCase();
}


function standardNombreCampo(x){

	//alert('Entra a standard campo ' + x);
	var y = document.getElementById(x);
	//alert('obtiene el objeto ');
	//alert(' y.nodeName= ' + y);
	var texto = y.value;
	//alert('obtiene el texto ');
	var valueUpper = texto.toUpperCase();
	y.value	= valueUpper;
	
	if(valueUpper.length != null){
	
		var primerCaracter = valueUpper.substring(0,1);
		var firstChar = primerCaracter.charCodeAt(0);
		
		if(firstChar==95 ||(firstChar>47&&firstChar<58)){
			alert(' No est� permitido el caracter "_" o un caracter num�rico como inicio de nombre ');
			while(firstChar==95 ||(firstChar>47&&firstChar<58)){
				y.value = valueUpper.substring(1,valueUpper.length);
				valueUpper = y.value;
				primerCaracter = valueUpper.substring(0,1);
				firstChar = primerCaracter.charCodeAt(0);
			}
			
		}
			
	}
}



function validaNombreAtributo(e,i){
	
	//alert('entra a valida ' + i);
	
	var myElement = document.getElementById(i);
	var texto = myElement.value;
	var textoLength;
	if(texto.length != null){
		//alert('no es nulo');
		textoLength = texto.length;
	}else{
		//alert('Es nulo');
		textoLength = 0;
	}

	//alert(textoLength);

	var unicode= e.charCode? e.charCode : e.keyCode
	if (unicode!=8 || unicode!=9){ //if the key isn't the backspace or underscore key (which we should allow)
	    if (!((unicode>47&&unicode<58) || unicode==95 || (unicode>64&&unicode<91) || (unicode>96&&unicode<123))){ //if not a number
		return false //disable key press
	    }
	    else{
	        
			//alert('Entra a else ');
			if(textoLength==0) {
				if(unicode==95 ||(unicode>47&&unicode<58)){
					return false //disable key press
				}
			}
		}
	}
}

function validaAltaEntidad(documentoForm){

	//alert('entra a valida entidad');
	//return true;
	var iNumFieldsPK = 0;
	var sMensajes;
	var sTmpColumnName;
	var dupCols = new Array();
	var countDupl = 0;
	var bYaRegistrada = false;
	var sMensajeFaltaCampos;
	
	//alert('antes de recuperar ');
	var tableName = documentoForm.getElementById("NOM_ENTIDAD");
	//alert('tableName= ' + tableName);
	var nomTabla = tableName.value;
	//alert(nomTabla.substring(0,4));
	if(nomTabla.substring(0,4) == 'AOS_'){
		sMensajes = ' - El prefijo "AOS_" es reservado para nombres de tablas del sistema. \n';
	}
	
	names 	= documentoForm.getElementsByClassName('ATR');
	tags 	= documentoForm.getElementsByClassName('PK');
	dts 	= documentoForm.getElementsByClassName('DT');
	sizes 	= documentoForm.getElementsByClassName('SIZE');
	scales 	= documentoForm.getElementsByClassName('SCALE');
	dfVal	= documentoForm.getElementsByClassName('DFVAL');
	comms	= documentoForm.getElementsByClassName('COMM');
	
	
	//alert('atributos  -- ' + names.length);
	//alert('dts  -- ' + dts.length);
	//alert('tags  -- ' + tags.length);
	//alert('sizes  -- ' + sizes.length);
	//alert('scales  -- ' + scales.length);
	
	for(i = 0; i < names.length; i++) {

		//alert('itera -- ' + i);
		//Verifica que exista un campo definido de llave primaria
		if(tags[i].checked){
			//alert('si est� checado ');
			iNumFieldsPK = iNumFieldsPK + 1;
			//alert('aumenta el numero ');
			if( dts[i].options[dts[i].selectedIndex].value == 'CLOB' ||	
				dts[i].options[dts[i].selectedIndex].value == 'BLOB' || 
				dts[i].options[dts[i].selectedIndex].value == 'BFILE'){
				//alert('entra a la condici�n');
				if(sMensajes == null)
					sMensajes = '';
				
				sMensajes = sMensajes + ' - El tipo de dato ' + dts[i].options[dts[i].selectedIndex].value + ' del atributo ' + names[i].value + 
				' No es permitido para ser parte de la llave primaria de la tabla \n'; 
			}
		}
		
		//alert('names[i].value: ' + names[i].value);
		if((names[i].value == '' || sizes[i].value == '' || scales[i].value == '' || comms[i].value == '') && sMensajeFaltaCampos==null){
			//alert('entra a la condici�n de nulos');
			sMensajeFaltaCampos = 'Verifique los campos que faltan de capturar  \n';
		}

		//alert('pasa el itera');
		//e.options[e.selectedIndex].value
		//alert(dts[i].options[dts[i].selectedIndex].value);
		if(dts[i].options[dts[i].selectedIndex].value == 'NUMBER'){
			//alert('El tipo de dato es number');
			if(parseInt(sizes[i].value) > 38){
				if(sMensajes == null)
					sMensajes = '';
				sMensajes = sMensajes + ' - La longitud del atributo num�rico ' + names[i].value + ' no debe exceder 38 \n'; 
			}
			if(parseInt(scales[i].value) > parseInt(sizes[i].value)){
				if(sMensajes == null)
					sMensajes = '';
				sMensajes = sMensajes + ' - El n�mero de decimales del atributo num�rico ' + names[i].value + ' no debe exceder la longitud del atributo \n'; 
			}
		}
		
		if(dts[i].options[dts[i].selectedIndex].value == 'TIMESTAMP'){
			//alert('El tipo de dato es TIMESTAMP');
			if(parseInt(sizes[i].value) < 0 || parseInt(sizes[i].value) > 9){
				if(sMensajes == null)
					sMensajes = '';
				sMensajes = sMensajes + ' - El rango de precisi�n para la estampa de tiempo del atributo ' + names[i].value + ' debe ser (0 a 9) \n'; 
			}
		}
		
		if(dts[i].options[dts[i].selectedIndex].value == 'VARCHAR2'){
			//alert('El tipo de dato es VARCHAR2');
			if(parseInt(sizes[i].value) < 1 || parseInt(sizes[i].value) > 4000){
				if(sMensajes == null)
					sMensajes = '';
				sMensajes = sMensajes + ' - El rango de longitud del atributo de Texto ' + names[i].value + ' debe ser (1 a 4000) \n'; 
			}
		}
		
		for(y = 0; y < names.length; y++) {
			//alert('valor de i ' + i);
			if(i != y && names[i].value != ''){
				//alert('i: ' + i + ' y: ' + y);
				//alert('i: ' + names[i].value + ' y: ' + names[y].value);
				if(names[i].value == names[y].value){
				
					for(x = 0; x < dupCols.length; x++){
						if(names[i].value == dupCols[x]){
							//alert('YA ESTA REGISTGRADA..!!! ' );
							bYaRegistrada = true;
						}
					}
					
					if(!bYaRegistrada){
						if(sMensajes == null)
							sMensajes = '';

						sMensajes = sMensajes + ' - El nombre de atributo ' + names[i].value + ' est� duplicado, no se deben definir nombres duplicados \n'; 
						dupCols[countDupl] = names[i].value;
						//alert('duplicado: ' + dupCols[countDupl]);
						countDupl = countDupl + 1;
						//break;
					}
				}
			}
			bYaRegistrada = false;
		}
		
		if(dfVal[i].value != null && dfVal[i].value != ''){
		
			//alert(' default: >' + dfVal[i].value + '<');
		
			if(dts[i].options[dts[i].selectedIndex].value == 'DATE' || dts[i].options[dts[i].selectedIndex].value == 'TIMESTAMP'){
			
				if(dts[i].options[dts[i].selectedIndex].value == 'DATE' ){

					if(dfVal[i].value.toUpperCase() != 'SYSDATE' && dfVal[i].value.toUpperCase() != 'TRUNC(SYSDATE)'){
						//alert('el valor default no es nulo y el tipo de dato es fecha');
						if(!ValidateDate(dfVal[i].value)){
							if(sMensajes == null)
								sMensajes = '';
								sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' no es correcto: \n ("SYSDATE", "TRUNC(SYSDATE)", \n  "DD/MM/YYYY", "DD/MM/YYYY HH:MI:SS") \n'; 
						}
					}
				}else{

					if(dfVal[i].value.toUpperCase() != 'SYSTIMESTAMP' && dfVal[i].value.toUpperCase() != 'TRUNC(SYSTIMESTAMP)'){
						//alert('el valor default no es nulo y el tipo de dato es fecha');
						if(!ValidateDate(dfVal[i].value)){
							if(sMensajes == null)
								sMensajes = '';

								sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' no es correcto: \n ("SYSTIMESTAMP", "TRUNC(SYSTIMESTAMP)", \n  "DD/MM/YYYY", "DD/MM/YYYY HH:MI:SS") \n'; 
						}
					}
				}
			}else{

				if(dts[i].options[dts[i].selectedIndex].value == 'NUMBER'){
				
					if(isNaN(dfVal[i].value)){
						if(sMensajes == null)
							sMensajes = '';
						sMensajes = sMensajes + ' - Capture un n�mero v�lido como valor default del atributo ' + names[i].value + ' \n'; 
					}else{
						if(parseInt(sizes[i].value) > 0){
							// Cuando el n�mero es positivo
							if(dfVal[i].value.indexOf('-') === -1){

								//alert('es numero positivo');
								if(parseInt(scales[i].value) == 0 ){
									//alert('dfVal[i].length: ' + dfVal[i].value.length);
									//alert('dfVal[i].value: ' + dfVal[i].value);
									//alert('sizes[i].value: ' + sizes[i].value);
									if(dfVal[i].value.length > parseInt(sizes[i].value)){
										if(sMensajes == null)
											sMensajes = '';
										sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' sobrepasa la longitud total del atributo \n'; 
									}
								}else{
									if(dfVal[i].value.indexOf('.') === -1){
										if(dfVal[i].value.length > (parseInt(sizes[i].value) - parseInt(scales[i].value))){
											if(sMensajes == null)
												sMensajes = '';
											sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' sobrepasa la longitud de enteros \n'; 
										}
									}else{
										//alert(' Numero de decimales  ');
										//alert(' dfVal[i].value.length  ' + dfVal[i].value.length);
										//alert(' dfVal[i].value.indexOf(.) ' + dfVal[i].value.indexOf('.'));
										//alert(' NUmero de decimales  ' + ((dfVal[i].value.length-1) - dfVal[i].value.indexOf('.')));
										//alert(' Split  ' + dfVal[i].value.split(".")[1].length);
										//if(scales[i].value > ((dfVal[i].value.length-1) - dfVal[i].value.indexOf('.')){
										if((parseInt(sizes[i].value) - parseInt(scales[i].value)) < dfVal[i].value.split(".")[0].length){
											//text.split(".")[1].length;
											if(sMensajes == null)
												sMensajes = '';
											sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' excede el n�mero de enteros \n'; 
										}
										if(parseInt(scales[i].value) < dfVal[i].value.split(".")[1].length){
											//text.split(".")[1].length;
											if(sMensajes == null)
												sMensajes = '';
											sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' excede el n�mero de decimales \n'; 
										}
									}
								}
							}else{
								// Cuando el n�mero es negativo
								//alert('Es Negativo');
								if(parseInt(scales[i].value) == 0 ){
									if((dfVal[i].value.length - 1) > parseInt(sizes[i].value)){
										if(sMensajes == null)
											sMensajes = '';
										sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' sobrepasa la longitud total del atributo \n'; 
									}
								}else{
									if(dfVal[i].value.indexOf('.') === -1){
										if((dfVal[i].value.length - 1) > (parseInt(sizes[i].value) - parseInt(scales[i].value))){
											if(sMensajes == null)
												sMensajes = '';
											sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' sobrepasa la longitud de enteros \n'; 
										}
									}else{
										//alert(' Numero de decimales negativo ');
										//alert(' dfVal[i].value.length  ' + dfVal[i].value.length);
										//alert(' dfVal[i].value.indexOf(.) ' + dfVal[i].value.indexOf('.'));
										//alert(' NUmero de decimales  ' + ((dfVal[i].value.length-1) - dfVal[i].value.indexOf('.')));
										//alert(' Split  ' + dfVal[i].value.split(".")[1].length);
										//if(scales[i].value > ((dfVal[i].value.length-1) - dfVal[i].value.indexOf('.')){
										if((parseInt(sizes[i].value) - parseInt(scales[i].value)) < (dfVal[i].value.split(".")[0].length - 1)){
											//text.split(".")[1].length;
											if(sMensajes == null)
												sMensajes = '';
											sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' excede el n�mero de enteros \n'; 
										}
										if(parseInt(scales[i].value) < dfVal[i].value.split(".")[1].length){
											//text.split(".")[1].length;
											if(sMensajes == null)
												sMensajes = '';
											sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' excede el n�mero de decimales \n'; 
										}
									}
								}
							}
						}
					}
				}else{
				
					if(dts[i].options[dts[i].selectedIndex].value == 'VARCHAR2'){
						if(dfVal[i].value.length > parseInt(sizes[i].value)){
							if(sMensajes == null)
								sMensajes = '';
							sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' excede el n�mero de caracteres \n'; 
						}
					}else{
					
						if(dts[i].options[dts[i].selectedIndex].value == 'BFILE'){
							
							if(sMensajes == null)
								sMensajes = '';
							sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' No aplica para el tipo de dato de referencia de archivo \n'; 
						}
					}
				}
			}
		}
	}
	//alert('Numero de checboxes checked = ' + iNumFieldsPK);

	if(iNumFieldsPK == 0){
		//alert('Debe definir al menos un campo de llave primaria de la entidad');
		if(sMensajes == null)
			sMensajes = '';
		sMensajes = sMensajes + ' - Debe definir al menos un campo de llave primaria de la entidad \n'; 
	}
	
	if(sMensajes != null || sMensajeFaltaCampos != null){
		alert((sMensajes==null?'':sMensajes) + '\n'  + (sMensajeFaltaCampos==null?'':sMensajeFaltaCampos) );
		return false;
	}else{
		//alert('else de mensajes ');
		if(confirm('�Ejecutar Proceso?')){
		
			for(i = 0; i < names.length; i++) {
				names[i].name	= 'COLUMN_NAME';
				dts[i].name	 	= 'DATA_TYPE';
				sizes[i].name	= 'N_SIZE';
				scales[i].name	= 'N_SCALE';
				dfVal[i].name	= 'DEFAULT_VALUE';
				comms[i].name	= 'COL_COMMENTS';
			}
		
			return true;
		}else{
			return false;
		}
	}
} 


function validaAgregaAtributoEntidad(documentoForm){

	//alert('entra a valida AGREGA ATRIBUTO ENTIDAD');
	//return true;
	var iNumFieldsPK = 0;
	var sMensajes;
	var sTmpColumnName;
	var dupCols = new Array();
	var countDupl = 0;
	var bYaRegistrada = false;
	var sMensajeFaltaCampos;
	
	//alert('antes de recuperar ');
	var tableName = documentoForm.getElementById("NOM_ENTIDAD");
	//alert('tableName= ' + tableName);
	var nomTabla = tableName.value;
	//alert(nomTabla.substring(0,4));
	if(nomTabla.substring(0,4) == 'AOS_'){
		sMensajes = ' - El prefijo "AOS_" es reservado para nombres de tablas del sistema. \n';
	}
	
	names 	= documentoForm.getElementsByClassName('ATR');
	//tags 	= documentoForm.getElementsByClassName('PK');
	dts 	= documentoForm.getElementsByClassName('DT');
	sizes 	= documentoForm.getElementsByClassName('SIZE');
	scales 	= documentoForm.getElementsByClassName('SCALE');
	dfVal	= documentoForm.getElementsByClassName('DFVAL');
	comms	= documentoForm.getElementsByClassName('COMM');
	
	
	//alert('atributos  -- ' + names.length);
	//alert('dts  -- ' + dts.length);
	//alert('tags  -- ' + tags.length);
	//alert('sizes  -- ' + sizes.length);
	//alert('scales  -- ' + scales.length);
	
	for(i = 0; i < names.length; i++) {

		//alert('intera -- ');
		//Verifica que exista un campo definido de llave primaria
		/*if(tags[i].checked){
			iNumFieldsPK = iNumFieldsPK + 1;
			
			if( dts[i].options[dts[i].selectedIndex].value == 'CLOB' ||	
				dts[i].options[dts[i].selectedIndex].value == 'BLOB' || 
				dts[i].options[dts[i].selectedIndex].value == 'BFILE'){
				
				if(sMensajes == null)
					sMensajes = '';
				
				sMensajes = sMensajes + ' - El tipo de dato ' + dts[i].options[dts[i].selectedIndex].value + ' del atributo ' + names[i].value + 
				' No es permitido para ser parte de la llave primaria de la tabla \n'; 
			}
		}*/
		//alert('pasa el itera');
		//e.options[e.selectedIndex].value
		//alert(dts[i].options[dts[i].selectedIndex].value);
		if(dts[i].options[dts[i].selectedIndex].value == 'NUMBER'){
			//alert('El tipo de dato es number');
			if(parseInt(sizes[i].value) > 38){
				if(sMensajes == null)
					sMensajes = '';
				sMensajes = sMensajes + ' - La longitud del atributo num�rico ' + names[i].value + ' no debe exceder 38 \n'; 
			}
			if(parseInt(scales[i].value) > parseInt(sizes[i].value)){
				if(sMensajes == null)
					sMensajes = '';
				sMensajes = sMensajes + ' - El n�mero de decimales del atributo num�rico ' + names[i].value + ' no debe exceder la longitud del atributo \n'; 
			}
		}
		
				//alert('names[i].value: ' + names[i].value);
		if((names[i].value == '' || sizes[i].value == '' || scales[i].value == '' || comms[i].value == '') && sMensajeFaltaCampos==null){
			//alert('entra a la condici�n de nulos');
			sMensajeFaltaCampos = 'Verifique los campos que faltan de capturar  \n';
		}

		
		
		
		if(dts[i].options[dts[i].selectedIndex].value == 'TIMESTAMP'){
			//alert('El tipo de dato es number');
			if(parseInt(sizes[i].value) < 0 || parseInt(sizes[i].value) > 9){
				if(sMensajes == null)
					sMensajes = '';
				sMensajes = sMensajes + ' - El rango de precisi�n para la estampa de tiempo del atributo ' + names[i].value + ' debe ser (0 a 9) \n'; 
			}
		}
		
		if(dts[i].options[dts[i].selectedIndex].value == 'VARCHAR2'){
			//alert('El tipo de dato es number');
			if(parseInt(sizes[i].value) < 1 || parseInt(sizes[i].value) > 4000){
				if(sMensajes == null)
					sMensajes = '';
				sMensajes = sMensajes + ' - El rango de longitud del atributo de Texto ' + names[i].value + ' debe ser (1 a 4000) \n'; 
			}
		}
		
		for(y = 0; y < names.length; y++) {
			//alert('valor de i ' + i);
			if(i != y && names[i].value != ''){
				//alert('i: ' + i + ' y: ' + y);
				//alert('i: ' + names[i].value + ' y: ' + names[y].value);
				if(names[i].value == names[y].value ){
				
					for(x = 0; x < dupCols.length; x++){
						if(names[i].value == dupCols[x]){
							//alert('YA ESTA REGISTGRADA..!!! ' );
							bYaRegistrada = true;
						}
					}
					
					if(!bYaRegistrada){
						if(sMensajes == null)
							sMensajes = '';

						sMensajes = sMensajes + ' - El nombre de atributo ' + names[i].value + ' est� duplicado, no se deben definir nombres duplicados \n'; 
						dupCols[countDupl] = names[i].value;
						//alert('duplicado: ' + dupCols[countDupl]);
						countDupl = countDupl + 1;
						//break;
					}
				}
			}
			bYaRegistrada = false;
		}
		
		if(dfVal[i].value != null && dfVal[i].value != ''){
		
			//alert(' default: >' + dfVal[i].value + '<');
		
			if(dts[i].options[dts[i].selectedIndex].value == 'DATE' || dts[i].options[dts[i].selectedIndex].value == 'TIMESTAMP'){
			
				if(dts[i].options[dts[i].selectedIndex].value == 'DATE' ){

					if(dfVal[i].value.toUpperCase() != 'SYSDATE' && dfVal[i].value.toUpperCase() != 'TRUNC(SYSDATE)'){
						//alert('el valor default no es nulo y el tipo de dato es fecha');
						if(!ValidateDate(dfVal[i].value)){
							if(sMensajes == null)
								sMensajes = '';
								sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' no es correcto: \n ("SYSDATE", "TRUNC(SYSDATE)", \n  "DD/MM/YYYY", "DD/MM/YYYY HH:MI:SS") \n'; 
						}
					}
				}else{

					if(dfVal[i].value.toUpperCase() != 'SYSTIMESTAMP' && dfVal[i].value.toUpperCase() != 'TRUNC(SYSTIMESTAMP)'){
						//alert('el valor default no es nulo y el tipo de dato es fecha');
						if(!ValidateDate(dfVal[i].value)){
							if(sMensajes == null)
								sMensajes = '';

								sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' no es correcto: \n ("SYSTIMESTAMP", "TRUNC(SYSTIMESTAMP)", \n  "DD/MM/YYYY", "DD/MM/YYYY HH:MI:SS") \n'; 
						}
					}
				}
			}else{

				if(dts[i].options[dts[i].selectedIndex].value == 'NUMBER'){
				
					if(isNaN(dfVal[i].value)){
						if(sMensajes == null)
							sMensajes = '';
						sMensajes = sMensajes + ' - Capture un n�mero v�lido como valor default del atributo ' + names[i].value + ' \n'; 
					}else{

						if(parseInt(sizes[i].value) > 0){
							//alert('es mayor a 0');
							// Cuando el n�mero es positivo
							if(dfVal[i].value.indexOf('-') === -1){

								//alert('es numero positivo');
								if(parseInt(scales[i].value) == 0 ){
									//alert('dfVal[i].length: ' + dfVal[i].value.length);
									//alert('dfVal[i].value: ' + dfVal[i].value);
									//alert('sizes[i].value: ' + sizes[i].value);
									if(dfVal[i].value.length > parseInt(sizes[i].value)){
										if(sMensajes == null)
											sMensajes = '';
										sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' sobrepasa la longitud total del atributo \n'; 
									}
								}else{
									if(dfVal[i].value.indexOf('.') === -1){
										if(dfVal[i].value.length > (parseInt(sizes[i].value) - parseInt(scales[i].value))){
											if(sMensajes == null)
												sMensajes = '';
											sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' sobrepasa la longitud de enteros \n'; 
										}
									}else{
										//alert(' Numero de decimales  ');
										//alert(' dfVal[i].value.length  ' + dfVal[i].value.length);
										//alert(' dfVal[i].value.indexOf(.) ' + dfVal[i].value.indexOf('.'));
										//alert(' NUmero de decimales  ' + ((dfVal[i].value.length-1) - dfVal[i].value.indexOf('.')));
										//alert(' Split  ' + dfVal[i].value.split(".")[1].length);
										//if(scales[i].value > ((dfVal[i].value.length-1) - dfVal[i].value.indexOf('.')){
										if((parseInt(sizes[i].value) - parseInt(scales[i].value)) < dfVal[i].value.split(".")[0].length){
											//text.split(".")[1].length;
											if(sMensajes == null)
												sMensajes = '';
											sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' excede el n�mero de enteros \n'; 
										}
										if(parseInt(scales[i].value) < dfVal[i].value.split(".")[1].length){
											//text.split(".")[1].length;
											if(sMensajes == null)
												sMensajes = '';
											sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' excede el n�mero de decimales \n'; 
										}
									}
								}
							
							}else{
								// Cuando el n�mero es negativo
								//alert('Es Negativo');
								if(parseInt(scales[i].value) == 0 ){
									if((dfVal[i].value.length - 1) > parseInt(sizes[i].value)){
										if(sMensajes == null)
											sMensajes = '';
										sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' sobrepasa la longitud total del atributo \n'; 
									}
								}else{
									if(dfVal[i].value.indexOf('.') === -1){
										if((dfVal[i].value.length - 1) > (parseInt(sizes[i].value) - parseInt(scales[i].value))){
											if(sMensajes == null)
												sMensajes = '';
											sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' sobrepasa la longitud de enteros \n'; 
										}
									}else{
										//alert(' Numero de decimales  ');
										//alert(' dfVal[i].value.length  ' + dfVal[i].value.length);
										//alert(' dfVal[i].value.indexOf(.) ' + dfVal[i].value.indexOf('.'));
										//alert(' NUmero de decimales  ' + ((dfVal[i].value.length-1) - dfVal[i].value.indexOf('.')));
										//alert(' Split  ' + dfVal[i].value.split(".")[1].length);
										//if(scales[i].value > ((dfVal[i].value.length-1) - dfVal[i].value.indexOf('.')){
										if((parseInt(sizes[i].value) - parseInt(scales[i].value)) < (dfVal[i].value.split(".")[0].length - 1)){
											//text.split(".")[1].length;
											if(sMensajes == null)
												sMensajes = '';
											sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' excede el n�mero de enteros \n'; 
										}
										if(parseInt(scales[i].value) < dfVal[i].value.split(".")[1].length){
											//text.split(".")[1].length;
											if(sMensajes == null)
												sMensajes = '';
											sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' excede el n�mero de decimales \n'; 
										}
									}
								}
							}
						}
					}
				}else{
				
					if(dts[i].options[dts[i].selectedIndex].value == 'VARCHAR2'){
						if(dfVal[i].value.length > parseInt(sizes[i].value)){
							if(sMensajes == null)
								sMensajes = '';
							sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' excede el n�mero de caracteres \n'; 
						}
					}else{
					
						if(dts[i].options[dts[i].selectedIndex].value == 'BFILE'){
							
							if(sMensajes == null)
								sMensajes = '';
							sMensajes = sMensajes + ' - El valor default del atributo ' + names[i].value + ' No aplica para el tipo de dato de referencia de archivo \n'; 
						}
					}
				}
			}
		}
	}
	//alert('Numero de checboxes checked = ' + iNumFieldsPK);

	if(sMensajes != null || sMensajeFaltaCampos != null){
		alert((sMensajes==null?'':sMensajes) + '\n'  + (sMensajeFaltaCampos==null?'':sMensajeFaltaCampos) );
		return false;
	}else{
		if(confirm('�Ejecutar Proceso?')){
			for(i = 0; i < names.length; i++) {
				names[i].name	= 'COLUMN_NAME';
				sizes[i].name	= 'N_SIZE';
				scales[i].name	= 'N_SCALE';
				dfVal[i].name	= 'DEFAULT_VALUE';
				comms[i].name	= 'COL_COMMENTS';
			}
		
			return true;
		}else{
			return false;
		}
	}
} 

function validaRenombraCol(){

	//alert('entra a validaRenombraCol');
	var iNumFieldsPK = 0;
	var sMensajes;
	var sTmpColumnName;
	var dupCols = new Array();
	var countDupl = 0;
	var bYaRegistrada = false;
	var sMensajeFaltaCampos;
	
	var names	= document.getElementsByClassName('NEWNAME');

	for(i = 0; i < names.length; i++) {

		if(names[i].value == '' && sMensajeFaltaCampos==null){
			sMensajeFaltaCampos = 'Verifique los campos que faltan de capturar  \n';
		}
		
	}

	if(sMensajes != null || sMensajeFaltaCampos != null){
		alert((sMensajes==null?'':sMensajes) + '\n'  + (sMensajeFaltaCampos==null?'':sMensajeFaltaCampos) );
		return false;
	}else{
		if(confirm('�Ejecutar Proceso?')){
			for(i = 0; i < names.length; i++) {
				names[i].name	= 'NOMBRE_NUEVO';
			}
			return true;
		}else{
			return false;
		}
	}
} 

function validaModifColComment(){

	//alert('entra a validaRenombraCol');
	var sMensajes;
	var sMensajeFaltaCampos;
	
	var comments 		= document.getElementsByClassName('COMM');

	for(i = 0; i < comments.length; i++) {

		if(comments[i].value == '' && sMensajeFaltaCampos==null){
			sMensajeFaltaCampos = 'Verifique los campos que faltan de capturar  \n';
		}
		
	}

	if(sMensajes != null || sMensajeFaltaCampos != null){
		alert((sMensajes==null?'':sMensajes) + '\n'  + (sMensajeFaltaCampos==null?'':sMensajeFaltaCampos) );
		return false;
	}else{
		if(confirm('�Ejecutar Proceso?')){
			for(i = 0; i < comments.length; i++) {
				comments[i].name	= 'COL_COMMENTS';
			}
			return true;
		}else{
			return false;
		}
	}
}


function validaAltaIndice(){

	//alert('entra a validaRenombraCol');
	var sMensajes;
	var sMensajeFaltaCampos;
	
	var expressions	= document.getElementsByClassName('EXPR');
	var combos		= document.getElementsByClassName('COMBO');

	for(i = 0; i < expressions.length; i++) {

		if(expressions[i].value == '' && combos[i].options[combos[i].selectedIndex].value == '' && sMensajeFaltaCampos==null){
			sMensajeFaltaCampos = 'Para cada rengl�n debe elegir un atributo o capturar una expresi�n  \n';
		}
		
	}

	if(sMensajes != null || sMensajeFaltaCampos != null){
		alert((sMensajes==null?'':sMensajes) + '\n'  + (sMensajeFaltaCampos==null?'':sMensajeFaltaCampos) );
		return false;
	}else{
		if(confirm('�Ejecutar Proceso?')){
			return true;
		}else{
			return false;
		}
	}
}

function validaAltaChk(){

	alert('entra a validaAltaChk');
	var iNumFieldsPK = 0;
	var sMensajes;
	var sTmpColumnName;
	var dupCols = new Array();
	var countDupl = 0;
	var bYaRegistrada = false;
	var sMensajeFaltaCampos;
	
	names 		= document.getElementsByClassName('CHKNAME');
	atributo 	= document.getElementsByClassName('ATRIB');
	datyps 		= document.getElementsByClassName('DATYP');
	operador	= document.getElementsByClassName('OPER');
	expini 		= document.getElementsByClassName('EXPINI');
	expfin		= document.getElementsByClassName('EXPFIN');


	for(i = 0; i < names.length; i++) {

		if(atributo[i].options[atributo[i].selectedIndex].value == '' && operador[i].options[operador[i].selectedIndex].value != 'EXP'){
			if(sMensajes == null)
				sMensajes = '';
			sMensajes = sMensajes + ' - Para la modalidad elegida del atributo ' + names[i].value + ' se debe seleccionar un atributo de la tabla \n'; 
		}

		if(expfin[i].value == '' && operador[i].options[operador[i].selectedIndex].value == 'BETWEEN'){
			if(sMensajes == null)
				sMensajes = '';
			sMensajes = sMensajes + ' - Para la modalidad elegida del atributo ' + names[i].value + ' se debe capturar un valor en la expresi�n final \n'; 
		}
		
		if(names[i].value == '' || expini[i].value == '' && sMensajeFaltaCampos==null){
			sMensajeFaltaCampos = 'verifique los campos que faltan de capturar  \n';
		}
	}

	if(sMensajes != null || sMensajeFaltaCampos != null){
		alert(sMensajes + '\n'  + (sMensajeFaltaCampos==null?'':sMensajeFaltaCampos) );
		return false;
	}else{
		if(confirm('�Ejecutar Proceso?')){
		
			for(i = 0; i < names.length; i++) {
				names[i].name	= 'NOM_RESTRICCION';
				expini[i].name	= 'EXPRESION_INI';
				expfin[i].name	= 'EXPRESION_FIN';
				dfVal[i].name	= 'DEFAULT_VALUE';
				comms[i].name	= 'COL_COMMENTS';
			}

		
			return true;
		}else{
			return false;
		}
	}
} 


function validaAltaFk(){

	//alert('entra a validaAltaFk');
	var ConstraintRefK = document.getElementById('CONSTRAINT_REFERENCIA');
	
	if(ConstraintRefK == null){
		alert('Debe seleccionar una entidad destino que contenga una restricci�n �nica o una llave primaria');
		return false;
	}else{
		if(confirm('�Ejecutar Proceso?')){
			return true;
		}else{
			return false;
		}
	}
} 


function ActualizaDefault(comboId){

	//alert('ENTRA A ActualizaDefault');

	var combo = document.getElementById(comboId);
	
	if(combo.options[combo.selectedIndex].value == 'TIMESTAMP' || combo.options[combo.selectedIndex].value == 'DATE'){
		var inputDefValue = document.getElementById(comboId.replace("dataType","DEFAULT_VALUE_"));
		
		if (combo.options[combo.selectedIndex].value == 'TIMESTAMP'){
			inputDefValue.placeholder ='SYSTIMESTAMP o DD/MM/YYYY HH:MI:SS';
			var vlSize = document.getElementById(comboId.replace("dataType","size"));
			vlSize.value = 6;
		}else{
			inputDefValue.placeholder ='SYSDATE o DD/MM/YYYY HH:MI:SS';
		}
	}else{
		if(combo.options[combo.selectedIndex].value == 'CLOB' || combo.options[combo.selectedIndex].value == 'BFILE' || combo.options[combo.selectedIndex].value == 'BLOB'){
			var inputDefValue = document.getElementById(comboId.replace("dataType","DEFAULT_VALUE_"));
			inputDefValue.placeholder ='NO APLICA';
		}else{
			if(combo.options[combo.selectedIndex].value == 'NUMBER' || combo.options[combo.selectedIndex].value == 'VARCHAR2'){
				var inputDefValue = document.getElementById(comboId.replace("dataType","DEFAULT_VALUE_"));
				inputDefValue.placeholder ='Default Value';
			}
		}
	}
}


 

function onlyNumbers(e){

	var unicode= e.charCode? e.charCode : e.keyCode
	if (unicode!=8){ //if the key isn't the backspace or underscore key (which we should allow)
	    if (!(unicode>47&&unicode<58)){ //if not a number
		return false //disable key press
	    }
	}
}


function ValidateDate(testdate) {
        var Status;
        //var reg = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;
		//var fechaHora = /^(([0]?[1-9]|1[0-2])\/([0-2]?[0-9]|3[0-1])\/[1-2]\d{3}) (20|21|22|23|[0-1]?\d{1}):([0-5]?\d{1}):([0-5]?\d{1})$/g;
		var fechaHora = /^(([0-2]?[0-9]|3[0-1])\/([0]?[1-9]|1[0-2])\/[1-2]\d{3}) (20|21|22|23|[0-1]?\d{1}):([0-5]?\d{1}):([0-5]?\d{1})$/g; 
		//var fecha = /^(([0]?[1-9]|1[0-2])\/([0-2]?[0-9]|3[0-1])\/[1-2]\d{3})$/g;
		var fecha = /^(([0-2]?[0-9]|3[0-1])\/([0]?[1-9]|1[0-2])\/[1-2]\d{3})$/g;
        if (fechaHora.test(testdate) || fecha.test(testdate)) {
            Status = true;
			//alert('SI es fecha');
        }else{
			//alert('NO es fecha');
			Status = false;
		}
        return Status;
    }
	
	
	

function CambiaFormMethodSubmit(form,method,idEjecutable){

	if(idEjecutable== -121){

		var comboEntidadOrigen 	= document.getElementById('ENTIDAD_ORIGEN');
		var selectedOrigen		= comboEntidadOrigen.options[comboEntidadOrigen.selectedIndex];
		
		var comboEntidadDestino	= document.getElementById('ENTIDAD_DESTINO');
		var selectedDestino		= comboEntidadDestino.options[comboEntidadDestino.selectedIndex];
		
		//alert('selectedOrigen.value=>'  + selectedOrigen.value  + '<');
		//alert('selectedDestino.value=>' + selectedDestino.value + '<');
		
		if(selectedDestino.value !== '' && selectedOrigen.value !== ''){

			var formulario = document.getElementById(form);
			formulario.method = method;
			formulario.submit();
		}
	}else{
		//alert('entra al otro else');
		var formulario = document.getElementById(form);
		formulario.method = method;
		formulario.submit();
	}
}



/*********************************************************************************************************************************************************************/

    // Funci�n para clonar una fila y pegarla en la tabla donde se capturan los atributos de una tabla 
    function cloneRow()
        {
            //alert('entra cloneRow' ); 
			var myform = document.getElementById("formValidate"); // find form to append to
            var table = document.getElementById("tableToModify"); // find table to append to
            var tbody = document.getElementById("tableToModify").getElementsByTagName("tbody")[0];
            //alert('paso 1 ' );
            var innerRows = table.getElementsByTagName("tr");
            //alert('paso 2 ');
            var row = innerRows[1];
            //alert('paso 3 ');
            //alert('id del primero ' + row.getAttribute('id'));

            //var row = document.getElementById("rowToClone"); // find row to copy
            var clone = row.cloneNode(true); // copy children too
            var rowCount = table.rows.length - 1;
			var newId = String(rowCount + 1);
			
			while(typeof document.getElementsByName("COLUMN_NAME_" + newId)[0] != "undefined"){
				newId = String(parseInt(newId) + 1);   
			}   
			
            //alert('Hay ' + rowCount + ' Rows ya restado 1');
            //var newId = "rowAtributo" ; // String(rowCount + 1);
            //alert('newId: ' + newId);
            clone.id = "rowAtributo" + newId; // change id or other attributes/contents
            //clone.style.display = '';
            
            //get the array of all cells in that row and enable inputs 
            var cells    = clone.getElementsByTagName("td");
            var myInput  = cells[3].getElementsByTagName("input")[0];
            myInput.value = '';   
            myInput.id = "Atributo" + newId;
			myInput.name = "COLUMN_NAME_" + newId;
            myInput    = cells[3].getElementsByTagName("input")[1];
            myInput.value = newId;
            //myInput.disabled = false;
            //alert('Nombre: ' + myInput.getAttribute('name'));
            //alert('Valor: ' + myInput.value);

            //alert(myInput.getAttribute('id'));
            myInput  = cells[4].getElementsByTagName("select")[0];
			cells[4].removeChild(cells[4].getElementsByTagName('div')[0]);
			var dataTypeId = "dataType" + newId;
            myInput.options[0].selected = true;
            myInput.id = "dataType" + newId; 
			myInput.style.display = 'block';
			
			//alert('pasa el select');
            myInput  = cells[5].getElementsByTagName("input")[0];
            myInput.value = 0;
            myInput.id = "size" + newId;  
			myInput.name = "N_SIZE_" + newId;  
            //myInput.disabled = false; 
            myInput  = cells[6].getElementsByTagName("input")[0];
            myInput.value = 0;
            myInput.id = "scale" + newId;  
			myInput.name = "N_SCALE_" + newId;  
            //myInput.disabled = false; 
            myInput  = cells[7].getElementsByTagName("input")[0];
            myInput.checked = false;
            myInput.name = 'IS_NOT_NULL_' + newId;
			myInput.id   = 'IS_NOT_NULL_' + newId;
			//myInput.className = "";
			
			//alert(' va por el div 1');
			//var myDiv    = cells[7].getElementsByTagName("span")[0];
			//myDiv.parentNode.removeChild(myDiv);
			//cells[7].appendChild(myInput);
			
            //myInput.disabled = false; 
            //myInput.value = 'IS_NOT_NULLABLE_' + String(rowCount + 1);
            //alert('Nuevo Name NN : ' + myInput.name);
            myInput  = cells[8].getElementsByTagName("input")[0];
            myInput.checked = false;
            myInput.name = 'IS_PRIMARY_KEY_' + newId;

			//myInput.className = "";
			//myInput.className = "PK";
			//myDiv    = cells[8].getElementsByTagName("span")[0];
			//myDiv.parentNode.removeChild(myDiv);
			//cells[8].appendChild(myInput);

            //myInput.disabled = false; 
            //myInput.value = 'IS_PRIMARY_KEY_' + String(rowCount + 1);
            //alert('Nuevo name PK : ' + myInput.name);
            myInput  = cells[9].getElementsByTagName("input")[0];
            myInput.value = '';         
            myInput.id = "DEFAULT_VALUE_" + newId;
			myInput.name = "DEFAULT_VALUE_" + newId;
			myInput.placeholder = 'Valor Default';
            myInput  = cells[10].getElementsByTagName("input")[0];
            myInput.id = "COL_COMMENTS_" + newId;
			myInput.name = "COL_COMMENTS_" + newId;
            myInput.value = '';
			
            tbody.appendChild(clone); // add new row to end of table body
			$("#" + dataTypeId).chosen({allowClear:true}); 	
        }

	// Funci�n para clonar una fila y pegarla en la tabla donde se capturan los atributos de una tabla 
    function cloneRowAdd()
        {
            //alert('entra ' );
            var table = document.getElementById("tableToModify"); // find table to append to
            var tbody = document.getElementById("tableToModify").getElementsByTagName("tbody")[0];
            //alert('paso 1 ' );
            var innerRows = table.getElementsByTagName("tr");
            //alert('paso 2 ');
            var row = innerRows[1];
            //alert('paso 3 ');

            //var row = document.getElementById("rowToClone"); // find row to copy
            var clone = row.cloneNode(true); // copy children too
            var rowCount = table.rows.length - 1;

			var newId = String(rowCount + 1);

			while(document.getElementById("COLUMN_NAME_" + newId)){
				newId = String(parseInt(newId) + 1);   
			}   
			
            clone.id = "rowAtributo" + newId; // change id or other attributes/contents
            
            //get the array of all cells in that row and enable inputs 
            var cells    = clone.getElementsByTagName("td");
            var myInput  = cells[3].getElementsByTagName("input")[0];
            myInput.value = '';   
            myInput.id = "COLUMN_NAME_" + newId;      
            myInput.name = "COLUMN_NAME_" + newId;

            myInput  = cells[3].getElementsByTagName("input")[1];
            myInput.value = newId;

            myInput  = cells[4].getElementsByTagName("select")[0];
			cells[4].removeChild(cells[4].getElementsByTagName('div')[0]);
			myInput.options[0].selected = true;
            myInput.id = "dataType" + newId;  
			myInput.style.display = 'block';


            myInput  = cells[5].getElementsByTagName("input")[0];
            myInput.value = 0;
            myInput.id = "size" + newId;  
			myInput.name = "N_SIZE_" + newId;  
            //myInput.disabled = false; 
            myInput  = cells[6].getElementsByTagName("input")[0];
            myInput.value = 0;
            myInput.id = "scale" + newId;  
			myInput.name = "N_SCALE_" + newId;  

            myInput  = cells[7].getElementsByTagName("input")[0];
            myInput.checked = false;
			myInput.id 	 = 'IS_NOT_NULL_' + newId;
            myInput.name = 'IS_NOT_NULL_' + newId;
            //alert('Nuevo Name NN : ' + myInput.name);
            myInput  = cells[8].getElementsByTagName("input")[0];
            myInput.value = '';         
            myInput.id = "DEFAULT_VALUE_" + newId;
            myInput  = cells[9].getElementsByTagName("input")[0];     
            myInput.id = "COL_COMMENTS_" + newId; 
            myInput.name = "COL_COMMENTS_" + newId;
            myInput.value = '';

            tbody.appendChild(clone); // add new row to end of table body
			$("#dataType" + newId).chosen({allowClear:true}); 	
        }
		
        function createRow()
        {
            var row = document.createElement('tr'); // create row node
            var col = document.createElement('td'); // create column node
            var col2 = document.createElement('td'); // create second column node
            row.appendChild(col); // append first column to row
            row.appendChild(col2); // append second column to row
            col.innerHTML = "qwe"; // put data in first column
            col2.innerHTML = "rty"; // put data in second column
            var table = document.getElementById("tableToModify"); // find table to append to
            table.appendChild(row); // append row to table
        }

    function getId( el ) {
		//alert('this: ' + this);
		//alert('el: ' + el);
		//alert('el.nodeName: ' + el.nodeName);
        while( (el = el.parentNode) && el.nodeName.toLowerCase() !== 'tr' );

        if( el )
             return el.getAttribute('id');
            //return el.rowIndex;
    }

    
    
    function deleteRow(rowID) {
        //alert('entra ' + rowID);
        var row = document.getElementById(rowID);
        var table = document.getElementById('tableToModify');
        
        if(table.getElementsByTagName("tr").length > 2){
            row.parentNode.removeChild(row);
        }else{
            alert('Debe capturar al menos un campo');
        }
    }


    function move(TrId,direction){
        //alert(' El rengl�n actual es ' + TrId);
        var row = document.getElementById(TrId);
        //var table = document.getElementById('tableToModify');
        var table = row.parentNode;
        
        
        //alert('Renglones en la tabla: ' + table.getElementsByTagName("tr").length);
        //alert('Id del row = ' + row.getAttribute('id'));
        //alert('id del Papa del row = ' + row.parentNode.getAttribute('id'));

        if (direction==-1) {
        
            //alert(' Va a subir row.previousElementSibling = ' + row.previousElementSibling);
            
            if(row.previousElementSibling != null && row.previousElementSibling.getAttribute('id') != null){
            //if(row.previousElementSibling != null){
            
                //alert('El previo tiene un valor');
                var vPrevSibiling = row.previousElementSibling;
                
                //alert('id del anterior: ' + vPrevSibiling.getAttribute('id'));
                //alert('el tipo de nodo es: ' + vPrevSibiling.nodeName);
                //alert('el index del anterior es: ' + vPrevSibiling.rowIndex);
                if(vPrevSibiling.getAttribute('id') != 'encabezado'){
                    //alert(' va a mover el renglon');
                    //alert(' parentNode = ' + vPrevSibiling.parentNode);
                    //alert(' parentNode.id = ' + vPrevSibiling.parentNode.getAttribute('id'));
                    //var papaNode = vPrevSibiling.parentNode;
                    //alert('Pap� del papa parentNode.id = ' + papaNode.parentNode.getAttribute('id'));
                    //vPrevSibiling.parentNode = table;
                    //alert('Asigna el parentNode ');
                    //row.parentNode = table;
                    //alert('Asigna el parentNode a row');
                    table.insertBefore(row,vPrevSibiling);
                    //alert(' despues de moverlo');
                }
            }else{
                //alert('El previo es nulo ');
                //alert('row.previousElementSibling = ' + row.previousElementSibling);
                //alert('row.previousElementSibling.getAttribute(id) = ' + row.previousElementSibling.getAttribute('id'));
                return;
            }

        }else{

            //alert('va a bajar ');
            
            if(row.nextSibling != null){
                //alert(' el elemento de abajo no es nulo');
                var vNextSibiling = row.nextSibling;
                //alert('el tipo de nodo es: ' + vNextSibiling.nodeName);
                //alert('va a preguntar por el id');
                //alert('Id: ' + vNextSibiling.getAttribute('id'));
                table.insertBefore(row,vNextSibiling.nextSibling);
            }else{
            
                //alert('El pr�ximo es nulo');
                return;
            }
        }
    }
	
	
	

	//Funci�n para confirmar la ejecuci�n del proceso, si se confirma regresa verdadero y el submit se concluye
    function confirmaEjecucion(vlIdEjecutable){
        
		//alert('entra a validar proceso vlIdEjecutable: ' + vlIdEjecutable);
		if(vlIdEjecutable == -128){

            //alert('es la 101');     
            return validaAltaIndice();       
		}else{

			if(vlIdEjecutable == -103){

				//alert('es la 101');     
				return validaModifColComment();       
			}else{

				if(vlIdEjecutable == -110){

					//alert('es la 101');     
					return validaRenombraCol();       
				}else{

					if(vlIdEjecutable == -101){

						//alert('es la 101');     
						return validaAltaEntidad(document);       
					}else{
					
						if(vlIdEjecutable == -102){

							//alert('es la 101');     
							return validaAgregaAtributoEntidad(document);       

						}else{
							if(vlIdEjecutable == -149){

								//alert('es la 149');     
								return validaAltaChk();       

							}else{
								if(vlIdEjecutable == -121){
						
									//alert('es la 121');     
									return validaAltaFk();       

								}else{
									//alert('entra a else: ');
									if(confirm('�Ejecutar Proceso?')){
										//alert('entra a validar proceso vlIdEjecutable: ' + vlIdEjecutable);
										var botonEjecutar = document.getElementById('ButtonExec');
										var botonBack = document.getElementById('ButtonBack');

										//alert('botonEjecutar: ' + botonEjecutar);
										//alert('botonBack: ' + botonBack);
										//deshabilita los botones
										botonEjecutar.disable = true;
										//alert('botonBack: ' + botonBack);
										botonBack.disable = true;
										//alert('botonBack: ' + botonBack);

										if(vlIdEjecutable== -132){
											alert('Este proceso puede tardar tiempo en terminar, si la p�gina no responde r�pidamente no intente volver a ejecutar el script, verifique en la p�gina de log de procesos el estatus del mismo');
										}
										return true;

									}else{
										return false;
									}
								}
							}
						}
					}
				}
			}
		}
    }
     
    
function showhidefieldsPassword(myField)
{
    
    //alert('hola esta entrando');
    var a = (document.getElementById("SHR_" + myField)).style;
    //alert(a.display);
    if(a.display == 'none'){
        //alert('Entra a condicion');
        document.getElementById("SHR_" + myField).style.display = "table-row";
        document.getElementById("SHR_" + myField + "1").style.display = "table-row";
        document.getElementById("SHR_" + myField + "2").style.display = "table-row";
        document.getElementById("l" + myField).innerHTML = "Cancelar Cambio";
        //alert('Acaba de poner display');
        document.getElementById(myField).required = true;
        document.getElementById(myField).disabled  = false;
        //alert('Asigna required 1');
        document.getElementById(myField + "1").required = true;
        //alert('Asigna required 2');
        document.getElementById(myField + "2").required = true;
        //alert('Asigna required 3');
        //alert(document.getElementById(myField).required);
    }
    else{
    //    alert('Entra a else');
        document.getElementById("SHR_" + myField).style.display = "none";
        document.getElementById("SHR_" + myField + "1").style.display = "none";
        document.getElementById("SHR_" + myField + "2").style.display = "none";
        document.getElementById("l" + myField).innerHTML = "Cambio de Password";
        document.getElementById(myField).required = false;
        document.getElementById(myField).disabled  = true;
        document.getElementById(myField + "1").required = false;
        document.getElementById(myField + "2").required = false;
    }
    
}



    

/*****************************************************************************************************************************************************/

    // Funci�n para clonar una fila y pegarla en la tabla donde se capturan los atributos de una tabla 
    function cloneRowAltaIndex()
        {
            //alert('entra ' );
            var table = document.getElementById("tableToModify"); // find table to append to
            var tbody = document.getElementById("tableToModify").getElementsByTagName("tbody")[0];
			var myform = table.form;
            //alert('paso 1 ' );
            var innerRows = table.getElementsByTagName("tr");
            //alert('paso 2 ');
            var row = innerRows[1];
            
            //var row = document.getElementById("rowToClone"); // find row to copy
            var clone = row.cloneNode(true); // copy children too
            var rowCount = table.rows.length - 1;
			var newId = String(rowCount + 1);

			while(document.getElementById("ATRIBUTO_INDEX_" + newId)){
				newId = String(parseInt(newId) + 1);   
			}   
			//alert('paso 3 ');
            clone.id = "rowAtributo" + newId; // change id or other attributes/contents
            
            //get the array of all cells in that row and enable inputs 
            var cells   = clone.getElementsByTagName("td");
			//alert('Si tiene td ');
			myInput  	= cells[1].getElementsByTagName("select")[0];
			cells[1].removeChild(cells[1].getElementsByTagName('div')[0]);
            myInput.options[0].selected = true;
			myInput.id = "ATRIBUTO_INDEX_" + newId;
			myInput.style.display = 'block';

			myInput  	= cells[2].getElementsByTagName("input")[0];
			myInput.value = '';
            tbody.appendChild(clone); // add new row to end of table body
			$("#ATRIBUTO_INDEX_" + newId).chosen({allowClear:true}); 	
        }


    // Funci�n para clonar una fila y pegarla en la tabla donde se capturan los atributos de una tabla 
    function cloneRowAltaRefUnico()
        {
            //alert('entra ' );
            var table = document.getElementById("tableToModify"); // find table to append to
            var tbody = document.getElementById("tableToModify").getElementsByTagName("tbody")[0];
			var myform = table.form;
            //alert('paso 1 ' );
            var innerRows = table.getElementsByTagName("tr");
            //alert('paso 2 ');
            var row = innerRows[1];
            //alert('paso 3 ');
            //alert('id del primero ' + row.getAttribute('id'));
            
            
            //var row = document.getElementById("rowToClone"); // find row to copy
            var clone = row.cloneNode(true); // copy children too
            var rowCount = table.rows.length - 1;
			
			var newId = String(rowCount + 1);
			
			while(document.getElementById("ATRIBUTO_" + newId)){
				newId = String(parseInt(newId) + 1);   
			}   
			
            clone.id = "rowAtributo" + newId; // change id or other attributes/contents
            
            //get the array of all cells in that row and enable inputs 
            var cells   = clone.getElementsByTagName("td");
			//alert('Si tiene td ');
			myInput  	= cells[1].getElementsByTagName("select")[0];
			cells[1].removeChild(cells[1].getElementsByTagName('div')[0]);
            myInput.options[0].selected = true;
			myInput.style.display = 'block';
			myInput.id = "ATRIBUTO_" + newId;
			//alert('New id: ' + myInput.id);
			cells[1].appendChild(myInput);

            tbody.appendChild(clone); // add new row to end of table body
			$("#" + "ATRIBUTO_" + newId).chosen({allowClear:true}); 
            //alert(newId);
        }

	function cloneRowAltaPK()
        {
            //alert('entra a cloneRowAltaPK' ); 
            var table = document.getElementById("tableToModify"); // find table to append to
            var tbody = document.getElementById("tableToModify").getElementsByTagName("tbody")[0];
			var myform = table.form;
            //alert('paso 1 ' );
            var innerRows = table.getElementsByTagName("tr");
            //alert('paso 2 ');
            var row = innerRows[1];
            //alert('paso 3 ');
            //alert('id del primero ' + row.getAttribute('id'));
			
			
            //var row = document.getElementById("rowToClone"); // find row to copy
            var clone = row.cloneNode(true); // copy children too
            var rowCount = table.rows.length - 1;
            //alert('Hay ' + rowCount + ' Rows ya restado 1');
			var newId = String(rowCount + 1);
			
			while(document.getElementById("ATRIBUTO_" + newId)){
				newId = String(parseInt(newId) + 1);   
				//alert(newId);
			}   

            //alert('newId: ' + newId);
            clone.id = "rowAtributo" + newId; // change id or other attributes/contents
            clone.style.display = '';
            
            //get the array of all cells in that row and enable inputs 
            var cells   = clone.getElementsByTagName("td");
			//alert('Si tiene td ');
			myInput  	= cells[1].getElementsByTagName("select")[0];
			cells[1].removeChild(cells[1].getElementsByTagName('div')[0]);
            myInput.options[0].selected = true;
			myInput.style.display = 'block';
			var newSelectId = "ATRIBUTO_" + newId;
			myInput.id = newSelectId;

			cells[1].appendChild(myInput);
			//alert('antes de agregar  ');
            tbody.appendChild(clone); // add new row to end of table body
			$("#" + newSelectId).chosen({allowClear:true}); 	
			//alert('despu�s de agregar  ');
        }


	function cloneRowRenombraCol()
        {
            //alert('entra ' );
            var table = document.getElementById("tableToModify"); // find table to append to
            var tbody = document.getElementById("tableToModify").getElementsByTagName("tbody")[0];
			var myform = table.form;
            //alert('paso 1 ' );
            var innerRows = table.getElementsByTagName("tr");
            //alert('paso 2 ');
            var row = innerRows[1];
            //alert('paso 3 ');
            //alert('id del primero ' + row.getAttribute('id'));
            
            
            //var row = document.getElementById("rowToClone"); // find row to copy
            var clone = row.cloneNode(true); // copy children too
            var rowCount = table.rows.length - 1;
            var newId = String(rowCount + 1);
			
			while(typeof document.getElementsByName("NOMBRE_NUEVO_" + newId)[0] != "undefined"){
				newId = String(parseInt(newId) + 1);   
			}   
			
            clone.id = "rowAtributo" + newId; // change id or other attributes/contents
            clone.style.display = '';
            
            //get the array of all cells in that row and enable inputs 
            var cells   = clone.getElementsByTagName("td");
			//alert('Si tiene td ');
			myInput  	= cells[1].getElementsByTagName("select")[0];
			cells[1].removeChild(cells[1].getElementsByTagName('div')[0]);
            myInput.options[0].selected = true;
			myInput.id = 'NOM_ATRIBUTO_' + newId;
			myInput.style.display = 'block';

			myInput  	= cells[2].getElementsByTagName("input")[0];
			//alert('paso 18');
			myInput.value = '';
			myInput.id = 'NOMBRE_NUEVO_' + newId;
			myInput.name = 'NOMBRE_NUEVO_' + newId;
			myInput.placeholder = 'Nuevo Nombre';
			//alert('paso 19');
			
            tbody.appendChild(clone); // add new row to end of table body
			$("#NOM_ATRIBUTO_" + newId).chosen({allowClear:true}); 	
        }			

	function cloneRowDrop()
        {
            //alert('entra ' );
            var table = document.getElementById("tableToModify"); // find table to append to
            var tbody = document.getElementById("tableToModify").getElementsByTagName("tbody")[0];
			var myform = table.form;
            //alert('paso 1 ' );
            var innerRows = table.getElementsByTagName("tr");
            //alert('paso 2 ');
            var row = innerRows[1];
            //alert('paso 3 ');
            //alert('id del primero ' + row.getAttribute('id'));
            
            
            //var row = document.getElementById("rowToClone"); // find row to copy
            var clone = row.cloneNode(true); // copy children too
            var rowCount = table.rows.length - 1;
            //alert('Hay ' + rowCount + ' Rows ya restado 1');
			var newId = String(rowCount + 1);
			
			while(typeof document.getElementsByName("ATRIBUTO_" + newId)[0] != "undefined"){
				newId = String(parseInt(newId) + 1);   
			}   
			
            //alert('newId: ' + newId);
            clone.id = "rowAtributo" + newId; // change id or other attributes/contents
            clone.style.display = '';
            
            //get the array of all cells in that row and enable inputs 
            var cells   = clone.getElementsByTagName("td");
			//alert('Si tiene td ');
			myInput  	= cells[1].getElementsByTagName("select")[0];
			cells[1].removeChild(cells[1].getElementsByTagName('div')[0]);
            myInput.options[0].selected = true;
			myInput.id = "ATRIBUTO_" + newId;

            tbody.appendChild(clone); // add new row to end of table body
			$("#ATRIBUTO_" + newId).chosen({allowClear:true}); 	
        }
		
    // Funci�n para clonar una fila y pegarla en la tabla donde se capturan los atributos de una tabla 
    function cloneRowAltaChk()
        {
            //alert('entra ' );
            var table = document.getElementById("tableToModify"); // find table to append to
            var tbody = document.getElementById("tableToModify").getElementsByTagName("tbody")[0];
            //alert('paso 1 ' );
            var innerRows = table.getElementsByTagName("tr");
            //alert('paso 2 ');
            var row = innerRows[1];
			var myform = table.form;
            //alert('paso 3 ');
            //alert('id del primero ' + row.getAttribute('id'));
            
            
            var clone = row.cloneNode(true); // copy children too
            var rowCount = table.rows.length - 1;
            //alert('Hay ' + rowCount + ' Rows ya restado 1');
            var newId = String(rowCount + 1);
			
			while(document.getElementById("NOM_RESTRICCION_" + newId)){
				newId = String(parseInt(newId) + 1);   
			}
			
            //alert('newId: ' + newId);
            clone.id = "rowAtributo" + newId; // change id or other attributes/contents
            //clone.style.display = '';

            //get the array of all cells in that row and enable inputs 
            var cells   = clone.getElementsByTagName("td");
			
			myInput  	= cells[1].getElementsByTagName("input")[0];
			myInput.value = '';
			myInput.id = 'NOM_RESTRICCION_' + newId;
			myInput.name = 'NOM_RESTRICCION_' + newId;
			
			//alert('Si tiene td ');
			myInput  	= cells[2].getElementsByTagName("select")[0];
			cells[2].removeChild(cells[2].getElementsByTagName('div')[0]);
            myInput.options[0].selected = true;
			myInput.id = 'ATRIBUTO_' + newId;
			myInput.style.display = 'block';
			
			var selected = myInput.options[0];
			var atributo = selected.getAttribute('Atributo');
			myInput  	= cells[2].getElementsByTagName("input")[0];
			//alert('paso 14');
			myInput.value = atributo;
			myInput.id = 'DATA_TYPE_' + newId;
			//alert('paso 15');

			myInput  	= cells[3].getElementsByTagName("select")[0];
			cells[3].removeChild(cells[3].getElementsByTagName('div')[0]);
            myInput.options[0].selected = true;
			myInput.id = 'OPERADOR_' + newId;
			myInput.style.display = 'block';
			
			myInput  	= cells[4].getElementsByTagName("input")[0];
			//alert('paso 18');
			myInput.value = '';
			myInput.id = 'EXPRESION_INI_' + newId;
			myInput.name = 'EXPRESION_INI_' + newId;
			myInput.placeholder = 'Listado de valores';
			//alert('paso 19');
			
            myInput  	= cells[4].getElementsByTagName("input")[1];
			//alert('paso 20');
			myInput.value = '';
			myInput.id = 'EXPRESION_FIN_' + newId;
			myInput.name = 'EXPRESION_FIN_' + newId;
			myInput.placeholder = 'N/A';
			myInput.required = false;
			//alert('paso 21');
			
			tbody.appendChild(clone); // add new row to end of table body
			$("#" + "OPERADOR_" + newId).chosen({allowClear:true}); 
			$("#" + "ATRIBUTO_" + newId).chosen({allowClear:true}); 
        }
		
		function cloneRowBorraRestric(){
            //alert('entra ' );
            var table = document.getElementById("tableToModify"); // find table to append to
            var tbody = document.getElementById("tableToModify").getElementsByTagName("tbody")[0];
            //alert('paso 1 ' );
            var innerRows = table.getElementsByTagName("tr");
			var myform = table.form;

            //alert('paso 2 ');
            var row = innerRows[1];
            //alert('paso 3 ');
            //alert('id del primero ' + row.getAttribute('id'));
            
            
            var clone = row.cloneNode(true); // copy children too
            var rowCount = table.rows.length - 1;
            //alert('Hay ' + rowCount + ' Rows ya restado 1');
			
			var newId = String(rowCount + 1);
			
			while(document.getElementById("NOM_RESTRICCION_" + newId)){
				newId = String(parseInt(newId) + 1);   
			}   

            clone.id = "rowAtributo" + newId; // change id or other attributes/contents
            
            //get the array of all cells in that row and enable inputs 
            var cells   = clone.getElementsByTagName("td");
			
			myInput  	= cells[1].getElementsByTagName("select")[0];
			cells[1].removeChild(cells[1].getElementsByTagName('div')[0]);
            myInput.options[0].selected = true;
			myInput.id = 'NOM_RESTRICCION_' + newId;
			myInput.style.display = 'block';
			
			myInput  	= cells[2].getElementsByTagName("input")[0];
			//alert('paso 10');
            myInput.checked = false;
			myInput.id = 'EN_CASCADA_' + newId;
			myInput.name = 'EN_CASCADA_' + newId;
			
			tbody.appendChild(clone); // add new row to end of table body
			$("#" + "NOM_RESTRICCION_" + newId).chosen({allowClear:true}); 
        }

		
		function CloneRowModComentario(){
		    //alert('entra ' );
            var table = document.getElementById("tableToModify"); // find table to append to
            var tbody = document.getElementById("tableToModify").getElementsByTagName("tbody")[0];
            //alert('paso 1 ' );
            var innerRows = table.getElementsByTagName("tr");
            //alert('paso 2 ');
            var row = innerRows[1];
            //alert('paso 3 ');
            //alert('id del primero ' + row.getAttribute('id'));
            
            
            var clone = row.cloneNode(true); // copy children too
            var rowCount = table.rows.length - 1;
			var newId = String(rowCount + 1);

			while(document.getElementById("COL_COMMENTS_" + newId)){
				newId = String(parseInt(newId) + 1);   
			}   
			
            clone.id = "rowAtributo" + newId; // change id or other attributes/contents
            
            //get the array of all cells in that row and enable inputs 
            var cells   = clone.getElementsByTagName("td");
			
			myInput  	= cells[1].getElementsByTagName("select")[0];
			cells[1].removeChild(cells[1].getElementsByTagName('div')[0]);
            myInput.options[0].selected = true;
			myInput.id = 'COLUMN_NAME_' + newId;
			myInput.style.display = 'block';
			
			myInput  	= cells[2].getElementsByTagName("input")[0];
			myInput.id = 'COL_COMMENTS_' + newId;
			myInput.name = 'COL_COMMENTS_' + newId;

			tbody.appendChild(clone); // add new row to end of table body
			$("#COLUMN_NAME_" + newId).chosen({allowClear:true}); 	
			ActualizaDatosModAttribComment('COLUMN_NAME_' + newId);
			
        }

/*****************************************************************************************************************************************************/

// Popup window code
function newPopup(url) {
    popupWindow = window.open(
        url,'popUpWindow','height=500,width=1100,left=700,top=100,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=yes')
        //window.open("width=400,height=300,screenX=50,left=50,screenY=50,top=50,status=yes,menubar=yes");
}


function MarcaEliminaFile (s){
    
    s.value = 0;
    alert("El archivo se ha marcado para ser borrado");
    //alert("Hola: " + s + " valor del campo: " + s.value);
}

function confirmaBorrado(){
    
    if(confirm('�Est� seguro que desea borrar el registro y sus posibles dependencias?')){
        //document.frmAltaMod.PARAM_CTL_BAJA.value = 'borrar';
        document.formValidate.PARAM_CTL_OPERACION.value = 'B';
        document.formValidate.submit();
     }

}


function UpdateRecord(){
    
	document.formValidate.PARAM_CTL_OPERACION.value = 'M';
	document.formValidate.submit();
}


function submitForm(tipoOperacion){
    
    document.formCreateUpdate.PARAM_CTL_OPERACION.value = tipoOperacion;
    document.formCreateUpdate.submit();
}


	function clear_form_elements() {
	
		var ele = document.getElementById('frmBusqueda');
	
		alert('clear_form_elements ' + ele);
		tags = ele.getElementsByTagName('input');
		for(i = 0; i < tags.length; i++) {
			switch(tags[i].type) {
				case 'password': 
				case 'text': 
				case 'datetime': 
				case 'file': 
				case 'email': 
				case 'tel': 
				case 'url': 
				case 'search': 
				case 'password': 
				case 'date': 
					tags[i].value = '';
					break;
				case 'checkbox':
				case 'radio':
					tags[i].checked = false;
					break;
			}
		}

		tags = ele.getElementsByTagName('select');
		for(i = 0; i < tags.length; i++) {
			if(tags[i].type == 'select-one') {
				//alert('tags[i].name: ' + tags[i].name);
				tags[i].selectedIndex = 0;
				//alert('tags[i].selectedIndex: ' + tags[i].selectedIndex);
				//alert('tags[i].innerHTML: ' + tags[i].innerHTML);
			}
			else {
				for(j = 0; j < tags[i].options.length; j++) {
					tags[i].options[j].selected = false;
				}
			}
		}

		tags = ele.getElementsByTagName('textarea');
		for(i = 0; i < tags.length; i++) {
			tags[i].value = '';
		}
		
	}


	function altaDesdeBusqueda() {
		//alert('este es el archivo');
		document.frmAlta.submit();
	}

	function sendValorRegreso (Objvalue,ObjDescription){

		//alert("Hola entra a la funcion: ");
		//alert("Hola este es el valor: " + s + " nombre del campo: " + document.frmBusqueda.PARAM_CTL_REGRESA_VAL_NAME.value);
		
		var selvalue = Objvalue;
		var selDescr = ObjDescription;
		//var inputDescription = window.opener.document.getElementById('CTL_DESC_' + document.formValidate.PARAM_CTL_REGRESA_VAL_NAME.value);
		//var selvalue = s.value;
		window.opener.document.getElementById(document.formValidate.PARAM_CTL_REGRESA_VAL_NAME.value).value = selvalue;
		//window.opener.document.getElementById('CTL_DESC_' + document.formValidate.PARAM_CTL_REGRESA_VAL_NAME.value).value = selDescr;
		//$('#'+ 'CTL_DESC_' + element).val("").change();
		//window.opener.document.getElementById('REF_' + document.formValidate.PARAM_CTL_REGRESA_VAL_NAME.value).value = selvalue;
		//alert('antes de obtener objeto');
		var $inputDescription = $(window.opener.document.getElementById('CTL_DESC_' + document.formValidate.PARAM_CTL_REGRESA_VAL_NAME.value)); 
		//alert('antes de asignar valor');
		$inputDescription.prop('value',ObjDescription);
		window.opener.$('.data-link').trigger('change');
		//alert('despues de asignar valor');
		window.close();
	}

	
	
	
	function ActualizaValorDataType(idCombo){
	
		//alert('entra');
		var sel = document.getElementById(idCombo);
		//alert('PASO 1');
		var selected = sel.options[sel.selectedIndex];
		//alert('PASO 1');
		var extra = selected.getAttribute('Atributo');
		//alert('PASO 3 - ' + extra);
		
		var datatype = document.getElementById('DATA_TYPE_' + idCombo.replace('ATRIBUTO_', ''));
		datatype.value = extra;
		var expini		= document.getElementById('EXPRESION_INI_' + idCombo.replace('ATRIBUTO_', ''));
		ActualizaPlaceholder('OPERADOR_' + idCombo.replace('ATRIBUTO_', ''));
		
	}
	

	function ActualizaPlaceholder(idCombo){
	
		//alert('entra');
		var sel = document.getElementById(idCombo);
		//alert('PASO 1');
		var selected = sel.options[sel.selectedIndex];
		//alert('PASO 2');
		var comboValue = sel.value;
		//alert('comboValue: >' + comboValue + '<');

		var expini		= document.getElementById('EXPRESION_INI_' + idCombo.replace('OPERADOR_', ''));
		var expFin		= document.getElementById('EXPRESION_FIN_' + idCombo.replace('OPERADOR_', ''));

		if(comboValue == 'EXP'){
			expini.placeholder = '(Expresi�n Incluyendo Atributo(s)) ';
			expFin.placeholder = 'N/A';
		}else{
		
			var atributo 		= document.getElementById('ATRIBUTO_' + idCombo.replace('OPERADOR_', ''));
			var selectedAtr 	= atributo.options[atributo.selectedIndex];
			var atribDataType 	= selectedAtr.getAttribute('Atributo');
			
			var datatype	= document.getElementById('DATA_TYPE_' + idCombo.replace('OPERADOR_', ''));
			
			datatype.value 	= atribDataType;
		
			//alert('OBTIENE OBJETOS datatype.value: ' + datatype.value);
			if(datatype.value.indexOf('CHAR') > 0){
			
				//alert('ES CHAR ');
				if(comboValue == 'BETWEEN'){
					expini.placeholder 	= '(' + datatype.value + ') ' + 'TEXTO';
					expFin.placeholder 	= '(' + datatype.value + ') ' + 'TEXTO';
					expFin.required 	= true;
				}else{
					if(comboValue == 'IN'){
						expini.placeholder = '(' + datatype.value + ') ' + 'Listado de valores';
						expFin.placeholder = 'N/A';
					}else{
						expini.placeholder = '(' + datatype.value + ') ' + 'TEXTO';
						expFin.placeholder = 'N/A';
					}
					expFin.required = false;
				}
				//if(comboValue != 'IN'){
				//	alert('Para tipos de dato de texto solo aplica el operador de listado de valores');
				//}		
			}else{
				if(datatype.value == 'DATE' || datatype.value == 'SYSTIMESTAMP'){

					if(datatype.value == 'DATE'){
					
						if(comboValue == 'IN'){
							expini.placeholder = '(' + datatype.value + ') ' + 'Listado - SYSDATE o DD/MM/YYYY HH:MI:SS';
							expFin.placeholder = 'N/A';
							expFin.required = false;
						}else{
						
							expini.placeholder = '(' + datatype.value + ') ' + 'SYSDATE o DD/MM/YYYY HH:MI:SS';
							
							if(comboValue == 'BETWEEN'){
								expFin.placeholder = '(' + datatype.value + ') ' + 'SYSDATE o DD/MM/YYYY HH:MI:SS';
								expFin.required = true;
							}else{
								expFin.placeholder = 'N/A';
								expFin.required = false;
							}
						
						}
					}else{
						if(comboValue == 'IN'){
							expini.placeholder = '(' + datatype.value + ') ' + 'Listado - SYSTIMESTAMP o DD/MM/YYYY HH:MI:SS';
							expFin.placeholder = 'N/A';
							expFin.required = false;
						}else{
							expini.placeholder = '(' + datatype.value + ') ' + 'SYSTIMESTAMP o DD/MM/YYYY HH:MI:SS';
							
							if(comboValue == 'BETWEEN'){
								expFin.placeholder = '(' + datatype.value + ') ' + 'SYSTIMESTAMP o DD/MM/YYYY HH:MI:SS';
								expFin.required = true;
							}else{
								expFin.placeholder = 'N/A';
								expFin.required = false;
							}
						}
					}
				}else{
					if(datatype.value == 'NUMBER'){
						//alert('ENTRA A NUMBER: ' );
						if(comboValue == 'IN'){
							//alert('ES IN : ' );
							expini.placeholder = '(' + datatype.value + ') ' + 'Listado de valores';
							expFin.placeholder = 'N/A';
							expFin.required = false;
						}else{
							if(comboValue == 'BETWEEN'){
								//alert('ES BETWEEN : ' );
								expini.placeholder = '(' + datatype.value + ') ' + 'Dato num�rico';
								expFin.placeholder = '(' + datatype.value + ') ' + 'Dato num�rico';
								expFin.required = true;
							}else{
								//alert('ES ELSE: ' );
								expini.placeholder = '(' + datatype.value + ') ' + 'Dato num�rico';
								expFin.placeholder = 'N/A';
								expFin.required = false;
							}
						}
					}
				}
			}
		}
		//alert('nuevo valor de data type: ' + datatype.value);
		//inputDefValue.placeholder ='SYSTIMESTAMP o DD/MM/YYYY HH:MI:SS';
	}


	function selectItemByValue(elmnt, value){

		for(var i=0; i < elmnt.options.length; i++)
		{
		  if(elmnt.options[i].value == value)
			elmnt.selectedIndex = i;
		}
	}
	

	function ActualizaDatosModEntidad(idCombo){
	
		//alert('entra');
		var comboAtributo = document.getElementById(idCombo);
		//alert('PASO 1');
		var selected = comboAtributo.options[comboAtributo.selectedIndex];
		//alert('PASO 2');
		var AtrDataType = selected.getAttribute('data_type');
		var DataType	= document.getElementById('dataType1');
		selectItemByValue(DataType, AtrDataType);

		//alert('antes de obtener');
		var AtrSize 	= selected.getAttribute('n_size');
		//alert('despu�s de obtener AtrSize: ' + AtrSize);
		var Longitud	= document.getElementById('n_size');
		//alert('va a modificar Longitud: ' + Longitud);
		Longitud.value = AtrSize;
		//alert('Modifica');
		
		var AtrEscala 	= selected.getAttribute('n_scale');
		var Escala		= document.getElementById('n_scale');
		Escala.value 	= AtrEscala;
		
		var AtrDefval	= selected.getAttribute('default_value');
		var DefVal		= document.getElementById('DEFAULT_VALUE_1');
		DefVal.value 	= AtrDefval;
		
		var AtrIsNotNull= selected.getAttribute('is_not_null');
		var IsNotNull	= document.getElementById('is_not_null');
		if(AtrIsNotNull=='V'){
			IsNotNull.checked = true;
		}else{
			IsNotNull.checked = false;
		}
	}

	
	function ActualizaDatosModAttribComment(idCombo){
	
		//alert('entra');
		var comboAtributo = document.getElementById(idCombo);
		//alert('PASO 1');
		var selected = comboAtributo.options[comboAtributo.selectedIndex];
		//alert('PASO 2');
		var AtrComment = selected.getAttribute('comentario');
		//alert('PASO 3');
		var Comment	   = document.getElementById('COL_COMMENTS_' + idCombo.replace('COLUMN_NAME_', ''));
		//alert('PASO 4');
		Comment.value = AtrComment;
		//alert('PASO 5');
	}

	
	function ActualizaDatosModCommentEnt(idCombo){
	
		//alert('entra');
		var comboAtributo = document.getElementById(idCombo);
		//alert('PASO 1');
		var selected = comboAtributo.options[comboAtributo.selectedIndex];
		//alert('PASO 2');
		var AtrComment = selected.getAttribute('comentario');
		//alert('PASO 3');
		var Comment	   = document.getElementById('COMENTARIO_ENTIDAD');
		//alert('PASO 4');
		Comment.value = AtrComment;
		//alert('PASO 5');
	}


	function ActualizaCampoDescEntidad(){
	 
		//alert('entra');
		var Entidad = document.getElementById('NOM_ENTIDAD');
		//alert('PASO 1');
		if(Entidad.value!=''){
			//alert('el valor NO es null');
			var selected = Entidad.options[Entidad.selectedIndex];
			//alert('PASO 2');
			var AtrCampo 	= selected.getAttribute('campoDesc');
			var AtrExpresion= selected.getAttribute('expresion');

			var atributo    = document.getElementById('ATRIBUTO');
			selectItemByValue(atributo, AtrCampo);
			
			var Expresion   = document.getElementById('EXPRESION'); 
			if(Expresion.value==''){
				Expresion.value = AtrExpresion;
			}
		}
	}
	
	function regresar(){
		//alert('hola');
		window.history.go(-1);
		//alert('no truena');
	}
	
	
	function GeneraOpcionesDependiente(Padre, idCombodependiente, actualizaSelected){
		console.log('GeneraOpcionesDependiente');
		//alert('hola: ' + idCombodependiente);
		//alert('Padre: ' + Padre + ' Id dependiente: ' + idCombodependiente);
		var vPadre  = document.getElementById(Padre);
		var valorPadre = vPadre.options[vPadre.selectedIndex].value;
		//alert('valor padre: ' + valorPadre);
		var selectObj = document.getElementById(idCombodependiente);
		var selectOculto = document.getElementById(idCombodependiente + '_OCULTO');
		var iOption = 0;
		
		//alert('Va a obtener las reglas ');
		console.log('antes de obtener reglas');
		try {
		var selRules = $( "#" +idCombodependiente ).rules();
		}
		catch (e)
		  {
		  alert(e);
		  }		
		    console.log('despues de obtener reglas');
		//alert('Obitne las reglas ' + selRules);

		//var selectParentNode = selectObj.parentNode;
		var wrapper = selectObj.parentNode;
		//alert('wrapper className: ' + wrapper.className );
		
		//selectParentNode.parentNode.removeChild(selectParentNode);
		
		var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
		//wrapper.removeChild(selectObj);
		
		wrapper.removeChild(wrapper.getElementsByTagName('div')[0]);
		
		while (wrapper.hasChildNodes()) {
			wrapper.removeChild(wrapper.lastChild);
		}
		//alert('borra el div');
		//newSelectObj.className = '';
		newSelectObj.style.display = 'block';
		newSelectObj.id = idCombodependiente;
		//alert('clona el objeto: ' );
		//selectParentNode.replaceChild(newSelectObj, selectObj);
		//alert('remplaza el objeto: ' );
		
		selectObj.options.length = 0;
		//selectObj.className = '';
		selectObj.style.display = 'block';
		selectObj.id = idCombodependiente;
		
		//alert('intenta el try ');
		console.log('antes de try ');
		try
		  {
			if(valorPadre != ''){
			
				for (var i=0; i<selectOculto.length; i++){
					var selected = selectOculto.options[i];
					var atributo = selected.getAttribute('atributo');
					
					//alert(' texto: ' + selectOculto.options[i].text + ' valor: ' + selectOculto.options[i].value + ' atributo: ' + atributo + ' valorPadre: ' + valorPadre);
					if((valorPadre == atributo && atributo != 'ctlSeleccione') || atributo == 'bObligatorio'){
						//alert('Entra valor iOption: ' + iOption);
						newSelectObj.options[iOption] = new Option(selectOculto.options[i].text, selectOculto.options[i].value);
						if(selectOculto.options[i].selected == true && actualizaSelected=='SI'){
							newSelectObj.options[iOption].selected = true;
						}
						iOption = iOption + 1;
					}
				}
			}else{
				newSelectObj.options[0] = new Option(selectOculto.options[0].text, selectOculto.options[0].value);
			}
		  
		  }
		catch (e)
		  {
		  selectObj.options[0] = new Option('Eror en combo depeendiente', '1');
		  }
		
		//alert(' veamos por que no regresa');
		wrapper.appendChild(newSelectObj);
		
		//  $('select', myform).jqTransSelect2();
		 $("#" + idCombodependiente).chosen({allowClear:true})
			.on('change', function(){
				if(selRules){
					console.log('entra a validar');
				$(this).closest('form').validate().element($(this));
				}
		}).trigger('resize.combo'); 		
		//alert('agrega div');
		if(selRules){
			console.log('entra a agregar regla');
		$( "#" + idCombodependiente).rules( "add", selRules);
		}
		//alert('termina la funcion');
	}

	var formReset = function(f){
		//alert('entra a la funcion reset');
		
		$("div.chosen-container").each(function() {
			var selectName = $(this).prop('id').replace("_chosen", "");
			$("#" + selectName).prop('selectedIndex', 0); 
			var selectTextFirstOption = $("#" + selectName + " option:first").text();
			$("a span", this).text(selectTextFirstOption);
			$("#" + selectName).trigger('change');
		});
		
		$(".date-picker").each(function() {
			$(this).prop('value','');
		});
		
		$(".txtReset").each(function() {
			$(this).prop('value','');
		});

		$(".chkBox").each(function() {
			$(this).prop('checked',false);
		});
		
		$(".RadioBtn").each(function() {
			$(this).prop('checked',false);
		});
		
	};
	
	$.fn.jqBtnReset = function(){
		//alert('Primer paso 2: ');
		/* each form */
		return this.each(function(){
			//alert('Primer paso 3: ');
			var selfForm = $(this);
			//alert('Primer paso 4: ' + selfForm.prop('name'));
			selfForm.bind('reset',function(){var action = function(){formReset(this);};  window.setTimeout(action, 10);});
			//alert('Termina: ' + this.name);

			
		}); /* End Form each */
		
	};/* End the Plugin */
	
	
	function submitFrmAltaMD(){
		var formAltaMD = document.getElementById('frmAltaMD');
		formAltaMD.submit();
	}
	
	function GetURLParameter(sParam){

		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++)
		{
			var sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] == sParam){
				return sParameterName[1];
			}
		}
	}

	function clearInput(element){
		var input = document.getElementById(element);
		input.value= '';
		var input = document.getElementById('CTL_DESC_' + element);
		$('#'+ 'CTL_DESC_' + element).val("").change();
		//input.value= '';
	
	}
	
	
	function setval(element){
		var input = document.getElementById('CTL_DESC_' + element);
		input.value= 'hola';
		$('#'+ 'CTL_DESC_' + element).val("Hola otra vez").change();
	
	}
	
	function justtest(){
		alert('si entra');
	
	}
	
	function setOptions(){
	
		//alert('ya entr�');
		var objReferencia = document.getElementById('CONSTRAINT_REFERENCIA');
		//alert('Obtiene el objeto ' );
		var objSelected = objReferencia.options[objReferencia.selectedIndex].value;
		//alert('Selected: ' + objSelected);
		var refValue;
		var vlDivs;

		for (i = 0; i < objReferencia.length; i++) {
			refValue = objReferencia.options[i].value; 
			vlDivs 		= document.getElementsByClassName('divs-atts');
			vlSelects	= document.getElementsByClassName('ctl-select');
			vlDestinos	= document.getElementsByClassName('ctl-input');

			for (x = 0; x < vlDivs.length; x++) {
				//alert('antes de iterar div ');
				if(!vlDivs[x].classList.contains(objSelected)){
					//alert('es distinto div ');
					vlDivs[x].classList.add("hidden");
					//alert('vlDivs[' + x + '].classList ' + vlDivs[x].classList);
				}else{
					//alert('es igual ');
					vlDivs[x].classList.remove("hidden");
					//vlDivs[x].classList.add("width-80");
				}
			}

			for (y = 0; y < vlSelects.length; y++) {
			
				if(vlSelects[y].classList.contains(objSelected + "-select-origen")){
					//alert('habilita por que es igual' + vlSelects[y]);
					vlSelects[y].disabled = false;
					//alert('lo desabilita : ' + vlSelects[y].name + ' ' + vlSelects[y].disabled);
				}else{
					//alert('Deshabilita por que es diferente' + vlSelects[y]);
					vlSelects[y].disabled = true;
				}
				if(vlDestinos[y].classList.contains(objSelected + "-input-destino")){
					//alert('habilita por que es igual' + vlSelects[y]);
					vlDestinos[y].disabled = false;
					//alert('lo desabilita : ' + vlSelects[y].name + ' ' + vlSelects[y].disabled);
				}else{
					//alert('Deshabilita por que es diferente' + vlSelects[y]);
					vlDestinos[y].disabled = true;
				}
			}
		}
	}
	
	
	/*****************************************************     COOKIES   *************************************************************************/
	
	
function setCookie(cname,cvalue,exdays){
	var d = new Date();
	d.setTime(d.getTime()+(exdays*24*60*60*1000));
	var expires = "expires="+d.toGMTString();
	alert(expires);
	document.cookie = cname + "=" + cvalue + "; " + expires + ';path=/';;
}

function getCookie(cname){
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++){
	  var c = ca[i].trim();
	  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
	}
	return "";
}
	
$.fn.copyEvents = function( to, filter )
{
    var to = to.jquery ? to : jQuery(to);
    var filter = filter ? filter.split(" ") : null;
    var events = this[0].events || jQuery.data(this[0], "events") || jQuery._data(this[0], "events");

    return this.each(function()
    {
        if (!to.length || !events) {
            return;
        }

        $.each(events, function(eventType, eventArray) {
            $.each(eventArray, function(index, event) {
                var eventToBind = event.namespace.length > 0
                    ? (event.type + '.' + event.namespace)
                    : (event.type);
                if (filter && $.inArray(eventToBind, filter) == -1) {
                    return true;
                }
                to.bind(eventToBind, event.data, event.handler);
            });
        });
    });
}	


	
	