function generaMatriz(data) {
			
	//var json_obj = $.parseJSON(data);//parse JSON
	//var datosPB = json_obj['datosPB'];
	//var datosSerie = json_obj['datosSerie'];
	var datosPB = data['datosPB'];
	var datosSerie = data['datosSerie'];
	var contenido = '<table id="tcm"><tr><td>';
	var htmlSerie = '';

	$.each(datosSerie, function() {

		var h_ini = this['VHI']; 
		var h_fin = this['VHF']; 
		var h_int = this['VHT']; 
		var v_ini = this['VVI']; 
		var v_fin = this['VVF']; 
		var v_int = this['VVT']; 

		/*console.log('h_ini: ' + h_ini); 
		console.log('h_fin: ' + h_fin); 
		console.log('h_int: ' + h_int); 
		console.log('v_ini: ' + v_ini); 
		console.log('v_fin: ' + v_fin); 
		console.log('v_int: ' + v_int); */

		var vlCountRow 		= 0;  
		var vlRemplazoRow	= ''; 
		var vlEncabezado	= '<th style="text-align:center"></th>'; 
		var vlCountCol		= 0;
		var v_ini_tmp 		= v_ini;
		var h_ini_tmp 		= h_ini;
		var vlTmpTD			= '';
		var horVal			= '';

		while(v_ini_tmp <= v_fin){
			//console.log('loop vertical'); 
			vlRemplazoRow 	+= ' <tr><th style="text-align:center">' + v_ini_tmp + '</th>\n';
			vlCountRow  = vlCountRow + 1;
			vlCountCol  = 0;

			h_ini_tmp = h_ini;

			while(h_ini_tmp <= h_fin){
				//console.log('loop horizontal'); 

				vlCountCol  += 1;

				if(vlCountRow == 1){
					vlEncabezado += '<th style="text-align:center">' + h_ini_tmp + '</th>';
				}

				vlTmpTD = '\n<td><input type="text" class="cnt" size="' + datosPB[0].S + 
					'" id="' + datosPB[0].P + '_' + this['I'] + '_' + h_ini_tmp.toString().replace(/\./g, 'd') + '_' +
					v_ini_tmp.toString().replace(/\./g, 'd') + '_' + vlCountCol + '"/></td>';

				h_ini_tmp = h_ini_tmp + h_int;
				vlRemplazoRow += vlTmpTD; 
			}

			v_ini_tmp = v_ini_tmp + v_int; 
			vlRemplazoRow += '\n</tr>'; 

		}

		htmlSerie = '<table class="msg"><tr><td><div class="alertMsg" style="display:none"></div></td></tr></table>\n' + 
					'<table><tr><td colspan="2" align="center" bgcolor="#B5284D"><font color="white">' + this['N'] + '</font></td></tr>\n' +
					'<tr><td class="va" align="right" bgcolor="#E6E6FA" nowrap>' + (datosPB[0].MV?datosPB[0].MV:'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;') + '</td>\n' + 
					'<td>\n' +  
					'    <table class="data">\n' + 
					'    <tr><td colspan="' + (vlCountCol + 1) + '" align="center" bgcolor="#E6E6FA">' + (datosPB[0].MH?datosPB[0].MH:'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;') + '</td></tr>\n' + 
					vlEncabezado + vlRemplazoRow + '\n</table></td></tr></table>\n</br>\n';

		contenido += htmlSerie; 

	});

	contenido += '<table class="msg"><tr><td><div class="alertMsg" style="display:none"></div></td></tr></table></td></tr></table>\n' +
		'<button type="button" class="btn btn-info" onClick="Export(\'tcm\',\'' + datosPB[0].B + '\',\'' + datosPB[0].T + '\');"><i class="icon-download bigger-125"></i>Exporta Excel</button>' +
		'<input type="hidden" id="ctl_bgc" value="' + datosPB[0].B + '"/><input type="hidden" id="ctl_tc" value="' + datosPB[0].T + '"/>';

	//console.log(contenido);

	return contenido;	
}