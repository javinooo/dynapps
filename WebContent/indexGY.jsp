<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Entrar a Gayosso</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<link rel="shortcut icon" href="usr/vst/image/favicon.png">
		<link rel="stylesheet" href="usr/vst/css/style.css">
		<script src="usr/vst/js/script.js"></script>
		<script language="javascript">
			function setFocus()
			{
				document.getElementById("login").focus();
			}
		</script>
	</head>
	<body class="sky">
		<div id="loginContainer">
			<div id="loginContent">
				<div id="loginBox">
					<img id="logo" src="usr/vst/image/logo-gayosso.png">
					<!-- <h2>Ingresar a Gayosso</h2> -->
					<form action="DynAppsSrv" method="post">
						<% if(request.getAttribute("MSG_ERROR")!=null){ %>
							<label><font color="red"><%=request.getAttribute("MSG_ERROR")%></font></label>
						<% }%>
						<label>Usuario</label>
						<input type="text"  name="login" id="login" value="<%=(request.getParameter("login")!=null?request.getParameter("login"):"")%>" placeholder="Escriba su nombre de usuario">
						<label>Contrase&ntilde;a</label>
						<input type="password" name="password" required placeholder="Escriba su contrase&ntilde;a">
						<!--a class="passwordRecovery" href="javascript:void(0)">Recuperar contrase&ntilde;a</a-->
						<input type="submit" value="Ingresar a Gayosso">
						<input type="hidden" name="PARAM_CTL_PANTALLA" value="0">
						<input type="hidden" name="PARAM_CTL_ID_TABLA" value="0">
						<input type="hidden" name="PARAM_CTL_OPERACION" value="C">
						<input type="hidden" name="PARAM_CTL_CVE_NAVEGACION" value="NAVEGA_CAT_CTL_MENU">
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
