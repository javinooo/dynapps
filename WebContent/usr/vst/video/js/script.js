function profileMenu() {
	var menu = document.getElementById("menuProfile");
	menu.style.display = (menu.style.display == 'block') ? 'none' : 'block';
}
function profileMenuClose() {
	var menu = document.getElementById("menuProfile");
	menu.style.display = 'none';
}
function desplegar(cual, cuantosItem) {
	var submenu = document.getElementsByClassName("submenu");
	var item = submenu[0].getElementsByTagName("LI")[0];
	alto = item.offsetHeight * cuantosItem;
	submenu[cual].style.height = (submenu[cual].style.height == alto + 'px') ? 0 : alto + 'px';
	// submenu[cual].style.borderBottom = (submenu[cual].style.borderBottom == 'thin solid #DDD') ? 'none' : 'thin solid #DDD';
	submenu[cual].style.transition = '0.3s';
}
function LeftMenuResponsive() {
	var menuResponsive = document.getElementById("menuResponsive");
	menuResponsive.style.left = (menuResponsive.style.left == "0em") ? "-19em" : "0em";
	// menuResponsive.style.transition = "0.3s";
	// menuResponsive.style.webkitTransition = "0.3s";
	// menuResponsive.style.mozTransition = "0.3s";
	// menuResponsive.style.oTransition = "0.3s";
	// menuResponsive.style.msTransition = "0.3s";
}
function headerResponsive() {
	var header = document.getElementById("header");
	var positionScroll = document.body.scrollTop;
	var windowWidth = document.body.offsetWidth;
	if(windowWidth < 640) {
		if(positionScroll > 65) {
			header.style.width = "100%";
			header.style.position = "fixed";
			header.style.zIndex = 10;
			header.style.top = 0;
			header.style.boxShadow = "0 0.2em 0.5em rgba(0,0,0,0.2)";
		} else {
			header.style.position = "static";
		}
	} else {
		header.style.position = "static";
	}
}