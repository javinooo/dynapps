$(document).ready(function() {

  function inputAtts()
  {
    $(".cnt").each(function() {
		var vname = $(this).attr('id');
		var vTooltip = 'H: ' + vname.split("|")[2] + ' - V: ' + vname.split("|")[3];
		$(this).attr( 'title', vTooltip );
		if($(this).val()){$(this).css({"background-color": "#FF6600"});$(this).css({"text-align": "center"});$(this).css({"color": "#00008A"});
		}else{$(this).css({"background-color": "white"});$(this).css({"text-align": "center"});}
		if ($('#ALT_MOD_AJU_SUM').length == 0) {	
		  $(this).prop('disabled', true);
		}
	});
	
	
	
$('input.cnt').focus(function() {
        input = $(this);
        if ($(this).val() != null) {
            cantidadAnterior = $(this).val();
        }
    }).change(function() {

  if($(this).val() || (cantidadAnterior != null) ){
	var $Elem = $(this);
    var valor = $Elem.val();
	var lastVal = cantidadAnterior;
	if(!$(this).val()){
		  valor = '0';
	  }
	var $vId   = $Elem.attr('id');
    console.log('tiene un valor ' + valor + ' valor anterior: ' + cantidadAnterior);

	if(!$("#F_MOVIMIENTO").val()){
		$Elem.val(cantidadAnterior);
		if($Elem.val()){$Elem.css({"background-color": "#FF6600"});$Elem.css({"text-align": "center"});$Elem.css({"color": "#00008A"});
		}else{$Elem.css({"background-color": "white"});$Elem.css({"text-align": "center"});}
		$( ".alertMsg" ).each(function() {
			$(this).addClass( "alert alert-danger" );
			$(this).text('Debe capturar una fecha de ajuste');
			$(this).css( "display", "block" ).fadeOut( 20000 );
		});
		return;
	}

	//console.log('tiene un estatus ' + $("#CVE_ESTATUS").val());
	if($("#PRECIO").length && !$("#PRECIO").val()){
		$( ".alertMsg" ).each(function() {
			$(this).addClass( "alert alert-danger" );
			$(this).text( "Capture el precio del producto" );
			$(this).css( "display", "block" ).fadeOut( 5000 );
		});
		$Elem.val(cantidadAnterior);
		if($Elem.val()){$Elem.css({"background-color": "#FF6600"});$Elem.css({"text-align": "center"});$Elem.css({"color": "#00008A"});
		}else{$Elem.css({"background-color": "white"});$Elem.css({"text-align": "center"});}
		return;
	}
	
    if($.isNumeric(valor)){

	
	if ( !$( "#ID_MOVIMIENTO" ).length ) {
		console.log('ya existe ');
	}
	
var $divRespuesta     	= $("#RESPUESTA");
var $ID_MOVIMIENTO		= $("#ID_MOVIMIENTO").val();
console.log('ID_MOVIMIENTO valor ' + $ID_MOVIMIENTO);
var $ID_PROVEEDOR   	= $("#ID_PROVEEDOR").val();
var $ID_EMPRESA			= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
var $ID_CLIENTE    		= $("#ID_CLIENTE").val();
var $ID_ALMACEN        	= $("#ID_ALMACEN").val();
var $TX_REFERENCIA      = $("#TX_REFERENCIA").val();
var $TIPO      			= 'ENTRADA';
var $SIT_MOVIMIENTO     = $("#SIT_MOVIMIENTO").val();
var $CTL_TIPO_OPER		= $('[name="PARAM_CTL_OPERACION"]').val();
var $B_SINCRONIZA     	= $("#PARAM_CTL_B_SINCRONIZA").val();
var $ID_PRODUCTO_BASE  	= $vId.split("|")[0];
var $ID_SERIE  			= $vId.split("|")[1];
var $ID_VALOR_HORIZ  	= $vId.split("|")[2];
var $ID_VALOR_VERT  	= $vId.split("|")[3];


		console.log('antes de la llamada ');
		//var datastring = $("#formValidate").serialize();
		$.ajax({
            type: "POST",
            url: "OpticsSrv",
			async: false,
            data: {CTL_CVE_OPERACION:"GENERA_PRE_MOVIMIENTO",ID_EMPRESA:$ID_EMPRESA,CVE_PRODUCTO:$vId, TIPO_MOVIMIENTO:'AJUSTE_SUMA', 
					CANTIDAD:valor,ID_MOVIMIENTO:$ID_MOVIMIENTO, PARAM_CTL_B_SINCRONIZA:$B_SINCRONIZA,PARAM_CTL_OPERACION:$CTL_TIPO_OPER},
            dataType: "json",
            success: function(data) {

					console.log('id movto: >' + data[0] + '<');
					console.log('MSG ERROR: >' + data[1] + '<');
					if(data[1]){
						$Elem.val(lastVal);
						$( ".alertMsg" ).each(function() {
							$(this).text( '');
							$(this).addClass( "alert alert-danger" );
							$(this).text( data[1] );
							$(this).css( "display", "block" ).fadeOut( 20000 );
						});
						console.log('Mensaje de error');
					}else{
						console.log('data[0]: ' + data[0]); 
						$("#ID_MOVIMIENTO").val(data[0]); 
						$("#PARAM_CTL_B_SINCRONIZA").val('F');
						if(valor=='0'){
							$Elem.val('');
						}
						if($Elem.val()){$Elem.css({"background-color": "#FF6600"});$Elem.css({"text-align": "center"});$Elem.css({"color": "#00008A"});
						}else{$Elem.css({"background-color": "white"});$Elem.css({"text-align": "center"});}
						
						console.log('success: ' + data[0]);
						console.log('success cantidadAnterior: ' + lastVal);
					}
            },
            error: function(data){
					console.log('Error cantidadAnterior: ' + lastVal);
					$Elem.val(lastVal);
					console.log('error handing here'); 
					$( ".alertMsg" ).each(function() {
						$(this).addClass( "alert alert-danger" );
						$(this).text( data );
						$(this).css( "display", "block" ).fadeOut( 20000 );
					});
            }
        });

		
		console.log('despues de la llamada ');
		
    }else{
       console.log('El valor no es numérico');
	   $Elem.val(cantidadAnterior);
	   if($Elem.val()){$Elem.css({"background-color": "#FF6600"});$Elem.css({"text-align": "center"});$Elem.css({"color": "#00008A"});
		}else{$Elem.css({"background-color": "white"});$Elem.css({"text-align": "center"});}
		$( ".alertMsg" ).each(function() {
			$(this).addClass( "alert alert-danger" );
			$(this).text( "Solo valores numéricos enteros están permitidos" );
			$(this).css( "display", "block" ).fadeOut( 20000 );
		});
    }

  }

});
	
	
	
  } // termina el inputAtts







  
  
  











  

	//alert('hola 2');
	console.log('preuba de consola otra vez');
	$('#ID_PRODUCTO_BASE').change(function() {
	
		if($(this).val()){
	
			// Limpia el valor del ID_TRANSAC_TMP
			$("#CTL_OPTICS_TMP_TRAN_ID").val('');
			
			console.log('preuba de consola 2 otra vez');
			var valorSelectNuevo = $(this).val();
			console.log('obtiene el valor nuevo del select: ' + valorSelectNuevo);
			var IdEmpresa = $('[name="PARAM_CTL_ID_EMPRESA"]').val();
			console.log('obtiene el valor del ID: ' + IdEmpresa);
			var $select = $(this);
			console.log('Asigna el valor del ID: ');
			$.ajax({
				type: "POST",
				url: "OpticsSrv",
				data: {
					ID_EMPRESA: IdEmpresa,
					CTL_CVE_OPERACION: "ACTUALIZA_SERIES",
					ID_PRODUCTO_BASE: valorSelectNuevo,
					ACCION: "getSeries"
				},
				dataType: "json",
				success: function(data) {
					//console.log('DATOS' + data);
					var divCantidad = $("#DynTCANTIDAD");
					//console.log('OBTIENE EL OBJETO DIV');
					divCantidad.html( data);
					//console.log('ASIGNA VALOR AL DIV ');
					inputAtts();
					//console.log('DO SOMETHING');
					
				},
				error: function() {
					console.log("Hay error grave");
				}
			});
		}
    }); //   termina change select 


	inputAtts();
	

  function AltaModCompra()
  {
	var $ID_EMPRESA			= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $F_MOVIMIENTO		= $("#F_MOVIMIENTO").val();
	var $ID_MOVIMIENTO		= $("#ID_MOVIMIENTO").val();
	var $ID_PROVEEDOR   	= $("#ID_PROVEEDOR").val();
	var $ID_ALMACEN        	= $("#ID_ALMACEN").val();
	var $TX_REFERENCIA      = $("#TX_REFERENCIA").val();
	var $TIPO      			= 'AJUSTE_SUMA';
	var $SIT_MOVIMIENTO     = $("#SIT_MOVIMIENTO").val();
	var $ID_PRODUCTO_BASE  	= $("#ID_PRODUCTO_BASE").val();
	var $CTL_TIPO_OPER		= $('[name="PARAM_CTL_OPERACION"]').val();
	//var $CTL_OPTICS_TMP_TRAN_ID	$("#CTL_OPTICS_TMP_TRAN_ID").val();
	
	if($("#ID_MOVIMIENTO").val() == ''){
		console.log('Aún no hay id_movimiento ');
		$( ".alertMsg" ).each(function() { 
			$(this).addClass( "alert alert-danger" );
			$(this).text('Debe capturar algún valor');
			$(this).css( "display", "block" ).fadeOut( 10000 );
		});
		return;
	};
	
	var tieneValor 			= '0';
	
	$(".cnt").each(function() {
		if($(this).val()){
		   tieneValor = '1';
		};
	});
	
	if(tieneValor == '0'){
		console.log('No hay elementos con valor');
		$( ".alertMsg" ).each(function() { 
			$(this).addClass( "alert alert-danger" );
			$(this).text('Debe capturar un valor 2');
			$(this).css( "display", "block" ).fadeOut( 10000 );
		});
		return;
	}
	
		$.ajax({
            type: "POST",
            url: "OpticsSrv",
            data: {	CTL_CVE_OPERACION:"ALTA_MOD_AJU_SUMA",ID_EMPRESA:$ID_EMPRESA,ID_MOVIMIENTO:$ID_MOVIMIENTO, 
					ID_PROVEEDOR:$ID_PROVEEDOR, F_MOVIMIENTO:$F_MOVIMIENTO,
					ID_ALMACEN:$ID_ALMACEN,TX_REFERENCIA:$TX_REFERENCIA,TIPO:$TIPO,SIT_MOVIMIENTO:$SIT_MOVIMIENTO, //CTL_OPTICS_TMP_TRAN_ID:$CTL_OPTICS_TMP_TRAN_ID,
					ID_PRODUCTO_BASE:$ID_PRODUCTO_BASE,PARAM_CTL_OPERACION:$CTL_TIPO_OPER},
            dataType: "json",
            success: function(data) {
                //var obj = jQuery.parseJSON(data); if the dataType is not specified as json uncomment this
                // do what ever you want with the server response
				
				if(data[1]){
					$( ".alertMsg" ).each(function() {
						$(this).text( '');
						$(this).addClass( "alert alert-danger" );
						$(this).text( data[0] );
						$(this).css( "display", "block" ).fadeOut( 10000 );
					});
					console.log('Mensaje de error');
				}else{
					
					$("#PARAM_CTL_B_SINCRONIZA").val('V');
					$('[name="PARAM_CTL_OPERACION"]').val('M');
					$("#dSIT_MOVIMIENTO" ).show();
					$( "#ID_PRODUCTO_BASE" ).prop( "disabled", true );
					
					console.log('success: ' + data);
					$( ".alertMsg" ).each(function() { 
						$(this).addClass( "alert alert-danger" );
						$(this).text( data[0]);
						$(this).css( "display", "block" ).fadeOut( 10000 );
					});
				}
            },
            error: function(data){
					console.log('Error en alta modificación: ' );
					$( ".alertMsg" ).each(function() {
						$(this).addClass( "alert alert-danger" );
						$(this).text( "Error en la operación: " + data[0]);
						$(this).css( "display", "block" ).fadeOut( 10000 );
					});
					console.log('error handing here'); 
            }
        });
  }


 $('#ALT_MOD_AJU_SUM').click(function() {  
  AltaModCompra();   
 });

if ( !$( "#ID_MOVIMIENTO" ).length ) {
		console.log('NO EXISTE ID_MOVIMIENTO Y LO CREA ');
		/*$('<input>').attr({
			type: 'hidden',
			id: 'ID_MOVIMIENTO',
			name: 'ID_MOVIMIENTO'
		}).appendTo('formValidate');*/
		$('#formValidate').append('<input type="hidden" name="ID_MOVIMIENTO" id="ID_MOVIMIENTO" />');
		$("#dSIT_MOVIMIENTO" ).hide();
		console.log('ya lo creo ');
	}

	
/************************************************************************************************************************/
window.addEventListener('paste', function (event) {
	
	var $input = $(':focus');
	var vId = $input.attr('id');
	//console.log('este es el id: ' + $input.attr('id'));
	var idColIni = vId.split("|")[4];
	var idColAct = vId.split("|")[4];
	//console.log('>> Inicial ' + idColIni);
	var vTr = $input.closest('tr');
	var vTrId = $input.closest('tr').attr('id');
	//console.log('>> esta es el renglon ' + vTrId);
    //var clipText = window.clipboardData.getData('Text');
	var clipText = event.clipboardData.getData('text');
	
    // split into rows

    clipRows = clipText.split(String.fromCharCode(13));
	//console.log('>> clipRows.length ' + clipRows.length);
    // split rows into columns

    for (i=0; i<clipRows.length; i++) {
    	clipRows[i] = clipRows[i].split(String.fromCharCode(9));
		//console.log('>> dentro');
    }

    for (i=0; i<clipRows.length - 1; i++) {
		//console.log('>> row: ' + i);
		if(i>0){
			vTr = vTr.closest('tr').next('tr');
			$input = vTr.find("td:first input");
			//console.log('>> esta es el proximo renglon: ' + vTr.attr('id'));
			for (x=0; x<idColIni -1; x++) {
				$input = $input.closest('td').next().find('input'); 
			}
		}
    	for (j=0; j<clipRows[i].length; j++) {
			
			//console.log('>> val: ' + clipRows[i][j]);
			$input.focus();
			$input.val(clipRows[i][j]);
			$input.trigger( "change" );
			//console.log('>> input1: ' + $input.val());
			$input = $input.closest('td').next().find('input'); 
			//console.log('>> Actualiza');
    	}
    }

	event.preventDefault();
});	

/************************************************************************************************************************/

	
	
}); 

