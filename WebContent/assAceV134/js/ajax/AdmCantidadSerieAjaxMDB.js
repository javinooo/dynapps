$(document).ready(function() {

	var cveOperacion = $('[name="PARAM_CTL_OPERACION"]').val();
	console.log('CveOperacion = ' + cveOperacion); 
	var $ID_APLICACION 		= $('#dyn_ctl_id_app').val(); 
	console.log('$ID_APLICACION = ' + $ID_APLICACION); 
	var BgColorI = '';
	var TxColorI = '';
	
  function inputAtts(vPermiso){
	
    $(".cnt").each(function() {
		var vname = $(this).attr('id');
		vname = vname.replace(/d/g, '.');
		var horiz = vname.split("_")[2]; 
		var vert  = vname.split("_")[3]; 
		var vTooltip = 'H: ' + horiz + ' - V: ' + vert;
		$(this).attr( 'title', vTooltip );
		if($(this).val()){$(this).css({"background-color": BgColorI, "color": TxColorI, "text-align": "center"});
		}else{$(this).css({"background-color": "white", "text-align": "center", "color": "black"});}
		if (vPermiso=='F') {	
		  $(this).prop('disabled', true);
		}
	});
	
	$('input.cnt').focus(function() {
        input = $(this);
        if ($(this).val() != null) {
            cantidadAnterior = $(this).val();
        }
    }).change(function() {

  if($(this).val() || (cantidadAnterior != null) ){
	var $Elem = $(this);
    var valor = $Elem.val();
	var lastVal = cantidadAnterior;
	if(!$(this).val()){
		  valor = 0;
	  }
	var $vId   = $Elem.attr('id');
	//$vId = $vId.replace(/d/g, '.');
    console.log('tiene un valor ' + valor + ' valor anterior: ' + cantidadAnterior);

	if($.isNumeric(valor)&&valor>=0){

		console.log('Valor numerico: ' + valor);
	
var $ID_MOVIMIENTO		= $("#ID_MOVIMIENTO").val();
console.log('ID_MOVIMIENTO valor ' + $ID_MOVIMIENTO);
var $CVE_OPERACION		= valor>0?'ADM_MATRIX_DATA':'DEL_MATRIX_DATA';



		console.log('antes de la llamada ');
		//var datastring = $("#formValidate").serialize(); $.ajaxSetup({async: false});
		$.ajax({
            type: "GET",
            url: "DynAppsSrv",
            data: {	DYN_CTL_CVE_OPERACION:"DYN_OPERATION",
					DYN_CTL_OPERATION:$CVE_OPERACION,
					ID_MOVIMIENTO:$ID_MOVIMIENTO,
					ID_PRODUCTO_BASE_SERIE:$vId, 
					CANTIDAD:valor},
            dataType: "json",
            success: function(data) {
				var vResultado = data['result'][0].A; 
				var vError = data['result'][0].E; 
				
				if(vError){
					$('#idResult').html('<font color="red"><b>' + vError + '</b></font>');
					$('#msgResult').modal('show');
				}else{
					$('[name="PARAM_CTL_OPERACION"]').val('M');
					$("#ID_PRODUCTO_BASE").off('change');
					$("#dID_PRODUCTO_BASE" ).find('div.chosen-drop').remove();
					if(valor=='0'){
						$Elem.val('');
					}
					if($Elem.val()){$Elem.css({"background-color": BgColorI, "text-align": "center", "color": TxColorI});
					}else{$Elem.css({"background-color": "white", "text-align": "center", "color": "black"});}
				}	
            },
            error: function(data){
				if(xhr.status=403){
					$('#idResult').html('<font color="red"><b>Su sesi&oacute;n ha caducado</b></font>');
					$('#msgResult').modal('show');
					//window.location.assign("index.jsp");
				}else{
					var vError = data['result'][0].E; 
					$('#idResult').html('<font color="red"><b>' + vError + '</b></font>');
					$('#msgResult').modal('show');
				}
            }
        });

		//console.log('despues de la llamada ');

    }else{
       //console.log('El valor no es numérico');
	   $Elem.val(cantidadAnterior);

	   if($Elem.val()){$Elem.css({"background-color": BgColorI, "text-align": "center", "color": TxColorI});
		}else{$Elem.css({"background-color": "white", "text-align": "center", "color": "black"});}

		$('#idResult').html('<font color="red"><b>Solo valores numéricos enteros están permitidos</b></font>');
		$('#msgResult').modal('show');
    }
  }
});
} // termina el inputAtts

	console.log('cveOperacion: ' + cveOperacion);
	if(cveOperacion=='A'){

		console.log('preuba de consola otra vez');
		$('#ID_PRODUCTO_BASE').change(function() {
		
			if($(this).val()){

				var $ID_MOVIMIENTO		= $("#ID_MOVIMIENTO").val();
				
				// Limpia el valor del ID_TRANSAC_TMP
				$("#CTL_OPTICS_TMP_TRAN_ID").val('');
				
				//console.log('preuba de consola ******* prueba de cambios ');
				var valorSelectNuevo = $(this).val();
				//console.log('obtiene el valor nuevo del select: ' + valorSelectNuevo);
				var $select = $(this);
				//console.log('Asigna el valor del ID: ');
				$.ajax({
					type: "GET",
					url: "DynAppsSrv",
					data: {
						DYN_CTL_CVE_OPERACION:"DYN_OPERATION",
						DYN_CTL_OPERATION: "GET_MATRIX_DATA",
						ID_PRODUCTO_BASE: valorSelectNuevo, 
						ID_MOVIMIENTO: $ID_MOVIMIENTO, 
						ID_TABLA:372, 
						ID_APLICACION: $ID_APLICACION, 
						CVE_OPERACION: 'A'
					},
					dataType: "json", 
					success: function(data) {
						var contenido = generaMatriz(data);
						var divCantidad = $("#DynTCANTIDAD");
						divCantidad.html(contenido);
						var vPermiso	= data['datosPermisos'][0].I;
						BgColorI = $("#ctl_bgc").val();
						TxColorI = $("#ctl_tc").val();
						inputAtts(vPermiso);
					},
					error: function() {
						console.log("Hay error grave");
					}
				});
			}else{
				var divCantidad = $("#DynTCANTIDAD");
				divCantidad.html( '');
			}
		}); 
	}else{
		var IdProductobase = $('#ID_PRODUCTO_BASE').val();
		var IdMovimiento = $('#ID_MOVIMIENTO').val();
		$.ajax({
			type: "GET",
			url: "DynAppsSrv",
			data: {
				DYN_CTL_CVE_OPERACION:"DYN_OPERATION",
				DYN_CTL_OPERATION:"GET_MATRIX_DATA",
				ID_PRODUCTO_BASE: IdProductobase, 
				ID_MOVIMIENTO:IdMovimiento, 
				ID_TABLA:372, 
				ID_APLICACION: $ID_APLICACION, 
				CVE_OPERACION: 'A'
			},
			dataType: "json", 
			success: function(data) {
				var contenido = generaMatriz(data);
				var divCantidad = $("#DynTCANTIDAD");
				divCantidad.html(contenido);
				//var json_obj = $.parseJSON(data);
				var datosMovto = data['datosResultado'];
				var vPermiso	= data['datosPermisos'][0].U;
				console.log("Antes de each");
				$.each(datosMovto, function() {
					console.log("dentro de each");
					$("#"+this['I']).val(this['V']);
				});
				console.log("después de each");
				BgColorI = $("#ctl_bgc").val();
				TxColorI = $("#ctl_tc").val();
				inputAtts(vPermiso);
			},
			error: function() {
				console.log("Hay error grave");
			}
		});
	}

	//inputAtts();
	
  function AltaModCompra()
  {
	var $ID_MOVIMIENTO		= $("#ID_MOVIMIENTO").val();
	var valorId				= $("#ID_MOVIMIENTO").val();
	var $ID_NOTA        	= $("#ID_NOTA").val();
	var $TX_REFERENCIA      = $("#TX_REFERENCIA").val();
	var $SIT_MOVIMIENTO     = $("#SIT_MOVIMIENTO").val();
	var $ID_PRODUCTO_BASE  	= $("#ID_PRODUCTO_BASE").val();
	var $CTL_TIPO_OPER		= $('[name="PARAM_CTL_OPERACION"]').val();

	
	
	var tieneValor 			= '0';
	
	$(".cnt").each(function() {
		if($(this).val()){
		   tieneValor = '1';
		};
	});
	
	console.log('tieneValor: ' + tieneValor);
	
	if(tieneValor == '0'){
		console.log('No hay elementos con valor');
		$('#idResult').html('<font color="red"><b>Debe capturar al menos un valor</b></font>');
		$('#msgResult').modal('show');
		return;
	}
	
		$.ajax({
            type: "GET",
            url: "DynAppsSrv",
            data: {	
					DYN_CTL_CVE_OPERACION:"DYN_OPERATION",
					DYN_CTL_OPERATION:"ALTA_MOD_COMPRA_PB",
					ID_NOTA:$ID_NOTA,
					ID_MOVIMIENTO:$ID_MOVIMIENTO, 
					TX_REFERENCIA:$TX_REFERENCIA,
					SIT_MOVIMIENTO:$SIT_MOVIMIENTO, 
					ID_PRODUCTO_BASE:$ID_PRODUCTO_BASE},
            dataType: "json",
            success: function(data) {
				var vResultado = data['result'][0].A; 
				var vError = data['result'][0].E; 
				
				$('#ID_BACK').trigger('click');
				
				/*if(vError){
					$('#idResult').html('<font color="red"><b>' + vError + '</b></font>');
					$('#msgResult').modal('show');
				}else{
					$('[name="PARAM_CTL_OPERACION"]').val('M');
					$("#dSIT_MOVIMIENTO" ).show();
					$( "#ID_PRODUCTO_BASE" ).prop( "disabled", true );
					$('#idResult').html('<font color="green"><b>' + vResultado + '</b></font>');
					$('#msgResult').modal('show');
				}*/
            },
            error: function(data){
				if(xhr.status=403){
					$('#idResult').html('<font color="red"><b>Su sesi&oacute;n ha caducado</b></font>');
					$('#msgResult').modal('show');
					//window.location.assign("index.jsp");
				}else{
					var vError = data['result'][0].E; 
					$('#idResult').html('<font color="red"><b>' + vError + ' v</b></font>');
					$('#msgResult').modal('show');
				}
            }
        });
  }

 $('#ALT_MOD_CPA').click(function() {  
  AltaModCompra();   
 });

}); 

