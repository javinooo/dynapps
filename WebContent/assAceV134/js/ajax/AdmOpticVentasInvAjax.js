$(document).ready(function() {

var cveOperacion = $('[name="PARAM_CTL_OPERACION"]').val();
console.log('CveOperacion = ' + cveOperacion); 

  function inputAtts()
  {
	var BgColor = $("#ctl_bgc").val();
	var TxColor = $("#ctl_tc").val();
	
    $(".cnt").each(function() {
		var vname = $(this).attr('id');
		vname = vname.replace(/d/g, '.');
		var horiz = vname.split("_")[2]; 
		var vert  = vname.split("_")[3]; 
		var vTooltip = 'H: ' + horiz + ' - V: ' + vert;
		$(this).attr( 'title', vTooltip );
		if($(this).val()){$(this).css({"background-color": BgColor, "color": TxColor, "text-align": "center"});
		}else{$(this).css({"background-color": "white", "text-align": "center"});}
		if ($('#ALT_MOD_VTA').length == 0) {	
		  $(this).prop('disabled', true);
		}
	});
	
	
$('input.cnt').focus(function() {
        input = $(this);
        if ($(this).val() != null) {
            cantidadAnterior = $(this).val();
        }
    }).change(function() {

  if($(this).val() || (cantidadAnterior != null) ){
	var $Elem = $(this);
    var valor = $Elem.val();
	var lastVal = cantidadAnterior;
	if(!$(this).val()){
		  valor = '0';
	  }
	var $vId   = $Elem.attr('id');
	$vId = $vId.replace(/d/g, '.');
    console.log('tiene un valor ' + valor + ' valor anterior: ' + cantidadAnterior);

    if($.isNumeric(valor)){


	
	
	if ( !$( "#ID_MOVIMIENTO" ).length ) {
		console.log('ya existe ');
	}
	
var $divRespuesta     	= $("#RESPUESTA");
var $ID_NOTA        	= $("#ID_NOTA").val();
var $ID_MOVIMIENTO		= $("#ID_MOVIMIENTO").val();
console.log('ID_MOVIMIENTO valor ' + $ID_MOVIMIENTO);
var $ID_EMPRESA			= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
var $ID_CLIENTE    		= $("#ID_CLIENTE").val();
var $ID_ALMACEN        	= $("#ID_ALMACEN").val();
var $TX_REFERENCIA      = $("#TX_REFERENCIA").val();
var $SIT_MOVIMIENTO     = $("#SIT_MOVIMIENTO").val();
var $CTL_TIPO_OPER		= $('[name="PARAM_CTL_OPERACION"]').val();
var $B_SINCRONIZA     	= $("#PARAM_CTL_B_SINCRONIZA").val();

		console.log('antes de la llamada ');
		//var datastring = $("#formValidate").serialize();
		$.ajax({
            type: "POST",
            url: "OpticsSrv",
			async: false,
            data: {CTL_CVE_OPERACION:"GENERA_PRE_MOVIMIENTO",ID_EMPRESA:$ID_EMPRESA,CVE_PRODUCTO:$vId, ID_ALMACEN:$ID_ALMACEN,ID_NOTA:$ID_NOTA,
					TIPO_MOVIMIENTO:'SALIDA',
					CANTIDAD:valor,ID_MOVIMIENTO:$ID_MOVIMIENTO, PARAM_CTL_B_SINCRONIZA:$B_SINCRONIZA,PARAM_CTL_OPERACION:$CTL_TIPO_OPER},
            dataType: "json",
            success: function(data) {

					console.log('id movto: >' + data[0] + '<');
					console.log('MSG ERROR: >' + data[1] + '<');
					if(data[1]){
						$Elem.val(lastVal);
						$( ".alertMsg" ).each(function() {
							$(this).text( '');
							$(this).addClass( "alert alert-danger" );
							$(this).text( data[1] );
							$(this).css( "display", "block" ).fadeOut( 10000 );
						});
						console.log('Mensaje de error');
					}else{
						console.log('data[0]: ' + data[0]); 
						$("#ID_MOVIMIENTO").val(data[0]); 
						$("#PARAM_CTL_B_SINCRONIZA").val('F');
						$("#ID_PRODUCTO_BASE").off('change');
						$("#dID_PRODUCTO_BASE" ).find('div.chosen-drop').remove();
						if(valor=='0'){
							$Elem.val('');
						}
						if($Elem.val()){$Elem.css({"background-color": "#FF6600"});$Elem.css({"text-align": "center"});$Elem.css({"color": "#00008A"});
						}else{$Elem.css({"background-color": "white"});$Elem.css({"text-align": "center"});}
						
						console.log('success: ' + data[0]);
						console.log('success cantidadAnterior: ' + lastVal);
					}
            },
            error: function(data){
					console.log('Error cantidadAnterior: ' + lastVal);
					$Elem.val(lastVal);
					console.log('error handing here'); 
					$( ".alertMsg" ).each(function() {
						$(this).addClass( "alert alert-danger" );
						$(this).text( data );
						$(this).css( "display", "block" ).fadeOut( 10000 );
					});
            }
        });

		
		console.log('despues de la llamada ');
		
    }else{
       console.log('El valor no es numérico');
	   $Elem.val(cantidadAnterior);
	   if($Elem.val()){$Elem.css({"background-color": "#FF6600"});$Elem.css({"text-align": "center"});$Elem.css({"color": "#00008A"});
		}else{$Elem.css({"background-color": "white"});$Elem.css({"text-align": "center"});}
		$( ".alertMsg" ).each(function() {
			$(this).addClass( "alert alert-danger" );
			$(this).text( "Solo valores numéricos enteros están permitidos" );
			$(this).css( "display", "block" ).fadeOut( 10000 );
		});
    }

  }

});



  } // termina el inputAtts


	function actualizaSeries(){
		var IdEmpresa 			= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
		var $ID_ALMACEN        	= $("#ID_ALMACEN").val();
		var $ID_PRODUCTO_BASE  	= $("#ID_PRODUCTO_BASE").val();
		var $ID_MOVIMIENTO  	= $("#ID_MOVIMIENTO").val();
		if(!$ID_MOVIMIENTO){
			$ID_MOVIMIENTO = '0';
		}
		var divCantidad 		= $("#DynTCANTIDAD");

		if($("#ID_ALMACEN").val() && $("#ID_PRODUCTO_BASE").val()){
				$.ajax({
					type: "POST",
					url: "OpticsSrv",
					data: {
						CTL_CVE_OPERACION: "DAME_DATOS_MATRIZ",
						CVE_CONFIGURACION: "VENTAS_PRODUCTO_BASE",
						ID_EMPRESA: IdEmpresa,
						ID_PRODUCTO_BASE:  $ID_PRODUCTO_BASE,
						ID_ALMACEN: $ID_ALMACEN, 
						ID_MOVIMIENTO:$ID_MOVIMIENTO
					},
					dataType: "json",
					success: function(data) {
						var contenido = generaMatriz(data);
						var divCantidad = $("#DynTCANTIDAD");
						divCantidad.html(contenido);
						var json_obj = $.parseJSON(data);
						var datosMovto = json_obj['datosResultado'];
						$.each(datosMovto, function() {
							$("#"+this['I']).val(this['V']);
						});
						var datosPlaceholder = json_obj['datosPlaceholder'];
						$.each(datosPlaceholder, function() {
							$("#"+this['I']).attr('placeholder',(this['V']));
						});
						inputAtts();
					},
					error: function() {
						console.log("Hay error grave");
					}
				});

		}else{
			divCantidad.html( '');
		}
	}
  
	if(cveOperacion=='A'){
		$('#ID_PRODUCTO_BASE').change(function() {
			actualizaSeries();
		}); 
	}else{
		actualizaSeries();
	}

  function AltaModVenta() 
  {
	var $ID_EMPRESA			= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $F_MOVIMIENTO		= $("#F_MOVIMIENTO").val();
	var $ID_MOVIMIENTO		= $("#ID_MOVIMIENTO").val();
	var $ID_NOTA   			= $("#ID_NOTA").val();
	var $ID_ALMACEN        	= $("#ID_ALMACEN").val();
	var $TX_REFERENCIA      = $("#TX_REFERENCIA").val();
	var $TIPO      			= 'SALIDA';
	var $SIT_MOVIMIENTO     = $("#SIT_MOVIMIENTO").val();
	var $ID_PRODUCTO_BASE  	= $("#ID_PRODUCTO_BASE").val();
	var $CTL_TIPO_OPER		= $('[name="PARAM_CTL_OPERACION"]').val();
	
	if($("#ID_MOVIMIENTO").val() == ''){
		console.log('Aún no hay id_movimiento ');
		$( ".alertMsg" ).each(function() { 
			$(this).addClass( "alert alert-danger" );
			$(this).text('Debe capturar algún valor');
			$(this).css( "display", "block" ).fadeOut( 10000 );
		});
		return;
	};
	
	var tieneValor 			= '0';
	
	$(".cnt").each(function() {
		if($(this).val()){
		   tieneValor = '1';
		};
	});
	
	if(tieneValor == '0'){
		console.log('No hay elementos con valor');
		$( ".alertMsg" ).each(function() { 
			$(this).addClass( "alert alert-danger" );
			$(this).text('Debe capturar un valor');
			$(this).css( "display", "block" ).fadeOut( 10000 );
		});
		return;
	}
	
		$.ajax({
            type: "POST",
            url: "OpticsSrv",
            data: {	CTL_CVE_OPERACION:"ALTA_MOD_VENTA",ID_EMPRESA:$ID_EMPRESA,ID_MOVIMIENTO:$ID_MOVIMIENTO, 
					ID_NOTA:$ID_NOTA,TX_REFERENCIA:$TX_REFERENCIA,TIPO:$TIPO,SIT_MOVIMIENTO:$SIT_MOVIMIENTO, //CTL_OPTICS_TMP_TRAN_ID:$CTL_OPTICS_TMP_TRAN_ID,
					ID_PRODUCTO_BASE:$ID_PRODUCTO_BASE,PARAM_CTL_OPERACION:$CTL_TIPO_OPER},
            dataType: "json",
            success: function(data) {
                //var obj = jQuery.parseJSON(data); if the dataType is not specified as json uncomment this
                // do what ever you want with the server response
				
				if(data[1]){
					$( ".alertMsg" ).each(function() {
						$(this).text( '');
						$(this).addClass( "alert alert-danger" );
						$(this).text( data[0] );
						$(this).css( "display", "block" ).fadeOut( 10000 );
					});
					console.log('Mensaje de error');
				}else{

					$("#PARAM_CTL_B_SINCRONIZA").val('V');
					$('[name="PARAM_CTL_OPERACION"]').val('M');
					$("#dSIT_MOVIMIENTO" ).show();
					console.log('success: ' + data);
					$( ".alertMsg" ).each(function() { 
						$(this).addClass( "alert alert-danger" );
						$(this).text( data[0]);
						$(this).css( "display", "block" ).fadeOut( 10000 );
					});
				}
            },
            error: function(data){
					console.log('Error en alta modificación: ' );
					$( ".alertMsg" ).each(function() {
						$(this).addClass( "alert alert-danger" );
						$(this).text( "Error en la operación: " + data[0]);
						$(this).css( "display", "block" ).fadeOut( 10000 );
					});
					console.log('error handing here'); 
            }
        });
  }


 $('#ALT_MOD_VTA').click(function() {  
  AltaModVenta();   
 });

if ( !$( "#ID_MOVIMIENTO" ).length ) {
		console.log('NO EXISTE ID_MOVIMIENTO Y LO CREA ');
		$('#formValidate').append('<input type="hidden" name="ID_MOVIMIENTO" id="ID_MOVIMIENTO" />');
		$("#dSIT_MOVIMIENTO" ).hide();
		console.log('ya lo creo ');
	}

  function getNota()
  {
	var $ID_EMPRESA	= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $ID_NOTA   	= $("#ID_NOTA").val();
	var $ID_ALMACEN = $("#ID_ALMACEN").val();
	var $ACTION 	= 'DinamicapsSrv?PARAM_CTL_ID_EMPRESA=' + $ID_EMPRESA + '&PARAM_CTL_CVE_NAVEGACION=NAVEGA_CAT_CTL_GET_REG&PARAM_CTL_ID_TABLA=373&PARAM_CTL_ID_MENU=menu_243&PARAM_CTL_OPERACION=M&PARAM_CTL_PANTALLA=583&TIPO_NOTA=SALIDA&ID_PANTALLA=583&ID_EMPRESA=' + $ID_EMPRESA + '&ID_NOTA=' + $ID_NOTA + '&ID_ALMACEN=' + $ID_ALMACEN
	$(location).attr('href',$ACTION);
  }

 $('#BACKBUTTON').click(function() {  
  getNota();   
 });

  /************************************************************************************************************************/



}); 

