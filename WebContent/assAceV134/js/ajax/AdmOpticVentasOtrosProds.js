function getId(el){
 while( (el = el.parentNode) && el.nodeName.toLowerCase() !== 'tr' );
  if(el)
   return el.getAttribute('id');
}

function addCommas(nStr){
nStr += '';
var x = nStr.split('.');
var x1 = x[0];
var x2 = x.length > 1 ? '.' + x[1] : '';
var rgx = /(\d+)(\d{3})/;
while (rgx.test(x1)) {
	x1 = x1.replace(rgx, '$1' + ',' + '$2');
}
return x1 + x2;
}

$('#idADD').click(function() {

	console.log('Entra al avento click');

	var vlRowPendiente  = document.getElementById("rowMovto0");
	console.log('Identifica el id 0 ');
	if(vlRowPendiente){
		alert('Debe capturar el movimiento pendiente');
		return;
	}
	console.log('Valida el tipo de producto');
	if(!$('#ID_TIPO_PRODUCTO').val()){
		alert('Elija un tipo de producto');
		return;
	}
	
	var idProducto 		= $('#ID_PRODUCTO').val();
	var descProducto	= $('#ID_PRODUCTO :selected').text();

	var IdEmpresa = $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var IdAlmacen = $('#ID_ALMACEN').val();
	
	var AplicaColor = $('option:selected', $('#ID_TIPO_PRODUCTO')).attr('atributo');
	
	var CveColor	= $('#CVE_COLOR').val();
	var descColor	= $('#CVE_COLOR :selected').text();
	
	if(AplicaColor=='F'){
		CveColor	= '';
		descColor	= '';
	}
	
	var precio = $('#PRECIO').val();
	precio = '$' + addCommas(parseFloat(precio).toFixed(2));
		
	console.log('Antes de ajax');
	$.ajax({
		type: "POST",
		url: "OpticsSrv",
		data: {
			ID_EMPRESA: IdEmpresa,
			CTL_CVE_OPERACION: "DAME_EXISTENCIA_OTROS_PRODUCTOS",
			ID_PRODUCTO: idProducto,
			ID_ALMACEN: IdAlmacen, 
			CVE_COLOR: CveColor
		},
		dataType: "json",
		success: function(data) {
			if($.isNumeric(data)){
				var valor = addCommas(data);
				//$('#tableToModify > tbody:last').append
				console.log('Obtiene resultado: ' + data);
				$('<tr id="rowMovto0"><td></td><td><div align="right">' + idProducto + '</div></td><td>' + descProducto + 
					'</td><td><input type="hidden" value="' + CveColor + '"/><div align="left">' + descColor + '</div></td><td><div align="right">' + 
					precio + '</div></td><td><div align="right">' + valor + '</div></td><td><div align="right">' + 
					'<input type="text" size="7" style="text-align:right;"/></div></td><td>' + 
					'<a href="#" title="Aplicar" onClick="JavaScript:aplicaMovto(getId(this));"><span class="green"><i class="fa fa-check bigger-160" aria-hidden="true">' + 
					'</i></span></a><a href="#" title="Cancelar" onClick="JavaScript:updateTransac(getId(this),\'CA\');"><span class="red">' + 
					'<i class="fa fa-times bigger-160" aria-hidden="true"></i></span></a></td><td></td></tr>').prependTo('#idTableID_PROVEEDOR > tbody:first');
			}else{
				alert(data);
			}
		},
		error: function(xhr, status, error) {
			var err = eval("(" + xhr.responseText + ")");
			alert(err.Message);
		}
	});
}); 

function isInt(value){
 if($.isNumeric(value)){
  if(value > 0){
	if((value.indexOf(' ') >= 0)||value.indexOf('.') >= 0||value.indexOf('-') >= 0){
	 alert('Caracter no v�lido');
	  return false;
	}else{
	 if(value % 1 === 0){
	  return true;
	 }else{
	  alert('Capture un n�mero entero');
	  return false;
	 }
	}
   }else{
	alert('El valor debe ser mayor a 0');
	return false;
   }
  } else{
  alert('Debe capturar un n�mero v�lido');
  return false;
 }
}

function aplicaMovto(IdRow){
	
	var $ID_EMPRESA		= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $ID_NOTA        = $("#ID_NOTA").val();
	var $ROW 			= $('#'+IdRow);
	var $ID_PRODUCTO	= $ROW.find('td:eq(1)').text();
	var $CVE_COLOR		= $ROW.find('td:eq(3)').find("input:hidden").val();
	var $PRECIO			= $ROW.find('td:eq(4)').text().replace('$', '').replace(',', '');
	var $EXISTENCIA		= $ROW.find('td:eq(5)').text().replace(',', ''); 
	var $CANTIDAD		= $ROW.find('td:eq(6)').find("input:text").val();

	if(isInt($CANTIDAD)){
		if(parseInt($CANTIDAD) <= parseInt($EXISTENCIA)){
			if(confirm('�Desea aplicar el movimiento?')){console.log('Aplicar');}else{return;}
			$.ajax({
				type: "POST",
				url: "OpticsSrv",
				data: {	CTL_CVE_OPERACION:"ALT_MOD_MOVTO_OTROS_PRODS",
					ID_EMPRESA:$ID_EMPRESA,
					ID_NOTA:$ID_NOTA,
					TIPO:'A',
					SIT_MOVIMIENTO:'AC',
					CANTIDAD:$CANTIDAD, 
					PRECIO: $PRECIO, 
					CVE_COLOR: $CVE_COLOR,
					ID_PRODUCTO:$ID_PRODUCTO},
				dataType: "json",
				success: function(data) {
					if($.isNumeric(data)){
						$("#PRECIO").trigger('change');
						$ROW.find('td:eq(0)').append('<div align="right">' + data + '</div>');
						$ROW.find('td:eq(6)').find("input:text").remove();
						$ROW.find('td:eq(6)').append('<div align="right">' + $CANTIDAD + '</div>');
						$ROW.attr('id','rowMovto' + data);
						$ROW.find('td:eq(7)').find("a:first").remove();
						$ROW.find('td:eq(7)').append('<a href="#" title="Cerrar" onClick="JavaScript:updateTransac(getId(this),\'CL\');"><span class="gray"><i class="fa fa-lock bigger-160" aria-hidden="true"></i></span></a>');
					}else{
						alert(data);
					}
				},
				error: function(data){
					console.log('error handing here ' + data); 
				}
			});
		}else{
			alert('Debe capturar una cantidad igual o menor a la existencia');
		}
	}
}

function updateTransac(IdRow, sitMovto){

	var $ROW = $('#'+IdRow);
	var $ID_MOVIMIENTO = $ROW.find('td:eq(0)').text();
	var rowDelete = document.getElementById(IdRow);

	if(!$.isNumeric($ID_MOVIMIENTO) && sitMovto=='CA'){console.log('Borra el rengl�n');rowDelete.parentNode.removeChild(rowDelete);return;}
	if(!$.isNumeric($ID_MOVIMIENTO) && (sitMovto=='AC'||sitMovto=='CL')){console.log('No encuentra');alert('No se pudo identificar el movimiento');return;}

	if(sitMovto=='AC'){if(confirm('�Desea abrir el movimiento?')){console.log('Abrir');}else{return;}}
	if(sitMovto=='CL'){if(confirm('�Desea cerrar el movimiento?')){console.log('Cerrar');}else{return;}}
	if(sitMovto=='CA'){if(confirm('�Desea eliminar el movimiento?')){console.log('Eliminar');}else{return;}}

	var $ID_EMPRESA	= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $ID_NOTA    = $("#ID_NOTA").val();

	$.ajax({
		type: "POST",
		url: "OpticsSrv",
		data: {	CTL_CVE_OPERACION:"ALT_MOD_MOVTO_OTROS_PRODS",
				ID_EMPRESA:$ID_EMPRESA,
				ID_NOTA:$ID_NOTA,
				TIPO:'M',
				SIT_MOVIMIENTO:sitMovto,
				ID_MOVIMIENTO:$ID_MOVIMIENTO},
		dataType: "json",
		success: function(data) {
				console.log('Resultado: ' + data);
			if($.isNumeric(data)){
				//console.log('Si es numerico');
				if(sitMovto=='CA'){
					$("#PRECIO").trigger('change');
					alert('Operaci�n Cancelada');
					rowDelete.parentNode.removeChild(rowDelete);
					return;
				}else{
					if(sitMovto=='CL'){
						alert('Operaci�n Cerrada');
						$ROW.find('td:eq(7)').children('a').remove();
						if(data=='1'){
							$ROW.find('td:eq(7)').append('<a href="#" class="remove" title="Cancelar" onClick="JavaScript:updateTransac(getId(this),\'CA\');"><span class="blue"><i class="fa fa-times bigger-160" aria-hidden="true"></i></span></a>');
						}
						return;
					}else{
						if(sitMovto=='AC'){
							alert('Operaci�n Abierta');
							$ROW.find('td:eq(7)').find("a:first").remove();
							$ROW.find('td:eq(7)').append('<a href="#" title="Cancelar" onClick="JavaScript:updateTransac(getId(this),\'CA\');"><span class="red"><i class="fa fa-times bigger-160" aria-hidden="true"></i></span></a><a href="#" title="Cerrar" onClick="JavaScript:updateTransac(getId(this),\'CL\');"><span class="blue"><i class="fa fa-lock bigger-160" aria-hidden="true"></i></span></a>');
						}
					}
				}
			}else{
				alert(data);
			}
		},
		error: function(data){
			console.log('error handing here'); 
		}
	});
}


$(document).ready(function() {
	console.log('Document ready !!!'); 
	$( "#idTableID_PROVEEDOR" ).append(	' <thead align="right"> \n' + 
										' <tr id="encabezado"> \n' + 
										'   <th style="width: 4%;"><div align="right">Id</div></th> \n' + 
										'   <th style="width: 7%;"><div align="right">Id Producto</div></th> \n' + 
										'   <th style="width: 30%;"><div align="center">Producto</div></th> \n' + 
										'   <th style="width: 15%;"><div align="center">Color</div></th> \n' + 
										'   <th style="width: 10%;"><div align="center">Precio</div></th> \n' + 
										'   <th style="width: 8%;"><div align="right">Existencia</div></th> \n' + 
										'   <th style="width: 8%;"><div align="right">Cantidad</div></th> \n' + 
										'   <th style="width: 7%;"><div align="center">Acci&oacute;n</div></th> \n' + 
										'   <th style="width: 3%;"></th> \n' + 
										'  </tr> \n' + 
										' </thead> \n' + 
										' <tbody> \n' + 
										'</tbody> \n'); 
	$( "#idTableID_PROVEEDOR" ).insertAfter( "#divButtons" );
	var $ID_NOTA    = $("#ID_NOTA").val();
	
	$.ajax({
		type: "POST",
		url: "OpticsSrv",
		data: {
			CTL_CVE_OPERACION: "GET_MOVTOS_OTROS_PRODS_NOTA",
			ID_NOTA:$ID_NOTA,
			TIPO_NOTA: 'SALIDA'
		},
		dataType: "json",
		success: function(data) {
			var datosMovtos	= data['datosMovtos'];
			$.each(datosMovtos, function() {
				console.log('Obtiene Movto: ' + this['A']);
				var vIdMovto		= this['A']; 
				var vIdProducto		= this['B']; 
				var vDesProducto	= this['C']; 
				var vCveColor		= this['D']?this['D']:''; 
				var vDesColor		= this['E']?this['E']:''; 
				var vPrecio			= this['F']; 
				var vCantidad		= this['G']; 
				var vSitNota		= this['H']; 
				var vSitMovto		= this['I']; 
				var vIdRol			= this['J']; 
				var vRemoveClose	= '';	
				var vOpen			= '';	
				
				console.log('vIdProducto: ' + vIdProducto);
				console.log('vSitNota: ' + vSitNota);
				console.log('vSitMovto: ' + vSitMovto);
				
				if(vSitNota=='AC'&&vSitMovto=='AC'){
					vRemoveClose = '<a href="#" class="remove"><span class="red"><i class="fa fa-times bigger-160" aria-hidden="true"></i></span></a>\n' + 
									 '<a href="#" class="closed" title="Cerrar"><span class="gray"><i class="fa fa-lock bigger-160"></i></span></a>';
				}
				if(vIdRol){
					if(vSitNota=='AC'&&vSitMovto=='CL'){
						vOpen = '<a href="#" class="remove" title="Cancelar Movimiento Cerrado"><span class="blue"><i class="fa fa-times bigger-160" aria-hidden="true"></i></span></a>';
					}
				}
				
				$('<tr id="rowMovto' + vIdMovto + '"><td><div align="right">' + vIdMovto + '</div></td><td><div align="right">' + vIdProducto + '</div></td><td>' + vDesProducto + 
					'</td><td><input type="hidden" value="' + vCveColor + '"/><div align="left">' + vDesColor + '</div></td><td><div align="right">' + 
					vPrecio + '</div></td><td></td><td><div align="right">' + 
					vCantidad + '</div></td><td>' + vRemoveClose + vOpen + 
					'</td><td></td></tr>').prependTo('#idTableID_PROVEEDOR > tbody:first');
			});
			$( ".remove" ).click(function() {
				updateTransac(getId(this),'CA'); 
			});
			$( ".closed" ).click(function() {
				updateTransac(getId(this),'CL');
			});
			$('.fa-check').attr( 'title', 'Aceptar');
			$('.icon-remove remove').attr( 'title', 'Cancelar');
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log("Hay error grave");
			console.log(xhr.status);
			console.log(thrownError);
			console.log(xhr.responseText); 
      }
	});
	
	$('.fa-check').attr( 'title', 'Aceptar');
	$('.icon-remove').attr( 'title', 'Cancelar');
	$( ".remove" ).click(function() {
		updateTransac(getId(this),'CA'); 
	});
	$( ".closed" ).click(function() {
		updateTransac(getId(this),'CL');
	});

	
	

}); 