function getId(el){
 while( (el = el.parentNode) && el.nodeName.toLowerCase() !== 'tr' );
  if(el)
   return el.getAttribute('id');
}

$('#ADDBUTTON').click(function() {

	var vlRowPendiente  = document.getElementById("rowMovto0");
	if(vlRowPendiente){
		alert('Debe capturar el movimiento pendiente');
		return;
	}
	
	if(!$('#ID_TIPO_PRODUCTO').val()){
		alert('Elija un tipo de producto');
		return;
	}
	
	var idProducto 	= $('#ID_PRODUCTO').val();
	var descProducto= $('#ID_PRODUCTO :selected').text();

	var IdEmpresa 	= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var IdAlmacen 	= $('#ID_ALMACEN').val();
	if(!IdAlmacen){
		alert('Capture un Almacen');
		return;
	}
	var AplicaColor = $('option:selected', $('#ID_TIPO_PRODUCTO')).attr('atributo');
	
	var CveColor	= $('#CVE_COLOR').val();
	var descColor	= $('#CVE_COLOR :selected').text();
	
	
	
	if(AplicaColor=='F'){
		CveColor	= '';
		descColor	= '';
	}

	//$('#tableToModify > tbody:first').append
	$('<tr id="rowMovto0"><td><input type="hidden" value="' + IdAlmacen + '"/></td><td><div align="right">' + idProducto + '</div></td><td>' + descProducto + 
		'</td><td><input type="hidden" value="' + CveColor + '"/><div align="left">' + descColor + 
		'</div></td><td><div align="right">' + vPrecio +
		'</div></td><td><div align="right"><input type="text" size="7" style="text-align:right;"/></div></td><td>' + 
		'<div align="left"><input type="text" size="20" style="text-align:left;"/></div></td><td>' + 
		'<a href="#" title="Aplicar" onClick="JavaScript:aplicaMovto(getId(this));"><span class="green"><i class="fa fa-check bigger-180"></i></span></a>' +
		'<a href="#" title="Cancelar" onClick="JavaScript:updateTransac(getId(this),\'CA\');"><span class="red"><i class="fa fa-times bigger-160" aria-hidden="true"></i></span>' +
		'</a></td><td></td></tr>').prependTo('#tableToModify > tbody:first');
}); 

function isInt(value){
 if($.isNumeric(value)){
  if(value > 0){
	if((value.indexOf(' ') >= 0)||value.indexOf('.') >= 0||value.indexOf('-') >= 0){
	 alert('Caracter no v�lido');
	  return false;
	}else{
	 if(value % 1 === 0){
	  return true;
	 }else{
	  alert('Capture un n�mero entero');
	  return false;
	 }
	}
   }else{
	alert('El valor debe ser mayor a 0');
	return false;
   }
  } else{
  alert('Debe capturar un n�mero v�lido');
  return false;
 }
}

function aplicaMovto(IdRow){
	
	var $ID_EMPRESA		= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $ROW 			= $('#'+IdRow);
	var $ID_ALMACEN		= $ROW.find('td:eq(0)').find("input:hidden").val();
	var $ID_PRODUCTO	= $ROW.find('td:eq(1)').text();
	var $CVE_COLOR		= $ROW.find('td:eq(3)').find("input:hidden").val();
	var $PRECIO			= $ROW.find('td:eq(4)').text().replace(',', ''); 
	var $CANTIDAD		= $ROW.find('td:eq(5)').find("input:text").val();
	var $TX_REFERENCIA	= $ROW.find('td:eq(6)').find("input:text").val();
	
	if(isInt($CANTIDAD)){
		
		if(confirm('�Desea aplicar el movimiento?')){console.log('Aplicar');}else{return;}
		$.ajax({
			type: "POST",
			url: "OpticsSrv",
			data: {	CTL_CVE_OPERACION:"DYN_OPERATION",
					DYN_CTL_CVE_OPERACION:"DYN_OPERATION",
					DYN_CTL_OPERATION:"INSERT_MOVIMIENTO_AJUSTE_SUMA",
					ID_ALMACEN: $ID_ALMACEN,
					ID_PRODUCTO: $ID_PRODUCTO, 
					CVE_COLOR: $CVE_COLOR,
					PRECIO: $PRECIO,
					CANTIDAD:$CANTIDAD,
					TX_REFERENCIA:$TX_REFERENCIA
					},
			dataType: "json",
			success: function(data) {
				
				var vResult	= data['result'];
				if(vResult){
					console.log('sucess DYN_OPERATION');
					console.log(data);
					console.log(data.result);
					var vMessage = vResult[0].A;
					if(vMessage){
						$ROW.find('td:eq(5)').find("input:text").remove();
						$ROW.find('td:eq(5)').append('<div align="right">' + $CANTIDAD + '</div>');
						$ROW.find('td:eq(6)').find("input:text").remove();
						$ROW.find('td:eq(6)').append('<div align="left">' + $TX_REFERENCIA + '</div>');
						$ROW.attr('id','rowMovto' + data);
						$ROW.find('td:eq(7)').find("a").remove();
						alert(vMessage);
					}else{
						alert('Error en success');
					}
				}
			},
			error: function(data){
				alert('Error');
			}
		});
	}
}


function updateTransac(IdRow, sitMovto){
	var $ROW = $('#'+IdRow);
	var $ID_MOVIMIENTO = $ROW.find('td:eq(0)').text();
	var rowDelete = document.getElementById(IdRow);
	rowDelete.parentNode.removeChild(rowDelete);
}


$(document).ready(function() {
	$( "#tableToModify" ).insertAfter( "#divButtons" );
}); 