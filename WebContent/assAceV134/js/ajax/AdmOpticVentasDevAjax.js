$(document).ready(function() {

  function inputAtts()
  {
    $(".cnt").each(function() {
		var vname = $(this).attr('id');
		var vlVendidos  = $(this).attr("placeholder");
		if(typeof vlVendidos === 'undefined'){
		   vlVendidos = '0';
		};
		var vTooltip = 'H: ' + vname.split("|")[2] + ' - V: ' + vname.split("|")[3] + ' - Vendidos: ' + vlVendidos;
		$(this).attr( 'title', vTooltip );
		if($(this).val()){$(this).css({"background-color": "#FF6600"});$(this).css({"text-align": "center"});$(this).css({"color": "#00008A"});
		}else{$(this).css({"background-color": "white"});$(this).css({"text-align": "center"});}
		if ($('#ALT_MOD_VTA_DEV').length == 0) {	
		  $(this).prop('disabled', true);
		}
	});
	
	
	
$('input.cnt').focus(function() {
	if ($(this).val() != null) {
		cantidadAnterior = $(this).val();
	}
	}).change(function() {

  if($(this).val() || (cantidadAnterior != null) ){

	var lastVal = cantidadAnterior;
	if(! lastVal){
		lastVal = 0;
		console.log('lastVal es nulo 2: >' + lastVal + '<');
	}else{
		console.log('lastVal NO es nulo 2: >' + lastVal + '<');
	}

	var $Elem = $(this);
    var valor = $Elem.val();
	var vlVendidos  = $Elem.attr("placeholder");
	console.log('Placeholder: ' + vlVendidos);
	
	if(typeof vlVendidos === 'undefined'){
	   console.log('Placeholder es nulo');
	   $Elem.val('');
	   $( ".alertMsg" ).each(function() {
			$(this).text( '');
			$(this).addClass( "alert alert-danger" );
			$(this).text('No existen productos a devolver');
			$(this).css( "display", "block" ).fadeOut( 10000 );
		});
	   return;
	};
	 
	if(!$(this).val()){
		valor = '0';
	}

	var $vId   = $Elem.attr('id');
    console.log('tiene un valor ' + valor + ' valor anterior: ' + cantidadAnterior);

	
    if($.isNumeric(valor)){

		if(valor > vlVendidos){
			
			console.log('Sobrepasa lo vendido');
		   $Elem.val(cantidadAnterior);
		   if($Elem.val()){$Elem.css({"background-color": "#FF6600"});$Elem.css({"text-align": "center"});$Elem.css({"color": "#00008A"});
			}else{$Elem.css({"background-color": "white"});$Elem.css({"text-align": "center"});}
			$( ".alertMsg" ).each(function() {
				$(this).addClass( "alert alert-danger" );
				$(this).text( "Solo se pueden devolver " + vlVendidos + ' productos');
				$(this).css( "display", "block" ).fadeOut( 10000 );
			});
			return;
		}
		
		if(valor < 0){
			console.log('Valor negativo');
		   $Elem.val(cantidadAnterior);
		   if($Elem.val()){$Elem.css({"background-color": "#FF6600"});$Elem.css({"text-align": "center"});$Elem.css({"color": "#00008A"});
			}else{$Elem.css({"background-color": "white"});$Elem.css({"text-align": "center"});}
			$( ".alertMsg" ).each(function() {
				$(this).addClass( "alert alert-danger" );
				$(this).text('No se permiten valores negativos');
				$(this).css( "display", "block" ).fadeOut( 10000 );
			});
			return;
		}
		
	
	
	if ( !$( "#ID_MOVIMIENTO" ).length ) {
		console.log('ya existe ');
	}
	
var $divRespuesta     	= $("#RESPUESTA");
var $ID_MOVIMIENTO		= $("#ID_MOVIMIENTO").val();
console.log('ID_MOVIMIENTO valor ' + $ID_MOVIMIENTO);
var $ID_DEVOLUCION		= $("#ID_DEVOLUCION").val();
var $ID_EMPRESA			= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
var $ID_CLIENTE    		= $("#ID_CLIENTE").val();
var $ID_ALMACEN        	= $("#ID_ALMACEN").val();
var $TX_REFERENCIA      = $("#TX_REFERENCIA").val();
var $SIT_MOVIMIENTO     = $("#SIT_MOVIMIENTO").val();
var $CTL_TIPO_OPER		= $('[name="PARAM_CTL_OPERACION"]').val();
var $B_SINCRONIZA     	= $("#PARAM_CTL_B_SINCRONIZA").val();
var $ID_PRODUCTO_BASE  	= $vId.split("|")[0];
var $ID_SERIE  			= $vId.split("|")[1];
var $ID_VALOR_HORIZ  	= $vId.split("|")[2];
var $ID_VALOR_VERT  	= $vId.split("|")[3];

		console.log('antes de la llamada ');
		//var datastring = $("#formValidate").serialize();
		$.ajax({
            type: "POST",
            url: "OpticsSrv",
			async: false, 
            data: {CTL_CVE_OPERACION:"GENERA_PRE_MOVIMIENTO",ID_EMPRESA:$ID_EMPRESA,CVE_PRODUCTO:$vId, ID_ALMACEN:$ID_ALMACEN,
					TIPO_MOVIMIENTO:'SALIDA_DEVOLUCION',TIPO_DEVOLUCION:$SIT_MOVIMIENTO,
					CANTIDAD:valor,ID_MOVIMIENTO:$ID_MOVIMIENTO, ID_DEVOLUCION:$ID_DEVOLUCION,PARAM_CTL_B_SINCRONIZA:$B_SINCRONIZA,PARAM_CTL_OPERACION:$CTL_TIPO_OPER},
            dataType: "json",
            success: function(data) {

					console.log('id movto: >' + data[0] + '<');
					console.log('MSG ERROR: >' + data[1] + '<');
					if(data[1]){
						$Elem.val(lastVal);
						$( ".alertMsg" ).each(function() {
							$(this).text( '');
							$(this).addClass( "alert alert-danger" );
							$(this).text( data[1] );
							$(this).css( "display", "block" ).fadeOut( 10000 );
						});
						console.log('Mensaje de error');
					}else{
						console.log('data[0]: ' + data[0]); 
						$("#ID_MOVIMIENTO").val(data[0]); 
						$("#PARAM_CTL_B_SINCRONIZA").val('F');
						if(valor=='0'){
							$Elem.val('');
						}
						if($Elem.val()){$Elem.css({"background-color": "#FF6600"});$Elem.css({"text-align": "center"});$Elem.css({"color": "#00008A"});
						}else{$Elem.css({"background-color": "white"});$Elem.css({"text-align": "center"});}
						
						console.log('success: ' + data[0]);
						console.log('success cantidadAnterior: ' + lastVal);
					}
            },
            error: function(data){
					console.log('Error cantidadAnterior: ' + lastVal);
					$Elem.val(lastVal);
					console.log('error handing here'); 
					$( ".alertMsg" ).each(function() {
						$(this).addClass( "alert alert-danger" );
						$(this).text( data );
						$(this).css( "display", "block" ).fadeOut( 10000 );
					});
            }
        });

		
		console.log('despues de la llamada ');
		
    }else{
       console.log('El valor no es numérico');
	   $Elem.val(cantidadAnterior);
	   if($Elem.val()){$Elem.css({"background-color": "#FF6600"});$Elem.css({"text-align": "center"});$Elem.css({"color": "#00008A"});
		}else{$Elem.css({"background-color": "white"});$Elem.css({"text-align": "center"});}
		$( ".alertMsg" ).each(function() {
			$(this).addClass( "alert alert-danger" );
			$(this).text( "Solo valores numéricos enteros están permitidos" );
			$(this).css( "display", "block" ).fadeOut( 10000 );
		});
    }

  }

});



  } // termina el inputAtts


	inputAtts();
	

  function AplicaDevolucionVenta()
  {
	var $ID_EMPRESA			= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $ID_MOVTO_VENTA		= $("#ID_MOVIMIENTO").val();
	var $ID_DEVOLUCION		= $("#ID_DEVOLUCION").val();
	var $SIT_MOVIMIENTO    	= $("#TIPO_MOVIMIENTO").val();
	var $TX_REFERENCIA      = $("#TX_REFERENCIA").val();
	var tieneValor 			= '0'; 
	
	$(".cnt").each(function() {
		if($(this).val()){
		   tieneValor = '1';
		};
	});
	
	console.log('tieneValor: ' + tieneValor);
	
	if(tieneValor == '0'){
		console.log('No hay elementos con valor');
		$( ".alertMsg" ).each(function() { 
			$(this).addClass( "alert alert-danger" );
			$(this).text('Debe capturar un valor 2');
			$(this).css( "display", "block" ).fadeOut( 10000 );
		});
		return;
	}
	
		$.ajax({
            type: "POST",
            url: "OpticsSrv",
            data: {	CTL_CVE_OPERACION:"ALTA_MOD_DEVOLUCION_VENTA",ID_EMPRESA:$ID_EMPRESA,ID_MOVTO_VENTA:$ID_MOVTO_VENTA, 
					TX_REFERENCIA:$TX_REFERENCIA,ID_DEVOLUCION:$ID_DEVOLUCION,SIT_MOVIMIENTO:$SIT_MOVIMIENTO
				},
            dataType: "json",
            success: function(data) {
                //var obj = jQuery.parseJSON(data); if the dataType is not specified as json uncomment this
                // do what ever you want with the server response
				$("#PARAM_CTL_B_SINCRONIZA").val('V');
				$("#dTIPO_MOVIMIENTO" ).show();
				console.log('success: ' + data);
				$( ".alertMsg" ).each(function() { 
					$(this).addClass( "alert alert-danger" );
					$(this).text( data[0]);
					$(this).css( "display", "block" ).fadeOut( 10000 );
				});
            },
            error: function(data){
					console.log('Error en alta modificación: ' );
					
					$( ".alertMsg" ).each(function() {
						$(this).addClass( "alert alert-danger" );
						$(this).text( "Error en la operación: " + data[0]);
						$(this).css( "display", "block" ).fadeOut( 10000 );
					});
					console.log('error handing here'); 
            }
        });
  }


 $('#ALT_MOD_VTA_DEV').click(function() {  
  AplicaDevolucionVenta();   
 });

 
/************************************************************************************************************************/
window.addEventListener('paste', function (event) {
	
	var $input = $(':focus');
	var vId = $input.attr('id');
	//console.log('este es el id: ' + $input.attr('id'));
	var idColIni = vId.split("|")[4];
	var idColAct = vId.split("|")[4];
	//console.log('>> Inicial ' + idColIni);
	var vTr = $input.closest('tr');
	var vTrId = $input.closest('tr').attr('id');
	//console.log('>> esta es el renglon ' + vTrId);
    //var clipText = window.clipboardData.getData('Text');
	var clipText = event.clipboardData.getData('text');
	
    // split into rows

    clipRows = clipText.split(String.fromCharCode(13));
	//console.log('>> clipRows.length ' + clipRows.length);
    // split rows into columns

    for (i=0; i<clipRows.length; i++) {
    	clipRows[i] = clipRows[i].split(String.fromCharCode(9));
		//console.log('>> dentro');
    }

    for (i=0; i<clipRows.length - 1; i++) {
		//console.log('>> row: ' + i);
		if(i>0){
			vTr = vTr.closest('tr').next('tr');
			$input = vTr.find("td:first input");
			//console.log('>> esta es el proximo renglon: ' + vTr.attr('id'));
			for (x=0; x<idColIni -1; x++) {
				$input = $input.closest('td').next().find('input'); 
			}
		}
    	for (j=0; j<clipRows[i].length; j++) {
			
			//console.log('>> val: ' + clipRows[i][j]);
			$input.focus();
			$input.val(clipRows[i][j]);
			$input.trigger( "change" );
			//console.log('>> input1: ' + $input.val());
			$input = $input.closest('td').next().find('input'); 
			//console.log('>> Actualiza');
    	}
    }

	event.preventDefault();
});	

/************************************************************************************************************************/

  function getNota()
  {
	var $ID_EMPRESA	= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $ID_NOTA   	= $("#ID_NOTA").val();
	var $ID_ALMACEN = $("#ID_ALMACEN").val();
	var $ACTION 	= 'DinamicapsSrv?PARAM_CTL_ID_EMPRESA=' + $ID_EMPRESA + '&PARAM_CTL_CVE_NAVEGACION=NAVEGA_CAT_CTL_GET_REG&PARAM_CTL_ID_TABLA=381&PARAM_CTL_ID_MENU=menu_257&PARAM_CTL_OPERACION=M&PARAM_CTL_PANTALLA=632&ID_PANTALLA=632&ID_EMPRESA=' + $ID_EMPRESA + '&ID_NOTA=' + $ID_NOTA + '&ID_ALMACEN=' + $ID_ALMACEN;
	//console.log($ACTION);
	//var url = "http://stackoverflow.com";    
	$(location).attr('href',$ACTION);
	//$('<form id="getNota" action="' + $ACTION + '"></form>').appendTo('body').submit();
  }

 $('#BACKBUTTON').click(function() {  
  getNota();   
 });

  /************************************************************************************************************************/

$("#dTIPO_MOVIMIENTO" ).hide();

}); 

