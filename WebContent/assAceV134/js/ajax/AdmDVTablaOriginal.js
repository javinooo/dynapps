$(document).ready(function() {


	$('#TIPO').on('change', function() {
		var $vVal = $(this).val();
		console.log('valor: ' + $vVal);

		var $vType = $("#TIPO").get(0).tagName;
		console.log('Tipo de elemento: ' + $vType);
		//$("#TIPO").is("input")
		if($vType=='SELECT'){
			if($vVal=='ORDEN'){
				$('#dID_PROVEEDOR').show();
				$('#dID_TIPO_MEDIO').show();
				$('#dID_ORDEN_REF').hide();
				$('#dIMPORTE').hide();
			}else{
				if($vVal=='NCRED'){
					$('#dID_PROVEEDOR').hide();
					$('#dID_TIPO_MEDIO').hide();
					$('#dID_ORDEN_REF').show();
					$('#dIMPORTE').show();
				}
			}
			$(window).trigger('resize.combo');
		}else{
			console.log('Else');
			if($vVal=='ORDEN'){
				console.log('Elimina el elemento: ');
				$('#dID_ORDEN_REF').remove();
				$("#IMPORTE").prop('disabled', true);
			}
		}
	}).trigger('change');

}); 

