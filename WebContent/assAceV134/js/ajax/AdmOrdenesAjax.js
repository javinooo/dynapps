$(document).ready(function() {
	$('#TIPO').on('change', function() {
		var $vVal = $(this).val();
		var $vType = $("#TIPO").get(0).tagName;

		if($vType=='SELECT'){
			if($vVal=='ORDEN'){
				console.log('Es orden ');
				$('#dID_PROVEEDOR').show().removeClass( "ignore" );
				$('#ID_PROVEEDOR').removeClass( "ignore" );
				$('#dID_TIPO_MEDIO').show().removeClass( "ignore" );
				$('#ID_TIPO_MEDIO').removeClass( "ignore" );
				$('#dID_ORDEN_REF').hide();
				$('#ID_ORDEN_REF').addClass( "ignore" );
				$('#dIMPORTE').hide();
			}else{
				if($vVal=='NCRED'){
					console.log('Es nota de credito ');
					$('#dID_PROVEEDOR').hide().addClass( "ignore" );
					$('#ID_PROVEEDOR').addClass( "ignore" );
					$('#dID_TIPO_MEDIO').hide().addClass( "ignore" );
					$('#ID_TIPO_MEDIO').addClass( "ignore" );
					$('#dID_ORDEN_REF').show();
					$('#ID_ORDEN_REF').removeClass( "ignore" );
					$('#dIMPORTE').show();
				}
			}
			$(window).trigger('resize.combo');
		}else{
			if($vVal=='ORDEN'){
				console.log('Elimina el elemento: ');
				$('#dID_ORDEN_REF').remove();
				$("#IMPORTE").prop('disabled', true);
			}
		}
	}).trigger('change');

}); 