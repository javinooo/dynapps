$(document).ready(function() {


function actualizaMensaje(vRow,vStatus){
	
	console.log(vRow);
	var $vIsChecked	= vRow.find('input:checkbox:first').is(":checked"); 
	console.log('vIsChecked: ' + $vIsChecked);
	var $vIdMsg		= vRow.find('td:eq(1)').text(); 
	console.log($vIdMsg);
	
	var $vTable = $('#TableRs500').dataTable();

	$.ajax({
		type: "GET",
		url: "VStreamSrv",
		data: {
			DYN_CTL_CVE_OPERACION: 	"UPDATE_STATUS_MSG",
			ID_MENSAJE: 			$vIdMsg, 
			SIT_MENSAJE: 			vStatus
		},
        contentType: "application/json; charset=utf-8",
		dataType: "json", 
		success: function(data) {
			var vResult	= data['result'];
			var vMessage = vResult[0].A;
			vRow.find('td:eq(5)').text(vMessage); 
			vRow.find('input:checkbox:first').prop( "checked", false );
			//$vTable.row(vRow).deselect();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log("Hay error grave");
			console.log(xhr.status);
			console.log(thrownError);
			console.log(xhr.responseText); 
			/*if(xhr.status=403){
                window.location.assign("index.jsp");
			}*/
      }
	});



	
}


 $('#boton_rechazar').click(function() {  
	console.log('entra a boton_aceptar VVV	');
	$('#TableRs500 tbody tr').each(function() {
		var $Row 		= $(this); 
		var $vIsChecked	= $(this).find('input:checkbox:first').is(":checked"); 
		console.log($vIsChecked);
		if($vIsChecked){
			actualizaMensaje($Row,'RE');
		}
	});
 });


 $('#boton_aceptar').click(function() {  
	console.log('entra a boton_aceptar VVV	');
	$('#TableRs500 tbody tr').each(function() {
		var $Row 		= $(this); 
		var $vIsChecked	= $(this).find('input:checkbox:first').is(":checked"); 
		console.log($vIsChecked);
		if($vIsChecked){
			actualizaMensaje($Row,'OK');
		}
	});
 });

/************************************************************************************************************************/

}); 

