$(document).ready(function() {

  function inputAtts()
  {
    $(".cnt").each(function() {
		var vname = $(this).attr('id');
		//console.log('obtiene el id');
		var vTooltip = 'H: ' + vname.split("|")[2] + ' - V: ' + vname.split("|")[3];
		
		//console.log('obtiene el vTooltip');
		
		if($(this).val()){
			$(this).css({"background-color": "#FF6600"});$(this).css({"text-align": "center"});$(this).css({"color": "#00008A"});
			vTooltip = vTooltip + ' - Mínimo: ' + vname.split("|")[4];
		}else{$(this).css({"background-color": "white"});$(this).css({"text-align": "center"});}
		$(this).attr( 'title', vTooltip );
	});
	
	
} // termina el inputAtts
	 
	 
  function RefrescaReporte()
  {
	var $ID_EMPRESA	= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $ID_ALMACEN = $("#ID_ALMACEN").val();
	var $ID_PRODUCTO_BASE = $("#ID_PRODUCTO_BASE").val();
	var divCantidad = $("#DynTID_SERIE");

	if($ID_ALMACEN){
		$.ajax({
			type: "POST",
			url: "OpticsSrv",
			data: {	CTL_CVE_OPERACION:"REPORTE_MINIMOS_INVENTARIO_PB", ID_EMPRESA:$ID_EMPRESA, ID_ALMACEN:$ID_ALMACEN,
					ID_PRODUCTO_BASE:$ID_PRODUCTO_BASE},
			dataType: "json",
			success: function(data) {
				divCantidad.html( data);
				inputAtts();
			},
			error: function(data){
					console.log('Error en alta modificación: ' );
					$( ".alertMsg" ).each(function() {
						$(this).addClass( "alert alert-danger" );
						$(this).text( "Error en la operación");
						$(this).css( "display", "block" ).fadeOut( 10000 );
					});
					console.log('error handing here'); 
			}
		});
	}else{
		divCantidad.html('');
		alert('Elija un Almacén y un producto')
	}
  }

 $('#ID_PRODUCTO_BASE').change(function() {  
  RefrescaReporte();   
 });



}); 

