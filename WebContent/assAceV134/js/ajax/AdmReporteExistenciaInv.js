$(document).ready(function() {

 var ResultDiv = $('<div class="hr hr-18 dotted hr-double"></div><div class="space-6"></div><div class="space-6"></div><div id="SearchResult1" class="row"><div class="col-xs-12"><form class="form-horizontal" id="none"><div class="form-group"><label class="col-sm-3 control-label no-padding-right">Series</label><div class="col-sm-9"><div class="clearfix"><div id="DynTableRep"></div></div></div></div></form></div></div>');
 $('#frmAltaMD').after(ResultDiv); 
 $('#SearchResult1').hide();
 
  function inputAtts()
  {
    $(".cnt").each(function() {
		var vname = $(this).attr('id');
		//console.log('obtiene el id');
		var vTooltip = 'H: ' + vname.split("|")[2] + ' - V: ' + vname.split("|")[3];
		//console.log('obtiene el vTooltip');
		$(this).attr( 'title', vTooltip );
		if($(this).val()){$(this).css({"background-color": "#FF6600"});$(this).css({"text-align": "center"});$(this).css({"color": "#00008A"});
		}else{$(this).css({"background-color": "white"});$(this).css({"text-align": "center"});}
		//$(this).attr('disabled','disabled');
	});
	
	
	$('input.cnt').focus(function() {
		if ($(this).val() != null) {
			cantidadAnterior = $(this).val();
		}
		}).change(function() {
			$(this).val(cantidadAnterior);
	});
	
	
} // termina el inputAtts
	 

  function RefrescaReporte()
  {
	$('#DynTableRep').html('');
	var $ID_EMPRESA			= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $ID_ALMACEN     	= $("#ID_ALMACEN").val();
	var $ID_PRODUCTO_BASE	= $("#ID_PRODUCTO_BASE").val();
	
		$.ajax({
            type: "POST",
            url: "OpticsSrv",
            data: {	CTL_CVE_OPERACION:"REPORTE_EXISTENCIAS",ID_EMPRESA:$ID_EMPRESA,
					ID_PRODUCTO_BASE:$ID_PRODUCTO_BASE,ID_ALMACEN:$ID_ALMACEN},
            dataType: "json",
            success: function(data) {
                //var obj = jQuery.parseJSON(data); if the dataType is not specified as json uncomment this
                // do what ever you want with the server response
				
				if(data[1]){
					console.log('Error: ' + data[1]);
					$( ".alertMsg" ).each(function() { 
						$(this).addClass( "alert alert-danger" );
						$(this).text( data[1]);
						$(this).css( "display", "block" ).fadeOut( 10000 );
					});
					
				}else{
					console.log('No hay mensaje de error');
					$('#SearchResult1').show();
					$('#DynTableRep').html( data[0] );
					inputAtts();
				}
            },
            error: function(data){
					console.log('Error en alta modificación: ' );
					$( ".alertMsg" ).each(function() {
						$(this).addClass( "alert alert-danger" );
						$(this).text( "Error en la operación: " + data[1]);
						$(this).css( "display", "block" ).fadeOut( 10000 );
					});
					console.log('error handing here'); 
            }
        });
  }


 $('#SEARCH_REP').click(function() {  
  RefrescaReporte();   
 });

 
 

  /************************************************************************************************************************/







}); 

