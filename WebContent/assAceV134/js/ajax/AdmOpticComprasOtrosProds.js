function getId(el){
 while( (el = el.parentNode) && el.nodeName.toLowerCase() !== 'tr' );
  if(el)
   return el.getAttribute('id');
}

function addCommas(nStr){
nStr += '';
var x = nStr.split('.');
var x1 = x[0];
var x2 = x.length > 1 ? '.' + x[1] : '';
var rgx = /(\d+)(\d{3})/;
while (rgx.test(x1)) {
	x1 = x1.replace(rgx, '$1' + ',' + '$2');
}
return x1 + x2;
}

function fValDup(vIdProd){
	
	var vExiste = false; 
	
	$(".cidprod").each(function() {
		if($(this).text()==vIdProd){
			vExiste = true;
		}
	});
	return vExiste; 
}


$('#ADDBUTTON').click(function() {

	var vlRowPendiente  = document.getElementById("rowMovto0");
	if(vlRowPendiente){
		alert('Debe capturar el movimiento pendiente');
		return;
	}
	
	if(!$('#ID_TIPO_PRODUCTO').val()){
		alert('Elija un tipo de producto');
		return;
	}
	
	var idProducto 	= $('#ID_PRODUCTO').val();
	var idMovimiento= $('#ID_MOVIMIENTO').val();
	var vCantidad 	= $('#CANTIDAD').val();
	var vPrecio 	= $('#PRECIO').val();
	var descProducto= $('#ID_PRODUCTO :selected').text();

	var IdEmpresa 	= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var vTipoOper 	= $('[name="PARAM_CTL_OPERACION"]').val();
	var IdAlmacen 	= $('#ID_ALMACEN').val();
	var AplicaColor = $('option:selected', $('#ID_TIPO_PRODUCTO')).attr('a');
	
	var CveColor	= $('#CVE_COLOR').val();
	var descColor	= $('#CVE_COLOR :selected').text();
	
	if(AplicaColor=='F'){
		CveColor	= '';
		descColor	= '';
	}
	
	if(fValDup(idProducto)){
		alert('El producto (' + idProducto + ' - ' + descProducto + ') Ya ha sido registrado'); 
		return; 
	}

	$.ajax({
		type: "GET",
		url: "DynAppsSrv",
		data: {
			DYN_CTL_CVE_OPERACION:"DYN_OPERATION",
			DYN_CTL_OPERATION:'ALTA_COMPRA_OP_DET',
			TIPO_OPERACION: vTipoOper, 
			ID_MOVIMIENTO: idMovimiento, 
			ID_PRODUCTO: idProducto, 
			CVE_COLOR: CveColor, 
			CANTIDAD: vCantidad, 
			PRECIO: vPrecio
		},
		dataType: "json",
		success: function(data) {
			if(data['Id']){
				var vIdDetalle = data['Id'][0].ID;

				$('<tr id="rowMovto' + vIdDetalle + '">\n' + 
					'<td><div align="right">' + vIdDetalle + '</div></td>\n' + 
					'<td><div align="right" class="cidprod">' + idProducto + '</div></td>\n' +
					'<td>' + descProducto + '</td>\n' + 
					'<td>' + descColor + '</td>\n' + 
					'<td><div align="right">' + addCommas(vCantidad) + '</div></td>\n' + 
					'<td><div align="right">' + addCommas(vPrecio) + '</div></td>\n' + 
					'<td><a href="#" title="Cancelar" onClick="JavaScript:updateTransac(getId(this),\'CA\');"><span class="red"><i class="fa fa-times bigger-160" aria-hidden="true"></i></span>' +
					'</a></td>\n' + 
					'<td></td></tr>').prependTo('#tableToModify > tbody:first');
			}else{
				alert(data);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log("Hay error grave");
			console.log(xhr.status);
			console.log(thrownError);
			console.log(xhr.responseText); 
			if(xhr.status=403){
				$('#idResult').html('<font color="red"><b>Su sesi&oacute;n ha caducado</b></font>');
				$('#msgResult').modal('show');
				//window.location.assign("index.jsp");
			}
      }
	});
}); 

function isInt(value){
 if($.isNumeric(value)){
  if(value > 0){
	if((value.indexOf(' ') >= 0)||value.indexOf('.') >= 0||value.indexOf('-') >= 0){
	 alert('Caracter no válido');
	  return false;
	}else{
	 if(value % 1 === 0){
	  return true;
	 }else{
	  alert('Capture un número entero');
	  return false;
	 }
	}
   }else{
	alert('El valor debe ser mayor a 0');
	return false;
   }
  } else{
  alert('Debe capturar un número válido');
  return false;
 }
}

function aplicaMovto(IdRow){
	
	var $ID_EMPRESA		= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $ID_NOTA        = $("#ID_NOTA").val();
	var $ROW 			= $('#'+IdRow);
	var $ID_PRODUCTO	= $ROW.find('td:eq(1)').text();
	var $CVE_COLOR		= $ROW.find('td:eq(3)').find("input:hidden").val();
	var $EXISTENCIA		= $ROW.find('td:eq(4)').text().replace(',', ''); 
	var $CANTIDAD		= $ROW.find('td:eq(5)').find("input:text").val();
	
	if(isInt($CANTIDAD)){
		
		if(confirm('¿Desea aplicar el movimiento?')){console.log('Aplicar');}else{return;}
		$.ajax({
			type: "POST",
			url: "DynAppsSrv",
			data: {	CTL_CVE_OPERACION:"ALT_MOD_MOVTO_OTROS_PRODS",
				ID_EMPRESA:$ID_EMPRESA,
				ID_NOTA:$ID_NOTA,
				TIPO:'A',
				SIT_MOVIMIENTO:'IS',
				CVE_COLOR: $CVE_COLOR,
				CANTIDAD:$CANTIDAD, 
				ID_PRODUCTO:$ID_PRODUCTO},
			dataType: "json",
			success: function(data) {
				if($.isNumeric(data)){
					$ROW.find('td:eq(0)').append('<div align="right">' + data + '</div>');
					$ROW.find('td:eq(5)').find("input:text").remove();
					$ROW.find('td:eq(5)').append('<div align="right">' + $CANTIDAD + '</div>');
					$ROW.attr('id','rowMovto' + data);
					$ROW.find('td:eq(6)').find("a:first").remove();
					$ROW.find('td:eq(6)').append('<a href="#" title="A Inventario" onClick="JavaScript:updateTransac(getId(this),\'IN\');"><span class="orange"><i class="fa fa-tag bigger-160" aria-hidden="true"></i></span></a><a href="#" title="Cerrar" onClick="JavaScript:updateTransac(getId(this),\'CL\');"><span class="gray"><i class="fa fa-lock bigger-160" aria-hidden="true"></i></span></a>');
					alert('Movimiento Aplicado');
				}else{
					alert(data);
				}
			},
			error: function(data){
				console.log('error handing here ' + data); 
			}
		});
	}
}

function updateTransac(IdRow, sitMovto){

	var $ROW = $('#'+IdRow);
	var $ID_MOVIMIENTO = $ROW.find('td:eq(0)').text();
	var rowDelete = document.getElementById(IdRow);

	if(!$.isNumeric($ID_MOVIMIENTO) && sitMovto=='CA'){console.log('Borra el renglón');rowDelete.parentNode.removeChild(rowDelete);return;}
	if(!$.isNumeric($ID_MOVIMIENTO) && (sitMovto=='AC'||sitMovto=='CL')){console.log('No encuentra');alert('No se pudo identificar el movimiento');return;}

	if(sitMovto=='CL'){if(confirm('¿Desea cerrar el movimiento?')){console.log('Cerrar');}else{return;}}
	if(sitMovto=='CA'){if(confirm('¿Desea eliminar el movimiento?')){console.log('Eliminar');}else{return;}}
	if(sitMovto=='IN'){if(confirm('¿Desea colocar el producto en Inventario?')){console.log('Eliminar');}else{return;}}

	var $ID_EMPRESA	= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $ID_NOTA    = $("#ID_NOTA").val();
	
	var AplicaColor = $('option:selected', $('#ID_TIPO_PRODUCTO')).attr('a');
	
	var CveColor	= $('#CVE_COLOR').val();
	var descColor	= $('#CVE_COLOR :selected').text();
	
	if(AplicaColor=='F'){
		CveColor	= '';
		descColor	= '';
	}

	$.ajax({
		type: "POST",
		url: "DynAppsSrv",
		data: {	CTL_CVE_OPERACION:"ALT_MOD_MOVTO_OTROS_PRODS",
				ID_EMPRESA:$ID_EMPRESA,
				ID_NOTA:$ID_NOTA,
				TIPO:'M',
				SIT_MOVIMIENTO:sitMovto,
				CVE_COLOR: CveColor,
				ID_MOVIMIENTO:$ID_MOVIMIENTO},
		dataType: "json",
		success: function(data) {
				console.log('Resultado: ' + data);
			if($.isNumeric(data)){
				//console.log('Si es numerico');
				if(sitMovto=='CA'){
					alert('Operación Cancelada');
					rowDelete.parentNode.removeChild(rowDelete);
					return;
				}else{
					if(sitMovto=='CL'){
						alert('Operación Cerrada');
						$ROW.find('td:eq(6)').children('a').remove();
						//$ROW.find('td:eq(6)').find("a:first").remove();
						if(data=='1'){
							$ROW.find('td:eq(6)').append('<a href="#" class="openTransac" title="Cancelar" onClick="JavaScript:updateTransac(getId(this),\'CA\');"><span class="blue"><i class="fa fa-times bigger-160" aria-hidden="true"></i></span></a>');
						}
						return;
					}else{
						if(sitMovto=='IS'){
							alert('Operación Abierta');
							$ROW.find('td:eq(6)').find("a:first").remove();
							$ROW.find('td:eq(6)').append('<a href="#" title="Cancelar" onClick="JavaScript:updateTransac(getId(this),\'CA\');"><span class="red"><i class="fa fa-times bigger-160" aria-hidden="true"></i></span></a><a href="#" title="Cerrar" onClick="JavaScript:updateTransac(getId(this),\'CL\');"><span class="blue"><i class="fa fa-lock bigger-160" aria-hidden="true"></i></span></a>');
						}else{
							if(sitMovto=='IN'){
								alert('El producto está disponible en Inventario');
								$ROW.find('td:eq(6)').children('a').eq(1).remove();
							}
						}
					}
				}
			}else{
				alert(data);
			}
		},
		error: function(data){
			console.log('error handing here'); 
		}
	});
}


$(document).ready(function() {
	console.log('Entra a la clase js otros prods --- '); 
	//$('.fa-check').attr( 'title', 'Aceptar');
	//$('.icon-remove').attr( 'title', 'Cancelar');
	$( ".remove" ).click(function() {
		updateTransac(getId(this),'CA');
	});
	$( ".inventario" ).click(function() {
		updateTransac(getId(this),'IN');
	});
	$( ".closed" ).click(function() {
		updateTransac(getId(this),'CL');
	});
	
	$( "#dynIdID_PROVEEDOR").html('<div class="space-8"></div>\n' + 
'<div class="space-8"></div>\n' + 
'<table id="tableToModify" class="table table-striped table-bordered">\n' + 
' <thead align="right">\n' + 
'  <tr id="encabezado">\n' + 
'   <th style="width: 5%;"><div align="right">Id</div></th>\n' + 
'   <th style="width: 10%;"><div align="right">Id Producto</div></th>\n' + 
'   <th style="width: 30%;"><div align="center">Producto</div></th>\n' + 
'   <th style="width: 10%;"><div align="center">Color</div></th>\n' + 
'   <th style="width: 10%;"><div align="right">Cantidad</div></th>\n' + 
'   <th style="width: 10%;"><div align="right">Precio</div></th>\n' + 
'   <th style="width: 10%;"><div align="center">Acción</div></th>\n' + 
'   <th style="width: 15%;"></th>\n' + 
'  </tr>\n' + 
' </thead>\n' + 
' <tbody>\n' + 
' </tbody>\n' + 
'</table>');
	
	
	$( "#tableToModify" ).insertAfter( "#divButtons" );
	$( "#dCVE_COLOR" ).hide();
	
	$('#ID_TIPO_PRODUCTO').change(function(event) {  
		var $id_tipo = $('option:selected', $('#ID_TIPO_PRODUCTO')).attr('a');
        console.log('Atributo = ' + $id_tipo);
		if(!$id_tipo){
			$id_tipo = 'F';
		}
        if($id_tipo=='V'){
            $('#dCVE_COLOR').show();
        }else{
            $('#dCVE_COLOR').hide();
        }
	});
	
	//If it is update
	if($('[name="PARAM_CTL_OPERACION"]').val()=='M'){
		
	}
	

}); 