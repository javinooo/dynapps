$(document).ready(function() {

  function inputAtts()
  {
	// Se formatean los inputs
    $(".cnt").each(function() {
		var vname = $(this).attr('id');
		var vTooltip = 'H: ' + vname.split("|")[2] + ' - V: ' + vname.split("|")[3];
		$(this).attr( 'title', vTooltip );
		if($(this).val()){$(this).css({"background-color": "#FF6600"});$(this).css({"text-align": "center"});$(this).css({"color": "#00008A"});
		}else{$(this).css({"background-color": "white"});$(this).css({"text-align": "center"});}
	});
	

	// Se deifinen los eventos
$('input.cnt').focus(function() {
        input = $(this);
        if ($(this).val() != null) {
            cantidadAnterior = $(this).val();
        }
    }).change(function() {
		
  if($(this).val() || (cantidadAnterior != null) ){
	var $Elem = $(this);
    var valor = $Elem.val();
	var lastVal = cantidadAnterior;
	if(!$(this).val()){
		  valor = '0';
	  }

	var $vId   = $Elem.attr('id');
    console.log('tiene un valor >' + valor + '< valor anterior: ' + cantidadAnterior);
	
    if($.isNumeric(valor) && valor > 0 && valor == parseInt(valor, 10)){

	var $ID_EMPRESA			= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
	var $ID_ALMACEN			= $('#ID_ALMACEN').val();
	var $ID_PRODUCTO_BASE  	= $vId.split("|")[0];
	var $ID_SERIE  			= $vId.split("|")[1];
	var $ID_VALOR_HORIZ  	= $vId.split("|")[2];
	var $ID_VALOR_VERT  	= $vId.split("|")[3];
	
	if($ID_VALOR_HORIZ.charAt(0) == '.') 
			$ID_VALOR_HORIZ = '0'+$ID_VALOR_HORIZ;
	if($ID_VALOR_VERT.charAt(0) == '.') 
			$ID_VALOR_VERT = '0'+$ID_VALOR_VERT;

		console.log('antes de la llamada ');
		//var datastring = $("#formValidate").serialize(); $.ajaxSetup({async: false});
		$.ajax({
            type: "POST",
            url: "OpticsSrv",
			async: false,
            data: {	CTL_CVE_OPERACION:"ACTUALIZA_MINIMO_INVENTARIO",
					ID_EMPRESA:$ID_EMPRESA, 
					ID_ALMACEN:$ID_ALMACEN, 
					ID_PRODUCTO_BASE:$ID_PRODUCTO_BASE, 
					ID_SERIE:$ID_SERIE,
					ID_VALOR_HORIZ:$ID_VALOR_HORIZ,
					ID_VALOR_VERT:$ID_VALOR_VERT,
					CANTIDAD:valor},
            dataType: "json",
            success: function(data) {

					console.log('Resultado: >' + data + '<');
					if(!$.isNumeric(data)){
						$Elem.val(lastVal);
						$( ".alertMsg" ).each(function() {
							$(this).text( '');
							$(this).addClass( "alert alert-danger" );
							$(this).text(data);
							$(this).css( "display", "block" ).fadeOut( 20000 );
						});
						console.log('Mensaje de error');
					}else{
						console.log('data: ' + data); 
						if(valor=='0'){
							$Elem.val('');
						}
						if($Elem.val()){$Elem.css({"background-color": "#FF6600"});$Elem.css({"text-align": "center"});$Elem.css({"color": "#00008A"});
						}else{$Elem.css({"background-color": "white"});$Elem.css({"text-align": "center"});}
						
						console.log('success: ' + data);
						console.log('success cantidadAnterior: ' + lastVal);
					}
            },
            error: function(data){
					console.log('Error cantidadAnterior: ' + lastVal);
					$Elem.val(lastVal);
					console.log('error handing here'); 
					$( ".alertMsg" ).each(function() {
						$(this).addClass( "alert alert-danger" );
						$(this).text( data );
						$(this).css( "display", "block" ).fadeOut( 20000 );
					});
            }
        });

    }else{
       console.log('El valor no es numérico');
	   $Elem.val(cantidadAnterior);
	   if($Elem.val()){$Elem.css({"background-color": "#FF6600"});$Elem.css({"text-align": "center"});$Elem.css({"color": "#00008A"});
		}else{$Elem.css({"background-color": "white"});$Elem.css({"text-align": "center"});}
		$( ".alertMsg" ).each(function() {
			$(this).addClass( "alert alert-danger" );
			$(this).text( "Solo estan permitidos valores numéricos enteros positivos" );
			$(this).css( "display", "block" ).fadeOut( 20000 );
		});
    }

  }

});
	
} // termina el inputAtts

$('#ID_ALMACEN').change(function() {
	
	$('#ID_PRODUCTO_BASE').trigger('change');
	
}); //   termina change select 



$('#ID_PRODUCTO_BASE').change(function() {
	
		if($(this).val()){
	
			var valorSelectNuevo 	= $(this).val();
			var IdEmpresa 			= $('[name="PARAM_CTL_ID_EMPRESA"]').val();
			var $ID_ALMACEN			= $('#ID_ALMACEN').val();
			var $select 			= $(this);

			if($ID_ALMACEN){
				$.ajax({
					type: "POST",
					url: "OpticsSrv",
					data: {
						ID_EMPRESA: IdEmpresa,
						ID_ALMACEN:$ID_ALMACEN, 
						CTL_CVE_OPERACION: "ACTUALIZA_SERIES_MIN_INV",
						ID_PRODUCTO_BASE: valorSelectNuevo
					},
					dataType: "json",
					success: function(data) {
						var divCantidad = $("#DynTID_SERIE");
						divCantidad.html( data);
						inputAtts();
						
					},
					error: function() {
						console.log("Hay error grave");
					}
				});
			}else{
				alert('Elija un Almacén y un producto')
			}
		}else{
			var divCantidad = $("#DynTID_SERIE");
			divCantidad.html( '');
		}
    }); //   termina change select 


	inputAtts();
	
/************************************************************************************************************************/
window.addEventListener('paste', function (event) {
	
	var $input = $(':focus');
	var vId = $input.attr('id');
	var idColIni = vId.split("|")[4];
	var idColAct = vId.split("|")[4];
	var vTr = $input.closest('tr');
	var vTrId = $input.closest('tr').attr('id');
    //var clipText = window.clipboardData.getData('Text');
	var clipText = event.clipboardData.getData('text');
	
    // split into rows
    clipRows = clipText.split(String.fromCharCode(13));

    for (i=0; i<clipRows.length; i++) {
    	clipRows[i] = clipRows[i].split(String.fromCharCode(9));
    }

    for (i=0; i<clipRows.length - 1; i++) {

	if(i>0){
			vTr = vTr.closest('tr').next('tr');
			$input = vTr.find("td:first input");
			//console.log('>> esta es el proximo renglon: ' + vTr.attr('id'));
			for (x=0; x<idColIni -1; x++) {
				$input = $input.closest('td').next().find('input'); 
			}
		}
    	for (j=0; j<clipRows[i].length; j++) {
			
			//console.log('>> val: ' + clipRows[i][j]);
			$input.focus();
			$input.val(clipRows[i][j]);
			$input.trigger( "change" );
			//console.log('>> input1: ' + $input.val());
			$input = $input.closest('td').next().find('input'); 
			//console.log('>> Actualiza');
    	}
    }

	event.preventDefault();
});	

/******/

}); 

