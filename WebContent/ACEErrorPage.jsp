<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
<meta charset="utf-8" />
<title>Mensajes del Sistema</title>

<meta name="description" content="and Validation" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
 
<!-- basic styles -->

<link href="assAceV134/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="assAceV134/css/font-awesome.min.css" />

<!--[if IE 7]>
  <link rel="stylesheet" href="assAceV134/css/font-awesome-ie7.min.css" />
<![endif]-->

<!-- fonts -->

<link rel="stylesheet" href="assAceV134/css/ace-fonts.css" />

<!-- ace styles -->

<link rel="stylesheet" href="assAceV134/css/ace.min.css" />
<link rel="stylesheet" href="assAceV134/css/ace-rtl.min.css" />
<link rel="stylesheet" href="assAceV134/css/ace-skins.min.css" />

<!--[if lte IE 8]>
  <link rel="stylesheet" href="assAceV134/css/ace-ie.min.css" />
<![endif]-->

<!-- inline styles related to this page -->

<!-- ace settings handler -->

<script src="assAceV134/js/ace-extra.min.js"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

<!--[if lt IE 9]>
<script src="assAceV134/js/html5shiv.js"></script>
<script src="assAceV134/js/respond.min.js"></script>
<![endif]-->
</head>

<body>

<!--NAV_BAR-->
<!--SIDE_BAR-->


<!--BREAD_CRUMBS-->

 <div class="page-content">        

  <div class="page-header">
   <h1>&nbsp;</h1>
  </div><!-- /.page-title -->
  
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-4">

	<div class="widget-box">
		<div class="widget-header center">
	   <h4>Mensajes de Error</h4>
	   
	  </div>
	   <div class="widget-body">
	    <div class="widget-main">


<div class="alert alert-danger center"><strong><i class="icon-remove"></i>Error!</strong><br /><br /><br />
		<%=request.getAttribute("MSG_ERROR") %>
</div>
 <div class="clearfix form-actions">
  <div class="center">
  
    
    
    <% if(request.getParameter("ID_LOG_ERROR_ALTA_ENTIDAD") == null) { 
            
            	System.out.println("EL PARAMETRO SI ES NULO EN LA JSP");
            %>
            
            	<% if(request.getParameter("SinSesion") != null && request.getParameter("SinSesion").equals("V")) { %>
					<button type="button" onClick="window.close()" class="btn btn-red"><i class="icon-remove bigger-125"></i>Cerrar Ventana</button>
  				<% }else{ %>
            		<button type="button" onClick="javascript:history.go(-1);" class="btn btn-grey"><i class="icon-arrow-left bigger-125"></i>Regresar</button>
            	<% } %>
            	
			<% }else{ 
			
				System.out.println("EL PARAMETRO NO ES NULO EN LA JSP");
				%>
			
	    		<form name="frmRecuperaDatos" action="DinamicapsSrv" method="get">
					<button type="submit" class="btn btn-grey"><i class="icon-arrow-left bigger-125"></i>Aceptar</button>
					<input name="PARAM_CTL_PANTALLA" value="<%=request.getParameter("PARAM_CTL_PANTALLA")%>" type="hidden">
	                <input name="PARAM_CTL_EJECUTA" value="Ejecutar" type="hidden">
	                <input name="PARAM_CTL_ID_EMPRESA" value="<%=request.getParameter("PARAM_CTL_ID_EMPRESA")%>" type="hidden">
	                <input name="PARAM_CTL_ID_TABLA" value="<%=request.getParameter("PARAM_CTL_ID_TABLA")%>" type="hidden">
	                <input name="NOM_ENTIDAD" value="<%=request.getParameter("NOM_ENTIDAD")%>" type="hidden">
	                <input name="NOM_OBJETO" value="<%=request.getParameter("NOM_OBJETO")%>" type="hidden">
	                <input name="ID_EJECUTABLE" value="<%=request.getParameter("ID_EJECUTABLE")%>" type="hidden">
	                <input name="ID_LOG_ERROR_ALTA_ENTIDAD" value="<%=request.getParameter("ID_LOG_ERROR_ALTA_ENTIDAD")%>" type="hidden">
	                <input name="PARAM_CTL_CVE_NAVEGACION" value="<%=request.getParameter("PARAM_CTL_CVE_NAVEGACION")%>" type="hidden">
	                <input name="PARAM_CTL_OPERACION" value="C" type="hidden">
				</form>
           	<% } %>
    
    
  </div>
 </div>
 
 
  </div> </div> </div>
 

    </div><!-- /.col -->
    <div class="col-xs-4">
    </div>
   </div><!-- /.row -->
  </div><!-- /.page-content -->
 

<!--SETTINGS-->

  </div><!-- /.main-container-inner -->

  <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
  </a>
 </div><!-- /.main-container -->

<input id="GLBX_ID_MENU" value="@PARAM_CTL_ID_MENU" type="hidden">

<!-- basic scripts -->

<!--[if !IE]> -->

<script type="text/javascript">
    window.jQuery || document.write("<script src='assAceV134/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assAceV134/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->


 <script type="text/javascript">
  if("ontouchend" in document) document.write("<script src='assAceV134/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
 </script>
 
 <script src="assAceV134/js/bootstrap.min.js"></script>

 <!-- page specific plugin scripts -->

 <!-- ace scripts -->


 <!-- inline scripts related to this page -->
 
</body>
</html>
