package com.dinamicaps.web;


import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dinamicaps.web.impl.Servicio;
import com.dinamicaps.web.impl.domain.Atributo;
import com.dinamicaps.web.impl.domain.BrowserType;
import com.dinamicaps.web.impl.domain.Buttons;
import com.dinamicaps.web.impl.domain.Ejecutable;
import com.dinamicaps.web.impl.domain.Navegacion;
import com.dinamicaps.web.impl.domain.NavegacionNext;
import com.dinamicaps.web.impl.domain.Pantalla;
import com.dinamicaps.web.impl.domain.Usuario;


import com.google.gson.Gson;
import com.google.gson.JsonArray;

/**
 * Servlet implementation class DynAppsSrv
 */
public class DynAppsSrv extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DynAppsSrv() {
        super();
        // TODO Auto-generated constructor stub
    }
 
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    //@RequestMapping(method=RequestMethod.GET, produces={"application/json; charset=UTF-8"})
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        
        String sCtlCveOperacion = request.getParameter("DYN_CTL_CVE_OPERACION"); 
        String sDynCtlTipoOperacion = request.getParameter("DYN_CTL_TIPO_OPERACION"); 
        
        if(request.getParameter("AP_PATERNO")!=null){
        	String sTest = new String(request.getParameter("AP_PATERNO").getBytes("ISO-8859-1"),"UTF-8");
        	System.out.println(" sTest:  " + sTest);
        }
		
		// Objetos
        Usuario usuario	= null;
        int iIdEmpresa	= 0;
        boolean lbDebug	= false;

		// Se inicializa el sevicio de conexi�n
        Servicio servicio 	= new Servicio();

        //Obtain the session object
        HttpSession session = request.getSession(false);
        
        /*******************         CONTROL DE SESI�N DEL USUARIO        *****************************/
        
        if(session == null){
	        response.setContentType("text/html");
			request.getRequestDispatcher("index.jsp").forward(request,response);
        	return;
        }else{
        	System.out.println(" La sesion si existe  ");
        	usuario 			= (Usuario) session.getAttribute("Usuario");
        	
        	if(usuario != null){
        		
    	        // Se obtinenen los par�metros necesarios para ejecutar la respuesta  
    	        iIdEmpresa 	= usuario.iIdEmpresa;
    	        lbDebug 	= servicio.bDebug(iIdEmpresa, usuario.sCveUsuario);

    	        if(lbDebug){
    	        	System.out.println(" El usuario no es nulo -  Antes del control de sesi�n ");
    	        }
        	}else{
        		System.out.println(" El usuario NO existe  ");
        		//System.out.println(" El usuario es nulo **** ");
            	// Se verifica que hayan enviado los par�metros para realizar el login
            	if(request.getParameter("login") == null || request.getParameter("password") == null){
            		//System.out.println(" No llegan los datos de login **** ");
            		//response.sendRedirect(request.getContextPath() + "/index.jsp");
            		//response.sendRedirect("www.google.com");
            		System.out.println(" NO vienen los parametros de login ");
            		System.out.println(" request.getContextPath():  " + request.getContextPath());
            		session.invalidate();
            		try {
            			
            		       response.setStatus(403);
            		    } catch (Throwable t) {
            		        //do something
            		    	t.printStackTrace(); 

            		    }
            		return; 
            	}else{
            		System.out.println(" SI vienen los parametros de login:  " + request.getParameter("login") + " Password: " + request.getParameter("password"));
            		// Se genera la sesi�n
    	        	session = request.getSession(true);
    	        	//System.out.println(" Se genera la sesi�n **** ");

    	        	// Se verifica que la interface est� compilada
    	        	//System.out.println(" servicio.CompilaInterface 3 **** " + servicio.sCompilaInterface(false));        
    	        	// Se obtiene el usuario y se valida el password
    	        	usuario = servicio.getUsuario(request.getParameter("login"), request.getParameter("password"), request.getParameter("usuario_evento"), false);

    	        	if(usuario != null && usuario.sCveUsuario.equals("ERROR_EN_SYSTEMA")){
    	        		if(lbDebug){
    	        			System.out.println(" Error al obtener el usuario ");
    	        		}
            			request.setAttribute("MSG_ERROR", "El servicio no est� disponible, por favor comun�quese con el administrador");
        		        response.setContentType("text/html");
        				request.getRequestDispatcher("index.jsp").forward(request,response);
                    	return;
    	        	}

    	        	//System.out.println(" Se obtiene el usuario  **** ");

    	        	// Verifica si el usuario captur� el password correcto
    	        	if(usuario != null && usuario.sPasswordValido.equals("V")){

            			if(usuario.sSettings.equals("V")){
                			request.setAttribute("MSG_ERROR", "Por favor actualice su versi�n del sistema");
            		        response.setContentType("text/html");
            				request.getRequestDispatcher("index.jsp").forward(request,response);
                        	return;
            			}

            	        // Se obtinenen los par�metros necesarios para ejecutar la respuesta  
            	        iIdEmpresa 		= usuario.iIdEmpresa;
            	        lbDebug 		= servicio.bDebug(iIdEmpresa, usuario.sCveUsuario);
            	        
            			session.setAttribute("Usuario", usuario);

            			if(lbDebug){
            				System.out.println(" Password correcto, se agrega el usuario a la sesi�n  **** ");
            			}
            			
            			// set another default locale
            		    Locale.setDefault(new Locale("en", "US"));
            		    
    	        	}
    	        	else{
    	        		if(usuario != null && usuario.sPasswordValido.equals("F")){
    	        			if(usuario.sSettings.equals("V")){
    	            			request.setAttribute("MSG_ERROR", "Por favor actualice su versi�n del sistema");
    	        		        response.setContentType("text/html");
    	        				request.getRequestDispatcher("index.jsp").forward(request,response);
    	                    	return;
    	        			}
    		        		// Si el password es incorrecto se redirecciona a la pantalla de login
                			request.setAttribute("MSG_ERROR", "Password Incorrecto");
            		        response.setContentType("text/html");
            				request.getRequestDispatcher("index.jsp").forward(request,response);
                        	return;
    	        		}else{
    	        			if(usuario==null){	
    	            			request.setAttribute("MSG_ERROR", "El usuario no existe");
    	        		        response.setContentType("text/html");
    	        				request.getRequestDispatcher("index.jsp").forward(request,response);
    	                    	return;
    	        			}
    	        		}
    	        	}
            	}
        	}
        }

        if(lbDebug){
			System.out.println(" PASA LA VERIFICACI�N DE USUARIO   **** ");
		}
        
        /**********************     OBTIENE LOS PARAMETROS DEL REQUEST     ********************************************************/
        
        HashMap<String, Object> map 		= new HashMap<String, Object>();
        HashMap<String, Object> mapInterno 	= null;
		Enumeration<String> parameterNames 	= request.getParameterNames();
		String paramValue					= ""; 
		String paramName 					= "";
		
        while (parameterNames.hasMoreElements()) {
            
        	paramName 				= parameterNames.nextElement();
            String[] paramValues 	= request.getParameterValues(paramName);
            if(lbDebug){
    			System.out.println(" En Servlet llega:  " + paramName + " Longitud: " + paramValues.length);
    		}
            
            if(paramValues.length == 1){
        		map.put(paramName, paramValues[0]);
            	//map.put(paramName, new String(paramValues[0].getBytes("ISO-8859-1"),"UTF-8"));
            	if(!paramValues[0].equals("")){
	            	if(lbDebug){
	        			System.out.println(" En Servlet agrega:  " + paramName + " Valor: " + new String(paramValues[0].getBytes("ISO-8859-1"),"UTF-8"));
	        		}
            	}else{
            		if(lbDebug){
	        			System.out.println(" No tiene valor  " + paramName + " Valor: >" + new String(paramValues[0].getBytes("ISO-8859-1"),"UTF-8") + "<");
	        		}
            		if(paramName.equals("DYN_CTL_VAL_INPUT_FILTER")){
            			//map.put(paramName, "");
            			if(lbDebug){
    	        			System.out.println(" ES DYN_CTL_VAL_INPUT_FILTER:  ");
    	        		}
            		}
            	}
            }else{
            	mapInterno 		= new HashMap<String, Object>();
            	for (int i = 0; i < paramValues.length; i++) {
            		if(!paramValues[i].equals("")){
	            		mapInterno.put(String.valueOf(i), paramValues[i]);
            			//mapInterno.put(String.valueOf(i), new String(paramValues[i].getBytes("ISO-8859-1"),"UTF-8"));
	            		if(lbDebug){
	            			System.out.println(" En Servlet agrega interno:  " + String.valueOf(i) + " Valor: " + new String(paramValues[i].getBytes("ISO-8859-1"),"UTF-8"));
	            		}
            		}
		        }
            	map.put(paramName, mapInterno);
            }
        }
        map.put("DYN_CTL_USUARIO", usuario);
        map.put("DYN_CTL_BDEBUG", lbDebug); 
        map.put("DYN_CTL_ID_EMPRESA",String.valueOf(usuario.iIdEmpresa));
        map.put("DYN_CTL_USUARIO_SCHEMA", usuario.sUserSchema);
        
        if(lbDebug){
			System.out.println(" En Servlet usuario.sUserSchema:  " + usuario.sUserSchema);
		}

        /****************************************************************************************************************************/
        
        
        String sResponse = ""; 
        
        if(lbDebug){
			System.out.println("Charset.defaultCharset() --->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + Charset.defaultCharset());
		}
        
        switch (sCtlCveOperacion){
        	case "GET_MENU": 
        		sResponse = servicio.sGetMenu(iIdEmpresa, usuario.sCveUsuario, usuario.sUserSchema, usuario.sCveMasterTemplate, usuario.iIdRol, usuario.sGenero, request.getParameter("DYN_CTL_IS_MOBILE"), lbDebug);
        		break;
        	case "GET_SEARCH_FORM": 
        		sResponse = servicio.sGetPantallaJQ(map); 
        		break;
        	case "GET_RS": 
        		sResponse = servicio.sGetTableRs(map); 
        		break; 
        	case "GET_RECORD": 
        		sResponse = servicio.sGetRecord(map); 
        		break; 
        	case "UPDATE_RECORD": 
        		sResponse = servicio.sUpdateRecord(map); 
        		break; 
        	case "GET_INSERT_FORM": 
        		sResponse = servicio.sGetInsertForm(map); 
        		break; 
        	case "INSERT_RECORD": 
        		sResponse = servicio.sInsertRecord(map); 
        		break; 
        	case "DELETE_RECORD": 
        		sResponse = servicio.sDeleteRecord(map); 
        		break; 
        	case "GET_COMBO_OPTIONS":
        		sResponse = servicio.sGetSelectOptions(map); 
        		break; 
        	case "GET_COMBO_OPTIONS_FILTERED":
        		sResponse = servicio.sGetSelectOptions(map); 
        		break; 
        	case "DYN_OPERATION":
        		sResponse = servicio.sDynOperation(map); 
        		break;
        }
        
        //response.setHeader("Content-Type", "application/json; charset=ISO-8859-1");
        response.setHeader("Content-Type", "application/json; charset=utf-8");
        //response.setContentType("application/json");
		response.setHeader("Cache-Control", "nocache");

       	/*
       	String jsonString = new Gson().toJson(sResponse);
       	byte[] utf8JsonString = jsonString.getBytes("UTF8");
       	responseToClient.write(utf8JsonString, 0, utf8JsonString.Length);
       	
       	
       	String json = new Gson().toJson(sResponse);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
       	*/
       	
       	
       	//response.setContentType("application/json;charset=utf-8");
        //response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		//out.write(utf8JsonString, 0, utf8JsonString.length);
		out.println(sResponse); 
		out.close();
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

        // Objetos
        Usuario usuario							= null;
 
        // Variables
        int iIdEmpresa;
        boolean lbDebug							= false;

        // Se inicializa el sevicio de conexi�n
        Servicio servicio 						= new Servicio();

        //Obtain the session object
        HttpSession session = request.getSession(false);
        
        /*if(session == null){
        	System.out.println(" La sesi�n es nula en POST");
        	response.sendRedirect(request.getContextPath() + "/index.jsp");
        	return;
        }*/
        
        /*******************         CONTROL DE SESI�N DEL USUARIO        *****************************/
        
        //Check if our session variable is set, if so, get the session variable value
        //which is an Integer object, and add one to the value.
        //If the value is not set, create an Integer object with the default value 1.
        //Add the variable to the session overwriting any possible present values.
        if(session != null){
        	System.out.println(" La sesion no es nula ***************************************** ");
        	usuario 			= (Usuario) session.getAttribute("Usuario");
        	
        	if(usuario != null){
        		System.out.println(" El usuario NO es  nulo ***************************************** ");
    	        // Se obtinenen los par�metros necesarios para ejecutar la respuesta  
    	        iIdEmpresa 	= usuario.iIdEmpresa;
    	        lbDebug 	= servicio.bDebug(iIdEmpresa, usuario.sCveUsuario);
    	        
    	        if(lbDebug){
    	        	System.out.println(" El usuario no es nulo -  Antes del control de sesi�n ");
    	        }
        	}else{
        		System.out.println(" El usuario SI es nulo ***************************************** ");
        	}
        }


        // Si no existe el usuario se redirecciona a la p�gina de login
        if (usuario == null) {
        	//System.out.println(" El usuario es nulo **** ");
        	// Se verifica que hayan enviado los par�metros para realizar el login
        	if(request.getParameter("login") == null || request.getParameter("password") == null){
        		//System.out.println(" No llegan los datos de login **** ");
        		if(request.getParameter("usuario_evento")!=null){
        			response.sendRedirect(request.getContextPath() + "/indexUser.jsp");
        		}else{
        			response.sendRedirect(request.getContextPath() + "/index.jsp");
        		}
        		return;
        	}else{

        		// Se genera la sesi�n
	        	session = request.getSession(true);
	        	System.out.println(" Se genera la sesi�n **** ");

	        	// Se verifica que la interface est� compilada
	        	//System.out.println(" servicio.CompilaInterface 3 **** " + servicio.sCompilaInterface(false));        
	        	// Se obtiene el usuario y se valida el password
	        	usuario = servicio.getUsuario(request.getParameter("login"), request.getParameter("password"), request.getParameter("usuario_evento"), false);
	        	
	        	if(usuario!=null){
	        		lbDebug 		= servicio.bDebug(usuario.iIdEmpresa, usuario.sCveUsuario);
	        	}

	        	System.out.println(" lbDebug **** " + lbDebug);

	        	if(usuario != null && usuario.sCveUsuario.equals("ERROR_EN_SYSTEMA")){
	        		if(lbDebug){
	        			System.out.println(" Error al obtener el usuario ");
	        		}
        			request.setAttribute("MSG_ERROR", "El servicio no est� disponible, por favor comun�quese con el administrador");
    		        response.setContentType("text/html");
    		        if(request.getParameter("usuario_evento")!=null){
    		        	request.getRequestDispatcher("indexUser.jsp").forward(request,response);
            		}else{
            			request.getRequestDispatcher("index.jsp").forward(request,response);
            		}
    				
                	return;
	        	}

	        	if(lbDebug){
	        		System.out.println(" Se obtiene el usuario  **** ");
	        	}

	        	// Verifica si el usuario captur� el password correcto
	        	if(usuario != null && usuario.sPasswordValido.equals("V")){
	        		
	        		if(lbDebug){
	        			System.out.println(" sPasswordValido = V  ");
	        		}

        			if(usuario.sSettings.equals("V")){
        				if(lbDebug){
        					System.out.println(" sSettings = V  ");
        				}
            			request.setAttribute("MSG_ERROR", "Por favor actualice su versi�n del sistema");
        		        response.setContentType("text/html");
        		        if(request.getParameter("usuario_evento")!=null){
        		        	request.getRequestDispatcher("indexUser.jsp").forward(request,response);
                		}else{
                			request.getRequestDispatcher("index.jsp").forward(request,response);
                		}
                    	return;
        			}

        	        // Se obtinenen los par�metros necesarios para ejecutar la respuesta  
        	        iIdEmpresa 		= usuario.iIdEmpresa;
        	        
        	        
        	        if(lbDebug){
        	        	System.out.println(" AGREGA ATRIBUTO USUARIO --------------  ");
        	        }
        			session.setAttribute("Usuario", usuario);

        			if(lbDebug){
        				System.out.println(" Password correcto, se agrega el usuario a la sesi�n  ////////////////////// **** ");
        			}
        			
        			// set another default locale
        		    Locale.setDefault(new Locale("en", "US"));
        		    
	        	}
	        	else{
	        		
	        		if(usuario != null && usuario.sPasswordValido.equals("F")){
	        			if(lbDebug){
		        			System.out.println(" Password No es v�lido  ////////////////////// **** ");
		        		}
	        			if(usuario.sSettings.equals("V")){
	        				if(lbDebug){
	    	        			System.out.println(" Settings = V  ////////////////////// **** ");
	    	        		}
	            			request.setAttribute("MSG_ERROR", "Por favor actualice su versi�n del sistema");
	        		        response.setContentType("text/html");
	        		        if(request.getParameter("usuario_evento")!=null){
	        		        	request.getRequestDispatcher("indexUser.jsp").forward(request,response);
	                		}else{
	                			request.getRequestDispatcher("index.jsp").forward(request,response);
	                		}
	                    	return;
	        			}
		        		// Si el password es incorrecto se redirecciona a la pantalla de login
            			request.setAttribute("MSG_ERROR", "Password Incorrecto");
        		        response.setContentType("text/html");
        		        if(request.getParameter("usuario_evento")!=null){
        		        	request.getRequestDispatcher("indexUser.jsp").forward(request,response);
                		}else{
                			request.getRequestDispatcher("index.jsp").forward(request,response);
                		}
                    	return;
	        		}else{
	        			if(usuario==null){
	            			request.setAttribute("MSG_ERROR", "El usuario no existe");
	        		        response.setContentType("text/html");
	        		        if(request.getParameter("usuario_evento")!=null){
	        		        	request.getRequestDispatcher("indexUser.jsp").forward(request,response);
	                		}else{
	                			request.getRequestDispatcher("index.jsp").forward(request,response);
	                		}
	                    	return;
	        			}
	        		}
	        	}
        	}
        }
        
        
		if(lbDebug){
			System.out.println(" Password correcto, se agrega el usuario a la sesi�n  **** ");
		}
		

		usuario 			= (Usuario) session.getAttribute("Usuario");
    	
		String sHTMLCode = servicio.sGetHTML(usuario, lbDebug);
		    
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println(sHTMLCode); 
		out.close();
        
	}

}
