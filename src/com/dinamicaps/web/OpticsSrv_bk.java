package com.dinamicaps.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dinamicaps.web.impl.Servicio;
import com.dinamicaps.web.impl.domain.Usuario;
import com.dinamicaps.web.impl.domain.optics.OpticsDAO;
import com.google.gson.Gson;

/**
 * Servlet implementation class ReportsMerdiaSrv
 */
// @WebServlet("/ReportsMerdiaSrv")
public class OpticsSrv_bk extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public OpticsSrv_bk() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Entra al servlet -------------------------------");
		// processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// Obtain the session object, create a new session if doesn't exist
		HttpSession session = request.getSession(false);
		String sResultado = "";
		String[] sParametros = new String[2];
		boolean lbDebug = false;

		if (session == null) {
			// System.out.println(" La sesi�n es nula ");
			sResultado = "[\"0\",\"Su sesi�n ha caducado\"]";
			// sResultado = new Gson().toJson(sResultado);
			// System.out.println("SResultadoJson: " + sResultado);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(sResultado);
			return;
		}

		Usuario usuario = (Usuario) session.getAttribute("Usuario");
		Servicio servicio = new Servicio();

		if (usuario == null) {
			sParametros[0] = null;
			sParametros[1] = "Su sesi�n ha caducado";
			sResultado = new Gson().toJson(sParametros);
			// System.out.println("SResultadoJson: " + SResultado);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(sResultado);
			return;
		}

		lbDebug = servicio.bDebug(usuario.iIdEmpresa, usuario.sCveUsuario);

		String sCveOperacion = request.getParameter("CTL_CVE_OPERACION");

		if (lbDebug) {
			System.out.println("sCveOperacion -------------------------------" + sCveOperacion);
		}

		if (sCveOperacion == null) {

			return;
		} else {

			if (sCveOperacion.equals("ACTUALIZA_SERIES")) {

				OpticsDAO dao = new OpticsDAO();
				sResultado = "";

				if (lbDebug) {
					System.out.println("iIdProductoBase: " + request.getParameter("ID_PRODUCTO_BASE"));
					System.out.println("iIdMovimiento: " + request.getParameter("ID_MOVIMIENTO"));
				}

				int iIdProductoBase = Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				int iIdMovimiento = Integer.parseInt(((request.getParameter("ID_MOVIMIENTO") == null
						|| request.getParameter("ID_MOVIMIENTO").equals(""))

				? "0" : request.getParameter("ID_MOVIMIENTO")));

				if (lbDebug) {
					System.out.println("iIdProductoBase: " + iIdProductoBase);
					System.out.println("iIdMovimiento: " + iIdMovimiento);
				}

				try {

					dao.abreConexion();
					sResultado = dao.sGetSeries(usuario.iIdEmpresa, iIdProductoBase, iIdMovimiento, usuario.sCveUsuario,
							lbDebug);

					// System.out.println("SResultado directo: " + SResultado);
					sResultado = new Gson().toJson(sResultado);
					// System.out.println("SResultadoJson: " + SResultado);
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write(sResultado);
					return;
				} catch (Exception exc) {
					exc.printStackTrace();
				} finally {
					try {
						if (dao != null) {
							if (dao.conn != null) {
								dao.conn.close();
							}
						}
					} catch (Exception exc) {
						exc.printStackTrace();
					}
				}
			} else {
				if (sCveOperacion.equals("GENERA_PRE_MOVIMIENTO")) {
					if (lbDebug) {
						System.out.println("GENERA_PRE_MOVIMIENTO");
					}
					OpticsDAO dao = new OpticsDAO();
					sResultado = "";

					String sIdPreMovto = request.getParameter("ID_MOVIMIENTO");
					String sCveProducto = request.getParameter("CVE_PRODUCTO");
					int iCantidad = Integer.parseInt(request.getParameter("CANTIDAD"));
					String sTipoOperacion = request.getParameter("PARAM_CTL_OPERACION");
					String sTipoDevolucion = request.getParameter("TIPO_DEVOLUCION");
					int iIdDevolucion = Integer.parseInt(request.getParameter("ID_DEVOLUCION") == null ? "0"
							: request.getParameter("ID_DEVOLUCION"));
					String sTipoMovimiento = request.getParameter("TIPO_MOVIMIENTO");
					String sBSincroniza = request.getParameter("PARAM_CTL_B_SINCRONIZA");
					int iIdAlmacen = Integer.parseInt(
							(request.getParameter("ID_ALMACEN") == null ? "0" : request.getParameter("ID_ALMACEN")));
					int iIdNota = Integer
							.parseInt(request.getParameter("ID_NOTA") == null ? "0" : request.getParameter("ID_NOTA"));

					if (lbDebug) {
						System.out.println("iIdNota: " + iIdNota);
						System.out.println("iIdPreMovto: " + sIdPreMovto);
						System.out.println("sCveProducto: " + sCveProducto);
						System.out.println("iCantidad: " + iCantidad);
						System.out.println("sTipoOperacion: " + sTipoOperacion);
						System.out.println("sBSincroniza: " + sBSincroniza);
						System.out.println("iIdAlmacen: " + iIdAlmacen);
						System.out.println("sTipoDevolucion: " + sTipoDevolucion);
						System.out.println("iIdDevolucion: " + iIdDevolucion);
					}

					try {

						dao.abreConexion();

						sParametros = dao.sGeneraPreMovto(usuario.iIdEmpresa, sIdPreMovto, sCveProducto, iCantidad,
								sTipoOperacion, sTipoMovimiento, iIdAlmacen, sTipoDevolucion, usuario.sCveUsuario,
								iIdDevolucion, sBSincroniza, usuario.iIdRol, iIdNota, lbDebug);

						// sResultado = "{'idMovimiento':'"+ sParametros[0] +
						// "','sResultado':'" + sParametros[1] +"'}";

						// System.out.println("SResultado directo: " +
						// sResultado);
						sResultado = new Gson().toJson(sParametros);
						if (lbDebug) {
							System.out.println("SResultadoJson: " + sResultado);
						}
						response.setContentType("application/json");
						response.setCharacterEncoding("UTF-8");
						response.getWriter().write(sResultado);
						return;
					} catch (Exception exc) {
						exc.printStackTrace();

						String sError = exc.getMessage();
						if (sError.contains("ORA-20")) {
							sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
						} else {
							sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
						}
						sResultado = new Gson().toJson(sResultado);
						if (lbDebug) {
							System.out.println("SResultadoJsonError: " + sResultado);
						}
						response.setContentType("application/json");
						response.setCharacterEncoding("UTF-8");
						response.getWriter().write(sResultado);
						return;
					} finally {
						try {
							if (dao != null) {
								if (dao.conn != null) {
									dao.conn.close();
								}
							}
						} catch (Exception exc) {
							exc.printStackTrace();
						}
					}
				} else {
					if (sCveOperacion.equals("ALTA_MOD_COMPRA")) {
						if (lbDebug) {
							System.out.println("ALTA_MOD_COMPRA*********************************");
						}

						// , , FH_CREACION, FH_ULTIMA_MODIF, USUARIO_CREACION,
						// USUARIO_ULTIMA_MODIF, ID_REFERENCIA, SIT_MOVIMIENTO

						OpticsDAO dao = new OpticsDAO();

						sResultado = "";

						int iIdNota = Integer.parseInt(request.getParameter("ID_NOTA"));
						int iIdMovimiento = Integer.parseInt(request.getParameter("ID_MOVIMIENTO"));
						int iIdProductoBase = Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
						String sTxReferencia = request.getParameter("TX_REFERENCIA");
						String sSituacion = request.getParameter("SIT_MOVIMIENTO");
						String sTipoOperacion = request.getParameter("PARAM_CTL_OPERACION");

						if (lbDebug) {
							System.out.println("iIdNota: " + iIdNota);
							System.out.println("sIdMovimiento: " + iIdMovimiento);
							System.out.println("sIdProductoBase: " + iIdProductoBase);
							System.out.println("sTxReferencia: " + sTxReferencia);
							System.out.println("sUsuario: " + usuario.sCveUsuario);
							System.out.println("sSituacion: " + sSituacion);
							System.out.println("sTipoOperacion: " + sTipoOperacion);
						}

						try {

							dao.abreConexion();
							sResultado = dao.sAltaModEntrada(usuario.iIdEmpresa, iIdNota, iIdMovimiento, iIdProductoBase,
									sTxReferencia, usuario.sCveUsuario, sSituacion, sTipoOperacion, lbDebug);

							if (lbDebug) {
								System.out.println("SResultado directo: " + sResultado);
							}

							sParametros[0] = sResultado;
							sParametros[1] = null;
							sResultado = new Gson().toJson(sParametros);
							if (lbDebug) {
								System.out.println("SResultadoJson: " + sResultado);
							}

							// sResultado = "{'idMovimiento':'"+ sIdMovimiento +
							// "','sResultado':'" + sResultado +"'}";

							response.setContentType("application/json");
							response.setCharacterEncoding("UTF-8");
							response.getWriter().write(sResultado);
							return;

						} catch (Exception exc) {
							exc.printStackTrace();
							String sError = exc.getMessage();
							if (sError.contains("ORA-20")) {
								sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
							} else {
								sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
							}
							sParametros[0] = sResultado;
							sParametros[1] = "1";
							sResultado = new Gson().toJson(sParametros);
							response.setContentType("application/json");
							response.setCharacterEncoding("UTF-8");
							response.getWriter().write(sResultado);
							return;
						} finally {
							try {
								if (dao != null) {
									if (dao.conn != null) {
										dao.conn.close();
									}
								}
							} catch (Exception exc) {
								exc.printStackTrace();
							}
						}

					} else {
						if (sCveOperacion.equals("ACTUALIZA_SERIES_VTA")) {

							if (lbDebug) {
								System.out.println("iIdProductoBase: " + request.getParameter("ID_PRODUCTO_BASE"));
								System.out.println("iIdAlmacen: " + request.getParameter("ID_ALMACEN"));
								System.out.println("iIdMovimiento: " + request.getParameter("ID_MOVIMIENTO"));
							}

							OpticsDAO dao = new OpticsDAO();
							sResultado = "";

							int iIdProductoBase = Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
							int iIdAlmacen = Integer.parseInt(request.getParameter("ID_ALMACEN"));
							int iIdMovimiento = Integer.parseInt(((request.getParameter("ID_MOVIMIENTO") == null
									|| request.getParameter("ID_MOVIMIENTO").equals("")) ? "0"
											: request.getParameter("ID_MOVIMIENTO")));

							try {

								dao.abreConexion();
								sResultado = dao.sGetSeriesVta(usuario.iIdEmpresa, iIdProductoBase, iIdAlmacen, iIdMovimiento,
										usuario.sCveUsuario, lbDebug);

								// System.out.println("SResultado directo: " +
								// SResultado);
								sResultado = new Gson().toJson(sResultado);
								// System.out.println("SResultadoJson: " +
								// SResultado);
								response.setContentType("application/json");
								response.setCharacterEncoding("UTF-8");
								response.getWriter().write(sResultado);
								return;
							} catch (Exception exc) {
								exc.printStackTrace();
							} finally {
								try {
									if (dao != null) {
										if (dao.conn != null) {
											dao.conn.close();
										}
									}
								} catch (Exception exc) {
									exc.printStackTrace();
								}
							}
						} else {
							if (sCveOperacion.equals("ALTA_MOD_VENTA")) {
								if (lbDebug) {
									System.out.println("ALTA_MOD_VENTA*********************************");
								}

								OpticsDAO dao = new OpticsDAO();

								sResultado = "";

								int iIdMovimiento = Integer.parseInt(request.getParameter("ID_MOVIMIENTO"));
								int iIdNota = Integer.parseInt(request.getParameter("ID_NOTA"));
								int iIdProductoBase = Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
								String sTxReferencia = request.getParameter("TX_REFERENCIA");
								String sSituacion = request.getParameter("SIT_MOVIMIENTO");
								String sTipoOperacion = request.getParameter("PARAM_CTL_OPERACION");

								if (lbDebug) {
									System.out.println("sIdMovimiento: " + iIdMovimiento);
									System.out.println("iIdNota: " + iIdNota);
									System.out.println("sIdProductoBase: " + iIdProductoBase);
									System.out.println("sTxReferencia: " + sTxReferencia);
									System.out.println("sUsuario: " + usuario.sCveUsuario);
									System.out.println("sSituacion: " + sSituacion);
									System.out.println("sTipoOperacion: " + sTipoOperacion);
								}

								try {

									dao.abreConexion();
									sResultado = dao.sAltaModSalida(usuario.iIdEmpresa, iIdNota, iIdMovimiento, iIdProductoBase,
											sTxReferencia, usuario.sCveUsuario, sSituacion, sTipoOperacion, lbDebug);

									if (lbDebug) {
										System.out.println("SResultado directo sAltaModSalida: " + sResultado);
									}
									sParametros[0] = sResultado;
									sParametros[1] = null;
									sResultado = new Gson().toJson(sParametros);
									if (lbDebug) {
										System.out.println("SResultadoJson sAltaModSalida: " + sResultado);
									}
									response.setContentType("application/json");
									response.setCharacterEncoding("UTF-8");
									response.getWriter().write(sResultado);
									return;

								} catch (Exception exc) {
									exc.printStackTrace();
									String sError = exc.getMessage();
									if (sError.contains("ORA-20")) {
										sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
									} else {
										sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
									}
									sParametros[0] = sResultado;
									sParametros[1] = "1";
									sResultado = new Gson().toJson(sParametros);
									response.setContentType("application/json");
									response.setCharacterEncoding("UTF-8");
									response.getWriter().write(sResultado);
									return;
								} finally {
									try {
										if (dao != null) {
											if (dao.conn != null) {
												dao.conn.close();
											}
										}
									} catch (Exception exc) {
										exc.printStackTrace();
									}
								}
							} else {
								if (sCveOperacion.equals("ALTA_MOD_AJU_SUMA")) {
									if (lbDebug) {
										System.out.println("ALTA_MOD_COMPRA*********************************");
									}

									// , , FH_CREACION, FH_ULTIMA_MODIF,
									// USUARIO_CREACION, USUARIO_ULTIMA_MODIF,
									// ID_REFERENCIA, SIT_MOVIMIENTO

									OpticsDAO dao = new OpticsDAO();

									sResultado = "";

									int iIdMovimiento = Integer.parseInt(request.getParameter("ID_MOVIMIENTO"));
									String sFMovimiento = request.getParameter("F_MOVIMIENTO");
									int iIdAlmacen = Integer.parseInt(request.getParameter("ID_ALMACEN"));
									int iIdProductoBase = Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
									String sTxReferencia = request.getParameter("TX_REFERENCIA");
									String sSituacion = request.getParameter("SIT_MOVIMIENTO");
									String sTipoOperacion = request.getParameter("PARAM_CTL_OPERACION");

									if (lbDebug) {
										System.out.println("sIdMovimiento: " + iIdMovimiento);
										System.out.println("sFMOvimiento: " + sFMovimiento);
										System.out.println("sIdAlmacen: " + iIdAlmacen);
										System.out.println("sIdProductoBase: " + iIdProductoBase);
										System.out.println("sTxReferencia: " + sTxReferencia);
										System.out.println("sUsuario: " + usuario.sCveUsuario);
										System.out.println("sSituacion: " + sSituacion);
										System.out.println("sTipoOperacion: " + sTipoOperacion);
									}

									try {

										dao.abreConexion();
										sResultado = dao.sAltaModAjusSuma(usuario.iIdEmpresa, iIdMovimiento, sFMovimiento,
												iIdAlmacen, iIdProductoBase, sTxReferencia, usuario.sCveUsuario,
												sSituacion, sTipoOperacion, lbDebug);

										if (lbDebug) {
											System.out.println("SResultado directo: " + sResultado);
										}
										sParametros[0] = sResultado;
										sParametros[1] = null;
										sResultado = new Gson().toJson(sParametros);
										if (lbDebug) {
											System.out.println("SResultadoJson: " + sResultado);
										}

										// sResultado = "{'idMovimiento':'"+
										// sIdMovimiento + "','sResultado':'" +
										// sResultado +"'}";

										response.setContentType("application/json");
										response.setCharacterEncoding("UTF-8");
										response.getWriter().write(sResultado);
										return;

									} catch (Exception exc) {
										exc.printStackTrace();
										String sError = exc.getMessage();
										if (sError.contains("ORA-20")) {
											sResultado = sError.substring(sError.indexOf(":") + 2,
													sError.indexOf("\n"));
										} else {
											sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
										}
										sParametros[0] = sResultado;
										sParametros[1] = "1";
										sResultado = new Gson().toJson(sParametros);
										response.setContentType("application/json");
										response.setCharacterEncoding("UTF-8");
										response.getWriter().write(sResultado);
										return;
									} finally {
										try {
											if (dao != null) {
												if (dao.conn != null) {
													dao.conn.close();
												}
											}
										} catch (Exception exc) {
											exc.printStackTrace();
										}
									}

								} else {
									if (sCveOperacion.equals("ALTA_MOD_AJUSTE_RESTA")) {
										if (lbDebug) {
											System.out
													.println("ALTA_MOD_AJUSTE_RESTA*********************************");
										}

										OpticsDAO dao = new OpticsDAO();

										sResultado = "";

										int iIdMovimiento = Integer.parseInt(request.getParameter("ID_MOVIMIENTO"));
										String sFMovimiento = request.getParameter("F_MOVIMIENTO");
										int iIdAlmacen = Integer.parseInt(request.getParameter("ID_ALMACEN"));
										int iIdProductoBase = Integer
												.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
										String sTxReferencia = request.getParameter("TX_REFERENCIA");
										String sSituacion = request.getParameter("SIT_MOVIMIENTO");
										String sTipoOperacion = request.getParameter("PARAM_CTL_OPERACION");

										if (lbDebug) {
											System.out.println("sIdMovimiento: " + iIdMovimiento);
											System.out.println("sFMOvimiento: " + sFMovimiento);
											System.out.println("sIdAlmacen: " + iIdAlmacen);
											System.out.println("sIdProductoBase: " + iIdProductoBase);
											System.out.println("sTxReferencia: " + sTxReferencia);
											System.out.println("sUsuario: " + usuario.sCveUsuario);
											System.out.println("sSituacion: " + sSituacion);
											System.out.println("sTipoOperacion: " + sTipoOperacion);
										}

										try {

											dao.abreConexion();
											sResultado = dao.sAltaModAjusteResta(usuario.iIdEmpresa, iIdMovimiento,
													sFMovimiento, iIdAlmacen, iIdProductoBase, sTxReferencia,
													usuario.sCveUsuario, sSituacion, sTipoOperacion, lbDebug);

											if (lbDebug) {
												System.out.println(
														"SResultado directo sAltaModAjusteResta: " + sResultado);
											}
											sParametros[0] = sResultado;
											sParametros[1] = null;
											sResultado = new Gson().toJson(sParametros);
											if (lbDebug) {
												System.out.println("SResultadoJson sAltaModAjusteResta: " + sResultado);
											}

											// sResultado = "{'idMovimiento':'"+
											// sIdMovimiento +
											// "','sResultado':'" + sResultado
											// +"'}";

											response.setContentType("application/json");
											response.setCharacterEncoding("UTF-8");
											response.getWriter().write(sResultado);
											return;

										} catch (Exception exc) {
											exc.printStackTrace();
											String sError = exc.getMessage();
											if (sError.contains("ORA-20")) {
												sResultado = sError.substring(sError.indexOf(":") + 2,
														sError.indexOf("\n"));
											} else {
												sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario,
														lbDebug);
											}
											sParametros[0] = sResultado;
											sParametros[1] = "1";
											sResultado = new Gson().toJson(sParametros);
											response.setContentType("application/json");
											response.setCharacterEncoding("UTF-8");
											response.getWriter().write(sResultado);
											return;
										} finally {
											try {
												if (dao != null) {
													if (dao.conn != null) {
														dao.conn.close();
													}
												}
											} catch (Exception exc) {
												exc.printStackTrace();
											}
										}
									} else {
										if (sCveOperacion.equals("ALTA_MOD_DEVOLUCION_VENTA")) {
											if (lbDebug) {
												System.out.println(
														"ALTA_MOD_DEVOLUCION_VENTA*********************************");
											}

											OpticsDAO dao = new OpticsDAO();

											sResultado = "";

											if (lbDebug) {
												System.out.println("sIdMovimiento: " + request.getParameter("ID_MOVTO_VENTA"));
												System.out.println("iIdDevolucion: " + request.getParameter("ID_DEVOLUCION"));
												System.out.println("sUsuario: " + usuario.sCveUsuario);
												System.out.println("sSituacion: " + request.getParameter("SIT_MOVIMIENTO"));
												System.out.println("TX_REFERENCIA: " + request.getParameter("TX_REFERENCIA"));
											}

											int iIdMovtoVenta = Integer
													.parseInt(request.getParameter("ID_MOVTO_VENTA"));
											int iIdDevolucion = Integer.parseInt(request.getParameter("ID_DEVOLUCION"));
											String sSituacion = request.getParameter("SIT_MOVIMIENTO");
											String sTxReferencia = request.getParameter("TX_REFERENCIA");

											if (lbDebug) {
												System.out.println("sIdMovimiento: " + iIdMovtoVenta);
												System.out.println("iIdDevolucion: " + iIdDevolucion);
												System.out.println("sUsuario: " + usuario.sCveUsuario);
												System.out.println("sSituacion: " + sSituacion);
											}
											try {

												dao.abreConexion();
												sResultado = dao.sAltaModDevolucionVenta(usuario.iIdEmpresa, iIdMovtoVenta,
														iIdDevolucion, sTxReferencia, usuario.sCveUsuario, sSituacion,
														lbDebug);

												if (lbDebug) {
													System.out.println("SResultado directo sAltaModDevolucionVenta: "
															+ sResultado);
												}
												sParametros[0] = sResultado;
												sParametros[1] = null;
												sResultado = new Gson().toJson(sParametros);
												if (lbDebug) {
													System.out.println(
															"SResultadoJson sAltaModDevolucionVenta: " + sResultado);
												}

												response.setContentType("application/json");
												response.setCharacterEncoding("UTF-8");
												response.getWriter().write(sResultado);
												return;

											} catch (Exception exc) {
												exc.printStackTrace();
												String sError = exc.getMessage();
												if (sError.contains("ORA-20")) {
													sResultado = sError.substring(sError.indexOf(":") + 2,
															sError.indexOf("\n"));
												} else {
													sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario,
															lbDebug);
												}
												if (lbDebug) {
													System.out.println("SResultado en Error: " + sResultado);
												}
												sParametros[0] = sResultado;
												sParametros[1] = "1";
												sResultado = new Gson().toJson(sParametros);
												if (lbDebug) {
													System.out.println("SResultado en Error toJson: " + sResultado);
												}
												response.setContentType("application/json");
												response.setCharacterEncoding("UTF-8");
												response.getWriter().write(sResultado);
												if (lbDebug) {
													System.out.println("Escribe : " + sResultado);
												}
											} finally {
												try {
													if (dao != null) {
														if (dao.conn != null) {
															dao.conn.close();
														}
													}
												} catch (Exception exc) {
													exc.printStackTrace();
												}
											}
										} else {
											if (sCveOperacion.equals("REPORTE_CLIENTE_VEND_VTA")) {
												if (lbDebug) {
													System.out.println(
															"REPORTE_CLIENTE_VEND_VTA*********************************");
												}

												OpticsDAO dao = new OpticsDAO();

												sResultado = "";

												if (lbDebug) {
													System.out.println("PREOPER_F_MOVIMIENTO: " + request.getParameter("PREOPER_F_MOVIMIENTO"));
													System.out.println("F_MOVIMIENTO: " + request.getParameter("F_MOVIMIENTO"));
													System.out.println("POSTVAL_F_MOVIMIENTO: " + request.getParameter("POSTVAL_F_MOVIMIENTO"));
													System.out.println("ID_CLIENTE: " + request.getParameter("ID_CLIENTE"));
													System.out.println("sUsuario: " + usuario.sCveUsuario);
													System.out.println("ID_PRODUCTO_BASE: " + request.getParameter("ID_PRODUCTO_BASE"));
													System.out.println("ID_ALMACEN: " + request.getParameter("ID_ALMACEN"));
													System.out.println("USUARIO_CREACION: " + request.getParameter("USUARIO_CREACION"));
												}

												int iIdProductoBase = Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
												int iIdAlmacen = Integer.parseInt(request.getParameter("ID_ALMACEN"));
												int iIdCliente = (request.getParameter("ID_CLIENTE") != null
														&& !request.getParameter("ID_CLIENTE").equals(""))
																? Integer.parseInt(request.getParameter("ID_CLIENTE")): 0;
												String sPreOperFMovto = request.getParameter("PREOPER_F_MOVIMIENTO");
												String sFMovto = request.getParameter("F_MOVIMIENTO");
												String sPostValFMovto = request.getParameter("POSTVAL_F_MOVIMIENTO");
												String sVendedor = request.getParameter("USUARIO_CREACION");

												try {

													dao.abreConexion();
													sParametros = dao.sReporteVtaCteAlm(usuario.iIdEmpresa, iIdProductoBase,
															iIdAlmacen, sPreOperFMovto, sFMovto, sPostValFMovto,
															iIdCliente, sVendedor, usuario.sCveUsuario, lbDebug);

													if (lbDebug) {
														System.out.println(
																"SResultado directo sReporteVtaCteAlm: " + sResultado);
													}
													sResultado = new Gson().toJson(sParametros);
													if (lbDebug) {
														System.out.println("SResultadoJson sAltaModDevolucionVenta: "
																+ sResultado);
													}

													response.setContentType("application/json");
													response.setCharacterEncoding("UTF-8");
													response.getWriter().write(sResultado);
													return;

												} catch (Exception exc) {
													exc.printStackTrace();
													String sError = exc.getMessage();
													if (sError.contains("ORA-20")) {
														sResultado = sError.substring(sError.indexOf(":") + 2,
																sError.indexOf("\n"));
													} else {
														sResultado = servicio.sDameMensajeSistema(3,
																usuario.sCveUsuario, lbDebug);
													}
													if (lbDebug) {
														System.out.println("SResultado en Error: " + sResultado);
													}
													sParametros[0] = sResultado;
													sParametros[1] = "1";
													sResultado = new Gson().toJson(sParametros);
													if (lbDebug) {
														System.out.println("SResultado en Error toJson: " + sResultado);
													}
													response.setContentType("application/json");
													response.setCharacterEncoding("UTF-8");
													response.getWriter().write(sResultado);
													if (lbDebug) {
														System.out.println("Escribe : " + sResultado);
													}
												} finally {
													try {
														if (dao != null) {
															if (dao.conn != null) {
																dao.conn.close();
															}
														}
													} catch (Exception exc) {
														exc.printStackTrace();
													}
												}
											} else {
												if (sCveOperacion.equals("REPORTE_EXISTENCIAS")) {
													if (lbDebug) {
														System.out.println(
																"REPORTE_EXISTENCIAS*********************************");
													}

													OpticsDAO dao = new OpticsDAO();

													sResultado = "";

													if (lbDebug) {
														System.out.println("sUsuario: " + usuario.sCveUsuario);
														System.out.println("ID_PRODUCTO_BASE: " + request.getParameter("ID_PRODUCTO_BASE"));
														System.out.println("ID_ALMACEN: " + request.getParameter("ID_ALMACEN"));
													}

													int iIdProductoBase = Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
													int iIdAlmacen = Integer.parseInt(request.getParameter("ID_ALMACEN"));

													try {

														dao.abreConexion();
														sParametros = dao.sReporteExistenciaAlm(usuario.iIdEmpresa,
																iIdProductoBase, iIdAlmacen, usuario.sCveUsuario,lbDebug);

														if (lbDebug) {
															System.out.println(
																	"SResultado directo sReporteExistenciaAlm: "
																			+ sResultado);
														}
														sResultado = new Gson().toJson(sParametros);
														if (lbDebug) {
															System.out.println("SResultadoJson sReporteExistenciaAlm: "
																	+ sResultado);
														}

														response.setContentType("application/json");
														response.setCharacterEncoding("UTF-8");
														response.getWriter().write(sResultado);
														return;

													} catch (Exception exc) {
														exc.printStackTrace();
														String sError = exc.getMessage();
														if (sError.contains("ORA-20")) {
															sResultado = sError.substring(sError.indexOf(":") + 2,
																	sError.indexOf("\n"));
														} else {
															sResultado = servicio.sDameMensajeSistema(3,
																	usuario.sCveUsuario, lbDebug);
														}
														if (lbDebug) {
															System.out.println("SResultado en Error: " + sResultado);
														}
														sParametros[0] = sResultado;
														sParametros[1] = "1";
														sResultado = new Gson().toJson(sParametros);
														if (lbDebug) {
															System.out.println(
																	"SResultado en Error toJson: " + sResultado);
														}
														response.setContentType("application/json");
														response.setCharacterEncoding("UTF-8");
														response.getWriter().write(sResultado);
														if (lbDebug) {
															System.out.println("Escribe : " + sResultado);
														}
													} finally {
														try {
															if (dao != null) {
																if (dao.conn != null) {
																	dao.conn.close();
																}
															}
														} catch (Exception exc) {
															exc.printStackTrace();
														}
													}
												} else {
													if (sCveOperacion.equals("DAME_EXISTENCIA_OTROS_PRODUCTOS")) {
														if (lbDebug) {
															System.out.println(
																	"DAME_EXISTENCIA_OTROS_PRODUCTOS *********************");
														}

														OpticsDAO dao = new OpticsDAO();

														sResultado = "";

														if (lbDebug) {
															System.out.println("sUsuario: " + usuario.sCveUsuario);
															System.out.println("ID_PRODUCTO: "
																	+ request.getParameter("ID_PRODUCTO"));
															System.out.println("ID_ALMACEN: "
																	+ request.getParameter("ID_ALMACEN"));
															System.out.println(
																	"CVE_COLOR: " + request.getParameter("CVE_COLOR"));
														}

														int iIdProducto = Integer
																.parseInt(request.getParameter("ID_PRODUCTO"));
														int iIdAlmacen = Integer
																.parseInt(request.getParameter("ID_ALMACEN"));
														String sCveColor = request.getParameter("CVE_COLOR");

														try {

															dao.abreConexion();
															sResultado = dao.getExistenciaOtrosProductos(usuario.iIdEmpresa,
																	iIdProducto, iIdAlmacen, sCveColor, lbDebug);

															if (lbDebug) {
																System.out.println(
																		"SResultado directo getExistenciaOtrosProductos: "
																				+ sResultado);
															}
															sResultado = new Gson().toJson(sResultado);

															if (lbDebug) {
																System.out.println(
																		"SResultadoJson getExistenciaOtrosProductos: "
																				+ sResultado);
															}

															response.setContentType("application/json");
															response.setCharacterEncoding("UTF-8");
															response.getWriter().write(sResultado);
															return;

														} catch (Exception exc) {
															exc.printStackTrace();
															String sError = exc.getMessage();

															if (sError.contains("ORA-20")) {
																sResultado = sError.substring(sError.indexOf(":") + 2,
																		sError.indexOf("\n"));
															} else {
																sResultado = servicio.sDameMensajeSistema(3,
																		usuario.sCveUsuario, lbDebug);
															}
															if (lbDebug) {
																System.out
																		.println("SResultado en Error: " + sResultado);
															}
															sResultado = new Gson().toJson(sParametros);
															if (lbDebug) {
																System.out.println(
																		"SResultado en Error toJson: " + sResultado);
															}
															response.setContentType("application/json");
															response.setCharacterEncoding("UTF-8");
															response.getWriter().write(sResultado);
															if (lbDebug) {
																System.out.println("Escribe : " + sResultado);
															}
														} finally {
															try {
																if (dao != null) {
																	if (dao.conn != null) {
																		dao.conn.close();
																	}
																}
															} catch (Exception exc) {
																exc.printStackTrace();
															}
														}
													} else {
														if (sCveOperacion.equals("ALT_MOD_MOVTO_OTROS_PRODS")) {
															if (lbDebug) {
																System.out.println(
																		"ALT_MOD_MOVTO_OTROS_PRODS *********************");
															}

															OpticsDAO dao = new OpticsDAO();

															sResultado = "";

															if (lbDebug) {
																System.out.println("sUsuario: " + usuario.sCveUsuario);
																System.out.println(
																		"ID_NOTA: " + request.getParameter("ID_NOTA"));
																System.out.println("ID_MOVIMIENTO: "
																		+ request.getParameter("ID_MOVIMIENTO"));
																System.out.println("ID_PRODUCTO: "
																		+ request.getParameter("ID_PRODUCTO"));
																System.out.println("CANTIDAD: "
																		+ request.getParameter("CANTIDAD"));
																System.out.println(
																		"TIPO: " + request.getParameter("TIPO"));
																System.out.println("CVE_COLOR: "
																		+ request.getParameter("CVE_COLOR"));
																System.out.println(
																		"PRECIO: " + request.getParameter("PRECIO"));
																System.out.println("SIT_MOVIMIENTO: "
																		+ request.getParameter("SIT_MOVIMIENTO"));
															}

															
															int iIdNota = Integer
																	.parseInt(request.getParameter("ID_NOTA"));
															int iIdProducto = 0;
															int iCantidad = 0;
															int iIdMovimiento = 0;
															float fPrecio = (request.getParameter("PRECIO") != null
																	&& !request.getParameter("PRECIO").equals("")
																			? Float.parseFloat(
																					request.getParameter("PRECIO"))
																			: 0);
															String sTipo = request.getParameter("TIPO");
															String sCveColor = request.getParameter("CVE_COLOR");
															String sSitMovimiento = request
																	.getParameter("SIT_MOVIMIENTO");

															if (sTipo.equals("M")) {
																iIdMovimiento = Integer.parseInt(
																		request.getParameter("ID_MOVIMIENTO"));
															}
															if (sTipo.equals("A")) {
																iIdProducto = Integer
																		.parseInt(request.getParameter("ID_PRODUCTO"));
																iCantidad = Integer
																		.parseInt(request.getParameter("CANTIDAD"));
															}

															try {

																dao.abreConexion();
																sResultado = dao.sAltaModOtrosProds(usuario.iIdEmpresa, iIdNota,
																		iIdMovimiento, iIdProducto, iCantidad,
																		usuario.sCveUsuario, sTipo, // tipo
																									// operacion
																		sCveColor, fPrecio, sSitMovimiento,
																		usuario.iIdRol, lbDebug);

																if (lbDebug) {
																	System.out.println(
																			"SResultado directo sAltaModOtrosProds: "
																					+ sResultado);
																}
																sResultado = new Gson().toJson(sResultado);

																if (lbDebug) {
																	System.out.println(
																			"SResultadoJson sAltaModOtrosProds: "
																					+ sResultado);
																}

																response.setContentType("application/json");
																response.setCharacterEncoding("UTF-8");
																response.getWriter().write(sResultado);
																return;

															} catch (Exception exc) {
																exc.printStackTrace();
																String sError = exc.getMessage();

																if (sError.contains("ORA-20")) {
																	sResultado = sError.substring(
																			sError.indexOf(":") + 2,
																			sError.indexOf("\n"));
																} else {
																	sResultado = servicio.sDameMensajeSistema(3,
																			usuario.sCveUsuario, lbDebug);
																}
																if (lbDebug) {
																	System.out.println(
																			"SResultado en Error: " + sResultado);
																}
																sResultado = new Gson().toJson(sResultado);
																if (lbDebug) {
																	System.out.println("SResultado en Error toJson: "
																			+ sResultado);
																}
																response.setContentType("application/json");
																response.setCharacterEncoding("UTF-8");
																response.getWriter().write(sResultado);
																if (lbDebug) {
																	System.out.println("Escribe : " + sResultado);
																}
															} finally {
																try {
																	if (dao != null) {
																		if (dao.conn != null) {
																			dao.conn.close();
																		}
																	}
																} catch (Exception exc) {
																	exc.printStackTrace();
																}
															}
														} else {
															if (sCveOperacion.equals("ACTUALIZA_MINIMO_INVENTARIO")) {
																if (lbDebug) {
																	System.out.println(
																			"ACTUALIZA_MINIMO_INVENTARIO *********************");
																}

																OpticsDAO dao = new OpticsDAO();

																sResultado = "0";

																if (lbDebug) {
																	System.out.println(
																			"sUsuario: " + usuario.sCveUsuario);
																	System.out.println("ID_ALMACEN: 		"
																			+ request.getParameter("ID_ALMACEN"));
																	System.out.println("ID_PRODUCTO_BASE: "
																			+ request.getParameter("ID_PRODUCTO_BASE"));
																	System.out.println("ID_SERIE: "
																			+ request.getParameter("ID_SERIE"));
																	System.out.println("ID_VALOR_HORIZ: "
																			+ request.getParameter("ID_VALOR_HORIZ"));
																	System.out.println("ID_VALOR_VERT: "
																			+ request.getParameter("ID_VALOR_VERT"));
																}

																int iIdAlmacen = Integer
																		.parseInt(request.getParameter("ID_ALMACEN"));
																int iIdProductoBase = Integer.parseInt(
																		request.getParameter("ID_PRODUCTO_BASE"));
																int iIdSerie = Integer
																		.parseInt(request.getParameter("ID_SERIE"));
																float fValHorizontal = (request
																		.getParameter("ID_VALOR_HORIZ") != null
																		&& !request.getParameter("ID_VALOR_HORIZ")
																				.equals("") ? Float.parseFloat(
																						request.getParameter(
																								"ID_VALOR_HORIZ"))
																						: 0);
																float fValVertical = (request
																		.getParameter("ID_VALOR_VERT") != null
																		&& !request.getParameter("ID_VALOR_VERT")
																				.equals("") ? Float.parseFloat(
																						request.getParameter(
																								"ID_VALOR_VERT"))
																						: 0);
																int iCantidad = Integer
																		.parseInt(request.getParameter("CANTIDAD"));

																try {

																	dao.abreConexion();
																	sResultado = dao.sMergeMinInvProdBase(usuario.iIdEmpresa,
																			iIdAlmacen, iIdProductoBase, iIdSerie,
																			fValHorizontal, fValVertical, iCantidad,
																			usuario.sCveUsuario, usuario.iIdRol,
																			lbDebug);

																	if (lbDebug) {
																		System.out.println(
																				"SResultado directo sMergeMinInvProdBase: "
																						+ sResultado);
																	}
																	sResultado = new Gson().toJson(sResultado);

																	if (lbDebug) {
																		System.out.println(
																				"SResultadoJson sMergeMinInvProdBase: "
																						+ sResultado);
																	}

																	response.setContentType("application/json");
																	response.setCharacterEncoding("UTF-8");
																	response.getWriter().write(sResultado);
																	return;

																} catch (Exception exc) {
																	exc.printStackTrace();
																	String sError = exc.getMessage();

																	if (sError.contains("ORA-20")) {
																		sResultado = sError.substring(
																				sError.indexOf(":") + 2,
																				sError.indexOf("\n"));
																	} else {
																		sResultado = servicio.sDameMensajeSistema(3,
																				usuario.sCveUsuario, lbDebug);
																	}
																	if (lbDebug) {
																		System.out.println(
																				"SResultado en Error: " + sResultado);
																	}
																	sResultado = new Gson().toJson(sResultado);
																	if (lbDebug) {
																		System.out
																				.println("SResultado en Error toJson: "
																						+ sResultado);
																	}
																	response.setContentType("application/json");
																	response.setCharacterEncoding("UTF-8");
																	response.getWriter().write(sResultado);
																	if (lbDebug) {
																		System.out.println("Escribe : " + sResultado);
																	}
																} finally {
																	try {
																		if (dao != null) {
																			if (dao.conn != null) {
																				dao.conn.close();
																			}
																		}
																	} catch (Exception exc) {
																		exc.printStackTrace();
																	}
																}
															} else{
																
																if(sCveOperacion.equals("ACTUALIZA_SERIES_MIN_INV")){
																
																	OpticsDAO dao 	= new OpticsDAO();
																	sResultado 				= "";
																	
																	if(lbDebug){
																		System.out.println("iIdAlmacen: 		" + request.getParameter("ID_ALMACEN"));
																		System.out.println("iIdProductoBase: 	" + request.getParameter("ID_PRODUCTO_BASE"));
																	}

																	int iIdAlmacen		= Integer.parseInt(request.getParameter("ID_ALMACEN"));
																	int iIdProductoBase	= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
																	
																	if(lbDebug){
																		System.out.println("iIdProductoBase: " + iIdProductoBase);
																	}
																	
																	try {
																		
																		dao.abreConexion();
																		sResultado = dao.sGetSeriesMinInv(usuario.iIdEmpresa, iIdAlmacen, iIdProductoBase, usuario.sCveUsuario, lbDebug);
																        
																		sResultado	= new Gson().toJson(sResultado);   
																		response.setContentType("application/json");
																		response.setCharacterEncoding("UTF-8");
																		response.getWriter().write(sResultado);      
																		return;
																	} catch (Exception exc) {
															            exc.printStackTrace();
															        }
																	 finally {
																		try {
																			if( dao != null ){
																				if(dao.conn != null){
																					dao.conn.close();	
																				}							
																			}							
																		} catch(Exception exc){
																			exc.printStackTrace();
																		}			
																	}
																}else{
																	
																	if(sCveOperacion.equals("REPORTE_MINIMOS_INVENTARIO_PB")){
																	
																		OpticsDAO dao 	= new OpticsDAO();
																		sResultado 				= "";
																		
																		if(lbDebug){
																			System.out.println("iIdAlmacen: 		" + request.getParameter("ID_ALMACEN"));
																			System.out.println("iIdProductoBase: 	" + request.getParameter("ID_PRODUCTO_BASE"));
																		}

																		int iIdAlmacen		= Integer.parseInt(request.getParameter("ID_ALMACEN"));
																		int iIdProductoBase	= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
																		
																		if(lbDebug){
																			System.out.println("iIdProductoBase: " + iIdProductoBase);
																		}
																		
																		try {
																			
																			dao.abreConexion();
																			sResultado = dao.sGetSeriesInvMenorMin(usuario.iIdEmpresa, iIdAlmacen, iIdProductoBase, usuario.sCveUsuario, lbDebug);
																	        
																			sResultado	= new Gson().toJson(sResultado);   
																			response.setContentType("application/json");
																			response.setCharacterEncoding("UTF-8");
																			response.getWriter().write(sResultado);      
																			return;
																		} catch (Exception exc) {
																            exc.printStackTrace();
																        }
																		 finally {
																			try {
																				if( dao != null ){
																					if(dao.conn != null){
																						dao.conn.close();	
																					}							
																				}							
																			} catch(Exception exc){
																				exc.printStackTrace();
																			}			
																		}
																	}else{
																		
																		if(sCveOperacion.equals("DYN_OPERATION")){
																			
																			/**********************     OBTIENE LOS PARAMETROS DEL REQUEST     ********************************************************/
																	        
																	        HashMap<String, Object> map 		= new HashMap<String, Object>();
																	        HashMap<String, Object> mapInterno 	= null;
																			Enumeration<String> parameterNames 	= request.getParameterNames();
																			String paramName 					= "";
																			
																	        while (parameterNames.hasMoreElements()) {
																	            
																	        	paramName 				= parameterNames.nextElement();
																	            String[] paramValues 	= request.getParameterValues(paramName);
																	            if(lbDebug){
																	    			System.out.println(" En Servlet llega:  " + paramName + " Longitud: " + paramValues.length);
																	    		}
																	            
																	            if(paramValues.length == 1){
																	            	if(!paramValues[0].equals("")){
																		            	map.put(paramName, paramValues[0]);
																		            	//map.put(paramName, new String(paramValues[0].getBytes("ISO-8859-1"),"UTF-8"));
																		            	if(lbDebug){
																		        			System.out.println(" En Servlet agrega:  " + paramName + " Valor: " + new String(paramValues[0].getBytes("ISO-8859-1"),"UTF-8"));
																		        		}
																	            	}else{
																	            		if(lbDebug){
																		        			System.out.println(" No tiene valor  " + paramName + " Valor: >" + new String(paramValues[0].getBytes("ISO-8859-1"),"UTF-8") + "<");
																		        		}
																	            		if(paramName.equals("DYN_CTL_VAL_INPUT_FILTER")){
																	            			map.put(paramName, "");
																	            		}
																	            	}
																	            }else{
																	            	mapInterno 		= new HashMap<String, Object>();
																	            	for (int i = 0; i < paramValues.length; i++) {
																	            		if(!paramValues[i].equals("")){
																		            		mapInterno.put(String.valueOf(i), paramValues[i]);
																	            			//mapInterno.put(String.valueOf(i), new String(paramValues[i].getBytes("ISO-8859-1"),"UTF-8"));
																		            		if(lbDebug){
																		            			System.out.println(" En Servlet agrega interno:  " + String.valueOf(i) + " Valor: " + new String(paramValues[i].getBytes("ISO-8859-1"),"UTF-8"));
																		            		}
																	            		}
																			        }
																	            	map.put(paramName, mapInterno);
																	            }
																	        }
																	        map.put("DYN_CTL_USUARIO", usuario);
																	        map.put("DYN_CTL_BDEBUG", lbDebug); 
																	        map.put("DYN_CTL_ID_EMPRESA",String.valueOf(usuario.iIdEmpresa));
																	        map.put("DYN_CTL_USUARIO_SCHEMA", usuario.sUserSchema);

																	        /****************************************************************************************************************************/
																			
																			OpticsDAO dao 	= new OpticsDAO();
																			sResultado 				= "";
																			
																			try {
																				dao.abreConexion();
																				sResultado = dao.sDynOperation(map); 
																				response.setHeader("Content-Type", "application/json; charset=utf-8");
																				response.setHeader("Cache-Control", "nocache");
																				PrintWriter out = response.getWriter();
																				out.println(sResultado); 
																				out.close(); 
																				return;
																			} catch (Exception exc) {
																	            exc.printStackTrace();
																	        }
																			 finally {
																				try {
																					if( dao != null ){
																						if(dao.conn != null){
																							dao.conn.close();	
																						}							
																					}							
																				} catch(Exception exc){
																					exc.printStackTrace();
																				}			
																			}
																		}
																	}
																}
															}
														}

													}

												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
