package com.dinamicaps.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.dinamicaps.web.impl.Servicio;
import com.dinamicaps.web.impl.domain.ComboConf;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Servlet implementation class GetComboSrv
 */
public class GetComboSrv extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetComboSrv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		System.out.println("iIdEmpresa: " + request.getParameter("PARAM_CTL_ID_EMPRESA"));
		System.out.println("iIdListado: " + request.getParameter("ID_LISTADO"));
		System.out.println("sParentKey: " + request.getParameter("PARENT_KEY"));

		int iIdEmpresa =  Integer.parseInt(request.getParameter("PARAM_CTL_ID_EMPRESA"));
		int iIdListado =  Integer.parseInt(request.getParameter("ID_LISTADO"));
		String sParentKey = request.getParameter("PARENT_KEY");
		ComboConf config = null;
		LinkedHashMap<String, String> map = null; 
		StringTokenizer tokens 	= null;
		
		Servicio servicio = new Servicio();
		
		LinkedHashMap<String, String> opciones = servicio.getComboOptionsMap(iIdEmpresa, iIdListado, sParentKey, config, tokens, map, true);
		String sOpciones = "";
		
		sOpciones	= new Gson().toJson(opciones);   
		System.out.println("sOpciones: " + sOpciones);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(sOpciones);       

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/*
		System.out.println("  doPost GetComboSrv -->>>>>>>>>>>>>>>>>>>  \n" + 
				" sParentKey: 	" + request.getParameter("CTL_PARENT_KEY") 	+ "\n" +
				" iIdEmpresa: 	" + request.getParameter("PARAM_CTL_ID_EMPRESA") 	+ "\n" +
				" iIdListado: 	" + request.getParameter("CTL_ID_LISTADO") 	+ "\n");
		
		
		// TODO Auto-generated method stub
		System.out.println("iIdEmpresa: " + request.getParameter("PARAM_CTL_ID_EMPRESA"));
		System.out.println("iIdListado: " + request.getParameter("CTL_ID_LISTADO"));
		System.out.println("sParentKey: " + request.getParameter("CTL_PARENT_KEY"));
*/
		int iIdEmpresa =  Integer.parseInt(request.getParameter("PARAM_CTL_ID_EMPRESA"));
		int iIdListado =  Integer.parseInt(request.getParameter("CTL_ID_LISTADO"));
		LinkedHashMap<String, String> map = null; 

		String sParentKey = request.getParameter("CTL_PARENT_KEY");
		Servicio servicio = new Servicio();
		
		// Se obtiene el query y la configuracion del listado
		ComboConf config = servicio.cGetComboConf(iIdEmpresa, iIdListado, sParentKey, true);

		StringTokenizer tokens 	= null;
		
		if(config.sVars!=null){
			System.out.println("Tiene valor en las variables");
			String variables=config.sVars;
			tokens 	= new StringTokenizer(variables, ",");
			System.out.println("Cuenta los tokens: " + tokens.countTokens());
			
			map = new LinkedHashMap<String, String>();
			String sParamName = "";
			while(tokens.hasMoreTokens()){
				sParamName = tokens.nextToken();
		            System.out.println("En servlet ComboSrv agrega parametro: " + sParamName + " Con valor: " + request.getParameter(sParamName));
		            map.put(sParamName, request.getParameter(sParamName));
		    }
			tokens 	= new StringTokenizer(variables, ",");
			/*
			lStickersNew = new LinkedList<ComboStickerReplace>();
			lValues = new LinkedList();
			ComboStickerReplace cStickersNew;
			ComboStickerReplace cStickersTmp;

			Iterator itera = config.lStickers.iterator();
			while(itera.hasNext()){
				cStickersTmp = (ComboStickerReplace)itera.next();
				cStickersNew = new ComboStickerReplace();
				cStickersNew = cStickersTmp;
				
				if(cStickersTmp.sParamRequest!=null){
					cStickersNew.sValue = request.getParameter(cStickersTmp.sParamRequest);
				}else{
					
					
					
				}
				
			}*/
		}

		LinkedHashMap<String, String> opciones = servicio.getComboOptionsMap(iIdEmpresa, iIdListado, sParentKey, config, tokens, map, true);
		String sOpciones = "";
		
		sOpciones	= new Gson().toJson(opciones);   
		System.out.println("sOpciones: " + sOpciones);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(sOpciones);       

	
	}

}
