package com.dinamicaps.web.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.Context;
import javax.naming.InitialContext;

public class ServicioConexion {

	public Connection conn;
	public ResultSet rs 			= null;
	public String sSentenciaSql 	= "";
	public Statement st 			= null;
	public PreparedStatement pstmt 	= null;


	//public void abreConexion(String sDriver, String sUrl, String sUsuario, String sPassword) throws SQLException{
	public void abreConexion() throws SQLException{

		try {
			
			//Class.forName("org.mariadb.jdbc.Driver");
			//conn = DriverManager.getConnection("jdbc:mariadb://localhost/OPTM_BASE", "OPTM_BASE_USR", "OPTICA");
			

			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:ORCL", "ASSETADM", "!9Mhg$sz6");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "ASSETADM", "!9Mhg$sz6");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/control_pdb", "OPTICMAY", "0pt1cm4y");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/control_pdb", "ASSETADM", "OPTICA");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:ORCL", "OPTICMAY", "m4y0p71c5"); 
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:ORCL", "OPT_MAY_JQ", "OPTICA");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "VSTREAM", "OPTICA");   
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@104.238.101.242:1521:XE", "VSTREAM", "OPTICA");   //GAYOSSO XE OK
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@104.238.101.242:1521:XE", "ASSETADM", "OPTICA");   //AUDI XE OK
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:ORCL", "OPTICA_MAYORISTA", "OPTICA");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:ORCL", "HIDROSINA", "HIDROSINA");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@104.238.101.242:1521:XE", "OPTICMAY", "m4y0p71c5");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:ORCL", "BASEODS", "BASEODS");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:ORCL", "BASEODSSLF", "BASEODSSLF");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "JMEDIA", "7m3d14");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:ORCL", "JMEDIA", "7m3d14");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:ORCL", "TEST1", "TEST1");
			//conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:ORCL", "BASIC_JQ", "BASIC_JQ");

			conn.setAutoCommit(false);
		}
		catch (Exception e) {		
			e.printStackTrace();
			throw new SQLException();
		} 
	}
	
	public void abreConexion(String sEnvReference) throws SQLException {
        try {
            Context jndiCntx = new InitialContext();
            javax.sql.DataSource ds = (javax.sql.DataSource) jndiCntx.lookup(sEnvReference);
            conn = ds.getConnection();
        } catch (Exception e) {            
            e.printStackTrace();
            throw new SQLException();
        }
    }
	

    public void cierraConexion() throws SQLException {
        try{            
            conn.close();            
        }catch(Exception excp){
            excp.printStackTrace();
            throw new SQLException();
        }
    }
	
	public void ejecutaSentencia() throws SQLException{
		
		try {
			if (rs != null) {
				//CIERRA LA CONSULTA
				rs.close();
				rs = null;
			}

			if (st != null) {
				//CIERRA EL STATEMENT
				st.close();
				st = null;
			}
			st = conn.createStatement();
			rs = st.executeQuery(sSentenciaSql);
		}
		catch (SQLException e) {			
			throw e;
		}
	}
}
