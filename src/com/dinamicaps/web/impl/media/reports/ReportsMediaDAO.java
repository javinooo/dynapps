package com.dinamicaps.web.impl.media.reports;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//import org.apache.commons.fileupload.FileItem;

import com.dinamicaps.web.impl.ServicioConexion;
import com.dinamicaps.web.impl.domain.media.ReportVO;

public class ReportsMediaDAO extends ServicioConexion{


    public ReportVO getEncabezadoReporte(String idEmpresa, String idPlan) throws SQLException {

        ReportVO encabezadoReporte = new ReportVO();
        sSentenciaSql = "SELECT ID_EMPRESA, ID_PLAN, CVE_CLIENTE, CPS_CONTRATO_MAESTRO, \n"
                + "NOM_TARIFA, FOLIO,  \n"
                + "CVE_CLIENTE_AGENCIA, PLATAFORMA, CATEGORIA_PRODUCTO, \n"
                + "TOTAL_SPOTS, ORDEN_SIN_DESCUENTO, ORDEN_CON_DESCUENTO, \n"
                + "EMAIL_RESPONSABLE, PROCESAR_POR_LINEA, \n"
                + "TIPO_FACTURACION, COMENTARIO_ORDEN, USO_INTERNO, \n"
                + "ING_CONTRATO_MAESTRO, TARIFA_CUT_IN, GARANTIZADO\n"
                + "FROM ENCABEZADO_FORMATO_TVSA_V \n"
                + "WHERE ID_PLAN = " + idPlan + "\n"
                + "AND ID_EMPRESA = " + idEmpresa + "\n";

        System.out.println(sSentenciaSql);
        
        ejecutaSentencia();

        if (rs.next()) {
        	System.out.println("si encuentra detalle ");
            encabezadoReporte.setIdEmpresa(rs.getString("ID_EMPRESA"));
            encabezadoReporte.setIdPlan(rs.getString("ID_PLAN"));
            encabezadoReporte.setCveCliente(rs.getString("CVE_CLIENTE"));
            encabezadoReporte.setContratoMaestro(rs.getString("CPS_CONTRATO_MAESTRO"));
            encabezadoReporte.setNombreTarifa(rs.getString("NOM_TARIFA"));
            encabezadoReporte.setFolio(rs.getString("FOLIO"));
            encabezadoReporte.setCveEncClienteAg(rs.getString("CVE_CLIENTE_AGENCIA"));
            encabezadoReporte.setPlataforma(rs.getString("PLATAFORMA"));
            encabezadoReporte.setCategoriaProducto(rs.getString("CATEGORIA_PRODUCTO"));
            encabezadoReporte.setTotalSpots(rs.getString("TOTAL_SPOTS"));
            encabezadoReporte.setTotalOrdenSinDesc(rs.getString("ORDEN_SIN_DESCUENTO"));
            encabezadoReporte.setTotalOrdenConDesc(rs.getString("ORDEN_CON_DESCUENTO"));
            encabezadoReporte.setNomEmailResponsable(rs.getString("EMAIL_RESPONSABLE"));
            encabezadoReporte.setProcesarPorLinea(rs.getString("PROCESAR_POR_LINEA"));
            encabezadoReporte.setCanalOrden(rs.getString("PLATAFORMA"));
            encabezadoReporte.setTipoFacturacion(rs.getString("TIPO_FACTURACION"));
            encabezadoReporte.setComentarios(rs.getString("COMENTARIO_ORDEN"));
            encabezadoReporte.setDescUsoInterno(rs.getString("USO_INTERNO"));
            encabezadoReporte.setMasterContact(rs.getString("ING_CONTRATO_MAESTRO"));
            encabezadoReporte.setTarifaCutIn(rs.getString("TARIFA_CUT_IN"));
            encabezadoReporte.setGarantizado(rs.getString("GARANTIZADO"));
            encabezadoReporte.setReferenciaFolio(rs.getString("FOLIO"));

        }
        return encabezadoReporte;
    }

    public ResultSet getDatosReporte(String idPlan) throws SQLException {

        sSentenciaSql = "SELECT LINEA, ID_PLAN, ID_ORDEN, CANAL, CVE_ESTATUS, INICIO, FIN, DURACION, SPOTS_X_SEMANA, \n"
                + "LUN, MAR, MIE, JUE, VIE, SAB, DOM, TIPO_SERVICIO, MARCA,\n"
                + "VERSION, TARIFA_SIN_DESCUENTO, TOTAL_X_SPOT_SIN_DESCUENTO, TARIFA_CON_DESCUENTO, TOTAL_X_SPOT_CON_DESCUENTO,\n"
                + "PROGRAMA, H_REF_INI, H_REF_FIN, BN, P, SOBRECARGO, OBSERVACIONES\n"
                + "FROM ORDEN_PUBLICITARIA_FACTURA_V \n"
                + "WHERE ID_PLAN  = " + idPlan;

        System.out.println(sSentenciaSql);
        ejecutaSentencia();

        return rs;
    }
	
    

	public String sInsertaCantidad(String sIdEmpresa, String sIdOrdenInsercion, String sTarifaCliente, 
			String sFInsercion, String sIdTarifa, String sCantidad, String sCveMedida, String sHorizontal,
			String sVertical, String sImpHorizVert, String ScveEstatus) throws SQLException,  IOException{

		String sResultado = "";

		CallableStatement callst = conn.prepareCall("{ ? = call FN_INSERTA_CANTIDAD(?,?,?,?,?,?,?,?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.VARCHAR);
	      callst.setString(2,sIdEmpresa);
	      callst.setString(3,sIdOrdenInsercion);
	      callst.setString(4,sTarifaCliente);
	      callst.setString(5,sFInsercion);
	      callst.setString(6,sIdTarifa);
	      callst.setString(7,sCantidad);
	      callst.setString(8,sCveMedida);
	      callst.setString(9,sHorizontal);
	      callst.setString(10,sVertical);
	      callst.setString(11,sImpHorizVert);
	      callst.setString(12,ScveEstatus);
	      
	      
	      System.out.println("Antes de ejecutar sInsertaCantidad EN DAO ");
	      
	      callst.execute(); //  ------->>>>> ERROR Occur
	      System.out.println("Despues de sInsertaCantidad Archivo EN DAO");
	      
	      sResultado = callst.getString(1);
	      
	      return sResultado;

	}
	
}
