package com.dinamicaps.web.impl.domain;

public class Navegacion {
	public String sCVE_NAVEGACION  = "";
	public String sB_OBTIENE_ID_TABLA  = "";
	public String sB_OBTIENE_PERMISOS_MAESTRO  = "";
	public String sB_VALIDA_OPERACION = "";
	public String sB_OBTIENE_CAMPOS_PK = "";
	public String sB_OBTIENE_CAMPOS_ALT_MOD = "";
	public String sB_EJECUTA_ALT_MOD = "";
	public String sB_OBTIENE_CAMPOS_EXEC = "";
	public String sB_OBTIENE_CONSULTA_MAESTRO_DET = "";
	public String sB_OBTIENE_NAVEGA_NEXT = "";
	public String sB_REDIRECCIONA_ID_TABLA = "";
	public String sB_OBTIENE_CAMPOS_FILTRO = "";
	public String sB_NAVEGA_TEMPLT_REMPLAZA = "";
	public String sB_EJECUTA_EXECUTABLE = "";
	public String sB_OBTIENE_CAMPOS_FK  = "";
	public String sCVE_OPERACION = "";
}

