package com.dinamicaps.web.impl.domain;

public class MasterDetail {
	public int iIdEmpresa;
	public int iIdPantalla;
	public int iIdMasterTable;
	public int iIdDetailTable;
	public int iIdMasterTableSource;
	public int iIdDetailTableSource;
	//public int iIdOrder;
	public String sMasterAttribute 			= "";
	public String sDetailAttribute 			= "";
	public String sDetailDataType 			= "";
	public String sMasterDescriptionField 	= "";
	public boolean bIsFKParentPage;
	public String sCveParametroEnvio 	= "";
}
