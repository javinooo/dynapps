package com.dinamicaps.web.impl.domain.optics;

import java.io.IOException;
//import java.io.Reader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import com.dinamicaps.web.impl.Servicio;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
//import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
//import java.io.StringReader;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;

import com.google.gson.Gson;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.MalformedJsonException;

import com.dinamicaps.web.impl.ServicioConexion;
import com.dinamicaps.web.impl.domain.ComboConf;
import com.dinamicaps.web.impl.domain.Usuario;
import com.dinamicaps.web.impl.domain.optics.MatrizResultadoConf;
import com.dinamicaps.web.impl.domain.optics.MatrizAdmDatosConf;


public class OpticsDAO extends ServicioConexion{

  public String sGetSeries(int sIdEmpresa, int iIdProductoBase, int iIdMovimiento, String sUsuario, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "";

		CallableStatement callst = conn.prepareCall("{ ? = call FN_GENERA_MATRIZ_SERIE(?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.CLOB);
	      callst.setInt(2,sIdEmpresa);
	      callst.setInt(3,iIdProductoBase);
	      callst.setInt(4,iIdMovimiento);
	      callst.setString(5,sUsuario);

	      if(bDebug){
	    	  System.out.println("Antes de ejecutar sGetSeries EN DAO ");
	      }

	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de sGetSeries Archivo EN DAO");
	      }

	      sResultado = callst.getString(1);

	      return sResultado;

	}



  public String sGetSeriesVta(int sIdEmpresa, int iIdProductoBase, int iIdAlmacen, int iIdMovimiento, String sUsuario, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "";

		CallableStatement callst = conn.prepareCall("{ ? = call FN_GENERA_MATRIZ_SERIE_VTA(?,?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.CLOB);
	      callst.setInt(2,sIdEmpresa);
	      callst.setInt(3,iIdProductoBase);
	      callst.setInt(4,iIdAlmacen);
	      callst.setInt(5,iIdMovimiento);
	      callst.setString(6,sUsuario);
	      
	      if(bDebug){
	    	  System.out.println("Antes de ejecutar sGetSeries EN DAO ");
	      }
	      
	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de sGetSeries Archivo EN DAO");
	      }
	      
	      sResultado = callst.getString(1);
	      
	      return sResultado;

	}
  
  public String[] sGeneraPreMovto(int sIdEmpresa, String sIdPreMovto, String sCveProducto, int iCantidad, 
		  String sTipoOperacion, String sTipoMovimiento, int iIdAlmacen, String sTipoDevolucion, 
		  String sCveUsuario, int iIdDevolucion, String sBSincroniza, int iIdRol, int iIdNota, boolean bDebug) throws SQLException,  IOException{

		String sIdResultado[] = new String[2];

		CallableStatement callst = conn.prepareCall("{call PCD_GENERA_PRE_MOVTO(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
	      //callst.registerOutParameter(1, java.sql.Types.INTEGER);
	      callst.setInt(1,sIdEmpresa);
	      callst.registerOutParameter(2, java.sql.Types.VARCHAR);
	      callst.setString(2,sIdPreMovto);
	      callst.setString(3,sCveProducto);
	      callst.setInt(4,iCantidad);
	      callst.setString(5,sTipoOperacion);
	      callst.setString(6,sTipoMovimiento);
	      callst.setInt(7,iIdAlmacen);
	      callst.setString(8,sTipoDevolucion);
	      callst.setString(9,sCveUsuario);
	      callst.setInt(10,iIdDevolucion);
	      callst.setString(11,sBSincroniza);
	      callst.setInt(12,iIdRol);
	      callst.registerOutParameter(13, java.sql.Types.VARCHAR);
	      callst.setInt(14,iIdNota);

	      
	      if(bDebug){
		      System.out.println("Antes de ejecutar iGeneraPreMovto EN DAO ");
		      
		      System.out.println("sIdEmpresa: " + sIdEmpresa);
		      System.out.println("sIdPreMovto: " + sIdPreMovto);
		      System.out.println("sCveProducto: " + sCveProducto);
		      System.out.println("iCantidad: " + iCantidad);
		      System.out.println("sTipoOperacion: " + sTipoOperacion);
		      System.out.println("sTipoMovimiento: " + sTipoMovimiento);
		      System.out.println("sTipoDevolucion: " + sTipoDevolucion);
		      System.out.println("sBSincroniza: " + sBSincroniza);
		      System.out.println("iIdNota: " + iIdNota);
	      }
	      
	      callst.execute(); //  ------->>>>> ERROR Occur
	      
	      if(bDebug){
	    	  System.out.println("Despues de iGeneraPreMovto EN DAO");
	      }
	      
	      sIdResultado[0] = callst.getString(2);
	      sIdResultado[1] = callst.getString(13);
	      
	      return sIdResultado;

	}
  
  public String sAltaModEntrada(int iIdEmpresa, int iIdNota, int iIdMovimiento, 
		  	int iIdProductoBase, String sTxReferencia, String sUsuario, String sSituacion, String sTipoOperacion, boolean bDebug) throws SQLException,  IOException{


		String sResultado = "";
		
		if(sTipoOperacion.equals("A")){

			sSentenciaSql = 
					"INSERT INTO MOVIMIENTO( \n " +
					"      	ID_EMPRESA, ID_NOTA, F_MOVIMIENTO, ID_PROVEEDOR, ID_ALMACEN, TIPO_MOVIMIENTO, SIT_MOVIMIENTO,  \n " +
					"  		ID_PRODUCTO_BASE, TX_REFERENCIA, FH_CREACION, FH_ULTIMA_MODIF, USUARIO_CREACION, \n " +
					"  		USUARIO_ULTIMA_MODIF, ID_MOVIMIENTO) \n " +
				    "SELECT ID_EMPRESA, ID_NOTA, F_NOTA, ID_PROVEEDOR, ID_ALMACEN, TIPO_NOTA, 'IS', \n " +
					"        ?,?, SYSDATE,SYSDATE,?,?,? \n " +
				    "FROM 	NOTA 			\n " +
				    "WHERE	ID_EMPRESA = ?  \n " +
				    "	AND ID_NOTA    = ?  \n " ;

			if(bDebug){
				System.out.println(sSentenciaSql);
			}
			
	        pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1,iIdProductoBase);
			pstmt.setString(2,sTxReferencia);
			pstmt.setString(3,sUsuario);
			pstmt.setString(4,sUsuario);
			pstmt.setInt(5,iIdMovimiento);
			pstmt.setInt(6,iIdEmpresa);
			pstmt.setInt(7,iIdNota);
	
						
			if(bDebug){
				System.out.println("Antes de ejecutar sGetSeries EN DAO ");
			}
			    
			rs = pstmt.executeQuery();

			if(bDebug){
				System.out.println("Despues de sGetSeries Archivo EN DAO");
			}
			
			rs.close();
		    pstmt.close();
		
		    Servicio servicio = new Servicio();
		    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
		    
		}else{
			
			if(sTipoOperacion.equals("M")){
				
				if(bDebug){
					System.out.println("sSituacion EN DAO:  " + sSituacion);
					
				}

				sSentenciaSql = 
						" UPDATE MOVIMIENTO SET \n " +
								"  TX_REFERENCIA= ?,	  \n " +
								"  FH_ULTIMA_MODIF= SYSDATE, \n " +
								(sSituacion==null?"":"  SIT_MOVIMIENTO= ?, \n ") +
								"  USUARIO_ULTIMA_MODIF = ? \n " +
					    " WHERE ID_EMPRESA = ? \n " +
						"   AND ID_MOVIMIENTO = ? \n "; 
				
					if(bDebug){
						System.out.println(sSentenciaSql);
					}
					
			        pstmt = conn.prepareStatement(sSentenciaSql);
					pstmt.setString(1,sTxReferencia);
					if(sSituacion!=null){
						pstmt.setString(2,sSituacion);
						pstmt.setString(3,sUsuario);
						pstmt.setInt(4,iIdEmpresa);
						pstmt.setInt(5,iIdMovimiento);
					}else{
						pstmt.setString(2,sUsuario);
						pstmt.setInt(3,iIdEmpresa);
						pstmt.setInt(4,iIdMovimiento);
					}
					
					
					if(bDebug){
						System.out.println("Antes de ejecutar MODIFICA MOVTO EN DAO ");
					}
					    
					rs = pstmt.executeQuery();

					if(bDebug){
						System.out.println("Despues de MODIFICA MOVTO EN DAO");
					}
					
					rs.close();
				    pstmt.close();
				
				    Servicio servicio = new Servicio();
				    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
				
			}
		}
		
		
		CallableStatement callst = conn.prepareCall("{call PCD_ACTUALIZA_IMPORTE_NOTA(?,?,?)}");
		callst.setInt(1,iIdEmpresa);
		callst.setInt(2,iIdNota);
		callst.setString(3,sUsuario);
		callst.execute(); 
  
		if(bDebug){
			System.out.println("Despues de PCD_ACTUALIZA_IMPORTE_NOTA EN DAO");
	  	}
	        
	      return sResultado;
	}
  

  public String sAltaModAjusSuma(int iIdEmpresa, int iIdMovimiento, String sFMovimiento, int iIdAlmacen, 
		  	int iIdProductoBase, String sTxReferencia, String sUsuario, String sSituacion, String sTipoOperacion, boolean bDebug) throws SQLException,  IOException{


		String sResultado = "";
		
		if(sTipoOperacion.equals("A")){

			sSentenciaSql = 
					" INSERT INTO MOVIMIENTO( \n " +
							"  ID_EMPRESA, ID_MOVIMIENTO, F_MOVIMIENTO, ID_ALMACEN, ID_PRODUCTO_BASE, \n " +
							"  TX_REFERENCIA, TIPO_MOVIMIENTO, FH_CREACION, FH_ULTIMA_MODIF, USUARIO_CREACION, USUARIO_ULTIMA_MODIF, \n " +
							"  SIT_MOVIMIENTO) \n " +
				    " VALUES(?,?,TO_DATE('" + sFMovimiento + "','DD/MM/YYYY'),?,?,?,'AJUSTE_SUMA', SYSDATE,SYSDATE,?,?,'AC') \n " ;


			if(bDebug){
				System.out.println(sSentenciaSql);
			}
			
		        pstmt = conn.prepareStatement(sSentenciaSql);
				pstmt.setInt(1,iIdEmpresa);
				pstmt.setInt(2,iIdMovimiento);
				pstmt.setInt(3,iIdAlmacen);
				pstmt.setInt(4,iIdProductoBase);
				pstmt.setString(5,sTxReferencia);
				pstmt.setString(6,sUsuario);
				pstmt.setString(7,sUsuario);
		
				
				if(bDebug){
					System.out.println("Antes de ejecutar sGetSeries EN DAO ");
				}
				    
				rs = pstmt.executeQuery();

				if(bDebug){
					System.out.println("Despues de sGetSeries Archivo EN DAO");
				}
				
				rs.close();
			    pstmt.close();
			
			    Servicio servicio = new Servicio();
			    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
		}else{
			
			if(sTipoOperacion.equals("M")){
				
				sSentenciaSql = 
						" UPDATE MOVIMIENTO SET \n " +
								"  F_MOVIMIENTO = TO_DATE('" + sFMovimiento + "','DD/MM/YYYY'), \n " +
								"  ID_ALMACEN	= ?,  \n " +
								"  TX_REFERENCIA= ?,	  \n " +
								"  FH_ULTIMA_MODIF= SYSDATE, \n " +
								(sSituacion==null?"":"  SIT_MOVIMIENTO= ?, \n ") +
								"  USUARIO_ULTIMA_MODIF = ? \n " +
					    " WHERE ID_EMPRESA = ? \n " +
						"   AND ID_MOVIMIENTO = ? \n "; 
				
					if(bDebug){
						System.out.println(sSentenciaSql);
					}
					
			        pstmt = conn.prepareStatement(sSentenciaSql);
					pstmt.setInt(1,iIdAlmacen);
					pstmt.setString(2,sTxReferencia);
					if(sSituacion!=null){
						pstmt.setString(3,sSituacion);
						pstmt.setString(4,sUsuario);
						pstmt.setInt(5,iIdEmpresa);
						pstmt.setInt(6,iIdMovimiento);
					}else{
						pstmt.setString(3,sUsuario);
						pstmt.setInt(4,iIdEmpresa);
						pstmt.setInt(5,iIdMovimiento);
					}
					
					
					if(bDebug){
						System.out.println("Antes de ejecutar MODIFICA MOVTO EN DAO ");
					}
					    
					rs = pstmt.executeQuery();

					if(bDebug){
						System.out.println("Despues de MODIFICA MOVTO EN DAO");
					}
					
					rs.close();
				    pstmt.close();
				
				    Servicio servicio = new Servicio();
				    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
				
			}
		}
		
	      
	      return sResultado;

	}

  
  public String sAltaModSalida(int iIdEmpresa, int iIdNota, int iIdMovimiento, 
		  	int iIdProductoBase, String sTxReferencia, String sUsuario, String sSituacion, String sTipoOperacion, boolean bDebug) throws SQLException,  IOException{


		String sResultado = "";
		
		if(sTipoOperacion.equals("A")){

			sSentenciaSql = 
				"INSERT INTO MOVIMIENTO( \n " +
				"      	ID_EMPRESA, ID_NOTA, F_MOVIMIENTO, ID_CLIENTE, ID_ALMACEN, TIPO_MOVIMIENTO, SIT_MOVIMIENTO,  \n " +
				"  		ID_PRODUCTO_BASE, TX_REFERENCIA, FH_CREACION, FH_ULTIMA_MODIF, USUARIO_CREACION, \n " +
				"  		USUARIO_ULTIMA_MODIF, ID_MOVIMIENTO) \n " +
			    "SELECT ID_EMPRESA, ID_NOTA, F_NOTA, ID_CLIENTE, ID_ALMACEN, TIPO_NOTA, 'AC', \n " +
				"        ?,?, SYSDATE,SYSDATE,?,?,? \n " +
			    "FROM 	NOTA 			\n " +
			    "WHERE	ID_EMPRESA = ?  \n " +
			    "	AND ID_NOTA    = ?  \n " ;
	
			
			if(bDebug){
				System.out.println(sSentenciaSql);
			}

	        pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1,iIdProductoBase);
			pstmt.setString(2,sTxReferencia);
			pstmt.setString(3,sUsuario);
			pstmt.setString(4,sUsuario);
			pstmt.setInt(5,iIdMovimiento);
			pstmt.setInt(6,iIdEmpresa);
			pstmt.setInt(7,iIdNota);

			if(bDebug){
				System.out.println("Antes de ejecutar sGetSeries EN DAO ");
			}
			    
			rs = pstmt.executeQuery();

			if(bDebug){
				System.out.println("Despues de sGetSeries Archivo EN DAO");
			}
			
			rs.close();
		    pstmt.close();
		
		    Servicio servicio = new Servicio();
		    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
		    
		}else{
			
			if(sTipoOperacion.equals("M")){
				
				sSentenciaSql = 
						" UPDATE MOVIMIENTO SET \n " +
								"  TX_REFERENCIA= ?,	  \n " +
								"  FH_ULTIMA_MODIF= SYSDATE, \n " +
								(sSituacion==null?"":"  SIT_MOVIMIENTO= ?, \n ") +
								"  USUARIO_ULTIMA_MODIF = ? \n " +
					    " WHERE ID_EMPRESA = ? \n " +
						"   AND ID_MOVIMIENTO = ? \n "; 
				
					if(bDebug){
						System.out.println(sSentenciaSql);
					}
					
			        pstmt = conn.prepareStatement(sSentenciaSql);
					pstmt.setString(1,sTxReferencia);
					if(sSituacion!=null){
						pstmt.setString(2,sSituacion);
						pstmt.setString(3,sUsuario);
						pstmt.setInt(4,iIdEmpresa);
						pstmt.setInt(5,iIdMovimiento);
					}else{
						pstmt.setString(2,sUsuario);
						pstmt.setInt(3,iIdEmpresa);
						pstmt.setInt(4,iIdMovimiento);
					}
					
					
					if(bDebug){
						System.out.println("Antes de ejecutar MODIFICA MOVTO VENTA EN DAO ");
					}
					    
					rs = pstmt.executeQuery();

					if(bDebug){
						System.out.println("Despues de MODIFICA MOVTO VENTA EN DAO");
					}
					
					rs.close();
				    pstmt.close();
				
				    Servicio servicio = new Servicio();
				    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
				
			}
		}
		
		
		CallableStatement callst = conn.prepareCall("{call PCD_ACTUALIZA_IMPORTE_NOTA(?,?)}");
		callst.setInt(1,iIdEmpresa);
		callst.setInt(2,iIdNota);
		callst.execute(); 
  
		if(bDebug){
			System.out.println("Despues de PCD_ACTUALIZA_IMPORTE_NOTA EN DAO");
	  	}
	      
	      return sResultado;

	}
	
  public String sAltaModAjusteResta(int iIdEmpresa, int iIdMovimiento, String sFMovimiento, int iIdAlmacen, 
		  	int iIdProductoBase, String sTxReferencia, String sUsuario, String sSituacion, String sTipoOperacion, boolean bDebug) throws SQLException,  IOException{


		String sResultado = "";
		
		if(sTipoOperacion.equals("A")){

			sSentenciaSql = 
					" INSERT INTO MOVIMIENTO( \n " +
							"  ID_EMPRESA, ID_MOVIMIENTO, F_MOVIMIENTO, ID_ALMACEN, ID_PRODUCTO_BASE, \n " +
							"  TX_REFERENCIA, TIPO_MOVIMIENTO, FH_CREACION, FH_ULTIMA_MODIF, USUARIO_CREACION, USUARIO_ULTIMA_MODIF, \n " +
							"  SIT_MOVIMIENTO) \n " +
				    " VALUES(?,?,TO_DATE('" + sFMovimiento + "','DD/MM/YYYY'),?,?,?,'AJUSTE_RESTA', SYSDATE,SYSDATE,?,?,'AC') \n " ;


			if(bDebug){
				System.out.println(sSentenciaSql);
			}
			
	        pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1,iIdEmpresa);
			pstmt.setInt(2,iIdMovimiento);
			pstmt.setInt(3,iIdAlmacen);
			pstmt.setInt(4,iIdProductoBase);
			pstmt.setString(5,sTxReferencia);
			pstmt.setString(6,sUsuario);
			pstmt.setString(7,sUsuario);
	
			
			if(bDebug){
				System.out.println("Antes de ejecutar sGetSeries EN DAO ");
			}
			    
			rs = pstmt.executeQuery();

			if(bDebug){
				System.out.println("Despues de sGetSeries Archivo EN DAO");
			}
			
			rs.close();
		    pstmt.close();
		
		    Servicio servicio = new Servicio();
		    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
		    
		}else{
			
			if(sTipoOperacion.equals("M")){
				
				sSentenciaSql = 
						" UPDATE MOVIMIENTO SET \n " +
								"  F_MOVIMIENTO = TO_DATE('" + sFMovimiento + "','DD/MM/YYYY'), \n " +
								"  ID_ALMACEN	= ?,  \n " +
								"  TX_REFERENCIA= ?,	  \n " +
								"  FH_ULTIMA_MODIF= SYSDATE, \n " +
								(sSituacion==null?"":"  SIT_MOVIMIENTO= ?, \n ") +
								"  USUARIO_ULTIMA_MODIF = ? \n " +
					    " WHERE ID_EMPRESA = ? \n " +
						"   AND ID_MOVIMIENTO = ? \n "; 
				
					if(bDebug){
						System.out.println(sSentenciaSql);
					}
					
			        pstmt = conn.prepareStatement(sSentenciaSql);
					pstmt.setInt(1,iIdAlmacen);
					pstmt.setString(2,sTxReferencia);
					if(sSituacion!=null){
						pstmt.setString(3,sSituacion);
						pstmt.setString(4,sUsuario);
						pstmt.setInt(5,iIdEmpresa);
						pstmt.setInt(6,iIdMovimiento);
					}else{
						pstmt.setString(3,sUsuario);
						pstmt.setInt(4,iIdEmpresa);
						pstmt.setInt(5,iIdMovimiento);
					}
					
					
					if(bDebug){
						System.out.println("Antes de ejecutar MODIFICA MOVTO VENTA EN DAO ");
					}
					    
					rs = pstmt.executeQuery();

					if(bDebug){
						System.out.println("Despues de MODIFICA MOVTO VENTA EN DAO");
					}
					
					rs.close();
				    pstmt.close();
				
				    Servicio servicio = new Servicio();
				    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
				
			}
		}
		
	      
	      return sResultado;

	}


  public String sAltaModDevolucionVenta(int iIdEmpresa, int iIdMovtoVenta, int iIdDevolucion, String sTxReferencia, 
		  String sUsuario, String sSituacion, boolean bDebug) throws SQLException,  IOException{


	  String sResultado = "";
		
		sSentenciaSql = 
				" MERGE INTO MOVIMIENTO A  		\n " +
				" USING (  						\n " +
				" 		SELECT	ID_EMPRESA, ? AS ID_MOVIMIENTO, TRUNC(SYSDATE) AS F_MOVIMIENTO, \n " +
				"				ID_CLIENTE, ID_ALMACEN, ? AS ID_REFERENCIA, ID_PRODUCTO_BASE, 	\n " +
				"				SYSDATE AS FH_CREACION, SYSDATE AS FH_ULTIMA_MODIF, ID_NOTA, 	\n " +
				"				? AS USUARIO_CREACION, ? AS USUARIO_ULTIMA_MODIF, ID_PRODUCTO, 	\n " +
				"               ? AS TX_REFERENCIA, 'SALIDA_DEVOLUCION' AS TIPO_MOVIMIENTO,		\n " +
				"               ? AS SIT_MOVIMIENTO 											\n " +
				"       FROM 	MOVIMIENTO 			\n " +
				"       WHERE 	ID_EMPRESA = ?		\n " +
				"       	AND ID_MOVIMIENTO = ?	\n " +
				"     ) B 										\n " +
				"     ON (A.ID_EMPRESA = B.ID_EMPRESA 			\n " +
				"     AND A.ID_MOVIMIENTO = B.ID_MOVIMIENTO) 	\n " +
				" WHEN MATCHED THEN  											\n " +
				"     UPDATE SET  												\n " +
				"         A.FH_ULTIMA_MODIF 		= SYSDATE, 					\n " +
				"         A.USUARIO_ULTIMA_MODIF 	= B.USUARIO_ULTIMA_MODIF, 	\n " +
				"         A.SIT_MOVIMIENTO 			= B.SIT_MOVIMIENTO, 		\n " +
				"         A.TX_REFERENCIA 			= B.TX_REFERENCIA 			\n " +
				" WHEN NOT MATCHED THEN  																\n " +
				"     INSERT(A.ID_EMPRESA, A.ID_MOVIMIENTO, A.F_MOVIMIENTO, A.ID_CLIENTE, A.ID_ALMACEN, \n " +
				"			A.ID_REFERENCIA, A.ID_PRODUCTO_BASE, A.FH_CREACION, A.FH_ULTIMA_MODIF, 		\n " +
				"			A.ID_NOTA, A.USUARIO_CREACION, A.USUARIO_ULTIMA_MODIF, A.ID_PRODUCTO, 		\n " +
				"           A.TX_REFERENCIA, A.TIPO_MOVIMIENTO, A.SIT_MOVIMIENTO)						\n " +
				"     VALUES(B.ID_EMPRESA, B.ID_MOVIMIENTO, B.F_MOVIMIENTO, B.ID_CLIENTE, B.ID_ALMACEN, \n " +
				"			B.ID_REFERENCIA, B.ID_PRODUCTO_BASE, B.FH_CREACION, B.FH_ULTIMA_MODIF, 		\n " +
				"			B.ID_NOTA, B.USUARIO_CREACION, B.USUARIO_ULTIMA_MODIF, B.ID_PRODUCTO, 		\n " +
				"           B.TX_REFERENCIA, B.TIPO_MOVIMIENTO, B.SIT_MOVIMIENTO) 						\n ";
		 
		if(bDebug){
			System.out.println(sSentenciaSql);
		}
			
        pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1,iIdDevolucion);
		pstmt.setInt(2,iIdMovtoVenta);
		pstmt.setString(3,sUsuario);
		pstmt.setString(4,sUsuario);
		pstmt.setString(5,sTxReferencia);
		pstmt.setString(6,sSituacion);	
		pstmt.setInt(7,iIdEmpresa);
		pstmt.setInt(8,iIdMovtoVenta);
					
		if(bDebug){
			System.out.println("Antes de ejecutar sAltaModDevolucionVenta EN DAO ");
		}
		    
		rs = pstmt.executeQuery();

		if(bDebug){
			System.out.println("Despues de sAltaModDevolucionVenta EN DAO");
		}
		
		rs.close();
	    pstmt.close();
	
	    Servicio servicio = new Servicio();
	    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);

	    return sResultado;

	}
  

  public String[] sReporteVtaCteAlm(int iIdEmpresa, int iIdProductoBase, int iIdAlmacen, 
		  String sPreOperFMovto, String sFMovto, String sPostValFMovto, 
		  int iIdCliente, String sVendedor, String sCveUsuario, boolean bDebug) throws SQLException,  IOException{

		String sIdResultado[] = new String[2];


		CallableStatement callst = conn.prepareCall("{ ? = call FN_REPORTE_VTA_CLI_ALM(?,?,?,?,?,?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.CLOB);
	      callst.setInt(2,iIdEmpresa);
	      callst.setInt(3,iIdProductoBase);
	      callst.setInt(4,iIdAlmacen);
	      callst.setString(5,sPreOperFMovto);
	      callst.setString(6,sFMovto);
	      callst.setString(7,sPostValFMovto);
	      callst.setInt(8,iIdCliente);
	      callst.setString(9,sVendedor);
	      callst.setString(10,sCveUsuario);

	      if(bDebug){
		      System.out.println("Antes de ejecutar sReporteVtaCteAlm EN DAO ");
		      
		      System.out.println("sIdEmpresa: " + iIdEmpresa);
		      System.out.println("iIdProductoBase: " + iIdProductoBase);
		      System.out.println("iIdAlmacen: " + iIdAlmacen);
		      System.out.println("sPreOperFMovto: " + sPreOperFMovto);
		      System.out.println("sFMovto: " + sFMovto);
		      System.out.println("sPostValFMovto: " + sPostValFMovto);
		      System.out.println("iIdCliente: " + iIdCliente);
		      System.out.println("sVendedor: " + sVendedor);
		      System.out.println("sCveUsuario: " + sCveUsuario);
	      }
	      
	      callst.execute(); //  ------->>>>> ERROR Occur
	      
	      if(bDebug){
	    	  System.out.println("Despues de sReporteVtaCteAlm EN DAO");
	      }
	      
	      sIdResultado[0] = callst.getString(1);
	      sIdResultado[1] = null;
	      
	      return sIdResultado;

	}
  

  public String[] sReporteExistenciaAlm(int iIdEmpresa, int iIdProductoBase, int iIdAlmacen, 
		  String sCveUsuario, boolean bDebug) throws SQLException,  IOException{

		String sIdResultado[] = new String[2];


		CallableStatement callst = conn.prepareCall("{ ? = call FN_REPORTE_EXISTENCIA(?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.CLOB);
	      callst.setInt(2,iIdEmpresa);
	      callst.setInt(3,iIdProductoBase);
	      callst.setInt(4,iIdAlmacen);
	      callst.setString(5,sCveUsuario);

	      if(bDebug){
		      System.out.println("Antes de ejecutar sReporteVtaCteAlm EN DAO ");
		      
		      System.out.println("sIdEmpresa: " + iIdEmpresa);
		      System.out.println("iIdProductoBase: " + iIdProductoBase);
		      System.out.println("iIdAlmacen: " + iIdAlmacen);
		      System.out.println("sCveUsuario: " + sCveUsuario);
	      }
	      
	      callst.execute(); //  ------->>>>> ERROR Occur
	      
	      if(bDebug){
	    	  System.out.println("Despues de sReporteVtaCteAlm EN DAO");
	      }
	      
	      sIdResultado[0] = callst.getString(1);
	      sIdResultado[1] = null;
	      
	      return sIdResultado;

	}
  
  
  public InvoiceVO getInvoiceHeader(String companyId, String idNota) throws SQLException {

      InvoiceVO invoiceHeader = new InvoiceVO();
      sSentenciaSql = "select ID_EMPRESA, ID_NOTA, NAME, STREET_ADDRESS, CITY_ADDRESS, INVOICE_DATE, \n"
              + "INVOICE_MUMBER, PO_NUMBER, DUE_DATE, SHIPPING_METHOD\n"
              + "from INVOICE_HEADER_V\n"
              + "WHERE ID_EMPRESA = " + companyId + "\n"
              + "AND ID_NOTA = " + idNota;

      ejecutaSentencia();

      if (rs.next()) {
    	  System.out.println("SI TIENE DATOS EL HEADER ");
          invoiceHeader.setCityAddress(rs.getString("CITY_ADDRESS"));
          invoiceHeader.setCompanyName(rs.getString("NAME"));
          invoiceHeader.setDueDate(rs.getString("DUE_DATE"));
          invoiceHeader.setInvoiceDate(rs.getString("INVOICE_DATE"));
          invoiceHeader.setInvoiceNumber(rs.getString("INVOICE_MUMBER"));
          invoiceHeader.setPoNumber(rs.getString("PO_NUMBER"));
          invoiceHeader.setShippingMethod(rs.getString("SHIPPING_METHOD"));
          invoiceHeader.setStreetAddress(rs.getString("STREET_ADDRESS"));
          invoiceHeader.setBillingAddress(invoiceHeader.getStreetAddress() + "\n" + invoiceHeader.getCityAddress());
      }
      return invoiceHeader;
  }

  public ResultSet getInvoiceData(String companyId, String idNota) throws SQLException {

      sSentenciaSql = "SELECT ID_EMPRESA, ID_NOTA, ITEM, NVL(DESCRIPTION, ' ') AS DESCRIPTION, QTY_HOURS, PRICE_RATE, TOTAL\n"
              + "FROM INVOICE_DETAIL_V \n"
              + "WHERE ID_EMPRESA = " + companyId + "\n"
              + "AND ID_NOTA = " + idNota;

      ejecutaSentencia();

      return rs;
  }

  public String getInvoiceTotal(String companyId, String idNota) throws SQLException {

      sSentenciaSql = "SELECT SUM(TOTAL) AS TOTAL\n"
              + "FROM INVOICE_DETAIL_V \n"
              + "WHERE ID_EMPRESA = " + companyId + "\n"
              + "AND ID_NOTA = " + idNota;

      ejecutaSentencia();

      if (rs.next()) {
          return rs.getString("TOTAL");
      }
      return "";
  }
  
  public boolean esRolAbreMovtoCerrado(int iIdEmpresa, String sIdRol, boolean bDebug) throws SQLException {

	  boolean bEsRolAbreMovtoCerrado = false;

		sSentenciaSql =	"	SELECT  'V' AS B_ES_ROL 				\n" +
						" 	FROM    AOS_ARQ_CAT_LISTADO_VALOR_DET 	\n" +
						" 	WHERE   ID_EMPRESA 		= ?  			\n" +
						"     	AND ID_LISTADO		= 105 			\n" +
						"     	AND VALOR 			= ?  			\n" +
						" 		AND SIT_VALOR		= 'AC' 			\n" ;
		
		if(bDebug){
			System.out.println("  QUERY VerificaRolAbreMovtoCerrado EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" ID_EMPRESA:	" + iIdEmpresa + "\n" +
					" VALOR: 		" + sIdRol + "\n"); 
		}

		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setString(2, sIdRol);

		rs = pstmt.executeQuery();

		if( rs.next() ){
			bEsRolAbreMovtoCerrado	= true;
		}

		rs.close();
		pstmt.close();
		return bEsRolAbreMovtoCerrado;
  }
  
  public String getExistenciaOtrosProductos(int iIdEmpresa, int iIdProducto, int iIdAlmacen, String sCveColor, boolean bDebug) throws SQLException {

	  String sExistencia = "0";

		sSentenciaSql =	"	SELECT  COUNT(1) AS EXISTENCIA			\n" +
						" 	FROM    PRODUCTO_INVENTARIO 			\n" +
						" 	WHERE   ID_EMPRESA 				= ?  	\n" +
						"     	AND ID_PRODUCTO 			= ?  	\n" +
						"     	AND ID_ALMACEN 				= ?  	\n" + (sCveColor!=null&&!sCveColor.equals("")?
						"     	AND CVE_COLOR  				= ?  	\n" :"") +
						" 		AND SIT_PRODUCTO_INVENTARIO	= 'IN' 	";
		
		if(bDebug){
			System.out.println("  QUERY getExistenciaOtrosProductos EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" ID_EMPRESA: 		" + iIdEmpresa + "\n" +
					" ID_PRODUCTO: 		" + iIdProducto + "\n" + 
					" ID_ALMACEN: 		" + iIdAlmacen + "\n" +
					" CVE_COLOR: 		" + sCveColor + "\n"); 
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setInt(2, iIdProducto);
		pstmt.setInt(3, iIdAlmacen);
		if(sCveColor!=null&&!sCveColor.equals("")){
			pstmt.setString(4, sCveColor);
		}
		
		rs = pstmt.executeQuery();

		if( rs.next() ){
			sExistencia  				= rs.getString("EXISTENCIA");
		}
		
		rs.close();
		pstmt.close();
		return sExistencia;
  }
  

  public String sAltaModOtrosProds(int iIdEmpresa, int iIdNota, int iIdMovimiento, 
		  	int iIdProducto, int iCantidad, String sUsuario, String sTipoOperacion, String sCveColor, float fPrecio, 
		  	String sSitMovto, int iIdRol, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "";
		
		if(sTipoOperacion.equals("A")){
			
			int vlIdMovimiento = 0;
			
			sSentenciaSql = "SELECT SEQ_MOVIMIENTO.NEXTVAL AS ID_MOVTO FROM DUAL";

			ejecutaSentencia();

			if(rs.next()){
				vlIdMovimiento = rs.getInt("ID_MOVTO");			
			}
				
			
			sSentenciaSql = 
				"INSERT INTO MOVIMIENTO( \n " +
				"      	ID_EMPRESA, ID_NOTA, F_MOVIMIENTO, ID_CLIENTE, ID_ALMACEN, TIPO_MOVIMIENTO, SIT_MOVIMIENTO,  \n " +
				"  		ID_PRODUCTO, FH_CREACION, FH_ULTIMA_MODIF, USUARIO_CREACION, \n " +
				"  		USUARIO_ULTIMA_MODIF, ID_MOVIMIENTO, CANTIDAD, CVE_COLOR, PRECIO) \n " +
			    "SELECT ID_EMPRESA, ID_NOTA, F_NOTA, ID_CLIENTE, ID_ALMACEN, TIPO_NOTA, 'AC', \n " +
				"        ?,SYSDATE,SYSDATE,?,?,?,?,?,? \n " +
			    "FROM 	NOTA 			\n " +
			    "WHERE	ID_EMPRESA = ?  \n " +
			    "	AND ID_NOTA    = ?  \n " ;

			if(bDebug){
				System.out.println(sSentenciaSql);
			}

	        pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1,iIdProducto);
			pstmt.setString(2,sUsuario);
			pstmt.setString(3,sUsuario);
			pstmt.setInt(4,vlIdMovimiento);
			pstmt.setInt(5,iCantidad);
			pstmt.setString(6,sCveColor);
			pstmt.setFloat(7,fPrecio);
			pstmt.setInt(8,iIdEmpresa);
			pstmt.setInt(9,iIdNota);

			if(bDebug){
				System.out.println("Antes de ejecutar sAltaModOtrosProds A EN DAO \n" +
						"vlIdMovimiento:		" + vlIdMovimiento 	+ "\n" + 
						"iCantidad:				" + iCantidad 		+ "\n" + 
						"iIdNota:				" + iIdNota 		+ "\n" +
						"sCveColor:				" + sCveColor 		+ "\n" + 
						"fPrecio:				" + fPrecio 		+ "\n" +
						"iIdProducto:			" + iIdProducto 	+ "\n" );
			}
			    
			rs = pstmt.executeQuery();

			if(bDebug){
				System.out.println("Despues de sGetSeries sAltaModOtrosProds A EN DAO");
			}
			
			rs.close();
		    pstmt.close();
		
		    sResultado = String.valueOf(vlIdMovimiento);
		    
		}else{
			
			if(sTipoOperacion.equals("M")){
				
				sSentenciaSql = 
						"UPDATE MOVIMIENTO SET 						\n " +
						"  		FH_ULTIMA_MODIF			= SYSDATE, 	\n " +
						"  		SIT_MOVIMIENTO			= ?,		\n " +
						"  		USUARIO_ULTIMA_MODIF 	= ? 		\n " +
					    " WHERE ID_EMPRESA 				= ? 		\n " +
						"   AND ID_MOVIMIENTO 			= ? 		\n "; 
				
				if(bDebug){
					System.out.println(sSentenciaSql);
				}
				
		        pstmt = conn.prepareStatement(sSentenciaSql);
				pstmt.setString(1,sSitMovto);
				pstmt.setString(2,sUsuario);
				pstmt.setInt(3,iIdEmpresa);
				pstmt.setInt(4,iIdMovimiento);
				
				if(bDebug){
					System.out.println("Antes de ejecutar sAltaModOtrosProds M EN DAO ");
				}
				    
				rs = pstmt.executeQuery();

				if(bDebug){
					System.out.println("Despues de sAltaModOtrosProds M EN DAO");
				}
				
				rs.close();
			    pstmt.close();
			
			    // Verifica si el rol tiene privilegios de cancelar un Movimiento a�n cuando ya est� cerrado
			    if(sSitMovto.equals("CL")){
			    	if(esRolAbreMovtoCerrado(iIdEmpresa, String.valueOf(iIdRol), bDebug)){
			    		sResultado = "1";
			    	}else{
			    		sResultado = "0";
			    	}
			    }else{
			    	sResultado = "0";
			    }
			}
		}
		
		
		CallableStatement callst = conn.prepareCall("{call PCD_ACTUALIZA_IMPORTE_NOTA(?,?)}");
		callst.setInt(1,iIdEmpresa);
		callst.setInt(2,iIdNota);
		callst.execute(); 
  
		if(bDebug){
			System.out.println("Despues de PCD_ACTUALIZA_IMPORTE_NOTA EN DAO");
	  	}

	      return sResultado;

	}
  
  

  public String sMergeMinInvProdBase(int iIdEmpresa, int iIdAlmacen, int iIdProductoBase, int iIdSerie, 
		  float fValHorizontal, float fValVertical, int iCantidad, String sUsuario, int iIdRol, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "0";
		
		sSentenciaSql = 
			"		MERGE INTO PROD_BASE_EXIST_CONF_MIN  A 								\n " + 
            "            USING (  														\n " + 
            "                SELECT  ? AS ID_EMPRESA, 									\n " +  
            "                        ? AS ID_ALMACEN,  									\n " + 
            "                        ? AS ID_PRODUCTO_BASE,  							\n " + 
            "                        ? AS ID_SERIE,  									\n " + 
            "                        ? AS VALOR_HORIZONTAL, 							\n " +  
            "                        ? AS VALOR_VERTICAL,  								\n " + 
            "                        ? AS MINIMO_EXISTENCIA 							\n " + 
            "                FROM DUAL ) B 												\n " + 
            "           ON (     A.ID_EMPRESA       = B.ID_EMPRESA 						\n " + 
            "                AND A.ID_ALMACEN 		= B.ID_ALMACEN 						\n " + 
            "                AND A.ID_PRODUCTO_BASE = B.ID_PRODUCTO_BASE 				\n " + 
            "                AND A.ID_SERIE         = B.ID_SERIE 						\n " + 
            "                AND A.VALOR_HORIZONTAL = B.VALOR_HORIZONTAL 				\n " + 
            "                AND A.VALOR_VERTICAL   = B.VALOR_VERTICAL)  				\n " + 
            "        WHEN MATCHED THEN  												\n " + 
            "            UPDATE SET A.MINIMO_EXISTENCIA = B.MINIMO_EXISTENCIA 			\n " +  
            "			 DELETE WHERE B.MINIMO_EXISTENCIA = 0  							\n " +
            "        WHEN NOT MATCHED THEN 												\n " + 
            "            INSERT (A.ID_EMPRESA, A.ID_ALMACEN, A.ID_PRODUCTO_BASE, A.ID_SERIE, A.VALOR_HORIZONTAL, A.VALOR_VERTICAL, A.MINIMO_EXISTENCIA) 	\n " + 
            "            VALUES (B.ID_EMPRESA, B.ID_ALMACEN, B.ID_PRODUCTO_BASE, B.ID_SERIE, B.VALOR_HORIZONTAL, B.VALOR_VERTICAL, B.MINIMO_EXISTENCIA) 	\n " +
            "			 WHERE  B.MINIMO_EXISTENCIA > 0 ";

			if(bDebug){
				System.out.println(sSentenciaSql);
			}

	        pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1,iIdEmpresa);
			pstmt.setInt(2,iIdAlmacen);
			pstmt.setInt(3,iIdProductoBase);
			pstmt.setInt(4,iIdSerie);
			pstmt.setFloat(5,fValHorizontal);
			pstmt.setFloat(6,fValVertical);
			pstmt.setInt(7,iCantidad);

			if(bDebug){
				System.out.println("Antes de ejecutar sMergeMinInvProdBase A EN DAO \n" +
						"iIdEmpresa:		" + iIdEmpresa 		+ "\n" + 
						"iIdAlmacen:		" + iIdAlmacen 		+ "\n" + 
						"iIdProductoBase:	" + iIdProductoBase + "\n" + 
						"iIdSerie:			" + iIdSerie 		+ "\n" +
						"iValHorizontal:	" + fValHorizontal 	+ "\n" + 
						"iValVertical:		" + fValVertical 	+ "\n" +
						"iCantidad:			" + iCantidad 		+ "\n" );
			}
			    
			rs = pstmt.executeQuery();

			if(bDebug){
				System.out.println("Despues de sMergeMinInvProdBase A EN DAO");
			}
			
			rs.close();
		    pstmt.close();
		
		    sResultado = "1";
		    
		    return sResultado;

	}
  
  
  public String sGetSeriesInvMenorMin(int sIdEmpresa, int iIdAlmacen, int iIdProductoBase, String sUsuario, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "";

		CallableStatement callst = conn.prepareCall("{ ? = call FN_GENERA_MAT_INV_MENOR_MIN(?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.CLOB);
	      callst.setInt(2,sIdEmpresa);
	      callst.setInt(3,iIdAlmacen);
	      callst.setInt(4,iIdProductoBase);
	      callst.setString(5,sUsuario);

	      if(bDebug){
	    	  System.out.println("Antes de ejecutar sGetSeriesInvMenorMin EN DAO ");
	      }

	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de sGetSeriesInvMenorMin EN DAO");
	      }

	      sResultado = callst.getString(1);

	      return sResultado;

	}
  

  public String sGetSeriesMinInv(int sIdEmpresa, int iIdAlmacen, int iIdProductoBase, String sUsuario, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "";

		CallableStatement callst = conn.prepareCall("{ ? = call FN_GENERA_MATRIZ_MIN_INV(?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.CLOB);
	      callst.setInt(2,sIdEmpresa);
	      callst.setInt(3,iIdAlmacen);
	      callst.setInt(4,iIdProductoBase);
	      callst.setString(5,sUsuario);

	      if(bDebug){
	    	  System.out.println("Antes de ejecutar sGetSeriesMinInv EN DAO ");
	      }

	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de sGetSeriesMinInv Archivo EN DAO");
	      }

	      sResultado = callst.getString(1);

	      return sResultado;

	}
  
  
public static String resultSetToJson(Connection connection, String query, Object[] params) {
      List<Map<String, Object>> listOfMaps = null;
      try {
          QueryRunner queryRunner = new QueryRunner();
          listOfMaps = queryRunner.query(connection, query, new MapListHandler(), params);
      } catch (SQLException se) {
          throw new RuntimeException("Couldn't query the database.", se);
      } //finally {DbUtils.closeQuietly(connection);}
      return new Gson().toJson(listOfMaps);
  }





public MatrizAdmDatosConf getMatrizAdmDatosConf(int iIdEmpresa, String sCveConfiguracion, boolean bDebug) throws SQLException{

	MatrizAdmDatosConf conf = null; 

	sSentenciaSql = "SELECT  TIPO_OPERACION, NOM_TABLA, PARAMETROS, QUERY_ADM	\n" +
            		"FROM    AOS_ARQ_CONF_ADM_MATRIZ_PB	\n" +
        			"WHERE   ID_EMPRESA 		= ? 		\n" +
        			"	 AND CVE_CONFIGURACION 	= ?";

	if(bDebug){
		System.out.println("  QUERY getMatrizAdmDatosConf nuevo Obtiene query EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
			" iIdEmpresa: 			" + iIdEmpresa 	+ "\n" +
			" sCveConfiguracion: 	" + sCveConfiguracion 	+ "\n");
	}
	
	pstmt = conn.prepareStatement(sSentenciaSql);
	pstmt.setInt(1, iIdEmpresa);
	pstmt.setString(2, sCveConfiguracion);
	
	rs = pstmt.executeQuery();

	if(rs.next()){
		conf = new MatrizAdmDatosConf();
		conf.sTipoOperacion 	= rs.getString("TIPO_OPERACION");
		conf.sNomTabla			= rs.getString("NOM_TABLA");
		conf.sParamsAdmDatos	= rs.getString("PARAMETROS");
		conf.sQueryAdmDatos 	= rs.getString("QUERY_ADM");
	}

	rs.close();
    pstmt.close();

	return conf;
}

public MatrizResultadoConf getMatrizResultadoConf(int iIdEmpresa, String sCveConfiguracion, boolean bDebug) throws SQLException{

	MatrizResultadoConf conf = new MatrizResultadoConf();

	sSentenciaSql = "SELECT  QUERY_DATOS, PARAMETROS_DATOS, QUERY_PLACEHOLDER, PARAMETROS_PLACEHOLDER	\n" +
            		"FROM    AOS_ARQ_CONF_RESULT_MATRIZ_PB	\n" +
        			"WHERE   ID_EMPRESA 		= ? 		\n" +
        			"	 AND CVE_CONFIGURACION 	= ?";

	if(bDebug){
		System.out.println("  QUERY getComboOptionsMap nuevo Obtiene query EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
			" iIdEmpresa: 			" + iIdEmpresa 	+ "\n" +
			" sCveConfiguracion: 	" + sCveConfiguracion 	+ "\n");
	}
	
	pstmt = conn.prepareStatement(sSentenciaSql);
	pstmt.setInt(1, iIdEmpresa);
	pstmt.setString(2, sCveConfiguracion);
	
	rs = pstmt.executeQuery();

	if(rs.next()){
		conf.sQueryDatos 		= rs.getString("QUERY_DATOS");
		conf.sParamsDatos 		= rs.getString("PARAMETROS_DATOS");
		conf.sQueryPlaceholder	= rs.getString("QUERY_PLACEHOLDER");
		conf.sParamsPlaceholder = rs.getString("PARAMETROS_PLACEHOLDER");
	}

	rs.close();
    pstmt.close();

	return conf;
}

public String sAdmDatosMatriz(int iIdEmpresa, MatrizAdmDatosConf conf, 
		  StringTokenizer tokens, LinkedHashMap<String, String> map, 
		  String sCveUsuario, boolean bDebug) throws SQLException{

	  	String sResultado 		= "";
	  	String sParametro 		= "";
		String sNomParametro 	= "";
		String sTipoParametro	= "";
		String sPartes[];
		Object[] params			= new Object[0];
		Gson gson 				= new Gson();

		sSentenciaSql = conf.sQueryAdmDatos;

		if(bDebug){
			System.out.println("  QUERY sAdmDatosMatriz PB EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n"); 
		}

		JsonObject obj = new JsonObject();

		if(tokens!=null&&conf.sTipoOperacion.equals("SELECT")){
			
			params = new Object[tokens.countTokens()];
			
			int i = 0;
			while(tokens.hasMoreTokens()){
				
				sParametro 		= tokens.nextToken();
				sPartes 		= sParametro.split(Pattern.quote("|"));
				sNomParametro 	= sPartes[0];
				sTipoParametro	= sPartes[1];

				if(bDebug){
					System.out.println("sNomParametro: >" + sNomParametro + "<" +  
							"\n sTipoParametro: >" + sTipoParametro + "<" +  
							"\n sParametro: >" + sParametro + "<" +  
							"\n Valor: >" + map.get(sNomParametro) + "<"  + 
							"\n Elementos map: >" + map.size() + "<"  
							);
				}	
				if(sTipoParametro.equals("STRING")){
					params[i] = (String)map.get(sNomParametro);
				}else{
					if(sTipoParametro.equals("INT")){
						params[i] = Integer.parseInt(map.get(sNomParametro));
					}
				}
				if(bDebug){
					System.out.println("  Agrega el parametro en el stmt: " + i + " Parametro: " + sNomParametro);
				}	
				i = i + 1;
			}
			
		}

		sResultado = resultSetToJson(conn, conf.sQueryAdmDatos, params); 
		
		if(bDebug){
			System.out.println("  sResultado sAdmDatosMatriz EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sResultado + "\n" ); 
		}
		
		JsonArray datosResultado = new JsonArray();
		datosResultado  = gson.fromJson(sResultado, JsonArray.class);
		
		obj.add("datosResultado", datosResultado);

		sResultado = obj.toString();
		if(bDebug){
	    	  System.out.println("sAdmDatosMatriz JSON Completo EN DAO \n" + obj.toString());
	    }
		return sResultado;
	}

  public String sGetDatosMatriz(int iIdEmpresa, int iIdProductoBase, MatrizResultadoConf conf, 
		  StringTokenizer tokens, LinkedHashMap<String, String> map, 
		  StringTokenizer tokensPlaceholder, LinkedHashMap<String, String> mapPlaceholder, 
		  String sCveUsuario, boolean bDebug) throws SQLException{
		
	  	String sResultado 		= "";
	  	String sParametro 		= "";
		String sNomParametro 	= "";
		String sTipoParametro	= "";
		String sPartes[];
		
		sSentenciaSql = "SELECT E, P, MH, MV, S, B, T			\n" +
						"FROM   PRODUCTO_BASE_DATOS_MATRIZ_V 	\n" +
                    	"WHERE 	 E	= ? 						\n" +
						"    AND P 	= ?";

		if(bDebug){
			System.out.println("  QUERY sGetDatosMatriz PB EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdEmpresa: 		" + iIdEmpresa 			+ "\n" +
					" iIdProductoBase: 	" + iIdProductoBase 		+ "\n" ); 
		}
		
		
		Object[] params = new Object[2];
		params[0] = (Integer)iIdEmpresa;
		params[1] = (Integer)iIdProductoBase; 
		
		
		sResultado = resultSetToJson(conn, sSentenciaSql, params); 
		
		 Gson gson = new Gson();
		 JsonArray datosPB = new JsonArray();
		 datosPB  = gson.fromJson(sResultado, JsonArray.class);
		 
		 JsonObject obj = new JsonObject();
		 
		 obj.add("datosPB", datosPB);
		 
		 
		if(bDebug){
			System.out.println("  datosPB EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + datosPB.toString() + "\n" ); 
		}

		 
		 
		 sSentenciaSql ="SELECT E, P, I, N, VHI, VHF, VHT, VVI, VVF, VVT, NS	\n" +
						"FROM   SERIE_DATOS_MATRIZ_V 	\n" +
						"WHERE 	 E	= ? 						\n" +
						"    AND P 	= ?";
	
		if(bDebug){
			System.out.println("  QUERY sGetDatosMatriz SERIE EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdEmpresa: 		" + iIdEmpresa 			+ "\n" +
					" iIdProductoBase: 	" + iIdProductoBase 	+ "\n" ); 
		}
		
		 
		sResultado = resultSetToJson(conn, sSentenciaSql, params); 
		
		if(bDebug){
			System.out.println("  sResultado SERIES EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sResultado + "\n" ); 
		}
		
		JsonArray datosSerie = new JsonArray(); 
		datosSerie  = gson.fromJson(sResultado, JsonArray.class);
		
		obj.add("datosSerie", datosSerie);
		
		if(tokens!=null){
			
			params = new Object[tokens.countTokens()];
			
			int i = 0;
			while(tokens.hasMoreTokens()){
				
				sParametro 		= tokens.nextToken();
				sPartes 		= sParametro.split(Pattern.quote("|"));
				sNomParametro 	= sPartes[0];
				sTipoParametro	= sPartes[1];
				if(bDebug){
					System.out.println("sNomParametro: >" + sNomParametro + "<" +  
							"\n sTipoParametro: >" + sTipoParametro + "<" +  
							"\n sParametro: >" + sParametro + "<" +  
							"\n Valor: >" + map.get(sNomParametro) + "<"  + 
							"\n Elementos map: >" + map.size() + "<"  
							);
				}	
				if(sTipoParametro.equals("STRING")){
					params[i] = (String)map.get(sNomParametro);
				}else{
					if(sTipoParametro.equals("INT")){
						params[i] = Integer.parseInt(map.get(sNomParametro));
					}
				}
				if(bDebug){
					System.out.println("  Agrega el parametro en el stmt: " + i + " Parametro: " + sNomParametro);
				}	
				i = i + 1;
			}
			
			sResultado = resultSetToJson(conn, conf.sQueryDatos, params); 
			
			if(bDebug){
				System.out.println("  sResultado SERIES EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sResultado + "\n" ); 
			}
			
			JsonArray datosResultado = new JsonArray();
			datosResultado  = gson.fromJson(sResultado, JsonArray.class);
			
			obj.add("datosResultado", datosResultado);
			
		}
		
		//Majeno para placeholder
		if(tokensPlaceholder!=null){
			
			params = new Object[tokensPlaceholder.countTokens()];
			
			int i = 0;
			while(tokensPlaceholder.hasMoreTokens()){
				
				sParametro 		= tokensPlaceholder.nextToken();
				sPartes 		= sParametro.split(Pattern.quote("|"));
				sNomParametro 	= sPartes[0];
				sTipoParametro	= sPartes[1];
				if(bDebug){
					System.out.println("sNomParametro: >" + sNomParametro + "<" +  
							"\n sTipoParametro: >" + sTipoParametro + "<" +  
							"\n sParametro: >" + sParametro + "<" +  
							"\n Valor: >" + mapPlaceholder.get(sNomParametro) + "<"  + 
							"\n Elementos map: >" + mapPlaceholder.size() + "<"  
							);
				}	
				if(sTipoParametro.equals("STRING")){
					params[i] = (String)mapPlaceholder.get(sNomParametro);
				}else{
					if(sTipoParametro.equals("INT")){
						params[i] = Integer.parseInt(mapPlaceholder.get(sNomParametro));
					}
				}
				if(bDebug){
					System.out.println("  Agrega el parametro en el stmt tokensPlaceholder: " + i + " Parametro: " + sNomParametro);
				}	
				i = i + 1;
			}
			
			sResultado = resultSetToJson(conn, conf.sQueryPlaceholder, params); 
			
			if(bDebug){
				System.out.println("  sResultado tokensPlaceholder EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sResultado + "\n" ); 
			}
			
			JsonArray datosResultado = new JsonArray();
			datosResultado  = gson.fromJson(sResultado, JsonArray.class);
			
			obj.add("datosPlaceholder", datosResultado);
		}
		
		sResultado = obj.toString();
		if(bDebug){
	    	  System.out.println("JSON Completo EN DAO \n" + obj.toString());
	    }
		return sResultado;
	}
  
  
  
  	public String sGetOtrosProdsNotaVta(LinkedHashMap<String, Object> map) throws SQLException{
		
		int iIdEmpresa 			= (int)map.get("DYN_CTL_ID_EMPRESA");
		int iIdNota 			= (int)map.get("DYN_CTL_ID_NOTA");
		boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
		int iIdRol				= (int)map.get("DYN_CTL_ID_ROL");
		Object[] params 		= new Object[0];
		String sResultado 		= "";
		
		sSentenciaSql =	"SELECT  M.ID_MOVIMIENTO AS A, M.ID_PRODUCTO AS B, P.DESC_PRODUCTO AS C, M.CVE_COLOR AS D, 	\n " +
						"		 COL.DESC_COLOR AS E, '$'||TRIM(TO_CHAR(M.PRECIO,'999,999,999.00')) AS F, 			\n " +
					    "    	 M.CANTIDAD AS G, N.SIT_NOTA AS H, M.SIT_MOVIMIENTO AS I, R.VALOR AS J				\n " +	
					    "FROM    NOTA N																				\n " +
					    "    JOIN MOVIMIENTO M 																		\n " +
					    "        ON  N.ID_EMPRESA 	= M.ID_EMPRESA													\n " +
					    "        AND N.ID_NOTA   	= M.ID_NOTA														\n " +
					    "    LEFT JOIN CAT_COLOR COL 																\n " +
					    "        ON  COL.ID_EMPRESA = M.ID_EMPRESA													\n " +
					    "        AND COL.CVE_COLOR 	= M.CVE_COLOR													\n " +	
					    "    JOIN CAT_PRODUCTO_V P 																	\n " +	
					    "        ON  M.ID_EMPRESA 	= P.ID_EMPRESA													\n " +
					    "        AND M.ID_PRODUCTO 	= P.ID_PRODUCTO													\n " +	
					    "	LEFT JOIN AOS_ARQ_CAT_LISTADO_VALOR_DET R												\n " +
					    "        ON  R.ID_EMPRESA 	= N.ID_EMPRESA													\n " +
					    "        AND R.ID_LISTADO	= 105	-- Roles with privileges to cancel closed transactions	\n " +
					    "        AND R.VALOR      	= ?																\n " +
					    "WHERE   N.ID_EMPRESA   	= ?																\n " +
					    "    AND N.ID_NOTA       	= ?																\n " +
					    "    AND N.TIPO_NOTA 		= 'SALIDA'														\n " +
					    "    AND M.ID_PRODUCTO IS NOT NULL															\n " +
					    "    AND M.SIT_MOVIMIENTO 	<> 'CA'															\n " +
					    "ORDER BY M.ID_MOVIMIENTO 																	\n " ;

		if(bDebug){
			System.out.println("  QUERY sGetOtrosProdsNotaVta GET TABLA EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdRol: 		" + iIdRol + "\n" +  
					" iIdNota: 		" + iIdNota + "\n"); 
		}
		
		params 		= new Object[3];
		params[0] 	= (String)String.valueOf(iIdRol);
		params[1] 	= (Integer)iIdEmpresa;
		params[2] 	= (Integer)iIdNota;
		
		sResultado = resultSetToJson(conn, sSentenciaSql, params); 
		
		Gson gson 				= new Gson();
		JsonArray datosMovtos 	= new JsonArray();
		datosMovtos  			= gson.fromJson(sResultado, JsonArray.class);
		JsonObject obj 		= new JsonObject();
		obj.add("datosMovtos", datosMovtos);
 
		sResultado = obj.toString();

		if(bDebug){
			  System.out.println("JSON Completo EN DAO \n" + obj.toString());
		}
		return sResultado;
	}
  
  public static void main4(String[] args) {
      String json = "{\"results\":[{\"lat\":\"value\",\"lon\":\"value\" }, { \"lat\":\"value\", \"lon\":\"value\"}]}";
      Gson gson = new Gson();
      JsonObject inputObj  = gson.fromJson(json, JsonObject.class);
      JsonObject newObject = new JsonObject() ;
      newObject.addProperty("lat", "newValue");
      newObject.addProperty("lon", "newValue");
      inputObj.get("results").getAsJsonArray().add(newObject);
      System.out.println(inputObj);
  }

}
