package com.dinamicaps.web.impl.domain.optics;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import com.dinamicaps.web.impl.Servicio;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.dinamicaps.web.impl.ServicioConexion;
import com.dinamicaps.web.impl.domain.Usuario;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class OpticsDAO_bk extends ServicioConexion{

  public String sGetSeries(int sIdEmpresa, int iIdProductoBase, int iIdMovimiento, String sUsuario, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "";

		CallableStatement callst = conn.prepareCall("{ ? = call FN_GENERA_MATRIZ_SERIE(?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.CLOB);
	      callst.setInt(2,sIdEmpresa);
	      callst.setInt(3,iIdProductoBase);
	      callst.setInt(4,iIdMovimiento);
	      callst.setString(5,sUsuario);
	      
	      if(bDebug){
	    	  System.out.println("Antes de ejecutar sGetSeries EN DAO ");
	      }
	      
	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de sGetSeries Archivo EN DAO");
	      }
	      
	      sResultado = callst.getString(1);
	      
	      return sResultado;

	}
  
  public String sGetSeriesVta(int sIdEmpresa, int iIdProductoBase, int iIdAlmacen, int iIdMovimiento, String sUsuario, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "";

		CallableStatement callst = conn.prepareCall("{ ? = call FN_GENERA_MATRIZ_SERIE_VTA(?,?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.CLOB);
	      callst.setInt(2,sIdEmpresa);
	      callst.setInt(3,iIdProductoBase);
	      callst.setInt(4,iIdAlmacen);
	      callst.setInt(5,iIdMovimiento);
	      callst.setString(6,sUsuario);
	      
	      if(bDebug){
	    	  System.out.println("Antes de ejecutar sGetSeries EN DAO ");
	      }
	      
	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de sGetSeries Archivo EN DAO");
	      }
	      
	      sResultado = callst.getString(1);
	      
	      return sResultado;

	}
  
  public String[] sGeneraPreMovto(int sIdEmpresa, String sIdPreMovto, String sCveProducto, int iCantidad, 
		  String sTipoOperacion, String sTipoMovimiento, int iIdAlmacen, String sTipoDevolucion, 
		  String sCveUsuario, int iIdDevolucion, String sBSincroniza, int iIdRol, int iIdNota, boolean bDebug) throws SQLException,  IOException{

		String sIdResultado[] = new String[2];

		CallableStatement callst = conn.prepareCall("{call PCD_GENERA_PRE_MOVTO(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
	      //callst.registerOutParameter(1, java.sql.Types.INTEGER);
	      callst.setInt(1,sIdEmpresa);
	      callst.registerOutParameter(2, java.sql.Types.VARCHAR);
	      callst.setString(2,sIdPreMovto);
	      callst.setString(3,sCveProducto);
	      callst.setInt(4,iCantidad);
	      callst.setString(5,sTipoOperacion);
	      callst.setString(6,sTipoMovimiento);
	      callst.setInt(7,iIdAlmacen);
	      callst.setString(8,sTipoDevolucion);
	      callst.setString(9,sCveUsuario);
	      callst.setInt(10,iIdDevolucion);
	      callst.setString(11,sBSincroniza);
	      callst.setInt(12,iIdRol);
	      callst.registerOutParameter(13, java.sql.Types.VARCHAR);
	      callst.setInt(14,iIdNota);

	      
	      if(bDebug){
		      System.out.println("Antes de ejecutar iGeneraPreMovto EN DAO ");
		      
		      System.out.println("sIdEmpresa: " + sIdEmpresa);
		      System.out.println("sIdPreMovto: " + sIdPreMovto);
		      System.out.println("sCveProducto: " + sCveProducto);
		      System.out.println("iCantidad: " + iCantidad);
		      System.out.println("sTipoOperacion: " + sTipoOperacion);
		      System.out.println("sTipoMovimiento: " + sTipoMovimiento);
		      System.out.println("sTipoDevolucion: " + sTipoDevolucion);
		      System.out.println("sBSincroniza: " + sBSincroniza);
		      System.out.println("iIdNota: " + iIdNota);
	      }
	      
	      callst.execute(); //  ------->>>>> ERROR Occur
	      
	      if(bDebug){
	    	  System.out.println("Despues de iGeneraPreMovto EN DAO");
	      }
	      
	      sIdResultado[0] = callst.getString(2);
	      sIdResultado[1] = callst.getString(13);
	      
	      return sIdResultado;

	}
  
  public String sAltaModEntrada(int iIdEmpresa, int iIdNota, int iIdMovimiento, 
		  	int iIdProductoBase, String sTxReferencia, String sUsuario, String sSituacion, String sTipoOperacion, boolean bDebug) throws SQLException,  IOException{


		String sResultado = "";
		
		if(sTipoOperacion.equals("A")){

			sSentenciaSql = 
					"INSERT INTO MOVIMIENTO( \n " +
					"      	ID_EMPRESA, ID_NOTA, F_MOVIMIENTO, ID_PROVEEDOR, ID_ALMACEN, TIPO_MOVIMIENTO, SIT_MOVIMIENTO,  \n " +
					"  		ID_PRODUCTO_BASE, TX_REFERENCIA, FH_CREACION, FH_ULTIMA_MODIF, USUARIO_CREACION, \n " +
					"  		USUARIO_ULTIMA_MODIF, ID_MOVIMIENTO) \n " +
				    "SELECT ID_EMPRESA, ID_NOTA, F_NOTA, ID_PROVEEDOR, ID_ALMACEN, TIPO_NOTA, 'IS', \n " +
					"        ?,?, SYSDATE,SYSDATE,?,?,? \n " +
				    "FROM 	NOTA 			\n " +
				    "WHERE	ID_EMPRESA = ?  \n " +
				    "	AND ID_NOTA    = ?  \n " ;

			if(bDebug){
				System.out.println(sSentenciaSql);
			}
			
	        pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1,iIdProductoBase);
			pstmt.setString(2,sTxReferencia);
			pstmt.setString(3,sUsuario);
			pstmt.setString(4,sUsuario);
			pstmt.setInt(5,iIdMovimiento);
			pstmt.setInt(6,iIdEmpresa);
			pstmt.setInt(7,iIdNota);
	
						
			if(bDebug){
				System.out.println("Antes de ejecutar sGetSeries EN DAO ");
			}
			    
			rs = pstmt.executeQuery();

			if(bDebug){
				System.out.println("Despues de sGetSeries Archivo EN DAO");
			}
			
			rs.close();
		    pstmt.close();
		
		    Servicio servicio = new Servicio();
		    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
		    
		}else{
			
			if(sTipoOperacion.equals("M")){
				
				if(bDebug){
					System.out.println("sSituacion EN DAO:  " + sSituacion);
					
				}

				sSentenciaSql = 
						" UPDATE MOVIMIENTO SET \n " +
								"  TX_REFERENCIA= ?,	  \n " +
								"  FH_ULTIMA_MODIF= SYSDATE, \n " +
								(sSituacion==null?"":"  SIT_MOVIMIENTO= ?, \n ") +
								"  USUARIO_ULTIMA_MODIF = ? \n " +
					    " WHERE ID_EMPRESA = ? \n " +
						"   AND ID_MOVIMIENTO = ? \n "; 
				
					if(bDebug){
						System.out.println(sSentenciaSql);
					}
					
			        pstmt = conn.prepareStatement(sSentenciaSql);
					pstmt.setString(1,sTxReferencia);
					if(sSituacion!=null){
						pstmt.setString(2,sSituacion);
						pstmt.setString(3,sUsuario);
						pstmt.setInt(4,iIdEmpresa);
						pstmt.setInt(5,iIdMovimiento);
					}else{
						pstmt.setString(2,sUsuario);
						pstmt.setInt(3,iIdEmpresa);
						pstmt.setInt(4,iIdMovimiento);
					}
					
					
					if(bDebug){
						System.out.println("Antes de ejecutar MODIFICA MOVTO EN DAO ");
					}
					    
					rs = pstmt.executeQuery();

					if(bDebug){
						System.out.println("Despues de MODIFICA MOVTO EN DAO");
					}
					
					rs.close();
				    pstmt.close();
				
				    Servicio servicio = new Servicio();
				    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
				
			}
		}
		
		
		CallableStatement callst = conn.prepareCall("{call PCD_ACTUALIZA_IMPORTE_NOTA(?,?)}");
		callst.setInt(1,iIdEmpresa);
		callst.setInt(2,iIdNota);
		callst.execute(); 
  
		if(bDebug){
			System.out.println("Despues de PCD_ACTUALIZA_IMPORTE_NOTA EN DAO");
	  	}
	        
		
	      return sResultado;

	}
  

  public String sAltaModAjusSuma(int iIdEmpresa, int iIdMovimiento, String sFMovimiento, int iIdAlmacen, 
		  	int iIdProductoBase, String sTxReferencia, String sUsuario, String sSituacion, String sTipoOperacion, boolean bDebug) throws SQLException,  IOException{


		String sResultado = "";
		
		if(sTipoOperacion.equals("A")){

			sSentenciaSql = 
					" INSERT INTO MOVIMIENTO( \n " +
							"  ID_EMPRESA, ID_MOVIMIENTO, F_MOVIMIENTO, ID_ALMACEN, ID_PRODUCTO_BASE, \n " +
							"  TX_REFERENCIA, TIPO_MOVIMIENTO, FH_CREACION, FH_ULTIMA_MODIF, USUARIO_CREACION, USUARIO_ULTIMA_MODIF, \n " +
							"  SIT_MOVIMIENTO) \n " +
				    " VALUES(?,?,TO_DATE('" + sFMovimiento + "','DD/MM/YYYY'),?,?,?,'AJUSTE_SUMA', SYSDATE,SYSDATE,?,?,'AC') \n " ;


			if(bDebug){
				System.out.println(sSentenciaSql);
			}
			
		        pstmt = conn.prepareStatement(sSentenciaSql);
				pstmt.setInt(1,iIdEmpresa);
				pstmt.setInt(2,iIdMovimiento);
				pstmt.setInt(3,iIdAlmacen);
				pstmt.setInt(4,iIdProductoBase);
				pstmt.setString(5,sTxReferencia);
				pstmt.setString(6,sUsuario);
				pstmt.setString(7,sUsuario);
		
				
				if(bDebug){
					System.out.println("Antes de ejecutar sGetSeries EN DAO ");
				}
				    
				rs = pstmt.executeQuery();

				if(bDebug){
					System.out.println("Despues de sGetSeries Archivo EN DAO");
				}
				
				rs.close();
			    pstmt.close();
			
			    Servicio servicio = new Servicio();
			    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
		}else{
			
			if(sTipoOperacion.equals("M")){
				
				sSentenciaSql = 
						" UPDATE MOVIMIENTO SET \n " +
								"  F_MOVIMIENTO = TO_DATE('" + sFMovimiento + "','DD/MM/YYYY'), \n " +
								"  ID_ALMACEN	= ?,  \n " +
								"  TX_REFERENCIA= ?,	  \n " +
								"  FH_ULTIMA_MODIF= SYSDATE, \n " +
								(sSituacion==null?"":"  SIT_MOVIMIENTO= ?, \n ") +
								"  USUARIO_ULTIMA_MODIF = ? \n " +
					    " WHERE ID_EMPRESA = ? \n " +
						"   AND ID_MOVIMIENTO = ? \n "; 
				
					if(bDebug){
						System.out.println(sSentenciaSql);
					}
					
			        pstmt = conn.prepareStatement(sSentenciaSql);
					pstmt.setInt(1,iIdAlmacen);
					pstmt.setString(2,sTxReferencia);
					if(sSituacion!=null){
						pstmt.setString(3,sSituacion);
						pstmt.setString(4,sUsuario);
						pstmt.setInt(5,iIdEmpresa);
						pstmt.setInt(6,iIdMovimiento);
					}else{
						pstmt.setString(3,sUsuario);
						pstmt.setInt(4,iIdEmpresa);
						pstmt.setInt(5,iIdMovimiento);
					}
					
					
					if(bDebug){
						System.out.println("Antes de ejecutar MODIFICA MOVTO EN DAO ");
					}
					    
					rs = pstmt.executeQuery();

					if(bDebug){
						System.out.println("Despues de MODIFICA MOVTO EN DAO");
					}
					
					rs.close();
				    pstmt.close();
				
				    Servicio servicio = new Servicio();
				    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
				
			}
		}
		
	      
	      return sResultado;

	}

  
  public String sAltaModSalida(int iIdEmpresa, int iIdNota, int iIdMovimiento, 
		  	int iIdProductoBase, String sTxReferencia, String sUsuario, String sSituacion, String sTipoOperacion, boolean bDebug) throws SQLException,  IOException{


		String sResultado = "";
		
		if(sTipoOperacion.equals("A")){

			sSentenciaSql = 
				"INSERT INTO MOVIMIENTO( \n " +
				"      	ID_EMPRESA, ID_NOTA, F_MOVIMIENTO, ID_CLIENTE, ID_ALMACEN, TIPO_MOVIMIENTO, SIT_MOVIMIENTO,  \n " +
				"  		ID_PRODUCTO_BASE, TX_REFERENCIA, FH_CREACION, FH_ULTIMA_MODIF, USUARIO_CREACION, \n " +
				"  		USUARIO_ULTIMA_MODIF, ID_MOVIMIENTO) \n " +
			    "SELECT ID_EMPRESA, ID_NOTA, F_NOTA, ID_CLIENTE, ID_ALMACEN, TIPO_NOTA, 'AC', \n " +
				"        ?,?, SYSDATE,SYSDATE,?,?,? \n " +
			    "FROM 	NOTA 			\n " +
			    "WHERE	ID_EMPRESA = ?  \n " +
			    "	AND ID_NOTA    = ?  \n " ;
	
			
			if(bDebug){
				System.out.println(sSentenciaSql);
			}

	        pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1,iIdProductoBase);
			pstmt.setString(2,sTxReferencia);
			pstmt.setString(3,sUsuario);
			pstmt.setString(4,sUsuario);
			pstmt.setInt(5,iIdMovimiento);
			pstmt.setInt(6,iIdEmpresa);
			pstmt.setInt(7,iIdNota);

			if(bDebug){
				System.out.println("Antes de ejecutar sGetSeries EN DAO ");
			}
			    
			rs = pstmt.executeQuery();

			if(bDebug){
				System.out.println("Despues de sGetSeries Archivo EN DAO");
			}
			
			rs.close();
		    pstmt.close();
		
		    Servicio servicio = new Servicio();
		    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
		    
		}else{
			
			if(sTipoOperacion.equals("M")){
				
				sSentenciaSql = 
						" UPDATE MOVIMIENTO SET \n " +
								"  TX_REFERENCIA= ?,	  \n " +
								"  FH_ULTIMA_MODIF= SYSDATE, \n " +
								(sSituacion==null?"":"  SIT_MOVIMIENTO= ?, \n ") +
								"  USUARIO_ULTIMA_MODIF = ? \n " +
					    " WHERE ID_EMPRESA = ? \n " +
						"   AND ID_MOVIMIENTO = ? \n "; 
				
					if(bDebug){
						System.out.println(sSentenciaSql);
					}
					
			        pstmt = conn.prepareStatement(sSentenciaSql);
					pstmt.setString(1,sTxReferencia);
					if(sSituacion!=null){
						pstmt.setString(2,sSituacion);
						pstmt.setString(3,sUsuario);
						pstmt.setInt(4,iIdEmpresa);
						pstmt.setInt(5,iIdMovimiento);
					}else{
						pstmt.setString(2,sUsuario);
						pstmt.setInt(3,iIdEmpresa);
						pstmt.setInt(4,iIdMovimiento);
					}
					
					
					if(bDebug){
						System.out.println("Antes de ejecutar MODIFICA MOVTO VENTA EN DAO ");
					}
					    
					rs = pstmt.executeQuery();

					if(bDebug){
						System.out.println("Despues de MODIFICA MOVTO VENTA EN DAO");
					}
					
					rs.close();
				    pstmt.close();
				
				    Servicio servicio = new Servicio();
				    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
				
			}
		}
		
		
		CallableStatement callst = conn.prepareCall("{call PCD_ACTUALIZA_IMPORTE_NOTA(?,?)}");
		callst.setInt(1,iIdEmpresa);
		callst.setInt(2,iIdNota);
		callst.execute(); 
  
		if(bDebug){
			System.out.println("Despues de PCD_ACTUALIZA_IMPORTE_NOTA EN DAO");
	  	}
	      
	      return sResultado;

	}
	
  public String sAltaModAjusteResta(int iIdEmpresa, int iIdMovimiento, String sFMovimiento, int iIdAlmacen, 
		  	int iIdProductoBase, String sTxReferencia, String sUsuario, String sSituacion, String sTipoOperacion, boolean bDebug) throws SQLException,  IOException{


		String sResultado = "";
		
		if(sTipoOperacion.equals("A")){

			sSentenciaSql = 
					" INSERT INTO MOVIMIENTO( \n " +
							"  ID_EMPRESA, ID_MOVIMIENTO, F_MOVIMIENTO, ID_ALMACEN, ID_PRODUCTO_BASE, \n " +
							"  TX_REFERENCIA, TIPO_MOVIMIENTO, FH_CREACION, FH_ULTIMA_MODIF, USUARIO_CREACION, USUARIO_ULTIMA_MODIF, \n " +
							"  SIT_MOVIMIENTO) \n " +
				    " VALUES(?,?,TO_DATE('" + sFMovimiento + "','DD/MM/YYYY'),?,?,?,'AJUSTE_RESTA', SYSDATE,SYSDATE,?,?,'AC') \n " ;


			if(bDebug){
				System.out.println(sSentenciaSql);
			}
			
	        pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1,iIdEmpresa);
			pstmt.setInt(2,iIdMovimiento);
			pstmt.setInt(3,iIdAlmacen);
			pstmt.setInt(4,iIdProductoBase);
			pstmt.setString(5,sTxReferencia);
			pstmt.setString(6,sUsuario);
			pstmt.setString(7,sUsuario);
	
			
			if(bDebug){
				System.out.println("Antes de ejecutar sGetSeries EN DAO ");
			}
			    
			rs = pstmt.executeQuery();

			if(bDebug){
				System.out.println("Despues de sGetSeries Archivo EN DAO");
			}
			
			rs.close();
		    pstmt.close();
		
		    Servicio servicio = new Servicio();
		    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
		    
		}else{
			
			if(sTipoOperacion.equals("M")){
				
				sSentenciaSql = 
						" UPDATE MOVIMIENTO SET \n " +
								"  F_MOVIMIENTO = TO_DATE('" + sFMovimiento + "','DD/MM/YYYY'), \n " +
								"  ID_ALMACEN	= ?,  \n " +
								"  TX_REFERENCIA= ?,	  \n " +
								"  FH_ULTIMA_MODIF= SYSDATE, \n " +
								(sSituacion==null?"":"  SIT_MOVIMIENTO= ?, \n ") +
								"  USUARIO_ULTIMA_MODIF = ? \n " +
					    " WHERE ID_EMPRESA = ? \n " +
						"   AND ID_MOVIMIENTO = ? \n "; 
				
					if(bDebug){
						System.out.println(sSentenciaSql);
					}
					
			        pstmt = conn.prepareStatement(sSentenciaSql);
					pstmt.setInt(1,iIdAlmacen);
					pstmt.setString(2,sTxReferencia);
					if(sSituacion!=null){
						pstmt.setString(3,sSituacion);
						pstmt.setString(4,sUsuario);
						pstmt.setInt(5,iIdEmpresa);
						pstmt.setInt(6,iIdMovimiento);
					}else{
						pstmt.setString(3,sUsuario);
						pstmt.setInt(4,iIdEmpresa);
						pstmt.setInt(5,iIdMovimiento);
					}
					
					
					if(bDebug){
						System.out.println("Antes de ejecutar MODIFICA MOVTO VENTA EN DAO ");
					}
					    
					rs = pstmt.executeQuery();

					if(bDebug){
						System.out.println("Despues de MODIFICA MOVTO VENTA EN DAO");
					}
					
					rs.close();
				    pstmt.close();
				
				    Servicio servicio = new Servicio();
				    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);
				
			}
		}
		
	      
	      return sResultado;

	}


  public String sAltaModDevolucionVenta(int iIdEmpresa, int iIdMovtoVenta, int iIdDevolucion, String sTxReferencia, 
		  String sUsuario, String sSituacion, boolean bDebug) throws SQLException,  IOException{


	  String sResultado = "";
		
		sSentenciaSql = 
				" MERGE INTO MOVIMIENTO A  		\n " +
				" USING (  						\n " +
				" 		SELECT	ID_EMPRESA, ? AS ID_MOVIMIENTO, TRUNC(SYSDATE) AS F_MOVIMIENTO, \n " +
				"				ID_CLIENTE, ID_ALMACEN, ? AS ID_REFERENCIA, ID_PRODUCTO_BASE, 	\n " +
				"				SYSDATE AS FH_CREACION, SYSDATE AS FH_ULTIMA_MODIF, ID_NOTA, 	\n " +
				"				? AS USUARIO_CREACION, ? AS USUARIO_ULTIMA_MODIF, ID_PRODUCTO, 	\n " +
				"               ? AS TX_REFERENCIA, 'SALIDA_DEVOLUCION' AS TIPO_MOVIMIENTO,		\n " +
				"               ? AS SIT_MOVIMIENTO 											\n " +
				"       FROM 	MOVIMIENTO 			\n " +
				"       WHERE 	ID_EMPRESA = ?		\n " +
				"       	AND ID_MOVIMIENTO = ?	\n " +
				"     ) B 										\n " +
				"     ON (A.ID_EMPRESA = B.ID_EMPRESA 			\n " +
				"     AND A.ID_MOVIMIENTO = B.ID_MOVIMIENTO) 	\n " +
				" WHEN MATCHED THEN  											\n " +
				"     UPDATE SET  												\n " +
				"         A.FH_ULTIMA_MODIF 		= SYSDATE, 					\n " +
				"         A.USUARIO_ULTIMA_MODIF 	= B.USUARIO_ULTIMA_MODIF, 	\n " +
				"         A.SIT_MOVIMIENTO 			= B.SIT_MOVIMIENTO, 		\n " +
				"         A.TX_REFERENCIA 			= B.TX_REFERENCIA 			\n " +
				" WHEN NOT MATCHED THEN  																\n " +
				"     INSERT(A.ID_EMPRESA, A.ID_MOVIMIENTO, A.F_MOVIMIENTO, A.ID_CLIENTE, A.ID_ALMACEN, \n " +
				"			A.ID_REFERENCIA, A.ID_PRODUCTO_BASE, A.FH_CREACION, A.FH_ULTIMA_MODIF, 		\n " +
				"			A.ID_NOTA, A.USUARIO_CREACION, A.USUARIO_ULTIMA_MODIF, A.ID_PRODUCTO, 		\n " +
				"           A.TX_REFERENCIA, A.TIPO_MOVIMIENTO, A.SIT_MOVIMIENTO)						\n " +
				"     VALUES(B.ID_EMPRESA, B.ID_MOVIMIENTO, B.F_MOVIMIENTO, B.ID_CLIENTE, B.ID_ALMACEN, \n " +
				"			B.ID_REFERENCIA, B.ID_PRODUCTO_BASE, B.FH_CREACION, B.FH_ULTIMA_MODIF, 		\n " +
				"			B.ID_NOTA, B.USUARIO_CREACION, B.USUARIO_ULTIMA_MODIF, B.ID_PRODUCTO, 		\n " +
				"           B.TX_REFERENCIA, B.TIPO_MOVIMIENTO, B.SIT_MOVIMIENTO) 						\n ";
		 
		if(bDebug){
			System.out.println(sSentenciaSql);
		}
			
        pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1,iIdDevolucion);
		pstmt.setInt(2,iIdMovtoVenta);
		pstmt.setString(3,sUsuario);
		pstmt.setString(4,sUsuario);
		pstmt.setString(5,sTxReferencia);
		pstmt.setString(6,sSituacion);	
		pstmt.setInt(7,iIdEmpresa);
		pstmt.setInt(8,iIdMovtoVenta);
					
		if(bDebug){
			System.out.println("Antes de ejecutar sAltaModDevolucionVenta EN DAO ");
		}
		    
		rs = pstmt.executeQuery();

		if(bDebug){
			System.out.println("Despues de sAltaModDevolucionVenta EN DAO");
		}
		
		rs.close();
	    pstmt.close();
	
	    Servicio servicio = new Servicio();
	    sResultado = servicio.sDameMensajeSistema(62, sUsuario, bDebug);

	    return sResultado;

	}
  

  public String[] sReporteVtaCteAlm(int iIdEmpresa, int iIdProductoBase, int iIdAlmacen, 
		  String sPreOperFMovto, String sFMovto, String sPostValFMovto, 
		  int iIdCliente, String sVendedor, String sCveUsuario, boolean bDebug) throws SQLException,  IOException{

		String sIdResultado[] = new String[2];


		CallableStatement callst = conn.prepareCall("{ ? = call FN_REPORTE_VTA_CLI_ALM(?,?,?,?,?,?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.CLOB);
	      callst.setInt(2,iIdEmpresa);
	      callst.setInt(3,iIdProductoBase);
	      callst.setInt(4,iIdAlmacen);
	      callst.setString(5,sPreOperFMovto);
	      callst.setString(6,sFMovto);
	      callst.setString(7,sPostValFMovto);
	      callst.setInt(8,iIdCliente);
	      callst.setString(9,sVendedor);
	      callst.setString(10,sCveUsuario);

	      if(bDebug){
		      System.out.println("Antes de ejecutar sReporteVtaCteAlm EN DAO ");
		      
		      System.out.println("sIdEmpresa: " + iIdEmpresa);
		      System.out.println("iIdProductoBase: " + iIdProductoBase);
		      System.out.println("iIdAlmacen: " + iIdAlmacen);
		      System.out.println("sPreOperFMovto: " + sPreOperFMovto);
		      System.out.println("sFMovto: " + sFMovto);
		      System.out.println("sPostValFMovto: " + sPostValFMovto);
		      System.out.println("iIdCliente: " + iIdCliente);
		      System.out.println("sVendedor: " + sVendedor);
		      System.out.println("sCveUsuario: " + sCveUsuario);
	      }
	      
	      callst.execute(); //  ------->>>>> ERROR Occur
	      
	      if(bDebug){
	    	  System.out.println("Despues de sReporteVtaCteAlm EN DAO");
	      }
	      
	      sIdResultado[0] = callst.getString(1);
	      sIdResultado[1] = null;
	      
	      return sIdResultado;

	}
  

  public String[] sReporteExistenciaAlm(int iIdEmpresa, int iIdProductoBase, int iIdAlmacen, 
		  String sCveUsuario, boolean bDebug) throws SQLException,  IOException{

		String sIdResultado[] = new String[2];


		CallableStatement callst = conn.prepareCall("{ ? = call FN_REPORTE_EXISTENCIA(?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.CLOB);
	      callst.setInt(2,iIdEmpresa);
	      callst.setInt(3,iIdProductoBase);
	      callst.setInt(4,iIdAlmacen);
	      callst.setString(5,sCveUsuario);

	      if(bDebug){
		      System.out.println("Antes de ejecutar sReporteVtaCteAlm EN DAO ");
		      
		      System.out.println("sIdEmpresa: " + iIdEmpresa);
		      System.out.println("iIdProductoBase: " + iIdProductoBase);
		      System.out.println("iIdAlmacen: " + iIdAlmacen);
		      System.out.println("sCveUsuario: " + sCveUsuario);
	      }
	      
	      callst.execute(); //  ------->>>>> ERROR Occur
	      
	      if(bDebug){
	    	  System.out.println("Despues de sReporteVtaCteAlm EN DAO");
	      }
	      
	      sIdResultado[0] = callst.getString(1);
	      sIdResultado[1] = null;
	      
	      return sIdResultado;

	}
  
	  public InvoiceVO getInvoiceHeader(String companyId, String idNota) throws SQLException {

	        InvoiceVO invoiceHeader = new InvoiceVO();
	        sSentenciaSql = "select ID_EMPRESA, ID_NOTA, NAME, STREET_ADDRESS, CITY_ADDRESS, INVOICE_DATE, TX_REFERENCIA,\n"
	                + "INVOICE_MUMBER, PO_NUMBER, DUE_DATE, SHIPPING_METHOD, FREIGHT, SALES_PERSON, PRINTED_TOTAL, CALLE \n"
	                + "from INVOICE_HEADER_V\n"
	                + "WHERE ID_EMPRESA = " + companyId + "\n"
	                + "AND ID_NOTA = " + idNota;

	        ejecutaSentencia();

	        if (rs.next()) {
	            invoiceHeader.setCityAddress(rs.getString("CITY_ADDRESS"));
	            invoiceHeader.setCompanyName(rs.getString("NAME"));
	            invoiceHeader.setDueDate(rs.getString("DUE_DATE"));
	            invoiceHeader.setInvoiceDate(rs.getString("INVOICE_DATE"));
	            invoiceHeader.setTxReferencia(rs.getString("TX_REFERENCIA"));
	            invoiceHeader.setInvoiceNumber(rs.getString("INVOICE_MUMBER"));
	            invoiceHeader.setPoNumber(rs.getString("PO_NUMBER"));
	            invoiceHeader.setShippingMethod(rs.getString("SHIPPING_METHOD"));
	            invoiceHeader.setStreetAddress(rs.getString("STREET_ADDRESS"));
	            invoiceHeader.setfReight(rs.getString("FREIGHT"));
	            invoiceHeader.setSalesPerson(rs.getString("SALES_PERSON"));
	            invoiceHeader.setPrintedTotal(rs.getString("PRINTED_TOTAL"));
	            invoiceHeader.setBillingAddress(invoiceHeader.getStreetAddress() + "\n" + invoiceHeader.getCityAddress());
	            invoiceHeader.setCalle(rs.getString("CALLE"));
	        }
	        return invoiceHeader;
	    }

	    public ResultSet getInvoiceData(String companyId, String idNota) throws SQLException {

	        sSentenciaSql = "SELECT ID_EMPRESA, ID_NOTA, ITEM, NVL(DESCRIPTION, ' ') AS DESCRIPTION, QTY_HOURS, PRICE_RATE, TOTAL \n"
	                + "FROM INVOICE_DETAIL_V \n"
	                + "WHERE ID_EMPRESA = " + companyId + "\n"
	                + "AND ID_NOTA = " + idNota;

	        ejecutaSentencia();

	        return rs;
	    }

	    public String getInvoiceTotal(String companyId, String idNota) throws SQLException {

	        sSentenciaSql = "SELECT SUM(TOTAL) AS TOTAL\n"
	                + "FROM INVOICE_DETAIL_V \n"
	                + "WHERE ID_EMPRESA = " + companyId + "\n"
	                + "AND ID_NOTA = " + idNota;

	        ejecutaSentencia();

	        if (rs.next()) {
	            return rs.getString("TOTAL");
	        }
	        return "";
	    }
	    
	    
	    public String getInvoiceTotalPiezas(String companyId, String idNota) throws SQLException {

	        sSentenciaSql = "SELECT SUM(QTY_HOURS) AS TOTAL\n"
	                + "FROM INVOICE_DETAIL_V \n"
	                + "WHERE ID_EMPRESA = " + companyId + "\n"
	                + "AND ID_NOTA = " + idNota;

	        ejecutaSentencia();

	        if (rs.next()) {
	            return rs.getString("TOTAL");
	        }
	        return "";
	    }
	  
	public boolean esRolAbreMovtoCerrado(int iIdEmpresa, String sIdRol, boolean bDebug) throws SQLException {

	  boolean bEsRolAbreMovtoCerrado = false;

		sSentenciaSql =	"	SELECT  'V' AS B_ES_ROL 				\n" +
						" 	FROM    AOS_ARQ_CAT_LISTADO_VALOR_DET 	\n" +
						" 	WHERE   ID_EMPRESA 		= ?  			\n" +
						"     	AND ID_LISTADO		= 105 			\n" +
						"     	AND VALOR 			= ?  			\n" +
						" 		AND SIT_VALOR		= 'AC' 			\n" ;
		
		if(bDebug){
			System.out.println("  QUERY VerificaRolAbreMovtoCerrado EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" ID_EMPRESA:	" + iIdEmpresa + "\n" +
					" VALOR: 		" + sIdRol + "\n"); 
		}

		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setString(2, sIdRol);

		rs = pstmt.executeQuery();

		if( rs.next() ){
			bEsRolAbreMovtoCerrado	= true;
		}

		rs.close();
		pstmt.close();
		return bEsRolAbreMovtoCerrado;
  }
  
  public String getExistenciaOtrosProductos(int iIdEmpresa, int iIdProducto, int iIdAlmacen, String sCveColor, boolean bDebug) throws SQLException {

	  String sExistencia = "0";

		sSentenciaSql =	"	SELECT  COUNT(1) AS EXISTENCIA			\n" +
						" 	FROM    PRODUCTO_INVENTARIO 			\n" +
						" 	WHERE   ID_EMPRESA 				= ?  	\n" +
						"     	AND ID_PRODUCTO 			= ?  	\n" +
						"     	AND ID_ALMACEN 				= ?  	\n" +(sCveColor!=null&&!sCveColor.equals("")?
						"     	AND CVE_COLOR  				= ?  	\n" :"") +
						" 		AND SIT_PRODUCTO_INVENTARIO	= 'IN' 	";
		
		if(bDebug){
			System.out.println("  QUERY getExistenciaOtrosProductos EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" ID_EMPRESA: 		" + iIdEmpresa + "\n" +
					" ID_PRODUCTO: 		" + iIdProducto + "\n" + 
					" ID_ALMACEN: 		" + iIdAlmacen + "\n" +
					" CVE_COLOR: 		" + sCveColor + "\n"); 
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setInt(2, iIdProducto);
		pstmt.setInt(3, iIdAlmacen);
		if(sCveColor!=null&&!sCveColor.equals("")){
			pstmt.setString(4, sCveColor);
		}
		
		rs = pstmt.executeQuery();

		if( rs.next() ){
			sExistencia  				= rs.getString("EXISTENCIA");
		}
		
		rs.close();
		pstmt.close();
		return sExistencia;
  }
  

  public String sAltaModOtrosProds(int iIdEmpresa, int iIdNota, int iIdMovimiento, 
		  	int iIdProducto, int iCantidad, String sUsuario, String sTipoOperacion, String sCveColor, float fPrecio, 
			String sSitMovto, int iIdRol, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "";
		
		if(sTipoOperacion.equals("A")){
			
			int vlIdMovimiento = 0;
			
			sSentenciaSql = "SELECT SEQ_MOVIMIENTO.NEXTVAL AS ID_MOVTO FROM DUAL";

			ejecutaSentencia();

			if(rs.next()){
				vlIdMovimiento = rs.getInt("ID_MOVTO");			
			}
				
			
			sSentenciaSql = 
				"INSERT INTO MOVIMIENTO( \n " +
				"      	ID_EMPRESA, ID_NOTA, F_MOVIMIENTO, ID_CLIENTE, ID_ALMACEN, TIPO_MOVIMIENTO, SIT_MOVIMIENTO,  \n " +
				"  		ID_PRODUCTO, FH_CREACION, FH_ULTIMA_MODIF, USUARIO_CREACION, \n " +
				"  		USUARIO_ULTIMA_MODIF, ID_MOVIMIENTO, CANTIDAD, CVE_COLOR, PRECIO) \n " +
			    "SELECT ID_EMPRESA, ID_NOTA, F_NOTA, ID_CLIENTE, ID_ALMACEN, TIPO_NOTA, 'AC', \n " +
				"        ?,SYSDATE,SYSDATE,?,?,?,?,?,? \n " +
			    "FROM 	NOTA 			\n " +
			    "WHERE	ID_EMPRESA = ?  \n " +
			    "	AND ID_NOTA    = ?  \n " ;

			if(bDebug){
				System.out.println(sSentenciaSql);
			}

	        pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1,iIdProducto);
			pstmt.setString(2,sUsuario);
			pstmt.setString(3,sUsuario);
			pstmt.setInt(4,vlIdMovimiento);
			pstmt.setInt(5,iCantidad);
			pstmt.setString(6,sCveColor);
			pstmt.setFloat(7,fPrecio);
			pstmt.setInt(8,iIdEmpresa);
			pstmt.setInt(9,iIdNota);

			if(bDebug){
				System.out.println("Antes de ejecutar sAltaModOtrosProds A EN DAO \n" +
						"vlIdMovimiento:		" + vlIdMovimiento 	+ "\n" + 
						"iCantidad:				" + iCantidad 		+ "\n" + 
						"iIdNota:				" + iIdNota 		+ "\n" + 
						"sCveColor:				" + sCveColor 		+ "\n" + 
						"fPrecio:				" + fPrecio 		+ "\n" +
						"iIdProducto:			" + iIdProducto 	+ "\n" );
			}
			    
			rs = pstmt.executeQuery();

			if(bDebug){
				System.out.println("Despues de sGetSeries sAltaModOtrosProds A EN DAO");
			}
			
			rs.close();
		    pstmt.close();
		
		    sResultado = String.valueOf(vlIdMovimiento);
		    
		}else{
			
			if(sTipoOperacion.equals("M")){
				
				sSentenciaSql = 
						"UPDATE MOVIMIENTO SET 						\n " +
						"  		FH_ULTIMA_MODIF			= SYSDATE, 	\n " +
						"  		SIT_MOVIMIENTO			= ?,		\n " +
						"  		USUARIO_ULTIMA_MODIF 	= ? 		\n " +
					    " WHERE ID_EMPRESA 				= ? 		\n " +
						"   AND ID_MOVIMIENTO 			= ? 		\n "; 
				
				if(bDebug){
					System.out.println(sSentenciaSql);
				}
				
		        pstmt = conn.prepareStatement(sSentenciaSql);
				pstmt.setString(1,sSitMovto);
				pstmt.setString(2,sUsuario);
				pstmt.setInt(3,iIdEmpresa);
				pstmt.setInt(4,iIdMovimiento);
				
				if(bDebug){
					System.out.println("Antes de ejecutar sAltaModOtrosProds M EN DAO ");
				}
				    
				rs = pstmt.executeQuery();

				if(bDebug){
					System.out.println("Despues de sAltaModOtrosProds M EN DAO");
				}
				
				rs.close();
			    pstmt.close();
			
			    // Verifica si el rol tiene privilegios de cancelar un Movimiento aún cuando ya esté cerrado
			    if(sSitMovto.equals("CL")){
			    	if(esRolAbreMovtoCerrado(iIdEmpresa, String.valueOf(iIdRol), bDebug)){
			    		sResultado = "1";
			    	}else{
			    		sResultado = "0";
			    	}
			    }else{
			    	sResultado = "0";
			    }
			}
		}
		
		
		CallableStatement callst = conn.prepareCall("{call PCD_ACTUALIZA_IMPORTE_NOTA(?,?)}");
		callst.setInt(1,iIdEmpresa);
		callst.setInt(2,iIdNota);
		callst.execute(); 
  
		if(bDebug){
			System.out.println("Despues de PCD_ACTUALIZA_IMPORTE_NOTA EN DAO");
	  	}

	      return sResultado;

	}
  
  
  public String sGetSeriesMinInv(int sIdEmpresa, int iIdAlmacen, int iIdProductoBase, String sUsuario, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "";

		CallableStatement callst = conn.prepareCall("{ ? = call FN_GENERA_MATRIZ_MIN_INV(?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.CLOB);
	      callst.setInt(2,sIdEmpresa);
	      callst.setInt(3,iIdAlmacen);
	      callst.setInt(4,iIdProductoBase);
	      callst.setString(5,sUsuario);

	      if(bDebug){
	    	  System.out.println("Antes de ejecutar sGetSeriesMinInv EN DAO ");
	      }

	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de sGetSeriesMinInv Archivo EN DAO");
	      }

	      sResultado = callst.getString(1);

	      return sResultado;

	}



  public String sMergeMinInvProdBase(int iIdEmpresa, int iIdProductoBase, int iIdSerie, 
		  float fValHorizontal, float fValVertical, int iCantidad, String sUsuario, int iIdRol, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "0";
		
		sSentenciaSql = 
			"		MERGE INTO PROD_BASE_EXIST_CONF_MIN  A 								\n " + 
          "            USING (  														\n " + 
          "                SELECT  ? AS ID_EMPRESA, 									\n " +  
          "                        ? AS ID_PRODUCTO_BASE,  							\n " + 
          "                        ? AS ID_SERIE,  									\n " + 
          "                        ? AS VALOR_HORIZONTAL, 							\n " +  
          "                        ? AS VALOR_VERTICAL,  								\n " + 
          "                        ? AS MINIMO_EXISTENCIA 							\n " + 
          "                FROM DUAL ) B 												\n " + 
          "           ON (     A.ID_EMPRESA       = B.ID_EMPRESA 						\n " + 
          "                AND A.ID_PRODUCTO_BASE = B.ID_PRODUCTO_BASE 				\n " + 
          "                AND A.ID_SERIE         = B.ID_SERIE 						\n " + 
          "                AND A.VALOR_HORIZONTAL = B.VALOR_HORIZONTAL 				\n " + 
          "                AND A.VALOR_VERTICAL   = B.VALOR_VERTICAL)  				\n " + 
          "        WHEN MATCHED THEN  												\n " + 
          "            UPDATE SET A.MINIMO_EXISTENCIA = B.MINIMO_EXISTENCIA 			\n " +  
          "        WHEN NOT MATCHED THEN  											\n " + 
          "            INSERT (A.ID_EMPRESA, A.ID_PRODUCTO_BASE, A.ID_SERIE, A.VALOR_HORIZONTAL, A.VALOR_VERTICAL, A.MINIMO_EXISTENCIA) 	\n " + 
          "            VALUES (B.ID_EMPRESA, B.ID_PRODUCTO_BASE, B.ID_SERIE, B.VALOR_HORIZONTAL, B.VALOR_VERTICAL, B.MINIMO_EXISTENCIA) 	\n " ;

			if(bDebug){
				System.out.println(sSentenciaSql);
			}

	        pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1,iIdEmpresa);
			pstmt.setInt(2,iIdProductoBase);
			pstmt.setInt(3,iIdSerie);
			pstmt.setFloat(4,fValHorizontal);
			pstmt.setFloat(5,fValVertical);
			pstmt.setInt(6,iCantidad);

			if(bDebug){
				System.out.println("Antes de ejecutar sMergeMinInvProdBase A EN DAO \n" +
						"iIdEmpresa:		" + iIdEmpresa 		+ "\n" + 
						"iIdProductoBase:	" + iIdProductoBase + "\n" + 
						"iIdSerie:			" + iIdSerie 		+ "\n" +
						"iValHorizontal:	" + fValHorizontal 	+ "\n" + 
						"iValVertical:		" + fValVertical 	+ "\n" +
						"iCantidad:			" + iCantidad 		+ "\n" );
			}
			    
			rs = pstmt.executeQuery();

			if(bDebug){
				System.out.println("Despues de sMergeMinInvProdBase A EN DAO");
			}
			
			rs.close();
		    pstmt.close();
		
		    sResultado = "1";
		    
		    return sResultado;

	}
    
  public String sGetSeriesInvMenorMin(int sIdEmpresa, int iIdAlmacen, int iIdProductoBase, String sUsuario, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "";

		CallableStatement callst = conn.prepareCall("{ ? = call FN_GENERA_MAT_INV_MENOR_MIN(?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.CLOB);
	      callst.setInt(2,sIdEmpresa);
	      callst.setInt(3,iIdAlmacen);
	      callst.setInt(4,iIdProductoBase);
	      callst.setString(5,sUsuario);

	      if(bDebug){
	    	  System.out.println("Antes de ejecutar sGetSeriesInvMenorMin EN DAO ");
	      }

	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de sGetSeriesInvMenorMin EN DAO");
	      }

	      sResultado = callst.getString(1);

	      return sResultado;

	}
  

  public String sMergeMinInvProdBase(int iIdEmpresa, int iIdAlmacen, int iIdProductoBase, int iIdSerie, 
		  float fValHorizontal, float fValVertical, int iCantidad, String sUsuario, int iIdRol, boolean bDebug) throws SQLException,  IOException{

		String sResultado = "0";
		
		sSentenciaSql = 
			"		MERGE INTO PROD_BASE_EXIST_CONF_MIN  A 								\n " + 
            "            USING (  														\n " + 
            "                SELECT  ? AS ID_EMPRESA, 									\n " +  
            "                        ? AS ID_ALMACEN,  									\n " + 
            "                        ? AS ID_PRODUCTO_BASE,  							\n " + 
            "                        ? AS ID_SERIE,  									\n " + 
            "                        ? AS VALOR_HORIZONTAL, 							\n " +  
            "                        ? AS VALOR_VERTICAL,  								\n " + 
            "                        ? AS MINIMO_EXISTENCIA 							\n " + 
            "                FROM DUAL ) B 												\n " + 
            "           ON (     A.ID_EMPRESA       = B.ID_EMPRESA 						\n " + 
            "                AND A.ID_ALMACEN 		= B.ID_ALMACEN 						\n " + 
            "                AND A.ID_PRODUCTO_BASE = B.ID_PRODUCTO_BASE 				\n " + 
            "                AND A.ID_SERIE         = B.ID_SERIE 						\n " + 
            "                AND A.VALOR_HORIZONTAL = B.VALOR_HORIZONTAL 				\n " + 
            "                AND A.VALOR_VERTICAL   = B.VALOR_VERTICAL)  				\n " + 
            "        WHEN MATCHED THEN  												\n " + 
            "            UPDATE SET A.MINIMO_EXISTENCIA = B.MINIMO_EXISTENCIA 			\n " +  
            "			 DELETE WHERE B.MINIMO_EXISTENCIA = 0  							\n " +
            "        WHEN NOT MATCHED THEN 												\n " + 
            "            INSERT (A.ID_EMPRESA, A.ID_ALMACEN, A.ID_PRODUCTO_BASE, A.ID_SERIE, A.VALOR_HORIZONTAL, A.VALOR_VERTICAL, A.MINIMO_EXISTENCIA) 	\n " + 
            "            VALUES (B.ID_EMPRESA, B.ID_ALMACEN, B.ID_PRODUCTO_BASE, B.ID_SERIE, B.VALOR_HORIZONTAL, B.VALOR_VERTICAL, B.MINIMO_EXISTENCIA) 	\n " +
            "			 WHERE  B.MINIMO_EXISTENCIA > 0 ";

			if(bDebug){
				System.out.println(sSentenciaSql);
			}

	        pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1,iIdEmpresa);
			pstmt.setInt(2,iIdAlmacen);
			pstmt.setInt(3,iIdProductoBase);
			pstmt.setInt(4,iIdSerie);
			pstmt.setFloat(5,fValHorizontal);
			pstmt.setFloat(6,fValVertical);
			pstmt.setInt(7,iCantidad);

			if(bDebug){
				System.out.println("Antes de ejecutar sMergeMinInvProdBase A EN DAO \n" +
						"iIdEmpresa:		" + iIdEmpresa 		+ "\n" + 
						"iIdAlmacen:		" + iIdAlmacen 		+ "\n" + 
						"iIdProductoBase:	" + iIdProductoBase + "\n" + 
						"iIdSerie:			" + iIdSerie 		+ "\n" +
						"iValHorizontal:	" + fValHorizontal 	+ "\n" + 
						"iValVertical:		" + fValVertical 	+ "\n" +
						"iCantidad:			" + iCantidad 		+ "\n" );
			}
			    
			rs = pstmt.executeQuery();

			if(bDebug){
				System.out.println("Despues de sMergeMinInvProdBase A EN DAO");
			}
			
			rs.close();
		    pstmt.close();
		
		    sResultado = "1";
		    
		    return sResultado;

	}
  
	  public ResultSet getRegistrosEdoCta(String companyId, String idCliente) throws SQLException {
		  
		  sSentenciaSql = "SELECT ID_CLIENTE,FECHA,TIPO_MOVIMIENTO,ID_NOTA,IMP_VENTA,IMP_DEPOSITO,  \n"
				  + "IMP_DEVOLUCION, (IMP_VENTA + IMP_DEPOSITO + IMP_DEVOLUCION) IMPORTE, \n"
				  + "TX_REFERENCIA, NOM_CLIENTE  \n"
				  + "FROM OPTICMAY.EDOCTA_CLIENTE_DET_V   \n"
				  + "WHERE ID_EMPRESA = " + companyId + "\n"
	              + "AND ID_CLIENTE = " + idCliente + "\n"
				  + "AND (TX_REFERENCIA NOT LIKE '%INICIAL%' \n"
				  + "OR TX_REFERENCIA IS NULL) \n"
				  + "ORDER BY TIPO_MOVIMIENTO,FECHA \n";
	
	      ejecutaSentencia();
	
	      return rs;
	  }
	  
	  public ResultSet getRegistrosEdoCtaPorFecha(String companyId, String idCliente) throws SQLException {
		  
		  sSentenciaSql = "SELECT ID_CLIENTE,FECHA,TIPO_MOVIMIENTO,ID_NOTA,IMP_VENTA,IMP_DEPOSITO,  \n"
				  + "IMP_DEVOLUCION, (IMP_VENTA + IMP_DEPOSITO + IMP_DEVOLUCION) IMPORTE, \n"
				  + "TX_REFERENCIA, NOM_CLIENTE  \n"
				  + "FROM OPTICMAY.EDOCTA_CLIENTE_DET_V   \n"
				  + "WHERE ID_EMPRESA = " + companyId + "\n"
	              + "AND ID_CLIENTE = " + idCliente + "\n"
				  + "AND (TX_REFERENCIA NOT LIKE '%INICIAL%' \n"
				  + "OR TX_REFERENCIA IS NULL) \n"
				  + "ORDER BY FECHA \n";
	
	      ejecutaSentencia();
	
	      return rs;
	  }
	  
	  public int getSaldoInicial(String companyId, String idCliente) throws SQLException {
	      
	      sSentenciaSql = "SELECT IMP_DEPOSITO * -1 AJUSTE_INICIAL  \n"
	    		  + "FROM OPTICMAY.EDOCTA_CLIENTE_DET_V  \n"
	    		  + "WHERE ID_EMPRESA = " + companyId + "\n"
	              + "AND ID_CLIENTE = " + idCliente + "\n"
	              + "AND TX_REFERENCIA LIKE '%INICIAL%' \n";
	
	      ejecutaSentencia();
	      
	      int iSaldoInicial = 0;
	      if(rs.next()){
	    	  iSaldoInicial = rs.getInt("AJUSTE_INICIAL");			
	      }
	      
	      return iSaldoInicial;
	  }
	  
	  public Double getSaldoFinal(int idEmpresa, int idCliente) throws SQLException,  IOException{

		  	double iResultado = 0.0;

			CallableStatement callst = conn.prepareCall("{ ? = call FN_IMP_SALDO_CLIENTE(?,?)}");
		      callst.registerOutParameter(1, java.sql.Types.DECIMAL);
		      callst.setInt(2,idEmpresa);
		      callst.setInt(3,idCliente);

		      callst.execute(); 

		      iResultado = callst.getDouble(1);
		      return iResultado;

		}
	  
	  //ESTE METODO AUN NO ESTA PROBADO
	  public Double getSaldoFinalFecha(int idEmpresa, int idCliente, Date fecha) throws SQLException,  IOException{

		  	double iResultado = 0.0;

			CallableStatement callst = conn.prepareCall("{ ? = call FN_IMP_SALDO_CTE_EDOCTA(?,?,?)}");
			                                                        
		      callst.registerOutParameter(1, java.sql.Types.DECIMAL);
		      callst.setInt(2,idEmpresa);
		      callst.setInt(3,idCliente);
		      callst.setDate(4,(java.sql.Date) fecha);

		      callst.execute(); 

		      iResultado = callst.getDouble(1);
		      return iResultado;

	}
	  
	  public String sGetMensaje(int iIdError, boolean bDebug) throws SQLException{
			
			String sMensaje = ""; 
			
			sSentenciaSql = " 	SELECT  MENSAJE  					\n "  +
						    "   FROM    AOS_ARQ_MENSAJE_SISTEMA  	\n " +
						    " 	WHERE 	ID_MENSAJE	= ? ";

			if(bDebug){
				System.out.println("  QUERY sGetMensaje EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdLogerror: 	" + iIdError +  " \n");
			}

			pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1, iIdError);
			rs = pstmt.executeQuery();

			if(rs.next()){
				sMensaje	= rs.getString("MENSAJE");
			}else{
				
				sSentenciaSql = " 	SELECT  MENSAJE  					\n "  +
							    "   FROM    AOS_ARQ_MENSAJE_SISTEMA  	\n " +
							    " 	WHERE 	ID_MENSAJE	= ? ";

				if(bDebug){
					System.out.println("  QUERY sGetMensaje no encontrado EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n");
				}
			
				pstmt = conn.prepareStatement(sSentenciaSql);
				pstmt.setInt(1, 0);
				rs = pstmt.executeQuery();
				
				if(rs.next()){
					sMensaje	= rs.getString("MENSAJE") + iIdError;
				}
			}

			rs.close();
		    pstmt.close();
			return sMensaje;
		}
	  
	  
	  
	  
	  public String sDynOperation(HashMap<String, Object> map) throws SQLException{

			int iIdEmpresa 			= Integer.parseInt((String)map.get("DYN_CTL_ID_EMPRESA"));
			String sDynOperation	= (String)map.get("DYN_CTL_OPERATION");
			boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
			Usuario usuario 		= (Usuario)map.get("DYN_CTL_USUARIO");
			String sQuery 			= ""; 
			String sParameters 		= "";
			String sParameter		= "";
			String sResultado		= "";
			StringTokenizer tokens  = null; 
			JsonArray jaJasonArray	= null;
			Gson gson 				= new Gson();
			int i 					= 1;
			
			if(bDebug){
				System.out.println(" sDynOperation ----->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ");
			}
			
			sSentenciaSql = 
					" 	SELECT  QUERY_OPERATION, QUERY_PARAMETERS  	\n " +
	                "	FROM    AOS_ARQ_DYN_OPERATION 				\n " +
	                "	WHERE   ID_EMPRESA  	= ?  				\n " +
	                "    	AND CVE_OPERATION  	= ? 				\n " ;

			if(bDebug){
				System.out.println("  QUERY sDynOperation Obtiene query EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdEmpresa: 		" + iIdEmpresa 		+ "\n" +
					" sDynOperation: 	" + sDynOperation 	+ "\n");
			}
			
			pstmt = conn.prepareStatement(sSentenciaSql);
			
			pstmt.setInt(1, iIdEmpresa);
			pstmt.setString(2, sDynOperation);
			rs = pstmt.executeQuery();

			if(rs.next()){
				if(bDebug){
					System.out.println("  TIENE RS sDynOperation ----->>>>>>>>>>>>>>>>>>>  ");
				}
				sQuery 		= rs.getString("QUERY_OPERATION");
				sParameters = rs.getString("QUERY_PARAMETERS");
				tokens 		= new StringTokenizer(sParameters, ",");
				
				pstmt 		= conn.prepareStatement(sQuery);
								
				while(tokens.hasMoreTokens()){
					sParameter = tokens.nextToken();
					String[] sSplit = sParameter.split(Pattern.quote("|"));
					
					if(sSplit[0].equals("VARCHAR")){
						if(sSplit[1].equals("DYN_CTL_USER")){
							pstmt.setString(i, usuario.sCveUsuario);
						}else{
							pstmt.setString(i, (String)map.get(sSplit[1]));
						}
					}else{
						if(sSplit[0].equals("FLOAT")){
							pstmt.setFloat(i, Float.valueOf((String)map.get(sSplit[1])));
						}else{
							if(sSplit[0].equals("INT")){
								pstmt.setInt(i, Integer.valueOf((String)map.get(sSplit[1])));
							}else{
								if(sSplit[0].equals("DATE")){
							
									SimpleDateFormat formatter = new SimpleDateFormat(sSplit[2]);
							        String dateInString = sSplit[1];

							        try {
							            Date date = formatter.parse(dateInString);
							            pstmt.setDate(i, java.sql.Date.valueOf((String)map.get(sSplit[1])));
							        } catch (ParseException e) {
							            e.printStackTrace();
							        }
								}
							}
						}
					}
					if(bDebug){
						System.out.println("  Agrega el parametro en el DYN stmt: " + sParameter);
					}
					i = i + 1;
				}
				
				if(bDebug){
					System.out.println("  Antes de ejecutar DYN stmt -->>>>>>>>>>>>>>>> ");
				}
				rs = pstmt.executeQuery();
				rs.close();
			    pstmt.close();
			}
			
			sResultado 		= "[{\"A\":\"" + sGetMensaje(-4, bDebug) + "\"}]";
			jaJasonArray	= gson.fromJson(sResultado, JsonArray.class); 
		
			if(bDebug){
				System.out.println("  sResultado en DYN_OPERATION ----->>>>>>>>>>>>>>>>>>>  " + sResultado);
			}
			JsonObject joObj = new JsonObject();
			joObj.add("result", jaJasonArray);
		
			sResultado = joObj.toString();

			if(bDebug){
				System.out.println("sResultado final: " + sResultado);
			}
			return sResultado;
			
		} // sDynOperation
	  
  
}