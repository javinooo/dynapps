package com.dinamicaps.web.impl.domain.optics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class OpticsServiceAjaxBorrar {
	
	public void mandaJsonRespuesta(HttpServletResponse response, Map<String, Object> map) throws IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	}

	public void mandaJsonRespuesta(HttpServletResponse response, ArrayList<?> lista) throws IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(lista));
	}
}