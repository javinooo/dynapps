package com.dinamicaps.web.impl.domain;

public class Ejecutable {

	public int iIdEmpresa;
	public int iIdEjecutable;
	public int iListadoRmpzaDetalle;
	public int iListadoRmpzaEntidad;
	public int iIdLogError;
	public int iIdLogErrorHtml = 0;
	public String sTituloExecutable = "";
	public String sTemplateGuardaDatos ="";
	public String sBRemplazaComboEnt = "";
	public String sBRemplazaComboAts = "";
	public String sFuncJsAgregaAtributo = "";
	public String sEtiquetaRmpzaDetalle = "";
	public String sBObtieneElementos = "";
	public String sBGuardaLogErrorHtml = "";
	public String sBBotonRegresar = "";
	public String sBAtribDependEntidad = "";
	public String sBObligatorioComboDet = "";
	
}
