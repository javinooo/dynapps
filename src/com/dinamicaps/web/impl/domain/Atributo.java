package com.dinamicaps.web.impl.domain;

public class Atributo {
	
	public String sNomAtributo = "";
	public String sTipoDato = "";
	public String sIdPk = "";
	public String sTipoTemplate = "";
	public int iIdDetalle;
	public int iSeqVal =0;
	public String sCveParameter = "";
	public String sAtributoHijo = "";
	public String sCveMultiInsert = "";
	public String sSequenceAutoInc = "";
	public boolean bIsMultiInsert;
}
