package com.dinamicaps.web.impl.domain;

public class Pantalla {

	public int 		iIdPantalla 		= 0;
	public int 		iIdTabla			= 0;
	public String 	sHtmlPantalla 		= "";	
	public String 	sSitPantalla		= "";
	public String 	sTipoPantalla 		= "";
	public String 	sTituloPantalla 	= "";
	public String 	sSubtituloPantalla 	= "";
	public String 	sCveMasterTemplate 	= "";
	public String 	sCveMasterTemplateVersion 	= "";
	public String 	sPageContentHeader 	= "";
	public String 	sNotifications 		= "";
	public String 	sJQTransformRSTable = "";
	public String   sPageSpecificPlugins= "";
	public String   sDefaultSkin		= "";
	public String   sQryIncludeDetail	= "";
	public boolean 	bBConsulta;
	public boolean 	bBAlta;
	public boolean 	bBModifica;
	public boolean 	bBBaja;
	public boolean 	bBButtonBackCreate;
	public boolean 	bBButtonClear;
	public boolean 	bBButtonBackUpdate;
	public boolean 	bBButtonCreateSearch;
	public boolean 	bBButtonSearch;
	public boolean 	bBButtonCreate;
	public boolean 	bBButtonUpdate;
	public boolean 	bBButtonCreateFromResult;
	public boolean 	bBRedirectAltToMod;
	
	
}

