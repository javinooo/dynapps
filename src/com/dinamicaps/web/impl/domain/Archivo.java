package com.dinamicaps.web.impl.domain;

import java.io.File;
import java.io.FileOutputStream;
import oracle.sql.BLOB;

public class Archivo {

	public String sNomArchivo = "";
	public String sTipoContenido = "";
	public File fArchivo = null;
	public FileOutputStream oStream = null;
	public byte[] bDatos = null;
	public BLOB blob = null;
}
