package com.dinamicaps.web.impl.domain;

public class Buttons {
	public String sButtonBack = "";
	public String sButtonSearch = "";
	public String sButtonClear = "";
	public String sButtonUpdateRecord = "";
	public String sButtonDeleteRecord = "";
	public String sButtonCreateRecord = "";
	public String sButtonGoToCreateRecord = "";
	public String sButtonGoToCreateRecordFromSearch = "";
	public String sButtonAddAttribute = "";
	
}
