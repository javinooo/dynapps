package com.dinamicaps.web.impl.domain.vstream;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.lang3.StringUtils;

import com.dinamicaps.web.impl.ServicioConexion;
import com.dinamicaps.web.impl.domain.Usuario;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.Gson;

public class VStreamDAO  extends ServicioConexion{
	
	
	public int iIdMensaje() throws SQLException{
		
		int iIdFile = 0;

		sSentenciaSql = "SELECT SEQ_EVENTO_MENSAJE.NEXTVAL AS ID_MENSAJE FROM DUAL";

		ejecutaSentencia();
		
		if(rs.next()){
			iIdFile = rs.getInt("ID_MENSAJE");			
		}
		
		return iIdFile;

	}
	
	public String sUpdateMessageStatus(int iIdEmpresa, int iIdMensaje, String sMsgStatus, String sCveusuario, boolean bDebug) throws SQLException{
		Gson gson 				= new Gson();
		JsonArray jaJasonArray 	= new JsonArray();
		String sResultado 		= ""; 
		
		sSentenciaSql = " 	UPDATE 	EVENTO_MENSAJE SET 	\n" +
						"			SIT_MENSAJE 		= ?, 	\n" +
						"			FH_REVISION 		= ?, 	\n" +
						"			USUARIO_REVISION 	= ? 	\n" +
						"	WHERE 	ID_EMPRESA 	= ? 	\n" +
						"		AND ID_MENSAJE 	= ? 	";

		if(bDebug){
			System.out.println("  QUERY sUpdateMessageStatus EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n");
		}
	
		pstmt = conn.prepareStatement(sSentenciaSql);
		
		Calendar cal 		= Calendar.getInstance();
		Timestamp timestamp = new java.sql.Timestamp(cal.getTimeInMillis());
		
		pstmt.setString(1, sMsgStatus);
        pstmt.setTimestamp(2, timestamp);
        pstmt.setString(3, sCveusuario);
		pstmt.setInt(4, iIdEmpresa);
		pstmt.setInt(5, iIdMensaje);
		rs = pstmt.executeQuery();
		
		sSentenciaSql = " 	SELECT  ETIQUETA_VALOR					\n" +
		                "	FROM    AOS_ARQ_CAT_LISTADO_VAL_DET_JQ	\n" +
		                "	WHERE   ID_EMPRESA  = ?					\n" +
		                "    	AND ID_LISTADO  = 130				\n" +
		                "    	AND VALOR       = ? 				";

		if(bDebug){
			System.out.println("  QUERY sUpdateMessageStatus get estatus EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n");
		}

		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setString(2, sMsgStatus);
		rs = pstmt.executeQuery();
		
		if(rs.next()){
			sResultado = rs.getString("ETIQUETA_VALOR");
		}
		
		sResultado 			= "[{\"A\":\"" + sResultado + "\"}]";
		jaJasonArray		= gson.fromJson(sResultado, JsonArray.class); 
		JsonObject joObj 	= new JsonObject();
		joObj.add("result", jaJasonArray);
		sResultado 			= joObj.toString();
		
		if(bDebug){
			System.out.println("  sResultado ----->>>>>>>>>>>>>>>>>>>  " + sResultado + "\n");
		}
		
		return sResultado;
		
	}
	

	public static String resultSetToJson(Connection connection, String query, Object[] params) {
	      List<Map<String, Object>> listOfMaps = null;
	      try {
	    	  QueryRunner queryRunner = new QueryRunner();
	          listOfMaps = queryRunner.query(connection, query, new MapListHandler(), params);
	      } catch (SQLException se) {
	          throw new RuntimeException("Couldn't query the database.", se);
	      } //finally {DbUtils.closeQuietly(connection);}
	      return new Gson().toJson(listOfMaps);
	  }

	
	
	public String sGetMessages(int iIdEmpresa, int IIdEvento, boolean bDebug) throws SQLException{

		String sResultado 		= "";
		Gson gson 				= new Gson();
		JsonArray jaJasonArray 	= new JsonArray();
		JsonObject joObj 		= new JsonObject();
		
		sSentenciaSql =	"SELECT  NOM_USUARIO AS N, TX_MENSAJE AS M, DES_FH_CREACION AS F, FOTO_RECUERDO AS R, FOTO_PROPIA AS P	\n" + 
					    "FROM    EVENTO_MENSAJE_V					\n" +
					    "WHERE   ID_EMPRESA    	= ? 				\n" +
					    "    AND ID_EVENTO     	= ?					\n" +
					    "    AND SIT_MENSAJE    = 'OK'				\n" +
					    "ORDER BY FH_CREACION						\n" ;

		if(bDebug){
			System.out.println("  QUERY sGetMessages GET TABLA EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" sCveOperacion: 	" + iIdEmpresa + "\n" +  
					" iIdTabla: 		" + IIdEvento + "\n"); 
		}
		
		Object[] params 	= new Object[2];
		params[0] 			= (Integer)iIdEmpresa;
		params[1] 			= (Integer)IIdEvento;
		
		sResultado 			= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Tabla: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		joObj.add("datosMsges", jaJasonArray);
		
		sSentenciaSql =	"SELECT  NOM_PERSONA AS P, FEC_DE_HASTA AS H, FOTO AS F, URL_CAMARA AS U \n" + 
				        "FROM    EVENTO_V			\n" + 
				        "WHERE   ID_EMPRESA  = ? 	\n" + 
				        "    AND ID_EVENTO   = ? 	\n" ; 
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Evento Tabla: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		joObj.add("datosEvento", jaJasonArray);
		
		sResultado = joObj.toString();

		if(bDebug){
			System.out.println("sResultado: " + sResultado);
		}
		return sResultado;
	}
	

	public UsuarioEvto getUsuarioEvto(String sCveUsuario, String sPassword, boolean bDebug) throws SQLException{

		UsuarioEvto usuarioEvto = null; 
		
		sSentenciaSql = "SELECT  ID_EMPRESA, ID_EVENTO, ID_SUCURSAL, ID_SALA, ID_USUARIO, NOM_USUARIO,  \n " + 
						"        SIT_USUARIO, SIT_USUARIO, 5 AS ID_ROL, CASE WHEN ? = PASSWORD THEN   	\n " + 
						"       'V' ELSE 'F' END AS PASSWORD_VALIDO, 'F' AS B_CAMBIO_PASSWORD,  		\n " + 
						"        NVL(GENERO,'H') AS GENERO, FOTO, URL_CAMARA							\n " + 
						"FROM    USUARIO_EVENTO_V 														\n " + 
						"WHERE 	 ID_EMPRESA	= 1 														\n " + 
						"    AND ID_USUARIO = ? 	 													\n " ;
		
		
		if(bDebug){
			System.out.println("  QUERY getUsuarioEvto EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
		}

		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setString(1, sPassword);
		pstmt.setInt(2, Integer.parseInt(sCveUsuario));
		rs = pstmt.executeQuery();

		if(rs.next()){
			usuarioEvto = new UsuarioEvto();
			usuarioEvto.iIdEmpresa          = rs.getInt("ID_EMPRESA");
			usuarioEvto.iIdEvento           = rs.getInt("ID_EVENTO");
			usuarioEvto.iIdSucursal         = rs.getInt("ID_SUCURSAL");
			usuarioEvto.iIdSala             = rs.getInt("ID_SALA");
			usuarioEvto.iIdRol              = rs.getInt("ID_ROL");
			usuarioEvto.iIdUsuario          = rs.getInt("ID_USUARIO");
			usuarioEvto.sNomUsuario         = rs.getString("NOM_USUARIO");
			usuarioEvto.bPasswordValido     = rs.getString("PASSWORD_VALIDO").equals("V");
			usuarioEvto.sGenero             = rs.getString("GENERO");
			usuarioEvto.sFoto               = rs.getString("FOTO");
			usuarioEvto.sSitUsuario			= rs.getString("SIT_USUARIO");
		}

		rs.close();
	    pstmt.close();
		return usuarioEvto;
	}
	
	

	public String sGetMessagesUser(UsuarioEvto usuarioEvto, boolean bDebug) throws SQLException{

		String sResultado 		= "";
		Gson gson 				= new Gson();
		JsonArray jaJasonArray 	= new JsonArray();
		JsonObject joObj 		= new JsonObject();
		
		sSentenciaSql =	"SELECT  M.NOM_USUARIO AS N, M.TX_MENSAJE AS M, M.DES_FH_CREACION AS F, M.FOTO_RECUERDO AS R, NVL(M.FOTO_PROPIA,U.FOTO) AS P	\n" + 
					    "FROM    EVENTO_MENSAJE_V M						\n" +
					    "     JOIN USUARIO_EVENTO_V U 					\n" +
					    " 		ON M.ID_EMPRESA = U.ID_EMPRESA 			\n" +
					    "		AND M.ID_USUARIO = U.ID_USUARIO			\n" +
					    "WHERE   M.ID_EMPRESA    	= ? 				\n" +
					    "    AND M.ID_EVENTO     	= ?					\n" +
					    "    AND M.SIT_MENSAJE    = 'OK'				\n" +
					    "ORDER BY M.FH_CREACION	DESC					\n" ;

		if(bDebug){
			System.out.println("  QUERY sGetMessages GET TABLA EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" sCveOperacion: 	" + usuarioEvto.iIdEmpresa + "\n" +  
					" iIdTabla: 		" + usuarioEvto.iIdEvento + "\n"); 
		}
		
		Object[] params 	= new Object[2];
		params[0] 			= (Integer)usuarioEvto.iIdEmpresa;
		params[1] 			= (Integer)usuarioEvto.iIdEvento;
		
		sResultado 			= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Tabla: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		joObj.add("datosMsges", jaJasonArray);
		
		sSentenciaSql =	"SELECT  E.NOM_PERSONA AS P, E.FEC_DE_HASTA AS H, U.FOTO AS F, E.URL_CAMARA AS U, U.NOM_USUARIO AS N, U.GENERO AS G \n" + 
				        "FROM    EVENTO_V E							\n" + 
				        "    JOIN USUARIO_EVENTO_V U  				\n" +
				        "        ON  E.ID_EMPRESA 	= U.ID_EMPRESA 	\n" +
				        "		 AND E.ID_EVENTO  	= U.ID_EVENTO  	\n" +
				        " 		 AND U.ID_USUARIO 	= ?  			\n" +
				        "WHERE   E.ID_EMPRESA  		= ? 			\n" + 
				        "    AND E.ID_EVENTO   		= ? 			\n" ; 
		
		params 		= new Object[3];
		params[0] 	= (Integer)usuarioEvto.iIdUsuario;
		params[1] 	= (Integer)usuarioEvto.iIdEmpresa;
		params[2] 	= (Integer)usuarioEvto.iIdEvento;
		
		sResultado 	= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Evento Tabla: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		joObj.add("datosEvento", jaJasonArray);
		
		sResultado = joObj.toString();

		if(bDebug){
			System.out.println("sResultado: " + sResultado);
		}
		return sResultado;
	}
	
	

	public String sInsertMessage(int iIdEmpresa, int iIdMensaje, int iIdUsuario, String sTxMensaje, String sFotoRecuerdo, 
				String sFotoPropia, String sNombreAlterno, boolean bDebug) throws SQLException{

		String sResultado 		= ""; 
		
		sSentenciaSql = " 	INSERT INTO	EVENTO_MENSAJE(ID_EMPRESA, ID_MENSAJE, ID_USUARIO, TX_MENSAJE, FOTO_RECUERDO, FOTO_PROPIA, NOM_USUARIO_ALTERNO)   	\n" +
						"	VALUES (?,?,?,?,?,?,?) ";

		if(bDebug){
			System.out.println("  QUERY sInsertMessage EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n");
		}
	
		pstmt = conn.prepareStatement(sSentenciaSql);
		
		
		pstmt.setInt(1, 	iIdEmpresa); 
		pstmt.setInt(2, 	iIdMensaje);
		pstmt.setInt(3, 	iIdUsuario);
		pstmt.setString(4, 	sTxMensaje);
		pstmt.setString(5, 	sFotoRecuerdo); 
		pstmt.setString(6, 	sFotoPropia);
		pstmt.setString(7, 	sNombreAlterno);

		rs = pstmt.executeQuery();
		
		return sResultado;
		
	}
	

	public void sUpdateFotoEvento(int iIdEmpresa, int iIdEvento, String sFoto, boolean bDebug) throws SQLException{

		String sResultado 		= ""; 
		
		sSentenciaSql = " 	UPDATE EVENTO SET FOTO = ? WHERE ID_EMPRESA = ? AND ID_EVENTO = ? ";

		if(bDebug){
			System.out.println("  QUERY sInsertMessage EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n");
			System.out.println("  iIdEmpresa ----->>>>>>>>>>>>>>>>>>>  " + iIdEmpresa);
			System.out.println("  iIdEvento ----->>>>>>>>>>>>>>>>>>>  " + iIdEvento);
			System.out.println("  sFoto ----->>>>>>>>>>>>>>>>>>>  " + sFoto);
		}
	
		pstmt = conn.prepareStatement(sSentenciaSql);

		pstmt.setString(1, 	sFoto);
		pstmt.setInt(2, 	iIdEmpresa); 
		pstmt.setInt(3, 	iIdEvento);

		rs = pstmt.executeQuery();
		
	}
	
	public void sUpdateFotoUsuario(int iIdEmpresa, int iIdUsuario, String sFoto, boolean bDebug) throws SQLException{

		sSentenciaSql = " 	UPDATE USUARIO_EVENTO SET FOTO = ? WHERE ID_EMPRESA = ? AND ID_USUARIO = ? ";

		if(bDebug){
			System.out.println("  QUERY sInsertMessage EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n");
			System.out.println("  iIdEmpresa ----->>>>>>>>>>>>>>>>>>>  " + iIdEmpresa);
			System.out.println("  iIdEvento ----->>>>>>>>>>>>>>>>>>>  " + iIdUsuario);
			System.out.println("  sFoto ----->>>>>>>>>>>>>>>>>>>  " + sFoto);
		}
	
		pstmt = conn.prepareStatement(sSentenciaSql);

		pstmt.setString(1, 	sFoto);
		pstmt.setInt(2, 	iIdEmpresa); 
		pstmt.setInt(3, 	iIdUsuario);

		rs = pstmt.executeQuery();
		
	}
	
	
}
