package com.dinamicaps.web.impl.domain.vstream;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.dinamicaps.web.impl.Servicio;
import com.dinamicaps.web.impl.domain.Usuario;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class VStreamUserSrv
 */
public class VStreamUserSrv extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VStreamUserSrv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        
		// Objetos
        UsuarioEvto usuarioEvto	= null;
        int iIdEmpresa	= 0;
        boolean lbDebug	= false;
        
        VStreamDAO dao = new VStreamDAO();
        
        // Se inicializa el sevicio de conexi�n
        Servicio servicio 	= new Servicio();

		//Obtain the session object
        HttpSession session = request.getSession(false);
        
        /*******************         CONTROL DE SESI�N DEL USUARIO        *****************************/
        
        //Check if our session variable is set, if so, get the session variable value
        //which is an Integer object, and add one to the value.
        //If the value is not set, create an Integer object with the default value 1.
        //Add the variable to the session overwriting any possible present values.
        
        
        if(session == null){
	        response.setContentType("text/html");
			request.getRequestDispatcher("indexUser.jsp").forward(request,response);
        	return;
        }else{
        	
        	if(request.getParameter("CerrarSesion") != null){
                System.out.println("*******************         Salir de la sesi�n      ************************************************** ");
        		session.invalidate();
        		//response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
        		//response.setDateHeader("Expires", 0);
    	        response.setContentType("text/html");
    			// request.getRequestDispatcher("index.jsp").forward(request,response);
    			response.sendRedirect("indexUser.jsp");
    			System.out.println("*******************         Salir de la sesi�n      ************************************************** ");
    			return;
        	}
        	
        	
        	System.out.println(" La sesion si existe  ");
        	usuarioEvto 			= (UsuarioEvto) session.getAttribute("UsuarioEvto");
        	
        	if(usuarioEvto != null){
        		
    	        // Se obtinenen los par�metros necesarios para ejecutar la respuesta  
    	        iIdEmpresa 	= usuarioEvto.iIdEmpresa;
    	        lbDebug 	= servicio.bDebug(iIdEmpresa, String.valueOf(usuarioEvto.iIdUsuario));

    	        if(lbDebug){
    	        	System.out.println(" El usuario no es nulo -  Antes del control de sesi�n ");
    	        }
        	}else{
        		System.out.println(" El usuario NO existe  ");

            	if(request.getParameter("login") == null || request.getParameter("password") == null){
            		System.out.println(" NO vienen los parametros de login ");
            		System.out.println(" request.getContextPath():  " + request.getContextPath());
            		try {
            		       response.setStatus(403);
            		    } catch (Throwable t) {
            		        //do something
            		    	t.printStackTrace(); 
            		    }
            		return;
            	}else{
            		System.out.println(" SI vienen los parametros de login:  " + request.getParameter("login") + " Password: " + request.getParameter("password"));
            		// Se genera la sesi�n
    	        	session = request.getSession(true);
    	        	//System.out.println(" Se genera la sesi�n **** ");

    	        	try{
    	        		dao.abreConexion(); 
    	        		usuarioEvto = dao.getUsuarioEvto(request.getParameter("login"), request.getParameter("password"), false);
    	        		dao.conn.commit();
    	        		dao.conn.close();
    	        	}catch(Exception e){
    	        		e.printStackTrace(); 
    	        	}
    	        	
    	        	// Verifica si el usuario captur� el password correcto
    	        	if(usuarioEvto != null && usuarioEvto.bPasswordValido){

            	        // Se obtinenen los par�metros necesarios para ejecutar la respuesta  
            	        iIdEmpresa 		= usuarioEvto.iIdEmpresa;
            	        lbDebug 		= servicio.bDebug(iIdEmpresa, String.valueOf(usuarioEvto.iIdUsuario));
            	        
            			session.setAttribute("UsuarioEvto", usuarioEvto);

            			if(lbDebug){
            				System.out.println(" Password correcto, se agrega el usuario a la sesi�n  **** ");
            			}
            			
            			// set another default locale
            		    Locale.setDefault(new Locale("en", "US"));
            		    
    	        	}
    	        	else{
    	        		if(usuarioEvto != null && !usuarioEvto.bPasswordValido){
    	        			// Si el password es incorrecto se redirecciona a la pantalla de login
                			request.setAttribute("MSG_ERROR", "Password Incorrecto");
            		        response.setContentType("text/html");
            				request.getRequestDispatcher("indexUser.jsp").forward(request,response);
                        	return;
    	        		}else{
    	        			if(usuarioEvto==null){	
    	            			request.setAttribute("MSG_ERROR", "El usuario no existe");
    	        		        response.setContentType("text/html");
    	        				request.getRequestDispatcher("indexUser.jsp").forward(request,response);
    	                    	return;
    	        			}
    	        		}
    	        	}
            	}
        	}
        }

        if(lbDebug){
			System.out.println(" PASA LA VERIFICACI�N DE USUARIO   **** ");
		}
        

        
        String sCtlCveOperacion = request.getParameter("DYN_CTL_CVE_OPERACION"); 
        
        String sResultado = ""; 
        dao = new VStreamDAO();
        
        switch (sCtlCveOperacion){
        	case "GET_MESSAGES_USER": 
        		try{
        			dao.abreConexion();
        			sResultado = dao.sGetMessagesUser(usuarioEvto, lbDebug);
        			
        		} catch(SQLException  excp){
    				excp.printStackTrace();
    				sResultado 				= "[{\"A\":\"Error\"}]";
    				Gson gson 				= new Gson();
    				JsonArray jaJasonArray	= gson.fromJson(sResultado, JsonArray.class); 
    				JsonObject joObj 		= new JsonObject();
    				joObj.add("result", jaJasonArray);
    				sResultado 				= joObj.toString();
    			} finally {
    				try {
    					if( dao != null ){
    						if(dao.conn != null){
    							dao.conn.commit();
    							dao.conn.close();	
    						}							
    					}							
    				} catch(Exception exc){
    					exc.printStackTrace();
    				}			
    			}
        		break;
        }

        response.setHeader("Content-Type", "application/json; charset=utf-8");
		response.setHeader("Cache-Control", "nocache");
		PrintWriter out = response.getWriter();
		out.println(sResultado); 
		out.close();	

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        
		// Objetos
        UsuarioEvto usuarioEvto	= null;
        int iIdEmpresa	= 0;
        boolean lbDebug	= false;
        boolean isMultipart		= false;
        List<FileItem> items 	= null;
        String[] sParametros= new String[2];
        String sResultado		= "";
        
        VStreamDAO dao = new VStreamDAO();
        
        // Se inicializa el sevicio de conexi�n
        Servicio servicio 	= new Servicio();

		//Obtain the session object
        HttpSession session = request.getSession(false);
        
        /*******************         CONTROL DE SESI�N DEL USUARIO        *****************************/
        
        //Check if our session variable is set, if so, get the session variable value
        //which is an Integer object, and add one to the value.
        //If the value is not set, create an Integer object with the default value 1.
        //Add the variable to the session overwriting any possible present values.
        
        
        if(session == null){
	        response.setContentType("text/html");
			request.getRequestDispatcher("indexUser.jsp").forward(request,response);
        	return;
        }else{
        	System.out.println(" La sesion si existe  ");
        	usuarioEvto 			= (UsuarioEvto) session.getAttribute("UsuarioEvto");
        	
        	if(usuarioEvto != null){
        		if(request.getParameter("login") != null && request.getParameter("password") != null){
        			usuarioEvto = null;
        		}
        	}

        	if(usuarioEvto != null){

    	        // Se obtinenen los par�metros necesarios para ejecutar la respuesta  
    	        iIdEmpresa 	= usuarioEvto.iIdEmpresa;
    	        lbDebug 	= servicio.bDebug(iIdEmpresa, String.valueOf(usuarioEvto.iIdUsuario));

    	        if(lbDebug){
    	        	System.out.println(" El usuario no es nulo -  Antes del control de sesi�n ");
    	        }
        	}else{
        		System.out.println(" El usuario NO existe  ");

            	if(request.getParameter("login") == null || request.getParameter("password") == null){
            		System.out.println(" NO vienen los parametros de login ");
            		System.out.println(" request.getContextPath():  " + request.getContextPath());
            		try {
            		       response.setStatus(403);
            		    } catch (Throwable t) {
            		        //do something
            		    	t.printStackTrace(); 
            		    }
            		return;
            	}else{
            		System.out.println(" SI vienen los parametros de login:  " + request.getParameter("login") + " Password: " + request.getParameter("password"));
            		// Se genera la sesi�n
    	        	session = request.getSession(true);
    	        	//System.out.println(" Se genera la sesi�n **** ");

    	        	try{
    	        		dao.abreConexion(); 
    	        		usuarioEvto = dao.getUsuarioEvto(request.getParameter("login"), request.getParameter("password"), false);
    	        		dao.conn.commit();
    	        		dao.conn.close();
    	        	}catch(Exception e){
    	        		e.printStackTrace(); 
    	        	}
    	        	
    	        	// Verifica si el usuario captur� el password correcto
    	        	if(usuarioEvto != null && usuarioEvto.bPasswordValido){

            	        // Se obtinenen los par�metros necesarios para ejecutar la respuesta  
            	        iIdEmpresa 		= usuarioEvto.iIdEmpresa;
            	        lbDebug 		= servicio.bDebug(iIdEmpresa, String.valueOf(usuarioEvto.iIdUsuario));
            	        
            			session.setAttribute("UsuarioEvto", usuarioEvto);

            			if(lbDebug){
            				System.out.println(" Password correcto, se agrega el usuario a la sesi�n  **** ");
            			}
            			
            			// set another default locale
            		    Locale.setDefault(new Locale("en", "US"));
            		    
    	        	}
    	        	else{
    	        		if(usuarioEvto != null && !usuarioEvto.bPasswordValido){
    	        			// Si el password es incorrecto se redirecciona a la pantalla de login
                			request.setAttribute("MSG_ERROR", "Password Incorrecto");
            		        response.setContentType("text/html");
            				request.getRequestDispatcher("indexUser.jsp").forward(request,response);
                        	return;
    	        		}else{
    	        			if(usuarioEvto==null){	
    	            			request.setAttribute("MSG_ERROR", "El usuario no existe");
    	        		        response.setContentType("text/html");
    	        				request.getRequestDispatcher("indexUser.jsp").forward(request,response);
    	                    	return;
    	        			}
    	        		}
    	        	}
            	}
        	}
        }

        if(lbDebug){
			System.out.println(" PASA LA VERIFICACI�N DE USUARIO   **** ");
		}
        

        
        String sCtlCveOperacion = request.getParameter("DYN_CTL_CVE_OPERACION"); 
        
        dao = new VStreamDAO();
        
        switch (sCtlCveOperacion){
        case "ALTA_MENSAJE": 
        	
        	isMultipart = ServletFileUpload.isMultipartContent(request);
        	
        	try{
        	
        		dao.abreConexion(); 
        	
        		int iIdMensaje 			= dao.iIdMensaje(); 
        		String sTxMensaje 		= request.getParameter("TX_MENSAJE");
        		String sNomUsuario 		= request.getParameter("NOM_USUARIO_ALTERNO"); 
        		String sNomFotoPropia	= null;
        		String sNomFotoRecuerdo	= null;

	        	if(isMultipart){
	        		
	        		if(lbDebug){
	        			System.out.println("Es Multipart  ****************************** ");
	        			System.out.println("TX_MENSAJE  ****************************** " + request.getParameter("TX_MENSAJE"));
	        			System.out.println("NOM_USUARIO_ALTERNO  ****************************** " + request.getParameter("NOM_USUARIO_ALTERNO"));
	        		}
	        		
	        		
	
	        		// Para subir archivos a una carpeta 
	        		FileItemFactory factory = new DiskFileItemFactory();
	                ServletFileUpload upload = new ServletFileUpload(factory);
	                try {
	                	
	                	items = upload.parseRequest(request);
	                    if(lbDebug){
	                    	System.out.println("items: " + items);
	                    }
	                } catch (FileUploadException e) {
	                    e.printStackTrace();
	                }
	                
	                if(items.size()>0){
	                
		                FileItem file 			= items.get(0);
		                String sFileName 		= iIdMensaje + "1-" + file.getName();
		                sFileName				= sFileName.replaceAll("[^a-zA-Z0-9\\.-]", ""); 
		                long lSize 				= file.getSize();
		                String sContentType		= file.getContentType();
		                String sFieldName		= file.getFieldName(); 
		                if(sFieldName.equals("FOTO_RECUERDO-0")){
		                	sNomFotoRecuerdo= sFileName; 
		                }else{
		                	sNomFotoPropia 	= sFileName; 
		                }
		
		                if(lbDebug){
		        			System.out.println("Es Multipart sContentType: " + sContentType);
		        			System.out.println("sFileName: " + sFileName);
		        		}
		                
		                try{
		                	if(sContentType.contains("image/")){
		                		
		                		int iMaxBytes = Integer.parseInt(servicio.sDameValorParametro(usuarioEvto.iIdEmpresa, "VARIABLES_CONFIGURACION", 
		                    			"IMG_USUARIO", String.valueOf(usuarioEvto.iIdUsuario), lbDebug));

		                		if(lbDebug){
			                		System.out.println("iMaxBytes: " + iMaxBytes);
			                	}
		                		if(lSize <= iMaxBytes){
									//File uploadedFile = new File( "C:\\app\\Tecnofin\\product\\11.2.0\\dbhome_1\\files\\schema\\doctos\\" + sFileName);
				                	File uploadedFile = new File( servicio.sDameUrlDirectorio("VSTREAM", lbDebug) + sFileName);
				
				                	if(lbDebug){
				                		System.out.println("uploadedFile.getAbsolutePath(): " + uploadedFile.getAbsolutePath());
				                	}
			                	
									//Escribe el archivo
									file.write(uploadedFile);
									
									if(lbDebug){
										System.out.println("NOMBRE DEL ARCHIVO ---->>>> " + sFileName);
										System.out.println("file.getContentType() ---->>>> " + file.getContentType());
									}
									
		                		}else{
		                			sParametros[0] = null;
		                			sParametros[1] = servicio.sDameMensajeSistema(65, String.valueOf(usuarioEvto.iIdUsuario), lbDebug);
		                			sResultado	= new Gson().toJson(sParametros);   
		                			response.setContentType("application/json");
		                			response.setCharacterEncoding("UTF-8");
		                			response.getWriter().write(sResultado);   
		                    		return;	
		                		}
	                		} // si contiene image/
	                		else{
		                		sParametros[0] = null;
		            			sParametros[1] = servicio.sDameMensajeSistema(66, String.valueOf(usuarioEvto.iIdUsuario), lbDebug);
		            			sResultado	= new Gson().toJson(sParametros);   
		            			response.setContentType("application/json");
		            			response.setCharacterEncoding("UTF-8");
		            			response.getWriter().write(sResultado);   
		                		return;	
		                	}
	                	}catch (Exception e) {
	                		e.printStackTrace();
	                	}
		                
		                if(items.size()>1){
		                	
		                	FileItem file2 			= items.get(1);
			                String sFileName2 		= iIdMensaje + "2-" + file2.getName();
			                sFileName2				= sFileName2.replaceAll("[^a-zA-Z0-9\\.-]", ""); 
			                long lSize2				= file2.getSize();
			                String sContentType2	= file2.getContentType();
			                String sFieldName2		= file2.getFieldName(); 
			                if(sFieldName2.equals("FOTO_RECUERDO-0")){
			                	sNomFotoRecuerdo= sFileName2; 
			                }else{
			                	sNomFotoPropia 	= sFileName2; 
			                }
			
			                if(lbDebug){
			        			System.out.println("Es Multipart sContentType: " + sContentType2);
			        			System.out.println("sFileName: " + sFileName2);
			        		}
			                
			                try{
			                	if(sContentType2.contains("image/")){
			                		
			                		int iMaxBytes = Integer.parseInt(servicio.sDameValorParametro(usuarioEvto.iIdEmpresa, "VARIABLES_CONFIGURACION", 
			                    			"IMG_USUARIO", String.valueOf(usuarioEvto.iIdUsuario), lbDebug));
	
			                		if(lbDebug){
				                		System.out.println("iMaxBytes: " + iMaxBytes);
				                	}
			                		if(lSize2 <= iMaxBytes){

			                			File uploadedFile = new File( servicio.sDameUrlDirectorio("VSTREAM", lbDebug) + sFileName2);
					
					                	if(lbDebug){
					                		System.out.println("uploadedFile.getAbsolutePath(): " + uploadedFile.getAbsolutePath());
					                	}
				                	
										//Escribe el archivo
					                	file2.write(uploadedFile);
										
										if(lbDebug){
											System.out.println("NOMBRE DEL ARCHIVO ---->>>> " + sFileName2);
											System.out.println("file.getContentType() ---->>>> " + file2.getContentType());
										}
										
			                		}else{
			                			sParametros[0] = null;
			                			sParametros[1] = servicio.sDameMensajeSistema(65, String.valueOf(usuarioEvto.iIdUsuario), lbDebug);
			                			sResultado	= new Gson().toJson(sParametros);   
			                			response.setContentType("application/json");
			                			response.setCharacterEncoding("UTF-8");
			                			response.getWriter().write(sResultado);   
			                    		return;	
			                		}
		                		} // si contiene image/
		                		else{
			                		sParametros[0] = null;
			            			sParametros[1] = servicio.sDameMensajeSistema(66, String.valueOf(usuarioEvto.iIdUsuario), lbDebug);
			            			sResultado	= new Gson().toJson(sParametros);   
			            			response.setContentType("application/json");
			            			response.setCharacterEncoding("UTF-8");
			            			response.getWriter().write(sResultado);   
			                		return;	
			                	}
		                	}catch (Exception e) {
		                		e.printStackTrace();
		                	}
		                	
		                	
		                }
		                
	                }
	        	}
	        	
	        	sResultado	= dao.sInsertMessage(usuarioEvto.iIdEmpresa, iIdMensaje, usuarioEvto.iIdUsuario, sTxMensaje, sNomFotoRecuerdo, 
	        			sNomFotoPropia, sNomUsuario, lbDebug); 
	        	sResultado	= new Gson().toJson(sParametros);   
    			response.setContentType("application/json");
    			response.setCharacterEncoding("UTF-8");
    			response.getWriter().write(sResultado);   
        		return;	
	        	
        	} catch(Exception excp){
				excp.printStackTrace();			
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
        	break;
        }

        if(sCtlCveOperacion.equals("VIDEO")){
    		response.setContentType("text/html");
    		String context_path = request.getContextPath(); 
    		response.sendRedirect(context_path + "/usr/vst/video/video.html"); 
    		//request.getRequestDispatcher("/usr/vst/video/video.html").forward(request,response);
    		return;
        }
		
        response.setHeader("Content-Type", "application/json; charset=utf-8");
		response.setHeader("Cache-Control", "nocache");
		PrintWriter out = response.getWriter();
		out.println(sResultado); 
		out.close();	}

}
