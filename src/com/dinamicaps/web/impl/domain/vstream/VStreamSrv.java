package com.dinamicaps.web.impl.domain.vstream;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.dinamicaps.web.impl.Servicio;
import com.dinamicaps.web.impl.domain.Archivo;
import com.dinamicaps.web.impl.domain.BrowserType;
import com.dinamicaps.web.impl.domain.Usuario;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.Gson;

/**
 * Servlet implementation class VStreamSrv
 */
public class VStreamSrv extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VStreamSrv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        
        if(request.getParameter("AP_PATERNO")!=null){
        	String sTest = new String(request.getParameter("AP_PATERNO").getBytes("ISO-8859-1"),"UTF-8");
        	System.out.println(" sTest:  " + sTest);
        }
		
		// Objetos
        Usuario usuario	= null;
        int iIdEmpresa	= 0;
        boolean lbDebug	= false;

		// Se inicializa el sevicio de conexi�n
        Servicio servicio 	= new Servicio();

        //Obtain the session object
        HttpSession session = request.getSession(false);
        
        /*******************         CONTROL DE SESI�N DEL USUARIO        *****************************/
        
        //Check if our session variable is set, if so, get the session variable value
        //which is an Integer object, and add one to the value.
        //If the value is not set, create an Integer object with the default value 1.
        //Add the variable to the session overwriting any possible present values.
        
        
        if(session == null){
	        response.setContentType("text/html");
			request.getRequestDispatcher("index.jsp").forward(request,response);
        	return;
        }else{
        	System.out.println(" La sesion si existe  ");
        	usuario 			= (Usuario) session.getAttribute("Usuario");
        	
        	if(usuario != null){
        		
    	        // Se obtinenen los par�metros necesarios para ejecutar la respuesta  
    	        iIdEmpresa 	= usuario.iIdEmpresa;
    	        lbDebug 	= servicio.bDebug(iIdEmpresa, usuario.sCveUsuario);

    	        if(lbDebug){
    	        	System.out.println(" El usuario no es nulo -  Antes del control de sesi�n ");
    	        }
        	}else{
        		System.out.println(" El usuario NO existe  ");
        		//System.out.println(" El usuario es nulo **** ");
            	// Se verifica que hayan enviado los par�metros para realizar el login
            	if(request.getParameter("login") == null || request.getParameter("password") == null){
            		//System.out.println(" No llegan los datos de login **** ");
            		//response.sendRedirect(request.getContextPath() + "/index.jsp");
            		//response.sendRedirect("www.google.com");
            		System.out.println(" NO vienen los parametros de login ");
            		System.out.println(" request.getContextPath():  " + request.getContextPath());
            		try {
            		       response.setStatus(403);
            		    } catch (Throwable t) {
            		        //do something
            		    	t.printStackTrace(); 
            		    }
            		return;
            	}else{
            		System.out.println(" SI vienen los parametros de login:  " + request.getParameter("login") + " Password: " + request.getParameter("password"));
            		// Se genera la sesi�n
    	        	session = request.getSession(true);
    	        	//System.out.println(" Se genera la sesi�n **** ");

    	        	// Se verifica que la interface est� compilada
    	        	//System.out.println(" servicio.CompilaInterface 3 **** " + servicio.sCompilaInterface(false));        
    	        	// Se obtiene el usuario y se valida el password
    	        	usuario = servicio.getUsuario(request.getParameter("login"), request.getParameter("password"), request.getParameter("usuario_evento"), false);

    	        	if(usuario != null && usuario.sCveUsuario.equals("ERROR_EN_SYSTEMA")){
    	        		if(lbDebug){
    	        			System.out.println(" Error al obtener el usuario ");
    	        		}
            			request.setAttribute("MSG_ERROR", "El servicio no est� disponible, por favor comun�quese con el administrador");
        		        response.setContentType("text/html");
        				request.getRequestDispatcher("index.jsp").forward(request,response);
                    	return;
    	        	}

    	        	//System.out.println(" Se obtiene el usuario  **** ");

    	        	// Verifica si el usuario captur� el password correcto
    	        	if(usuario != null && usuario.sPasswordValido.equals("V")){

            			if(usuario.sSettings.equals("V")){
                			request.setAttribute("MSG_ERROR", "Por favor actualice su versi�n del sistema");
            		        response.setContentType("text/html");
            				request.getRequestDispatcher("index.jsp").forward(request,response);
                        	return;
            			}

            	        // Se obtinenen los par�metros necesarios para ejecutar la respuesta  
            	        iIdEmpresa 		= usuario.iIdEmpresa;
            	        lbDebug 		= servicio.bDebug(iIdEmpresa, usuario.sCveUsuario);
            	        
            			session.setAttribute("Usuario", usuario);

            			if(lbDebug){
            				System.out.println(" Password correcto, se agrega el usuario a la sesi�n  **** ");
            			}
            			
            			// set another default locale
            		    Locale.setDefault(new Locale("en", "US"));
            		    
    	        	}
    	        	else{
    	        		if(usuario != null && usuario.sPasswordValido.equals("F")){
    	        			if(usuario.sSettings.equals("V")){
    	            			request.setAttribute("MSG_ERROR", "Por favor actualice su versi�n del sistema");
    	        		        response.setContentType("text/html");
    	        				request.getRequestDispatcher("index.jsp").forward(request,response);
    	                    	return;
    	        			}
    		        		// Si el password es incorrecto se redirecciona a la pantalla de login
                			request.setAttribute("MSG_ERROR", "Password Incorrecto");
            		        response.setContentType("text/html");
            				request.getRequestDispatcher("index.jsp").forward(request,response);
                        	return;
    	        		}else{
    	        			if(usuario==null){	
    	            			request.setAttribute("MSG_ERROR", "El usuario no existe");
    	        		        response.setContentType("text/html");
    	        				request.getRequestDispatcher("index.jsp").forward(request,response);
    	                    	return;
    	        			}
    	        		}
    	        	}
            	}
        	}
        }

        if(lbDebug){
			System.out.println(" PASA LA VERIFICACI�N DE USUARIO   **** ");
		}
        
        String sCtlCveOperacion = request.getParameter("DYN_CTL_CVE_OPERACION"); 
        
        String sResultado = ""; 
        VStreamDAO dao = new VStreamDAO();
        int iIdEvento; 
        
        switch (sCtlCveOperacion){
        	case "UPDATE_STATUS_MSG": 
        		String sStatus = request.getParameter("SIT_MENSAJE");
        		int iIdMensaje = Integer.parseInt(request.getParameter("ID_MENSAJE").trim());
        		try{
        			dao.abreConexion();	
        			sResultado = dao.sUpdateMessageStatus(iIdEmpresa, iIdMensaje, sStatus, usuario.sCveUsuario, lbDebug);
        		} catch(SQLException  excp){
    				excp.printStackTrace();
    				sResultado 				= "[{\"A\":\"Error\"}]";
    				Gson gson 				= new Gson();
    				JsonArray jaJasonArray	= gson.fromJson(sResultado, JsonArray.class); 
    				JsonObject joObj 		= new JsonObject();
    				joObj.add("result", jaJasonArray);
    				sResultado 				= joObj.toString();
    			} finally {
    				try {
    					if( dao != null ){
    						if(dao.conn != null){
    							dao.conn.close();	
    						}							
    					}							
    				} catch(Exception exc){
    					exc.printStackTrace();
    				}			
    			}
        		break;
        	case "GET_MESSAGES": 
        		iIdEvento = Integer.parseInt(request.getParameter("DYN_CTL_ID_EVENTO").trim());
        		try{
        			dao.abreConexion();	
        			sResultado = dao.sGetMessages(iIdEmpresa, iIdEvento, lbDebug);
        		} catch(SQLException  excp){
    				excp.printStackTrace();
    				sResultado 				= "[{\"A\":\"Error\"}]";
    				Gson gson 				= new Gson();
    				JsonArray jaJasonArray	= gson.fromJson(sResultado, JsonArray.class); 
    				JsonObject joObj 		= new JsonObject();
    				joObj.add("result", jaJasonArray);
    				sResultado 				= joObj.toString();
    			} finally {
    				try {
    					if( dao != null ){
    						if(dao.conn != null){
    							dao.conn.close();	
    						}							
    					}							
    				} catch(Exception exc){
    					exc.printStackTrace();
    				}			
    			}
        		break;
        }
        
      //response.setHeader("Content-Type", "application/json; charset=ISO-8859-1");
        response.setHeader("Content-Type", "application/json; charset=utf-8");
        //response.setContentType("application/json");
		response.setHeader("Cache-Control", "nocache");

       	/*
       	String jsonString = new Gson().toJson(sResponse);
       	byte[] utf8JsonString = jsonString.getBytes("UTF8");
       	responseToClient.write(utf8JsonString, 0, utf8JsonString.Length);
       	
       	
       	String json = new Gson().toJson(sResponse);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
       	*/
       	
       	
       	//response.setContentType("application/json;charset=utf-8");
        //response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		//out.write(utf8JsonString, 0, utf8JsonString.length);
		out.println(sResultado); 
		out.close();
        
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Usuario usuario			= null;
        Archivo archivo			= null;
        BrowserType browserType	= new BrowserType(request.getHeader("user-agent"));
        boolean isMultipart		= false;
        boolean lbDebug			= true;
        List<FileItem> items 	= null;
        String[] sParametros= new String[2];
        String sResultado		= "";

        // Se inicializa el sevicio de conexi�n
        Servicio servicio = new Servicio();

        //Obtain the session object
        HttpSession session = request.getSession(false);

        //Check if our session variable is set, if so, get the session variable value
        //which is an Integer object, and add one to the value.
        //If the value is not set, create an Integer object with the default value 1.
        //Add the variable to the session overwriting any possible present values.
        
        if(session != null){
        	usuario = (Usuario) session.getAttribute("Usuario");
        }

        if (usuario == null) {
			System.out.println(" Entra al send redirect");
			response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp?SinSesion=V&ErrorMessage=Su sesi�n ha expirado, por favor vuelva a ingresar a la p�gina");
			return;
        }else{

            if(lbDebug){
            	
    	        System.out.println("***********************************************************************************************************: ");
    	        System.out.println("Usuario: " + usuario.sCveUsuario + " ejecuta POST AdmContenidosSrv " );
    	        System.out.println("BROWSER: " + browserType.getName());
    	        System.out.println("VERSION: " + browserType.getVersion());
    	        System.out.println("PARAM_CTL_CVE_NAVEGACION: " + request.getParameter("PARAM_CTL_CVE_NAVEGACION"));
    	        System.out.println("PARAM_CTL_PANTALLA: " + request.getParameter("PARAM_CTL_PANTALLA"));
    	        System.out.println("PARAM_CTL_ID_EMPRESA: " + request.getParameter("PARAM_CTL_ID_EMPRESA"));
    	        System.out.println("PARAM_CTL_ID_TABLA: " + request.getParameter("PARAM_CTL_ID_TABLA"));
    	        System.out.println("request.getContextPath(): " + request.getContextPath());
    	        System.out.println("DYN_CTL_CVE_OPERACION: " + request.getParameter("DYN_CTL_CVE_OPERACION"));
            }
            
            
            String sCveOperacion = request.getParameter("DYN_CTL_CVE_OPERACION"); 
            

            switch (sCveOperacion){
            case "ALTA_MENSAJE": 
            	
            	isMultipart = ServletFileUpload.isMultipartContent(request);
            	
            	VStreamDAO dao = new VStreamDAO(); 
            	try{
            	
            		dao.abreConexion(); 
            	
            		int iIdMensaje 			= dao.iIdMensaje(); 
            		String sTxMensaje 		= request.getParameter("TX_MENSAJE");
	        		String sNomUsuario 		= request.getParameter("NOM_USUARIO_ALTERNO"); 
	        		String sNomFotoPropia	= null;
	        		String sNomFotoRecuerdo	= null;

		        	if(isMultipart){
		        		
		        		if(lbDebug){
		        			System.out.println("Es Multipart  ****************************** ");
		        			System.out.println("TX_MENSAJE  ****************************** " + request.getParameter("TX_MENSAJE"));
		        			System.out.println("NOM_USUARIO_ALTERNO  ****************************** " + request.getParameter("NOM_USUARIO_ALTERNO"));
		        		}
		        		
		        		
		
		        		// Para subir archivos a una carpeta 
		        		FileItemFactory factory = new DiskFileItemFactory();
		                ServletFileUpload upload = new ServletFileUpload(factory);
		                try {
		                	
		                	items = upload.parseRequest(request);
		                    if(lbDebug){
		                    	System.out.println("items: " + items);
		                    }
		                } catch (FileUploadException e) {
		                    e.printStackTrace();
		                } 
		                
		                if(items.size()>0){
		                
			                FileItem file 			= items.get(0);
			                String sFileName 		= iIdMensaje + "1-" + file.getName();
			                sFileName				= sFileName.replaceAll("[^a-zA-Z0-9\\.-]", ""); 
			                long lSize 				= file.getSize();
			                String sContentType		= file.getContentType();
			                String sFieldName		= file.getFieldName(); 
			                if(lbDebug){
		                    	System.out.println("sFileName: " + sFileName + "    -- sFieldName: " + sFieldName);
		                    }
			                if(sFieldName.equals("FOTO_RECUERDO-0")){
			                	sNomFotoRecuerdo= sFileName; 
			                }else{
			                	sNomFotoPropia 	= sFileName; 
			                }
			
			                if(lbDebug){
			        			System.out.println("Es Multipart sContentType: " + sContentType);
			        			System.out.println("sFileName: " + sFileName);
			        		}
			                
			                try{
			                	if(sContentType.contains("image/")){
			                		
			                		int iMaxBytes = Integer.parseInt(servicio.sDameValorParametro(usuario.iIdEmpresa, "VARIABLES_CONFIGURACION", 
			                    			"IMG_USUARIO", usuario.sCveUsuario, lbDebug));
	
			                		if(lbDebug){
				                		System.out.println("iMaxBytes: " + iMaxBytes);
				                	}
			                		if(lSize <= iMaxBytes){
										//File uploadedFile = new File( "C:\\app\\Tecnofin\\product\\11.2.0\\dbhome_1\\files\\schema\\doctos\\" + sFileName);
					                	File uploadedFile = new File( servicio.sDameUrlDirectorio(usuario.sUserSchema, lbDebug) + sFileName);
					
					                	if(lbDebug){
					                		System.out.println("uploadedFile.getAbsolutePath(): " + uploadedFile.getAbsolutePath());
					                	}
				                	
										//Escribe el archivo
										file.write(uploadedFile);
										
										if(lbDebug){
											System.out.println("NOMBRE DEL ARCHIVO ---->>>> " + sFileName);
											System.out.println("file.getContentType() ---->>>> " + file.getContentType());
										}
										
			                		}else{
			                			sParametros[0] = null;
			                			sParametros[1] = servicio.sDameMensajeSistema(65, usuario.sCveUsuario, lbDebug);
			                			sResultado	= new Gson().toJson(sParametros);   
			                			response.setContentType("application/json");
			                			response.setCharacterEncoding("UTF-8");
			                			response.getWriter().write(sResultado);   
			                    		return;	
			                		}
		                		} // si contiene image/
		                		else{
			                		sParametros[0] = null;
			            			sParametros[1] = servicio.sDameMensajeSistema(66, usuario.sCveUsuario, lbDebug);
			            			sResultado	= new Gson().toJson(sParametros);   
			            			response.setContentType("application/json");
			            			response.setCharacterEncoding("UTF-8");
			            			response.getWriter().write(sResultado);   
			                		return;	
			                	}
		                	}catch (Exception e) {
		                		e.printStackTrace();
		                	}
			                
			                if(items.size()>1){
			                	
			                	FileItem file2 			= items.get(1);
				                String sFileName2 		= iIdMensaje + "2-" + file2.getName();
				                sFileName2				= sFileName2.replaceAll("[^a-zA-Z0-9\\.-]", ""); 
				                long lSize2				= file2.getSize();
				                String sContentType2	= file2.getContentType();
				                String sFieldName2		= file2.getFieldName();
				                
				                if(lbDebug){
			                    	System.out.println("sFileName2: " + sFileName2 + "    -- sFieldName2: " + sFieldName2);
			                    }

				                if(sFieldName2.equals("FOTO_RECUERDO-0")){
				                	sNomFotoRecuerdo= sFileName2; 
				                }else{
				                	sNomFotoPropia 	= sFileName2; 
				                }

				                if(lbDebug){
				        			System.out.println("Es Multipart sContentType: " + sContentType2);
				        			System.out.println("sFileName: " + sFileName2);
				        		}
				                
				                try{
				                	if(sContentType2.contains("image/")){
				                		
				                		int iMaxBytes = Integer.parseInt(servicio.sDameValorParametro(usuario.iIdEmpresa, "VARIABLES_CONFIGURACION", 
				                    			"IMG_USUARIO", usuario.sCveUsuario, lbDebug));
		
				                		if(lbDebug){
					                		System.out.println("iMaxBytes: " + iMaxBytes);
					                	}
				                		if(lSize2 <= iMaxBytes){

				                			File uploadedFile = new File( servicio.sDameUrlDirectorio(usuario.sUserSchema, lbDebug) + sFileName2);
						
						                	if(lbDebug){
						                		System.out.println("uploadedFile.getAbsolutePath(): " + uploadedFile.getAbsolutePath());
						                	}
					                	
											//Escribe el archivo
						                	file2.write(uploadedFile);
											
											if(lbDebug){
												System.out.println("NOMBRE DEL ARCHIVO ---->>>> " + sFileName2);
												System.out.println("file.getContentType() ---->>>> " + file2.getContentType());
											}
											
				                		}else{
				                			sParametros[0] = null;
				                			sParametros[1] = servicio.sDameMensajeSistema(65, usuario.sCveUsuario, lbDebug);
				                			sResultado	= new Gson().toJson(sParametros);   
				                			response.setContentType("application/json");
				                			response.setCharacterEncoding("UTF-8");
				                			response.getWriter().write(sResultado);   
				                    		return;	
				                		}
			                		} // si contiene image/
			                		else{
				                		sParametros[0] = null;
				            			sParametros[1] = servicio.sDameMensajeSistema(66, usuario.sCveUsuario, lbDebug);
				            			sResultado	= new Gson().toJson(sParametros);   
				            			response.setContentType("application/json");
				            			response.setCharacterEncoding("UTF-8");
				            			response.getWriter().write(sResultado);   
				                		return;	
				                	}
			                	}catch (Exception e) {
			                		e.printStackTrace();
			                	}
			                	
			                	
			                }
			                
		                }
		        	}
		        	
		        	sResultado	= dao.sInsertMessage(usuario.iIdEmpresa, iIdMensaje, 72, sTxMensaje, sNomFotoRecuerdo, 
		        			sNomFotoPropia, sNomUsuario, lbDebug); 
		        	sResultado	= new Gson().toJson(sParametros);   
        			response.setContentType("application/json");
        			response.setCharacterEncoding("UTF-8");
        			response.getWriter().write(sResultado);   
            		return;	
		        	
            	} catch(Exception excp){
    				excp.printStackTrace();			
    			} finally {
    				try {
    					if( dao != null ){
    						if(dao.conn != null){
    							dao.conn.commit();
    							dao.conn.close();	
    						}							
    					}							
    				} catch(Exception exc){
    					exc.printStackTrace();
    				}			
    			}
	        	break;
            	
            	
            }
            
            
        	
        }
	}

}
