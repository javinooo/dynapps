package com.dinamicaps.web.impl.domain.vstream;

import java.util.Properties;
import java.util.Scanner;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

import com.sun.mail.smtp.SMTPMessage;


/**
 *  Following jar are required:
 *  1) mail-1.4.7.jar from http://central.maven.org/maven2/javax/mail/mail/1.4.7/mail-1.4.7.jar
 *  2) activation-1.1.1.jar from http://central.maven.org/maven2/javax/activation/activation/1.1.1/activation-1.1.1.jar
 *
 */
public class EnviaCorreo {

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.print("gmail username: ");
        String username = sc.next();
        System.out.print("gmail password: ");
        String password = sc.next();
        System.out.print("destination email address: ");
        String to = sc.next();
        System.out.print("subject: ");
        String subject = sc.next();
        System.out.print("email body: ");
        String email_body = sc.next();
        EnviaCorreo test = new EnviaCorreo();
        test.doSendMail(username, password, to, subject, email_body, "C:\\Users\\Tecnofin\\workspace\\DinamicapsJava\\WebContent\\img\\gayosso-1.jpg");
        sc.close();

    }
    // sends mail
    public void doSendMail(final String username, final String password, String to, String subject, String email_body, String sImagePath) {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "587");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", "587");

        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        try {
        	
        	SMTPMessage m = new SMTPMessage(session);
        	MimeMultipart content 	= new MimeMultipart("related");
        	MimeBodyPart textPart  	= new MimeBodyPart();
        	String cid = "user15";
        	
        	/*
        	textPart.setText("<html><head>"
					   + "<title>This is not usually displayed</title>"
					   + "</head>n"
					   + "<body><div><strong>Hi there!</strong></div>"
					   + "<div>Sending HTML in email is so <em>cool!</em> </div>n"
					   + "<div>And here's an image: <img src=\"cid:imgcode\"/>" 
					   + "</div>n" + "<div>I hope you like it!</div></body></html>",
					   "US-ASCII", "html");*/
        	
        	
        	textPart.setText(email_body,"US-ASCII", "html");
        	
        	content.addBodyPart(textPart);
        	
        	MimeBodyPart imagePart = new MimeBodyPart();
        	//System.out.println("Ruta: " + System.getProperty("java.class.path"));

        	System.out.println("sImagePath: " + sImagePath); 
        	imagePart.attachFile(sImagePath);

        	imagePart.setContentID("<imgcode>");

        	imagePart.setDisposition(MimeBodyPart.INLINE);

        	//content.addBodyPart(imagePart);
        	
        	//System.out.println("email_body: \n" + email_body);
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(subject);
            message.setContent(content, "text/html");
            
                       
            Transport.send(message);
            System.out.println("message sent");
            //JOptionPane.showMessageDialog(null, "Message Sent!", "Sent", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
        	System.out.println(e);
            e.printStackTrace();
            //JOptionPane.showMessageDialog(null, e.toString());
        }
    }
}