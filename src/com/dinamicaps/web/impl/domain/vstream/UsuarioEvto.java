package com.dinamicaps.web.impl.domain.vstream;

public class UsuarioEvto {

		public int iIdEmpresa;
		public int iIdEvento;
		public int iIdSucursal;
		public int iIdSala;
		public int iIdRol;
		public int iIdUsuario;
		public String sNomUsuario = "";
		public boolean bPasswordValido;
		public String sGenero = "";
		public String sFoto = "";
		public String sSitUsuario = "";
}
