package com.dinamicaps.web.impl.domain.media;


import java.sql.SQLException;
import java.util.ArrayList;

import com.dinamicaps.web.impl.domain.media.OrdenInsercionDet;
import com.dinamicaps.web.impl.ServicioConexion;

public class ServicioCalendario {
	
	ServicioConexion servicioConexion = new ServicioConexion();	
	String sSentenciaSql = null;
	
	public ArrayList<OrdenInsercionDet> obtenListaOrdenInsercionBD(int idEmpresa, int idInsercion, String sFecha, int iIdTarifa) throws SQLException, Exception {
		ArrayList<OrdenInsercionDet> listaOrdenInsercion = new ArrayList<OrdenInsercionDet>();
		sSentenciaSql = "SELECT ID, ID_INSERCION_CANTIDAD, CVE_ESTATUS FROM ORDEN_PUBLICITARIA_INS_CANT_DT WHERE ID_INSERCION_CANTIDAD = " + idInsercion + " ORDER BY ID";
		try {
			System.out.println("sSentenciaSql: " + sSentenciaSql);
			servicioConexion.abreConexion();
			servicioConexion.sSentenciaSql = sSentenciaSql;
			servicioConexion.ejecutaSentencia();
			
			while (servicioConexion.rs.next()) {
				OrdenInsercionDet ordenInsercionDet = new OrdenInsercionDet();
				ordenInsercionDet.setIdOrdenInsercion(servicioConexion.rs.getInt("ID_INSERCION_CANTIDAD"));
				ordenInsercionDet.setId(Integer.parseInt(servicioConexion.rs.getString("ID")));
				ordenInsercionDet.setCveEstatus(servicioConexion.rs.getString("CVE_ESTATUS"));
				listaOrdenInsercion.add(ordenInsercionDet);
			}
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		}
		return listaOrdenInsercion;
	}
	
	public void actualizaOrdenPubliDet(int iId, String sCveEstatus) throws SQLException {
		sSentenciaSql = "UPDATE ORDEN_PUBLICITARIA_INS_CANT_DT SET CVE_ESTATUS = '" + sCveEstatus + "' WHERE ID = " + iId;
		try {
			servicioConexion.abreConexion();
			servicioConexion.sSentenciaSql = sSentenciaSql;
			servicioConexion.ejecutaSentencia();
		} catch(SQLException e) {
			throw e;			
		}		
	}

	public void eliminaOrdenPubliDet(int iId) throws SQLException {
		sSentenciaSql = "DELETE FROM ORDEN_PUBLICITARIA_INS_CANT_DT WHERE ID = " + iId;
		try {
			servicioConexion.abreConexion();
			servicioConexion.sSentenciaSql = sSentenciaSql;
			servicioConexion.ejecutaSentencia();
		} catch(SQLException e) {
			throw e;			
		}
		
	}

}
