package com.dinamicaps.web.impl.domain.media;

/**
 *
 * @author Pame
 */
public class ReportVO {
    
    private String idEmpresa = ""; 
    private String idPlan = "";
    private String idOrden = "";
    private String cveCliente = "";
    private String contratoMaestro = "";    
    private String nombreTarifa = "";
    private String folio = "";
    private String cveEncClienteAg = "";
    private String plataforma = "";
    private String categoriaProducto = "";
    private String totalSpots = "";
    private String totalOrdenSinDesc = "";
    private String totalOrdenConDesc = "";
    private String nomEmailResponsable = "";
    private String procesarPorLinea = "";
        
    private String canalOrden = "";
    private String tipoFacturacion = "";
    private String comentarios = "";
    private String cpsMasterContact = "";            
    private String descUsoInterno = "";
    private String masterContact = "";
    private String tarifaCutIn = "";        
    private String garantizado = "";    
    private String referenciaFolio = "";       

    /**
     * @return the cveCliente
     */
    public String getCveCliente() {
        return cveCliente;
    }

    /**
     * @param cveCliente the cveCliente to set
     */
    public void setCveCliente(String cveCliente) {
        this.cveCliente = cveCliente;
    }

    /**
     * @return the cveEncClienteAg
     */
    public String getCveEncClienteAg() {
        return cveEncClienteAg;
    }

    /**
     * @param cveEncClienteAg the cveEncClienteAg to set
     */
    public void setCveEncClienteAg(String cveEncClienteAg) {
        this.cveEncClienteAg = cveEncClienteAg;
    }

    /**
     * @return the canalOrden
     */
    public String getCanalOrden() {
        return canalOrden;
    }

    /**
     * @param canalOrden the canalOrden to set
     */
    public void setCanalOrden(String canalOrden) {
        this.canalOrden = canalOrden;
    }

    /**
     * @return the tipoFacturacion
     */
    public String getTipoFacturacion() {
        return tipoFacturacion;
    }

    /**
     * @param tipoFacturacion the tipoFacturacion to set
     */
    public void setTipoFacturacion(String tipoFacturacion) {
        this.tipoFacturacion = tipoFacturacion;
    }

    /**
     * @return the comentarios
     */
    public String getComentarios() {
        return comentarios;
    }

    /**
     * @param comentarios the comentarios to set
     */
    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    /**
     * @return the cpsMasterContact
     */
    public String getCpsMasterContact() {
        return cpsMasterContact;
    }

    /**
     * @param cpsMasterContact the cpsMasterContact to set
     */
    public void setCpsMasterContact(String cpsMasterContact) {
        this.cpsMasterContact = cpsMasterContact;
    }

    /**
     * @return the nombreTarifa
     */
    public String getNombreTarifa() {
        return nombreTarifa;
    }

    /**
     * @param nombreTarifa the nombreTarifa to set
     */
    public void setNombreTarifa(String nombreTarifa) {
        this.nombreTarifa = nombreTarifa;
    }

    /**
     * @return the categoriaProducto
     */
    public String getCategoriaProducto() {
        return categoriaProducto;
    }

    /**
     * @param categoriaProducto the categoriaProducto to set
     */
    public void setCategoriaProducto(String categoriaProducto) {
        this.categoriaProducto = categoriaProducto;
    }

    /**
     * @return the descUsoInterno
     */
    public String getDescUsoInterno() {
        return descUsoInterno;
    }

    /**
     * @param descUsoInterno the descUsoInterno to set
     */
    public void setDescUsoInterno(String descUsoInterno) {
        this.descUsoInterno = descUsoInterno;
    }

    /**
     * @return the masterContact
     */
    public String getMasterContact() {
        return masterContact;
    }

    /**
     * @param masterContact the masterContact to set
     */
    public void setMasterContact(String masterContact) {
        this.masterContact = masterContact;
    }

    /**
     * @return the tarifaCutIn
     */
    public String getTarifaCutIn() {
        return tarifaCutIn;
    }

    /**
     * @param tarifaCutIn the tarifaCutIn to set
     */
    public void setTarifaCutIn(String tarifaCutIn) {
        this.tarifaCutIn = tarifaCutIn;
    }

    /**
     * @return the totalSpots
     */
    public String getTotalSpots() {
        return totalSpots;
    }

    /**
     * @param totalSpots the totalSpots to set
     */
    public void setTotalSpots(String totalSpots) {
        this.totalSpots = totalSpots;
    }
   
    /**
     * @return the procesarPorLinea
     */
    public String getProcesarPorLinea() {
        return procesarPorLinea;
    }

    /**
     * @param procesarPorLinea the procesarPorLinea to set
     */
    public void setProcesarPorLinea(String procesarPorLinea) {
        this.procesarPorLinea = procesarPorLinea;
    }

    /**
     * @return the garantizado
     */
    public String getGarantizado() {
        return garantizado;
    }

    /**
     * @param garantizado the garantizado to set
     */
    public void setGarantizado(String garantizado) {
        this.garantizado = garantizado;
    }

    /**
     * @return the nomEmailResponsable
     */
    public String getNomEmailResponsable() {
        return nomEmailResponsable;
    }

    /**
     * @param nomEmailResponsable the nomEmailResponsable to set
     */
    public void setNomEmailResponsable(String nomEmailResponsable) {
        this.nomEmailResponsable = nomEmailResponsable;
    }

    /**
     * @return the referenciaFolio
     */
    public String getReferenciaFolio() {
        return referenciaFolio;
    }

    /**
     * @param referenciaFolio the referenciaFolio to set
     */
    public void setReferenciaFolio(String referenciaFolio) {
        this.referenciaFolio = referenciaFolio;
    }

    /**
     * @return the totalOrdenSinDesc
     */
    public String getTotalOrdenSinDesc() {
        return totalOrdenSinDesc;
    }

    /**
     * @param totalOrdenSinDesc the totalOrdenSinDesc to set
     */
    public void setTotalOrdenSinDesc(String totalOrdenSinDesc) {
        this.totalOrdenSinDesc = totalOrdenSinDesc;
    }

    /**
     * @return the totalOrdenConDesc
     */
    public String getTotalOrdenConDesc() {
        return totalOrdenConDesc;
    }

    /**
     * @param totalOrdenConDesc the totalOrdenConDesc to set
     */
    public void setTotalOrdenConDesc(String totalOrdenConDesc) {
        this.totalOrdenConDesc = totalOrdenConDesc;
    }

    /**
     * @return the idEmpresa
     */
    public String getIdEmpresa() {
        return idEmpresa;
    }

    /**
     * @param idEmpresa the idEmpresa to set
     */
    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    /**
     * @return the idPlan
     */
    public String getIdPlan() {
        return idPlan;
    }

    /**
     * @param idPlan the idPlan to set
     */
    public void setIdPlan(String idPlan) {
        this.idPlan = idPlan;
    }

    /**
     * @return the idOrden
     */
    public String getIdOrden() {
        return idOrden;
    }

    /**
     * @param idOrden the idOrden to set
     */
    public void setIdOrden(String idOrden) {
        this.idOrden = idOrden;
    }

    /**
     * @return the contratoMaestro
     */
    public String getContratoMaestro() {
        return contratoMaestro;
    }

    /**
     * @param contratoMaestro the contratoMaestro to set
     */
    public void setContratoMaestro(String contratoMaestro) {
        this.contratoMaestro = contratoMaestro;
    }

    /**
     * @return the folio
     */
    public String getFolio() {
        return folio;
    }

    /**
     * @param folio the folio to set
     */
    public void setFolio(String folio) {
        this.folio = folio;
    }

    /**
     * @return the plataforma
     */
    public String getPlataforma() {
        return plataforma;
    }

    /**
     * @param plataforma the plataforma to set
     */
    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }
    
	
	
}