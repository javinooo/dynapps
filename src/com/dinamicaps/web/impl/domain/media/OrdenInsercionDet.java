package com.dinamicaps.web.impl.domain.media;

public class OrdenInsercionDet {
	
	int iId;
	int iIdOrdenInsercion;	
	String sCveEstatus;
	
	public int getIdOrdenInsercion() {
		return iIdOrdenInsercion;
	}
	public void setIdOrdenInsercion(int i) {
		this.iIdOrdenInsercion = i;
	}
	public int getId() {
		return iId;
	}
	public void setId(int iId) {
		this.iId = iId;
	}
	public String getCveEstatus() {
		return sCveEstatus;
	}
	public void setCveEstatus(String sCveEstatus) {
		this.sCveEstatus = sCveEstatus;
	}
}