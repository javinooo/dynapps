package com.dinamicaps.web.impl.domain;

public class Usuario {

	public int iIdEmpresa;
	public int iIdPersona;
	public int iIdRol;
	public String sCveUsuario = "";
	public String sNomPersona = "";
	public String sSitPersona = "";
	public String sSitUsuario = "";
	public String sPasswordValido = "";
	public boolean bCambioPassword;
	public String sUserSchema = "";
	//public String sMenu = "";
	public String sCveMasterTemplate = "";
	public String sCveMasterTemplateVersion = "";
	public String sSettings = "";
	public String sGenero = "";
	public String sAvatar = "";
	
}
