package com.dinamicaps.web.impl;

import java.sql.*;
import java.io.*;
import java.util.*;

public class  LoadExcelIntoTable{
	
    public static void main(String args[])  throws Exception
    {
       //String filename = args[0];
       LoadExcelIntoTable obj = new LoadExcelIntoTable();
       //obj.processFile(filename);
    }
    
    public void processFile(String filename, String sTableName) throws Exception
    {
    	ServicioDAO dao 		= new ServicioDAO();
       // open file
       FileReader fr = new FileReader(filename);
       BufferedReader br = new BufferedReader(fr);

       // connect to Oracle
       dao.pstmt = dao.conn.prepareStatement("INSERT INTO " + sTableName + " values(?,?)");
       String line, columns[];
       // headings line. Ignore it
       line = br.readLine();
       Vector v;
       while( (line = br.readLine())!= null)
       {
	   v = getColumns(line);
	   Enumeration e = v.elements();
           int i=1;
           while ( e.hasMoreElements())
           {
        	   dao.pstmt.setString(i, e.nextElement().toString());
             i ++;
           }
           // insert into table after values for parameters are set
           dao.pstmt.executeUpdate();
       }
       fr.close();
       dao.pstmt.close();
       dao.conn.close();
    }
    
    public  Vector getColumns(String line)
    {
      Vector v = new Vector();
      StringBuffer bf = new StringBuffer();
      boolean  instring = false;  // indicates whether we are in string
      for ( int i = 0 ; i < line.length(); i ++)
      {
	      if ( line.charAt(i) == ',')
	      {
		  if (! instring )  // if not already in string.
		  {
		   v.add(new String(bf));  // add buffer to vector
		   bf = new StringBuffer();  // reset buffer
  	          } // end of if
	          else
		          bf.append(",");  // add comma also to string as we are in string
	      } // end of if
	      else
	      if ( line.charAt(i) == '"')  // toggle instring flag when " encountered
	      {
	        if ( instring )
	              instring = false;
    		else
		          instring = true;
              }
              else
                 bf.append( line.charAt(i));
      } // end of for
      v.add(new String(bf));  // add value at the end as column
      return v;
  }
}