package com.dinamicaps.web.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.Types;

import java.sql.CallableStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import oracle.sql.BLOB;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.dinamicaps.web.impl.domain.*;
import com.dinamicaps.web.impl.domain.optics.MatrizResultadoConf;



import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.MalformedJsonException;


import com.dinamicaps.web.impl.domain.vstream.EnviaCorreo;


public class ServicioDAO extends ServicioConexion{
	
	public String sGetValSticker(String sQuery, LinkedList<String> lVars, boolean bDebug)throws SQLException{
		
		String sValue = "";
		
		sSentenciaSql = sQuery ;

		pstmt = conn.prepareStatement(sSentenciaSql);
		
		int i = 0;
		
		if(lVars!= null ){
			
			i = i + 1;
			
			Iterator <String>itera = lVars.iterator();
			while(itera.hasNext()){
				
			}
			
		}
		

		if(bDebug){
			System.out.println("  QUERY sGetValSticker Obtiene query EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n"
					);
		}
		
		
		
		rs = pstmt.executeQuery();

		if(rs.next()){
			sValue	= rs.getString("QUERY_OPCIONES");
		}

		rs.close();
	    pstmt.close();
 
		return sValue;
		
	}
	
	public ComboConf cGetComboConf(int iIdEmpresa, int iIdListado, String sParentKey, boolean bDebug) throws SQLException{

		ComboConf config = new ComboConf();

		sSentenciaSql = 
				" 	SELECT  REPLACE(REPLACE(REPLACE(REPLACE(QUERY_OPCIONES, '@ESQUEMA@', SYS_CONTEXT('USERENV','CURRENT_SCHEMA')), \n " +
				"			'@DBLINK@', ''), '@PARENT_KEY@', ?), '@ID_EMPRESA@', ?) AS QUERY_OPCIONES, 	\n " +
				"			NVL(B_VALIDA_ETIQUETA,'F') AS B_VALIDA_ETIQUETA, QUERY_JAVA_VARS, 			\n " +
				"			NVL(B_EXEC_TO_GET_QUERY,'F') AS B_EXEC_TO_GET_QUERY,   						\n " +
				"			CASE WHEN QUERY_OPCIONES IS NULL AND B_REGENERA_OPCIONES = 'F' THEN 		\n " +
				"				OPCIONES ELSE NULL END AS OPCIONES    									\n " +
                "	FROM    AOS_ARQ_CAT_LISTADO_VALORES  	\n " +
                "	WHERE   ID_EMPRESA  = ?  	\n " +
                "    	AND ID_LISTADO  = ? 	\n " +
                "    	AND SIT_LISTADO = 'AC'  \n " ;

		if(bDebug){
			System.out.println("  QUERY cGetComboConf Obtiene query EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
				" sParentKey: 	" + sParentKey 	+ "\n" +
				" iIdEmpresa: 	" + iIdEmpresa 	+ "\n" +
				" iIdListado: 	" + iIdListado 	+ "\n");
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		
		pstmt.setString(1, sParentKey);
		pstmt.setInt(2, iIdEmpresa);
		pstmt.setInt(3, iIdEmpresa);
		pstmt.setInt(4, iIdListado);
		
		rs = pstmt.executeQuery();

		if(rs.next()){
			config.sQuery 		= rs.getString("QUERY_OPCIONES");
			config.sVars		= rs.getString("QUERY_JAVA_VARS");
			config.bStickers	= rs.getString("B_VALIDA_ETIQUETA").equals("V");
			config.bExecToQry	= rs.getString("B_EXEC_TO_GET_QUERY").equals("V");
		}

		rs.close();
	    pstmt.close();
 
		return config;
	}
	
	public LinkedHashMap<String, String> getComboOptionsMap(int iIdEmpresa, int iIdListado, String sParentKey, 
			ComboConf config, StringTokenizer tokens, LinkedHashMap<String, String> map, boolean bDebug) throws SQLException{

		LinkedHashMap <String, String> opciones = new LinkedHashMap<String, String>();
		String sQuery 	= config.sQuery;

		sSentenciaSql = sQuery ;

		if(bDebug){
			System.out.println("  QUERY getComboOptionsMap nuevo Obtiene query EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
				" sParentKey: 	" + sParentKey 	+ "\n" +
				" iIdEmpresa: 	" + iIdEmpresa 	+ "\n" +
				" iIdListado: 	" + iIdListado 	+ "\n");
		}
		

		if(sQuery != null && !sQuery.equals("")){

			sSentenciaSql = sQuery ;

			if(bDebug){
				System.out.println("  QUERY resultado nuevo getComboOptionsMap EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
			}
			
			pstmt = conn.prepareStatement(sSentenciaSql);
			//pstmt.setInt(1, iIdEmpresa);
			//pstmt.setString(2, sParentKey);
			
			int i = 0;
			if(tokens!=null){
				if(bDebug){
					System.out.println("  tokens size EN DAO ----->>>>>>>>>>>>>>>>>>> " + tokens.countTokens());
				}
				String sParameter ="";
				while(tokens.hasMoreTokens()){
					i = i + 1;
					sParameter = tokens.nextToken();
					pstmt.setString(i, (String)map.get(sParameter));
					if(bDebug){
						System.out.println("  Agrega el parametro en el stmt: " + sParameter);
					}	
				}
			}
			
			rs = pstmt.executeQuery();
			
			if(bDebug){
				System.out.println(" config.bExecToQry ----->>>>>>>>>>>>>>>>>>>  " + config.bExecToQry);
			}
			
			if(config.bExecToQry){
				if(bDebug){
					System.out.println("  QUERY resultado getComboOptionsMap EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
				}
			
				if(rs.next()){
					String sQueryFinal = rs.getString(1);
					if(bDebug){
						System.out.println(" sQueryFinal: ******************* \n" + sQueryFinal);
					}	
					pstmt = conn.prepareStatement(sQueryFinal);
					rs = pstmt.executeQuery();
				}else{
					if(bDebug){
						System.out.println(" No obtuvo regitros s ----->>>>>>>>>>>>>>>>>>>  ");
					}
				}
			}
			
			while(rs.next()){
				opciones.put(rs.getString("CLAVE"), rs.getString("VALOR"));
			}
		
			
			rs.close();
		    pstmt.close();

		}else{
			rs.close();
		    pstmt.close();
		}
 
		return opciones;
	}

	
	public LinkedHashMap<String, String> getComboOptionsMapBK(int iIdEmpresa, int iIdListado, String sParentKey, 
			ComboConf config, StringTokenizer tokens, LinkedHashMap<String, String> map, boolean bDebug) throws SQLException{

		LinkedHashMap <String, String> opciones = new LinkedHashMap<String, String>();
		String sQuery 	= "";

		sSentenciaSql = 
				" 	SELECT  REPLACE(REPLACE(REPLACE(REPLACE(QUERY_OPCIONES, '@ESQUEMA@', SYS_CONTEXT('USERENV','CURRENT_SCHEMA')), " +
				"			'@DBLINK@', ''), '@PARENT_KEY@', ?), '@ID_EMPRESA@', ?) AS QUERY_OPCIONES  \n " +
                "	FROM    AOS_ARQ_CAT_LISTADO_VALORES  	\n " +
                "	WHERE   ID_EMPRESA  = ?  	\n " +
                "    	AND ID_LISTADO  = ? 	\n " +
                "    	AND SIT_LISTADO = 'AC'  \n " ;

		if(bDebug){
			System.out.println("  QUERY getComboOptionsMap Obtiene query EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
				" sParentKey: 	" + sParentKey 	+ "\n" +
				" iIdEmpresa: 	" + iIdEmpresa 	+ "\n" +
				" iIdListado: 	" + iIdListado 	+ "\n");
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		
		pstmt.setString(1, sParentKey);
		pstmt.setInt(2, iIdEmpresa);
		pstmt.setInt(3, iIdEmpresa);
		pstmt.setInt(4, iIdListado);
		
		rs = pstmt.executeQuery();

		if(rs.next()){
			sQuery 	= rs.getString("QUERY_OPCIONES");
		}

		if(sQuery != null && !sQuery.equals("")){

			sSentenciaSql = sQuery ;

			if(bDebug){
				System.out.println("  QUERY resultado getComboOptionsMap EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
			}
			
			pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1, iIdEmpresa);
			pstmt.setString(2, sParentKey);
			
			rs = pstmt.executeQuery();
			while(rs.next()){
				opciones.put(rs.getString("CLAVE"), rs.getString("VALOR"));
			}

			rs.close();
		    pstmt.close();

		}else{
			rs.close();
		    pstmt.close();
		}
 
		return opciones;
	}
	
	public Usuario getUsuario(String sCveUsuario, String sPassword, String sEvento, boolean bDebug) throws SQLException{

		Usuario usuario 		= null; 
		DatabaseMetaData mtdata = conn.getMetaData();
		String sDBType 			= mtdata.getDriverName();
		String sDBProduct 		= mtdata.getDatabaseProductName();
		System.out.println("  sDBType ----->>>>>>>>>>>>>>>>>>>  " + sDBType);
		System.out.println("  sDBProduct ----->>>>>>>>>>>>>>>>>>>  " + sDBProduct);
		System.out.println("  conn.getSchema() ----->>>>>>>>>>>>>>>>>>>  " + conn.getCatalog());
		System.out.println("  mtdata.getSchemaTerm() ----->>>>>>>>>>>>>>>>>>>  " + mtdata.getSchemaTerm());
		
		
		if(sEvento==null){

			if(sDBProduct.equals("MySQL")){
				
				sSentenciaSql = " 	SELECT  U.ID_EMPRESA, U.CVE_USUARIO, P.NOM_PERSONA, U.ID_PERSONA, E.CVE_MASTER_TEMPLATE, E.CVE_MASTER_TEMPLATE_VERSION, \n " +
                		//"			PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.NOM_CORTO_USUARIO('''||U.CVE_USUARIO||'''); END;','" + sCveUsuario + "') AS PREFERENCIAS, \n "  +
                		"			'F' AS PREFERENCIAS, \n "  +
        			    "           U.SIT_USUARIO, P.SIT_PERSONA, U.ID_ROL, \n " +
					    //" 			CASE WHEN PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_ENCRIPTA_PASSWORD(''" + sPassword + "''); END;','" + sCveUsuario + "') = U.PASSWORD THEN \n " +
					    " 			CASE WHEN PASSWORD('" + sPassword + "') = U.PASSWORD THEN \n " +
                        "				'V' ELSE 'F' END AS PASSWORD_VALIDO, -- B_DEBUG_SESSION, \n " +
                        "			CASE WHEN F_PROX_CAMB_PASS <= DATE(now()) THEN 'V' ELSE 'F' END AS B_CAMBIO_PASSWORD, \n " +
                        //"			PKG_DYNAMIC_BASIC.AOS_GET_CURRENT_SCHEMA  AS USER_SCHEMA, " +
                        "			NULL AS USER_SCHEMA, " +
                        "           NVL(p.SEXO,'H') AS GENERO, U.AVATAR  \n " +
					    "   FROM    AOS_SECU_USUARIO U \n " +
					    "        JOIN CAT_PERSONA P \n " +
					    "           ON U.ID_EMPRESA = P.ID_EMPRESA \n " +
					    "           AND U.ID_PERSONA= P.ID_PERSONA \n " +
					    "			AND P.SIT_PERSONA = 'AC' \n " +
					    "        JOIN AOS_ARQ_EMPRESA E \n " +
					    "           ON U.ID_EMPRESA = E.ID_EMPRESA \n " +
					    " 	WHERE 	U.CVE_USUARIO	= '" + sCveUsuario + "'  \n " +
					    "       AND U.SIT_USUARIO   = 'AC' \n " ;
			}else{
				sSentenciaSql = " 	SELECT  U.ID_EMPRESA, U.CVE_USUARIO, P.NOM_PERSONA, U.ID_PERSONA, E.CVE_MASTER_TEMPLATE, E.CVE_MASTER_TEMPLATE_VERSION, \n " +
		                		//"			PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.NOM_CORTO_USUARIO('''||U.CVE_USUARIO||'''); END;','" + sCveUsuario + "') AS PREFERENCIAS, \n "  +
		                		"			'F' AS PREFERENCIAS, \n "  +
		        			    "           U.SIT_USUARIO, P.SIT_PERSONA, U.ID_ROL, \n " +
							    //" 			CASE WHEN PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_ENCRIPTA_PASSWORD(''" + sPassword + "''); END;','" + sCveUsuario + "') = U.PASSWORD THEN \n " +
							    " 			CASE WHEN CONTROL.PKG_CRYPTO.ENCRYPT('" + sPassword + "') = U.PASSWORD THEN \n " +
		                        "				'V' ELSE 'F' END AS PASSWORD_VALIDO, -- B_DEBUG_SESSION, \n " +
		                        "			CASE WHEN F_PROX_CAMB_PASS <= TRUNC(SYSDATE) THEN 'V' ELSE 'F' END AS B_CAMBIO_PASSWORD, \n " +
		                        //"			PKG_DYNAMIC_BASIC.AOS_GET_CURRENT_SCHEMA  AS USER_SCHEMA, " +
		                        "			sys_context( 'userenv', 'current_schema' ) AS USER_SCHEMA, " +
		                        "           NVL(p.SEXO,'H') AS GENERO, U.AVATAR  \n " +
							    "   FROM    AOS_SECU_USUARIO U \n " +
							    "        JOIN CAT_PERSONA P \n " +
							    "           ON U.ID_EMPRESA = P.ID_EMPRESA \n " +
							    "           AND U.ID_PERSONA= P.ID_PERSONA \n " +
							    "			AND P.SIT_PERSONA = 'AC' \n " +
							    "        JOIN AOS_ARQ_EMPRESA E \n " +
							    "           ON U.ID_EMPRESA = E.ID_EMPRESA \n " +
							    " 	WHERE 	U.CVE_USUARIO	= '" + sCveUsuario + "'  \n " +
							    "       AND U.SIT_USUARIO   = 'AC' \n " ;
			}
		}else{
			
				sSentenciaSql = " 	SELECT  U.ID_EMPRESA, TRIM(TO_CHAR(U.ID_USUARIO)) AS CVE_USUARIO, U.NOM_USUARIO AS NOM_PERSONA, U.ID_USUARIO AS ID_PERSONA,  \n " +
							"			'ACE' AS CVE_MASTER_TEMPLATE, 'V1_3_4' AS CVE_MASTER_TEMPLATE_VERSION, 'F' AS PREFERENCIAS,  \n " +
							"       	U.SIT_USUARIO, U.SIT_USUARIO AS SIT_PERSONA, 5 AS ID_ROL, CASE WHEN '" + sPassword + "'  = U.PASSWORD THEN   \n " +
							"           'V' ELSE 'F' END AS PASSWORD_VALIDO, 'F' AS B_CAMBIO_PASSWORD,  \n " +
							"        	'VSTREAM' AS USER_SCHEMA, NVL(U.GENERO,'H') AS GENERO, NULL AS AVATAR    \n " +
							"	FROM    USUARIO_EVENTO U  \n " +
							"		JOIN AOS_ARQ_EMPRESA E \n " +
						    "           ON U.ID_EMPRESA = E.ID_EMPRESA \n " +
							"	WHERE 	U.ID_EMPRESA	= 1 \n " +
							"   	AND U.ID_USUARIO    = " + sCveUsuario + " \n " +
							"    	AND U.SIT_USUARIO   = 'AC'  \n " ;
		}
		
		
		// if(bDebug){
			System.out.println("  QUERY getUsuario EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
		//}

		if(sEvento==null || StringUtils.isNumeric(sCveUsuario)){
				
			System.out.println("  Entgra a ejecutar la sentencdia  \n");
			ejecutaSentencia();
	
			if(rs.next()){
				System.out.println("  Si encuentra el registro  ----->>>>>>>>>>>>>>>>>>>  ");
				usuario = new Usuario();
				usuario.iIdEmpresa 					= rs.getInt("ID_EMPRESA");
				usuario.iIdPersona					= rs.getInt("ID_PERSONA");
				usuario.iIdRol						= rs.getInt("ID_ROL"); 
				usuario.sCveUsuario					= rs.getString("CVE_USUARIO");
				usuario.sNomPersona					= rs.getString("NOM_PERSONA");
				usuario.sSitUsuario					= rs.getString("SIT_USUARIO");
				usuario.sSitPersona					= rs.getString("SIT_PERSONA");
				usuario.sPasswordValido				= rs.getString("PASSWORD_VALIDO");
				usuario.bCambioPassword				= rs.getString("B_CAMBIO_PASSWORD").equals("V") ? true : false;
				if(sDBProduct.equals("Oracle")){
					usuario.sUserSchema				= rs.getString("USER_SCHEMA");  
				}else{
					usuario.sUserSchema				= conn.getCatalog();
				}
				usuario.sCveMasterTemplate			= rs.getString("CVE_MASTER_TEMPLATE");
				usuario.sCveMasterTemplateVersion	= rs.getString("CVE_MASTER_TEMPLATE_VERSION");
				usuario.sSettings					= rs.getString("PREFERENCIAS");
				usuario.sGenero						= rs.getString("GENERO");
				usuario.sAvatar						= rs.getString("AVATAR");
				
				//if(bDebug){
					System.out.println("  sUserSchema  DAO ----->>>>>>>>>>>>>>>>>>>  " + usuario.sUserSchema);
	
				//}
			}
		}

		if(usuario != null && usuario.sPasswordValido.equals("F")){
			sSentenciaSql = 
					" 	UPDATE 	AOS_SECU_USUARIO SET \n " +
				    "        	NUM_LOG_INVALIDO = NVL(NUM_LOG_INVALIDO,0) + 1  \n " +
				    " 	WHERE 	CVE_USUARIO	= ?  \n " ;

			//if(bDebug){
				System.out.println("  QUERY DE ACTUALIZACION EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
			//}

			pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setString(1, sCveUsuario);
			rs = pstmt.executeQuery();

			rs.close();
		    pstmt.close();

		}

		if(usuario != null && usuario.sPasswordValido.equals("V")){
			sSentenciaSql = 
					" 	UPDATE 	AOS_SECU_USUARIO SET \n " +
				    "        	NUM_LOG_INVALIDO = 0  \n " +
				    " 	WHERE 	CVE_USUARIO	= ?  \n " ;

			//if(bDebug){
				System.out.println("  QUERY DE ACTUALIZACION EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
			//}

			pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setString(1, sCveUsuario);
			rs = pstmt.executeQuery();

			rs.close();
		    pstmt.close();

		}

		return usuario;
	}
	
	

	public int getMensajeError(int iIdLogerror, boolean bDebug) throws SQLException{
		
		int iIdMensaje = -1;
		
		sSentenciaSql = " 	SELECT  NVL(ID_MENSAJE,1) AS ID_MENSAJE  \n "  +
					    "   FROM    AOS_ARQ_LOG_SISTEMA  \n " +
					    " 	WHERE 	ID_LOG	= ? ";

		if(bDebug){
			System.out.println("  QUERY getMensajeError EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
				" iIdLogerror: 	" + iIdLogerror 	+ "\n");
		}

		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdLogerror);
		rs = pstmt.executeQuery();

		if(rs.next()){
			iIdMensaje	= rs.getInt("ID_MENSAJE");
		}

		rs.close();
	    pstmt.close();
		return iIdMensaje;
	}
	
	
	public Archivo getArchivoBytes(int idFile, String sCveUsuario, boolean bDebug) throws SQLException {

		Archivo archivo = new Archivo();

		if(bDebug){
			System.out.println("  ID FILE ---->>>>>>>>>>>>>>>>>>> " + idFile);
		}
	    sSentenciaSql = " 	SELECT  NOM_ARCHIVO, TIPO_CONTENIDO, \n " +
	    				"			PKG_INTERFACE.AOS_DAME_ARCHIVO(ID_ARCHIVO) AS ARCHIVO \n " +
	    				//"			PKG_INTERFACE.AOS_INTFZ_RETURN_BLOB('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_DAME_ARCHIVO(" + idFile + "); END;','" + sCveUsuario + "') AS ARCHIVO \n " +
				        "   FROM    AOS_ARQ_ARCHIVO \n " +
				        "   WHERE   ID_ARCHIVO = ? \n" ;
	    
	    if(bDebug){
	    	System.out.println("  sSentenciaSql getArchivoBytes ---->>>>>>>>>>>>>>>>>>> \n" + sSentenciaSql);
	    }

		  pstmt = conn.prepareStatement(sSentenciaSql);
		  if(bDebug){
			  System.out.println("  compila el query " );
		  }
	      pstmt.setInt(1, idFile);
	      if(bDebug){
	    	  System.out.println("  antes de ejecutar " );
	      }
	      rs = pstmt.executeQuery();
	      if(bDebug){
	    	  System.out.println("  EJECUTA EL QUERY  " );
	      }
	      
	      if(rs.next()){
	    	  if(bDebug){
	    		  System.out.println("  TIENE DATOS  " );
	    	  }
	    	  
	    	  archivo.sNomArchivo = rs.getString("NOM_ARCHIVO");
	    	  if(bDebug){
	    		  System.out.println("  OBTIENE EL NOMBRE  " );
	    	  }
		      archivo.blob =  (BLOB)rs.getBlob("ARCHIVO");
	    	  if(bDebug){
	    		  System.out.println("  OBTIENE EL BLOB  " );
	    	  }
	    	  
	    	  if(archivo.blob!=null){
			      // materialize BLOB onto client
			      if(bDebug){
			    	  System.out.println("  (int) blob.length()  " + (int) archivo.blob.length());
			      }
			      
			      archivo.bDatos = archivo.blob.getBytes(1, (int) archivo.blob.length());
			      System.out.println("  archivo.bDatos longitud " + archivo.bDatos.length);
			      if(bDebug){
			    	  archivo.sTipoContenido = rs.getString("TIPO_CONTENIDO");
			    	  System.out.println("  TIPO_CONTENIDO " + rs.getString("TIPO_CONTENIDO"));
			      }
			  }else{
				  archivo = null;
			  }
	      }
	      rs.close();
	      pstmt.close();

	      System.out.println("  Termina de obtener el blob");
	      return archivo;
	  }

	
	public LinkedList<Detalle> getListaDetalle(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sAtributoMaestro, boolean bDebug) throws SQLException{
		
		LinkedList<Detalle> listadetalle = new LinkedList<Detalle>();

		sSentenciaSql = 	
		
				"	SELECT  DISTINCT ID_TABLA_MAESTRO, ID_TABLA_DETALLE, 	\n " +
				"			ATRIBUTO_MAESTRO, ATRIBUTO_DETALLE				\n " +
				"	FROM    MASTER_DETAIL_V	\n " +
				"	WHERE   ID_EMPRESA    		= ?	\n " +
				"	    AND ID_PANTALLA   		= ?	\n " +
				"	    AND ID_TABLA_MAESTRO  	= ? \n " +
				"	    AND ATRIBUTO_MAESTRO	= ? \n " +
				"	ORDER BY ID_TABLA_DETALLE		\n ";
		
		if(bDebug){
			System.out.println("  QUERY getListaDetalle EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdEmpresa: 		" + iIdEmpresa 			+ "\n" +
					" iIdPantalla: 		" + iIdPantalla 		+ "\n" +
					" iIdTabla: 		" + iIdTabla 			+ "\n" +
					" sAtributoMaestro: " + sAtributoMaestro 	+ "\n"); 
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setInt(2, iIdPantalla);
		pstmt.setInt(3, iIdTabla);
		pstmt.setString(4, sAtributoMaestro);

		rs = pstmt.executeQuery();

		Detalle detalle = new Detalle();
		while( rs.next() ){
			detalle = new Detalle();
			detalle.iDetIdTablaMaestro  = rs.getInt("ID_TABLA_MAESTRO");
			detalle.iDetIdTablaDetalle	= rs.getInt("ID_TABLA_DETALLE");
			detalle.sDetAtributoMaestro = rs.getString("ATRIBUTO_MAESTRO");
			detalle.sDetAtributoDetalle = rs.getString("ATRIBUTO_DETALLE");
			listadetalle.add(detalle);						
		}
		rs.close();
	    pstmt.close();
		return listadetalle;
	}
	

	public Ejecutable getEjecutable(int iIdEmpresa, int iIdEjecutable, boolean bDebug) throws SQLException{
		
		if(iIdEjecutable < 0){
			sSentenciaSql = " 	SELECT   ID_EJECUTABLE, CONTROL.AOS_CTL_LOG_SISTEMA_SQ.NEXTVAL AS ID_LOG_ERROR, NVL(B_GUARDA_DATOS_ERROR,'F') AS B_GUARDA_DATOS_ERROR, \n " +
					"			 TEMPLATE_GUARDA_DATOS_BASE AS TEMPLATE, NVL(B_REMPLAZA_ETIQ_COMBO_ENTIDAD, 'F') AS B_REMPLAZA_ETIQ_COMBO_ENTIDAD,  \n " +
					"			 NVL(B_REMPLAZA_ETIQ_COMBO_ATRIBS, 'F') AS B_REMPLAZA_ETIQ_COMBO_ATRIBS, FUNC_JS_BOTON_AGREGA_ATRIB,  \n " +
					"			 ETIQUETA_DETALLE, NVL(ID_LISTADO_RMPZA_DETALLE,0) AS ID_LISTADO_RMPZA_DETALLE,  \n " +
					" 			 NVL(ID_LISTADO_RMPZA_ENTIDAD,0) AS ID_LISTADO_RMPZA_ENTIDAD,  \n "+
					" 			 NVL(B_ATRIB_DEPEND_ENTIDAD,'F') AS B_ATRIB_DEPEND_ENTIDAD, \n " +
					"			 NVL(B_OBTIENE_ELEMENTOS,'F') AS B_OBTIENE_ELEMENTOS, NVL(B_BOTON_REGRESAR,'V') AS B_BOTON_REGRESAR,   \n" +
					"			 NVL(B_OBLIGATORIO_COMBO_DET,'F') AS B_OBLIGATORIO_COMBO_DET, NOM_EJECUCION  \n" +
					"    FROM    AOS_CTL_MTDAT_OBJ_EXE  \n " +
					"    WHERE   ID_EJECUTABLE = ?  \n" ;
		}else{
			
		
		sSentenciaSql = " 	SELECT   ID_EJECUTABLE, CONTROL.AOS_CTL_LOG_SISTEMA_SQ.NEXTVAL AS ID_LOG_ERROR, NVL(B_GUARDA_DATOS_ERROR,'F') AS B_GUARDA_DATOS_ERROR, \n " +
						"			 TEMPLATE_GUARDA_DATOS_BASE AS TEMPLATE, NVL(B_REMPLAZA_ETIQ_COMBO_ENTIDAD, 'F') AS B_REMPLAZA_ETIQ_COMBO_ENTIDAD,  \n " +
						"			 NVL(B_REMPLAZA_ETIQ_COMBO_ATRIBS, 'F') AS B_REMPLAZA_ETIQ_COMBO_ATRIBS, FUNC_JS_BOTON_AGREGA_ATRIB,  \n " +
						"			 ETIQUETA_DETALLE, NVL(ID_LISTADO_RMPZA_DETALLE,0) AS ID_LISTADO_RMPZA_DETALLE,  \n " +
						" 			 NVL(ID_LISTADO_RMPZA_ENTIDAD,0) AS ID_LISTADO_RMPZA_ENTIDAD,  \n "+
						" 			 NVL(B_ATRIB_DEPEND_ENTIDAD,'F') AS B_ATRIB_DEPEND_ENTIDAD,  \n " +
						"			 NVL(B_OBTIENE_ELEMENTOS,'F') AS B_OBTIENE_ELEMENTOS, NVL(B_BOTON_REGRESAR,'V') AS B_BOTON_REGRESAR,   \n" +
						"			 NVL(B_OBLIGATORIO_COMBO_DET,'F') AS B_OBLIGATORIO_COMBO_DET, NOM_EJECUCION  \n" +
						"    FROM    AOS_ARQ_MTDAT_OBJETO_EXE  \n " +
						"    WHERE   ID_EMPRESA    = ? \n " +
						"        AND ID_EJECUTABLE = ?  \n" ;
		}

		if(bDebug){
			System.out.println("  QUERY getPermisosTabla EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdEmpresa: 		" + iIdEmpresa 			+ "\n" +
					" iIdEjecutable: 	" + iIdEjecutable 		+ "\n" ); 
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		
		if(iIdEjecutable < 0){
			pstmt.setInt(1, iIdEjecutable);
		}else{
			pstmt.setInt(1, iIdEmpresa);
			pstmt.setInt(2, iIdEjecutable);
		}
		
		rs = pstmt.executeQuery();

		Ejecutable ejecutable = null;
		
		if(rs.next() ){
			ejecutable = new Ejecutable();
			ejecutable.iIdEmpresa			= iIdEmpresa;
			ejecutable.iIdEjecutable		= rs.getInt("ID_EJECUTABLE");
			ejecutable.iIdLogError			= rs.getInt("ID_LOG_ERROR");
			ejecutable.iListadoRmpzaDetalle	= rs.getInt("ID_LISTADO_RMPZA_DETALLE");
			ejecutable.iListadoRmpzaEntidad = rs.getInt("ID_LISTADO_RMPZA_ENTIDAD");
			ejecutable.sBGuardaLogErrorHtml	= rs.getString("B_GUARDA_DATOS_ERROR");
			ejecutable.sTemplateGuardaDatos = rs.getString("TEMPLATE");
			ejecutable.sBRemplazaComboEnt	= rs.getString("B_REMPLAZA_ETIQ_COMBO_ENTIDAD");
			ejecutable.sBRemplazaComboAts	= rs.getString("B_REMPLAZA_ETIQ_COMBO_ATRIBS");
			ejecutable.sFuncJsAgregaAtributo= rs.getString("FUNC_JS_BOTON_AGREGA_ATRIB");
			ejecutable.sEtiquetaRmpzaDetalle= rs.getString("ETIQUETA_DETALLE");
			ejecutable.sBObtieneElementos	= rs.getString("B_OBTIENE_ELEMENTOS");
			ejecutable.sBBotonRegresar		= rs.getString("B_BOTON_REGRESAR");
			ejecutable.sBAtribDependEntidad = rs.getString("B_ATRIB_DEPEND_ENTIDAD");
			ejecutable.sBObligatorioComboDet= rs.getString("B_OBLIGATORIO_COMBO_DET");
			ejecutable.sTituloExecutable	= rs.getString("NOM_EJECUCION");
			
		}
		rs.close();
	    pstmt.close();
		return ejecutable;
	}
	

	public LinkedList<ElementRow> getElementRow(int iIdEmpresa, int iIdEjecutable, boolean bDebug) throws SQLException{
		
		LinkedList<ElementRow> lista = new LinkedList<ElementRow>();
		
		sSentenciaSql = " 	SELECT  ID_EJECUTABLE, ELEMENTO, TIPO_ELEMENTO, ID_ORDEN   	\n " +
						"   FROM    AOS_CTL_ELEMENTOS_ATRIBUTO 					\n " +
						"   WHERE   ID_EJECUTABLE = ? 		\n" +
						"       AND SIT_ELEMENTO  = 'AC' 	\n" +
						"   ORDER BY ID_ORDEN" ;
		
		if(bDebug){
			System.out.println("  QUERY getElementRow EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdEjecutable: 	" + iIdEjecutable 		+ "\n" ); 
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEjecutable);
		rs = pstmt.executeQuery();

		ElementRow elementRow = null;
		
		while(rs.next() ){
			elementRow = new ElementRow();
			elementRow.iIdEjecutable	= rs.getInt("ID_EJECUTABLE");
			elementRow.sElemento		= rs.getString("ELEMENTO");
			elementRow.sTipoElemento	= rs.getString("TIPO_ELEMENTO");
			elementRow.iIdOrden 		= rs.getInt("ID_ORDEN");
			lista.add(elementRow);
		}
		rs.close();
	    pstmt.close();
		return lista;
	}
	
/*	public LinkedList<Detalle> getListaAtribMD(int iIdEmpresa, int iIdTablaMaestro, int iIdTablaDetalle, boolean bDebug) throws SQLException{
		 
		LinkedList<Detalle> listadetalle = new LinkedList<Detalle>();
		
		sSentenciaSql = " 	SELECT  ATRIBUTO_MAESTRO, ATRIBUTO_DETALLE \n " +
						"   FROM    AOS_ARQ_MAESTRO_DETALLE \n" +
				        "   WHERE   ID_EMPRESA      = " + iIdEmpresa + " \n" +
				        "       AND ID_TABLA_MAESTRO= " + iIdTablaMaestro + " \n" +
				        "       AND ID_TABLA_DETALLE= " + iIdTablaDetalle + " \n" +
				        "       AND SIT_RELACION    = 'AC'  \n" +
				        "   ORDER BY ID_ORDEN  \n" ;
		
		if(bDebug){
			System.out.println("  QUERY getListaAtribMD EN DAO  ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
		}
		ejecutaSentencia();

		Detalle detalle = new Detalle();
		while( rs.next() ){
			detalle = new Detalle();
			detalle.sDetAtributoMaestro = rs.getString("ATRIBUTO_MAESTRO");
			detalle.sDetAtributoDetalle = rs.getString("ATRIBUTO_DETALLE");
			listadetalle.add(detalle);						
		}
		return listadetalle;
	}
	*/
	
	public LinkedList<Detalle> getDistinctDetalle(int iIdEmpresa, int iIdPantalla, int iIdTabla, int iIdRol, String sListExclude, boolean bDebug) throws SQLException{
		
		LinkedList<Detalle> listadetalle = new LinkedList<Detalle>();

		sSentenciaSql = 
				"	SELECT 	DISTINCT D.ID_TABLA_DETALLE, " +
				"			CASE NVL(TB.B_REGENERATE_TABLE_RS, 'V') " +
				"				WHEN 'N' THEN NULL     	\n " +
				"				WHEN 'F' THEN  			\n " +
				"                    CASE WHEN T.JQUERY_TABLA IS NOT NULL THEN T.JQUERY_TABLA ELSE 							\n " +
				"                        PKG_DYNAMIC_BASIC.AOS_GENERA_JQUERY_TABLA(D.ID_TABLA_DETALLE,?,TB.CVE_MASTER_TEMPLATE, TB.CVE_MASTER_TEMPLATE_VERSION) END     			\n " +
				"				ELSE PKG_DYNAMIC_BASIC.AOS_GENERA_JQUERY_TABLA(D.ID_TABLA_DETALLE,?,TB.CVE_MASTER_TEMPLATE, TB.CVE_MASTER_TEMPLATE_VERSION) END AS JQUERY_TABLA 	\n" +
				"   FROM 	MASTER_DETAIL_V D						\n" +
				"   	LEFT JOIN AOS_ARQ_PANTALLA_TABLA TB		\n" +
				" 			ON  D.ID_TABLA_DETALLE	= TB.ID_TABLA		\n" +
				"   	LEFT JOIN AOS_ARQ_PANTALLA_TABLA_RS T		\n" +
				" 			ON  D.ID_TABLA_DETALLE	= T.ID_TABLA		\n" +
				" 			AND T.ID_ROL			= ?		\n" +
				" 	WHERE 	D.ID_EMPRESA    	= ? \n" +
				" 	    AND D.ID_PANTALLA   	= ?	\n" +
				" 	    AND D.ID_TABLA_MAESTRO	= ?	\n" +
				"       AND (? IS NULL OR NOT REGEXP_LIKE(TRIM(TO_CHAR(D.ID_TABLA_DETALLE)),?)) \n" +
				"	ORDER BY D.ID_TABLA_DETALLE		\n";

		if(bDebug){
			System.out.println("  QUERY getDistinctDetalle EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdRol: 		" + iIdRol 		+ "\n" +
					" iIdEmpresa: 	" + iIdEmpresa 	+ "\n" +
					" iIdPantalla: 	" + iIdPantalla + "\n" +
					" iIdTabla: 	" + iIdTabla 	+ "\n");
		}

		pstmt = conn.prepareStatement(sSentenciaSql);

		pstmt.setInt(1, iIdRol);
		pstmt.setInt(2, iIdRol);
		pstmt.setInt(3, iIdRol);
		pstmt.setInt(4, iIdEmpresa);
		pstmt.setInt(5, iIdPantalla);
		pstmt.setInt(6, iIdTabla);
		pstmt.setString(7, sListExclude);
		pstmt.setString(8, sListExclude);
		

		rs = pstmt.executeQuery();

		Detalle detalle = new Detalle();
		while( rs.next() ){
			detalle = new Detalle();
			detalle.iDetIdTablaDetalle	= rs.getInt("ID_TABLA_DETALLE");
			detalle.sJsTableTransform	= rs.getString("JQUERY_TABLA");
			listadetalle.add(detalle);						
		}
		rs.close();
		pstmt.close();

		return listadetalle;
	}
	
	public LinkedList<Atributo> getListaAtributosTabla(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sTipo, int iIdRol, String sTipoOperacion, boolean bDebug) throws SQLException{
		
		LinkedList<Atributo> atributos = new LinkedList<Atributo>();
		
		sSentenciaSql = "SELECT  A.ATRIBUTO, A.TIPO_DATO, A.ORDEN_PK, A.TIPO_TEMPLATE, NVL(A.CVE_MULTI_INSERT,'N') AS CVE_MULTI_INSERT, \n" +
				 		"		NVL(T.B_MULTI_INSERT,'F') AS B_MULTI_INSERT, A.SEQUENCE_AUTO_INCREMENTA  \n" +
				        "FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A \n" +
				        "    JOIN AOS_ARQ_PANTALLA_TABLA T  \n" +
				        "    	ON T.ID_EMPRESA = A.ID_EMPRESA  \n" +
				        "    	AND T.ID_PANTALLA = A.ID_PANTALLA  \n" +
				        "    	AND T.ID_TABLA = A.ID_TABLA  \n" +
				        "	LEFT JOIN AOS_SECU_ROL_PANT_TABLA_EX_CAM EC  \n" +
                        "       ON  EC.ID_ATTRIBUTE     = A.ID_ATRIBUTO  \n" +
                        "       AND EC.ID_ROL           = ?  	\n" +
				        "WHERE   A.ID_EMPRESA      	= ? 		\n" +
				        " 	 AND A.ID_PANTALLA  	= ? 		\n" +
				        "    AND A.ID_TABLA        	= ?			\n" +
				        "    AND A.TIPO_ATRIBUTO   	= ?			\n" +
				        "    AND A.SIT_ATRIBUTO    	= 'AC' 		\n" +	
				        "    AND NVL(A.ID_ROL,?) 	= ? 	 	\n" +
				        "    AND (A.TIPO_ATRIBUTO  	= 'FILTRO' OR NVL(T.B_MULTI_INSERT,'F') = 'F' OR NVL(A.CVE_MULTI_INSERT,'N') <> 'N')  \n" +
				        "    AND NVL(A.IS_DISPLAY_FOREIGN_DATA,'F') <> 'V' 					\n " +
				        "	 AND NOT(EC.ID_ATTRIBUTE IS NOT NULL AND ( 						\n " +
                        "    			(? = 'M' AND NVL(EC.B_ALTER,'F') = 'V')		\n " +
                        " 			OR  (? = 'A' AND NVL(EC.B_CREATE,'F') = 'V') )	\n " +
                        "			)														\n " + 
				        "ORDER BY NVL(A.CVE_MULTI_INSERT,'N') DESC, A.ID_ORDEN_ATRIBUTO " ;

		if(bDebug){
			System.out.println("  QUERY getListaAtributosTabla EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdEmpresa: 		" + iIdEmpresa 		+ "\n" +
					" iIdPantalla: 		" + iIdPantalla 	+ "\n" +
					" iIdTabla: 		" + iIdTabla 		+ "\n" +
					" iIdRol: 			" + iIdRol 			+ "\n" +
					" sTipoOperacion: 	" + sTipoOperacion 	+ "\n" +
					" sTipo: 			" + sTipo 			+ "\n");
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		
		pstmt.setInt(1, iIdRol);
		pstmt.setInt(2, iIdEmpresa);
		pstmt.setInt(3, iIdPantalla);
		pstmt.setInt(4, iIdTabla);
		pstmt.setString(5, sTipo);
		pstmt.setInt(6, iIdRol);
		pstmt.setInt(7, iIdRol);
		pstmt.setString(8, sTipoOperacion);
		pstmt.setString(9, sTipoOperacion);

		rs = pstmt.executeQuery();
		
		Atributo atributo = null;
		while( rs.next() ){
			atributo = new Atributo();
			atributo.sNomAtributo 		= rs.getString("ATRIBUTO");
			atributo.sTipoDato 			= rs.getString("TIPO_DATO");
			atributo.sIdPk 				= rs.getString("ORDEN_PK");
			atributo.sTipoTemplate 		= rs.getString("TIPO_TEMPLATE");
			atributo.sCveMultiInsert	= rs.getString("CVE_MULTI_INSERT");
			atributo.bIsMultiInsert 	= rs.getString("B_MULTI_INSERT").equals("V");
			atributo.sSequenceAutoInc	= rs.getString("SEQUENCE_AUTO_INCREMENTA");
			atributos.add(atributo);						
		}

		rs.close();
		pstmt.close();
		return atributos;
	}
	
	
	
	public LinkedList<Atributo> getListaParamExec(int iIdEmpresa, int iIdEjecutable, boolean bDebug) throws SQLException{
		
		LinkedList<Atributo> atributos = new LinkedList<Atributo>();
		
		if(iIdEjecutable < 0){
			sSentenciaSql = "SELECT  D.NOM_PARAMETRO, D.TIPO_DATO_PARAMETRO, T.TIPO_TEMPLATE, D.ID_DETALLE \n" +
					        "FROM    AOS_CTL_MTDAT_OBJ_EXE_DET D\n" +
					        "	 JOIN AOS_CTL_TEMPLATE_HTML T \n" +
					        "		ON D.TEMPLATE = T.TEMPLATE \n" +
					        "		AND D.CVE_MASTER_TEMPLATE = T.CVE_MASTER_TEMPLATE \n" +
					        "		AND D.CVE_MASTER_TEMPLATE_VERSION = T.CVE_MASTER_TEMPLATE_VERSION \n" +
					        "		AND T.SIT_TEMPLATE = 'AC' \n" +
					        "WHERE   D.ID_EJECUTABLE	= ? \n" +
					        "    AND D.SIT_PARAMETRO 	= 'AC' \n" +
					        "ORDER BY D.ID_ORDEN " ;
		}else{
			sSentenciaSql = "SELECT  D.NOM_PARAMETRO, D.TIPO_DATO_PARAMETRO, T.TIPO_TEMPLATE, D.ID_DETALLE \n" +
					        "FROM    AOS_ARQ_MTDAT_OBJETO_EXE_DET D\n" +
					        "	 JOIN AOS_CTL_TEMPLATE_HTML T \n" +
					        "		ON D.TEMPLATE = T.TEMPLATE \n" +
					        "		AND D.CVE_MASTER_TEMPLATE = T.CVE_MASTER_TEMPLATE \n" +
					        "		AND D.CVE_MASTER_TEMPLATE_VERSION = T.CVE_MASTER_TEMPLATE_VERSION \n" +
					        "		AND T.SIT_TEMPLATE = 'AC' \n" +
					        "WHERE   D.ID_EMPRESA		= ? \n" +
					        "    AND D.ID_EJECUTABLE	= ? \n" +
					        "    AND D.SIT_PARAMETRO 	= 'AC' \n" +
					        "ORDER BY D.ID_ORDEN " ;
		}

		if(bDebug){
			System.out.println("  QUERY getListaParamExec EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdEmpresa: 		" + iIdEmpresa 			+ "\n" +
					" iIdEjecutable: 	" + iIdEjecutable 		+ "\n" ); 
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		
		if(iIdEjecutable < 0){
			pstmt.setInt(1, iIdEjecutable);
		}else{
			pstmt.setInt(1, iIdEmpresa);
			pstmt.setInt(2, iIdEjecutable);
		}
		
		rs = pstmt.executeQuery();


		Atributo atributo = new Atributo();
		while( rs.next() ){
			atributo = new Atributo();
			atributo.sNomAtributo 	= rs.getString("NOM_PARAMETRO");
			atributo.sTipoDato 		= rs.getString("TIPO_DATO_PARAMETRO");
			atributo.sTipoTemplate	= rs.getString("TIPO_TEMPLATE");
			atributo.iIdDetalle		= rs.getInt("ID_DETALLE");
			atributos.add(atributo);						
		}

		rs.close();
		pstmt.close();
		return atributos;
	}
	

	public String altaModDatos(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sDatos, String sTipoOperacion, String sCveUsuario, int iIdRol, boolean bDebug) throws SQLException {

		String sResultado = "";
		int iClobParam = iGuardaCLOB(iIdEmpresa, 0, 0, sCveUsuario, sDatos, "java altaModDatos", bDebug);
		
		String sSentencia = 
					  "	  DECLARE \n"
					+ "		vlParamVarchar1    VARCHAR2(4000); \n "       
					+ "		vlParamVarchar2    VARCHAR2(4000); \n "
                    + "		vlParamNumber1     NUMBER; \n "  
                    + "		vlParamNumber2     NUMBER; \n "
                    + "		vlParamDate1       DATE; \n "      
                    + "		vlParamDate2       DATE; \n "
                    + "		vlParamClob1       CLOB; \n "      
                    + "		vlParamClob2       CLOB; \n\n "
					+ "   BEGIN \n "
                    + "			PKG_DYNAMIC_BASIC.AOS_ALTA_MOD_DATOS(" + iIdEmpresa + ", " + iIdPantalla +  ", " + iIdTabla + ", " + iClobParam + ", '" + 
                    			sTipoOperacion + "', '" + sCveUsuario + "', " + iIdRol + ", vlParamVarchar1); \n " 
					+ "		:psParamVarchar1     := vlParamVarchar1; \n " 
					+ "		:psParamVarchar2     := NULL; \n " 
	                + "		:psParamNumber1      := NULL; \n " 
	                + "		:psParamNumber2      := NULL; \n " 
	                + "		:psParamDate1        := NULL; \n "     
	                + "		:psParamDate2        := NULL; \n " 
	                + "		:psParamClob1        := NULL; \n "     
	                + "		:psParamClob2        := NULL; \n " 
	                + "		DELETE AOS_TMP_CLOB WHERE ID = " + iClobParam + "; \n " 
	                + "	 END; \n ";

		//CallableStatement callst = conn.prepareCall("{ call PKG_INTERFACE.AOS_ALTA_MOD_DATOS(?,?,?,?,?)}");
		CallableStatement callst = conn.prepareCall("{ call PKG_INTERFACE.AOS_INTFZ_EXEC_PROCEDURE(?,?,?,?,?,?,?,?,?,?,?)}");

		callst.setInt(1,iIdEmpresa);
		callst.setString(2,sCveUsuario);
    	callst.setString(3,sSentencia);

    	callst.registerOutParameter(4, java.sql.Types.VARCHAR);
    	callst.registerOutParameter(5, java.sql.Types.VARCHAR);
    	callst.registerOutParameter(6, java.sql.Types.NUMERIC);
    	callst.registerOutParameter(7, java.sql.Types.NUMERIC);
    	callst.registerOutParameter(8, java.sql.Types.DATE);
    	callst.registerOutParameter(9, java.sql.Types.DATE);
    	callst.registerOutParameter(10, java.sql.Types.CLOB);
    	callst.registerOutParameter(11, java.sql.Types.CLOB);

    	if(bDebug){
    		System.out.println("Antes Actualizar datos EN DAO altaModDatos ");
    		System.out.println(sSentencia);
    	}
    	callst.execute(); //  ------->>>>> ERROR Occur
    	if(bDebug){
    		System.out.println("Despues actualizar datos EN DAO altaModDatos");
    	}
		
	    sResultado = callst.getString(4);

	    return sResultado;
	}

	public String sDameAtributosFK(int iIdEmpresa, String sTablaDestino, String sCveUsuario, boolean bDebug) throws SQLException {
		
		String sResultado = "";
		
		CallableStatement callst = conn.prepareCall(
				"{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_CLOB('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_HTML_DAME_ATRIB_TABLA_FK(" + iIdEmpresa + 
				",''" + sTablaDestino + "''); END;','" + sCveUsuario + "')}");
		callst.registerOutParameter(1, java.sql.Types.CLOB);
    	
    	if(bDebug){
    		System.out.println("Antes o datos EN DAO sDameAtributosFK \n ");
    	}
    	callst.execute(); //  ------->>>>> ERROR Occur
    	if(bDebug){
    		System.out.println("Despues actualizar datos EN DAO sDameAtributosFK");
    	}
    	
    	sResultado = callst.getString(1);
    	return sResultado;
	}
	

	public int iGuardaCLOB(int iIdEmpresa, int iIdEjecutable, int iIdPantalla, String sCveUsuario, String sCLOB, String sOrigen, boolean bDebug) throws SQLException {
		
		int iIdResultado= 0;

		CallableStatement callst = conn.prepareCall("{ ? = call PKG_INTERFACE.AOS_GUARDA_CLOB_PARAM(?,?,?,?,?,?)}");
		callst.registerOutParameter(1, java.sql.Types.INTEGER);
		callst.setString(2,sCLOB);
		callst.setString(3,sCveUsuario);
		callst.setInt(4,iIdPantalla);
		callst.setInt(5,iIdEjecutable);
		callst.setInt(6,iIdEmpresa);
		callst.setString(7,sOrigen);
		
		
    	if(bDebug){
    		System.out.println("Despues actualizar datos EN DAO iGuardaCLOB");
    	}
    	callst.execute(); //  ------->>>>> ERROR Occur
    	if(bDebug){
    		System.out.println("Despues actualizar datos EN DAO iGuardaCLOB");
    	}

    	iIdResultado = callst.getInt(1);
    	return iIdResultado;
	}
	
	public void borraCLOB(int iId, boolean bDebug) throws SQLException {

		sSentenciaSql = " 	DELETE AOS_TMP_CLOB WHERE ID = ? ";
		
		if(bDebug){
			System.out.println("  QUERY borraCLOB EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iId: 		" + iId + "\n"  ); 
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iId);
		rs = pstmt.executeQuery();

    	if(bDebug){
    		System.out.println("Despues DE BORRADO ICLOB ");
    	}
		rs.close();
		pstmt.close();
	}
	

	public int iIdLogErrorGuardaDatosExec(int iIdEmpresa, String sDatos, String sCveUsuario, boolean bDebug) throws SQLException {

		int iIdResultado= 0;
		int iClobParam = iGuardaCLOB(iIdEmpresa, 0, 0, sCveUsuario, sDatos, "java iIdLogErrorGuardaDatosExec", bDebug);
		
		CallableStatement callst = conn.prepareCall(
				"{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_NUMBER('BEGIN :result := PKG_DYNAMIC_DB.AOS_GUARDA_DATOS_EXEC(" + iIdEmpresa + 
					"," + iClobParam + "); END;','" + sCveUsuario + "')}");

    	callst.registerOutParameter(1, java.sql.Types.NUMERIC);


    	if(bDebug){
    		System.out.println("Antes guardar datos EN DAO iIdLogErrorGuardaDatosExec ");
    	}
    	callst.execute(); //  ------->>>>> ERROR Occur
    	if(bDebug){
    		System.out.println("Despues de guardar datos EN DAO iIdLogErrorGuardaDatosExec ");
    	}
		
    	iIdResultado = callst.getInt(1);
    	borraCLOB(iClobParam, bDebug);
	    return iIdResultado;
	}
	
	public String sDameUrlDirectorio(String sDirectorio, boolean bDebug) throws SQLException {
		
		String sResultado = "";
		
		sSentenciaSql =	" SELECT  DIRECTORY_PATH  \n" +
						" FROM    ALL_DIRECTORIES  \n" +
						" WHERE   DIRECTORY_NAME = ? ";
	
		if(bDebug){
			System.out.println("  QUERY sDameUrlDirectorio EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" sDirectorio: 		" + sDirectorio + "\n"  ); 
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setString(1, sDirectorio);
		rs = pstmt.executeQuery();

		if( rs.next() ){
			sResultado	= rs.getString("DIRECTORY_PATH");
		}
		rs.close();
		pstmt.close();

		return sResultado;
	}

	public String sGetErrorTemplate(int iIdEmpresa, int iIdPantalla, boolean bDebug) throws SQLException {
		
		String sTemplateHtml = "";
		
		sSentenciaSql =	
						" 	-- QUERY TO GET ERROR TEMPLATE							\n " +
						" 	SELECT  T.HTML_TEMPLATE									\n " + 
			            "   FROM    AOS_CTL_TEMPLATE_HTML T					\n " +
			            "       JOIN AOS_ARQ_PANTALLA P						\n " +
			            "           ON P.ID_EMPRESA 	= ?							\n " +
			            "           AND P.ID_PANTALLA 	= ? 						\n " +
			            "   WHERE   T.CVE_MASTER_TEMPLATE = P.CVE_MASTER_TEMPLATE 	\n " +
			            "       AND T.CVE_MASTER_TEMPLATE_VERSION = CVE_MASTER_TEMPLATE_VERSION		\n " +
			            "       AND T.TIPO_TEMPLATE = 'ERROR_MESSAGE'				\n " +
			            "		AND T.B_DEFAULT 	= 'V' 							\n " +
			            " 		AND ROWNUM = 1 ";
	
		if(bDebug){
			System.out.println("  QUERY sGetErrorTemplate EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdEmpresa: 		" + iIdEmpresa + "\n"  +
					" iIdPantalla: 		" + iIdPantalla + "\n"); 
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setInt(2, iIdPantalla);
		rs = pstmt.executeQuery();

		if( rs.next() ){
			sTemplateHtml	= rs.getString("HTML_TEMPLATE");
		}
		rs.close();
		pstmt.close();
		
		return sTemplateHtml;
	}
	
	
	public String ejecutaProceso(int iIdEmpresa, int iIdTabla, int iIdEjecutable, String sDatos, String sCveUsuario, int iIdLogError, boolean bDebug) throws SQLException {
		
		String sResultado = "";

		//iGuardaCLOB(int iIdEmpresa, int iIdEjecutable, String sPantalla, String sCveUsuario, String sCLOB, boolean bDebug)
		int iClobParam = iGuardaCLOB(iIdEmpresa, iIdEjecutable, 0, sCveUsuario, sDatos, "java ejecutaProceso", bDebug);
		
		String sStatement = "{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_EXEC.AOS_EJECUTA_PROCESO(" + iIdEmpresa + ", " + iIdTabla +
				", " + iIdEjecutable +", " + iClobParam + ", ''" + sCveUsuario + "'', " + iIdLogError + "); END;','" + sCveUsuario + "')}"; 
		
		CallableStatement callst = conn.prepareCall(sStatement);

    	callst.registerOutParameter(1, java.sql.Types.CLOB);

    	if(bDebug){
    		System.out.println("Antes de ejecuci�n EN DAO EjecutaProceso \n" + sStatement);
    	}
	    callst.execute(); //  ------->>>>> ERROR Occur
	    if(bDebug){
	    	System.out.println("Despues ejecuci�n EN DAO EjecutaProceso");
	    }
		
	    sResultado = callst.getString(1);
	    return sResultado;
	}


	public String sDameValorParametro(int iIdEmpresa, String sSistema, String sParametro, String sCveUsuario, boolean bDebug) throws SQLException {

		String sResultado = "";

		//CallableStatement callst = conn.prepareCall("{ ? = call PKG_INTERFACE.AOS_PARAMETRO_VAL(?,?,?)}");
		CallableStatement callst = conn.prepareCall(
				"{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_PARAMETRO_VAL(" + iIdEmpresa + ", ''" + sSistema +
					"'', ''" + sParametro +"''); END;','" + sCveUsuario + "')}");
		callst.registerOutParameter(1, java.sql.Types.VARCHAR);

    	if(bDebug){
    		System.out.println("Antes de ejecuci�n EN DAO AOS_PARAMETRO_VAL ");
    	}
	    callst.execute(); //  ------->>>>> ERROR Occur
	    if(bDebug){
	    	System.out.println("Despues ejecuci�n EN DAO AOS_PARAMETRO_VAL");
	    }

	    sResultado = callst.getString(1);
	    return sResultado;
	}

	public String sDameCuerpoMail(int iIdEmpresa, String sCveUsuario, String sParamCuerpoMail, boolean bDebug) throws SQLException {

		String sResultado = "";

		//CallableStatement callst = conn.prepareCall("{ ? = call PKG_INTERFACE.AOS_CUERPO_EMAIL(?,?,?)}");
		CallableStatement callst = conn.prepareCall(
				"{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_CUERPO_EMAIL(" + iIdEmpresa + ", ''" + sCveUsuario +
					"'', ''" + sParamCuerpoMail +"''); END;','" + sCveUsuario + "')}");
		callst.registerOutParameter(1, java.sql.Types.VARCHAR);

    	if(bDebug){
    		System.out.println("Antes de ejecuci�n EN DAO sDameCuerpoMail ");
    	}
	    callst.execute(); //  ------->>>>> ERROR Occur
	    if(bDebug){
	    	System.out.println("Despues ejecuci�n EN DAO sDameCuerpoMail");
	    }

	    sResultado = callst.getString(1);
	    return sResultado;
	}

	public String sDameJsComboDependFiltro(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sTipoAtributo, String sCveUsuario, boolean bDebug) throws SQLException {
  
		String sResultado = "";

		CallableStatement callst = conn.prepareCall(
				"{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_DAME_ONCHANGE_FILTRO(" + iIdEmpresa + ", " + iIdPantalla +
					", " + iIdTabla +", ''" + sTipoAtributo + "''); END;','" + sCveUsuario + "')}");
		callst.registerOutParameter(1, java.sql.Types.VARCHAR);

    	if(bDebug){
    		System.out.println("Antes de ejecuci�n EN DAO sDameCuerpoMail ");
    	}
	    callst.execute(); //  ------->>>>> ERROR Occur
	    if(bDebug){
	    	System.out.println("Despues ejecuci�n EN DAO sDameCuerpoMail");
	    }

	    sResultado = callst.getString(1);
	    return sResultado;
	}
	
	public NavegacionNext getNavegacionNext(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sCveUsuario, boolean bDebug) throws SQLException {
		
		NavegacionNext navegacionNext = new NavegacionNext(); 
		
		//CallableStatement callst = conn.prepareCall("{ call PKG_INTERFACE.AOS_GET_NAVEGACION(?,?,?,?)}");
		CallableStatement callst = conn.prepareCall(
				"{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_NUMBER('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_GET_NAVEGACION(" + iIdEmpresa + ", " + iIdPantalla +
					", " + iIdTabla +"); END;','" + sCveUsuario + "')}");

		callst.registerOutParameter(1, java.sql.Types.NUMERIC);

		if(bDebug){
			System.out.println("Antes de consultar getNavegacionNext EN DAO");
		}
		callst.execute(); //  ------->>>>> ERROR Occur
		if(bDebug){
			System.out.println("Despues consultar getNavegacionNext EN DAO");
		}

		navegacionNext.iIdTabla	= callst.getInt(1);
		return navegacionNext;
	}
	
	public String sGetRemplazoEtiqueta(int iIdEmpresa, String sSentencia, int iIdDatos, String sCveUsuario, boolean bDebug) throws SQLException{
		
		String sRemplazo = "";
        Date date = null;

		String sStmt = "{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_CLOB('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_GET_HTML_REMPLAZO(''" + 
				sSentencia.replace("'", "''").replace("'", "''") + "'', " + iIdDatos + "); END;','" + sCveUsuario + "')}";

		CallableStatement callst = conn.prepareCall(sStmt);
		callst.registerOutParameter(1, java.sql.Types.CLOB);

    	if(bDebug){
    		date = new Date();
            System.out.println("fecha de sistema: " + date.toString());
    		System.out.println("Antes de ejecutar EN DAO sGetRemplazoEtiqueta " + iIdDatos + " \n" + sStmt);
    	}

    	callst.execute(); //  ------->>>>> ERROR Occur

    	if(bDebug){
    		date = new Date();
        	System.out.println("fecha de sistema: " + date.toString());
        	System.out.println("Despues de ejecutar EN DAO");
	    }

	    sRemplazo = callst.getString(1);
	    if(bDebug){
	    	date = new Date();
	        System.out.println("fecha de sistema: " + date.toString());
	    	System.out.println("sRemplazo ------------------------------------------------------------- \n" + sRemplazo);
	    	System.out.println("sRemplazo ------------------------------------------------------------- \n");
	    }

	    return sRemplazo;
	}

	public String sGetValidationForm(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sTipoForm, String sCveTemplateMaster, int iIdEjecutable, String sCveUsuario, boolean bDebug) throws SQLException{
		
		String sValidationForm = "";

		String sStmt = "{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_CLOB('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_GET_VALIDATION_FORM(" +  iIdEmpresa + 
				", " + iIdPantalla + ", " + iIdTabla + ", ''" + sTipoForm + "'', ''" + sCveTemplateMaster + "'', " + iIdEjecutable + "); END;','" + sCveUsuario + "')}";

		CallableStatement callst = conn.prepareCall(sStmt);

		callst.registerOutParameter(1, java.sql.Types.CLOB);
	      
    	if(bDebug){
    		System.out.println("Antes de ejecutar EN DAO sGetValidationForm \n" + sStmt);
    	}
	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de ejecutar EN DAO");
	      }

	      sValidationForm = callst.getString(1);
	      if(bDebug){
	    	  System.out.println("sRemplazo ------------------------------------------------------------- \n" + sValidationForm);
	    	  System.out.println("sRemplazo ------------------------------------------------------------- \n");
	      }
	      
	      return sValidationForm;
	}
	
	public boolean bVerificaPassword(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sDatos, String sColumna, String sPassword, String sCveUsuario, boolean bDebug) throws SQLException{
		
		String sEsPasswordValido = "";
		//iGuardaCLOB(int iIdEmpresa, int iIdEjecutable, String sPantalla, String sCveUsuario, String sCLOB, boolean bDebug)
		int iClobParam = iGuardaCLOB(iIdEmpresa, 0, 0, sCveUsuario, (sDatos!=null?sDatos:""), "java bVerificaPassword", bDebug);

		//CallableStatement callst = conn.prepareCall("{ ? = call PKG_INTERFACE.AOS_VERIFICA_PASSWORD(?,?,?,?,?)}");
		CallableStatement callst = conn.prepareCall(
				"{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_VERIFICA_PASSWORD(" + iIdEmpresa + ", " + iIdPantalla + ", " + iIdTabla +
					", " + iClobParam +", ''" + sColumna + "'', ''" + sPassword + "'', ''" + sCveUsuario + "''); END;','" + sCveUsuario + "')}");
		callst.registerOutParameter(1, java.sql.Types.VARCHAR);
		//callst.setInt(2,iIdEmpresa);
		//callst.setInt(3,iIdTabla);
		//callst.setString(4,sDatos);
		//callst.setString(5,sColumna);
		//callst.setString(6,sPassword);
	      
    	if(bDebug){
    		System.out.println("Antes de ejecutar EN DAO AOS_VERIFICA_PASSWORD");
    	}
	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de ejecutar EN DAO AOS_VERIFICA_PASSWORD");
	      }

	      sEsPasswordValido = callst.getString(1);
	      
	      if(bDebug){
	    	  System.out.println("sEsPasswordValido -------------------------------------------------------------" + sEsPasswordValido);
	      }
	      
	      if(sEsPasswordValido.equals("V")){
	    	  return true;
	      }else{
	    	  return false;
	      }
	}
	
	public Navegacion getNavegacion(String sCveNavegacion, boolean bDebug) throws SQLException{

		Navegacion navegacion = new Navegacion();
		
		sSentenciaSql =	"	SELECT  CVE_NAVEGACION, \n" +
						"			NVL(B_OBTIENE_ID_TABLA,'F') AS B_OBTIENE_ID_TABLA, NVL(B_OBTIENE_PERMISOS_MAESTRO,'F') AS B_OBTIENE_PERMISOS_MAESTRO, \n" +
						"	 		NVL(B_VALIDA_OPERACION,'F') AS B_VALIDA_OPERACION, NVL(B_OBTIENE_CAMPOS_PK,'f') AS B_OBTIENE_CAMPOS_PK, \n" +
						"			NVL(B_OBTIENE_CAMPOS_ALT_MOD,'F') AS B_OBTIENE_CAMPOS_ALT_MOD, NVL(B_EJECUTA_ALT_MOD,'F') AS B_EJECUTA_ALT_MOD, \n" +
						"			NVL(B_OBTIENE_CAMPOS_EXEC,'F') AS B_OBTIENE_CAMPOS_EXEC, NVL(B_OBTIENE_CONSULTA_MAESTRO_DET,'F') AS B_OBTIENE_CONSULTA_MAESTRO_DET, \n" +
						"			NVL(B_OBTIENE_NAVEGA_NEXT,'F') AS B_OBTIENE_NAVEGA_NEXT, NVL(B_REDIRECCIONA_ID_TABLA,'F') AS B_REDIRECCIONA_ID_TABLA, \n" +
						"			NVL(B_OBTIENE_CAMPOS_FILTRO,'F') AS B_OBTIENE_CAMPOS_FILTRO, NVL(B_NAVEGA_TEMPLT_REMPLAZA,'F') AS B_NAVEGA_TEMPLT_REMPLAZA, \n" +
						"			NVL(B_EJECUTA_EXECUTABLE,'F') AS B_EJECUTA_EXECUTABLE, NVL(CVE_OPERACION_SUBTITULO, 'NONE') AS CVE_OPERACION_SUBTITULO, \n" +
						"           NVL(B_OBTIENE_CAMPOS_FK,'F') AS B_OBTIENE_CAMPOS_FK								\n " +
						" 	FROM    AOS_CTL_CONF_NAVEGA \n" +
						" 	WHERE   CVE_NAVEGACION = ?  			\n" +
						"     	AND SIT_NAVEGACION = 'AC'   		\n";
		
		if(bDebug){
			System.out.println("  QUERY getNavegacion EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" sCveNavegacion: 		" + sCveNavegacion + "\n"); 
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setString(1, sCveNavegacion);
		
		rs = pstmt.executeQuery();

		if( rs.next() ){
			navegacion.sCVE_NAVEGACION  				= rs.getString("CVE_NAVEGACION");
			navegacion.sB_OBTIENE_ID_TABLA  			= rs.getString("B_OBTIENE_ID_TABLA");
			navegacion.sB_OBTIENE_PERMISOS_MAESTRO  	= rs.getString("B_OBTIENE_PERMISOS_MAESTRO");
			navegacion.sB_VALIDA_OPERACION 				= rs.getString("B_VALIDA_OPERACION");
			navegacion.sB_OBTIENE_CAMPOS_PK 			= rs.getString("B_OBTIENE_CAMPOS_PK");
			navegacion.sB_OBTIENE_CAMPOS_ALT_MOD 		= rs.getString("B_OBTIENE_CAMPOS_ALT_MOD");
			navegacion.sB_EJECUTA_ALT_MOD 				= rs.getString("B_EJECUTA_ALT_MOD");
			navegacion.sB_OBTIENE_CAMPOS_EXEC 			= rs.getString("B_OBTIENE_CAMPOS_EXEC");
			navegacion.sB_OBTIENE_CONSULTA_MAESTRO_DET 	= rs.getString("B_OBTIENE_CONSULTA_MAESTRO_DET");
			navegacion.sB_OBTIENE_NAVEGA_NEXT 			= rs.getString("B_OBTIENE_NAVEGA_NEXT");
			navegacion.sB_REDIRECCIONA_ID_TABLA 		= rs.getString("B_REDIRECCIONA_ID_TABLA");
			navegacion.sB_OBTIENE_CAMPOS_FILTRO 		= rs.getString("B_OBTIENE_CAMPOS_FILTRO");
			navegacion.sB_NAVEGA_TEMPLT_REMPLAZA 		= rs.getString("B_NAVEGA_TEMPLT_REMPLAZA");
			navegacion.sB_EJECUTA_EXECUTABLE 			= rs.getString("B_EJECUTA_EXECUTABLE");
			navegacion.sCVE_OPERACION					= rs.getString("CVE_OPERACION_SUBTITULO");
			navegacion.sB_OBTIENE_CAMPOS_FK				= rs.getString("B_OBTIENE_CAMPOS_FK");
		}
		
		rs.close();
		pstmt.close();
		return navegacion;
	}
	

	public LinkedList<NavegacionTemplate> getNavegacionTemplate(int iIdEmpresa, int iIdPantalla, String sCveNavegacion, String sMT, String sMTVrs, String sEsPopUpWindow, boolean bDebug) throws SQLException{
		
		LinkedList<NavegacionTemplate> listaNavegacion = new LinkedList<NavegacionTemplate>();
		
		sSentenciaSql =" SELECT  T.HTML_TEMPLATE, R.TEMPLATE_REMPLAZA, R.TIPO_TEMPLATE, \n" +
					   "		 NVL(R.B_REMPZA_CONSULTA_DET,'F') AS B_REMPZA_CONSULTA_DET \n" +
				       " FROM    AOS_CTL_CONF_NAVEGA_RMPZA_TPL R  \n" +
				       "     JOIN AOS_CTL_TEMPLATE_HTML T  \n" +
				       "         ON  R.TIPO_TEMPLATE = T.TIPO_TEMPLATE  \n" +
				       "         AND T.SIT_TEMPLATE = 'AC' \n" +
				       "         AND R.CVE_MASTER_TEMPLATE   = T.CVE_MASTER_TEMPLATE  \n" +
				       "         AND R.CVE_MASTER_TEMPLATE_VERSION   = T.CVE_MASTER_TEMPLATE_VERSION  \n" +
				       "      JOIN AOS_ARQ_PANTALLA_TEMPLATE P  \n" +
				       "         ON  P.ID_EMPRESA 	= ? \n" +
				       "         AND P.ID_PANTALLA  = ? \n" +
				       "         AND P.CVE_MASTER_TEMPLATE   = T.CVE_MASTER_TEMPLATE  \n" +
				       "         AND P.CVE_MASTER_TEMPLATE_VERSION   = T.CVE_MASTER_TEMPLATE_VERSION  \n" +
				       "         AND P.TEMPLATE   = T.TEMPLATE  \n" +
				       " WHERE   R.CVE_NAVEGACION      = ?  	\n" +
				       "     AND R.SIT_NAVEGACION_DET  = 'AC'  	\n" +
				       "     AND R.CVE_MASTER_TEMPLATE = ? 		\n" +
				       "     AND R.CVE_MASTER_TEMPLATE_VERSION = ?	\n" +
				       "     AND (? = 'NO' OR R.B_REMPZA_EN_POPUP_WIN = 'V')  \n" +
				       " ORDER BY R.ID_ORDEN  \n";

		if(bDebug){
			System.out.println("  QUERY getNavegacionTemplate EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdEmpresa: 		" + iIdEmpresa + "\n" +  
					" iIdPantalla: 		" + iIdPantalla + "\n" +  
					" sCveNavegacion: 	" + sCveNavegacion + "\n" +
					" sEsPopUpWindow: 	" + sEsPopUpWindow + "\n");
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setInt(2, iIdPantalla);
		pstmt.setString(3, sCveNavegacion);
		pstmt.setString(4, sMT);
		pstmt.setString(5, sMTVrs);
		pstmt.setString(6, sEsPopUpWindow);
		
		rs = pstmt.executeQuery();

		NavegacionTemplate navegacionTemplate = new NavegacionTemplate();
		while( rs.next() ){
			navegacionTemplate = new NavegacionTemplate();
			
			navegacionTemplate.sHtml 					= rs.getString("HTML_TEMPLATE");
			navegacionTemplate.sEtiquetaTemplate 		= rs.getString("TEMPLATE_REMPLAZA");
			navegacionTemplate.sTipoTemplate 			= rs.getString("TIPO_TEMPLATE");
			navegacionTemplate.sRemplazaConsultaDetalle = rs.getString("B_REMPZA_CONSULTA_DET");
			
			listaNavegacion.add(navegacionTemplate);						
		}
		
		rs.close();
		pstmt.close();
		
		return listaNavegacion;
	}
	

	public LinkedList<Remplazo> getListaRemplazoEtiquetas(int iIdEmpresa, String sCveNavegacion, String sTemplate, String sMT, String sMTVrs, String sEsPopUpWindow, boolean bDebug) throws SQLException{
		
		LinkedList<Remplazo> listaRemplazos = new LinkedList<Remplazo>();
		Remplazo remplazo = new Remplazo();
		
		sSentenciaSql = "SELECT  ETIQUETA_A_REMPLAZAR, PROCESO_REMPLAZA, B_NECESITA_DATOS, B_ES_TEXTO, B_ES_JAVASCRIPT  \n" +
			            "FROM    AOS_CTL_CONF_NAVEGA_RMPZA_ET  \n" +
			            "WHERE   CVE_NAVEGACION      = ?  	\n" +
			            "    AND TEMPLATE_REMPLAZA   = ?  	\n" +
			            "    AND CVE_MASTER_TEMPLATE = ?  	\n" +
			            "    AND CVE_MASTER_TEMPLATE_VERSION = ?  	\n" +
			            "    AND SIT_ETIQUETA        = 'AC' \n" +
				        "    AND (? = 'NO' OR B_REMPZA_EN_POPUP_WIN = 'V')  \n" +
			            "ORDER BY ID_ORDEN " ;
		
		if(bDebug){
			System.out.println("  QUERY getListaRemplazoEtiquetas EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" sCveNavegacion: 	" + sCveNavegacion + "\n" +  
					" sTemplate: 		" + sTemplate + "\n" +  
					" sEsPopUpWindow: 	" + sEsPopUpWindow + "\n");
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setString(1, sCveNavegacion);
		pstmt.setString(2, sTemplate);
		pstmt.setString(3, sMT);
		pstmt.setString(4, sMTVrs);
		pstmt.setString(5, sEsPopUpWindow);

		rs = pstmt.executeQuery();
		
		
		while( rs.next() ){
			remplazo = new Remplazo();
			
			remplazo.sEtiqueta 			= rs.getString("ETIQUETA_A_REMPLAZAR");
			remplazo.sProceso 			= rs.getString("PROCESO_REMPLAZA");
			remplazo.sBNecesitaDatos	= rs.getString("B_NECESITA_DATOS");
			remplazo.sBEsTexto			= rs.getString("B_ES_TEXTO");
			remplazo.sEsJavaScript		= rs.getString("B_ES_JAVASCRIPT");
			
			listaRemplazos.add(remplazo);						
		}
		
		rs.close();
		pstmt.close();
		
		return listaRemplazos;
	}

	
	public LinkedList<Atributo> getListaAtributosPk(int iIdEmpresa, int iIdPantalla, int iIdTabla, int iIdTablaAnterior, String sGenSeq, boolean bDebug) throws SQLException{
		
		LinkedList<Atributo> atributosPk = new LinkedList<Atributo>();
		
		sSentenciaSql = 
			            " 		WITH qry AS (   \n " + 
			            "        	SELECT  PARAM_ADITIONAL_GET_REG AS SCRIPT \n " + 
			            "        	FROM    AOS_ARQ_PANTALLA_TABLA \n " + 
			            "        	WHERE   ID_EMPRESA     	= ? \n" + 
				        "				AND ID_PANTALLA		= ? \n" +
				        "			 	AND ID_TABLA		= ? \n" + 
			            "       	) 												\n" +		
			            "    	SELECT SUBSTR(REGEXP_SUBSTR (SCRIPT, '[^&]+', 1, ROWNUM),1,		 \n" +
			            "    				INSTR(REGEXP_SUBSTR (SCRIPT, '[^&]+', 1, ROWNUM),'=',1)-1) AS ATRIBUTO, 'VARCHAR2' as TIPO_DATO,  \n" +
			            "				'999' AS ORDEN_PK, 'NEEDED' AS TIPO_PARAM, NULL AS ATRIBUTO_HIJO, 0 AS SEQ_VAL, NULL AS SEQUENCE_AUTO_INCREMENTA \n" +
			            "    	FROM   qry																	 \n" +
			            "    	WHERE   LENGTH(TRIM(REGEXP_SUBSTR (SCRIPT, '[^&]+', 1, ROWNUM))) > 0		 \n" +
			            "    	CONNECT BY LEVEL <= (LENGTH(REGEXP_REPLACE (SCRIPT, '[^&]+')) + 1)			 \n" +
		
				        "       UNION  		 \n " +
				
						"     	SELECT 	DISTINCT A.ATRIBUTO, A.TIPO_DATO, TRIM(TO_CHAR(NVL(A.ORDEN_PK,0))) AS ORDEN_PK,  \n" +
						"				'PK' AS TIPO_PARAM, L.ATRIBUTO AS ATRIBUTO_HIJO, " +
						"               CASE WHEN ? = 'V' THEN FN_GET_SEQ_NEXTVAL(A.SEQUENCE_AUTO_INCREMENTA) ELSE 0 END AS SEQ_VAL, \n " +
						"               A.SEQUENCE_AUTO_INCREMENTA                    	\n" +
				        "    	FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A 					\n" +
						"             LEFT JOIN AOS_ARQ_PANTALLA_TABLA_ATRIB L 			\n" +
						"                 ON A.ID_EMPRESA = L.ID_EMPRESA	 			\n" +
						"                 AND A.ID_PANTALLA = L.ID_PANTALLA 			\n" +
						" 				  AND L.ID_TABLA    = ? 						\n" +
						"				  AND L.ID_TABLA	> 0 						\n" +
						"				  AND L.TIPO_ATRIBUTO = 'ALT_MOD'  				\n" +
						"				  AND A.ATRIBUTO	= L.PARENT_ATTRIBUTE		\n" +
						"				  AND A.ID_TABLA	= L.ID_PARENT_TABLE			\n" +
						"				  AND L.SIT_ATRIBUTO= 'AC'						\n" +
				        "		WHERE   A.ID_EMPRESA    = ? 							\n" + 
				        "			AND A.ID_PANTALLA	= ? 							\n" +
				        "			AND A.ID_TABLA		= ? 							\n" + 
				        "    		AND (A.ORDEN_PK	 	IS NOT NULL OR 					\n" +
				        "             	NVL(A.IS_NEEDED_LINK_GET_REG,'F') = 'V')		\n" +
				        "			AND A.TIPO_ATRIBUTO = 'ALT_MOD'  					\n" +
				        "    		AND NVL(A.IS_DISPLAY_FOREIGN_DATA,'F') <> 'V' 		\n" +
				        "		-- ORDER BY A.ORDEN_PK" ;
		
		if(bDebug){
			System.out.println("  QUERY getListaAtributosPk EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" idEmpresa: 		" + iIdEmpresa 			+ "\n" +  
					" iIdPantalla: 		" + iIdPantalla 		+ "\n" +  
					" iIdTabla: 		" + iIdTabla 			+ "\n" +  
					" iIdTablaAnterior: " + iIdTablaAnterior 	+ "\n");
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setInt(2, iIdPantalla);
		pstmt.setInt(3, iIdTabla);
		pstmt.setString(4, sGenSeq);
		pstmt.setInt(5, iIdTablaAnterior);
		pstmt.setInt(6, iIdEmpresa);
		pstmt.setInt(7, iIdPantalla);
		pstmt.setInt(8, iIdTabla);
		
		rs = pstmt.executeQuery();
		
		Atributo atributo = new Atributo();
		while( rs.next() ){
			atributo = new Atributo();
			atributo.sNomAtributo 		= rs.getString("ATRIBUTO");
			atributo.sTipoDato 			= rs.getString("TIPO_DATO");
			atributo.sIdPk 				= rs.getString("ORDEN_PK");
			atributo.sCveParameter		= rs.getString("TIPO_PARAM");
			atributo.sAtributoHijo		= rs.getString("ATRIBUTO_HIJO");
			atributo.iSeqVal			= rs.getInt("SEQ_VAL");
			atributo.sSequenceAutoInc	= rs.getString("SEQUENCE_AUTO_INCREMENTA");
			atributosPk.add(atributo);						
		}

		rs.close();
		pstmt.close();
		
		return atributosPk;
	}
	
	

	public LinkedList<MasterDetail> getListMDAttributes(int iIdEmpresa, int iIdPantalla, int iIdTabla, int iIdTablaAnterior, boolean bDebug) throws SQLException{
		
		LinkedList<MasterDetail> lMasterDetail = new LinkedList<MasterDetail>();

		sSentenciaSql = 
				"	SELECT  MAST.ID_EMPRESA, MAST.ID_PANTALLA, MAST.ID_TABLA_MAESTRO, MAST.ID_TABLA_DETALLE, 			\n" +
				"			NVL(DET.ATRIBUTO_DETALLE,MAST.ATRIBUTO_MAESTRO) AS ATRIBUTO_MAESTRO,  						\n" +   
			    "           MAST.ATRIBUTO_DETALLE, MAST.ID_TABLA_MAESTRO_ORIGEN, MAST.ID_TABLA_DETALLE_ORIGEN,  		\n" +
			    "			MAST.TIPO_DATO, MAST.DESCRIPTION_FIELD, MAST.IS_FK_PARENT_PAGE, MAST.CVE_ATTRIB_SEND_DETAIL	\n" +   
			    "    FROM    MASTER_DETAIL_V 	MAST						\n" +
				"	    LEFT JOIN (												\n" +
		        "                SELECT  D.ATRIBUTO_MAESTRO, D.ATRIBUTO_DETALLE \n" +
		        "                FROM    MASTER_DETAIL_V  D				\n" +
		        "                WHERE   D.ID_EMPRESA            = ?			\n" +   
		        "                    AND D.ID_PANTALLA           = ?  			\n" +
		        "                    AND D.ID_TABLA_MAESTRO      = ? 			\n" +
		        "                    AND D.ID_TABLA_DETALLE      = ?  			\n" +
		        "                    AND D.ID_TABLA_DETALLE      > 0 			\n" +
		        "                    ) DET										\n" +
		        "            ON MAST.ATRIBUTO_MAESTRO = DET.ATRIBUTO_MAESTRO 	\n" +
		        "            AND DET.ATRIBUTO_MAESTRO <> DET.ATRIBUTO_DETALLE	\n" +
			    "    WHERE   MAST.ID_EMPRESA        =   ? 						\n" +   
			    "        AND MAST.ID_PANTALLA       =   ? 						\n" +
			    "        AND MAST.ID_TABLA_MAESTRO  =   ? 						\n" +
			    "    ORDER BY MAST.ID_TABLA_DETALLE, MAST.ATRIBUTO_MAESTRO 		\n";   
		
		if(bDebug){
			System.out.println("  QUERY getListMDAttributes EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" idEmpresa: 		" + iIdEmpresa 			+ "\n" +  
					" iIdPantalla: 		" + iIdPantalla 		+ "\n" +  
					" iIdTabla: 		" + iIdTabla 			+ "\n" +  
					" iIdTablaAnterior: " + iIdTablaAnterior 	+ "\n");
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setInt(2, iIdPantalla);
		pstmt.setInt(3, iIdTabla);
		pstmt.setInt(4, iIdTablaAnterior);
		pstmt.setInt(5, iIdEmpresa);
		pstmt.setInt(6, iIdPantalla);
		pstmt.setInt(7, iIdTabla);

		
		rs = pstmt.executeQuery();
		
		MasterDetail masterDetail;
		while( rs.next() ){
			masterDetail = new MasterDetail();
			masterDetail.iIdEmpresa				= rs.getInt("ID_EMPRESA");
			masterDetail.iIdMasterTable			= rs.getInt("ID_TABLA_MAESTRO");
			masterDetail.iIdDetailTable			= rs.getInt("ID_TABLA_DETALLE");
			masterDetail.iIdMasterTableSource	= rs.getInt("ID_TABLA_MAESTRO_ORIGEN");
			masterDetail.iIdDetailTableSource	= rs.getInt("ID_TABLA_DETALLE_ORIGEN");
			masterDetail.iIdPantalla			= rs.getInt("ID_PANTALLA");
			masterDetail.sMasterAttribute 		= rs.getString("ATRIBUTO_MAESTRO");
			masterDetail.sDetailAttribute		= rs.getString("ATRIBUTO_DETALLE");
			masterDetail.sDetailDataType		= rs.getString("TIPO_DATO");
			masterDetail.sMasterDescriptionField= rs.getString("DESCRIPTION_FIELD");
			masterDetail.bIsFKParentPage		= rs.getString("IS_FK_PARENT_PAGE").equals("V");
			masterDetail.sCveParametroEnvio		= rs.getString("CVE_ATTRIB_SEND_DETAIL");
			lMasterDetail.add(masterDetail);
		}

		rs.close();
		pstmt.close();
		
		return lMasterDetail;
	}

	
	public LinkedList<Atributo> getListaAtributosFk(int iIdEmpresa, int iIdPantalla, int iIdTabla, boolean bDebug) throws SQLException{
		
		LinkedList<Atributo> atributosFk = new LinkedList<Atributo>();

		sSentenciaSql = "   SELECT  DISTINCT ATRIBUTO_DETALLE, 			\n" +
						"			TIPO_DATO, CVE_ATTRIB_SEND_DETAIL	\n" +
						"	FROM    MASTER_DETAIL_V 	\n" +
						"	WHERE   ID_EMPRESA        = ? 	\n" +
						"		AND ID_PANTALLA       = ? 	\n" +
						"		AND ID_TABLA_DETALLE  = ? 	\n";
		
		if(bDebug){
			System.out.println("  QUERY getListMDAttributes EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" idEmpresa: 		" + iIdEmpresa 			+ "\n" +  
					" iIdPantalla: 		" + iIdPantalla 		+ "\n" +  
					" iIdTabla: 		" + iIdTabla 			+ "\n");
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setInt(2, iIdPantalla);
		pstmt.setInt(3, iIdTabla);
		
		rs = pstmt.executeQuery();

		Atributo atributo = new Atributo();
		while( rs.next() ){
			atributo = new Atributo();
			atributo.sNomAtributo 	= rs.getString("ATRIBUTO_DETALLE");
			atributo.sTipoDato 		= rs.getString("TIPO_DATO");
			atributo.sCveParameter	= rs.getString("CVE_ATTRIB_SEND_DETAIL");
			atributosFk.add(atributo);						
		}

		rs.close();
		pstmt.close();

		return atributosFk;
	}

	
	public Pantalla getPantalla(int iIdEmpresa, int iIdPantalla, int iIdTabla, int iRol, String sCveOperacion, String sCveUsuario, boolean bDebug) throws SQLException{

		Pantalla pantalla = new Pantalla();

		sSentenciaSql = " 	SELECT	P.ID_PANTALLA, P.SIT_PANTALLA, P.TIPO_PANTALLA, P.CVE_MASTER_TEMPLATE, P.CVE_MASTER_TEMPLATE_VERSION,	\n " +
						"			T.ID_TABLA, T.TITULO_PANTALLA, ST.SUBTITULO_PANTALLA, TPH.HTML_TEMPLATE AS PAGE_CONTENT_HEADER,	\n " +
						"			CASE WHEN TP.B_ALTA = 'V' THEN NVL(T.SHOW_BUTTON_CREATE_FROM_RESULT,'V') ELSE 'F' END AS SHOW_BUTTON_CREATE_FROM_RESULT,	\n " + 
						"			NVL(T.SHOW_BUTTON_CLEAR, 'V') AS SHOW_BUTTON_CLEAR,												\n " +
						"			NVL(TP.B_CONSULTA,'F') AS B_CONSULTA, NVL(T.SHOW_BUTTON_ALTA_BUSQ, 'V') AS SHOW_BUTTON_ALTA_BUSQ, \n " +
						"           NVL(TP.B_ALTA,'F') AS B_ALTA, NVL(T.SHOW_BUTTON_CREATE,'V') AS SHOW_BUTTON_CREATE, 				\n " +
						"			NVL(TP.B_BAJA, 'F') AS B_BAJA, NVL(T.SHOW_BUTTON_BACK_CREATE,'V') AS SHOW_BUTTON_BACK_CREATE,	\n " +
						"           NVL(TP.B_MODIFICA, 'F') B_MODIFICA, NVL(T.SHOW_BUTTON_UPDATE,'V') AS SHOW_BUTTON_UPDATE, 				\n " +
						"           NVL(T.SHOW_BUTTON_SEARCH,'V') AS SHOW_BUTTON_SEARCH, MT.DEFAULT_SKIN,							\n " +
                        "			AOS_GET_NOTIFICATIONS_ACTIVITY(T.ID_EMPRESA, ?, NULL) AS NOTIFICATIONS,							\n " +
						" 			CASE NVL(T.B_REGENERATE_TABLE_RS, 'V') 															\n " +
						"				WHEN 'N' THEN NULL     																		\n " +
						"				WHEN 'F' THEN 																				\n " +
						"					CASE WHEN TRS.JQUERY_TABLA IS NOT NULL THEN TRS.JQUERY_TABLA ELSE 						\n " +
						"                    PKG_DYNAMIC_BASIC.AOS_GENERA_JQUERY_TABLA(T.ID_TABLA,?,T.CVE_MASTER_TEMPLATE, T.CVE_MASTER_TEMPLATE_VERSION) END     						\n " +
						"				ELSE PKG_DYNAMIC_BASIC.AOS_GENERA_JQUERY_TABLA(T.ID_TABLA,?,T.CVE_MASTER_TEMPLATE, T.CVE_MASTER_TEMPLATE_VERSION) END AS JQUERY_TABLA,			\n " +
						"           T.PAGE_SPECIFIC_PLUGIN_SCRIPTS, NVL(T.SHOW_BUTTON_BACK_UPDATE,'V') AS SHOW_BUTTON_BACK_UPDATE,	\n " +
						"           T.QUERY_INCLUDE_DETAIL, NVL(T.B_REDIRECT_ALT_TO_MOD,'F') AS B_REDIRECT_ALT_TO_MOD				\n " +
						"   FROM    AOS_ARQ_PANTALLA_TABLA T 							\n " +
						"       JOIN AOS_ARQ_PANTALLA P 								\n " +
						"           ON	P.ID_EMPRESA = T.ID_EMPRESA 					\n " +
						"           AND P.ID_PANTALLA = T.ID_PANTALLA  					\n " +
						"       LEFT JOIN AOS_ARQ_PANTALLA_TABLA_RS TRS 				\n " +
						"           ON 	T.ID_TABLA  = TRS.ID_TABLA	 					\n " +
						"           AND TRS.ID_ROL  = ?									\n " +
						"       LEFT JOIN AOS_SECU_ROL R 								\n " +
						"           ON 	R.ID_EMPRESA   = T.ID_EMPRESA	 				\n " +
						"           AND R.ID_ROL       = ?								\n " +
						"           AND R.SIT_ROL      = 'AC' 							\n " +
						"       LEFT JOIN AOS_CTL_TEMPLATE_HTML TPH 					\n " +
						"           ON  TPH.TIPO_TEMPLATE = 'PAGE_CONTENT_HEADER'		\n " +
						"           AND TPH.CVE_MASTER_TEMPLATE	= P.CVE_MASTER_TEMPLATE	\n " +
						"           AND TPH.CVE_MASTER_TEMPLATE_VERSION	= P.CVE_MASTER_TEMPLATE_VERSION	\n " +
						"           AND TPH.B_DEFAULT   = 'V' 							\n " +
						"       LEFT JOIN AOS_SECU_ROL_PANT_TABLA_PERMIS TP 			\n " +
						"           ON  TP.ID_EMPRESA   = T.ID_EMPRESA 					\n " +
						"           AND TP.ID_PANTALLA  = T.ID_PANTALLA 				\n " +
						"           AND TP.ID_TABLA     = T.ID_TABLA 					\n " +
						"           AND TP.ID_ROL       = R.ID_ROL 						\n " +
						"    	LEFT JOIN AOS_CTL_SUBTITULO_PANTALLA ST 				\n " +
						"       	ON 	ST.CVE_OPERACION = ? 							\n " +
						"    	LEFT JOIN AOS_CTL_MASTER_TEMPLATE MT 		   	 		\n " +
						"       	ON 	P.CVE_MASTER_TEMPLATE = MT.CVE_MASTER_TEMPLATE	\n " +
						"       	AND	P.CVE_MASTER_TEMPLATE_VERSION = MT.CVE_MASTER_TEMPLATE_VERSION	\n " +
						"   WHERE   T.ID_EMPRESA    = ? 								\n " +
						"       AND T.ID_PANTALLA   = ?  								\n " +
						"       AND T.ID_TABLA      = ?  								\n " ;
		
		if(bDebug){
			System.out.println("  QUERY getPantalla EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" sCveUsuario: 		" + sCveUsuario			+ "\n" +  
					" iRol: 			" + iRol 				+ "\n" +  
					" sCveOperacion: 	" + sCveOperacion 		+ "\n" +  
					" idEmpresa: 		" + iIdEmpresa 			+ "\n" +  
					" iIdPantalla: 		" + iIdPantalla 		+ "\n" +  
					" iIdTabla: 		" + iIdTabla 			+ "\n");
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setString(1, sCveUsuario);
		pstmt.setInt(2, iRol);
		pstmt.setInt(3, iRol);
		pstmt.setInt(4, iRol);
		pstmt.setInt(5, iRol);
		pstmt.setString(6, sCveOperacion);
		pstmt.setInt(7, iIdEmpresa);
		pstmt.setInt(8, iIdPantalla);
		pstmt.setInt(9, iIdTabla);
		
		rs = pstmt.executeQuery();
		
		if(rs.next()){
			pantalla.iIdPantalla 				= rs.getInt("ID_PANTALLA");		
			pantalla.sSitPantalla				= rs.getString("SIT_PANTALLA");
			pantalla.sTipoPantalla				= rs.getString("TIPO_PANTALLA");
			pantalla.sCveMasterTemplate			= rs.getString("CVE_MASTER_TEMPLATE");
			pantalla.sCveMasterTemplateVersion	= rs.getString("CVE_MASTER_TEMPLATE_VERSION");
			pantalla.sDefaultSkin				= rs.getString("DEFAULT_SKIN");
			pantalla.iIdTabla 					= rs.getInt("ID_TABLA");
			pantalla.sTituloPantalla 			= rs.getString("TITULO_PANTALLA");
			pantalla.sSubtituloPantalla 		= rs.getString("SUBTITULO_PANTALLA");
			pantalla.bBConsulta					= rs.getString("B_CONSULTA").equals("V");
			pantalla.bBModifica					= rs.getString("B_MODIFICA").equals("V");
			pantalla.bBAlta						= rs.getString("B_ALTA").equals("V");
			pantalla.bBBaja						= rs.getString("B_BAJA").equals("V");
			pantalla.sPageContentHeader			= rs.getString("PAGE_CONTENT_HEADER");
			pantalla.sNotifications				= rs.getString("NOTIFICATIONS");
			pantalla.sJQTransformRSTable		= rs.getString("JQUERY_TABLA");
			pantalla.sPageSpecificPlugins		= rs.getString("PAGE_SPECIFIC_PLUGIN_SCRIPTS");
			pantalla.sQryIncludeDetail			= rs.getString("QUERY_INCLUDE_DETAIL");
			pantalla.bBButtonBackCreate			= rs.getString("SHOW_BUTTON_BACK_CREATE").equals("V");
			pantalla.bBButtonBackUpdate			= rs.getString("SHOW_BUTTON_BACK_UPDATE").equals("V");
			pantalla.bBButtonCreateSearch		= rs.getString("SHOW_BUTTON_ALTA_BUSQ").equals("V");
			pantalla.bBButtonSearch				= rs.getString("SHOW_BUTTON_SEARCH").equals("V");
			pantalla.bBButtonCreate				= rs.getString("SHOW_BUTTON_CREATE").equals("V");
			pantalla.bBButtonUpdate				= rs.getString("SHOW_BUTTON_UPDATE").equals("V");
			pantalla.bBButtonClear				= rs.getString("SHOW_BUTTON_CLEAR").equals("V");
			pantalla.bBButtonCreateFromResult 	= rs.getString("SHOW_BUTTON_CREATE_FROM_RESULT").equals("V");
			pantalla.bBRedirectAltToMod			= rs.getString("B_REDIRECT_ALT_TO_MOD").equals("V");
			
			if(bDebug){
				System.out.println("iIdPantalla EN DAO " + rs.getString("ID_PANTALLA"));
				System.out.println("iIdPantalla EN DAO " + rs.getString("ID_PANTALLA"));
				System.out.println("bBConsulta EN DAO " + rs.getString("B_CONSULTA") + "= " +  pantalla.bBConsulta);
				System.out.println("bBModifica EN DAO " + rs.getString("B_MODIFICA") + "= " +  pantalla.bBModifica);
				System.out.println("bBAlta EN DAO " + rs.getString("B_ALTA") + "= " +  pantalla.bBAlta);
				System.out.println("bBBaja EN DAO " + rs.getString("B_BAJA") + "= " +  pantalla.bBBaja);
			}
		}else{
			if(bDebug){
				System.out.println("NO ENCONTRO EL REGISTRO EN DAO **************************************");
			}
		}

		if(bDebug){
			System.out.println(" Datos despues de getPantalla EN DAO \n" +
					" pantalla.iIdPantalla: 		" + pantalla.iIdPantalla			+ "\n");
		}
		
		rs.close();
		pstmt.close();
		
		return pantalla;

	}
	
	
	public PantallaPermiso getPantallaPermiso(int iIdEmpresa, int iIdAplicacion, int iRol, int iIdPantalla, int iIdTabla, boolean bDebug) throws SQLException{

		PantallaPermiso pantallaPermiso = new PantallaPermiso();

		sSentenciaSql = " 	SELECT	ID_EMPRESA, ID_APLICACION, ID_ROL, ID_PANTALLA, ID_TABLA, 		\n " +
						"			NVL(B_CONSULTA,'F') AS B_CONSULTA,  NVL(B_ALTA,'F') AS B_ALTA,	\n " +
						"			NVL(B_BAJA, 'F') AS B_BAJA, NVL(B_MODIFICA, 'F') AS B_MODIFICA 	\n " +
						"   FROM	AOS_SECU_ROL_PANT_TABLA_PERMIS  							\n " +
						"   WHERE  	ID_EMPRESA    	= ? 	\n " +
						"       AND ID_APLICACION   = ?  	\n " +
						"       AND ID_ROL   		= ?  	\n " +
						"       AND ID_PANTALLA   	= ?  	\n " +
						"       AND ID_TABLA      	= ?  	\n " ;
		
		if(bDebug){
			System.out.println("  QUERY getPantallaPermiso EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" idEmpresa: 		" + iIdEmpresa 			+ "\n" +  
					" iIdAplicacion: 	" + iIdAplicacion 		+ "\n" +  
					" iRol: 			" + iRol 				+ "\n" +  
					" iIdPantalla: 		" + iIdPantalla 		+ "\n" +  
					" iIdTabla: 		" + iIdTabla 			+ "\n");
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setInt(2, iIdAplicacion);
		pstmt.setInt(3, iRol);
		pstmt.setInt(4, iIdPantalla);
		pstmt.setInt(5, iIdTabla);
		
		rs = pstmt.executeQuery();
		
		if(rs.next()){
			pantallaPermiso.iIdEmpresa 				= rs.getInt("ID_EMPRESA");		
			pantallaPermiso.iIdAplicacion			= rs.getInt("ID_APLICACION");
			pantallaPermiso.iIdRol					= rs.getInt("ID_ROL");
			pantallaPermiso.iIdPantalla 			= rs.getInt("ID_PANTALLA");		
			pantallaPermiso.iIdTabla 				= rs.getInt("ID_TABLA");
			pantallaPermiso.bBConsulta				= rs.getString("B_CONSULTA").equals("V");
			pantallaPermiso.bBModifica				= rs.getString("B_MODIFICA").equals("V");
			pantallaPermiso.bBAlta					= rs.getString("B_ALTA").equals("V");
			pantallaPermiso.bBBaja					= rs.getString("B_BAJA").equals("V");
			
			if(bDebug){
				System.out.println("bBConsulta EN DAO " 	+ rs.getString("B_CONSULTA")	+ "= " + pantallaPermiso.bBConsulta);
				System.out.println("bBModifica EN DAO " 	+ rs.getString("B_MODIFICA")	+ "= " + pantallaPermiso.bBModifica);
				System.out.println("bBAlta EN DAO " 		+ rs.getString("B_ALTA") 		+ "= " + pantallaPermiso.bBAlta);
				System.out.println("bBBaja EN DAO " 		+ rs.getString("B_BAJA") 		+ "= " + pantallaPermiso.bBBaja);
			}
		}

		rs.close();
		pstmt.close();
		
		return pantallaPermiso;

	}

	public Buttons getButtons(int iIdEmpresa, String sCveUsuario, boolean bDebug) throws SQLException{
		
		Buttons buttons = new Buttons();
		
		sSentenciaSql = "SELECT  PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_PARAMETRO_VAL(" + iIdEmpresa + 
								 ", ''HTML_ATRIB_TAGS'',''BUTTON_SUBMIT_UPDATE_RECORD''); END;','" + sCveUsuario + "') AS BUTTON_UPDATE_RECORD, \n" + 
						"        PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_PARAMETRO_VAL(" + iIdEmpresa + 
						 		 ", ''HTML_ATRIB_TAGS'',''BUTTON_GO_TO_CREATE_RECORD''); END;','" + sCveUsuario + "') AS BUTTON_GO_TO_CREATE_RECORD, \n" + 
						"        PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_PARAMETRO_VAL(" + iIdEmpresa + 
				 		 		 ", ''HTML_ATRIB_TAGS'',''BUTTON_GO_CREATE_FROM_SEARCH''); END;','" + sCveUsuario + "') AS BUTTON_GO_CREATE_FROM_SEARCH, \n" + 
						"        PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_PARAMETRO_VAL(" + iIdEmpresa + 
						 		 ", ''HTML_ATRIB_TAGS'',''BUTTON_CREATE_RECORD''); END;','" + sCveUsuario + "') AS BUTTON_CREATE_RECORD, \n" + 
						"		 PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_PARAMETRO_VAL(" + iIdEmpresa + 
								 ", ''HTML_ATRIB_TAGS'',''BUTTON_DELETE_RECORD''); END;','" + sCveUsuario + "') AS BUTTON_DELETE_RECORD, \n" + 
						"		 PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_PARAMETRO_VAL(" + iIdEmpresa + 
						 		", ''HTML_ATRIB_TAGS'',''BUTTON_ADD_ATTRIBUTE''); END;','" + sCveUsuario + "') AS BUTTON_ADD_ATTRIBUTE, \n" + 
						"		 PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_PARAMETRO_VAL(" + iIdEmpresa + 
				 		"        , ''HTML_ATRIB_TAGS'',''BUTTON_SEARCH''); END;','" + sCveUsuario + "') AS BUTTON_SEARCH, \n" + 
				 		"        PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_PARAMETRO_VAL(" + iIdEmpresa + 
						"        , ''HTML_ATRIB_TAGS'',''BUTTON_CLEAR''); END;','" + sCveUsuario + "') AS BUTTON_CLEAR, \n" + 
	                    "		 PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_PARAMETRO_VAL(" + iIdEmpresa + 
								 ", ''HTML_ATRIB_TAGS'',''BUTTON_BACK''); END;','" + sCveUsuario + "') AS BUTTON_BACK \n" + 
						"FROM    DUAL";

		if(bDebug){
			System.out.println("QUERY getPantalla EN DAO " + sSentenciaSql );
		}
		ejecutaSentencia();
		  
		if(rs.next()){
			buttons.sButtonBack							= rs.getString("BUTTON_BACK");
			buttons.sButtonSearch						= rs.getString("BUTTON_SEARCH");
			buttons.sButtonClear						= rs.getString("BUTTON_CLEAR");
			buttons.sButtonCreateRecord					= rs.getString("BUTTON_CREATE_RECORD");
			buttons.sButtonDeleteRecord					= rs.getString("BUTTON_DELETE_RECORD");
			buttons.sButtonUpdateRecord					= rs.getString("BUTTON_UPDATE_RECORD");
			buttons.sButtonAddAttribute					= rs.getString("BUTTON_ADD_ATTRIBUTE");
			buttons.sButtonGoToCreateRecord				= rs.getString("BUTTON_GO_TO_CREATE_RECORD");
			buttons.sButtonGoToCreateRecordFromSearch 	= rs.getString("BUTTON_GO_CREATE_FROM_SEARCH");
			
		}
		
		return buttons;

	}
	
	
	public String sGetPlantilla(int iIdEmpresa, String sCveUsuario, String sEsPopUpWin, boolean bDebug) throws SQLException{
		
		String sPlantilla = "";
		
		sSentenciaSql = "SELECT  PKG_INTERFACE.AOS_INTFZ_RETURN_CLOB('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_HTML_GET_PLANTILLA(" + 
							iIdEmpresa + ",''" + sCveUsuario + "'', ''" + sEsPopUpWin + "''); END;','" + sCveUsuario + "') AS HTML FROM DUAL ";

		if(bDebug){
			System.out.println("QUERY sGetPlantilla EN DAO " + sSentenciaSql );
		}
		ejecutaSentencia();
		
		if(rs.next()){
			sPlantilla = rs.getString("HTML");
		}
		
		return sPlantilla;
	}
	
	

	public int iIdFile() throws SQLException{
		
		int iIdFile = 0;

		sSentenciaSql = "SELECT AOS_SEQ_ID_ARCHIVO.NEXTVAL AS ID_ARCHIVO FROM DUAL";

		ejecutaSentencia();
		
		if(rs.next()){
			iIdFile = rs.getInt("ID_ARCHIVO");			
		}
		
		return iIdFile;

	}
	
	public int iIdLogEjecutable() throws SQLException{
		
		int iIdLog = 0;

		sSentenciaSql = "SELECT CONTROL.AOS_CTL_LOG_SISTEMA_SQ.NEXTVAL AS ID_LOG FROM DUAL";

		ejecutaSentencia();
		
		if(rs.next()){
			iIdLog = rs.getInt("ID_LOG");			
		}
		
		return iIdLog;

	}

	
	public String sDameMensajeSistema(int iIdMensaje, String sCveUsuario, boolean bDebug) throws SQLException{
		
		String sMensaje = "";
		CallableStatement callst = conn.prepareCall(
				"{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_DAME_MENSAJE_SISTEMA(" + iIdMensaje + "); END;','" 
						+ sCveUsuario + "')}");
		callst.registerOutParameter(1, java.sql.Types.VARCHAR);
	      
		callst.execute();
		
	    if(bDebug){
	    	System.out.println("Despues de ejecutar EN DAO sDameMensajeSistema");
	    }

	    sMensaje = callst.getString(1);
	      
		return sMensaje;
	}



	public String sDameAtributosTablaRecupera(int iIdEmpresa, int iIdEjecutable, int iIdLogError, String sDatos, String sCveUsuario, boolean bDebug) throws SQLException{

		String sResultado = "";

		//iGuardaCLOB(int iIdEmpresa, int iIdEjecutable, String sPantalla, String sCveUsuario, String sCLOB, boolean bDebug)
		int iClobParam = iGuardaCLOB(iIdEmpresa, iIdEjecutable, 0, sCveUsuario, sDatos, "java sDameAtributosTablaRecupera", bDebug);
		//sSentenciaSql = "SELECT PKG_INTERFACE.AOS_DAME_ATRIBUTOS_ENTIDAD(" + iIdEmpresa + ", " + idEjecutable + ", " + iIdLog + ", '" +
			//	sDatos.replace("'", "''") + "' ) AS ATRIBUTOS FROM DUAL";
		CallableStatement callst = conn.prepareCall(
				"{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_CLOB('BEGIN :result := PKG_DYNAMIC_DB.AOS_DAME_ATRIBUTOS_ENTIDAD(" + iIdEmpresa + 
					", " + iIdEjecutable +", " + iIdLogError + ", " + iClobParam  + "); END;','" + sCveUsuario + "')}");

    	callst.registerOutParameter(1, java.sql.Types.CLOB);

    	if(bDebug){
    		System.out.println("Antes de ejecuci�n EN DAO sDameAtributosTablaRecupera ");
    	}
	    callst.execute(); //  ------->>>>> ERROR Occur
	    if(bDebug){
	    	System.out.println("Despues ejecuci�n EN DAO sDameAtributosTablaRecupera");
	    }
		
	    sResultado = callst.getString(1);
	    return sResultado;
		
	}
	
	
	public String sDameMensajeControl(int iIdMensaje, String sCveUsuario) throws SQLException{
		
		String sMensaje = "";
		
		sSentenciaSql = "SELECT PKG_INTERFACE.AOS_INTFZ_RETURN_VARCHAR('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_DAME_MENSAJE_CONTROL(" + iIdMensaje + "); END;','" + sCveUsuario + "') AS MENSAJE FROM DUAL";

		ejecutaSentencia();
		
		if(rs.next()){
			sMensaje = rs.getString("MENSAJE");			
		}
		return sMensaje;
	}

	public String sFileInsert(int iIdFile, int iIdEmpresa, String sCveUsuario, String sNomArchivo, String sContentType, FileItem file, boolean bDebug) throws SQLException,  IOException{
		
		String sResultado = "";

		CallableStatement callst = conn.prepareCall("{ ? = call PKG_INTERFACE.AOS_INSERTA_ARCHIVO(?,?,?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.VARCHAR);
	      callst.setInt(2,iIdFile);
	      callst.setInt(3,iIdEmpresa);
	      callst.setString(4,sCveUsuario);
	      callst.setString(5,sNomArchivo);
	      callst.setString(6,sContentType);
	      callst.setBinaryStream(7, file.getInputStream(), (int) file.getSize());
	      
	      if(bDebug){
	    	  System.out.println("Antes de ejecutar inserta BLOB Archivo EN DAO ");
	      }
	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de ejecutar inserta BLOB Archivo EN DAO");
	      }

	      sResultado = callst.getString(1);
	      
	      return sResultado;

	}
	
	
	public String sDameOpcionesCombo(int iIdEmpresa, int iIdListado, String sSelected, String sDatos, 
			String sBObligatorio, String sCveUsuario, boolean bDebug) throws SQLException,  IOException{
		
		String sResultado = "";
		
		//iGuardaCLOB(int iIdEmpresa, int iIdEjecutable, String sPantalla, String sCveUsuario, String sCLOB, boolean bDebug)
		int iClobParam = iGuardaCLOB(iIdEmpresa, 0, 0, sCveUsuario, sDatos, "java sDameOpcionesCombo", bDebug);
 
		//CallableStatement callst = conn.prepareCall("{ ? = call PKG_INTERFACE.AOS_HTML_OPCIONES_COMBO(peIdEmpresa=>?,peIdListado=>?,
		// peSelected=>?, peIdDatos=>?,peBValidaEtiq=>?, peOpcionNull=>?)}");
		CallableStatement callst = conn.prepareCall(
				"{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_CLOB('BEGIN :result := PKG_DYNAMIC_BASIC.AOS_HTML_OPCIONES_COMBO(peIdEmpresa=>" + iIdEmpresa + 
				",peIdListado=>" + iIdListado + ", peSelected=>''" + sSelected + "'', peIdDatos=>" + iClobParam + ", "
						+ "peBObligatorio=>''" + sBObligatorio + "''); END;','" + sCveUsuario + "')}");
		callst.registerOutParameter(1, java.sql.Types.CLOB);
    	
    	if(bDebug){
    		System.out.println("Antes o datos EN DAO sDameOpcionesCombo \n ");
    	}
    	callst.execute(); //  ------->>>>> ERROR Occur
    	if(bDebug){
    		System.out.println("Despues actualizar datos EN DAO sDameOpcionesCombo");
    	}
    	
    	sResultado = callst.getString(1);
    	return sResultado;

	}
	
	public String sRefFileInsert(int iIdFile, int iIdEmpresa, String sCveUsuario, String sNomArchivo, String sContentType, String sFileDir, boolean bDebug) throws SQLException,  IOException{
		
		String sResultado = "";

		CallableStatement callst = conn.prepareCall("{ ? = call PKG_INTERFACE.AOS_INSERTA_ARCHIVO(?,?,?,?,?,?,?)}");
	      callst.registerOutParameter(1, java.sql.Types.VARCHAR);
	      callst.setInt(2,iIdFile);
	      callst.setInt(3,iIdEmpresa);
	      callst.setString(4,sCveUsuario);
	      callst.setString(5,sNomArchivo);
	      callst.setString(6,sContentType);
	      callst.setBinaryStream(7, null);
	      callst.setString(8,sFileDir);
	      
	      if(bDebug){
	    	  System.out.println("Antes de ejecutar inserta BFILE EN DAO ");
	    	  System.out.println("NOMBRE DEL ARCHIVO EN DAO sRefFileInsert --->>>  " + sNomArchivo);
	      }
	      
	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de ejecutar inserta BFILE EN DAO");
	      }

	      sResultado = callst.getString(1);
		
	      return sResultado;

	}
	
	public String sGetTemplateResultSet(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sCveUsuario, boolean bDebug) throws SQLException{
		
		String sTemplate = "";

		String sStmt = "{ ? = call PKG_INTERFACE.AOS_INTFZ_RETURN_CLOB('BEGIN :result := PKG_DYNAMIC_BASIC.GET_TEMPLATE_RESULT_SET(" +  iIdEmpresa + 
				", " + iIdPantalla + ", " + iIdTabla + "); END;','" + sCveUsuario + "')}";

		CallableStatement callst = conn.prepareCall(sStmt);

		callst.registerOutParameter(1, java.sql.Types.CLOB);
	      
    	if(bDebug){
    		System.out.println("Antes de ejecutar EN DAO sGetTemplateResultSet \n" + sStmt);
    	}
	      callst.execute(); //  ------->>>>> ERROR Occur
	      if(bDebug){
	    	  System.out.println("Despues de ejecutar sGetTemplateResultSet EN DAO");
	      }

	      sTemplate = callst.getString(1);
	      
	      if(bDebug){
	    	  System.out.println("sTemplate ------------------------------------------------------------- \n" + sTemplate);
	    	  System.out.println("sTemplate ------------------------------------------------------------- \n");
	      }
	      
	      return sTemplate;
	}
	
	public boolean bDebug(int iIdEmpresa, String sCveUsuario) throws SQLException {

		boolean bDebug = false;

		sSentenciaSql = 
				" 	SELECT  CASE WHEN (	SELECT NVL(VALOR_CONCEPTO, ?||'X')  \n " +
				"						FROM 	AOS_ARQ_REGISTRO 			\n " +
				"						WHERE 	ID_EMPRESA = ?				\n " +
				"			 			AND ID_REGISTRO = 2501) = ? THEN 'V' ELSE 'F' END AS B_DEBUG	\n " +	
			    "   FROM    DUAL \n " ;

		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setString(1, sCveUsuario);
		pstmt.setInt(2, iIdEmpresa);
		pstmt.setString(3, sCveUsuario);
		
		System.out.println("sCveUsuario ---------------------------- " + sCveUsuario);
		
		rs = pstmt.executeQuery();

		if(rs.next()){
			System.out.println("rs.getString(B_DEBUG) ---------------------------- " + rs.getString("B_DEBUG"));
			bDebug = rs.getString("B_DEBUG").equals("V") ? true : false;
		}else{
			System.out.println("rs. no tiene datos -- ");
		}

	    return bDebug;
	}
	

	public Map<String, Object>getSettings(String sCveMasterTemplate, String sCveMasterTemplateVersion, String sFiltro, boolean bDebug) throws SQLException {

		Map<String, Object> map = new HashMap<String, Object>();
		String[][] sRemplazos =  new String[10000][2];
		String sTmpClave = "";
		String sClave = "";
		int i = 0;
		int iCuentaRegs = 0;
		
		sSentenciaSql = 
				" 	SELECT  CVE_SETTING||VAL_SETTING AS CVE_SETTING, TAG_TO_REPLACE, D.VAL_REPLACE, S.ID_ORDER, D.ID_ORDER \n " + 
			    "   FROM    AOS_CTL_SETTINGS S  				\n " + 
			    "       JOIN AOS_CTL_SETTINGS_DET D 			\n " + 
			    "           USING (ID_SETTING) 					\n " + 
			    "   WHERE   S.CVE_MASTER_TEMPLATE = ? 			\n " +  
			    "       AND S.CVE_MASTER_TEMPLATE_VERSION = ? 	\n " +  
			    "       AND S.SIT_SETTING = 'AC' 				\n " + 
			    "       AND D.SIT_DETALLE = 'AC' 				\n " +
			    "   	AND REGEXP_LIKE(CVE_SETTING||VAL_SETTING, ?) \n " +
			    "   ORDER BY S.ID_ORDER, D.ID_ORDER 			\n ";

		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setString(1, sCveMasterTemplate);
		pstmt.setString(2, sCveMasterTemplateVersion);
		pstmt.setString(3, sFiltro);
		
		if(bDebug){
	    	  System.out.println("sSentenciaSql getSettings ------------------------------------------------------------- \n" + sSentenciaSql);
	      }
		
		rs = pstmt.executeQuery();

		while(rs.next()){
			sClave = rs.getString("CVE_SETTING"); 
			if(bDebug){
		    	  System.out.println("sClave " + sClave);
		    	  System.out.println("TAG_TO_REPLACE " + rs.getString("TAG_TO_REPLACE"));
		    	  System.out.println("VAL_REPLACE " + rs.getString("VAL_REPLACE"));
			}
		
			
			if(!sTmpClave.equals(sClave) && !sTmpClave.equals("")){
				iCuentaRegs = iCuentaRegs + 1; 
				map.put(String.valueOf(iCuentaRegs), sRemplazos);
				sRemplazos =  new String[10000][2];
				if(bDebug){
			    	  System.out.println("Setting " + sTmpClave + " Se agrega con " + i);
				}
				i = 0;
			}

			sRemplazos[i][0] = rs.getString("TAG_TO_REPLACE"); 
			sRemplazos[i][1] = rs.getString("VAL_REPLACE"); 
			i = i + 1; 
			sTmpClave = sClave;
		}
		
		if(!sClave.equals("")){
			iCuentaRegs = iCuentaRegs + 1; 
			map.put(String.valueOf(iCuentaRegs), sRemplazos);
		}

	    return map;
	}

	
	public String sDameListaExcluye(int iIdEmpresa, String sStmtInsert, String sQryExclude, boolean bDebug) throws SQLException {
		
		String sResultado = "";
		
		CallableStatement callst = conn.prepareCall("{ ? = call PKG_DYNAMIC_BASIC.AOS_EXCLUDE_LIST(?,?,?)}");
		callst.registerOutParameter(1, java.sql.Types.VARCHAR);
		callst.setInt(2,iIdEmpresa);		
		callst.setString(3,sStmtInsert);
    	callst.setString(4,sQryExclude);

    	if(bDebug){
    		System.out.println("Antes o datos EN DAO sDameListaExcluye \n ");
    	}
    	callst.execute(); //  ------->>>>> ERROR Occur
    	if(bDebug){
    		System.out.println("Despues actualizar datos EN DAO sDameListaExcluye");
    	}
    	
    	sResultado = callst.getString(1);
    	return sResultado;
	}	
	

/*****************************   	 	CODIGO NUEVO JQ     	*******************************************************************************************************************/

public String sGetMenu(int iIdEmpresa, String sCveUsuario,  String sSchema, String sCveTemplateMaster, int iIdRol, String sGenero, String sIsMobile, boolean bDebug) throws SQLException{

	String sResultado = "";
	String sSchemaName = "";
	DatabaseMetaData mtdata = conn.getMetaData();
	String sDBType 			= mtdata.getDriverName();
	String sDBProduct 		= mtdata.getDatabaseProductName();
	
	if(sDBProduct.equals("Oracle")){
		sSchemaName = sSchema; 
	}else{
		sSchemaName = conn.getSchema();
	}
	
	
	sSentenciaSql =	"SELECT  DISTINCT A.ID_APLICACION AS A, A.TITULO_APLICACION AS T, A.ID_ORDEN_APP AS O, 					\n " +
					"		A.RUTA_APP AS R, A.ICON_MENU_APP AS I, '1' AS X 												\n " +
					"FROM   ( 																								\n " +
					" 	SELECT  B.ID_MENU, B.TEMPLATE, B.TITULO_MENU, B.ID_PANTALLA, B.ID_TABLA, B.ID_ORDEN,				\n " +
					" 			B.TIPO_MENU AS CLASS_DIR, B.ID_EJECUTABLE, B.ID_PROCESO, B.TIPO_MENU, B.ICON_MENU, 			\n " +			
					"			B.CVE_NAVEGACION, B.RUTA, B.ID_APLICACION, B.TITULO_APLICACION, B.ID_ORDEN_APP, 			\n " +
					"			B.RUTA_APP, B.ICON_MENU_APP																	\n " +
					" 	FROM    ( 																							\n " +
					" 		SELECT  M.TEMPLATE_HTML AS TEMPLATE, M.ID_EMPRESA, M.TITULO_MENU, M.RUTA, 						\n " +
					" 	    		M.ID_PANTALLA, NVL(P.ID_TABLA_MAESTRO,0) AS ID_TABLA, M.ID_ORDEN, 						\n " +
					" 	    		M.TIPO_MENU, M.ID_MENU, M.ID_EJECUTABLE, M.ID_PROCESO, M.ICON_MENU, M.CVE_NAVEGACION, 	\n " +
					" 	    		AP.ID_APLICACION, AP.TITULO_APLICACION, AP.ID_ORDEN AS ID_ORDEN_APP,					\n " +
					"				AP.RUTA AS RUTA_APP, NVL(AP.ICON_MENU,'fa-desktop') AS ICON_MENU_APP					\n " +
					" 		FROM    AOS_ARQ_MENU M																			\n " +	
					" 			JOIN 	AOS_SECU_APLICACION AP																\n " +
					" 	    		ON  AP.ID_EMPRESA       	= M.ID_EMPRESA												\n " +
					" 	    		AND AP.ID_APLICACION    	= M.ID_APLICACION											\n " +
					" 			LEFT JOIN AOS_SECU_USUARIO U																\n " +
					" 	    		ON M.ID_EMPRESA     		= U.ID_EMPRESA												\n " +
					" 	    		AND U.CVE_USUARIO   		= ?															\n " +
					" 			JOIN AOS_SECU_ROL R																			\n " +
					" 	    		ON M.ID_EMPRESA    			= R.ID_EMPRESA												\n " +
					" 	    		AND NVL(U.ID_ROL,5)			= R.ID_ROL													\n " +		
					" 	    		AND R.SIT_ROL      			= 'AC'														\n " +
					" 			JOIN AOS_SECU_ROL_APLICACION RA																\n " +
					" 	    		ON RA.ID_EMPRESA    		= R.ID_EMPRESA												\n " +
					" 	    		AND RA.ID_ROL       		= R.ID_ROL													\n " +
					" 	    		AND RA.ID_APLICACION		= AP.ID_APLICACION											\n " +	
					" 	    		AND RA.SIT_ROL_APLICACION 	= 'AC'														\n " +
					" 			LEFT JOIN AOS_SECU_ROL_APLICACION_PANT A													\n " +
					" 	    		ON  A.ID_EMPRESA        	= M.ID_EMPRESA												\n " +
					" 	    		AND A.ID_APLICACION     	= M.ID_APLICACION											\n " +
					" 	    		AND A.ID_PANTALLA       	= M.ID_PANTALLA												\n " +
					" 	    		AND A.ID_ROL            	= R.ID_ROL													\n " +	
					" 	    		AND A.SIT_APLIC_PANTALLA	= 'AC'														\n " +
					" 			LEFT JOIN AOS_ARQ_PANTALLA P																\n " +	
					" 	    		ON  P.ID_EMPRESA    		= M.ID_EMPRESA												\n " +	
					" 	    		AND P.ID_PANTALLA   		= M.ID_PANTALLA												\n " +	
					" 	    		AND P.SIT_PANTALLA  		= 'AC'														\n " +
					" 			LEFT JOIN AOS_SECU_ROL_PANT_TABLA_PERMIS T													\n " +
					" 	    		ON  T.ID_EMPRESA   			= A.ID_EMPRESA												\n " +
					" 	    		AND T.ID_ROL       			= A.ID_ROL													\n " +	
					" 	    		AND T.ID_PANTALLA  			= A.ID_PANTALLA												\n " +	
					" 	    		AND T.ID_APLICACION			= A.ID_APLICACION											\n " +	
					" 	    		AND T.ID_TABLA     			= P.ID_TABLA_MAESTRO 										\n " +
					" 	    		AND T.B_CONSULTA   			= 'V'														\n " +
					" 			LEFT JOIN AOS_SECU_USUARIO_PERMISO_EXEC PE													\n " +
					" 	    		ON M.ID_EMPRESA         	= PE.ID_EMPRESA												\n " +
					" 	    		AND M.ID_PROCESO       	 	= PE.ID_EJECUTABLE											\n " +
					" 	    		AND PE.CVE_USUARIO      	= ?															\n " +	
					" 	    		AND PE.SIT_PERMISO      	= 'AC'														\n " +	
					" 			LEFT JOIN AOS_SECU_USU_PERMISO_EXEC_CTL PCTL												\n " +
					" 	    		ON  M.ID_EMPRESA        	= PCTL.ID_EMPRESA											\n " +
					" 	    		AND M.ID_EJECUTABLE     	= PCTL.ID_EJECUTABLE										\n " +
					" 				AND PCTL.CVE_USUARIO    	= ?															\n " +
					" 	    		AND PCTL.SIT_PERMISO    	= 'AC'														\n " +
					" 			-- Verificaci�n de permisos de ejecutables del Framework									\n " +
					" 			LEFT JOIN CONTROL.AOS_CTL_MTDAT_OBJ_EXE EXEC												\n " +
					" 	    		ON EXEC.ID_EJECUTABLE 		=	PCTL.ID_EJECUTABLE										\n " +
					" 			LEFT JOIN CONTROL.AOS_CTL_FRAMEWORK_MODULE FM												\n " +	
					" 	    		ON FM.MODULE_NAME   		= EXEC.MODULE_NAME											\n " +		
					" 	    		AND FM.STATUS_MODULE		= 'AC'														\n " +
					" 			LEFT JOIN CONTROL.AOS_CTL_SCHEMA_FRAMEWORK_GRANT FG											\n " +
					" 	    		ON  FG.SCHEMA_NAME  		= ?															\n " +
					" 	    		AND FG.MODULE_NAME  		= FM.MODULE_NAME											\n " +
					" 	    		AND FG.STATUS_GRANT 		= 'AC'														\n " +
					" 			-- Verificaci�n de permisos de p�ginas del Framework										\n " +
					" 			LEFT JOIN CONTROL.AOS_CTL_SCHEMA_PAGE_MODULE SPM											\n " +
					" 	   	 		ON  SPM.ID_PAGE   			= P.ID_PANTALLA												\n " +
					" 			LEFT JOIN CONTROL.AOS_CTL_FRAMEWORK_MODULE FMP												\n " +
					" 	    		ON FMP.MODULE_NAME   		= SPM.MODULE_NAME											\n " +	
					" 	    		AND FMP.STATUS_MODULE		= 'AC'														\n " +
					" 			LEFT JOIN CONTROL.AOS_CTL_SCHEMA_FRAMEWORK_GRANT PG											\n " +	
					" 	    		ON  PG.SCHEMA_NAME  		= ?															\n " +
					" 	    		AND PG.MODULE_NAME  		= FMP.MODULE_NAME											\n " +
					" 	    		AND PG.STATUS_GRANT 		= 'AC'														\n " +
					" 		WHERE   M.ID_EMPRESA    			= ?															\n " +
					" 			AND M.SIT_MENU      			= 'AC'														\n " +		
					" 			AND AP.SIT_APLICACION  			= 'AC'														\n " +	
					" 			AND (P.ID_EMPRESA IS NULL OR NOT (A.ID_EMPRESA IS NULL OR (NVL(P.ID_TABLA_MAESTRO,0) > 0 AND T.ID_EMPRESA IS NULL)))	\n " +
					" 			AND NOT(M.ID_PANTALLA IS NULL AND NVL(M.TIPO_MENU,'X') NOT IN ('DIR','DIVISOR'))										\n " +
					" 			AND NOT(PE.SIT_PERMISO IS NULL AND M.ID_PANTALLA = 0 AND M.ID_PROCESO IS NOT NULL)										\n " +
					" 			AND NOT(PCTL.SIT_PERMISO IS NULL AND M.ID_PANTALLA = -5 AND M.ID_EJECUTABLE IS NOT NULL AND M.ID_EJECUTABLE < 0)		\n " +
					" 			AND NOT(EXEC.ID_EJECUTABLE IS NOT NULL AND FG.SCHEMA_NAME IS NULL)														\n " +
					" 			AND NOT(SPM.ID_PAGE IS NOT NULL AND SPM.ID_PAGE = -5 AND SPM.MODULE_NAME = 'FRAMEWORK' AND M.ID_EJECUTABLE IS NULL AND PG.SCHEMA_NAME IS NULL)	\n " +
					" 			AND NOT(SPM.ID_PAGE IS NOT NULL AND SPM.ID_PAGE <> -5 AND PG.SCHEMA_NAME IS NULL)																\n " +
					" 	)B ) A";
	
	if(bDebug){
		System.out.println("  QUERY sGetMenu EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
				" sCveUsuario: 	" + sCveUsuario + "\n" +
				" sSchema: 		" + sSchemaName + "\n" +
				" iIdEmpresa: 	" + iIdEmpresa + "\n"); 
	}
	
	Object[] params = new Object[6];
	params[0] = (String)sCveUsuario; 
	params[1] = (String)sCveUsuario; 
	params[2] = (String)sCveUsuario;
	params[3] = sSchemaName;
	params[4] = sSchemaName; 
	params[5] = (Integer)iIdEmpresa;
	
	
	sResultado = resultSetToJson(conn, sSentenciaSql, params); 
	
	
	Gson gson = new Gson();
	JsonArray datosApp = new JsonArray();
	datosApp  = gson.fromJson(sResultado, JsonArray.class);
	 
	JsonObject obj = new JsonObject();
	 
	obj.add("datosApp", datosApp);
 
	
	sSentenciaSql =	" 	SELECT  	A.ID_MENU AS I, A.TITULO_MENU AS T, A.ID_PANTALLA AS P, A.ID_TABLA AS B, A.ID_ORDEN AS O, 		\n " +
					"				A.ID_ORDEN_APP AS X, DS AS D, A.TIPO_MENU AS Y, A.ID_EJECUTABLE AS E, A.ID_PROCESO AS R, 		\n " +
					"				A.ICON_MENU C, A.RUTA U, A.ID_MENU_PADRE AS H, A.ID_APLICACION AS A, A.DIRECT_TO_ALT_MOD AS M 	\n " +
					" 	FROM    ( 																									\n " +
					" 		SELECT  M.TEMPLATE_HTML AS TEMPLATE, M.ID_EMPRESA, M.TITULO_MENU, M.RUTA, 								\n " +
					" 	    		M.ID_PANTALLA, NVL(P.ID_TABLA_MAESTRO,0) AS ID_TABLA, M.ID_ORDEN, M.ID_MENU_PADRE,				\n " +
					//" 	    		DECODE(NVL(M.TIPO_MENU,'NORMAL'),'NORMAL','N','DIR','D','DIVISOR','V','ELINK','L',M.TIPO_MENU) " +
					" 	    		CASE NVL(M.TIPO_MENU,'NORMAL')																	\n " +
					"					WHEN 'NORMAL' 	THEN 'N'																	\n " +
					"					WHEN 'DIR'		THEN 'D'																	\n " +
					"					WHEN 'DIVISOR'	THEN 'V'																	\n " +
					"					WHEN 'ELINK'	THEN 'L'																	\n " +
					"				ELSE M.TIPO_MENU END AS TIPO_MENU, 																\n " +
					"				M.ID_MENU, M.ID_EJECUTABLE, M.ID_PROCESO, M.ICON_MENU, M.CVE_NAVEGACION, 						\n " +
					" 	    		AP.ID_APLICACION, AP.TITULO_APLICACION, AP.ID_ORDEN AS ID_ORDEN_APP, M.DIRECT_TO_ALT_MOD,		\n " +
					"				AP.RUTA AS RUTA_APP, NVL(AP.ICON_MENU,'fa-desktop') AS ICON_MENU_APP, M.DESC_MENU_SHORT DS		\n " +
					" 		FROM    AOS_ARQ_MENU M																					\n " +	
					" 			JOIN 	AOS_SECU_APLICACION AP																		\n " +
					" 	    		ON  AP.ID_EMPRESA       	= M.ID_EMPRESA														\n " +
					" 	    		AND AP.ID_APLICACION    	= M.ID_APLICACION													\n " +
					" 			LEFT JOIN AOS_SECU_USUARIO U																		\n " +
					" 	    		ON M.ID_EMPRESA     		= U.ID_EMPRESA														\n " +
					" 	    		AND U.CVE_USUARIO   		= ?																	\n " +
					" 			JOIN AOS_SECU_ROL R																					\n " +
					" 	    		ON M.ID_EMPRESA    			= R.ID_EMPRESA														\n " +
					" 	    		AND NVL(U.ID_ROL,5)			= R.ID_ROL															\n " +		
					" 	    		AND R.SIT_ROL      			= 'AC'																\n " +
					" 			JOIN AOS_SECU_ROL_APLICACION RA																		\n " +
					" 	    		ON RA.ID_EMPRESA    		= R.ID_EMPRESA														\n " +
					" 	    		AND RA.ID_ROL       		= R.ID_ROL															\n " +
					" 	    		AND RA.ID_APLICACION		= AP.ID_APLICACION													\n " +	
					" 	    		AND RA.SIT_ROL_APLICACION 	= 'AC'																\n " +
					" 			LEFT JOIN AOS_SECU_ROL_APLICACION_PANT A															\n " +
					" 	    		ON  A.ID_EMPRESA        	= M.ID_EMPRESA														\n " +
					" 	    		AND A.ID_APLICACION     	= M.ID_APLICACION													\n " +
					" 	    		AND A.ID_PANTALLA       	= M.ID_PANTALLA														\n " +
					" 	    		AND A.ID_ROL            	= R.ID_ROL															\n " +	
					" 	    		AND A.SIT_APLIC_PANTALLA	= 'AC'																\n " +
					" 			LEFT JOIN AOS_ARQ_PANTALLA P																		\n " +	
					" 	    		ON  P.ID_EMPRESA    		= M.ID_EMPRESA														\n " +	
					" 	    		AND P.ID_PANTALLA   		= M.ID_PANTALLA														\n " +	
					" 	    		AND P.SIT_PANTALLA  		= 'AC'																\n " +
					" 			LEFT JOIN AOS_SECU_ROL_PANT_TABLA_PERMIS T															\n " +
					" 	    		ON  T.ID_EMPRESA   			= A.ID_EMPRESA														\n " +
					" 	    		AND T.ID_ROL       			= A.ID_ROL															\n " +	
					" 	    		AND T.ID_PANTALLA  			= A.ID_PANTALLA														\n " +	
					" 	    		AND T.ID_APLICACION			= A.ID_APLICACION													\n " +	
					" 	    		AND T.ID_TABLA     			= P.ID_TABLA_MAESTRO 												\n " +
					" 	    		AND T.B_CONSULTA   			= 'V'																\n " +
					" 			LEFT JOIN AOS_SECU_USUARIO_PERMISO_EXEC PE															\n " +
					" 	    		ON M.ID_EMPRESA         	= PE.ID_EMPRESA														\n " +
					" 	    		AND M.ID_PROCESO       	 	= PE.ID_EJECUTABLE													\n " +
					" 	    		AND PE.CVE_USUARIO      	= ?																	\n " +	
					" 	    		AND PE.SIT_PERMISO      	= 'AC'																\n " +	
					" 			LEFT JOIN AOS_SECU_USU_PERMISO_EXEC_CTL PCTL														\n " +
					" 	    		ON  M.ID_EMPRESA        	= PCTL.ID_EMPRESA													\n " +
					" 	    		AND M.ID_EJECUTABLE     	= PCTL.ID_EJECUTABLE												\n " +
					" 				AND PCTL.CVE_USUARIO    	= ?																	\n " +
					" 	    		AND PCTL.SIT_PERMISO    	= 'AC'																\n " +
					" 			-- Verificaci�n de permisos de ejecutables del Framework											\n " +
					" 			LEFT JOIN CONTROL.AOS_CTL_MTDAT_OBJ_EXE EXEC														\n " +
					" 	    		ON EXEC.ID_EJECUTABLE 		=	PCTL.ID_EJECUTABLE												\n " +
					" 			LEFT JOIN CONTROL.AOS_CTL_FRAMEWORK_MODULE FM														\n " +	
					" 	    		ON FM.MODULE_NAME   		= EXEC.MODULE_NAME													\n " +		
					" 	    		AND FM.STATUS_MODULE		= 'AC'																\n " +
					" 			LEFT JOIN CONTROL.AOS_CTL_SCHEMA_FRAMEWORK_GRANT FG													\n " +
					" 	    		ON  FG.SCHEMA_NAME  		= ?																	\n " +
					" 	    		AND FG.MODULE_NAME  		= FM.MODULE_NAME													\n " +
					" 	    		AND FG.STATUS_GRANT 		= 'AC'																\n " +
					" 			-- Verificaci�n de permisos de p�ginas del Framework												\n " +
					" 			LEFT JOIN CONTROL.AOS_CTL_SCHEMA_PAGE_MODULE SPM													\n " +
					" 	   	 		ON  SPM.ID_PAGE   			= P.ID_PANTALLA														\n " +
					" 			LEFT JOIN CONTROL.AOS_CTL_FRAMEWORK_MODULE FMP														\n " +
					" 	    		ON FMP.MODULE_NAME   		= SPM.MODULE_NAME													\n " +	
					" 	    		AND FMP.STATUS_MODULE		= 'AC'																\n " +
					" 			LEFT JOIN CONTROL.AOS_CTL_SCHEMA_FRAMEWORK_GRANT PG													\n " +	
					" 	    		ON  PG.SCHEMA_NAME  		= ?																	\n " +
					" 	    		AND PG.MODULE_NAME  		= FMP.MODULE_NAME													\n " +
					" 	    		AND PG.STATUS_GRANT 		= 'AC'																\n " +
					" 		WHERE   M.ID_EMPRESA    			= ?																	\n " +
					" 			AND M.SIT_MENU      			= 'AC'																\n " +		
					" 			AND AP.SIT_APLICACION  			= 'AC'																\n " +	
					" 			AND (P.ID_EMPRESA IS NULL OR NOT (A.ID_EMPRESA IS NULL OR (NVL(P.ID_TABLA_MAESTRO,0) > 0 AND T.ID_EMPRESA IS NULL)))	\n " +
					" 			AND NOT(M.ID_PANTALLA IS NULL AND NVL(M.TIPO_MENU,'X') NOT IN ('DIR','DIVISOR'))										\n " +
					" 			AND NOT(PE.SIT_PERMISO IS NULL AND M.ID_PANTALLA = 0 AND M.ID_PROCESO IS NOT NULL)										\n " +
					" 			AND NOT(PCTL.SIT_PERMISO IS NULL AND M.ID_PANTALLA = -5 AND M.ID_EJECUTABLE IS NOT NULL AND M.ID_EJECUTABLE < 0)		\n " +
					" 			AND NOT(EXEC.ID_EJECUTABLE IS NOT NULL AND FG.SCHEMA_NAME IS NULL)														\n " +
					" 			AND NOT(SPM.ID_PAGE IS NOT NULL AND SPM.ID_PAGE = -5 AND SPM.MODULE_NAME = 'FRAMEWORK' AND M.ID_EJECUTABLE IS NULL AND PG.SCHEMA_NAME IS NULL)	\n " +
					" 			AND NOT(SPM.ID_PAGE IS NOT NULL AND SPM.ID_PAGE <> -5 AND PG.SCHEMA_NAME IS NULL)																\n " +
					" 		) A ";
	
	if(bDebug){
		System.out.println("  QUERY sGetMenu EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
				" sCveUsuario: 	" + sCveUsuario + "\n" +
				" sSchema: 		" + sSchema + "\n" +
				" iIdEmpresa: 	" + iIdEmpresa + "\n"); 
	}
	
	params = new Object[6];
	params[0] = (String)sCveUsuario; 
	params[1] = (String)sCveUsuario; 
	params[2] = (String)sCveUsuario;
	params[3] = sSchemaName;
	params[4] = sSchemaName; 
	params[5] = (Integer)iIdEmpresa;
	
	sResultado = resultSetToJson(conn, sSentenciaSql, params); 
	
	JsonArray datosMenu = new JsonArray();
	datosMenu  = gson.fromJson(sResultado, JsonArray.class);
	 
	obj.add("datosMenu", datosMenu);
	
	
	sSentenciaSql =	" 	SELECT  CASE WHEN ? = 'H' THEN 										\n " +
					"	            CASE WHEN ? = 'true' THEN								\n " +
					"	                NVL(H.IMG_MOBIL_MASC,   E.IMG_MOBIL_MASC)			\n " +
					"	            ELSE 													\n " +
					"	                NVL(H.IMG_DESKTOP_MASC, E.IMG_DESKTOP_MASC)			\n " +
					"	            END														\n " +		
					"	        ELSE 														\n " +
					"	            CASE WHEN ? = 'true' THEN								\n " +
					"	                NVL(H.IMG_MOBIL_FEM,    E.IMG_MOBIL_FEM)			\n " +
					"	            ELSE 													\n " +
					"	                NVL(H.IMG_DESKTOP_FEM,  E.IMG_DESKTOP_FEM) 			\n " +
					"	            END 													\n " +
					"	        END AS URL_IMG, ? AS E, ? AS U								\n " +		
					"	FROM    AOS_ARQ_EMPRESA E											\n " +
					"	    LEFT JOIN AOS_ARQ_HOME_PAGE_IMAGE H 							\n " +
					"	        ON E.ID_EMPRESA = H.ID_EMPRESA								\n " +
					"	        AND H.ID_ROL    = ? 										\n " +
					"	WHERE   E.ID_EMPRESA    = ?											\n " ; 
	
	params = new Object[7];
	params[0] = (String)sGenero; 
	params[1] = (String)sIsMobile;
	params[2] = (String)sIsMobile;
	params[3] = (Integer)iIdEmpresa;
	params[4] = (String)sCveUsuario;
	params[5] = (Integer)iIdRol; 
	params[6] = (Integer)iIdEmpresa;
	
	if(bDebug){
		System.out.println("  QUERY datosHome EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
				" sGenero: 		>" + sGenero 	+ "<\n" +
				" sIsMobile: 	>" + sIsMobile 	+ "<\n" +
				" iIdEmpresa: 	>" + iIdEmpresa + "<\n" +
				" sCveUsuario: 	>" + sCveUsuario+ "<\n" +
				" iIdRol: 		>" + iIdRol 	+ "<\n"); 
	}
	
	sResultado = resultSetToJson(conn, sSentenciaSql, params); 
	
	JsonArray datosHome = new JsonArray();
	datosHome  = gson.fromJson(sResultado, JsonArray.class);
	 
	obj.add("datosHome", datosHome);
 
	if(bDebug){
		System.out.println("  datosHome EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + obj.toString() + "\n" ); 
	}
	
	sResultado = obj.toString();

	return sResultado;
	
}



	public String sGetHTML(Usuario usuario, boolean bDebug) throws SQLException{

		String sHTMLCode = "";
		
		sSentenciaSql =	"	SELECT  T.HTML_PAGE_CODE, E.IMG_SITE, E.AVATAR_HTML, E.FOOTER, E.HOME_WELCOME_TEXT, ADDITIONAL_LINKS	\n" +
						" 	FROM    AOS_CTL_MASTER_TEMPLATE T 			\n" +
						"       LEFT JOIN 	AOS_ARQ_EMPRESA E					\n" +
						"			ON E.ID_EMPRESA 				= ?  		\n" +
						" 	WHERE   T.CVE_MASTER_TEMPLATE 			= ?  		\n" +
						"     	AND T.CVE_MASTER_TEMPLATE_VERSION 	= ? \n";
		
		if(bDebug){
			System.out.println("  QUERY sGetHTML EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" CVE_MASTER_TEMPLATE: 			" + usuario.sCveMasterTemplate + "\n" +
					" CVE_MASTER_TEMPLATE_VERSION: 	" + usuario.sCveMasterTemplateVersion + "\n"); 
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, usuario.iIdEmpresa);
		pstmt.setString(2, usuario.sCveMasterTemplate);
		pstmt.setString(3, usuario.sCveMasterTemplateVersion);  
		
		rs = pstmt.executeQuery();

		if( rs.next() ){
			String sBienvenida = usuario.sGenero.equals("H")?"<small>Bienvenido</small>&nbsp;":"<small>Bienvenida</small>&nbsp;";
			String sAvatar = usuario.sAvatar==null?(usuario.sGenero.equals("H")?"assAceV134/avatars/avatar4.png":"assAceV134/avatars/avatar3.png"):usuario.sAvatar;
			sHTMLCode	= rs.getString("HTML_PAGE_CODE");
			sHTMLCode	= sHTMLCode.replace("<!--BRAND-->", rs.getString("IMG_SITE")!=null?rs.getString("IMG_SITE"):""); 
			sHTMLCode	= sHTMLCode.replace("<!--PERSON_NAME-->", sBienvenida + usuario.sNomPersona);
			sHTMLCode	= sHTMLCode.replace("<!--AVATAR-->", rs.getString("AVATAR_HTML")!=null?rs.getString("AVATAR_HTML").replace("@AVATAR@", sAvatar):sAvatar);
			sHTMLCode	= sHTMLCode.replace("<!--FOOTER-->", rs.getString("FOOTER")!=null?rs.getString("FOOTER"):"");
			sHTMLCode	= sHTMLCode.replace("<!--ADDITIONAL_LINKS-->", rs.getString("ADDITIONAL_LINKS")!=null?rs.getString("ADDITIONAL_LINKS"):"");
		}
		
		rs.close();
		pstmt.close();
		return sHTMLCode;
	}
	
	public static String getDefaultCharEncoding(){
        byte [] bArray = {'w'};
        InputStream is = new ByteArrayInputStream(bArray);
        InputStreamReader reader = new InputStreamReader(is);
        String defaultCharacterEncoding = reader.getEncoding();
        return defaultCharacterEncoding;
    }



	
	public static String resultSetToJson(Connection connection, String query, Object[] params) {
	      List<Map<String, Object>> listOfMaps = null;
	      try {
	    	  QueryRunner queryRunner = new QueryRunner();
	          listOfMaps = queryRunner.query(connection, query, new MapListHandler(), params);
	      } catch (SQLException se) {
	    	  System.out.println("**********************************************"); 
	    	  System.out.println(se.getLocalizedMessage()); 
	    	  System.out.println("**********************************************");
	          throw new RuntimeException("Couldn't query the database.", se);
	      } //finally {DbUtils.closeQuietly(connection);}
	      return new Gson().toJson(listOfMaps);
	  }

	
	public JsonArray jaGetJQButtons(int iIdTabla, int iIdRol, String sCveOperacion, boolean bDebug){

		String sResultado 		= "";
		Gson gson 				= new Gson();
		JsonArray jaJasonArray 	= new JsonArray();
		Object[] params 		= new Object[3];

		// Obtiene los botones 
		sSentenciaSql =	"SELECT  A.TIPO_BOTON AS A, A.TX_TITULO AS B, A.ID_BOTON AS I, A.ID_ORDEN AS O, A.ICON AS C			\n " +		 	
			            "FROM    AOS_ARQ_PANTALLA_TABLA_BUTTON A								\n " +
			            "    LEFT JOIN AOS_SECU_ROL_PANT_TABLA_PERMIS TP						\n " +
			            "        ON  A.ID_EMPRESA 	= TP.ID_EMPRESA								\n " +
			            "		 AND A.ID_TABLA 	= TP.ID_TABLA								\n " +
			            "        AND TP.ID_ROL   	= ?											\n " +
			            "WHERE   A.ID_TABLA 		= ?											\n " +
			            "	 AND A.CVE_OPERACION 	= ?											\n " +
			            "    AND ( A.BIND_PRIV IS NULL -- No valida permisos 					\n " +
			            "        OR (-- SI Valida los Permisos, bot�n para un tipo de operaci�n	\n " +
			            "                 (A.BIND_PRIV = 'S'   AND TP.B_CONSULTA  = 'V') 		\n " +		
			            "             OR  (A.BIND_PRIV = 'I'   AND TP.B_ALTA      = 'V')		\n " +
			            "             OR  (A.BIND_PRIV = 'U'   AND TP.B_MODIFICA  = 'V')		\n " +
			            "             OR  (A.BIND_PRIV = 'D'   AND TP.B_BAJA      = 'V')		\n " +
			            "            )															\n " +
			            "        )																\n " +
			            "    AND A.SIT_BOTON = 'AC'												\n " ;
		
		if(bDebug){
			System.out.println("  QUERY sGetPantallaJQ BOTONES  EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdEmpresa: 		" + iIdRol 			+ "\n" +  
					" iIdTabla: 		" + iIdTabla 		+ "\n" + 
					" sCveOperacion: 	" + sCveOperacion 	+ "\n") ;   
		}
		
		params 		= new Object[3];
		params[0] 	= (Integer)iIdRol;
		params[1] 	= (Integer)iIdTabla;
		params[2] 	= (String)sCveOperacion;
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Botones Tabla: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		return jaJasonArray;
	}
	
	
	
	public String sGetPantallaJQ(HashMap<String, Object> map)throws SQLException{

		int iIdEmpresa 			= Integer.parseInt((String)map.get("DYN_CTL_ID_EMPRESA"));
		Usuario  usuario 		= (Usuario)map.get("DYN_CTL_USUARIO");
		String sCveOperacion	= (String) map.get("DYN_CTL_TIPO_OPERACION");
		String sCveNavegacion	= "MENU";
		int iIdTabla			= Integer.parseInt((String)map.get("DYN_CTL_ID_TABLA"));
		boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
		int iIdRol				= usuario.iIdRol;
		Object[] params 		= new Object[0];

		
		String sResultado 		= "";
		Gson gson 				= new Gson();
		JsonArray jaJasonArray 	= new JsonArray();
		JsonObject joObj 		= new JsonObject();
		JsonObject joObjList	= null;
		Integer iIdList  		= null;
		List<Integer> lLista 	= new ArrayList<Integer>();
		
		
		if(sCveNavegacion!=null&&sCveNavegacion.equals("MENU")){
			
			boolean bBuscaLRango	= true; 
			
			sSentenciaSql =	"SELECT	 A.TITULO_PANTALLA AS A, O.DESC_OPERACION AS B, P.ADDITIONAL_PLUGIN AS H, A.ID_EMPRESA AS I,	\n " +
							"		 A.PAGE_TYPE AS T, NVL(NVL(A.FONT_LABEL_COLOUR, E.FONT_LABEL_COLOUR),'#2679b5') AS C 			\n " +		
							"FROM    AOS_ARQ_PANTALLA_TABLA A				\n " +
							"	 JOIN AOS_ARQ_EMPRESA E 					\n " +
							"		 ON A.ID_EMPRESA = E.ID_EMPRESA 		\n " +
							" 	 JOIN AOS_ARQ_TIPO_OPERACION_DESC O 		\n " +
							" 		 ON O.ID_EMPRESA = A.ID_EMPRESA			\n " +
							" 		 AND O.CVE_OPERACION = ? 				\n " +
							"  	 LEFT JOIN AOS_ARQ_PANTALLA_TABLA_ADDP P 	\n " +
							"       ON 	P.ID_TABLA = A.ID_TABLA 			\n " +
							"       AND P.ID_EMPRESA = O.ID_EMPRESA			\n " +
							"       AND P.CVE_OPERACION = O.CVE_OPERACION	\n " +
							"       AND P.SIT_PLUGIN = 'AC' 				\n " +
							"WHERE   A.ID_TABLA = ? 						   " ;

			if(bDebug){
				System.out.println("  QUERY sGetPantallaJQ GET TABLA EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
						" sCveOperacion: 	" + sCveOperacion + "\n" +  
						" iIdTabla: 		" + iIdTabla + "\n"); 
			}
			
			params 		= new Object[2];
			params[0] 	= (String)sCveOperacion;
			params[1] 	= (Integer)iIdTabla;
			
			sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
			if(bDebug){
				System.out.println("sResultado Datos Tabla: " + sResultado);
			}
			jaJasonArray 	= new JsonArray();
			jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
			joObj.add("datosTabla", jaJasonArray);
			
			String sPageType= jaJasonArray.get(0).getAsJsonObject().get("T").getAsString();
			
			
			switch (sPageType){
			case "CATALOGO":
				// Obtiene los botones 
				joObj.add("datosBoton", jaGetJQButtons(iIdTabla, iIdRol, sCveOperacion, bDebug));

				
				// Obtiene los elementos del filtro de b�squeda
				sSentenciaSql =	"SELECT	 A.ID_ORDEN_ATRIBUTO AS O, A.ATRIBUTO AS A, A.B_OBLIGATORIO AS B, A.ETIQUETA_COMBO_DEPENDE AS C, A.DEFAULT_VAL AS D,  	\n " +			
								"		 A.ETIQUETA AS E, A.B_OPCION_COMBO_NULL AS F, A.ID_LISTADO AS G, A.TEMPLATE_HTML AS H, A.B_MODIFICABLE AS I, 			\n " +
								"		 A.B_SEARCH_USE_LIKE AS J, A.PLACEHOLDER AS K, TIPO_DATO AS L, A.B_FILTRO_INLINE AS M, A.DATE_FORMAT_FRONT AS N, 		\n " +
								"		 A.B_APLICA_RANGO_FILTRO AS P, A.SELECT_PARENT_ATTRIBUTE AS Q															\n " +	
								"FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A						\n " +
								"    LEFT JOIN AOS_SECU_ROL_PANT_TABLA_EX_CAM E   			\n " +
								" 		 ON 	E.ID_ATTRIBUTE 	= A.ID_ATRIBUTO 			\n " +
								"		 AND E.ID_ROL		= ? 							\n " +
								"WHERE   ID_TABLA = ? 										\n " +
		                        "    AND TIPO_ATRIBUTO = 'FILTRO'							\n " +
		                        " 	 AND SIT_ATRIBUTO = 'AC'								\n " +
		                        "    AND NOT(E.ID_ATTRIBUTE IS NOT NULL AND E.B_SELECT = 'F') ";
				 
				if(bDebug){
					System.out.println("  QUERY sGetPantallaJQ ATRIBUTOS EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
							" iIdTabla: 	" + iIdTabla + "\n" +
							" iIdEmpresa: 	" + iIdEmpresa + "\n"); 
				}
				
				params 		= new Object[2];
				params[0] 	= (Integer)iIdRol; 
				params[1] 	= (Integer)iIdTabla;
				
				sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
				if(bDebug){
					System.out.println("sResultado Datos Atributos: " + sResultado);
				}
				jaJasonArray 	= new JsonArray();
				jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
				
				for (int i = 0, size = jaJasonArray.size(); i < size; i++){
					joObjList 	= jaJasonArray.get(i).getAsJsonObject();
					if(joObjList.get("G")!=null&&!joObjList.get("H").getAsString().equals("ACE_INPUT_SELECT_FEED")){
						if(joObjList.get("Q")==null){
							iIdList		= joObjList.get("G").getAsInt(); 
							if(bDebug){
								System.out.println("Valor sel Listado: " + iIdList);
							}
							if(iIdList!=null&&!lLista.contains(iIdList)){
								lLista.add(iIdList);
							}
						}
					}
					if(bDebug){
						System.out.println("joObjList.get(L): " + joObjList.get("L").getAsString() + "  joObjList.get(H): " + joObjList.get("H").getAsString());
					}
					if(bBuscaLRango && (joObjList.get("L").getAsString().equals("DATE") || (joObjList.get("L").getAsString().equals("NUMBER") && joObjList.get("H").getAsString().equals("ACE_INPUT_TEXT")))){
						if(!lLista.contains(23)){
							lLista.add(23);
							bBuscaLRango = false; 
						}
					} 
				}
				if(bDebug){
					System.out.println("Tama�o del Listado: " + lLista.size());
				}
				
				joObj.add("datosFiltro", jaJasonArray);
				
				
				if(lLista.size() > 0){
					
					joObjList = new JsonObject();
					
					for(int element : lLista) {
						if(map.containsKey("DYN_CTL_ID_LISTADO")){
							map.replace("DYN_CTL_ID_LISTADO", String.valueOf(element));
						}else{
							map.put("DYN_CTL_ID_LISTADO", String.valueOf(element));
						} 
						sResultado 		= sGetSelectOptions(map); 
						jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
						joObjList.add(Integer.toString(element), jaJasonArray);
						if(bDebug){System.out.println(element);}
					}
					
					joObj.add("datosListados", joObjList);
					if(bDebug){
						System.out.println("sResultado Datos joObjList: " + joObjList.toString());
					}
				}
			
				break; // case "CATALOGO":
				
			}   // switch (sPageType){
			
		} //  if(sCveNavegacion!=null&&sCveNavegacion.equals("MENU")){
			
		sResultado = joObj.toString();

		if(bDebug){
			System.out.println("sResultado: " + sResultado);
		}
		return sResultado;
	} // sGetPantallaJQ
	

	
	public String sGetSelectOptions(HashMap<String, Object> map) throws SQLException{

		int iIdEmpresa 			= Integer.parseInt((String)map.get("DYN_CTL_ID_EMPRESA"));
		int iIdListado 			= Integer.parseInt((String)map.get("DYN_CTL_ID_LISTADO"));
		String sParentValue		= (String)map.get("DYN_CTL_PARENT_VALUE");
		String sParentKey		= (String)map.get("CTL_PARENT_KEY");
		String sUserSchema		= (String)map.get("DYN_CTL_USUARIO_SCHEMA");
		String sFilter			= (String)map.get("DYN_CTL_VAL_INPUT_FILTER"); 
		String sUseGetRecord	= (String)map.get("DYN_CTL_THIS_IS_GET_RECORD");
		JsonArray jaRecord		= (JsonArray)map.get("DYN_CTL_RECORD");
		boolean bUseGetRecord	= (boolean)(sUseGetRecord!=null&&sUseGetRecord.equals("YES"))?true:false;
		String sQuery 			= ""; 
		String sParameter 		= "";
		String sParameters 		= "";
		String sResultParams	= "";
		String sOpciones		= "";
		String sQryGetRecord	= "";
		String sParamsGetRecord	= "";
		boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
		boolean bSaveAndNotRegen= false;
		boolean bExecToQry		= false;
		boolean bRegenOpt		= false;
		StringTokenizer tokens  = null; 
		int i 					= 0;
		
		if(sParentValue!=null&&sParentKey!= null){
			if(map.containsKey(sParentKey)){
				map.replace(sParentKey, sParentValue);
			}else{
				map.put(sParentKey, sParentValue);
			}
		}
		
		if(bDebug){
			System.out.println(" sFilter ----->>>>>>>>>>>>>>>>>>>  " + sFilter);
		}
		
		sSentenciaSql = 
				" 	SELECT  REPLACE(REPLACE(REPLACE(QUERY_OPCIONES, '@ESQUEMA@', ?),'@DBLINK@', ''), '@ID_EMPRESA@', ?) AS QUERY_OPCIONES,	\n " +
				"			NVL(B_VALIDA_ETIQUETA,'F') AS B_VALIDA_ETIQUETA, QUERY_JAVA_VARS, 					\n " +
				"			NVL(B_EXEC_TO_GET_QUERY,'F') AS B_EXEC_TO_GET_QUERY, 								\n " +
				"			CASE WHEN NVL(B_SAVE_AND_NOT_REGEN,'F') = 'V' THEN 'V'  							\n " +
				"				ELSE B_REGENERA_OPCIONES END AS B_REGENERA_OPCIONES,							\n " +
				"			CASE WHEN OPCIONES IS NOT NULL AND B_REGENERA_OPCIONES = 'F' THEN 					\n " +
				"				OPCIONES ELSE NULL END AS OPCIONES, QUERY_PARAMETERS, QUERY_RESULT_PARAMETERS, 	\n " +
				"           NVL(B_SAVE_AND_NOT_REGEN,'F') AS B_SAVE_AND_NOT_REGEN, NUM_MAX_OPTION_FEED, 		\n " +
				"			QUERY_GET_RECORD, PARAMS_GET_RECORD													\n " +
                "	FROM    AOS_ARQ_CAT_LISTADO_VALORES_JQ 	\n " +
                "	WHERE   ID_EMPRESA  = ?  				\n " +
                "    	AND ID_LISTADO  = ? 				\n " +
                "    	AND SIT_LISTADO = 'AC'  			\n " ;

		if(bDebug){
			System.out.println("  QUERY sGetSelectOptions Obtiene query EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
				" usuario.sSchema: 	" + sUserSchema 	+ "\n" +
				" iIdEmpresa: 		" + iIdEmpresa 	+ "\n" +
				" iIdListado: 		" + iIdListado 	+ "\n");
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		
		pstmt.setString(1, sUserSchema);
		pstmt.setInt(2, iIdEmpresa);
		pstmt.setInt(3, iIdEmpresa);
		pstmt.setInt(4, iIdListado);
		
		rs = pstmt.executeQuery();

		if(rs.next()){
			if(bDebug){
				System.out.println("  TIENE RS ----->>>>>>>>>>>>>>>>>>>  ");
			}
			sQuery 				= rs.getString("QUERY_OPCIONES");
			//bStickers			= rs.getString("B_VALIDA_ETIQUETA").equals("V");
			bExecToQry			= rs.getString("B_EXEC_TO_GET_QUERY").equals("V");
			bRegenOpt			= rs.getString("B_REGENERA_OPCIONES").equals("V");
			bSaveAndNotRegen	= rs.getString("B_SAVE_AND_NOT_REGEN").equals("V");
			sParameters 		= rs.getString("QUERY_PARAMETERS");
			sResultParams		= rs.getString("QUERY_RESULT_PARAMETERS");
			sOpciones			= rs.getString("OPCIONES");
			int iNumMaxOptFeed 	= rs.getInt("NUM_MAX_OPTION_FEED");
			sQryGetRecord		= rs.getString("QUERY_GET_RECORD");
			sParamsGetRecord	= rs.getString("PARAMS_GET_RECORD");
			
			if(sFilter!=null){
				Object[] params = new Object[3];
				String sClean 	= "%" + StringUtils.stripAccents(sFilter.replace(" ", "%").toLowerCase()) + "%"; //.replaceAll("\\p{M}", "")
				if(bDebug){
					System.out.println(" sClean ----->>>>>>>>>>>>>>>>>>>" + sClean + "<<<<<<");
					System.out.println(" sQuery ----->>>>>>>>>>>>>>>>>>>" + sQuery + "<<<<<<");
					System.out.println(" iNumMaxOptFeed ----->>>>>>>>>>>>>>>>>>>" + iNumMaxOptFeed + "<<<<<<");
				}
				params[0] 		= Integer.valueOf(iIdEmpresa);
				params[1] 		= sClean;
				params[2] 		= Integer.valueOf(iNumMaxOptFeed);
				sSentenciaSql 	= sQuery;
				sOpciones 		= resultSetToJson(conn, sSentenciaSql, params);
			}else{
				
				if(bUseGetRecord&&sQryGetRecord!=null){
					sQuery 		= sQryGetRecord; 
					sParameters = sParamsGetRecord; 
				}
			
				if(bRegenOpt){
					
					if(bDebug){
						System.out.println("  REGENERA----->>>>>>>>>>>>>>>>>>>  ");
					}
					
					if(sQuery != null && !sQuery.equals("")){
						
						if(bDebug){
							System.out.println("  PASO 1 ----->>>>>>>>>>>>>>>>>>>  ");
						}

						Object[] params = new Object[0];
						sSentenciaSql 	= sQuery;
						if(sParameters!=null){
							tokens 		= new StringTokenizer(sParameters, ",");
						}
						

						if(bDebug){
							System.out.println("  QUERY resultado nuevo getComboOptionsMap EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
						}
						
						if(bExecToQry){
							
							pstmt = conn.prepareStatement(sSentenciaSql);
							
							if(tokens!=null){
								if(bDebug){
									System.out.println(" sParameters tokens size EN DAO ----->>>>>>>>>>>>>>>>>>> " + tokens.countTokens());
								}
								
								while(tokens.hasMoreTokens()){
									sParameter = tokens.nextToken();
									String[] sSplit = sParameter.split(Pattern.quote("|"));
									
									if(sSplit[0].equals("VARCHAR")){
										pstmt.setString(i, (String)map.get(sSplit[1]));
									}else{
										if(sSplit[0].equals("FLOAT")){
											pstmt.setFloat(i, Float.valueOf((String)map.get(sSplit[1])));
										}else{
											if(sSplit[0].equals("INT")){
												if(!map.containsKey(sSplit[1])){
													pstmt.setInt(i, Integer.valueOf((String)jaRecord.get(0).getAsJsonObject().get(sSplit[1]).getAsString()));
												}else{
													pstmt.setInt(i, Integer.valueOf((String)map.get(sSplit[1])));
												}
											}else{
												if(sSplit[0].equals("DATE")){
											
													SimpleDateFormat formatter = new SimpleDateFormat(sSplit[2]);
											        String dateInString = sSplit[1];
		
											        try {
											            Date date = formatter.parse(dateInString);
											            pstmt.setDate(i, java.sql.Date.valueOf((String)map.get(sSplit[1])));
											        } catch (ParseException e) {
											            e.printStackTrace();
											        }
												}
											}
										}
									}
									if(bDebug){
										System.out.println("  Agrega el parametro en el stmt: " + sParameter);
									}
									i = i + 1;
								}
							}
							
							rs = pstmt.executeQuery();
							if(rs.next()){
								sSentenciaSql 	= rs.getString(1);
								if(sResultParams!=null){
									tokens 		= new StringTokenizer(sResultParams, ",");
								}else{
									tokens 		= null; 
								}
								if(bDebug){
									System.out.println("Sentencia final stmt: \n " + sSentenciaSql);
								}	
							}
						}


						
						if(tokens!=null){
							
							if(bDebug){
								System.out.println(" sParametersResult tokens size EN DAO ----->>>>>>>>>>>>>>>>>>> " + tokens.countTokens());
							}
							
							params = new Object[tokens.countTokens()];
							i = 0; 
							while(tokens.hasMoreTokens()){
								sParameter = tokens.nextToken();
								String[] sSplit = sParameter.split(Pattern.quote("|"));
								if(bDebug){
									System.out.println(" sSplit tipo: " + sSplit[0] + " Columna: " + sSplit[1]);
								}	
								
								if(sSplit[0].equals("VARCHAR")){
									if(!map.containsKey(sSplit[1])){
										if(jaRecord.get(0).getAsJsonObject().get(sSplit[1])==null){
											String sStr = null;
											params[i] = sStr;
										}else{
											params[i] = (String)jaRecord.get(0).getAsJsonObject().get(sSplit[1]).getAsString();
										}
									}else{
										params[i] = (String)map.get(sSplit[1]);
									}
								}else{
									if(sSplit[0].equals("FLOAT")){
										if(!map.containsKey(sSplit[1])){
											if(jaRecord.get(0).getAsJsonObject().get(sSplit[1])==null){
												Float vFloat = null;
												params[i] = vFloat;
											}else{
												params[i] = Float.valueOf((String)jaRecord.get(0).getAsJsonObject().get(sSplit[1]).getAsString());
											}
										}else{
											params[i] = Float.valueOf((String)map.get(sSplit[1]));
										}
									}else{
										if(sSplit[0].equals("INT")){
											if(!map.containsKey(sSplit[1])){
												if(jaRecord.get(0).getAsJsonObject().get(sSplit[1])==null){
													Integer vInt = null;
													params[i] = vInt;
												}else{
													params[i] = Integer.valueOf((String)jaRecord.get(0).getAsJsonObject().get(sSplit[1]).getAsString());
												}
											}else{
												params[i] = Integer.valueOf((String)map.get(sSplit[1]));
											}
										}else{
											if(sSplit[0].equals("DATE")){
										
												if(!map.containsKey(sSplit[1])){
													if(jaRecord.get(0).getAsJsonObject().get(sSplit[1])==null){
														java.sql.Date vDate = null;
														params[i] = vDate;
													}else{
														params[i] = java.sql.Date.valueOf(jaRecord.get(0).getAsJsonObject().get(sSplit[1]).getAsString());
													}
												}else{
													//SimpleDateFormat formatter = new SimpleDateFormat(sSplit[2]);
													//String dateInString = sSplit[1];
													//try {
											        	//Date date = formatter.parse(dateInString);
											            params[i] = java.sql.Date.valueOf((String)map.get(sSplit[1]));
										            //} catch (ParseException e) {
										            //		e.printStackTrace();
										            //}
												}
											}
										}
									}
								}
								if(bDebug){
									System.out.println("  Agrega el parametro en el stmt: " + sParameter);
								}	
								i = i + 1;
							}
							
						}
							
						sOpciones 		= resultSetToJson(conn, sSentenciaSql, params);
					}else{
						if(bDebug){
							System.out.println("  PASO 2 ----->>>>>>>>>>>>>>>>>>>  ");
						}
						Object[] params = new Object[2];
						params[0] = Integer.valueOf(iIdEmpresa);
						params[1] = Integer.valueOf(iIdListado);
						sSentenciaSql = 
								" 	SELECT	VALOR AS I, ETIQUETA_VALOR AS V, ID_ORDEN AS O		\n " +
						        "   FROM    AOS_ARQ_CAT_LISTADO_VAL_DET_JQ						\n " +
						        "   WHERE   ID_EMPRESA = ?										\n " +
						        "       AND ID_LISTADO = ?										\n " +
						        "       AND SIT_VALOR = 'AC'									\n " +
				                "	ORDER BY ID_ORDEN  											\n ";
						
						sOpciones 		= resultSetToJson(conn, sSentenciaSql, params);
					}
				}
			}
			
			if(bDebug){
				System.out.println(" bSaveAndNotRegen: " + bSaveAndNotRegen);
				System.out.println(" sOpciones: " + sOpciones);
			}	
			
			if(bSaveAndNotRegen && sOpciones!=null){
			
				if(bDebug){
					System.out.println(" Entra: ");
				}
				
				Calendar cal = Calendar.getInstance();
				Timestamp  timestamp = new java.sql.Timestamp(cal.getTimeInMillis());
				
				sSentenciaSql = 
						" 	UPDATE  AOS_ARQ_CAT_LISTADO_VALORES_JQ SET  	\n " +
						"           B_SAVE_AND_NOT_REGEN	= NULL,			\n " +
						"           B_REGENERA_OPCIONES		= 'F', 			\n " +
						"           OPCIONES				= ?, 			\n " +
						"			FH_LAST_SAVED			= ?				\n " +
		                "	WHERE   ID_EMPRESA  = ?  						\n " +
		                "    	AND ID_LISTADO  = ? 						\n " ;

				if(bDebug){
					System.out.println("  QUERY sGetSelectOptions Actualiza listado query EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
						" iIdEmpresa: 		" + iIdEmpresa 	+ "\n" +
						" iIdListado: 		" + iIdListado 	+ "\n");
				}
				
				pstmt = conn.prepareStatement(sSentenciaSql);
				
				pstmt.setString(1, sOpciones);
				pstmt.setTimestamp(2, timestamp);
				pstmt.setInt(3, iIdEmpresa);
				pstmt.setInt(4, iIdListado);
				
				rs = pstmt.executeQuery();
				
			}
			
		}
			
		rs.close();
	    pstmt.close();
		return sOpciones;
	} // sGetSelectOptions
	
	public JsonObject sGetTableRsDetail(HashMap<String, Object> map) throws SQLException{

		int iIdEmpresa 			= Integer.parseInt((String)map.get("DYN_CTL_ID_EMPRESA"));
		int iIdTabla			= Integer.valueOf((String)map.get("DYN_CTL_ID_TABLA"));
		int iIdRol				= Integer.valueOf((String)map.get("DYN_CTL_ID_ROL"));
		JsonArray jaJasonArrayP	= (JsonArray)map.get("DYN_CTL_RECORD"); // parent record
		boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
		String sResultado		= ""; 
		String sAttributesPK	= "";
		DatabaseMetaData mtdata = conn.getMetaData();
		String sDBProduct 		= mtdata.getDatabaseProductName();

		Gson gson 				= new Gson();
		JsonArray jaJasonArray 	= new JsonArray();
		JsonArray jaJasonArrayT	= new JsonArray();
		JsonObject joObj 		= new JsonObject();
		Object[] params			= null;
		
		sSentenciaSql = 
		
					"SELECT  T.TITULO_PANTALLA AS A, NVL(T.VISTA_CONSULTA,OT.TABLA) AS B, 					\n " +  
					"        T.USER_FILTER_FIELD AS C, NVL(T.B_RS_SHOW_EDIT_BUTTON,'V') AS D, 				\n " +
					"		 T.ADDITIONAL_FILTERS AS F, T.ID_TABLA AS H, T.GET_RECORD_CODE AS I,			\n " + 
					"        CASE WHEN T.SHOW_BUTTON_CREATE_FROM_RESULT = 'V' THEN 'V' END  AS G,			\n " +
					"		 T.RS_TYPE AS TP																\n " +
					"FROM    AOS_ARQ_PANTALLA_TABLA	T														\n " + 
					"    LEFT JOIN AOS_ARQ_TABLA OT															\n " +
					"		ON  T.ID_TABLA_ORIGEN   = OT.ID_TABLA											\n " +
					"WHERE   T.ID_TABLA 	= ?																\n " + 
					"    AND T.SIT_TABLA 	= 'AC'															\n " ;
		

		params 			= new Object[1];
		params[0] 		= Integer.valueOf(iIdTabla);
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		jaJasonArrayT  	= gson.fromJson(sResultado, JsonArray.class);
		
		String sNomTablaVista 	= jaJasonArrayT.get(0).getAsJsonObject().get("B")!=null?jaJasonArrayT.get(0).getAsJsonObject().get("B").getAsString():"";
		String sAdditFilters  	= jaJasonArrayT.get(0).getAsJsonObject().get("F")!=null?jaJasonArrayT.get(0).getAsJsonObject().get("F").getAsString():"";
		String sColumnasPK 		= "";
		
		jaJasonArrayT.get(0).getAsJsonObject().remove("B");
		jaJasonArrayT.get(0).getAsJsonObject().remove("C");
		jaJasonArrayT.get(0).getAsJsonObject().remove("F");
		
		joObj.add("datosBoton", jaGetJQButtons(iIdTabla, iIdRol, "T", bDebug));
		
		
		if(bDebug){
			System.out.println("jaJasonArray Datos Tabla: " + jaJasonArrayT.toString());
		}
		
		sSentenciaSql = 
					"SELECT  A.ATRIBUTO AS A, A.TIPO_DATO AS B, A.ETIQUETA AS C, A.ID_ORDEN_ATRIBUTO AS O, A.ORDEN_PK AS D, \n " +
					"        A.DATE_FORMAT AS F, A.TIPO_ATRIBUTO AS G, A.H_ALIGN AS H, A.DATE_FORMAT_MYSQL AS I, 			\n " +
					"		 A.TD_WIDTH AS J										\n " +
					"FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A							\n " +
					"    LEFT JOIN AOS_SECU_ROL_PANT_TABLA_EX_CAM E   				\n " +
					" 		 ON  E.ID_ATTRIBUTE = A.ID_ATRIBUTO 					\n " +
					"		 AND E.ID_ROL		= ? 								\n " +
					"WHERE   A.ID_TABLA 		= ?									\n " +
                    "    AND A.TIPO_ATRIBUTO 	= 'VIEW'							\n " +
                    "    AND A.SIT_ATRIBUTO 	= 'AC' 								\n " +
                    "    AND NOT(E.ID_ATTRIBUTE IS NOT NULL AND E.B_SELECT = 'F') 	\n " +
                    "ORDER BY ID_ORDEN_ATRIBUTO 									\n ";
		
		if(bDebug){
			System.out.println("  QUERY sGetTableRsDetail Obtiene Atributos EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdRol: 		" + iIdRol 		+ "\n" + 
					" iIdTabla: 	" + iIdTabla 	+ "\n");
		}

		params 			= new Object[2];
		params[0] 		= Integer.valueOf(iIdRol);
		params[1] 		= Integer.valueOf(iIdTabla);
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);

		String sColumnas 	= "";
		
		int iAlias 			= 0;
		int iNumObjetcts 	= jaJasonArray.size();
		
		for (int i = 0, size = jaJasonArray.size(); i < size; i++){

			String sAttribueName 	= jaJasonArray.get(i).getAsJsonObject().get("A").getAsString(); 
			String sDataType		= jaJasonArray.get(i).getAsJsonObject().get("B").getAsString();
			String sDateFormat		=(jaJasonArray.get(i).getAsJsonObject().get("F")!=null?jaJasonArray.get(i).getAsJsonObject().get("F").getAsString():"");
			String sDateFormatMySql	=(jaJasonArray.get(i).getAsJsonObject().get("I")!=null?jaJasonArray.get(i).getAsJsonObject().get("I").getAsString():"");
			
			if(sDBProduct.equals("MySQL")){
				sColumnas 	= sColumnas + (sColumnas.equals("")?"":", ") +
						// If data type DATE we get the format date
						((sDataType.equals("DATE")&&!sDateFormatMySql.equals(""))?("DATE_FORMAT(" + sAttribueName + ", '" + sDateFormatMySql + "')"):(sAttribueName)) + 
							" AS \"" + iAlias + "\"";
			}else{
				sColumnas 	= sColumnas + (sColumnas.equals("")?"":", ") +
							// If data type DATE we get the format date
							((sDataType.equals("DATE")&&!sDateFormat.equals(""))?("TO_CHAR(" + sAttribueName + ", '" + sDateFormat + "')"):(sAttribueName)) + 
								" AS \"" + iAlias + "\"";
			}
			jaJasonArray.get(i).getAsJsonObject().remove("A");
			jaJasonArray.get(i).getAsJsonObject().remove("B");
			jaJasonArray.get(i).getAsJsonObject().remove("D");
			jaJasonArray.get(i).getAsJsonObject().remove("F");
			jaJasonArray.get(i).getAsJsonObject().remove("G");
			jaJasonArray.get(i).getAsJsonObject().remove("I");
			iAlias = iAlias + 1;
		}
		
		joObj.add("datosAtribs", jaJasonArray);
		
		
		sSentenciaSql = 
				"SELECT  A.ATRIBUTO AS A, A.TIPO_DATO AS B, A.ORDEN_PK AS D, A.DATE_FORMAT AS F,\n " +
				"        A.DATE_FORMAT_MYSQL AS I												\n " +
				"FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A											\n " +
				"WHERE   A.ID_TABLA 		= ?													\n " +
                "    AND A.TIPO_ATRIBUTO 	= 'ALT_MOD'											\n " +
                "    AND A.SIT_ATRIBUTO 	= 'AC' 												\n " +
                "    AND A.ORDEN_PK IS NOT NULL 												\n " + 
                "ORDER BY A.ORDEN_PK 															\n ";
	
		if(bDebug){
			System.out.println("  QUERY sGetTableRsDetail Obtiene Atributos PK EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdTabla: 	" + iIdTabla 	+ "\n");
		}
	
		params 			= new Object[1];
		params[0] 		= Integer.valueOf(iIdTabla);
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);

		for (int i = 0, size = jaJasonArray.size(); i < size; i++){
			String sAttribueName 	= jaJasonArray.get(i).getAsJsonObject().get("A").getAsString(); 
			String sDataType		= jaJasonArray.get(i).getAsJsonObject().get("B").getAsString();
			String sDateFormat		=(jaJasonArray.get(i).getAsJsonObject().get("F")!=null?jaJasonArray.get(i).getAsJsonObject().get("F").getAsString():"");
			String sDateFormatMySql	=(jaJasonArray.get(i).getAsJsonObject().get("I")!=null?jaJasonArray.get(i).getAsJsonObject().get("I").getAsString():"");
			
			if(sDBProduct.equals("MySQL")){
				sColumnasPK =  sColumnasPK + (sColumnasPK.equals("")?"CONCAT(":",'^$',") + 
						((sDataType.equals("DATE")&&!sDateFormatMySql.equals(""))?("DATE_FORMAT(" + sAttribueName + ", '" + sDateFormatMySql + "')"):sAttribueName);
				sAttributesPK = sAttributesPK + (sAttributesPK.equals("")?"":"|") + sAttribueName + "!" + sDataType + (!sDateFormatMySql.equals("")?"!" + sDateFormatMySql:"");
			}else{
				sColumnasPK =  sColumnasPK + (sColumnasPK.equals("")?"":"||'^$'||") + 
						((sDataType.equals("DATE")&&!sDateFormat.equals(""))?("TO_CHAR(" + sAttribueName + ", '" + sDateFormat + "')"):sAttribueName);
				sAttributesPK = sAttributesPK + (sAttributesPK.equals("")?"":"|") + sAttribueName + "!" + sDataType + (!sDateFormat.equals("")?"!" + sDateFormat:"");
			}
		}
		
		if(sDBProduct.equals("MySQL")){
			sColumnasPK =  sColumnasPK + ",'')";
		}

		if(bDebug){
			System.out.println("  sAttributesPK ----->>>>>>>>>>>>>>>>>>>" + sAttributesPK);
		}
		//We add the way to retrieve the key attributes for the records 
		JsonElement jeElement = new JsonParser().parse(sAttributesPK);
		jaJasonArrayT.get(0).getAsJsonObject().add("Z", jeElement);
		joObj.add("datosTabla", jaJasonArrayT);
		
		//Get the attributes to filter the query for detail records 
		sSentenciaSql =	"	SELECT  M.ATRIBUTO_MAESTRO AS B, M.ATRIBUTO_DETALLE AS C, 								\n" +
						"			M.DATE_FORMAT_MASTER AS D, M.DATE_FORMAT_DETAIL AS E, M.TIPO_DATO_DETAIL AS F,	\n" + 
						"			M.DATE_FORMAT_MYSQL_MASTER AS G, M.DATE_FORMAT_MYSQL_DETAIL AS H				\n" +
						"	FROM    AOS_ARQ_MASTER_DETAIL_V M														\n" + 	
						"	WHERE   M.ID_TABLA_DETALLE      = ?														\n" ; 
			
		params 		= new Object[1];
		params[0] 	= (Integer)iIdTabla;    
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado campos filtro para detalle de Tabla: " + sResultado);
			System.out.println("jaJasonArrayP.length ----->>>>>>>>>>>>>>>>>>> " + jaJasonArrayP.size());
			System.out.println("jaJasonArrayP.length ----->>>>>>>>>>>>>>>>>>> \n" + jaJasonArrayP);
		}
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		String sFiltros 		= ""; 
		params 					= new Object[jaJasonArray.size()];
		String sAttributeDName 	= "";
		String sAttributeMName 	= "";
		String sDataType		= "";
		String sDateFormat 		= ""; 
		String sDateFormatMySql = "";
		
		for (int i = 0, size = jaJasonArray.size(); i < size; i++){
			
			sAttributeMName = jaJasonArray.get(i).getAsJsonObject().get("B").getAsString();
			sAttributeDName = jaJasonArray.get(i).getAsJsonObject().get("C").getAsString();
			sDataType 		= jaJasonArray.get(i).getAsJsonObject().get("F").getAsString();
			sDateFormat 	= (jaJasonArray.get(i).getAsJsonObject().get("E")!=null?jaJasonArray.get(i).getAsJsonObject().get("E").getAsString():"");
			sDateFormatMySql= (jaJasonArray.get(i).getAsJsonObject().get("H")!=null?jaJasonArray.get(i).getAsJsonObject().get("H").getAsString():"");
			sFiltros 		= sFiltros + "    AND " + sAttributeDName + " = ? \n";

			//sColumnasPK 	=  sColumnasPK + (sColumnasPK.equals("")?"":"||'^$'||") + 
			//					((sDataType.equals("DATE")&&sDateFormat!=null&&!sDateFormat.equals(""))?
			//					("TO_CHAR(" + sAttributeName + ", '" + sDateFormat + "')"):sAttributeName);

			if(bDebug){
				System.out.println("sAttributeMName ----->>>>>>>>>>>>>>>>>>> " + sAttributeMName);
				System.out.println("sAttributeDName ----->>>>>>>>>>>>>>>>>>> " + sAttributeDName);
				System.out.println("sDataType ----->>>>>>>>>>>>>>>>>>> " + sDataType);
				System.out.println("sDateFormat ----->>>>>>>>>>>>>>>>>>> " + sDateFormat);
				System.out.println("sDateFormatMySql ----->>>>>>>>>>>>>>>>>>> " + sDateFormatMySql);
			}
			
			switch (sDataType){
				case "NUMBER": 
					if(sAttributeDName.equals("ID_EMPRESA")){
						params[i]  = Integer.valueOf(iIdEmpresa);
					}else{
						params[i]  = Float.valueOf(jaJasonArrayP.get(0).getAsJsonObject().get(sAttributeMName).getAsString());
					}
					break;
				case "VARCHAR": 
					params[i]  = jaJasonArrayP.get(0).getAsJsonObject().get(sAttributeMName).getAsString();
					break;
				case "DATE": 
			        try {
						SimpleDateFormat formatter 	= new SimpleDateFormat(sDateFormat);
			            Date date 					= formatter.parse(jaJasonArrayP.get(0).getAsJsonObject().get(sAttributeMName).getAsString());
			            long time   				= date.getTime(); 
			            Timestamp timestamp 		= new Timestamp(time); 
			            params[i]  					= timestamp;
			            if(bDebug){
							System.out.println("Fecha parseada: >" + timestamp.toString() + "<");
						}
			        } catch (ParseException e) {
			            e.printStackTrace();
			        }
		            break;
			}
		}

		if(!(sColumnasPK==null)){
			sColumnasPK = sColumnasPK + " AS \"" + (iNumObjetcts + 1) + "\"";
		}

		if(bDebug){
			System.out.println("sColumnas: " + sColumnas);
			System.out.println("sColumnasPK: " + sColumnasPK);
			System.out.println("datosAtributos: " + jaJasonArray.toString());
		}

		sSentenciaSql = "SELECT " + sColumnas 		+ ", " + sColumnasPK + " \n " + 
						"FROM 	" + sNomTablaVista 	+ " \n " +
						"WHERE 	1=1  					\n " + sAdditFilters + " \n " +
						sFiltros; 

		if(bDebug){
			System.out.println("  QUERY sGetTableRsDetail sSentenciaSql RS EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n");
		}
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		joObj.add("datosRs", jaJasonArray);

		if(bDebug){
			System.out.println("sResultado rs "  + iIdTabla + ": " + sResultado);
		}

		sResultado = joObj.toString();

		if(bDebug){
			System.out.println("sResultado rs" +  iIdTabla + ": " + sResultado);
		}
		return joObj;
	} // sGetTableRsDetail
	
	public String sGetTableRs(HashMap<String, Object> map) throws SQLException{

		HashMap<String, Object> maplLocal = map;
		Usuario  usuario 		= (Usuario)maplLocal.get("DYN_CTL_USUARIO");
		int iIdTabla			= Integer.parseInt((String)maplLocal.get("DYN_CTL_ID_TABLA"));
		boolean bDebug			= (boolean)maplLocal.get("DYN_CTL_BDEBUG");
		int iIdRol				= usuario.iIdRol;
		String sResultado		= ""; 
		DatabaseMetaData mtdata = conn.getMetaData();
		String sDBProduct 		= mtdata.getDatabaseProductName();
		
		Gson gson 				= new Gson();
		JsonArray jaJasonArray 	= new JsonArray();
		JsonArray jaJasonArrayT	= new JsonArray();
		JsonObject joObj 		= new JsonObject();
		Object[] params			= null;
		List<Integer> lLista 	= new ArrayList<Integer>();
		List<String[]> slLista 	= new ArrayList<String[]>();
		String sAttributesPK	= "";
		
		//maplLocal.values().remove("");
		
		sSentenciaSql = 
			"SELECT  T.TITULO_PANTALLA AS A, T.B_PRESENTA_LINK_CONSULTA AS B, T.B_PRESENTA_EXP_EXCEL AS C, NVL(T.VISTA_CONSULTA,OT.TABLA) AS D, 					\n " + 
			"        T.USER_FILTER_FIELD AS E, CASE WHEN T.B_RS_SHOW_EDIT_BUTTON = 'V' THEN 'V' END AS F, T.IS_UPDATE_DISABLED AS G, 								\n " + 
			"        T.IS_INSERT_DISABLED AS H, T.IS_DELETE_DISABLED AS I, T.PAGE_SPECIFIC_PLUGIN_SCRIPTS AS J, T.ADDITIONAL_FILTERS AS K, 							\n " + 
			"        CASE WHEN T.SHOW_BUTTON_CREATE = 'V' THEN 'V' END AS L, CASE WHEN T.SHOW_BUTTON_UPDATE = 'V' THEN 'V' END AS M, 								\n " +
			"		 CASE WHEN T.SHOW_BUTTON_BACK_CREATE = 'V' THEN 'V' END AS N, CASE WHEN T.SHOW_BUTTON_BACK_UPDATE = 'V' THEN 'V' END AS O, 						\n " +
			"		 CASE WHEN T.SHOW_BUTTON_ALTA_BUSQ = 'V' THEN 'V' END AS P, CASE WHEN T.SHOW_BUTTON_SEARCH = 'V' THEN 'V' END AS Q,								\n " + 
			"        CASE WHEN T.SHOW_BUTTON_CLEAR = 'V' THEN 'V' END AS R, CASE WHEN T.SHOW_BUTTON_CREATE_FROM_RESULT = 'V' THEN 'V' END AS T, T.ID_TABLA AS V, 	\n " + 
			"		 NVL(NUM_RECORDS_LIMIT_RS,(SELECT R.VALOR_CONCEPTO FROM AOS_ARQ_REGISTRO R WHERE R.ID_EMPRESA = T.ID_EMPRESA AND R.ID_REGISTRO = 2007)) AS W,	\n " +
			"		 T.B_RS_SHOW_CHECKBOX AS X, P.ADDITIONAL_PLUGIN AS Y, T.RS_TYPE AS TP																			\n " +
			"FROM    AOS_ARQ_PANTALLA_TABLA	T					\n " +
			" 	 JOIN AOS_ARQ_TIPO_OPERACION_DESC O 			\n " +
			" 		 ON  O.ID_EMPRESA 		= T.ID_EMPRESA		\n " +
			" 		 AND O.CVE_OPERACION 	= 'T' 				\n " +
			"  	 LEFT JOIN AOS_ARQ_PANTALLA_TABLA_ADDP P 		\n " +
			"       ON 	P.ID_TABLA 			= T.ID_TABLA 		\n " +
			"       AND P.ID_EMPRESA 		= O.ID_EMPRESA		\n " +
			"       AND P.CVE_OPERACION 	= O.CVE_OPERACION	\n " +
			"       AND P.SIT_PLUGIN 		= 'AC' 				\n " +
			" 	 JOIN AOS_ARQ_TABLA OT   						\n " +
			"        ON T.ID_TABLA_ORIGEN = OT.ID_TABLA 		\n " +
			"WHERE   T.ID_TABLA 	= ?							\n " + 
			"    AND T.SIT_TABLA 	= 'AC'						\n " ; 

		
		params 			= new Object[1];
		params[0] 		= Integer.valueOf(iIdTabla);
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		jaJasonArrayT  	= gson.fromJson(sResultado, JsonArray.class);
		
		String sNomTablaVista 	= jaJasonArrayT.get(0).getAsJsonObject().get("D")!=null?jaJasonArrayT.get(0).getAsJsonObject().get("D").getAsString():"";
		String sAdditFilters  	= jaJasonArrayT.get(0).getAsJsonObject().get("K")!=null?jaJasonArrayT.get(0).getAsJsonObject().get("K").getAsString():"";
		String sNumMaxRS		= jaJasonArrayT.get(0).getAsJsonObject().get("W").getAsString();
		
		jaJasonArrayT.get(0).getAsJsonObject().remove("D");
		jaJasonArrayT.get(0).getAsJsonObject().remove("K");
		
		joObj.add("datosBoton", jaGetJQButtons(iIdTabla, iIdRol, "T", bDebug));
		
		if(bDebug){
			System.out.println("jaJasonArray Datos Tabla: " + jaJasonArrayT.toString());
		}
		
		sSentenciaSql = 
					"SELECT  A.ATRIBUTO AS A, A.TIPO_DATO AS B, A.ETIQUETA AS C, A.ID_ORDEN_ATRIBUTO AS O, A.ORDEN_PK AS D,  \n " +
					"        CASE WHEN A.SIT_ATRIBUTO = 'AC' THEN NULL ELSE A.SIT_ATRIBUTO END AS E, A.DATE_FORMAT AS F, A.TIPO_ATRIBUTO AS G, " +
					"		 A.H_ALIGN AS H, A.DATE_FORMAT_MYSQL AS I, A.TD_WIDTH AS J				\n " +
					"FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A							\n " +
					"    LEFT JOIN AOS_SECU_ROL_PANT_TABLA_EX_CAM E   				\n " +
					" 		 ON  E.ID_ATTRIBUTE = A.ID_ATRIBUTO 					\n " +
					"		 AND E.ID_ROL		= ? 								\n " +
					"WHERE   A.ID_TABLA 		= ?									\n " +
                    "    AND A.TIPO_ATRIBUTO IN ('VIEW','FILTRO')					\n " +
                    "    AND (A.SIT_ATRIBUTO = 'AC' OR A.ORDEN_PK IS NOT NULL)		\n " +
                    "    AND NOT(E.ID_ATTRIBUTE IS NOT NULL AND E.B_SELECT = 'F') 	\n " +
                    "ORDER BY ID_ORDEN_ATRIBUTO \n ";

		
		
		if(bDebug){
			System.out.println("  QUERY sGetTableRs Obtiene Atributos EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdTabla: 		" + iIdTabla 	+ "\n");
		}

		params 			= new Object[2];
		params[0] 		= Integer.valueOf(iIdRol);
		params[1] 		= Integer.valueOf(iIdTabla);
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);

		String sColumnas 	= "";
		String sColumnasPK 	= ""; 
		String sOperador 	= ""; 
		String sTipoDato	= "";
		
		int iAlias 			= 0;
		int iElem  			= 0; 
		int iNumObjetcts 	= 0;
		for (int i = 0, size = jaJasonArray.size(); i < size; i++){
			String sAttribueName 	= jaJasonArray.get(i).getAsJsonObject().get("A").getAsString(); 
			String sDataType		= jaJasonArray.get(i).getAsJsonObject().get("B").getAsString();
			String sDateFormat		=(jaJasonArray.get(i).getAsJsonObject().get("F")!=null?jaJasonArray.get(i).getAsJsonObject().get("F").getAsString():"");
			String sDateFormatMySql	=(jaJasonArray.get(i).getAsJsonObject().get("I")!=null?jaJasonArray.get(i).getAsJsonObject().get("I").getAsString():"");
			
			if(jaJasonArray.get(i).getAsJsonObject().get("G").getAsString().equals("VIEW")){
				
				if((jaJasonArray.get(i).getAsJsonObject().get("D"))!=null){
					
					if(sDBProduct.equals("MySQL")){
						sColumnasPK =  sColumnasPK + (sColumnasPK.equals("")?"CONCAT(":",'^$',") + 
								((sDataType.equals("DATE")&&!sDateFormatMySql.equals(""))?("DATE_FORMAT(" + sAttribueName + 
										", '" + sDateFormatMySql + "')"):sAttribueName);
					}else{
						sColumnasPK =  sColumnasPK + (sColumnasPK.equals("")?"":"||'^$'||") + 
										((sDataType.equals("DATE")&&!sDateFormat.equals(""))?("TO_CHAR(" + sAttribueName + ", '" + 
												sDateFormat + "')"):sAttribueName);
					}
					sAttributesPK = sAttributesPK + (sAttributesPK.equals("")?"":"|") + sAttribueName + "!" + sDataType + 
							(!sDateFormat.equals("")?"!" + sDateFormat:""); 
				}
				if(jaJasonArray.get(i).getAsJsonObject().get("E")!=null){
					lLista.add(i);
				}else{
	
					if(sDBProduct.equals("MySQL")){
						sColumnas 	= sColumnas + (sColumnas.equals("")?"":", ") +
								// If data type DATE we get the format date
								((sDataType.equals("DATE")&&!sDateFormatMySql.equals(""))?
									("DATE_FORMAT(" + sAttribueName + ", '" + sDateFormatMySql + "')"): (sAttribueName)) + " AS \"" + iAlias + "\"";
					}else{
						sColumnas 	= sColumnas + (sColumnas.equals("")?"":", ") +
								// If data type DATE we get the format date
								((sDataType.equals("DATE")&&!sDateFormat.equals(""))?
									("TO_CHAR(" + sAttribueName + ", '" + sDateFormat + "')"): (sAttribueName)) + " AS \"" + iAlias + "\"";	
					}
					
					jaJasonArray.get(i).getAsJsonObject().remove("A");
					jaJasonArray.get(i).getAsJsonObject().remove("B");
					jaJasonArray.get(i).getAsJsonObject().remove("D");
					jaJasonArray.get(i).getAsJsonObject().remove("E");
					jaJasonArray.get(i).getAsJsonObject().remove("F");
					jaJasonArray.get(i).getAsJsonObject().remove("G");
					iAlias = iAlias + 1;
				}
			}else{
				// We get the filter parameters if any are in the map
				if(maplLocal.containsKey(sAttribueName)&&!((String)maplLocal.get(sAttribueName)).equals("")){
					if(bDebug){
						System.out.println("El mapa si contiene: " + sAttribueName);
					}
					
					sOperador = ""; 
					sTipoDato = (String)jaJasonArray.get(i).getAsJsonObject().get("B").getAsString(); 
					
					//Data type NUMBER or DATE
					if(sTipoDato.equals("DATE")||sTipoDato.equals("NUMBER")){
						if(bDebug){
							System.out.println("Es numero o fecha");
						}
						// The map contains the operator parameter
						if(maplLocal.containsKey("PRE_" + sAttribueName)){
							if(bDebug){
								System.out.println("Si contiene PRE");
							}	
							sOperador = (String)maplLocal.get("PRE_" + sAttribueName);
							
							// The operator parameter value is BETWEEN
							if(sOperador.equals("BETWEEN")) {
								if(bDebug){
									System.out.println("Es BETWEEN");
								}
								// The map has value of post parameter
								if(maplLocal.containsKey("POST_" + sAttribueName)){
									if(bDebug){
										System.out.println("Si contiene POST");
									}
									// We tell the number of parameters will increase by 1 
									iNumObjetcts = iNumObjetcts + 1; 
								}else{
									if(bDebug){
										System.out.println("No viene el POST");
									}
									// If post parameter is missing we use default = operator 
									sOperador = "=";
								}
							}
						}else{
							if(bDebug){
								System.out.println("No viene el PRE");
							}
							// If operator is missing we use default = operator
							sOperador = "=";
						}
					}
						
					if(bDebug){
						System.out.println("sOperador Antes de agregar: " + sOperador);
					}
					String[] sStrArray = {	sAttribueName,  //Attribute name
											sDataType,		//Attribute Data Type 
											sDateFormat, 	//Attribute date format
										    sOperador  		//Operator for dates and numbers
										}; 
					slLista.add(iElem, sStrArray); 
					iNumObjetcts = iNumObjetcts + 1; 
				}	
				lLista.add(i);
			}
		}
		
		if(sDBProduct.equals("MySQL")){
			sColumnasPK =  sColumnasPK + ",'')";
		}
		 
		if(lLista.size() > 0){
			
			Collections.reverse(lLista);

			for(int element : lLista) {
				jaJasonArray.remove(element);
			}
		}

		params 			= new Object[0];
		String sFiltros = ""; 

		if(slLista.size() > 0){
			params = new Object[iNumObjetcts];
			int ic = 0; 
			for(String[] sEelement : slLista) {
				
				if(bDebug){
					System.out.println("************************************************   FILTROS   **********************************************");
					System.out.println("Tipo de dato: >" + sEelement[1] + "<");
					System.out.println("Valores en lista: >" + sEelement[0] + "<, >" + sEelement[1] + "<, >" + sEelement[2] + "<");
					System.out.println("Valores en Object: >" + (String)maplLocal.get(sEelement[0]) + "<--nuevo");
				}
				sOperador = sEelement[3]!=null?sEelement[3]:" = ";
				if(bDebug){
					System.out.println("sOperador en case: >" + sOperador + "<");
				}
				
				switch (sEelement[1]){
					case "NUMBER": 
						if(sOperador.equals("BETWEEN")){
							sFiltros = sFiltros + " AND " + sEelement[0] + " BETWEEN ? AND ? \n";
							params[ic]  = Integer.parseInt((String)maplLocal.get(sEelement[0]));
							ic = ic + 1; 
							params[ic]  = Integer.parseInt((String)maplLocal.get("POST_" + sEelement[0]));
						}else{
							sFiltros = sFiltros + " AND " + sEelement[0] + " " + sOperador + " ? \n";
							params[ic]  = Integer.parseInt((String)maplLocal.get(sEelement[0]));
						}
						break;
					case "VARCHAR": 
						String sVal = (String)maplLocal.get(sEelement[0]);
						params[ic]  = "%" + StringUtils.stripAccents(sVal.replace(" ", "%").toLowerCase()) + "%";
						if(sDBProduct.equals("MySQL")){
							sFiltros = sFiltros + " AND " + sEelement[0] + "  LIKE  ? -- collate utf8_general_ci \n";
						}else{
							sFiltros = sFiltros + " AND TRANSLATE(LOWER(" + sEelement[0] + "), '�����������������','aaaeeeiiiooouuuyn') LIKE ? \n";	
						}
						
						break;
					case "DATE": 
				        try {
							if(sOperador.equals("BETWEEN")){
								sFiltros = sFiltros + " AND " + sEelement[0] + " BETWEEN ? AND ? \n";
								
								SimpleDateFormat formatter 	= new SimpleDateFormat(sEelement[2]);
					            Date date 					= formatter.parse((String)maplLocal.get(sEelement[0]));
					            long time   				= date.getTime(); 
					            Timestamp timestamp 		= new Timestamp(time); 
					            params[ic]  				= timestamp;
					            if(bDebug){
									System.out.println("Fecha 1 parseada: >" + timestamp.toString() + "<");
								}
					            ic = ic + 1; 
					            formatter 					= new SimpleDateFormat(sEelement[2]);
					            date 						= formatter.parse((String)maplLocal.get("POST_" + sEelement[0]));
					            time   						= date.getTime(); 
					            timestamp 					= new Timestamp(time); 
					            params[ic]  				= timestamp;
					            if(bDebug){
									System.out.println("Fecha 2 parseada: >" + timestamp.toString() + "<");
								}
							}else{
								sFiltros = sFiltros + " AND " + sEelement[0] + " " + sOperador + " ? \n";
								SimpleDateFormat formatter 	= new SimpleDateFormat(sEelement[2]);
					            Date date 					= formatter.parse((String)maplLocal.get(sEelement[0]));
					            long time   				= date.getTime(); 
					            Timestamp timestamp 		= new Timestamp(time); 
					            params[ic]  				= timestamp;
					            if(bDebug){
									System.out.println("Fecha parseada: >" + timestamp.toString() + "<");
								}
							}
				        	
				        } catch (ParseException e) {
				            e.printStackTrace();
				        }
			            break;
				}
				ic = ic + 1; 
			}
		}
		
		//We add the way to retrieve the key attributes for the records 
		JsonElement jeElement = new JsonParser().parse(sAttributesPK);
		jaJasonArrayT.get(0).getAsJsonObject().add("Z", jeElement);
		joObj.add("datosTabla", jaJasonArrayT);
		
		sColumnasPK = sColumnasPK + " AS \"" + (jaJasonArray.size() + 1) + "\"";
		if(bDebug){
			System.out.println("sColumnas: " + sColumnas);
			System.out.println("sColumnasPK: " + sColumnasPK);
			System.out.println("datosAtributos: " + jaJasonArray.toString());
		}
		joObj.add("datosAtribs", jaJasonArray);

		
		sSentenciaSql = "SELECT " + sColumnas 		+ ", " + sColumnasPK + " \n " + 
						"FROM 	" + sNomTablaVista 	+ " \n " +
						(sDBProduct.equals("MySQL")?"WHERE 1=1 ":"WHERE 	ROWNUM <= " + sNumMaxRS) + "	\n " + 
							sAdditFilters + " \n " +
						sFiltros +  " \n " +
						(sDBProduct.equals("MySQL")?" LIMIT " + sNumMaxRS + " ROWS EXAMINED 10000":"") + "	\n "; 

		if(bDebug){
			System.out.println("  QUERY sGetTableRs sSentenciaSql RS EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n");
		}
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		joObj.add("datosRs", jaJasonArray);

		if(bDebug){
			System.out.println("sResultado rs: " + sResultado);
		}

		sResultado = joObj.toString();

		if(bDebug){
			System.out.println("sResultado: " + sResultado);
		}
		return sResultado;
	}  // sGetTableRs

	public String sGetRecord(HashMap<String, Object> map) throws SQLException{

		String sParametros		= (String)map.get("DYN_CTL_PARAMETROS");
		boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
		if(bDebug){
			System.out.println("sParametros: " + sParametros);
		}
		String[] sConfig 		= sParametros.split(Pattern.quote("^$^"));
		int iIdTabla			= Integer.parseInt( sConfig[0]);
		String[] tokens 		= sConfig[1].split(Pattern.quote("^$"));
		String[] sColsPK 		= sConfig[2].split(Pattern.quote("|"));
		int iIdEmpresa 			= Integer.parseInt((String)map.get("DYN_CTL_ID_EMPRESA"));
		Usuario  usuario 		= (Usuario)map.get("DYN_CTL_USUARIO");
		String	sSchema			= usuario.sUserSchema;
		String sCveOperacion	= (String) map.get("DYN_CTL_TIPO_OPERACION");
		int iIdRol				= usuario.iIdRol;
		Object[] params 		= new Object[0];
		DatabaseMetaData mtdata = conn.getMetaData();
		String sDBProduct 		= mtdata.getDatabaseProductName();
		
		String sResultado 		= "";
		Gson gson 				= new Gson();
		JsonArray jaJasonArray 	= new JsonArray();
		JsonArray jaJasonArrayT	= new JsonArray();
		JsonArray jaJasonArrayR	= new JsonArray();
		JsonObject joObj 		= new JsonObject();
		JsonObject joObjList	= null;
		Integer iIdList  		= null;
		List<Integer> lLista 	= new ArrayList<Integer>();
		List<String> lListAtts 	= new ArrayList<String>();
		
		sSentenciaSql =	"SELECT	 A.TITULO_PANTALLA AS A, O.DESC_OPERACION AS B, A.IS_DELETE_DISABLED AS C, T.TABLA  AS D,		\n " + 
						"        CASE WHEN A.SHOW_BUTTON_UPDATE = 'V' THEN 'V' END AS F, A.ID_TABLA AS G, P.ADDITIONAL_PLUGIN AS H, 			\n " +
						"		 NVL(NVL(A.FONT_LABEL_COLOUR, E.FONT_LABEL_COLOUR),'#2679b5') AS L							 	\n " +
						"FROM    AOS_ARQ_PANTALLA_TABLA A				\n " +
						"	 JOIN AOS_ARQ_EMPRESA E 					\n " +
						"		 ON A.ID_EMPRESA = E.ID_EMPRESA 		\n " +
						" 	 JOIN AOS_ARQ_TIPO_OPERACION_DESC O 		\n " +
						" 		 ON O.ID_EMPRESA = A.ID_EMPRESA			\n " +
						" 		 AND O.CVE_OPERACION = ? 				\n " +
						"  	 JOIN AOS_ARQ_TABLA T  						\n " +
						"       ON 	A.ID_TABLA_ORIGEN = T.ID_TABLA 		\n " +
						"  	 LEFT JOIN AOS_ARQ_PANTALLA_TABLA_ADDP P 	\n " +
						"       ON 	P.ID_TABLA = A.ID_TABLA 			\n " +
						"       AND P.ID_EMPRESA = O.ID_EMPRESA			\n " +
						"       AND P.CVE_OPERACION = O.CVE_OPERACION	\n " +
						"       AND P.SIT_PLUGIN = 'AC' 				\n " +
						"WHERE   A.ID_TABLA = ? ";

		if(bDebug){
			System.out.println("  QUERY sGetPantallaJQ GET TABLA EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" sCveOperacion: 	" + sCveOperacion + "\n" +  
					" iIdTabla: 		" + iIdTabla + "\n"); 
		}
		
		params 		= new Object[2];
		params[0] 	= (String)sCveOperacion;
		params[1] 	= (Integer)iIdTabla;
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Tabla: " + sResultado);
		}
		//jaJasonArray 	= new JsonArray();
		//stream 			= new ByteArrayInputStream(sResultado.getBytes(StandardCharsets.UTF_8));
		//jaJasonArray  	= gson.fromJson(new InputStreamReader(stream), JsonArray.class);
		jaJasonArrayT  		= gson.fromJson(sResultado, JsonArray.class);

		String sNomTabla 	= jaJasonArrayT.get(0).getAsJsonObject().get("D").getAsString();
		
		// Obtiene los botones 
		joObj.add("datosBoton", jaGetJQButtons(iIdTabla, iIdRol, sCveOperacion, bDebug));
		
		// Obtiene los elementos del Registro
		sSentenciaSql =	"SELECT	 A.ID_ORDEN_ATRIBUTO AS O, A.ATRIBUTO AS A, A.B_OBLIGATORIO AS B, A.ETIQUETA_COMBO_DEPENDE AS C, NULL AS D,  			\n " +			
						"		 A.ETIQUETA AS E, A.B_OPCION_COMBO_NULL AS F, A.ID_LISTADO AS G, A.TEMPLATE_HTML AS H, A.B_MODIFICABLE AS I, 			\n " +
						"		 NULL AS J, A.PLACEHOLDER AS K, TIPO_DATO AS L, A.ORDEN_PK AS M, A.DATE_FORMAT AS N, A.DESCRIPTION_QUERY AS P, 			\n " +
						"		 A.SELECT_PARENT_ATTRIBUTE AS Q, A.ETIQUETA_COMBO_DEPENDE AS R, A.SELECT_PARENT_EVENT_BIND AS S, 						\n " +
						"		 A.SELECT_PARENT_ATTRIBUTE_NAME AS T, A.PARENT_ATTRIBUTE AS U, A.IS_NEEDED_GET_DETAIL AS V, A.DATE_FORMAT_FRONT AS W,	\n " +
						"        A.MAPPED_SELECT_ATTRIBUTE AS X, A.VALUE_FROM_SELECT AS Y, IS_DISPLAY_FOREIGN_DATA AS Z, A.DATE_FORMAT_MYSQL AS AA,		\n " +
						" 		 A.PARENT_ATTRIBUTE AS AB, A.NUM_ROWS_TXT_AREA AS AC, A.LONGITUD AS AD													\n " +
						"FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A							\n " +
						"    LEFT JOIN AOS_SECU_ROL_PANT_TABLA_EX_CAM E   				\n " +
						" 		 ON  E.ID_ATTRIBUTE 	= A.ID_ATRIBUTO 				\n " +
						"		 AND E.ID_ROL			= ? 							\n " +
						"WHERE   A.ID_TABLA 			= ? 							\n " +
                        "    AND A.TIPO_ATRIBUTO 		= 'ALT_MOD'						\n " +
                        " 	 AND A.SIT_ATRIBUTO 		= 'AC'							\n " +
                        "    AND NOT(E.ID_ATTRIBUTE IS NOT NULL AND E.B_SELECT = 'F') 	\n " +
						"	 AND NVL(A.ID_ROL, ?) 		= ? 							\n " +
						"ORDER BY A.ID_ORDEN_ATRIBUTO									\n " ; 
		
		if(bDebug){
			System.out.println("  QUERY sGetPantallaJQ ATRIBUTOS EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdTabla: 	" + iIdTabla + "\n" +
					" iIdEmpresa: 	" + iIdEmpresa + "\n"); 
		}
		
		params 		= new Object[4];
		params[0] 	= (Integer)iIdRol; 
		params[1] 	= (Integer)iIdTabla;
		params[2] 	= (Integer)iIdRol; 
		params[3] 	= (Integer)iIdRol; 
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Atributos: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		String sColumnas 	= "";
		String sColumnasPK 	= ""; 
		
		for (int i = 0, size = jaJasonArray.size(); i < size; i++){
			
			joObjList 				= jaJasonArray.get(i).getAsJsonObject();
			String sAttributeName	= jaJasonArray.get(i).getAsJsonObject().get("A").getAsString(); 
			String sDataType 		= jaJasonArray.get(i).getAsJsonObject().get("L").getAsString();
			String sTemplate		= jaJasonArray.get(i).getAsJsonObject().get("H").getAsString();
			String sDateFormat 		= jaJasonArray.get(i).getAsJsonObject().get("N")!=null?jaJasonArray.get(i).getAsJsonObject().get("N").getAsString():"";
			String sDateFormatMySql	= jaJasonArray.get(i).getAsJsonObject().get("AA")!=null?jaJasonArray.get(i).getAsJsonObject().get("AA").getAsString():"";
			String sDescQuery 		= jaJasonArray.get(i).getAsJsonObject().get("P")!=null?jaJasonArray.get(i).getAsJsonObject().get("P").getAsString():"";
			String sParentAttrib	= jaJasonArray.get(i).getAsJsonObject().get("U")!=null?jaJasonArray.get(i).getAsJsonObject().get("U").getAsString():"";
			String sSelParentAtt	= jaJasonArray.get(i).getAsJsonObject().get("Q")!=null?jaJasonArray.get(i).getAsJsonObject().get("Q").getAsString():"";
			String sMapSelectAtt	= jaJasonArray.get(i).getAsJsonObject().get("X")!=null?jaJasonArray.get(i).getAsJsonObject().get("X").getAsString():"";
			boolean bNeededGetDet	= jaJasonArray.get(i).getAsJsonObject().get("V")!=null?jaJasonArray.get(i).getAsJsonObject().get("V").getAsString().equals("V"):false;
			boolean bForeignData	= jaJasonArray.get(i).getAsJsonObject().get("Z")!=null?jaJasonArray.get(i).getAsJsonObject().get("Z").getAsString().equals("V"):false;
			
			if(joObjList.get("G")!=null){
				iIdList		= joObjList.get("G").getAsInt(); 
				if(bDebug){
					System.out.println("Valor sel Listado: " + iIdList);
				}
				if(iIdList!=null&&!lLista.contains(iIdList)){
					lLista.add(iIdList);
				}
			}
			if(bDebug){
				System.out.println("joObjList.get(L): " + sDataType + "  joObjList.get(H): " + joObjList.get("H").getAsString());
				
				if(!sParentAttrib.equals("")||!sSelParentAtt.equals("")||bNeededGetDet){
					System.out.println("sAttributeName: " + sAttributeName + " SI ENTRA ");
				}else{
					System.out.println("sAttributeName: " + sAttributeName + " NO ENTRA ");
				}
				
				if(!sSelParentAtt.equals("")&&!sColumnas.contains(", T." + sSelParentAtt)){
					System.out.println("sSelParentAtt: " + sAttributeName + " SI ENTRA ");
				}else{
					System.out.println("sSelParentAtt: " + sAttributeName + " NO ENTRA ");
				}
			}
			
			if(sDBProduct.equals("MySQL")){
				sColumnas 	=
						//We add a comma if needed
						sColumnas + (sColumnas.equals("")?"":", ") + (
						//If it is not a field to display data from other sources we add the table column 
							(	bForeignData?"":
						//We add the attribute with a numeric alias
						(sDataType.equals("DATE")&&!sDateFormatMySql.equals(""))?("DATE_FORMAT(T." + sAttributeName + ",'" + sDateFormatMySql + "') AS \"" + i + "\"") : ("T." + sAttributeName + " AS \"" + i + "\"")) 
							) +
						//If it has description we add it
						(!sDescQuery.equals("")?((bForeignData?"":", ") + sDescQuery + " AS \"" + i + "d\""):"") +
						//If it has a parent attribute or this field is needed to get some detail page data we add it without alias
						((!sParentAttrib.equals("")||bNeededGetDet)?((
								// Check if it is a date and has a format
								(sDataType.equals("DATE")&&!sDateFormatMySql.equals(""))?
								((bForeignData?"":", ") + " DATE_FORMAT(T." + sAttributeName + ",'" + sDateFormatMySql + "') AS " + sAttributeName) : (", T." + sAttributeName))):"") + 
						//If it is a child select element we add it in case not already added
						(!sSelParentAtt.equals("")&&!sColumnas.contains(", T." + sSelParentAtt+ ",")?((bForeignData?"":", ") + " T." + sSelParentAtt):"") +
						//If it is a select feed element we add it 
						(sTemplate.equals("ACE_INPUT_SELECT_FEED")&&!sColumnas.contains(", T." + sAttributeName + " AS " + 
								// if it has a mapped attribute name we use the mapped alias
								(!sMapSelectAtt.equals("")?sMapSelectAtt:sAttributeName) + 
								",")?((bForeignData?"":", ") + " T." + sAttributeName + " AS " +  (!sMapSelectAtt.equals("")?sMapSelectAtt:sAttributeName)):"");
			}else{
				sColumnas 	=
						//We add a comma if needed
						sColumnas + (sColumnas.equals("")?"":", ") + (
						//If it is not a field to display data from other sources we add the table column 
							(	bForeignData?"":
						//We add the attribute with numeric alias
						(sDataType.equals("DATE")&&!sDateFormat.equals(""))?("TO_CHAR(T." + sAttributeName + ",'" + sDateFormat + "') AS \"" + i + "\"") : ("T." + sAttributeName + " AS \"" + i + "\"")) 
							) +
						//If it has description we add it
						(!sDescQuery.equals("")?((bForeignData?"":", ") + sDescQuery + " AS \"" + i + "d\""):"") +
						//If it has a parent attribute or this field is needed to get some detail page data we add it without alias
						((!sParentAttrib.equals("")||bNeededGetDet)?((
								// Check if it is a date and has a format
								(sDataType.equals("DATE")&&!sDateFormat.equals(""))?
								((bForeignData?"":", ") + " TO_CHAR(T." + sAttributeName + ",'" + sDateFormat + "') AS " + sAttributeName) : (", T." + sAttributeName))):"") + 
						//If it is a child select element we add it in case not already added
						(!sSelParentAtt.equals("")&&!sColumnas.contains(", T." + sSelParentAtt+ ",")?((bForeignData?"":", ") + " T." + sSelParentAtt):"") +
						//If it is a select feed element we add it 
						(sTemplate.equals("ACE_INPUT_SELECT_FEED")&&!sColumnas.contains(", T." + sAttributeName + " AS " + 
								// if it has a mapped attribute name we use the mapped alias
								(!sMapSelectAtt.equals("")?sMapSelectAtt:sAttributeName) + 
								",")?((bForeignData?"":", ") + " T." + sAttributeName + " AS " +  (!sMapSelectAtt.equals("")?sMapSelectAtt:sAttributeName)):"");
			}
			 

			//Eliminate the now useless description query 
			if(jaJasonArray.get(i).getAsJsonObject().get("P")!=null){
				jaJasonArray.get(i).getAsJsonObject().remove("P");	
			}
			//Eliminate the now useless parent attribute
			if(jaJasonArray.get(i).getAsJsonObject().get("U")!=null){
				jaJasonArray.get(i).getAsJsonObject().remove("U");	
			}
			//Eliminate the now useless needed get detail
			if(jaJasonArray.get(i).getAsJsonObject().get("V")!=null){
				jaJasonArray.get(i).getAsJsonObject().remove("V");	
			}
			//We add parameters that may be needed to get the combo options
			if(joObjList.get("Q")!=null){
				if(bDebug){
					System.out.println("Es dependiente : " + sAttributeName + " de " + joObjList.get("Q").getAsString());
				}
				String sCampoDep = joObjList.get("Q").getAsString(); 
				if(!lListAtts.contains(sCampoDep)){
					lListAtts.add(sCampoDep); 
				}
			}
		}

		joObj.add("datosAtribs", jaJasonArray);
		
		if(bDebug){
			System.out.println("sColumnas: " + sColumnas);
		}  
		
		params 		= new Object[sColsPK.length];
		if(bDebug){
			System.out.println("sColumnas: " + sColumnas);
		}  
		sColumnasPK = ""; 
		for(int x = 0; x < sColsPK.length; x++){
			String[] sAttribute = sColsPK[x].split(Pattern.quote("!"));
			map.put(sAttribute[0], tokens[x]);
			sColumnasPK =  sColumnasPK + (sColumnasPK.equals("")?"":"    AND ") + sAttribute[0] + " = ?  \n"; 
			if(bDebug){
				System.out.println("sColumnasPK: " + sColumnasPK);
			}   
			switch (sAttribute[1]){
				case "NUMBER": 
					params[x] 	= Integer.valueOf(tokens[x]); 
					break;
				case "VARCHAR": 
					params[x] 	= tokens[x]; 
					break;
				case "DATE": 
					try {
						SimpleDateFormat format = new SimpleDateFormat(sAttribute[2]);
				        Date parsed = format.parse(tokens[x]);
				        java.sql.Date sql = new java.sql.Date(parsed.getTime());
						params[x] 	= sql;
			        } catch (ParseException e) {
			            e.printStackTrace();
			        }
					break;
			}

		}

		if(bDebug){
			System.out.println("sColumnas: " + sColumnas);
			System.out.println("sColumnasPK: " + sColumnasPK);
		}
		
		sSentenciaSql = "SELECT " + sColumnas 	+ " 	\n " + 
						"FROM 	" + sNomTabla	+ " T 	\n " +
						"WHERE 	" + sColumnasPK; 

		
		//params 			= new Object[lListaPk.size()];
		if(bDebug){
			System.out.println("  QUERY sGetTableRs sSentenciaSql RS EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n");
			System.out.println("  sColsPK.length ----->>>>>>>>>>>>>>>>>>>  " + sColsPK.length);
			System.out.println("  sParametros     ----->>>>>>>>>>>>>>>>>>>  " + sParametros);
		}		
		
		/*
		int i = 0; 
		for(String sElement : lListaPk) {
			String[] sSplit = sElement.split(Pattern.quote("|"));
			if(bDebug){
				System.out.println("  sElement  ----->>>>>>>>>>>>>>>>>>> " + sElement);
				System.out.println("  i         ----->>>>>>>>>>>>>>>>>>> " + i);
				System.out.println("  sSplit[0] ----->>>>>>>>>>>>>>>>>>> " + sSplit[0]);
				System.out.println("  tokens[i] ----->>>>>>>>>>>>>>>>>>> " + tokens[i]);
				
			}	
			map.put(sSplit[0], tokens[i]);

			switch (sSplit[1]){
				case "NUMBER": 
					params[i] 	= Integer.valueOf(tokens[i]); 
					break;
				case "VARCHAR2": 
					params[i] 	= tokens[i]; 
					break;
				case "DATE": 
					try {
						SimpleDateFormat format = new SimpleDateFormat(sSplit[2]);
				        Date parsed = format.parse(tokens[i]);
				        java.sql.Date sql = new java.sql.Date(parsed.getTime());
						params[i] 	= sql;
			        } catch (ParseException e) {
			            e.printStackTrace();
			        }
					break;
			}
			i = i + 1;
		}*/
				
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado primero: " + sResultado);
		}
		
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		jaJasonArrayR  	= jaJasonArray; 

		if(bDebug){
			System.out.println("jaJasonArray rs: " + jaJasonArray.toString());
		}
		
		 
		if(bDebug){
			System.out.println("Tama�o del Listado de campos dependientes: " + lListAtts.size());
		}
		if(lListAtts.size() > 0){
			joObjList = new JsonObject();
			for(String sElement : lListAtts) {
				if(bDebug){
					System.out.println("sElement: " + sElement);
				}
				if(!map.containsKey(sElement)){
					map.put(sElement, jaJasonArray.get(0).getAsJsonObject().get(sElement).getAsString());
				}
			}
		}  
		
		//Check if the record has a parent
		sSentenciaSql =	"	SELECT  M.ID_TABLA_MAESTRO AS A, M.ATRIBUTO_MAESTRO AS B, M.ATRIBUTO_DETALLE AS C, 		\n" +
						"			M.DATE_FORMAT_MASTER AS D, M.DATE_FORMAT_DETAIL AS E, M.TIPO_DATO_MASTER AS F	\n" + 
						"	FROM    AOS_ARQ_MASTER_DETAIL_V M														\n" + 	
						"	WHERE   M.ID_TABLA_DETALLE      = ?														\n" ; 
			
		params 		= new Object[1];
		params[0] 	= (Integer)iIdTabla;
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Master Parent Tabla: " + sResultado);
		}
		JsonArray jaJasonArrayM  	= gson.fromJson(sResultado, JsonArray.class);
		
		String sKeyGetRecordparent 	= null; 
		String sAttributesPK		= "";
		String sAttributesValues	= "";
		String sParamsDetail		= "";
		String sParamsDetailTmp	 	= "";

		if(jaJasonArrayM.size()>0){
			sAttributesPK		= "";
			sAttributesValues	= "";
			
			sKeyGetRecordparent		= jaJasonArrayM.get(0).getAsJsonObject().get("A").getAsString() + "^$^";
			
			for (int id = 0, sizeD 	= jaJasonArrayM.size(); id < sizeD; id++){
				String sNomAttM 	= jaJasonArrayM.get(id).getAsJsonObject().get("B").getAsString();
				String sNomAttD		= jaJasonArrayM.get(id).getAsJsonObject().get("C").getAsString();
				String sDatTyp		= jaJasonArrayM.get(id).getAsJsonObject().get("F").getAsString();
				String sDatFormat	= jaJasonArrayM.get(id).getAsJsonObject().get("D")!=null?jaJasonArrayM.get(id).getAsJsonObject().get("D").getAsString():"";
				if(bDebug){
					System.out.println("sNomAttM: 	" + sNomAttM);
					System.out.println("sNomAttD: 	" + sNomAttD);
					System.out.println("sDatTyp: 	" + sDatTyp);
					System.out.println("sDatFormat: " + sDatFormat);
				}
				sAttributesPK 		= sAttributesPK + (sAttributesPK.equals("")?"^$^":"|") + sNomAttM + "!" + sDatTyp + (!sDatFormat.equals("")?"!" + sDatFormat:"");
				sAttributesValues	= sAttributesValues + (sAttributesValues.equals("")?"":"^$") + jaJasonArray.get(0).getAsJsonObject().get(sNomAttD).getAsString(); 
				jaJasonArray.get(0).getAsJsonObject().remove(sNomAttD);
			}
		}
		if(sKeyGetRecordparent!=null){
			sKeyGetRecordparent = sKeyGetRecordparent + sAttributesValues + sAttributesPK; 
			//We add the way to retrieve the key attributes for the records 
			JsonElement jeElement = new JsonParser().parse(sKeyGetRecordparent);
			jaJasonArrayT.get(0).getAsJsonObject().add("Z", jeElement);
		}

		//We add the table data
		joObj.add("datosTabla", jaJasonArrayT);
		
		//We add the record data 
		joObj.add("datosRegistro", jaJasonArray);

		//Check if the record has detail
		sSentenciaSql =	"	SELECT  DISTINCT M.ID_TABLA_DETALLE, T.ID_ORDER		\n" + 
						"	FROM    AOS_ARQ_MASTER_DETAIL_V M					\n" + 	
						"	    JOIN AOS_ARQ_PANTALLA_TABLA T					\n" +
						"	        ON M.ID_TABLA_DETALLE   = T.ID_TABLA		\n" +
						"	        AND T.SIT_TABLA         = 'AC' 				\n" +
						"	WHERE   M.ID_TABLA_MAESTRO      = ?					\n" +
						"	ORDER BY T.ID_ORDER									\n" ; 
			
		
		params 		= new Object[1];
		params[0] 	= (Integer)iIdTabla;
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Master Detail Tabla: " + sResultado);
		}
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		if(jaJasonArray.size()>0){
			map.put("DYN_CTL_RECORD", jaJasonArrayR);
			if(bDebug){
				System.out.println("DYN_CTL_RECORD: " + jaJasonArrayR.toString());
			}
			map.put("DYN_CTL_ID_ROL",String.valueOf(iIdRol));
			JsonArray jalistResult = new JsonArray();
			
			for (int id = 0, sizeD = jaJasonArray.size(); id < sizeD; id++){
				
				if(map.containsKey("DYN_CTL_ID_TABLA")){
					map.replace("DYN_CTL_ID_TABLA", jaJasonArray.get(id).getAsJsonObject().get("ID_TABLA_DETALLE").getAsString());
				}else{
					map.put("DYN_CTL_ID_TABLA", jaJasonArray.get(id).getAsJsonObject().get("ID_TABLA_DETALLE").getAsString());
				}
				JsonObject joTableResult = sGetTableRsDetail(map); 
				joTableResult.addProperty("O", jaJasonArray.get(id).getAsJsonObject().get("ID_ORDER")!=null?
						jaJasonArray.get(id).getAsJsonObject().get("ID_ORDER").getAsString():"0");
				jalistResult.add(joTableResult);
 			}
			joObj.add("datosListDetail", jalistResult);
		}
		
		
		/*********************************************************************************************************************************
		 * 								Listados 
		 * 
		 */
		
		if(bDebug){
			System.out.println("Tama�o del Listado: " + lLista.size());
		}
		
		if(lLista.size() > 0){
			
			if(!map.containsKey("DYN_CTL_RECORD")){
				map.put("DYN_CTL_RECORD", jaJasonArrayR);
				if(bDebug){
					System.out.println("DYN_CTL_RECORD 2: " + jaJasonArrayR.toString());
				}
			}
			
			joObjList = new JsonObject();
			if(map.containsKey("DYN_CTL_THIS_IS_GET_RECORD")){
				map.replace("DYN_CTL_THIS_IS_GET_RECORD", "YES");
			}else{
				map.put("DYN_CTL_THIS_IS_GET_RECORD", "YES");
			} 
			
			for(int element : lLista) {
				if(map.containsKey("DYN_CTL_ID_LISTADO")){
					map.replace("DYN_CTL_ID_LISTADO", String.valueOf(element));
				}else{
					map.put("DYN_CTL_ID_LISTADO", String.valueOf(element));
				} 
				sResultado 		= sGetSelectOptions(map); 
				jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
				joObjList.add(Integer.toString(element), jaJasonArray);
				if(bDebug){System.out.println(element);}
			}
			
			if(map.containsKey("DYN_CTL_THIS_IS_GET_RECORD")){
				map.remove("DYN_CTL_THIS_IS_GET_RECORD");
			}
			
			joObj.add("datosListados", joObjList);
			if(bDebug){
				System.out.println("sResultado Datos joObjList: " + joObjList.toString());
			}
		}
		
		/***************************************************************************************************************************************
		 * 
		 * Validation rules 
		 * 
		 */
		
		
		// we get the validation of attributes 
		sSentenciaSql =	"SELECT  CASE WHEN A.TIPO_TEMPLATE = 'LINK_POPUP' AND A.ID_PANTALLA_LINK IS NOT NULL THEN 						\n " +
						"			'CTL_DESC_'||A.ATRIBUTO ELSE A.ATRIBUTO END AS A, A.TIPO_TEMPLATE AS B, 							\n " +
						"		 A.TIPO_DATO AS C, VAL.VALIDATION_KEY AS D, VF.ARGUMENTS AS E, VAL.VALUE_1 AS F,  						\n " +
				        "        VAL.VALUE_2 AS G, NVL(VAL.TX_MESSAGE_VALIDATION, VF.MESSAGE_VALIDATION) AS H, 							\n " +
				        "        VF.EXPRESSION_VALIDATION AS I, A.B_APLICA_RANGO_FILTRO	AS J											\n " +
				        "FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A																			\n " +
				        "    JOIN AOS_ARQ_ATRIBUTE_VALIDATION VAL																		\n " +
				        "        ON  A.ID_ATRIBUTO       = VAL.ID_ATTRIBUTE																\n " +	
				        "    JOIN AOS_ARQ_VALIDATION_FORM VF																			\n " +
				        "        ON  VAL.VALIDATION_KEY  = VF.VALIDATION_KEY															\n " +
				        "WHERE   A.ID_EMPRESA        = ?																				\n " +
				        "    AND A.ID_TABLA          = ?																				\n " +
				        "    AND A.TIPO_ATRIBUTO     = 'ALT_MOD'  																		\n " +	
				        "    AND A.SIT_ATRIBUTO      = 'AC'																				\n " +
				        "    AND A.TIPO_TEMPLATE     <> 'INPUT_CHECKBOX'																\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND 'ALT_MOD' = 'UPDATE' AND NVL(A.IS_SYSDATE_ON_UPDATE,'F') = 'V')	\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND 'ALT_MOD' = 'UPDATE' AND NVL(A.IS_SYSUSER_ON_UPDATE,'F') = 'V')	\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND 'ALT_MOD' = 'CREATE' AND NVL(A.IS_SYSDATE_ON_CREATE,'F') = 'V')	\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND 'ALT_MOD' = 'CREATE' AND NVL(A.IS_SYSUSER_ON_CREATE,'F') = 'V')	\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND 'ALT_MOD' = 'UPDATE' AND NVL(A.B_MODIFICABLE,'V') = 'F')			\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND NVL(A.IS_SYS_ADMIN,'F') = 'V')									\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND A.SEQUENCE_AUTO_INCREMENTA IS NOT NULL)							\n " +
				        "    AND A.ATRIBUTO <> 'ID_EMPRESA'																				\n " +
				        "    AND NVL(A.IS_DISPLAY_FOREIGN_DATA,'F') <> 'V'																\n " +
						"    AND NVL(A.IS_TABLE_FIELD,'V') = 'V'																		\n " +
				        "ORDER BY A.ID_ORDEN_ATRIBUTO																					\n " ;	
		
		params 		= new Object[2];
		params[0] 	= (Integer)iIdEmpresa; 
		params[1] 	= (Integer)iIdTabla;
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado validation Atributos GetRecord: " + sResultado);
		}
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		joObj.add("datosValidation", jaJasonArray);
		
		
		sResultado = joObj.toString();

		if(bDebug){
			System.out.println("sResultado �ltimo: " + sResultado);
		}
		return sResultado;
	} // sGetRecord 
	
	public String sGetPassEncr(String sPassword, String sDBProduct, boolean bDebug) throws SQLException{

		String sResultado = null;

		// Obtiene los botones 
		sSentenciaSql =	 (sDBProduct.equals("MySQL")?" SELECT PASSWORD(?) AS RESULT ":"SELECT  CONTROL.PKG_CRYPTO.ENCRYPT(?) AS RESULT FROM DUAL");
		
		if(bDebug){
			System.out.println("  QUERY sGetPassEncr EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" sPassword: 		" + sPassword) ;   
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setString(1, sPassword);
		rs = pstmt.executeQuery();

		if(rs.next()){
			sResultado	= rs.getString("RESULT");
		}

		rs.close();
	    pstmt.close();
 
		return sResultado;
	} // sGetPassEncr
	
	
	

	
	public String sUpdateRecord(HashMap<String, Object> map) throws SQLException{

		int iIdEmpresa 			= Integer.parseInt((String)map.get("DYN_CTL_ID_EMPRESA"));
		Usuario  usuario 		= (Usuario)map.get("DYN_CTL_USUARIO");
		String sCveOperacion	= (String) map.get("DYN_CTL_TIPO_OPERACION");
		int iIdTabla			= Integer.parseInt((String) map.get("DYN_CTL_ID_TABLA"));
		boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
		int iIdRol				= usuario.iIdRol;
		String sCveUsuario		= usuario.sCveUsuario; /// 
		Object[] params 		= new Object[0];

		DatabaseMetaData mtdata = conn.getMetaData();
		String sDBProduct 		= mtdata.getDatabaseProductName();
		
		String sResultado 		= "";
		Gson gson 				= new Gson();
		JsonArray jaJasonArray 	= new JsonArray();
		boolean bError			= false; 
		
		
		sSentenciaSql =	"SELECT	 A.IS_UPDATE_DISABLED AS A, T.TABLA AS B	\n " + 
						"FROM    AOS_ARQ_PANTALLA_TABLA A					\n " +
						"  	 JOIN AOS_ARQ_TABLA T  							\n " +
						"       ON 	A.ID_TABLA_ORIGEN = T.ID_TABLA 			\n " +
						"WHERE   A.ID_TABLA = ? 							\n " ;

		if(bDebug){
			System.out.println("  QUERY sGetPantallaJQ GET TABLA EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" sCveOperacion: 	" + sCveOperacion + "\n" +  
					" iIdTabla: 		" + iIdTabla + "\n"); 
		}
		
		params 		= new Object[1];
		params[0] 	= (Integer)iIdTabla;
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Tabla: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		String sNomTabla 		= jaJasonArray.get(0).getAsJsonObject().get("B").getAsString();
		boolean bUpdateDisabled = (jaJasonArray.get(0).getAsJsonObject().get("A")!=null?jaJasonArray.get(0).getAsJsonObject().get("A").getAsString():"F").equals("V");
		
		if(bUpdateDisabled){
			return "La opci�n para actualizaci�n ha sido deshabilitada";
		}
		
		
		// Obtiene los elementos del Registro
		sSentenciaSql =	"SELECT	 A.ID_ORDEN_ATRIBUTO AS O, A.ATRIBUTO AS A, A.ORDEN_PK AS B, A.IS_SYSDATE_ON_UPDATE AS C, A.IS_SYSUSER_ON_UPDATE AS D,  \n " +			
						"		 A.TIPO_DATO AS E, A.DATE_FORMAT AS F, A.IS_PASSWORD_ENCRYPTED G, A.TEMPLATE_HTML AS H, A.TIPO_TEMPLATE AS I, 			\n " +		
						"		 A.B_OBLIGATORIO AS J 																									\n " +
						"FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A							\n " +
						"    LEFT JOIN AOS_SECU_ROL_PANT_TABLA_EX_CAM E   				\n " +
						" 		 ON  E.ID_ATTRIBUTE 	= A.ID_ATRIBUTO 				\n " +
						"		 AND E.ID_ROL			= ? 							\n " +
						"WHERE   A.ID_TABLA 			= ? 							\n " +
                        "    AND A.TIPO_ATRIBUTO 		= 'ALT_MOD'						\n " +
                        " 	 AND A.SIT_ATRIBUTO 		= 'AC'							\n " +
                        "    AND NOT(E.ID_ATTRIBUTE IS NOT NULL AND E.B_ALTER = 'F') 	\n " +
						"	 AND NVL(A.ID_ROL, ?) 		= ? 							\n " +
                        "	 AND (A.ORDEN_PK IS NOT NULL OR (							\n " +
                        "			CASE WHEN NVL(A.IS_SYSDATE_ON_UPDATE,'F') = 'V'   	\n" +
                        "				OR NVL(A.IS_SYSUSER_ON_UPDATE,'F') = 'V' THEN   \n " +
                        "			'V' ELSE A.B_MODIFICABLE END <> 'F'))				\n " +
						"    AND NVL(A.IS_TABLE_FIELD,'V') = 'V'						\n " +
						"ORDER BY A.ID_ORDEN_ATRIBUTO									\n " ; 
		
		if(bDebug){
			System.out.println("  QUERY sGetPantallaJQ ATRIBUTOS EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdTabla: 	" + iIdTabla + "\n" +
					" iIdEmpresa: 	" + iIdEmpresa + "\n"); 
		}
		
		params 		= new Object[4];
		params[0] 	= (Integer)iIdRol; 
		params[1] 	= (Integer)iIdTabla;
		params[2] 	= (Integer)iIdRol; 
		params[3] 	= (Integer)iIdRol; 
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Atributos: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		String sColumnas 		= "";
		String sColumnasPK 		= ""; 
		String sCheckPasswords	= ""; 
		
		for (int i = 0, size = jaJasonArray.size(); i < size; i++){
			String sAttname 	= jaJasonArray.get(i).getAsJsonObject().get("A").getAsString();
			boolean bEncrypt	= jaJasonArray.get(i).getAsJsonObject().get("G")==null?false:jaJasonArray.get(i).getAsJsonObject().get("G").getAsString().equals("V");
			String sTemplate	= jaJasonArray.get(i).getAsJsonObject().get("H").getAsString();
			String sTipoTemplate= jaJasonArray.get(i).getAsJsonObject().get("I").getAsString();
			
			if(bDebug){
				System.out.println("Forma columnas con atributo : " + sAttname);
			}
			if(map.containsKey(sAttname)||sTipoTemplate.equals("INPUT_CHECKBOX")){
				if((jaJasonArray.get(i).getAsJsonObject().get("B"))!=null){
					sColumnasPK =  sColumnasPK + (sColumnasPK.equals("")?"":"    AND ") + sAttname + " = ?  \n"; 
				}else{
					sColumnas 	= sColumnas + (sColumnas.equals("")?"         ":"       , ") + sAttname + 
							(bEncrypt && !sTemplate.equals("ACE_INPUT_PASSWORD_CHANGE")?
									(sDBProduct.equals("MySQL")?" PASSWORD(?) ":" = CONTROL.PKG_CRYPTO.ENCRYPT(?) ")
									 : " = ? ");
				}
				if(bEncrypt&&jaJasonArray.get(i).getAsJsonObject().get("H").getAsString().equals("ACE_INPUT_PASSWORD_CHANGE")){
					if(bDebug){
						System.out.println("Si cumple condiciones : " + sAttname);
					}
					String sPassword = (String)map.get(sAttname);
					String sCheck	 = sGetPassEncr(sPassword, sDBProduct, bDebug);
					sCheckPasswords  = sCheckPasswords + "    AND '" + sCheck + "' = " + sAttname + "\n";
					map.replace(sAttname, sGetPassEncr((String)map.get("CTLNEW_" + sAttname), sDBProduct, bDebug));
				}
			} 
		}
		
		if(bDebug){
			System.out.println("sCheckPasswords : >" + sCheckPasswords + "<");
		}
		
		int icount = 1;
		if(!sCheckPasswords.equals("")){
			
			sSentenciaSql = "SELECT 1 FROM " + sNomTabla 	+ " \n " + 
							"WHERE 	" + sColumnasPK  		+ " \n " + sCheckPasswords;
			
			pstmt = conn.prepareStatement(sSentenciaSql);
			if(bDebug){
				System.out.println(" sSentenciaSql valida password ----->>>>>>>>>>>>>>>>>>>  " + sSentenciaSql);
			}
			
			boolean bMissingKey = false; 
			
			for (int i = 0, size = jaJasonArray.size(); i < size; i++){
				String sAtttribName =  jaJasonArray.get(i).getAsJsonObject().get("A").getAsString();

				if(bDebug){
					System.out.println("  icount  SI valida password  PK ----->>>>>>>>>>>>>>>>>>>  " + icount + " Atributo: " + sAtttribName +
							" Tipo dato: " + jaJasonArray.get(i).getAsJsonObject().get("E").getAsString());
				}

				// Next Key fields
				if((jaJasonArray.get(i).getAsJsonObject().get("B"))!=null){
					
					switch (jaJasonArray.get(i).getAsJsonObject().get("E").getAsString()){
						case "NUMBER":
							if(map.containsKey(sAtttribName)){
								pstmt.setFloat(icount, Float.valueOf((String)map.get(sAtttribName)));
								icount = icount + 1;
							}else{
								bMissingKey = true; 
							}
							break;
						case "VARCHAR":
							if(map.containsKey(sAtttribName)){
								pstmt.setString(icount, (String)map.get(sAtttribName));
								icount = icount + 1;
							}else{
								bMissingKey = true; 
							}
							break;
						case "DATE":
							if(map.containsKey(sAtttribName)){
								try {
									SimpleDateFormat formatter 	= new SimpleDateFormat(jaJasonArray.get(i).getAsJsonObject().get("F").getAsString());
						            Date date 					= formatter.parse((String)map.get(sAtttribName));
						            long time   				= date.getTime(); 
						            Timestamp timestamp 		= new Timestamp(time); 
						            pstmt.setTimestamp(icount, timestamp);
									icount = icount + 1;
						        } catch (ParseException e) {
						            e.printStackTrace();
						        }
							}else{
								bMissingKey = true; 
							}
							break;
					}
				}
			}

			
			if(bMissingKey){
				sResultado = sGetMensaje(-7, bDebug);
				bError = true;
			}else{
				if(bDebug){
					System.out.println("  icount final valida password ----->>>>>>>>>>>>>>>>>>>  " + icount);
				}
				rs = pstmt.executeQuery();
				if(!rs.next()){
					bError = true;
					sResultado	=  sGetMensaje(-8, bDebug);
				}
			}
		}
		
		
		
		if(!bError){
		
			sSentenciaSql = "UPDATE	" + sNomTabla + " SET 		\n " + 
							"       " + sColumnas + "  			\n " + 
							"WHERE 	" + sColumnasPK;
	
			if(bDebug){
				System.out.println("  QUERY DE ACTUALIZACION EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
			}
	
			pstmt = conn.prepareStatement(sSentenciaSql);
	
			// We process columns that are not part of the Primary key  
			icount = 1; 
			for (int i = 0, size = jaJasonArray.size(); i < size; i++){
				String sAttributeName 	= jaJasonArray.get(i).getAsJsonObject().get("A").getAsString();
				String sDataType 		= jaJasonArray.get(i).getAsJsonObject().get("E").getAsString();
				String sTipoTemplate	= jaJasonArray.get(i).getAsJsonObject().get("I").getAsString();
				boolean bObligatorio	= jaJasonArray.get(i).getAsJsonObject().get("J").getAsString().equals("V");
				if(bDebug){
					System.out.println(" icount INICIAL -->>>>  " + icount + " sAttributeName: "  + sAttributeName);
				}
				
				// First fields to update
				if((jaJasonArray.get(i).getAsJsonObject().get("B"))==null){
					if(bDebug){
						System.out.println(" No es PK icount  -->>>>  " + icount);
					}
					
					switch (sDataType){
						case "NUMBER":
							if(map.containsKey(sAttributeName)){
								Float vFloat = null; 
								if(!((String)map.get(sAttributeName)).equals("")){
									vFloat = Float.valueOf((String)map.get(sAttributeName));
									pstmt.setFloat(icount, vFloat);
								}else{
									pstmt.setNull(icount, Types.NUMERIC);
								}
								
								if(bDebug){
									System.out.println(" Agrega NUMBER -->>>>  " + icount + " Atributo: " + sAttributeName + " Tipo dato: " + 
											sDataType + " Valor: " + vFloat);
								}
								icount = icount + 1;
							}
							break;
						case "VARCHAR":
							
							if(jaJasonArray.get(i).getAsJsonObject().get("D")!=null&&
									jaJasonArray.get(i).getAsJsonObject().get("D").getAsString().equals("V")){
								pstmt.setString(icount, sCveUsuario);
								if(bDebug){
									System.out.println(" Agrega VARCHAR 1 -->>>>  " + icount + " Atributo: " + sAttributeName + " Tipo dato: " + sDataType);
								}
								icount = icount + 1;
							}else{
							
								if(sTipoTemplate.equals("INPUT_CHECKBOX")){
									if(map.containsKey(sAttributeName)&&((String)map.get(sAttributeName)).equals("on")){
										pstmt.setString(icount, "V");
									}else{
										if(bObligatorio){
											pstmt.setString(icount, "F");
										}else{
											pstmt.setString(icount, "");
										}
									}
									if(bDebug){
										System.out.println(" Agrega INPUT_CHECKBOX -->>>>  " + icount + " Atributo: " + sAttributeName + 
												" Tipo dato: " + sDataType);
									}
									icount = icount + 1;
								}else{
									if(map.containsKey(sAttributeName)){
										if(!((String)map.get(sAttributeName)).equals("")){
											pstmt.setString(icount, (String)map.get(sAttributeName));
										}else{
											pstmt.setNull(icount, Types.VARCHAR);
										}
										if(bDebug){
											System.out.println(" Agrega VARCHAR 2 -->>>>  " + icount + " Atributo: " + sAttributeName + " Tipo dato: " + sDataType);
										}
										icount = icount + 1;
									}else{
										if(bDebug){
											System.out.println(" NOOO Agrega VARCHAR 2 -->>>>  " + icount + " Atributo: " + sAttributeName + " Tipo dato: " + sDataType);
										}
									}
								}
							}
							break;
						case "DATE":
							
							if(jaJasonArray.get(i).getAsJsonObject().get("C")!=null&&jaJasonArray.get(i).getAsJsonObject().get("C").getAsString().equals("V")){
								Calendar cal = Calendar.getInstance();
								Timestamp  timestamp = new java.sql.Timestamp(cal.getTimeInMillis());
					            pstmt.setTimestamp(icount, timestamp);
					            if(bDebug){
									System.out.println(" Agrega DATE 1 -->>>>  " + icount + " Atributo: " + sAttributeName + " Tipo dato: " + sDataType);
								}
								icount = icount + 1;
							}else{
							
								if(map.containsKey(sAttributeName)){
									if(!((String)map.get(sAttributeName)).equals("")){
										try {
											SimpleDateFormat formatter 	= new SimpleDateFormat(jaJasonArray.get(i).getAsJsonObject().get("F").getAsString());
								            Date date 					= formatter.parse((String)map.get(sAttributeName));
								            long time   				= date.getTime(); 
								            Timestamp timestamp 		= new Timestamp(time); 
								            if(bDebug){
												System.out.println(" Agrega DATE 2 -->>>>  " + icount + " Atributo: " + sAttributeName + " Tipo dato: " + sDataType);
												System.out.println(" Valor fecha -->>>>  " + (String)map.get(sAttributeName)); 
												System.out.println(" Valor date -->>>>>> " + date);
												System.out.println(" Valor time -->>>>>> " + time);
												System.out.println(" Valor timestamp -->>>>>> " + timestamp);
											}
								            pstmt.setTimestamp(icount, timestamp);
											icount = icount + 1;
								        } catch (ParseException e) {
								            e.printStackTrace();
								        }
									}else{
										if(bDebug){
											System.out.println(" Agrega DATE NULL -->>>>  " + icount + " Atributo: " + sAttributeName + " Tipo dato: " + sDataType);
										}
										pstmt.setNull(icount, Types.DATE);
										icount = icount + 1;
									}
									
								}
							}
							break;
					}
				}else{
					if(bDebug){
						System.out.println("  icount  SI ES PK ----->>> icount " + icount + " i " + i + " - Atributo: " + sAttributeName + " Tipo dato: " + sDataType);
					}
				}
			}
			
			if(bDebug){
				System.out.println("  icount  ----->>>>>>>>>>>>>>>>>>>  " + icount);
			}
			
			boolean bMissingKey = false; 
			
			for (int i = 0, size = jaJasonArray.size(); i < size; i++){
	
				if(bDebug){
					System.out.println("  icount  SI  PK ----->>>>>>>>>>>>>>>>>>>  " + icount + " Atributo: " + jaJasonArray.get(i).getAsJsonObject().get("A").getAsString() +
							" Tipo dato: " + jaJasonArray.get(i).getAsJsonObject().get("E").getAsString());
				}
	
				// Next Key fields
				if((jaJasonArray.get(i).getAsJsonObject().get("B"))!=null){
					
					switch (jaJasonArray.get(i).getAsJsonObject().get("E").getAsString()){
						case "NUMBER":
							if(map.containsKey(jaJasonArray.get(i).getAsJsonObject().get("A").getAsString())){
								pstmt.setFloat(icount, Float.valueOf((String)map.get(jaJasonArray.get(i).getAsJsonObject().get("A").getAsString())));
								icount = icount + 1;
							}else{
								bMissingKey = true; 
							}
							break;
						case "VARCHAR":
							if(map.containsKey(jaJasonArray.get(i).getAsJsonObject().get("A").getAsString())){
								pstmt.setString(icount, (String)map.get(jaJasonArray.get(i).getAsJsonObject().get("A").getAsString()));
								icount = icount + 1;
							}else{
								bMissingKey = true; 
							}
							break;
						case "DATE":
							if(map.containsKey(jaJasonArray.get(i).getAsJsonObject().get("A").getAsString())){
								try {
									SimpleDateFormat formatter 	= new SimpleDateFormat(jaJasonArray.get(i).getAsJsonObject().get("F").getAsString());
						            Date date 					= formatter.parse((String)map.get(jaJasonArray.get(i).getAsJsonObject().get("A").getAsString()));
						            long time   				= date.getTime(); 
						            Timestamp timestamp 		= new Timestamp(time); 
						            pstmt.setTimestamp(icount, timestamp);
									icount = icount + 1;
						        } catch (ParseException e) {
						            e.printStackTrace();
						        }
							}else{
								bMissingKey = true; 
							}
							break;
					}
				}
			}
	
			
			if(bMissingKey){
				bError 		= true; 
				sResultado 	= sGetMensaje(-7, bDebug);
			}else{
				if(bDebug){
					System.out.println("  icount final ----->>>>>>>>>>>>>>>>>>>  " + icount);
				}
				
				rs = pstmt.executeQuery();
				int iAffected = pstmt.getUpdateCount();
				if(bDebug){
					System.out.println(" Registros actualizados -->>> " + iAffected);
				}
				if(iAffected>1){
					bError = true; 
					if(bDebug){
						System.out.println(" rollback -->>> " + iAffected);
					}
					conn.rollback();
					sResultado = sGetMensaje(-9, bDebug);
				}else{
					if(iAffected==0){
						conn.rollback();
						sResultado = sGetMensaje(-10, bDebug);
					}else{
						conn.commit();
						sResultado = sGetMensaje(-4, bDebug);
					}
				}
				
				rs.close();
			    pstmt.close();
			}
			
			if(bDebug){
				System.out.println("  sResultado ----->>>>>>>>>>>>>>>>>>>  " + sResultado);
			}
		}
		
		if(bError){
			//sResultado 		= "{\"result\":[{\"A\":\"" + sResultado + "\"}]}";
			sResultado 		= "[{\"E\":\"" + sResultado + "\"}]";
			jaJasonArray	= gson.fromJson(sResultado, JsonArray.class); 
		}else{
			//sResultado 		= "{\"result\":[{\"A\":\"" + sResultado + "\"}]}";
			sResultado 		= "[{\"A\":\"" + sResultado + "\"}]";
			jaJasonArray	= gson.fromJson(sResultado, JsonArray.class); 
		}
		
		/*Gson gson = new Gson(); 
		JsonReader jr = new JsonReader(new StringReader(s.trim())); 
		jr.setLenient(true); 
		Map keyValueMap = (Map) gson.fromJson(jr, Object.class);*/
		
		if(bDebug){
			System.out.println("  sResultado ----->>>>>>>>>>>>>>>>>>>  " + sResultado);
		}
		JsonObject joObj = new JsonObject();
		joObj.add("result", jaJasonArray);
		
		sResultado = joObj.toString();

		if(bDebug){
			System.out.println("sResultado final: " + sResultado);
		}
		return sResultado;
		
	} // sUpdateRecord
	
	
	public String sGetInsertForm(HashMap<String, Object> map) throws SQLException{

		//String sParametros		= (String)map.get("DYN_CTL_PARAMETROS");
		int iIdEmpresa 			= Integer.parseInt((String)map.get("DYN_CTL_ID_EMPRESA"));
		Usuario  usuario 		= (Usuario)map.get("DYN_CTL_USUARIO");
		String sCveOperacion	= (String) map.get("DYN_CTL_TIPO_OPERACION");
		int iIdTabla			= Integer.parseInt((String)map.get("DYN_CTL_ID_TABLA"));
		boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
		int iIdRol				= usuario.iIdRol;
		//String sCveUsuario		= usuario.sCveUsuario; /// 
		Object[] params 		= new Object[0];
		
		DatabaseMetaData mtdata = conn.getMetaData();
		String sDBProduct 		= mtdata.getDatabaseProductName();

		String sResultado 		= "";
		Gson gson 				= new Gson();
		JsonArray jaJasonArray 	= new JsonArray();
		JsonObject joObj 		= new JsonObject();
		JsonObject joObjList	= null;
		Integer iIdList  		= null;
		List<Integer> lLista 	= new ArrayList<Integer>();
		
		
		sSentenciaSql =	"SELECT	 A.TITULO_PANTALLA AS A, O.DESC_OPERACION AS B, A.IS_INSERT_DISABLED AS C, 		\n " + 
						"        A.ID_TABLA AS G, P.ADDITIONAL_PLUGIN AS H, A.ID_EMPRESA AS I,					\n " + 
						"		 NVL(NVL(A.FONT_LABEL_COLOUR, E.FONT_LABEL_COLOUR),'#2679b5') AS L				\n " +
						"FROM    AOS_ARQ_PANTALLA_TABLA A				\n " +
						"	 JOIN AOS_ARQ_EMPRESA E 					\n " +
						"		 ON A.ID_EMPRESA = E.ID_EMPRESA 		\n " +
						" 	 JOIN AOS_ARQ_TIPO_OPERACION_DESC O 		\n " +
						" 		 ON O.ID_EMPRESA = A.ID_EMPRESA			\n " +
						" 		 AND O.CVE_OPERACION = ? 				\n " +
						"  	 LEFT JOIN AOS_ARQ_PANTALLA_TABLA_ADDP P 	\n " +
						"       ON 	P.ID_TABLA = A.ID_TABLA 			\n " +
						"       AND P.ID_EMPRESA = O.ID_EMPRESA			\n " +
						"       AND P.CVE_OPERACION = O.CVE_OPERACION	\n " +
						"       AND P.SIT_PLUGIN = 'AC' 				\n " +
						"WHERE   A.ID_TABLA = ? ";

		if(bDebug){
			System.out.println("  QUERY sGetPantallaJQ GET TABLA EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" sCveOperacion: 	" + sCveOperacion + "\n" +  
					" iIdTabla: 		" + iIdTabla + "\n"); 
		}
		
		params 		= new Object[2];
		params[0] 	= (String)sCveOperacion;
		params[1] 	= (Integer)iIdTabla;
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Tabla: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		boolean bInsertDisabled = (jaJasonArray.get(0).getAsJsonObject().get("C")!=null?jaJasonArray.get(0).getAsJsonObject().get("C").getAsString():"F").equals("V");
		
		if(bInsertDisabled){
			return "La opci�n para actualizaci�n ha sido deshabilitada";
		}
		
		
		joObj.add("datosTabla", jaJasonArray);
		
		// Obtiene los botones 
		joObj.add("datosBoton", jaGetJQButtons(iIdTabla, iIdRol, sCveOperacion, bDebug));
		
		// Obtiene los elementos del Registro 
		sSentenciaSql =	
			"SELECT	 A.ID_ORDEN_ATRIBUTO AS O, A.ATRIBUTO AS A, A.B_OBLIGATORIO AS B, A.ETIQUETA_COMBO_DEPENDE AS C, NULL AS D,  		\n " +			
			"		 A.ETIQUETA AS E, A.B_OPCION_COMBO_NULL AS F, A.ID_LISTADO AS G, A.TEMPLATE_HTML AS H, A.B_MODIFICABLE AS I, 		\n " +
			"		 NULL AS J, A.PLACEHOLDER AS K, TIPO_DATO AS L, A.ORDEN_PK AS M, A.DATE_FORMAT_FRONT AS N,							\n " +
			"        CASE WHEN A.TEMPLATE_HTML = 'ACE_INPUT_TEXT' THEN A.LONGITUD END AS P,	 											\n " +
			"		 CASE WHEN A.ATRIBUTO = 'ID_EMPRESA' AND A.ORDEN_PK IS NOT NULL THEN 												\n " +
						(sDBProduct.equals("MySQL")?" TRIM(CAST(? AS CHAR)) ":"TRIM(TO_CHAR(?))") + 								"	\n " +
			"		 ELSE A.DEFAULT_VAL END AS Q, 																						\n " +
			"        A.ID_PARENT_TABLE AS R, A.PARENT_ATTRIBUTE AS S, A.SELECT_PARENT_ATTRIBUTE AS T, A.ETIQUETA_COMBO_DEPENDE AS U, 	\n " +
			" 		 A.SELECT_PARENT_EVENT_BIND AS V, A.SELECT_PARENT_ATTRIBUTE_NAME AS W, A.IFRAME_URL	AS X, A.LABEL_SIZE_DIV AS Y, 	\n " +
			" 		 A.INPUT_SIZE_DIV AS Z, A.VALUE_FROM_SELECT AS AA, 																	\n " +
			"  		 CASE WHEN A.IS_SHOWN_ON_CREATE_DEF_VAL = 'V' THEN 'V' END AS AB, A.NUM_ROWS_TXT_AREA AS AC							\n " +
			"FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A							\n " +
			"    LEFT JOIN AOS_SECU_ROL_PANT_TABLA_EX_CAM E   				\n " +
			" 		 ON  E.ID_ATTRIBUTE 	= A.ID_ATRIBUTO 				\n " +
			"		 AND E.ID_ROL			= ? 							\n " +
			"WHERE   A.ID_TABLA 			= ? 							\n " +
            "    AND A.TIPO_ATRIBUTO 		= 'ALT_MOD'						\n " +
            " 	 AND A.SIT_ATRIBUTO 		= 'AC'							\n " +
            "    AND NOT(E.ID_ATTRIBUTE IS NOT NULL AND E.B_CREATE = 'F') 	\n " +
			"	 AND NVL(A.ID_ROL, ?) 		= ? 							\n " +
			"	 AND CASE WHEN NVL(A.IS_SYSDATE_ON_CREATE,'F') = 'V' " +
            "		OR NVL(A.IS_SYSUSER_ON_CREATE,'F') = 'V' THEN " +
            "			'V' ELSE 'F' END <> 'V'								\n " +
            "    AND NVL(A.IS_NOT_CREATED,'F') <> 'V'						\n " +
            "    AND A.SEQUENCE_AUTO_INCREMENTA IS NULL 					\n " +
            "	 AND (								 						\n " +
            "			NVL(A.IS_DEF_VAL_ON_CREATE,'F') <> 'V'  			\n " +
            "			OR	NVL(A.IS_SHOWN_ON_CREATE_DEF_VAL,'F') = 'V')	\n " +
            "    -- AND A.ATRIBUTO <> 'ID_EMPRESA'							\n " +
			"    -- AND NVL(A.IS_TABLE_FIELD,'V') = 'V'						\n " +
			"ORDER BY A.ID_ORDEN_ATRIBUTO									\n " ; 

		if(bDebug){
			System.out.println("  QUERY sGetInsertForm ATRIBUTOS EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdTabla: 	" + iIdTabla + "\n" +
					" iIdEmpresa: 	" + iIdEmpresa + "\n"); 
		}
		
		params 		= new Object[5];
		params[0] 	= (Integer)iIdEmpresa; 
		params[1] 	= (Integer)iIdRol; 
		params[2] 	= (Integer)iIdTabla;
		params[3] 	= (Integer)iIdRol; 
		params[4] 	= (Integer)iIdRol; 

		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Atributos: " + sResultado);
		}
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		for (int i = 0, size = jaJasonArray.size(); i < size; i++){
			joObjList 			= jaJasonArray.get(i).getAsJsonObject();
			String sTemplate	= joObjList.get("H").getAsString();
			if(bDebug){
				System.out.println("Atributos en alta: " + joObjList.get("A").getAsString());
			}
			if(joObjList.get("G")!=null){
				iIdList		= joObjList.get("G").getAsInt(); 
				if(bDebug){
					System.out.println("Valor sel Listado: " + iIdList);
				}
				if(iIdList!=null&&!lLista.contains(iIdList)&&!sTemplate.equals("ACE_INPUT_SELECT_FEED")){
					if(joObjList.get("T")==null)
					lLista.add(iIdList);
				}
			}
			String sDefaultVal = (joObjList.get("Q")!=null?joObjList.get("Q").getAsString():"");
			if(!sDefaultVal.equals("")&&sDefaultVal.startsWith("**")){
				jaJasonArray.get(i).getAsJsonObject().remove("Q"); 
				jaJasonArray.get(i).getAsJsonObject().addProperty("Q", sGetVal(sDefaultVal.substring(2), bDebug));  
			}
			if(bDebug){
				System.out.println("joObjList.get(L): " + joObjList.get("L").getAsString() + "  joObjList.get(H): " + joObjList.get("H").getAsString());
			}
		}

		
		if(bDebug){
			System.out.println("Tama�o del Listado: " + lLista.size());
		}
		
		joObj.add("datosAtribs", jaJasonArray);
		
		
		if(lLista.size() > 0){
			
			joObjList = new JsonObject();
			
			for(int element : lLista) {
				if(map.containsKey("DYN_CTL_ID_LISTADO")){
					map.replace("DYN_CTL_ID_LISTADO", String.valueOf(element));
				}else{
					map.put("DYN_CTL_ID_LISTADO", String.valueOf(element));
				} 				
				sResultado 		= sGetSelectOptions(map); 
				jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
				joObjList.add(Integer.toString(element), jaJasonArray);
				if(bDebug){System.out.println("Elemento en loop lLista: " + element);}
			}
			
			joObj.add("datosListados", joObjList);
			if(bDebug){
				System.out.println("sResultado Datos joObjList: " + joObjList.toString());
			}
		}
		
		// we get the validation of attributes 
		sSentenciaSql =	"SELECT  CASE WHEN A.TIPO_TEMPLATE = 'LINK_POPUP' AND A.ID_PANTALLA_LINK IS NOT NULL THEN 						\n " +
						"			'CTL_DESC_'||A.ATRIBUTO ELSE A.ATRIBUTO END AS A, A.TIPO_TEMPLATE AS B, 							\n " +
						"		 A.TIPO_DATO AS C, VAL.VALIDATION_KEY AS D, VF.ARGUMENTS AS E, VAL.VALUE_1 AS F,  						\n " +
				        "        VAL.VALUE_2 AS G, NVL(VAL.TX_MESSAGE_VALIDATION, VF.MESSAGE_VALIDATION) AS H, 							\n " +
				        "        VF.EXPRESSION_VALIDATION AS I, A.B_APLICA_RANGO_FILTRO	AS J											\n " +
				        "FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A																			\n " +
				        "    JOIN AOS_ARQ_ATRIBUTE_VALIDATION VAL																		\n " +
				        "        ON  A.ID_ATRIBUTO       = VAL.ID_ATTRIBUTE																\n " +	
				        "    JOIN AOS_ARQ_VALIDATION_FORM VF																			\n " +
				        "        ON  VAL.VALIDATION_KEY  = VF.VALIDATION_KEY															\n " +
				        "WHERE   A.ID_EMPRESA        = ?																				\n " +
				        "    AND A.ID_TABLA          = ?																				\n " +
				        "    AND A.TIPO_ATRIBUTO     = 'ALT_MOD'  																		\n " +	
				        "    AND A.SIT_ATRIBUTO      = 'AC'																				\n " +
				        "    AND A.TIPO_TEMPLATE     <> 'INPUT_CHECKBOX'																\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND 'ALT_MOD' = 'UPDATE' AND NVL(A.IS_SYSDATE_ON_UPDATE,'F') = 'V')	\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND 'ALT_MOD' = 'UPDATE' AND NVL(A.IS_SYSUSER_ON_UPDATE,'F') = 'V')	\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND 'ALT_MOD' = 'CREATE' AND NVL(A.IS_SYSDATE_ON_CREATE,'F') = 'V')	\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND 'ALT_MOD' = 'CREATE' AND NVL(A.IS_SYSUSER_ON_CREATE,'F') = 'V')	\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND 'ALT_MOD' = 'UPDATE' AND NVL(A.B_MODIFICABLE,'V') = 'F')			\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND NVL(A.IS_SYS_ADMIN,'F') = 'V')									\n " +
				        "    AND NOT (A.TIPO_ATRIBUTO = 'ALT_MOD' AND A.SEQUENCE_AUTO_INCREMENTA IS NOT NULL)							\n " +
				        "    AND A.ATRIBUTO <> 'ID_EMPRESA'																				\n " +
				        "    AND NVL(A.IS_DISPLAY_FOREIGN_DATA,'F') <> 'V'																\n " +
						"    AND NVL(A.IS_TABLE_FIELD,'V') = 'V'																		\n " +
				        "ORDER BY A.ID_ORDEN_ATRIBUTO																					\n " ;	
		
		params 		= new Object[2];
		params[0] 	= (Integer)iIdEmpresa; 
		params[1] 	= (Integer)iIdTabla;
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado validation Atributos: " + sResultado);
		}
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		joObj.add("datosValidation", jaJasonArray);
		
		sResultado = joObj.toString();

		if(bDebug){
			System.out.println("sResultado: " + sResultado);
		}
		return sResultado;
	}  // sGetInsertForm
	
	
	public String sGetMensajeErrorDb(int iIdError, String sError, boolean bDebug) throws SQLException{
		
		String sMensaje = ""; 
		
		sSentenciaSql = " 	SELECT  DES_ERROR_MESSAGE			\n "  +
					    "   FROM    AOS_ARQ_ERROR_DB_MESSAGE  	\n " +
					    " 	WHERE 	ID_ERROR_DB	= ? ";

		if(bDebug){
			System.out.println("  QUERY sGetMensajeErrorDb EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
				" iIdLogerror: 	" + iIdError +  " \n");
		}

		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdError);
		rs = pstmt.executeQuery();

		if(rs.next()){
			sMensaje	= rs.getString("DES_ERROR_MESSAGE");
		}else{
			
			sSentenciaSql = " 	INSERT INTO AOS_ARQ_ERROR_DB_MESSAGE(ID_ERROR_DB, DES_ERROR_MESSAGE) VALUES (?,?) ";

			if(bDebug){
				System.out.println("  QUERY sGetMensaje no encontrado EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n");
			}
		
			pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1, iIdError);
			pstmt.setString(2, sError);
			rs = pstmt.executeQuery();
			
			sMensaje	= sError;
		}

		rs.close();
	    pstmt.close();
		return sMensaje;
	} // sGetMensajeErrorDb
	
	

	public String sGetMensaje(int iIdError, boolean bDebug) throws SQLException{
		
		String sMensaje = ""; 
		
		sSentenciaSql = " 	SELECT  MENSAJE  					\n "  +
					    "   FROM    AOS_ARQ_MENSAJE_SISTEMA  	\n " +
					    " 	WHERE 	ID_MENSAJE	= ? ";

		if(bDebug){
			System.out.println("  QUERY sGetMensaje EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
				" iIdLogerror: 	" + iIdError +  " \n");
		}

		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdError);
		rs = pstmt.executeQuery();

		if(rs.next()){
			sMensaje	= rs.getString("MENSAJE");
		}else{
			
			sSentenciaSql = " 	SELECT  MENSAJE  					\n "  +
						    "   FROM    AOS_ARQ_MENSAJE_SISTEMA  	\n " +
						    " 	WHERE 	ID_MENSAJE	= ? ";

			if(bDebug){
				System.out.println("  QUERY sGetMensaje no encontrado EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n");
			}
		
			pstmt = conn.prepareStatement(sSentenciaSql);
			pstmt.setInt(1, 0);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				sMensaje	= rs.getString("MENSAJE") + iIdError;
			}
		}

		rs.close();
	    pstmt.close();
		return sMensaje;
	} // sGetMensaje
	
	public String sGetVal(String sQuery, boolean bDebug) throws SQLException{
		
		String sResultado = ""; 
		sSentenciaSql = sQuery;
		
		if(bDebug){
			System.out.println("  QUERY sGetVal EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sQuery);
		}

		pstmt = conn.prepareStatement(sSentenciaSql);
		rs = pstmt.executeQuery();

		if(rs.next()){
			sResultado	= rs.getString("RESULT");
		}

		rs.close();
	    pstmt.close();
		return sResultado;
	}  // sGetVal
	
	public String sGetSeqNextVal(String sSequence, boolean bDebug) throws SQLException{
		
		String sNextval = ""; 
		DatabaseMetaData mtdata = conn.getMetaData();
		String sDBProduct 		= mtdata.getDatabaseProductName();
		
		
		if(sDBProduct.equals("MySQL")){
			sSentenciaSql = "SELECT  NEXTVAL(" + sSequence + ") AS SEQ_NEXTVAL ";
		}else{
			sSentenciaSql = "SELECT  " + sSequence + ".NEXTVAL AS SEQ_NEXTVAL FROM DUAL";			
		}
		
		if(bDebug){
			System.out.println("  QUERY sGetSeqNextVal EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
		}

		pstmt = conn.prepareStatement(sSentenciaSql);
		rs = pstmt.executeQuery();

		if(rs.next()){
			sNextval	= rs.getString("SEQ_NEXTVAL");
		}

		rs.close();
	    pstmt.close();
		return sNextval;
	} // sGetSeqNextVal
	
	
	public LinkedHashMap<String, String> getEmailElements(int iIdEmpresa, int iIdUsuario, boolean bDebug) throws SQLException{

		LinkedHashMap <String, String> opciones = new LinkedHashMap<String, String>();

		sSentenciaSql = "SELECT PASSWORD, EMAIL_TEMPLATE, NOM_USUARIO, GENERO, IMG_PATH FROM USUARIO_EVENTO JOIN AOS_ARQ_EMAIL_TEMPLATE ON 1=1 WHERE ID_EMPRESA = ? AND ID_USUARIO = ? ";

		if(bDebug){
			System.out.println("  QUERY getEmailElements EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
				" iIdEmpresa: 	" + iIdEmpresa 	+ "\n" +
				" iIdUsuario: 	" + iIdUsuario 	+ "\n");
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setInt(2, iIdUsuario);
		rs = pstmt.executeQuery();

		if(rs.next()){
			opciones.put("PASSWORD", rs.getString("PASSWORD"));
			opciones.put("TEMPLATE", rs.getString("EMAIL_TEMPLATE"));
			opciones.put("NOM_USUARIO", rs.getString("NOM_USUARIO"));
			opciones.put("GENERO", rs.getString("GENERO"));
			opciones.put("IMG_PATH", rs.getString("IMG_PATH"));
			if(bDebug){
				System.out.println(
					" -------iIdEmpresa: 	" + rs.getString("PASSWORD") 	+ "\n" +
					" -------TEMPLATE: 	" + rs.getString("EMAIL_TEMPLATE") 	+ "\n" +
					" -------NOM_USUARIO: 	" + rs.getString("NOM_USUARIO") 	+ "\n" +
					" -------IMG_PATH: 	" + rs.getString("IMG_PATH") 	+ "\n");
			}
		}

		rs.close();
	    pstmt.close();
		return opciones;
		

	} // getEmailElements
	
	
	
	
	public String sGetPasswordUserMsgs(int iIdEmpresa, int iIdUsuario, boolean bDebug) throws SQLException{
		
		String sPassword = ""; 
		
		sSentenciaSql = "SELECT PASSWORD, EMAIL_TEMPLATE FROM USUARIO_EVENTO JOIN AOS_EMAIL_TEMPLATE ON 1=1 WHERE ID_EMPRESA = ? AND ID_USUARIO = ? ";

		if(bDebug){
			System.out.println("  QUERY sGetPasswordUserMsgs EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
		}

		pstmt = conn.prepareStatement(sSentenciaSql);
		pstmt.setInt(1, iIdEmpresa);
		pstmt.setInt(2, iIdUsuario);
		rs = pstmt.executeQuery();

		if(rs.next()){
			sPassword	= rs.getString("PASSWORD");
		}

		rs.close();
	    pstmt.close();
		return sPassword;
	} // sGetPasswordUserMsgs
	

	public String sInsertRecord(HashMap<String, Object> map) throws SQLException{

		int iIdEmpresa 			= Integer.parseInt((String)map.get("DYN_CTL_ID_EMPRESA"));
		Usuario  usuario 		= (Usuario)map.get("DYN_CTL_USUARIO");
		String sCveOperacion	= (String) map.get("DYN_CTL_TIPO_OPERACION");
		int iIdTabla			= Integer.parseInt((String) map.get("DYN_CTL_ID_TABLA"));
		boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
		int iIdRol				= usuario.iIdRol;
		String sCveUsuario		= usuario.sCveUsuario; 
		Object[] params 		= new Object[0];
		String sKeyGetRecord	= "";
		
		String sResultado 		= "";
		Gson gson 				= new Gson();
		JsonArray jaJasonArray 	= new JsonArray();
		JsonObject joObj 		= new JsonObject();
		
		sSentenciaSql =	"SELECT	 A.IS_INSERT_DISABLED AS A, T.TABLA AS B	\n " + 
						"FROM    AOS_ARQ_PANTALLA_TABLA A					\n " +
						"  	 JOIN AOS_ARQ_TABLA T  							\n " +
						"       ON 	A.ID_TABLA_ORIGEN = T.ID_TABLA 			\n " +
						"WHERE   A.ID_TABLA = ? 							\n " ;

		if(bDebug){
			System.out.println("  QUERY sInsertRecord GET TABLA EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" sCveOperacion: 	" + sCveOperacion + "\n" +  
					" iIdTabla: 		" + iIdTabla + "\n"); 
		}
		
		params 		= new Object[1];
		params[0] 	= (Integer)iIdTabla;
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Tabla: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		String sNomTabla 		= jaJasonArray.get(0).getAsJsonObject().get("B").getAsString();
		boolean bInsertDisabled = (jaJasonArray.get(0).getAsJsonObject().get("A")!=null?jaJasonArray.get(0).getAsJsonObject().get("A").getAsString():"F").equals("V");
		
		if(bInsertDisabled){
			return sGetMensaje(-1, bDebug);
		}
		
		
		// We get the attributes of the table
		sSentenciaSql =	
			"SELECT	 A.ID_ORDEN_ATRIBUTO AS O, A.ATRIBUTO AS A, A.ORDEN_PK AS B, A.IS_SYSDATE_ON_CREATE AS C, 				\n " +
			"		 A.IS_SYSUSER_ON_CREATE AS D, A.TIPO_DATO AS E, A.DATE_FORMAT AS F, A.SEQUENCE_AUTO_INCREMENTA AS G, 	\n " +
			"		 A.IS_DEF_VAL_ON_CREATE AS H, A.DEFAULT_VAL AS I, A.IS_PASSWORD_ENCRYPTED AS J, A.TIPO_TEMPLATE AS K,	\n " +
			"		 A.B_OBLIGATORIO AS L 	\n " +
			"FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A							\n " +
			"    LEFT JOIN AOS_SECU_ROL_PANT_TABLA_EX_CAM E   				\n " +
			" 		 ON  E.ID_ATTRIBUTE 	= A.ID_ATRIBUTO 				\n " +
			"		 AND E.ID_ROL			= ? 							\n " +
			"WHERE   A.ID_TABLA 			= ? 							\n " +
            "    AND A.TIPO_ATRIBUTO 		= 'ALT_MOD'						\n " +
            " 	 AND A.SIT_ATRIBUTO 		= 'AC'							\n " +
            "    AND NOT(E.ID_ATTRIBUTE IS NOT NULL AND E.B_CREATE = 'F') 	\n " +
			"	 AND NVL(A.ID_ROL, ?) 		= ? 							\n " +
			"    AND NVL(A.IS_NOT_CREATED,'F') <> 'V'						\n " +
			"    AND NVL(A.IS_TABLE_FIELD,'V') = 'V'						\n " +
			"ORDER BY A.ORDEN_PK, A.ID_ORDEN_ATRIBUTO						\n " ; 
		
		
		
		if(bDebug){
			System.out.println("  QUERY sInsertRecord ATRIBUTOS EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdTabla: 	" + iIdTabla + "\n" +
					" iIdEmpresa: 	" + iIdEmpresa + "\n"); 
		}
		
		params 		= new Object[4];
		params[0] 	= (Integer)iIdRol; 
		params[1] 	= (Integer)iIdTabla;
		params[2] 	= (Integer)iIdRol; 
		params[3] 	= (Integer)iIdRol; 
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Atributos: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		String sColumnas 	= "";
		String sValues		= ""; 
		String sAttributesPK= ""; 
		
		for (int i = 0, size = jaJasonArray.size(); i < size; i++){
			String sAttributeName	= jaJasonArray.get(i).getAsJsonObject().get("A").getAsString();
			boolean bIsSysUsrCreate	=(jaJasonArray.get(i).getAsJsonObject().get("D")!=null?jaJasonArray.get(i).getAsJsonObject().get("D").getAsString().equals("V"):false);
			boolean bIsSysDateCreate=(jaJasonArray.get(i).getAsJsonObject().get("C")!=null?jaJasonArray.get(i).getAsJsonObject().get("C").getAsString().equals("V"):false);
			String sDataType		= jaJasonArray.get(i).getAsJsonObject().get("E").getAsString(); 
			String sDateFormat		=(jaJasonArray.get(i).getAsJsonObject().get("F")!=null?jaJasonArray.get(i).getAsJsonObject().get("F").getAsString():"");
			String sSequence		=(jaJasonArray.get(i).getAsJsonObject().get("G")!=null?jaJasonArray.get(i).getAsJsonObject().get("G").getAsString():"");
			boolean sIsDefValCreate	=(jaJasonArray.get(i).getAsJsonObject().get("H")!=null?jaJasonArray.get(i).getAsJsonObject().get("H").getAsString().equals("V"):false);
			String sDefaultVal		=(jaJasonArray.get(i).getAsJsonObject().get("I")!=null?jaJasonArray.get(i).getAsJsonObject().get("I").getAsString():"");
			boolean sIsPassEncrypt	=(jaJasonArray.get(i).getAsJsonObject().get("J")!=null?jaJasonArray.get(i).getAsJsonObject().get("J").getAsString().equals("V"):false);
			String sTipoTemplate	= jaJasonArray.get(i).getAsJsonObject().get("K").getAsString();
			boolean bObligatorio	= jaJasonArray.get(i).getAsJsonObject().get("L").getAsString().equals("V");
			
			if(sDataType.equals("NUMBER")&&
					sSequence!=null&&!sSequence.equals("")){
				String sSeqVal 		= sGetSeqNextVal(sSequence,bDebug); 
				sValues 			= sValues 	+ (sValues.equals("")	?"":", ") 	+ " " + sSeqVal;
				if(jaJasonArray.get(i).getAsJsonObject().get("B")!=null){
					sKeyGetRecord 	= sKeyGetRecord + (sKeyGetRecord.equals("")?"":"^$") + sSeqVal; 
					sAttributesPK 	= sAttributesPK + (sAttributesPK.equals("")?"":"|") + sAttributeName + "!" + sDataType + (!sDateFormat.equals("")?"!" + sDateFormat:""); 
				}
				sColumnas 				= sColumnas + (sColumnas.equals("")	?"":", ") + sAttributeName;
				map.put(sAttributeName, sSeqVal);
			}else{
				if(!sDefaultVal.equals("")&&sIsDefValCreate){
					switch (sDataType){
						case "NUMBER": 
							sValues 			= sValues 	+ (sValues.equals("") ? "" : ", ") 	+ sDefaultVal;
							break;
						case "VARCHAR": 
							if(sIsPassEncrypt){
								sValues 			= sValues 	+ (sValues.equals("")	?"":", ") 	+ "CONTROL.PKG_CRYPTO('" + sDefaultVal.replace("'", "''") + "')";
							}else{
								sValues 			= sValues 	+ (sValues.equals("")	?"":", ") 	+ "'" + sDefaultVal.replace("'", "''") + "'";
							}
							break;
						case "DATE": 
							sValues 			= sValues 	+ (sValues.equals("")	?"":", ") 	+ " TO_DATE('" + sDefaultVal + "','" + sDateFormat + "')";
							break;
					}
					if(jaJasonArray.get(i).getAsJsonObject().get("B")!=null){
						sKeyGetRecord 	= sKeyGetRecord + (sKeyGetRecord.equals("")?"":"^$") + (sDefaultVal);
						sAttributesPK 	= sAttributesPK + (sAttributesPK.equals("")?"":"|") + sAttributeName + "!" + sDataType + (!sDateFormat.equals("")?"!" + sDateFormat:""); 
					}	
					sColumnas 			= sColumnas + (sColumnas.equals("")	?"":", ") + sAttributeName;
				}else{
					if(map.containsKey(sAttributeName)||bIsSysDateCreate||bIsSysUsrCreate||sTipoTemplate.equals("INPUT_CHECKBOX")){
						if(sTipoTemplate.equals("INPUT_CHECKBOX")){
							if(map.containsKey(sAttributeName)){
								sValues 	= sValues 	+ (sValues.equals("")	?"":", ") 	+ " 'V'";
								sColumnas 	= sColumnas + (sColumnas.equals("")	?"":", ") + sAttributeName;
							}else{
								if(bObligatorio){
									sValues 	= sValues 	+ (sValues.equals("")	?"":", ") 	+ " 'F'";
									sColumnas 	= sColumnas + (sColumnas.equals("")	?"":", ") + sAttributeName;
								}
							}
						}else{
							if(sIsPassEncrypt){
								sValues 			= sValues 	+ (sValues.equals("")	?"":", ") 	+ "CONTROL.PKG_CRYPTO.ENCRYPT(?)";
							}else{
								sValues 			= sValues 	+ (sValues.equals("")	?"":", ") 	+ " ?";
							}
							sColumnas 				= sColumnas + (sColumnas.equals("")	?"":", ") + sAttributeName;
						}
					}
					if(jaJasonArray.get(i).getAsJsonObject().get("B")!=null){
						sKeyGetRecord 	= sKeyGetRecord + (sKeyGetRecord.equals("")?"":"^$") + ((String)map.get(sAttributeName));
						sAttributesPK 	= sAttributesPK + (sAttributesPK.equals("")?"":"|") + sAttributeName + "!" + sDataType + (!sDateFormat.equals("")?"!" + sDateFormat:""); 
					}
				}
			}
			if(bDebug){
				System.out.println("DAO: insert ATRIBUTO -->>> " + sAttributeName);
				System.out.println("DAO: insert sIsPassEncrypt -->>> " + sIsPassEncrypt);
				System.out.println("DAO: insert sDefaultVal -->>> " + sDefaultVal);
				System.out.println("DAO: insert sIsDefValCreate -->>> " + sIsDefValCreate);
				System.out.println("DAO: insert sColumnas -->>> " + sColumnas);
				System.out.println("DAO: insert sValues -->>> " + sValues);
				System.out.println("************************************************************");
			}
		}

		sKeyGetRecord = String.valueOf(iIdTabla) + "^$^" + sKeyGetRecord + "^$^" + sAttributesPK; 
		
		sSentenciaSql = "INSERT INTO " 	+ sNomTabla 	+ "( 		\n " + 
						"       " 		+ sColumnas		+ ")		\n " + 
						"VALUES ("		+ sValues		+ ")		\n " ;

		if(bDebug){
			System.out.println("  sKeyGetRecord ----->>>>>>>>>>>>>>>>>>> " + sKeyGetRecord);
			System.out.println("  QUERY DE INSERCION EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
		}

		pstmt = conn.prepareStatement(sSentenciaSql);

		int icount = 1; 
		for (int i = 0, size = jaJasonArray.size(); i < size; i++){

			String sAttributeName = jaJasonArray.get(i).getAsJsonObject().get("A").getAsString();
			if(bDebug){
				System.out.println(" En loop asigna paramatros ----->>>>>>>>>>>>>>>>>>>  " + icount + "  -  " + sAttributeName);
			}

			String sDefaultVal		=(jaJasonArray.get(i).getAsJsonObject().get("I")!=null?jaJasonArray.get(i).getAsJsonObject().get("I").getAsString():"");
			boolean bIsDefValCreate	=(jaJasonArray.get(i).getAsJsonObject().get("H")!=null?jaJasonArray.get(i).getAsJsonObject().get("H").getAsString().equals("V"):false);
			boolean bIsSysdateCreate=(jaJasonArray.get(i).getAsJsonObject().get("C")!=null?jaJasonArray.get(i).getAsJsonObject().get("C").getAsString().equals("V"):false);
			boolean bIsUserCreate	=(jaJasonArray.get(i).getAsJsonObject().get("D")!=null?jaJasonArray.get(i).getAsJsonObject().get("D").getAsString().equals("V"):false);
			String sTipoTemplate	= jaJasonArray.get(i).getAsJsonObject().get("K").getAsString();
			boolean bObligatorio	= jaJasonArray.get(i).getAsJsonObject().get("L").getAsString().equals("V");
			
			if(!(!sDefaultVal.equals("")&&bIsDefValCreate)&&(map.containsKey(sAttributeName)||bIsSysdateCreate||bIsUserCreate)){
				
				if(bDebug){
					System.out.println(" Dentro de no es default ");
				}
				switch (jaJasonArray.get(i).getAsJsonObject().get("E").getAsString()){
					case "NUMBER":
						if(jaJasonArray.get(i).getAsJsonObject().get("G")!=null&&!jaJasonArray.get(i).getAsJsonObject().get("G").getAsString().equals("")){
							if(bDebug){
								System.out.println(" No considera el campo, tiene secuencial ");
							}
						}else{
							if(map.get(sAttributeName)!=null&&!((String)map.get(sAttributeName)).equals("")){
								pstmt.setFloat(icount, Float.valueOf((String)map.get(sAttributeName)));
							}else{
								pstmt.setNull(icount, Types.NUMERIC);
							}
							if(bDebug){
								System.out.println("sAttributeName -->>>  " + sAttributeName + "  -  " + icount);
							}
							icount = icount + 1;
						}
						break;
					case "VARCHAR":
						if(bIsUserCreate){
							pstmt.setString(icount, sCveUsuario);
						}else{
							if(!sTipoTemplate.equals("INPUT_CHECKBOX")){
								if(map.get(sAttributeName)!=null&&!((String)map.get(sAttributeName)).equals("")){
									pstmt.setString(icount, (String)map.get(sAttributeName));
								}else{
									pstmt.setNull(icount, Types.VARCHAR);
								}
							}else{
								if(map.containsKey(sAttributeName)&&((String)map.get(sAttributeName)).equals("on")){
									pstmt.setString(icount, "V");
								}else{
									if(bObligatorio){
										pstmt.setString(icount, "F");
									}else{
										pstmt.setString(icount, "");
									}
								}
							}
						}
						if(bDebug){
							System.out.println("sAttributeName -->>>  " + sAttributeName + "  -  " + icount);
						}
						icount = icount + 1;
						break;
					case "DATE":
						try {
							if(bIsSysdateCreate){
								Calendar cal = Calendar.getInstance();
								Timestamp  timestamp = new java.sql.Timestamp(cal.getTimeInMillis());
					            pstmt.setTimestamp(icount, timestamp);
							}else{
								if(map.get(sAttributeName)!=null&&!((String)map.get(sAttributeName)).equals("")){
									SimpleDateFormat formatter 	= new SimpleDateFormat(jaJasonArray.get(i).getAsJsonObject().get("F").getAsString());
									String sDate				= (String)map.get(sAttributeName);
									if(sDate!=null&&!sDate.equals("")){
							            Date date 					= formatter.parse(sDate);
							            long time   				= date.getTime(); 
							            Timestamp timestamp 		= new Timestamp(time); 
							            pstmt.setTimestamp(icount, timestamp);
									}else{
										pstmt.setNull(icount, Types.DATE);
									}
								}else{
									pstmt.setNull(icount, Types.DATE);
								}
							}
							if(bDebug){
								System.out.println("sAttributeName -->>>  " + sAttributeName + "  -  " + icount);
							}
							icount = icount + 1;
				        } catch (ParseException e) {
				            e.printStackTrace();
				        }
						break;
				}
			}
		}
		if(bDebug){
			System.out.println("  icount final - nueva ---->>>>>>>>>>>>>>>>>>>  " + icount);
		}
		
		rs = pstmt.executeQuery();

		rs.close();
	    pstmt.close();
	    
	    if(iIdTabla==499&&usuario.sUserSchema.equals("VSTREAM")){
	    	try{
		    	int iIdusuario 		= Integer.parseInt((String)map.get("ID_USUARIO"));
		    	LinkedHashMap<String, String> mapEmailElems = getEmailElements(iIdEmpresa, iIdusuario, bDebug);
	    		if(bDebug){
					System.out.println(" sNomUsuario antes ----->>>>>> " + (String)mapEmailElems.get("NOM_USUARIO"));
				}
		    	//String sPassword 	= sGetPasswordUserMsgs(iIdEmpresa, iIdusuario, bDebug);
		    	String sPassword 	= (String)mapEmailElems.get("PASSWORD");
		    	String sEmail 		= (String)map.get("CORREO_ELECTRONICO");
		    	String sNomUsuario  = (String)mapEmailElems.get("NOM_USUARIO");
		    	String sImgPath		= (String)mapEmailElems.get("IMG_PATH");
		    	String sGenero		= (String)mapEmailElems.get("GENERO");
		    	sNomUsuario			= (sGenero.equals("H")?"Estimado ":"Estimada ") + sNomUsuario;
		    	//sNomUsuario			= sNomUsuario.replace("�", "&aacute;").replace("�", "&aacute;").replace("�", "&iacute;").replace("�", "&oacute;").replace("�", "&uacute;")
		    		//						.replace("�", "&Aacute;").replace("�", "&Eacute;").replace("�", "&Iacute;").replace("�", "&Oacute;").replace("�", "&Uacute;");
		    	
		    	sNomUsuario = StringEscapeUtils.escapeHtml4(sNomUsuario); 
		    	
		    	if(bDebug){
					System.out.println(" iIdusuario ----->>>> " + iIdusuario);
					System.out.println(" sPassword ----->>>>>> " + sPassword);
					System.out.println(" sEmail ----->>>>>> " + sEmail);
					System.out.println(" sImgPath ----->>>>>> " + sImgPath);
					System.out.println(" sNomUsuario ----->>>>>> " + sNomUsuario);
				}
		    	String sContent = (String)mapEmailElems.get("TEMPLATE");     
		    	
		    	sContent = sContent.replace("<!--CLIENTE-->", sNomUsuario);
		    	sContent = sContent.replace("<!--USUARIO-->", String.valueOf(iIdusuario));
		    	sContent = sContent.replace("<!--PASSWORD-->", sPassword);
		    	
		    	EnviaCorreo enviaEmail = new EnviaCorreo();    
			    enviaEmail.doSendMail("mikerugerio@gmail.com", "gayosso6974", sEmail, "Env�o de Credenciales", sContent, sImgPath);
	    	} catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	    
	    if(iIdTabla==496&&usuario.sUserSchema.equals("ASSETADMr")){
	    	try{
		    	int iIdusuario 		= Integer.parseInt((String)map.get("ID_USUARIO"));
		    	LinkedHashMap<String, String> mapEmailElems = getEmailElements(iIdEmpresa, iIdusuario, bDebug);
	    		if(bDebug){
					System.out.println(" sNomUsuario antes ----->>>>>> " + (String)mapEmailElems.get("NOM_USUARIO"));
				}
		    	//String sPassword 	= sGetPasswordUserMsgs(iIdEmpresa, iIdusuario, bDebug);
		    	String sPassword 	= (String)mapEmailElems.get("PASSWORD");
		    	String sEmail 		= (String)map.get("CORREO_ELECTRONICO");
		    	String sNomUsuario  = (String)mapEmailElems.get("NOM_USUARIO");
		    	String sImgPath		= (String)mapEmailElems.get("IMG_PATH");
		    	String sGenero		= (String)mapEmailElems.get("GENERO");
		    	sNomUsuario			= (sGenero.equals("H")?"Estimado ":"Estimada ") + sNomUsuario;
		    	//sNomUsuario			= sNomUsuario.replace("�", "&aacute;").replace("�", "&aacute;").replace("�", "&iacute;").replace("�", "&oacute;").replace("�", "&uacute;")
		    		//						.replace("�", "&Aacute;").replace("�", "&Eacute;").replace("�", "&Iacute;").replace("�", "&Oacute;").replace("�", "&Uacute;");
		    	
		    	sNomUsuario = StringEscapeUtils.escapeHtml4(sNomUsuario); 

		    	if(bDebug){
					System.out.println(" iIdusuario ----->>>> " + iIdusuario);
					System.out.println(" sPassword ----->>>>>> " + sPassword);
					System.out.println(" sEmail ----->>>>>> " + sEmail);
					System.out.println(" sImgPath ----->>>>>> " + sImgPath);
					System.out.println(" sNomUsuario ----->>>>>> " + sNomUsuario);
				}
		    	String sContent = (String)mapEmailElems.get("TEMPLATE");     
		    	
		    	sContent = sContent.replace("<!--CLIENTE-->", sNomUsuario);
		    	sContent = sContent.replace("<!--USUARIO-->", String.valueOf(iIdusuario));
		    	sContent = sContent.replace("<!--PASSWORD-->", sPassword);
		    	
		    	EnviaCorreo enviaEmail = new EnviaCorreo();    
			    enviaEmail.doSendMail("mikerugerio@gmail.com", "gayosso6974", sEmail, "Notificaci�n de Incidencia", sContent, sImgPath);
	    	} catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
  
	    joObj.addProperty("Response", sGetMensaje(-4,bDebug));
	    joObj.addProperty("RecordPK", sKeyGetRecord);
	    sResultado = joObj.toString();
	    
	    return sResultado;	
	} // sInsertRecord 

	public String sDeleteRecord(HashMap<String, Object> map) throws SQLException{

		int iIdEmpresa 			= Integer.parseInt((String)map.get("DYN_CTL_ID_EMPRESA"));
		Usuario  usuario 		= (Usuario)map.get("DYN_CTL_USUARIO");
		String sCveOperacion	= (String) map.get("DYN_CTL_TIPO_OPERACION");
		int iIdTabla			= Integer.parseInt((String) map.get("DYN_CTL_ID_TABLA"));
		boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
		boolean bError			= false; 
		int iIdRol				= usuario.iIdRol;
		String sCveUsuario		= usuario.sCveUsuario; 
		Object[] params 		= new Object[0];
		String sMissingKeyFields= ""; 

		
		String sResultado 		= "";
		Gson gson 				= new Gson();
		JsonArray jaJasonArray 	= new JsonArray();
		
		sSentenciaSql =	"SELECT	 A.IS_DELETE_DISABLED AS A, T.TABLA AS B	\n " + 
						"FROM    AOS_ARQ_PANTALLA_TABLA A					\n " +
						"  	 JOIN AOS_ARQ_TABLA T  							\n " +
						"       ON 	A.ID_TABLA_ORIGEN = T.ID_TABLA 			\n " +
						"WHERE   A.ID_TABLA = ? 							\n " ;

		if(bDebug){
			System.out.println("  QUERY sDeleteRecord GET TABLA EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" sCveOperacion: 	" + sCveOperacion + "\n" +  
					" iIdTabla: 		" + iIdTabla + "\n"); 
		}
		
		params 		= new Object[1];
		params[0] 	= (Integer)iIdTabla;
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Tabla: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		boolean bDeleteDisabled = (jaJasonArray.get(0).getAsJsonObject().get("A")!=null?jaJasonArray.get(0).getAsJsonObject().get("A").getAsString():"F").equals("V");
		String sNomTabla 		= jaJasonArray.get(0).getAsJsonObject().get("B").getAsString();
		
		if(bDeleteDisabled){
			return sGetMensaje(-2, bDebug);
		}
		
		// We get the Record's attributes
		sSentenciaSql =	"SELECT	 A.ATRIBUTO AS A, A.ORDEN_PK AS B,   		\n " +			
						"		 A.TIPO_DATO AS E, A.DATE_FORMAT AS F		\n " +		
						"FROM    AOS_ARQ_PANTALLA_TABLA_ATRIB A				\n " +
						"WHERE   A.ID_TABLA 			= ? 				\n " +
                        "    AND A.TIPO_ATRIBUTO 		= 'ALT_MOD'			\n " +
                        " 	 AND A.SIT_ATRIBUTO 		= 'AC'				\n " +
                        "	 AND A.ORDEN_PK IS NOT NULL 					\n " +
						"ORDER BY A.ORDEN_PK								\n " ; 

		if(bDebug){
			System.out.println("  QUERY sDeleteRecord ATRIBUTOS EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
					" iIdTabla: 	" + iIdTabla + "\n" +
					" iIdEmpresa: 	" + iIdEmpresa + "\n"); 
		}
		
		params 		= new Object[1];
		params[0] 	= (Integer)iIdTabla;
		
		sResultado 		= resultSetToJson(conn, sSentenciaSql, params); 
		if(bDebug){
			System.out.println("sResultado Datos Atributos: " + sResultado);
		}
		jaJasonArray 	= new JsonArray();
		jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
		
		String sColumnasPK 	= ""; 
		
		for (int i = 0, size = jaJasonArray.size(); i < size; i++){
			sColumnasPK =  sColumnasPK + (sColumnasPK.equals("")?"":"    AND ") + jaJasonArray.get(i).getAsJsonObject().get("A").getAsString() + " = ?  \n"; 
		}

		sSentenciaSql = "DELETE	FROM " + sNomTabla + " 	\n " + 
						"WHERE 	" + sColumnasPK;

		if(bDebug){
			System.out.println("  QUERY DE BORRADO EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql);
		}

		pstmt = conn.prepareStatement(sSentenciaSql);

		int icount = 1; 
		boolean bMissingKey = false; 
		
		for (int i = 0, size = jaJasonArray.size(); i < size; i++){

			if(bDebug){
				System.out.println("  icount ----->>>>>>>>>>>>>>>>>>>  " + icount);
			}

			switch (jaJasonArray.get(i).getAsJsonObject().get("E").getAsString()){
				case "NUMBER":
					if(map.containsKey(jaJasonArray.get(i).getAsJsonObject().get("A").getAsString())){
						pstmt.setFloat(icount, Float.valueOf((String)map.get(jaJasonArray.get(i).getAsJsonObject().get("A").getAsString())));
						icount = icount + 1;
					}else{
						bMissingKey = true; 
						sMissingKeyFields = sMissingKeyFields + (sMissingKeyFields.equals("")?"":", ") + jaJasonArray.get(i).getAsJsonObject().get("A").getAsString(); 
					}
					break;
				case "VARCHAR":
					if(map.containsKey(jaJasonArray.get(i).getAsJsonObject().get("A").getAsString())){
						pstmt.setString(icount, (String)map.get(jaJasonArray.get(i).getAsJsonObject().get("A").getAsString()));
						icount = icount + 1;
					}else{
						bMissingKey = true; 
						sMissingKeyFields = sMissingKeyFields + (sMissingKeyFields.equals("")?"":", ") + jaJasonArray.get(i).getAsJsonObject().get("A").getAsString();
					}
					break;
				case "DATE":
					if(map.containsKey(jaJasonArray.get(i).getAsJsonObject().get("A").getAsString())){
						try {
							SimpleDateFormat formatter 	= new SimpleDateFormat(jaJasonArray.get(i).getAsJsonObject().get("F").getAsString());
				            Date date 					= formatter.parse((String)map.get(jaJasonArray.get(i).getAsJsonObject().get("A").getAsString()));
				            long time   				= date.getTime(); 
				            Timestamp timestamp 		= new Timestamp(time); 
				            pstmt.setTimestamp(icount, timestamp);
							icount = icount + 1;
				        } catch (ParseException e) {
				            e.printStackTrace();
				        }
					}else{
						bMissingKey = true; 
						sMissingKeyFields = sMissingKeyFields + (sMissingKeyFields.equals("")?"":", ") + jaJasonArray.get(i).getAsJsonObject().get("A").getAsString();
					}
					break;
			}

		}

		if(bMissingKey){
			sResultado 	= sGetMensaje(-2, bDebug) + sMissingKeyFields;  
			bError 		= true; 
		}else{

			if(bDebug){
				System.out.println("  icount final ----->>>>>>>>>>>>>>>>>>>  " + icount);
			}

			conn.setAutoCommit(false);
			int iRowsAfected = pstmt.executeUpdate();
			
			if(iRowsAfected !=1){
				bError 		= true; 
				if(iRowsAfected>1){
					conn.rollback();
					conn.setAutoCommit(true);
					pstmt.close();
					if(bDebug){
						System.out.println("  Elimina m�s de un registro y da rollback: "  + iRowsAfected);
					}
					sResultado = sGetMensaje(-5, bDebug);	
				}else{
					conn.setAutoCommit(true);
					conn.commit();
					pstmt.close();
					if(bDebug){
						System.out.println(" NO Elimina registros "  + iRowsAfected);
					}
					sResultado = sGetMensaje(-6, bDebug);
				}
			}else{
				if(bDebug){
					System.out.println("  Elimina un registro y da commit "  + iRowsAfected);
				}
				conn.commit();
				conn.setAutoCommit(true);
				pstmt.close();				
				sResultado = sGetMensaje(-4, bDebug);	
			}
		}
		
		
		if(bError){
			sResultado 		= "[{\"E\":\"" + sResultado + "\"}]";
			jaJasonArray	= gson.fromJson(sResultado, JsonArray.class); 
		}else{
			sResultado 		= "[{\"A\":\"" + sResultado + "\"}]";
			jaJasonArray	= gson.fromJson(sResultado, JsonArray.class); 
		}
		
		if(bDebug){
			System.out.println("  sResultado en DELETE ----->>>>>>>>>>>>>>>>>>>  " + sResultado);
		}
		JsonObject joObj = new JsonObject();
		joObj.add("result", jaJasonArray);
		
		sResultado = joObj.toString();

		if(bDebug){
			System.out.println("sResultado final: " + sResultado);
		}
		return sResultado;
		
	} // sDeleteRecord
	
	
	
	public String sDynOperation(HashMap<String, Object> map) throws SQLException{

		int iIdEmpresa 			= Integer.parseInt((String)map.get("DYN_CTL_ID_EMPRESA"));
		String sDynOperation	= (String)map.get("DYN_CTL_OPERATION");
		boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
		Usuario usuario 		= (Usuario)map.get("DYN_CTL_USUARIO");
		String sQuery 			= ""; 
		String sParameters 		= "";
		String sParameter		= "";
		String sResultado		= "";
		String sJsonTag			= "";
		boolean bReturnRs		= false;
		StringTokenizer tokens  = null; 
		JsonArray jaJasonArray	= null;
		Gson gson 				= new Gson();
		JsonObject joObj 		= new JsonObject();
		int i 					= 1;
		ResultSet rsTmp			= null;
		
		if(bDebug){
			System.out.println(" sDynOperation ----->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ");
		}
		
		sSentenciaSql = 
			" 	SELECT  D.QUERY_OPERATION, D.QUERY_PARAMETERS, D.ID_ORDER,  \n " +
			"			D.JSON_TAG, NVL(D.B_RETURN_RS,'F') AS B_RETURN_RS	\n " +
            "	FROM    AOS_ARQ_DYN_OPERATION C								\n " +
			"   	JOIN AOS_ARQ_DYN_OPERATION_DET D 						\n " +
            "			ON C.ID_EMPRESA = D.ID_EMPRESA						\n " +
			"			AND C.CVE_OPERATION = D.CVE_OPERATION				\n " +
            "	WHERE   C.ID_EMPRESA  		= ?								\n " +
            "    	AND C.CVE_OPERATION  	= ?								\n " +
            "		AND D.SIT_DETAIL		= 'AC' 							\n " +
            "	ORDER BY D.ID_ORDER											\n " ;

		if(bDebug){
			System.out.println("  QUERY sDynOperation Obtiene query EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n" +
				" iIdEmpresa: 		" + iIdEmpresa 		+ "\n" +
				" sDynOperation: 	" + sDynOperation 	+ "\n");
			System.out.println(" Integer.MAX_VALUE =  " + Integer.MAX_VALUE + "\n" +
					" Integer.MIN_VALUE =  " + Integer.MIN_VALUE);
		}
		
		pstmt = conn.prepareStatement(sSentenciaSql);
		
		pstmt.setInt(1, 	iIdEmpresa);
		pstmt.setString(2, 	sDynOperation);
		rs = pstmt.executeQuery();
		Object[] params = new Object[0];
		int iParams		= 0;

		if(bDebug){
			System.out.println("  RS Count" + rs.getFetchSize() + "----->>>>>>>>>>>>>>>>>>>  ");
		}
		
		while(rs.next()){
			if(bDebug){
				System.out.println("  TIENE RS sDynOperation " + rs.getString("ID_ORDER") + "----->>>>>>>>>>>>>>>>>>>  ");
				System.out.println("  sQuery " + rs.getString("QUERY_OPERATION") + " \n ----->>>>>>>>>>>>>>>>>>>  ");
			}
			sQuery 		= rs.getString("QUERY_OPERATION");
			sParameters = rs.getString("QUERY_PARAMETERS");
			sJsonTag	= rs.getString("JSON_TAG");
			bReturnRs	= rs.getString("B_RETURN_RS").equals("V");

			pstmt 		= conn.prepareStatement(sQuery);

			if(sParameters!=null){
				tokens 		= new StringTokenizer(sParameters, ",");
				params 		= new Object[tokens.countTokens()];
				iParams		= 0;
				i			= 1;

				while(tokens.hasMoreTokens()){
					sParameter = tokens.nextToken();
					String[] sSplit = sParameter.split(Pattern.quote("|"));
					if(bDebug){
						System.out.println("  Tokens sDynOperation sParameter " + sParameter + "----->>>>>>>>>>>>>>>>>>>  ");
						System.out.println("  Tokens sSplit.length sParameter " + sSplit.length + "----->>>>>>>>>>>>>>>>>>>  ");
					}
	
					if(sSplit[0].equals("VARCHAR")){
						if(sSplit[1].equals("DYN_CTL_USER")){
							pstmt.setString(i, usuario.sCveUsuario);
							params[iParams] 	= usuario.sCveUsuario; 
							if(bDebug){
								System.out.println("Valor:  " + usuario.sCveUsuario + "-----******  ");
							}
						}else{
							if((String)map.get(sSplit[1])!=null&&!((String)map.get(sSplit[1])).equals("")){
								pstmt.setString(i, (String)map.get(sSplit[1]));
								params[iParams] 	= (String)map.get(sSplit[1]);
							}else{
								String sStr = null;
								pstmt.setNull(i, Types.VARCHAR);
								params[iParams] = sStr;
							}
							if(bDebug){
								System.out.println("Valor:  " + (String)map.get(sSplit[1]) + "-----******  ");
							}
						}
					}else{
						if(sSplit[0].equals("FLOAT")){
							if((String)map.get(sSplit[1])!=null&&!((String)map.get(sSplit[1])).equals("")){
								pstmt.setFloat(i, Float.valueOf((String)map.get(sSplit[1])));
								params[iParams] = Float.valueOf((String)map.get(sSplit[1]));
							}else{
								Float vFloat = null;
								pstmt.setNull(i, Types.NUMERIC);
								params[iParams] = vFloat;
							}
							if(bDebug){
								System.out.println("Valor:  " + (String)map.get(sSplit[1]) + "-----******  ");
							}
						}else{
							if(sSplit[0].equals("INT")){
								if(sSplit[1].equals("DYN_CTL_USER_ROL")){
									pstmt.setInt(i, usuario.iIdRol);
									params[iParams] = Integer.valueOf(usuario.iIdRol);
									if(bDebug){
										System.out.println("Valor DYN_CTL_USER_ROL:  " + usuario.iIdRol + "-----******  ");
									}
								
								}else{
									if(sSplit.length>=4&&sSplit[3]!=null&&sSplit[3].equals("JSON")){
										String[] sSplitJson = sSplit[1].split(Pattern.quote("-"));
										Integer iVal = Integer.parseInt(
															joObj.getAsJsonArray((String)sSplitJson[0]).get(
																	Integer.parseInt(sSplitJson[2])).
																	getAsJsonObject().get((String)sSplitJson[1]).getAsString());
										pstmt.setInt(i, iVal);
										params[iParams] = iVal;
										if(bDebug){
											System.out.println("Valor:  " + iVal + "-----******  ");
										}

									}else{
										if((String)map.get(sSplit[1])!=null&&!((String)map.get(sSplit[1])).equals("")){
											pstmt.setInt(i, Integer.valueOf((String)map.get(sSplit[1])));
											params[iParams] = Integer.valueOf((String)map.get(sSplit[1]));
										}else{
											Integer vInt = null;
											pstmt.setNull(i, Types.NUMERIC);
											params[iParams] = vInt;
										}
										if(bDebug){
											System.out.println("Valor:  " + Integer.valueOf((String)map.get(sSplit[1])) + "-----******  ");
										}
									}
								}
							}else{
								if(sSplit[0].equals("DATE")){
									if(sSplit[1].equals("DYN_CTL_SYSDATE")){
										Calendar cal = Calendar.getInstance();
										Timestamp  timestamp = new java.sql.Timestamp(cal.getTimeInMillis());
										pstmt.setTimestamp(i, timestamp);
										params[iParams] = timestamp;
									}else{
										//SimpleDateFormat formatter = new SimpleDateFormat(sSplit[2]);
										//String dateInString = sSplit[1];
								        if((String)map.get(sSplit[1])!=null&&!((String)map.get(sSplit[1])).equals("")){
								        	//try {
									        	//Date date = formatter.parse(dateInString);
									            pstmt.setDate(i, java.sql.Date.valueOf((String)map.get(sSplit[1])));
									            params[iParams] = java.sql.Date.valueOf((String)map.get(sSplit[1]));
									       //} catch (ParseException e) {
									            //     e.printStackTrace();
									       //}
								        }else{
								        	Date date = null;
								            pstmt.setNull(i, Types.DATE);
								            params[iParams] = date;
								        }
									}
							        if(bDebug){
										System.out.println("Valor:  " + (String)map.get(sSplit[1]) + "-----******  ");
									}
								}
							}
						}
					}
					if(bDebug){
						System.out.println("  Agrega el parametro en el DYN stmt: " + sParameter);
					}
					i = i + 1;
					iParams = iParams + 1; 
				}
			}
			
			if(bDebug){
				System.out.println("  Antes de ejecutar DYN stmt -->>>>>>>>>>>>>>>> ");
			}
			
			if(sJsonTag!=null&&bReturnRs){
				sResultado 		= resultSetToJson(conn, sQuery, params); 
				if(bDebug){
					System.out.println("sResultado DynQuery: " + sResultado);
				}
				jaJasonArray 	= new JsonArray();
				jaJasonArray  	= gson.fromJson(sResultado, JsonArray.class);
				joObj.add(sJsonTag, jaJasonArray);
			}else{
				if(bDebug){
					System.out.println("  Antes de ejecutar DYN pstmt no es JSON -->>>>>>>>>>>>>>>> ");
				}
				rsTmp = pstmt.executeQuery();
				if(bDebug){
					System.out.println("  Despu�s de ejecutar DYN pstmt no es JSON -->>>>>>>>>>>>>>>> ");
				}
			}
		}
		
		if (rsTmp != null) {
			//CIERRA LA CONSULTA
			rsTmp.close();
			rsTmp = null;
		}
		rs.close();
	    pstmt.close();
		
		sResultado 		= "[{\"A\":\"" + sGetMensaje(-4, bDebug) + "\"}]";
		jaJasonArray	= gson.fromJson(sResultado, JsonArray.class); 
	
		if(bDebug){
		System.out.println("  sResultado en DYN_OPERATION ----->>>>>>>>>>>>>>>>>>>  " + sResultado);
		}
		
		joObj.add("result", jaJasonArray);
	
		sResultado = joObj.toString();

		if(bDebug){
			System.out.println("sResultado final: " + sResultado);
		}
		//sResultado	= new Gson().toJson(sResultado); 
		//if(bDebug){
		//	System.out.println("sResultado final Gson: " + sResultado);
		//}
		return sResultado;
		
	} // sDynOperation
	
	public void sUpdateOrdenServicio(int iIdEmpresa, int iIdOrdenServicio, String sColumna, int iId, boolean bDebug) throws SQLException{

		sSentenciaSql = " UPDATE ORDEN_SERVICIO SET " + sColumna + " = ? WHERE ID_EMPRESA = ? AND ID_ORDEN_SERVICIO = ? ";

		if(bDebug){
			System.out.println("  QUERY update Orden servicio EN DAO ----->>>>>>>>>>>>>>>>>>>  \n" + sSentenciaSql + "\n");
			System.out.println("  iIdEmpresa ----->>>>>>>>>>>>>>>>>>>  " + iIdEmpresa);
			System.out.println("  iIdOrdenServicio ----->>>>>>>>>>>>>>>>>>>  " + iIdOrdenServicio);
			System.out.println("  sColumna ----->>>>>>>>>>>>>>>>>>>  " + sColumna);
			System.out.println("  sId ----->>>>>>>>>>>>>>>>>>>  " + iId);
		}
	
		pstmt = conn.prepareStatement(sSentenciaSql);

		pstmt.setInt(1, 	iId);
		pstmt.setInt(2, 	iIdEmpresa); 
		pstmt.setInt(3, 	iIdOrdenServicio);

		rs = pstmt.executeQuery();
		
	}
	
} // End of file 