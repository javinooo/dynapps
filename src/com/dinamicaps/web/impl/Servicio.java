package com.dinamicaps.web.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.fileupload.FileItem;
import com.dinamicaps.web.impl.domain.*;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Servicio {
	
	
	public ComboConf cGetComboConf(int iIdEmpresa, int iIdListado, String sParentKey, boolean bDebug){
		ServicioDAO dao 	= new ServicioDAO();
		ComboConf config 	= new ComboConf();

		try{			
			
			dao.abreConexion();	
			config = dao.cGetComboConf(iIdEmpresa, iIdListado, sParentKey, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return config;
	}
	
	public LinkedHashMap <String, String> getComboOptionsMap(int iIdEmpresa, int iIdListado, String sParentKey, 
			ComboConf config, StringTokenizer tokens, LinkedHashMap<String, String> map, boolean bDebug){

		ServicioDAO dao 							= new ServicioDAO();
		LinkedHashMap <String, String> opciones 	= new LinkedHashMap<String, String>();

		try{			
			
			dao.abreConexion();	
			opciones = dao.getComboOptionsMap(iIdEmpresa, iIdListado, sParentKey, config, tokens, map, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return opciones;
	}
	
	public LinkedList<Atributo> getListaAtributosPk(int iIdEmpresa, int iIdPantalla, int iIdTabla, int iIdTablaAnterior, String sGenSeq, boolean bDebug){
		
		ServicioDAO dao 		= new ServicioDAO();
		LinkedList<Atributo> listaCampos 	= new LinkedList<Atributo>();
		try{			

			dao.abreConexion();			
			listaCampos = dao.getListaAtributosPk(iIdEmpresa, iIdPantalla, iIdTabla, iIdTablaAnterior, sGenSeq, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return listaCampos;
	}
	
	

	public LinkedList<MasterDetail> getListMDAttributes(int iIdEmpresa, int iIdPantalla, int iIdTabla, int iIdTablaAnterior, boolean bDebug){
		ServicioDAO dao 				= new ServicioDAO();
		LinkedList<MasterDetail> listMD	= new LinkedList<MasterDetail>();
		try{			
			
			dao.abreConexion();			
			listMD = dao.getListMDAttributes(iIdEmpresa, iIdPantalla, iIdTabla, iIdTablaAnterior, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return listMD;
	}
	
	
	public LinkedList<Atributo> getListaAtributosFk(int iIdEmpresa, int iIdPantalla, int iIdTabla, boolean bDebug){
		
		ServicioDAO dao 		= new ServicioDAO();
		LinkedList<Atributo> listaCampos 	= new LinkedList<Atributo>();
		try{			
			
			dao.abreConexion();			
			listaCampos = dao.getListaAtributosFk(iIdEmpresa, iIdPantalla, iIdTabla, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return listaCampos;
	}

	
	public Buttons getButtons(int iIdEmpresa, String sCveUsuario, boolean bDebug){
		
		
		ServicioDAO dao 	= new ServicioDAO();
		Buttons 	buttons	= new Buttons();
		

		try{			
			
			dao.abreConexion();			
			buttons = dao.getButtons(iIdEmpresa, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return buttons;
	}
	
	public LinkedList<Detalle> getListaDetalle(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sAtributoMaestro, boolean bDebug){
		

		ServicioDAO dao 		= new ServicioDAO();
		LinkedList<Detalle> listadDetalle = new LinkedList<Detalle>();
		try{			
			
			dao.abreConexion();			
			listadDetalle = dao.getListaDetalle(iIdEmpresa, iIdPantalla, iIdTabla, sAtributoMaestro, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return listadDetalle;
	}
	

	public LinkedList<Detalle> getDistinctDetalle(int iIdEmpresa, int iIdPantalla, int iIdTabla, int iIdRol, String sListExclude, boolean bDebug){
		

		ServicioDAO dao 		= new ServicioDAO();
		LinkedList<Detalle> listadDetalle = new LinkedList<Detalle>();
		try{			
			
			dao.abreConexion();			
			listadDetalle = dao.getDistinctDetalle(iIdEmpresa, iIdPantalla, iIdTabla, iIdRol, sListExclude, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return listadDetalle;
	}
	
	
	public LinkedList<Remplazo> getListaRemplazoEtiquetas(int iIdEmpresa, String sCveNavegacion, String sTemplate, String sMT, String sMTVrs, String sEsPopUpWindow, boolean bDebug){

		ServicioDAO dao 		= new ServicioDAO();
		LinkedList<Remplazo> listaRemplazos = new LinkedList<Remplazo>();
		try{			
			
			dao.abreConexion();			
			listaRemplazos = dao.getListaRemplazoEtiquetas(iIdEmpresa, sCveNavegacion, sTemplate, sMT, sMTVrs, sEsPopUpWindow, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return listaRemplazos;

		
	}
	
/*
	public LinkedList<Detalle> getListaAtribMD(int iIdEmpresa, int iIdTablaMaestro, int iIdTablaDetalle, boolean bDebug) {
		
		ServicioDAO dao		= new ServicioDAO();
		LinkedList<Detalle> listaMD 	= new LinkedList<Detalle>();
		try{			
			
			dao.abreConexion();			
			listaMD = dao.getListaAtribMD(iIdEmpresa, iIdTablaMaestro, iIdTablaDetalle, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return listaMD;
	}
*/
	public NavegacionNext getNavegacionNext(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sCveUsuario, boolean bDebug) {
		
		ServicioDAO dao 				= new ServicioDAO();
		NavegacionNext navegacionNext 	= new NavegacionNext(); 

		try{			
			
			dao.abreConexion();			
			navegacionNext = dao.getNavegacionNext(iIdEmpresa, iIdPantalla, iIdTabla, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}

	      return navegacionNext;
	}
	
	
	public boolean bVerificaPassword(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sDatos, String sColumna, String sPassword, String sCveUsuario, boolean bDebug){
		
		ServicioDAO dao 	= new ServicioDAO();
		boolean 	bValido	= false;

		try{			
			
			dao.abreConexion();			
			bValido = dao.bVerificaPassword(iIdEmpresa, iIdPantalla, iIdTabla, sDatos, sColumna, sPassword, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return bValido;
	}

	public String sGetRemplazoEtiqueta(int iIdEmpresa, String sSentencia, int iIdDatos, String sCveUsuario, boolean bDebug){
		
		ServicioDAO dao 	= new ServicioDAO();
		String 	sRemplazo	= "";
		

		try{			
			
			dao.abreConexion();			
			sRemplazo = dao.sGetRemplazoEtiqueta(iIdEmpresa, sSentencia, iIdDatos, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sRemplazo;
	}
	

	public void borraCLOB(int iId, boolean bDebug){
		
		ServicioDAO dao 		= new ServicioDAO();
		
		try{			
			dao.abreConexion();			
			dao.borraCLOB(iId, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
	}

	public String sGetErrorTemplate(int iIdEmpresa, int iIdPantalla, boolean bDebug){
		
		ServicioDAO dao 		= new ServicioDAO();
		String 	sErrorTemplate	= "";
		

		try{			
			
			dao.abreConexion();			
			sErrorTemplate = dao.sGetErrorTemplate(iIdEmpresa, iIdPantalla, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sErrorTemplate;
	}

	public String sDameJsComboDependFiltro(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sTipoAtributo, String sCveUsuario, boolean bDebug){
		
		ServicioDAO dao 	= new ServicioDAO();
		String 	sJsFunciones= "";
		

		try{			
			
			dao.abreConexion();			
			sJsFunciones = dao.sDameJsComboDependFiltro(iIdEmpresa, iIdPantalla, iIdTabla, sTipoAtributo, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sJsFunciones;
	}
	
	
	
	

	
	public String sDameUrlDirectorio(String sDirectorio, boolean bDebug){
		
		ServicioDAO dao 	= new ServicioDAO();
		String 		sUrl	= "";
		

		try{			
			
			dao.abreConexion();			
			sUrl = dao.sDameUrlDirectorio(sDirectorio, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sUrl;
	}
	
	

	public String sDameValorParametro(int iIdEmpresa, String sSistema, String sParametro, String sCveUsuario, boolean bDebug) {
		
		ServicioDAO dao 	= new ServicioDAO();
		String 	sResultado	= "";
		
		try{			
			dao.abreConexion();			
			sResultado = dao.sDameValorParametro(iIdEmpresa, sSistema, sParametro, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sResultado;

	}
	
	public String sDameCuerpoMail(int iIdEmpresa, String sSistema, String sParametro, String sCveUsuario, boolean bDebug) {
		
		ServicioDAO dao 	= new ServicioDAO();
		String 	sResultado	= "";

		try{			
			dao.abreConexion();			
			sResultado = dao.sDameValorParametro(iIdEmpresa, sSistema, sParametro, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sResultado;

	}
	
	public String sGetValidationForm(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sTipoForm, String sCveTemplateMaster, int iIdEjecutable, String sCveUsuario, boolean bDebug){
		ServicioDAO dao 	= new ServicioDAO();
		String 	sResultado	= "";

		try{			
			dao.abreConexion();			
			sResultado = dao.sGetValidationForm(iIdEmpresa, iIdPantalla, iIdTabla, sTipoForm, sCveTemplateMaster, iIdEjecutable, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sResultado;

	}
	
	
	public Pantalla getPantalla(int iIdEmpresa, int iIdPantalla, int iIdTabla, int iRol, String sCveOperacion, String sCveUsuario, boolean bDebug){
		ServicioDAO dao 	= new ServicioDAO();
		Pantalla pantalla 	= new Pantalla();
		
		try{			
			
			dao.abreConexion();			
			pantalla = dao.getPantalla(iIdEmpresa, iIdPantalla, iIdTabla, iRol, sCveOperacion, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return pantalla;
		
	}
	
	public LinkedList<NavegacionTemplate> getNavegacionTemplate(int iIdEmpresa, int iIdPantalla, String sCveNavegacion, String sMT, String sMTVrs, String sEsPopUpWindow, boolean bDebug){
		
		ServicioDAO dao 						= new ServicioDAO();
		LinkedList<NavegacionTemplate> lista	= new LinkedList<NavegacionTemplate>();
		
		try{			

			dao.abreConexion();			
			lista = dao.getNavegacionTemplate(iIdEmpresa, iIdPantalla, sCveNavegacion, sMT, sMTVrs, sEsPopUpWindow, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return lista;
	}

	public String sGetPlantilla(int iIdEmpresa, String sCveUsuario, String sEsPopUpWin, boolean bDebug){
		ServicioDAO dao 	= new ServicioDAO();
		String 	sResultado	= "";

		try{			
			dao.abreConexion();			
			sResultado = dao.sGetPlantilla(iIdEmpresa, sCveUsuario, sEsPopUpWin, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sResultado;
	}

	
	public Usuario getUsuario(String sCveUsuario, String sPassword, String sEvento, boolean bDebug){
		
		ServicioDAO dao = new ServicioDAO(); 
		Usuario usuario = null;
		
		try{			
			dao.abreConexion();			
			usuario = (Usuario)dao.getUsuario(sCveUsuario, sPassword, sEvento, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
			usuario = new Usuario();
			usuario.sCveUsuario = "ERROR_EN_SYSTEMA";
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return usuario;
	}
	
	public Ejecutable getEjecutable(int iIdEmpresa, int iIdEjecutable, boolean bDebug){

		Ejecutable ejecutable	= null;
		ServicioDAO dao 		= new ServicioDAO();
		try{			
			dao.abreConexion();			
			ejecutable 	= dao.getEjecutable(iIdEmpresa, iIdEjecutable, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return ejecutable;
	}
	
	public String sDameMensajeSistema(int iIdMensaje, String sCveUsuario, boolean bDebug) {
		ServicioDAO dao 	= new ServicioDAO();
		String sResultado	= "";
		try{			
			dao.abreConexion();			
			sResultado = dao.sDameMensajeSistema(iIdMensaje, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sResultado;
	}

	public String sDameAtributosTablaRecupera(int iIdEmpresa, int iIdEjecutable, int iIdLog, String sDatos, String sCveUsuario, boolean bDebug) {
		ServicioDAO dao 	= new ServicioDAO();
		String sResultado	= "";
		try{			
			dao.abreConexion();			
			sResultado = dao.sDameAtributosTablaRecupera(iIdEmpresa, iIdEjecutable, iIdLog, sDatos, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sResultado;
	}
	
/*
	public String sDameAtributosTablaIdxRecupera(int iIdLog) {
		ServicioDAO dao 	= new ServicioDAO();
		String sResultado	= "";
		try{			
			dao.abreConexion();			
			sResultado = dao.sDameAtributosTablaIdxRecupera(iIdLog);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sResultado;
	}
	*/

	public String sDameMensajeControl(int iIdMensaje, String sCveUsuario) {
		ServicioDAO dao 	= new ServicioDAO();
		String sResultado	= "";
		try{			
			dao.abreConexion();			
			sResultado = dao.sDameMensajeControl(iIdMensaje, sCveUsuario);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sResultado;
	}
	
	
	public String altaModDatos(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sDatos, String sTipoOperacion, String sCveUsuario, int iIdRol, boolean bDebug) {
		
		ServicioDAO dao 	= new ServicioDAO();
		String sResultado	= "";
		try{			
			dao.abreConexion();			
			sResultado = dao.altaModDatos(iIdEmpresa, iIdPantalla, iIdTabla, sDatos, sTipoOperacion, sCveUsuario, iIdRol, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sResultado;
	}
	

	public String sDameAtributosFK(int iIdEmpresa, String sTablaDestino, String sCveUsuario, boolean bDebug) {
		
		ServicioDAO dao 	= new ServicioDAO();
		String sResultado	= "";
		try{			
			dao.abreConexion();			
			sResultado = dao.sDameAtributosFK(iIdEmpresa, sTablaDestino, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sResultado;
	}
	

	public String sDameOpcionesCombo(int iIdEmpresa, int iIdListado, String sSelected, String sDatos, 
			String sBObligatorio, String sCveUsuario, boolean bDebug) {
		
		ServicioDAO dao 	= new ServicioDAO();
		String sResultado	= "";
		try{			
			dao.abreConexion();			
			sResultado = dao.sDameOpcionesCombo(iIdEmpresa, iIdListado, sSelected, sDatos, sBObligatorio, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sResultado;
	}

	public String ejecutaProceso(int iIdEmpresa, int iIdTabla, int iIdEjecutable, String sDatos, String sCveUsuario, int iIdLogError, boolean bDebug) {
		
		ServicioDAO dao 	= new ServicioDAO();
		String sResultado	= "";
		try{			
			dao.abreConexion();			
			sResultado = dao.ejecutaProceso(iIdEmpresa, iIdTabla, iIdEjecutable, sDatos, sCveUsuario, iIdLogError, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sResultado;
	}
/*
	public int iIdLogErrorAltaEntidad(int iIdEmpresa, String sDatos, String sCveUsuario, boolean bDebug) {
		
		ServicioDAO dao 	= new ServicioDAO();
		int iIdLogResultado	= 0;
		try{			
			dao.abreConexion();			
			iIdLogResultado = dao.iIdLogErrorAltaEntidad(iIdEmpresa, sDatos, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return iIdLogResultado;
	}


	public int iIdLogErrorObjetoEntidad(int iIdEmpresa, String sDatos, String sCveUsuario, int iIdEjecutable, boolean bDebug) {
		
		ServicioDAO dao 	= new ServicioDAO();
		int iIdLogResultado	= 0;
		try{			
			dao.abreConexion();			
			iIdLogResultado = dao.iIdLogErrorObjetoEntidad(iIdEmpresa, sDatos, sCveUsuario, iIdEjecutable, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return iIdLogResultado;
	}
*/
	public int iIdLogErrorGuardaDatosExec(int iIdEmpresa, String sDatos, String sCveUsuario, boolean bDebug) {
		
		ServicioDAO dao 	= new ServicioDAO();
		int iIdLogResultado	= 0;
		try{
			dao.abreConexion();
			iIdLogResultado = dao.iIdLogErrorGuardaDatosExec(iIdEmpresa, sDatos, sCveUsuario, bDebug);
		} catch(Exception excp){
			excp.printStackTrace();
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();
					}
				}
			} catch(Exception exc){
				exc.printStackTrace();
			}
		}
		return iIdLogResultado;
	}

	
	public int iGuardaCLOB(int iIdEmpresa, int iIdEjecutable, int iIdPantalla, String sCveUsuario, String sCLOB, String sOrigen, boolean bDebug) {

		ServicioDAO dao 	= new ServicioDAO();
		int iIdCLOB	= 0;
		try{			
			dao.abreConexion();			
			iIdCLOB = dao.iGuardaCLOB(iIdEmpresa, iIdEjecutable, iIdPantalla, sCveUsuario, sCLOB, sOrigen, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return iIdCLOB;
	}
	
	public Navegacion getNavegacion(String sCveNavegacion, boolean bDebug){

		ServicioDAO dao 		= new ServicioDAO();
		Navegacion navegacion	= new Navegacion();
		
		try{			
			
			dao.abreConexion();			
			navegacion = dao.getNavegacion(sCveNavegacion, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return navegacion;
	}
	
	public int iIdFile(){
		
		int iIdFile 	= 0;
		ServicioDAO dao	= new ServicioDAO();
		
		try{			
			dao.abreConexion();
			iIdFile = dao.iIdFile();

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}
				}
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		
		return iIdFile;

	}

	public int iIdLogEjecutable(){
		
		int iIdLog 		= 0;
		ServicioDAO dao	= new ServicioDAO();
		
		try{			
			dao.abreConexion();
			iIdLog = dao.iIdLogEjecutable();

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}
				}
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return iIdLog;
	}	
	

	public int getMensajeError(int iIdLogerror, boolean bDebug){
		
		int iIdMsgError		= -1;
		ServicioDAO dao	= new ServicioDAO();
		
		try{			
			dao.abreConexion();
			iIdMsgError = dao.getMensajeError(iIdLogerror, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}
				}
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return iIdMsgError;
	}
	
	public Archivo getArchivoBytes(int iIdArchivo, String sCveUsuario, boolean bDebug){
		
		Archivo archivo	= new Archivo();
		ServicioDAO dao	= new ServicioDAO();
		
		try{			
			dao.abreConexion();			
			archivo = dao.getArchivoBytes(iIdArchivo, sCveUsuario, bDebug);

			
		} catch(Exception excp){
			excp.printStackTrace();			
		} 
/*		finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.close();	
					}
				}
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
*/
		return archivo;
	}
	
	public String sFileInsert(int iIdFile, int iIdEmpresa, String sCveUsuario, String sNomArchivo, String sContentType, FileItem file, boolean bDebug){
		
		String sResultado 	= "";
		ServicioDAO dao		= new ServicioDAO();
		
		try{			
			dao.abreConexion();			
			sResultado = dao.sFileInsert(iIdFile, iIdEmpresa, sCveUsuario, sNomArchivo, sContentType, file, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}
				}
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		
		return sResultado;

	}
	
	public String sRefFileInsert(int iIdFile, int iIdEmpresa, String sCveUsuario, String sNomArchivo, String sContentType, String sfileDir, boolean bDebug){

		String sResultado 	= "";
		ServicioDAO dao		= new ServicioDAO();
		
		try{			
			dao.abreConexion();			
			sResultado = dao.sRefFileInsert(iIdFile, iIdEmpresa, sCveUsuario, sNomArchivo, sContentType, sfileDir, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}
				}
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		
		return sResultado;

	}
	
	
	
	public LinkedList<Atributo> getListaParamExec(int iIdEmpresa, int iIdEjecutable, boolean bDebug) {
		
		ServicioDAO dao 			= new ServicioDAO();
		LinkedList<Atributo> listaParametros 	= new LinkedList<Atributo>();
		
		try{			
			
			dao.abreConexion();			
			listaParametros = dao.getListaParamExec(iIdEmpresa, iIdEjecutable, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return listaParametros;
	}

	public LinkedList<ElementRow> getElementRow(int iIdEmpresa, int iIdEjecutable, boolean bDebug){
		
		ServicioDAO dao 			= new ServicioDAO();
		LinkedList<ElementRow> lista= new LinkedList<ElementRow>();
		
		try{			
			
			dao.abreConexion();			
			lista = dao.getElementRow(iIdEmpresa, iIdEjecutable, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return lista;
	}

	
	public LinkedList<Atributo> getListaAtributosTabla(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sTipo, int iIdRol, String sTipoOperacion, boolean bDebug){
		
		ServicioDAO dao 					= new ServicioDAO();
		LinkedList<Atributo> listaFiltro 	= new LinkedList<Atributo>();
		try{			

			dao.abreConexion();			
			listaFiltro = dao.getListaAtributosTabla(iIdEmpresa, iIdPantalla, iIdTabla, sTipo, iIdRol, sTipoOperacion, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return listaFiltro;
	}
	
	
	public String sGetTemplateResultSet(int iIdEmpresa, int iIdPantalla, int iIdTabla, String sCveUsuario, boolean bDebug){
		
		ServicioDAO dao 	= new ServicioDAO();
		String 	sTemplate	= "";
		

		try{			
			
			dao.abreConexion();			
			sTemplate = dao.sGetTemplateResultSet(iIdEmpresa, iIdPantalla, iIdTabla, sCveUsuario, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return sTemplate;
	}
	
	
	public boolean bDebug(int iIdEmpresa, String sCveUsuario){

		ServicioDAO dao = new ServicioDAO();
		boolean bDebug 	= false;

		try{			
			
			dao.abreConexion();			
			bDebug = dao.bDebug(iIdEmpresa, sCveUsuario);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		
		
		
	    return bDebug;
	}
	
	
	
	
	public Map<String, Object>getSettings(String sCveMasterTemplate, String sCveMasterTemplateVersion, String sFilter, boolean bDebug){

		ServicioDAO dao 			= new ServicioDAO();
		Map<String, Object> map 	= new HashMap<String, Object>();

		try{			
			
			dao.abreConexion();	
			map = dao.getSettings(sCveMasterTemplate, sCveMasterTemplateVersion, sFilter, bDebug);

		} catch(Exception excp){
			excp.printStackTrace();			
		} finally {
			try {
				if( dao != null ){
					if(dao.conn != null){
						dao.conn.commit();
						dao.conn.close();	
					}							
				}							
			} catch(Exception exc){
				exc.printStackTrace();
			}			
		}
		return map;
	}
	
	
	
		public String sDameListaExcluye(int iIdEmpresa, String sStmtInsert, String sQryExclude, boolean bDebug){
			
			ServicioDAO dao 	= new ServicioDAO();
			String 	sTemplate	= "";
			try{			
				dao.abreConexion();			
				sTemplate = dao.sDameListaExcluye(iIdEmpresa, sStmtInsert, sQryExclude, bDebug);
			} catch(Exception excp){
				excp.printStackTrace();			
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
			return sTemplate;
		}
		
	
		public String sGetHTML(Usuario usuario, boolean bDebug){
			
			ServicioDAO dao 	= new ServicioDAO();
			String 	sHTMLCode	= "";
			try{			
				dao.abreConexion();			
				sHTMLCode = dao.sGetHTML(usuario, bDebug);
			} catch(Exception excp){
				excp.printStackTrace();			
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
			return sHTMLCode;
		}
	
		

		public String sGetMenu(int iIdEmpresa, String sCveUsuario, String sSchema, String sCveTemplateMaster, int iIdRol, String SGenero, String sIsMobile, boolean bDebug){
			
			ServicioDAO dao 	= new ServicioDAO();
			String 	sMenuApp = "";
			try{			
				dao.abreConexion();			
				sMenuApp = dao.sGetMenu(iIdEmpresa, sCveUsuario, sSchema, sCveTemplateMaster, iIdRol, SGenero, sIsMobile, bDebug);
			} catch(Exception excp){
				excp.printStackTrace();			
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
			return sMenuApp;
		}
		
		
		public String sGetPantallaJQ(HashMap<String, Object> map){
			
			ServicioDAO dao 	= new ServicioDAO();
			String 	sPantalla = "";
			try{			
				dao.abreConexion();			
				sPantalla = dao.sGetPantallaJQ(map);
			} catch(Exception excp){
				excp.printStackTrace();			
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
			return sPantalla;
		}
		
		public String sGetTableRs(HashMap<String, Object> map){
			ServicioDAO dao 	= new ServicioDAO();
			String 	sResultado 	= "";
			try{			
				dao.abreConexion();			
				sResultado = dao.sGetTableRs(map);
			} catch(Exception excp){
				excp.printStackTrace();			
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
			return sResultado;
		}
		
		public String sGetRecord(HashMap<String, Object> map){
			ServicioDAO dao 	= new ServicioDAO();
			String 	sResultado 	= "";
			try{			
				dao.abreConexion();			
				sResultado = dao.sGetRecord(map);
			} catch(Exception excp){
				excp.printStackTrace();			
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
			return sResultado;
		}
		
		public String sUpdateRecord(HashMap<String, Object> map){
			ServicioDAO dao 		= new ServicioDAO();
			String 	sResultado 		= "";
			Gson gson 				= new Gson();
			JsonArray jaJasonArray 	= new JsonArray();

			try{			
				dao.abreConexion();			
				sResultado = dao.sUpdateRecord(map);
			} catch(SQLException  excp){
				excp.printStackTrace();
				boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
				if(bDebug){
					System.out.println("Inicia mensaje: \n" + excp.getMessage() + "\n termina mensaje");
					System.out.println("Inicia getErrorCode: \n" + excp.getErrorCode() + "\n termina getErrorCode");
				}
				
				try{
					sResultado = dao.sGetMensaje(excp.getErrorCode(), bDebug);
					sResultado 			= dao.sGetMensajeErrorDb(excp.getErrorCode(), excp.getMessage(), bDebug);
					sResultado 			= "[{\"A\":\"" + sResultado + "\"}]";
					jaJasonArray		= gson.fromJson(sResultado, JsonArray.class); 
					JsonObject joObj 	= new JsonObject();
					joObj.add("result", jaJasonArray);
					sResultado 			= joObj.toString();

					if(bDebug){
						System.out.println("sResultado mensaje error: \n" + sResultado);
					}
				} catch(SQLException  excpi){
					excp.printStackTrace();	
				}
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
			return sResultado;
		}
		
		public String sInsertRecord(HashMap<String, Object> map){
			ServicioDAO dao 	= new ServicioDAO();
			String 	sResultado 	= "";
			Gson gson 				= new Gson();
			JsonArray jaJasonArray 	= new JsonArray();
			try{			
				dao.abreConexion();			
				sResultado = dao.sInsertRecord(map);
			} catch(SQLException  excp){
				excp.printStackTrace();
				boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
				if(bDebug){
					System.out.println("Inicia mensaje: \n" + excp.getMessage() + "\n termina mensaje");
					System.out.println("Inicia getErrorCode: \n" + excp.getErrorCode() + "\n termina getErrorCode");
				}
				
				try{
					sResultado 			= dao.sGetMensajeErrorDb(excp.getErrorCode(), excp.getMessage(), bDebug);
					sResultado 			= "[{\"A\":\"" + sResultado + "\"}]";
					jaJasonArray		= gson.fromJson(sResultado, JsonArray.class); 
					JsonObject joObj 	= new JsonObject();
					joObj.add("result", jaJasonArray);
					sResultado 			= joObj.toString();
					if(bDebug){
						System.out.println("sResultado mensaje error: \n" + sResultado);
					}
				} catch(SQLException  excpi){
					excp.printStackTrace();	
				}
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
			return sResultado;
		}
		
		
		public String sDeleteRecord(HashMap<String, Object> map){
			ServicioDAO dao 	= new ServicioDAO();
			String 	sResultado 	= "";
			try{			
				dao.abreConexion();			
				sResultado = dao.sDeleteRecord(map);
			} catch(SQLException  excp){
				excp.printStackTrace();
				boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
				if(bDebug){
					System.out.println("Inicia mensaje: \n" + excp.getMessage() + "\n termina mensaje");
					System.out.println("Inicia getErrorCode: \n" + excp.getErrorCode() + "\n termina getErrorCode");
				}
				
				try{
					sResultado = dao.sGetMensaje(excp.getErrorCode(), bDebug);
					if(bDebug){
						System.out.println("sResultado mensaje error: \n" + sResultado);
					}
				} catch(SQLException  excpi){
					excp.printStackTrace();	
				}
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
			return sResultado;
		}
		
		public String sGetInsertForm(HashMap<String, Object> map){
			ServicioDAO dao 	= new ServicioDAO();
			String 	sResultado 	= "";
			try{			
				dao.abreConexion();			
				sResultado = dao.sGetInsertForm(map);
			} catch(Exception excp){
				excp.printStackTrace();			
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
			return sResultado;
		}
		
		public String sGetSelectOptions(HashMap<String, Object> map){
			ServicioDAO dao 	= new ServicioDAO();
			String 	sResultado 	= "";
			try{			
				dao.abreConexion();			
				sResultado = dao.sGetSelectOptions(map);
			} catch(Exception excp){
				excp.printStackTrace();			
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
			return sResultado;
		}
	
		
		/***************   DYN_OPERATION   **********************/
		
		public String sDynOperation(HashMap<String, Object> map){
			ServicioDAO dao 	= new ServicioDAO();
			String 	sResultado 	= "";
			Gson gson 				= new Gson();
			JsonArray jaJasonArray 	= new JsonArray();
			try{
				dao.abreConexion();			
				sResultado = dao.sDynOperation(map);
			} catch(SQLException  excp){
				excp.printStackTrace();
				boolean bDebug			= (boolean)map.get("DYN_CTL_BDEBUG");
				System.out.println("Inicia mensaje bDebug: " + bDebug);
				if(bDebug){
					System.out.println("Inicia mensaje: \n" + excp.getMessage() + "\n termina mensaje");
					System.out.println("Inicia getErrorCode: \n" + excp.getErrorCode() + "\n termina getErrorCode");
				}
				
				try{
					System.out.println("Try interno");
					sResultado 			= dao.sGetMensajeErrorDb(excp.getErrorCode(), excp.getMessage(), bDebug);
					System.out.println("Despu�s de Try interno");
					sResultado 			= "[{\"E\":\"" + sResultado + "\"}]";
					jaJasonArray		= gson.fromJson(sResultado, JsonArray.class); 
					JsonObject joObj 	= new JsonObject();
					joObj.add("result", jaJasonArray);
					sResultado 			= joObj.toString();
					if(bDebug){
						System.out.println("sResultado mensaje error: \n" + sResultado);
					}
				} catch(SQLException  excpi){
					if(bDebug){
						System.out.println("Error al obtener el mensaje: ");
					}
					excp.printStackTrace();	
					sResultado 			= "[{\"E\":\"Error Interno\"}]";
					jaJasonArray		= gson.fromJson(sResultado, JsonArray.class); 
					JsonObject joObj 	= new JsonObject();
					joObj.add("result", jaJasonArray);
					sResultado 			= joObj.toString();
					if(bDebug){
						System.out.println("sResultado mensaje error Interno: \n" + sResultado);
					}
				}
			} finally {
				try {
					if( dao != null ){
						if(dao.conn != null){
							dao.conn.commit();
							dao.conn.close();	
						}							
					}							
				} catch(Exception exc){
					exc.printStackTrace();
				}			
			}
			return sResultado;
		}
		

		
		/***************   END OF FILE   **********************/
}
