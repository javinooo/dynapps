package com.dinamicaps.web;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletOutputStream;

import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperRunManager;

import com.dinamicaps.web.impl.domain.optics.*;

/**
 * Servlet implementation class OpticsReportsSrv
 */
public class OpticsReportsSrv extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OpticsReportsSrv() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //PrintWriter out = response.getWriter();
        ServletOutputStream outputstream = response.getOutputStream();
        System.out.println("processRequest EN servlet");
        try {
            
        	System.out.println("try");
          if (request.getParameter("ID_COMPANY") != null & request.getParameter("ID_BUSINESS") != null) {
        	  
        	  System.out.println("processRequest primer if");
        	  
                if (!request.getParameter("ID_COMPANY").equals("") & !request.getParameter("ID_BUSINESS").equals("")) {
                	System.out.println("processRequest segundo IF ");
                    String companyId = request.getParameter("ID_COMPANY");
                    String idNota = request.getParameter("ID_BUSINESS");
                    OpticsDAO datos = new OpticsDAO();
                    
                    System.out.println("companyId EN servlet: " + companyId);
                    System.out.println("idNota EN servlet: " + idNota);

                    datos.abreConexion();

                    InvoiceVO invoice = datos.getInvoiceHeader(companyId, idNota);
                    String total = datos.getInvoiceTotal(companyId, idNota);
                    ResultSet ds = datos.getInvoiceData(companyId, idNota);

                    String jrxmlFileName = "WEB-INF\\reportes\\Invoice.jasper";
                    File archivoInvoice = new File(request.getSession().getServletContext().getRealPath(jrxmlFileName));
                    HashMap parametros = new HashMap();
                    
                    System.out.println("companyId EN InvoiceVO: " + invoice.getCompanyName());
                    System.out.println("billingAddress EN InvoiceVO: " + invoice.getBillingAddress());
                    
                    parametros.put("companyName", invoice.getCompanyName());
                    parametros.put("billingAddress", invoice.getBillingAddress());
                    parametros.put("invoiceDate", invoice.getInvoiceDate());
                    parametros.put("invoiceNumber", invoice.getInvoiceNumber());
                    parametros.put("poNumber", invoice.getPoNumber());
                    parametros.put("dueDate", invoice.getDueDate());
                    parametros.put("shippingMethod", invoice.getShippingMethod());
                    parametros.put("inviceTotal", total);

                    JRResultSetDataSource dataSource = new JRResultSetDataSource(ds);

                    byte[] bytes = JasperRunManager.runReportToPdf(archivoInvoice.getPath(), parametros, dataSource);

                    System.out.println("bytes.length: " + bytes.length);
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    outputstream.write(bytes, 0, bytes.length);
                    outputstream.flush();
                    outputstream.close();

                    datos.cierraConexion();
                }

            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
    
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 System.out.println("**************************************************************");
		 System.out.println("doGet EN servlet: ");
		//processRequest(request, response);
		//PrintWriter out = response.getWriter();
	        ServletOutputStream outputstream = response.getOutputStream();
	        System.out.println("processRequest EN servlet");
	        try {
	            
	        	System.out.println("try");
	          if (request.getParameter("ID_COMPANY") != null & request.getParameter("ID_BUSINESS") != null) {
	        	  
	        	  System.out.println("processRequest primer if");
	        	  
	                if (!request.getParameter("ID_COMPANY").equals("") & !request.getParameter("ID_BUSINESS").equals("")) {
	                	System.out.println("processRequest segundo IF ");
	                    String companyId = request.getParameter("ID_COMPANY");
	                    String idNota = request.getParameter("ID_BUSINESS");
	                    OpticsDAO datos = new OpticsDAO();
	                    
	                    System.out.println("companyId EN servlet: " + companyId);
	                    System.out.println("idNota EN servlet: " + idNota);

	                    datos.abreConexion();

	                    InvoiceVO invoice = datos.getInvoiceHeader(companyId, idNota);
	                    String total = datos.getInvoiceTotal(companyId, idNota);
	                    ResultSet ds = datos.getInvoiceData(companyId, idNota);

	                    String sRealPath = request.getSession().getServletContext().getRealPath(request.getContextPath());
	                    sRealPath = sRealPath.substring(0, sRealPath.lastIndexOf("\\")) + "\\WEB-INF\\reportes\\Invoice.jasper";
	                    File archivoInvoice = new File(sRealPath);
	                    
	                    HashMap parametros = new HashMap();
	                    
	                    System.out.println("companyId EN InvoiceVO: " + invoice.getCompanyName());
	                    System.out.println("billingAddress EN InvoiceVO: " + invoice.getBillingAddress());
	                    
	                    parametros.put("companyName", invoice.getCompanyName());
	                    parametros.put("billingAddress", invoice.getBillingAddress());
	                    parametros.put("invoiceDate", invoice.getInvoiceDate());
	                    parametros.put("invoiceNumber", invoice.getInvoiceNumber());
	                    parametros.put("poNumber", invoice.getPoNumber());
	                    parametros.put("dueDate", invoice.getDueDate());
	                    parametros.put("shippingMethod", invoice.getShippingMethod());
	                    parametros.put("inviceTotal", total);

	                    JRResultSetDataSource dataSource = new JRResultSetDataSource(ds);

	                    byte[] bytes = JasperRunManager.runReportToPdf(archivoInvoice.getPath(), parametros, dataSource);

	                    System.out.println("bytes.length: " + bytes.length);
	                    response.setContentType("application/pdf");
	                    response.setContentLength(bytes.length);
	                    outputstream.write(bytes, 0, bytes.length);
	                    outputstream.flush();
	                    outputstream.close();
	                    System.out.println(" antes de cerrar ");
	                    datos.cierraConexion();
	                    System.out.println(" despu�s de cerrar ");
	                }

	            }
	        } catch (Exception exc) {
	            exc.printStackTrace();
	        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("doPost EN servlet: ");
		processRequest(request, response);
	}

}
