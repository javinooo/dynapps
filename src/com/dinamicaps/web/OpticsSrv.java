package com.dinamicaps.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dinamicaps.web.impl.Servicio;
import com.dinamicaps.web.impl.domain.Usuario;
import com.dinamicaps.web.impl.domain.optics.OpticsDAO;
import com.dinamicaps.web.impl.domain.optics.MatrizResultadoConf;
import com.dinamicaps.web.impl.domain.optics.MatrizAdmDatosConf;
import com.google.gson.Gson; 

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

/**
 * Servlet implementation class ReportsMerdiaSrv
 */
//@WebServlet("/ReportsMerdiaSrv")
public class OpticsSrv extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public OpticsSrv() {
        super();
        // TODO Auto-generated constructor stub
    }
 
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Entra al servlet -------------------------------");
		//processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Obtain the session object, create a new session if doesn't exist
        HttpSession session = 							request.getSession(false);
        String sResultado 								= "";
        String[] sParametros							= new String[2];
        boolean lbDebug 								= false;
        StringTokenizer tokensDatos						= null;
		StringTokenizer tokensPlaceholder				= null;
        LinkedHashMap<String, String> mapDatos 			= null; 
        LinkedHashMap<String, Object> map 				= null;
        OpticsDAO dao 									= null;
		MatrizAdmDatosConf admConf						= null;
		LinkedHashMap<String, String> mapPlaceholder 	= null;
		MatrizResultadoConf conf						= null;
		sResultado 										= "";
		int iIdProductoBase; 
		int iIdAlmacen;
		int iIdSerie;
		int iIdNota;
		int iIdProducto			= 0; 
		int iCantidad			= 0;
		int iIdMovimiento		= 0;
		int iIdMovtoVenta;
		float fValHorizontal;
		float fValVertical;
		float fPrecio; 
		String sCveColor		= "";
		int iIdDevolucion;
		String sSituacion		= "";
		String sTxReferencia	= "";
        

		if(session == null){
            //System.out.println(" La sesi�n es nula ");
            sResultado = "[\"0\",\"Su sesi�n ha caducado\"]";
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(sResultado);    
			return;
        }
		
		Usuario usuario	= (Usuario) session.getAttribute("Usuario");
		Servicio servicio = new Servicio();
		
		if(usuario==null){
			sParametros[0] = null;
			sParametros[1] = "Su sesi�n ha caducado";
			sResultado	= new Gson().toJson(sParametros);   
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(sResultado);   
    		return;
		}
		int iIdEmpresa			= usuario.iIdEmpresa;
		
		lbDebug	= servicio.bDebug(usuario.iIdEmpresa, usuario.sCveUsuario);
		
		String sCveOperacion   	= request.getParameter("CTL_CVE_OPERACION");
		
		if(lbDebug){
			System.out.println("sCveOperacion -------------------------------" + sCveOperacion);
		}
		
		
		if(sCveOperacion==null){
			return;
		}else{
			
			switch (sCveOperacion){
        	case "ACTUALIZA_SERIES": 
        		dao 		= new OpticsDAO();
				sResultado 	= "";
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " + request.getParameter("ID_EMPRESA"));
					System.out.println("iIdProductoBase: " + request.getParameter("ID_PRODUCTO_BASE"));
					System.out.println("iIdMovimiento: " + request.getParameter("ID_MOVIMIENTO"));
				}

				iIdProductoBase	= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				iIdMovimiento	= Integer.parseInt(
						(
							(request.getParameter("ID_MOVIMIENTO")==null||
									request.getParameter("ID_MOVIMIENTO").equals(""))
						
						?"0":request.getParameter("ID_MOVIMIENTO")));
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " + iIdEmpresa);
					System.out.println("iIdProductoBase: " + iIdProductoBase);
					System.out.println("iIdMovimiento: " + iIdMovimiento);
				}
				
				try {
					dao.abreConexion();
					sResultado = dao.sGetSeries(iIdEmpresa, iIdProductoBase, iIdMovimiento, usuario.sCveUsuario, lbDebug);
					sResultado	= new Gson().toJson(sResultado);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break;
        	case "GENERA_PRE_MOVIMIENTO": 
        		if(lbDebug){
					System.out.println("GENERA_PRE_MOVIMIENTO");
				}
				dao 		= new OpticsDAO();
				sResultado 	= "";
				
				String sIdPreMovto		= request.getParameter("ID_MOVIMIENTO");
				/*/ Codigo para validacion de Permisos Inicia
				int iIdPantalla			= Integer.parseInt(request.getParameter("ID_PANTALLA"));
				int iIdTabla			= Integer.parseInt(request.getParameter("ID_TABLA"));
				boolean bPermiso		= dao.bPermisoAltaModPreMovto(iIdEmpresa, usuario.iIdRol, iIdPantalla, iIdTabla, sIdPreMovto, lbDebug);
				
				
				
				//Codigo para validacion de Permisos termina */
				iCantidad				= Integer.parseInt(request.getParameter("CANTIDAD"));
				String sCveProducto		= request.getParameter("CVE_PRODUCTO");
				String sTipoOperacion 	= request.getParameter("PARAM_CTL_OPERACION");
				String sTipoDevolucion 	= request.getParameter("TIPO_DEVOLUCION");
				iIdDevolucion			= Integer.parseInt(request.getParameter("ID_DEVOLUCION")==null?"0":request.getParameter("ID_DEVOLUCION"));
				String sTipoMovimiento 	= request.getParameter("TIPO_MOVIMIENTO");
				String sBSincroniza		= request.getParameter("PARAM_CTL_B_SINCRONIZA");
				iIdAlmacen				= Integer.parseInt((request.getParameter("ID_ALMACEN")==null?"0":request.getParameter("ID_ALMACEN")));
				iIdNota					= Integer.parseInt(request.getParameter("ID_NOTA")==null?"0":request.getParameter("ID_NOTA"));
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " + iIdEmpresa);
					System.out.println("iIdNota: " + iIdNota);
					System.out.println("iIdPreMovto: " + sIdPreMovto);
					System.out.println("sCveProducto: " + sCveProducto);
					System.out.println("iCantidad: " + iCantidad);
					System.out.println("sTipoOperacion: " + sTipoOperacion);
					System.out.println("sBSincroniza: " + sBSincroniza);
					System.out.println("iIdAlmacen: " + iIdAlmacen);
					System.out.println("sTipoDevolucion: " + sTipoDevolucion);
					System.out.println("iIdDevolucion: " + iIdDevolucion);
				}


				try {
					
					dao.abreConexion();
					
					sParametros = dao.sGeneraPreMovto(iIdEmpresa, sIdPreMovto, sCveProducto, iCantidad, sTipoOperacion, sTipoMovimiento, iIdAlmacen, sTipoDevolucion, 
			      		  				usuario.sCveUsuario, iIdDevolucion, sBSincroniza, usuario.iIdRol, iIdNota, lbDebug);
			        sResultado	= new Gson().toJson(sParametros);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		            String sError = exc.getMessage();
		            if(sError.contains("ORA-20")){
		            	sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
		            }else{
		            	sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
		            }
			        sResultado	= new Gson().toJson(sResultado);   
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break;
        	case "ALTA_MOD_COMPRA": 
        		if(lbDebug){
					System.out.println("ALTA_MOD_COMPRA*********************************");
				}
				
				dao 			= new OpticsDAO();
				
				sResultado 		= "";
				iIdNota			= Integer.parseInt(request.getParameter("ID_NOTA"));
				iIdMovimiento	= Integer.parseInt(request.getParameter("ID_MOVIMIENTO"));
				iIdProductoBase	= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				sTxReferencia	= request.getParameter("TX_REFERENCIA");
				sSituacion		= request.getParameter("SIT_MOVIMIENTO");
				sTipoOperacion	= request.getParameter("PARAM_CTL_OPERACION");
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " + iIdEmpresa);
					System.out.println("iIdNota: " + iIdNota);
					System.out.println("sIdMovimiento: " + iIdMovimiento);
					System.out.println("sIdProductoBase: " + iIdProductoBase);
					System.out.println("sTxReferencia: " + sTxReferencia);
					System.out.println("sUsuario: " + usuario.sCveUsuario);
					System.out.println("sSituacion: " + sSituacion);
					System.out.println("sTipoOperacion: " + sTipoOperacion);
				}
				
				try {
				
					
					dao.abreConexion();
					sResultado = dao.sAltaModEntrada(iIdEmpresa, iIdNota, iIdMovimiento,  
					  	iIdProductoBase, sTxReferencia, usuario.sCveUsuario, sSituacion, sTipoOperacion, lbDebug);

					if(lbDebug){
						System.out.println("SResultado directo ALTA_MOD_COMPRA: " + sResultado);
					}
					
					sParametros[0] = sResultado;
					sParametros[1] = null;
					sResultado	= new Gson().toJson(sParametros);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		            String sError = exc.getMessage();
		            if(sError.contains("ORA-20")){
		            	sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
		            }else{
		            	sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
		            }
		            sParametros[0] = sResultado;
					sParametros[1] = "1";
					sResultado	= new Gson().toJson(sParametros);   
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "ACTUALIZA_SERIES_VTA": 
        		if(lbDebug){
					System.out.println("sIdEmpresa: " + request.getParameter("ID_EMPRESA"));
					System.out.println("iIdProductoBase: " + request.getParameter("ID_PRODUCTO_BASE"));
					System.out.println("iIdAlmacen: " + request.getParameter("ID_ALMACEN"));
					System.out.println("iIdMovimiento: " + request.getParameter("ID_MOVIMIENTO"));
				}
				
				dao 			= new OpticsDAO();

				sResultado 		= "";
				iIdProductoBase	= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				iIdAlmacen		= Integer.parseInt(request.getParameter("ID_ALMACEN"));
				iIdMovimiento	= Integer.parseInt(((request.getParameter("ID_MOVIMIENTO")==null||request.getParameter("ID_MOVIMIENTO").equals(""))?"0":request.getParameter("ID_MOVIMIENTO")));
				
				try {
					
					dao.abreConexion();
					sResultado 	= dao.sGetSeriesVta(iIdEmpresa, iIdProductoBase, iIdAlmacen, iIdMovimiento, usuario.sCveUsuario, lbDebug);
					sResultado	= new Gson().toJson(sResultado);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "ALTA_MOD_VENTA": 
        		if(lbDebug){
					System.out.println("ALTA_MOD_VENTA*********************************");
				}
				
				dao 			= new OpticsDAO();
				
				sResultado 		= "";
				iIdMovimiento	= Integer.parseInt(request.getParameter("ID_MOVIMIENTO"));
				iIdNota			= Integer.parseInt(request.getParameter("ID_NOTA"));
				iIdProductoBase	= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				sTxReferencia	= request.getParameter("TX_REFERENCIA");
				sSituacion		= request.getParameter("SIT_MOVIMIENTO");
				sTipoOperacion	= request.getParameter("PARAM_CTL_OPERACION");
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " + iIdEmpresa);
					System.out.println("sIdMovimiento: " + iIdMovimiento);
					System.out.println("iIdNota: " + iIdNota);
					System.out.println("sIdProductoBase: " + iIdProductoBase);
					System.out.println("sTxReferencia: " + sTxReferencia);
					System.out.println("sUsuario: " + usuario.sCveUsuario);
					System.out.println("sSituacion: " + sSituacion);
					System.out.println("sTipoOperacion: " + sTipoOperacion);
				}
				
				
				try {
				
					dao.abreConexion();
					sResultado = dao.sAltaModSalida(iIdEmpresa, iIdNota, iIdMovimiento,  
					  	iIdProductoBase, sTxReferencia, usuario.sCveUsuario, sSituacion, sTipoOperacion, lbDebug);

					if(lbDebug){
						System.out.println("SResultado directo sAltaModSalida: " + sResultado);
					}
					sParametros[0] = sResultado;
					sParametros[1] = null;
					sResultado	= new Gson().toJson(sParametros);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		            String sError = exc.getMessage();
		            if(sError.contains("ORA-20")){
		            	sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
		            }else{
		            	sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
		            }
		            sParametros[0] = sResultado;
					sParametros[1] = "1";
					sResultado	= new Gson().toJson(sParametros);   
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "ALTA_MOD_AJU_SUMA": 
        		if(lbDebug){
					System.out.println("ALTA_MOD_AJU_SUMA *********************************");
				}
				dao 				= new OpticsDAO();
				
				sResultado 			= "";
				iIdMovimiento		= Integer.parseInt(request.getParameter("ID_MOVIMIENTO"));
				String sFMovimiento	= request.getParameter("F_MOVIMIENTO");
				iIdAlmacen			= Integer.parseInt(request.getParameter("ID_ALMACEN"));
				iIdProductoBase		= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				sTxReferencia		= request.getParameter("TX_REFERENCIA");
				sSituacion			= request.getParameter("SIT_MOVIMIENTO");
				sTipoOperacion		= request.getParameter("PARAM_CTL_OPERACION");
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " + iIdEmpresa);
					System.out.println("sIdMovimiento: " + iIdMovimiento);
					System.out.println("sFMOvimiento: " + sFMovimiento);
					System.out.println("sIdAlmacen: " + iIdAlmacen);
					System.out.println("sIdProductoBase: " + iIdProductoBase);
					System.out.println("sTxReferencia: " + sTxReferencia);
					System.out.println("sUsuario: " + usuario.sCveUsuario);
					System.out.println("sSituacion: " + sSituacion);
					System.out.println("sTipoOperacion: " + sTipoOperacion);
				}
				
				try {
				
					dao.abreConexion();
					sResultado = dao.sAltaModAjusSuma(iIdEmpresa, iIdMovimiento, sFMovimiento, iIdAlmacen, 
					  	iIdProductoBase, sTxReferencia, usuario.sCveUsuario, sSituacion, sTipoOperacion, lbDebug);

				
					if(lbDebug){
						System.out.println("SResultado directo: " + sResultado);
					}
					sParametros[0] = sResultado;
					sParametros[1] = null;
					sResultado	= new Gson().toJson(sParametros);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		            String sError = exc.getMessage();
		            if(sError.contains("ORA-20")){
		            	sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
		            }else{
		            	sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
		            }
		            sParametros[0] = sResultado;
					sParametros[1] = "1";
					sResultado	= new Gson().toJson(sParametros);   
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "ALTA_MOD_AJUSTE_RESTA": 
        		if(lbDebug){
					System.out.println("ALTA_MOD_AJUSTE_RESTA*********************************");
				}
				
				dao 	= new OpticsDAO();
				
				sResultado 		= "";
				iIdMovimiento	= Integer.parseInt(request.getParameter("ID_MOVIMIENTO"));
				sFMovimiento	= request.getParameter("F_MOVIMIENTO");
				iIdAlmacen		= Integer.parseInt(request.getParameter("ID_ALMACEN"));
				iIdProductoBase	= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				sTxReferencia	= request.getParameter("TX_REFERENCIA");
				sSituacion		= request.getParameter("SIT_MOVIMIENTO");
				sTipoOperacion	= request.getParameter("PARAM_CTL_OPERACION");
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " + iIdEmpresa);
					System.out.println("sIdMovimiento: " + iIdMovimiento);
					System.out.println("sFMOvimiento: " + sFMovimiento);
					System.out.println("sIdAlmacen: " + iIdAlmacen);
					System.out.println("sIdProductoBase: " + iIdProductoBase);
					System.out.println("sTxReferencia: " + sTxReferencia);
					System.out.println("sUsuario: " + usuario.sCveUsuario);
					System.out.println("sSituacion: " + sSituacion);
					System.out.println("sTipoOperacion: " + sTipoOperacion);
				}
				
				try {
				
					dao.abreConexion();
					sResultado = dao.sAltaModAjusteResta(iIdEmpresa, iIdMovimiento, sFMovimiento, iIdAlmacen, 
					  	iIdProductoBase, sTxReferencia, usuario.sCveUsuario, sSituacion, sTipoOperacion, lbDebug);

					if(lbDebug){
						System.out.println("SResultado directo sAltaModAjusteResta: " + sResultado);
					}
					sParametros[0] 	= sResultado;
					sParametros[1] 	= null;
					sResultado		= new Gson().toJson(sParametros);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		            String sError = exc.getMessage();
		            if(sError.contains("ORA-20")){
		            	sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
		            }else{
		            	sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
		            }
		            sParametros[0] = sResultado;
					sParametros[1] = "1";
					sResultado	= new Gson().toJson(sParametros);   
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "ALTA_MOD_DEVOLUCION_VENTA": 
        		if(lbDebug){
					System.out.println("ALTA_MOD_DEVOLUCION_VENTA*********************************");
				}
				
				dao 	= new OpticsDAO();
				
				sResultado 		= "";
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " + request.getParameter("ID_EMPRESA"));
					System.out.println("sIdMovimiento: " + request.getParameter("ID_MOVTO_VENTA"));
					System.out.println("iIdDevolucion: " + request.getParameter("ID_DEVOLUCION"));
					System.out.println("sUsuario: " + usuario.sCveUsuario);
					System.out.println("sSituacion: " + request.getParameter("SIT_MOVIMIENTO"));
					System.out.println("TX_REFERENCIA: " + request.getParameter("TX_REFERENCIA"));
				}
				
				iIdMovtoVenta	= Integer.parseInt(request.getParameter("ID_MOVTO_VENTA"));
				iIdDevolucion	= Integer.parseInt(request.getParameter("ID_DEVOLUCION"));
				sSituacion		= request.getParameter("SIT_MOVIMIENTO");
				sTxReferencia	= request.getParameter("TX_REFERENCIA");
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " + iIdEmpresa);
					System.out.println("sIdMovimiento: " + iIdMovtoVenta);
					System.out.println("iIdDevolucion: " + iIdDevolucion);
					System.out.println("sUsuario: " + usuario.sCveUsuario);
					System.out.println("sSituacion: " + sSituacion);
				}
				try {
					
					dao.abreConexion();
					sResultado = dao.sAltaModDevolucionVenta(iIdEmpresa, iIdMovtoVenta, iIdDevolucion, 
								sTxReferencia, usuario.sCveUsuario, sSituacion, lbDebug);

				
					if(lbDebug){
						System.out.println("SResultado directo sAltaModDevolucionVenta: " + sResultado);
					}
					sParametros[0] 	= sResultado;
					sParametros[1] 	= null;
					sResultado		= new Gson().toJson(sParametros);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		            String sError 	= exc.getMessage();
		            if(sError.contains("ORA-20")){
		            	sResultado 	= sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
		            }else{
		            	sResultado 	= servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
		            }
		            if(lbDebug){
		            	System.out.println("SResultado en Error: " + sResultado);
		            }
		            sParametros[0] 	= sResultado;
					sParametros[1] 	= "1";
					sResultado		= new Gson().toJson(sParametros);   
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "REPORTE_CLIENTE_VEND_VTA":
        		if(lbDebug){
					System.out.println("REPORTE_CLIENTE_VEND_VTA*********************************");
				}
				
				dao 	= new OpticsDAO();
				
				sResultado 		= "";
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " 			+ request.getParameter("ID_EMPRESA"));
					System.out.println("PREOPER_F_MOVIMIENTO: " + request.getParameter("PREOPER_F_MOVIMIENTO"));
					System.out.println("F_MOVIMIENTO: " 		+ request.getParameter("F_MOVIMIENTO"));
					System.out.println("POSTVAL_F_MOVIMIENTO: " + request.getParameter("POSTVAL_F_MOVIMIENTO"));
					System.out.println("ID_CLIENTE: " 			+ request.getParameter("ID_CLIENTE"));
					System.out.println("sUsuario: " 			+ usuario.sCveUsuario);
					System.out.println("ID_PRODUCTO_BASE: " 	+ request.getParameter("ID_PRODUCTO_BASE"));
					System.out.println("ID_ALMACEN: " 			+ request.getParameter("ID_ALMACEN"));
					System.out.println("USUARIO_CREACION: " 	+ request.getParameter("USUARIO_CREACION"));
				}
				
				iIdProductoBase		= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				iIdAlmacen			= Integer.parseInt(request.getParameter("ID_ALMACEN"));
				int iIdCliente		= (request.getParameter("ID_CLIENTE")!=null&&!request.getParameter("ID_CLIENTE").equals(""))
											?Integer.parseInt(request.getParameter("ID_CLIENTE")):0;
				String sPreOperFMovto	= request.getParameter("PREOPER_F_MOVIMIENTO");
				String sFMovto			= request.getParameter("F_MOVIMIENTO");
				String sPostValFMovto	= request.getParameter("POSTVAL_F_MOVIMIENTO");
				String sVendedor		= request.getParameter("USUARIO_CREACION");
				
				try {
					
					dao.abreConexion();
					sParametros = dao.sReporteVtaCteAlm(iIdEmpresa, iIdProductoBase, iIdAlmacen, 
							  sPreOperFMovto, sFMovto, sPostValFMovto, 
							  iIdCliente, sVendedor, usuario.sCveUsuario, lbDebug);

					if(lbDebug){
						System.out.println("SResultado directo sReporteVtaCteAlm: " + sResultado);
					}
					sResultado	= new Gson().toJson(sParametros);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		            String sError = exc.getMessage();
		            if(sError.contains("ORA-20")){
		            	sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
		            }else{
		            	sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
		            }
		            if(lbDebug){
		            	System.out.println("SResultado en Error: " + sResultado);
		            }
		            sParametros[0] = sResultado;
					sParametros[1] = "1";
					sResultado	= new Gson().toJson(sParametros);   
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "REPORTE_EXISTENCIAS": 
        		if(lbDebug){
					System.out.println("REPORTE_EXISTENCIAS*********************************");
				}
				
				dao 	= new OpticsDAO();
				
				sResultado 		= "";
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " 			+ request.getParameter("ID_EMPRESA"));
					System.out.println("sUsuario: " 			+ usuario.sCveUsuario);
					System.out.println("ID_PRODUCTO_BASE: " 	+ request.getParameter("ID_PRODUCTO_BASE"));
					System.out.println("ID_ALMACEN: " 			+ request.getParameter("ID_ALMACEN"));
				}
				
				iIdProductoBase		= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				iIdAlmacen			= Integer.parseInt(request.getParameter("ID_ALMACEN"));
				
				try {
					
					dao.abreConexion();
					sParametros = dao.sReporteExistenciaAlm(iIdEmpresa, iIdProductoBase, iIdAlmacen, 
							  usuario.sCveUsuario, lbDebug);
				
					if(lbDebug){
						System.out.println("SResultado directo sReporteExistenciaAlm: " + sResultado);
					}
					sResultado	= new Gson().toJson(sParametros);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		            String sError = exc.getMessage();
		            if(sError.contains("ORA-20")){
		            	sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
		            }else{
		            	sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
		            }
		            if(lbDebug){
		            	System.out.println("SResultado en Error: " + sResultado);
		            }
		            sParametros[0] = sResultado;
					sParametros[1] = "1";
					sResultado	= new Gson().toJson(sParametros);   
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "DAME_EXISTENCIA_OTROS_PRODUCTOS": 
        		if(lbDebug){
					System.out.println("DAME_EXISTENCIA_OTROS_PRODUCTOS *********************");
				}
				
				dao 	= new OpticsDAO();
				
				sResultado 		= "";
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " 			+ request.getParameter("ID_EMPRESA"));
					System.out.println("sUsuario: " 			+ usuario.sCveUsuario);
					System.out.println("ID_PRODUCTO: " 			+ request.getParameter("ID_PRODUCTO"));
					System.out.println("ID_ALMACEN: " 			+ request.getParameter("ID_ALMACEN"));
					System.out.println("CVE_COLOR: " 			+ request.getParameter("CVE_COLOR"));
				}
				
				iIdProducto		= Integer.parseInt(request.getParameter("ID_PRODUCTO"));
				iIdAlmacen		= Integer.parseInt(request.getParameter("ID_ALMACEN"));
				sCveColor	= request.getParameter("CVE_COLOR");
				
				try {
					
					dao.abreConexion();
					sResultado = dao.getExistenciaOtrosProductos(iIdEmpresa, iIdProducto, iIdAlmacen, sCveColor, lbDebug);
				
					if(lbDebug){
						System.out.println("SResultado directo getExistenciaOtrosProductos: " + sResultado);
					}
					sResultado	= new Gson().toJson(sResultado);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		            String sError = exc.getMessage();
		            
		            if(sError.contains("ORA-20")){
		            	sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
		            }else{
		            	sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
		            }
		            if(lbDebug){
		            	System.out.println("SResultado en Error: " + sResultado);
		            }
					sResultado	= new Gson().toJson(sParametros);   
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "ALT_MOD_MOVTO_OTROS_PRODS": 
        		if(lbDebug){
					System.out.println("ALT_MOD_MOVTO_OTROS_PRODS *********************");
				}
				
				dao 	= new OpticsDAO();
				
				sResultado 		= "";
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " 			+ request.getParameter("ID_EMPRESA"));
					System.out.println("sUsuario: " 			+ usuario.sCveUsuario);
					System.out.println("ID_NOTA: " 				+ request.getParameter("ID_NOTA"));
					System.out.println("ID_MOVIMIENTO: " 		+ request.getParameter("ID_MOVIMIENTO"));
					System.out.println("ID_PRODUCTO: " 			+ request.getParameter("ID_PRODUCTO"));
					System.out.println("CANTIDAD: " 			+ request.getParameter("CANTIDAD"));
					System.out.println("TIPO: " 				+ request.getParameter("TIPO"));
					System.out.println("CVE_COLOR: "			+ request.getParameter("CVE_COLOR"));
					System.out.println("PRECIO: "				+ request.getParameter("PRECIO"));
					System.out.println("SIT_MOVIMIENTO: "		+ request.getParameter("SIT_MOVIMIENTO"));
				}
				
				iIdNota			= Integer.parseInt(request.getParameter("ID_NOTA"));
				iIdProducto		= 0; 
				iCantidad		= 0;
				iIdMovimiento	= 0;
				fPrecio			= (request.getParameter("PRECIO")!=null&&!request.getParameter("PRECIO").equals("")?
									Float.parseFloat(request.getParameter("PRECIO")):0);
				String sTipo	= request.getParameter("TIPO");
				sCveColor		= request.getParameter("CVE_COLOR");
				String sSitMovimiento	= request.getParameter("SIT_MOVIMIENTO");

				if(sTipo.equals("M")){
					iIdMovimiento 		= Integer.parseInt(request.getParameter("ID_MOVIMIENTO"));
				}
				if(sTipo.equals("A")){
					iIdProducto			= Integer.parseInt(request.getParameter("ID_PRODUCTO"));
					iCantidad			= Integer.parseInt(request.getParameter("CANTIDAD"));
				}

				try {
					
					dao.abreConexion();
					sResultado = dao.sAltaModOtrosProds(iIdEmpresa, iIdNota, iIdMovimiento, 
						  	iIdProducto, iCantidad, usuario.sCveUsuario, sTipo, // tipo operacion 
						  	sCveColor, fPrecio, sSitMovimiento, usuario.iIdRol, lbDebug);
				
					if(lbDebug){
						System.out.println("SResultado directo sAltaModOtrosProds: " + sResultado);
					}
					sResultado	= new Gson().toJson(sResultado);   

				} catch (Exception exc) {
		            exc.printStackTrace();
		            String sError = exc.getMessage();
		            
		            if(sError.contains("ORA-20")){
		            	sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
		            }else{
		            	sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
		            }
		            if(lbDebug){
		            	System.out.println("SResultado en Error: " + sResultado);
		            }
					sResultado	= new Gson().toJson(sResultado);   
		        } 
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "ACTUALIZA_MINIMO_INVENTARIO": 
        		if(lbDebug){
					System.out.println("ACTUALIZA_MINIMO_INVENTARIO *********************");
				}
				
				dao 	= new OpticsDAO();
				
				sResultado 		= "0";
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " 			+ request.getParameter("ID_EMPRESA"));
					System.out.println("sUsuario: " 			+ usuario.sCveUsuario);
					System.out.println("ID_ALMACEN: 		" 	+ request.getParameter("ID_ALMACEN"));
					System.out.println("ID_PRODUCTO_BASE: " 	+ request.getParameter("ID_PRODUCTO_BASE"));
					System.out.println("ID_SERIE: " 			+ request.getParameter("ID_SERIE"));
					System.out.println("ID_VALOR_HORIZ: " 		+ request.getParameter("ID_VALOR_HORIZ"));
					System.out.println("ID_VALOR_VERT: " 		+ request.getParameter("ID_VALOR_VERT"));																	
				}
				
				iIdAlmacen		= Integer.parseInt(request.getParameter("ID_ALMACEN"));
				iIdProductoBase	= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				iIdSerie		= Integer.parseInt(request.getParameter("ID_SERIE"));
				fValHorizontal	= (request.getParameter("ID_VALOR_HORIZ")!=null&&!request.getParameter("ID_VALOR_HORIZ").equals("")?
										Float.parseFloat(request.getParameter("ID_VALOR_HORIZ")):0);
				fValVertical	= (request.getParameter("ID_VALOR_VERT")!=null&&!request.getParameter("ID_VALOR_VERT").equals("")?
										Float.parseFloat(request.getParameter("ID_VALOR_VERT")):0);
				iCantidad		= Integer.parseInt(request.getParameter("CANTIDAD"));
				
				try {
					
					dao.abreConexion();
					sResultado = dao.sMergeMinInvProdBase(iIdEmpresa, iIdAlmacen, iIdProductoBase, iIdSerie, 
							fValHorizontal, fValVertical, iCantidad, usuario.sCveUsuario, usuario.iIdRol, lbDebug);
				
					if(lbDebug){
						System.out.println("SResultado directo sMergeMinInvProdBase: " + sResultado);
					}
					sResultado	= new Gson().toJson(sResultado);   

				} catch (Exception exc) {
		            exc.printStackTrace();
		            String sError = exc.getMessage();
		            
		            if(sError.contains("ORA-20")){
		            	sResultado = sError.substring(sError.indexOf(":") + 2, sError.indexOf("\n"));
		            }else{
		            	sResultado = servicio.sDameMensajeSistema(3, usuario.sCveUsuario, lbDebug);
		            }
		            if(lbDebug){
		            	System.out.println("SResultado en Error: " + sResultado);
		            }
					sResultado	= new Gson().toJson(sResultado);   
		        } 
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "ACTUALIZA_SERIES_MIN_INV": 
        		dao 		= new OpticsDAO();
				sResultado 	= "";
				
				if(lbDebug){
					System.out.println("sIdEmpresa: 		" + request.getParameter("ID_EMPRESA"));
					System.out.println("iIdAlmacen: 		" + request.getParameter("ID_ALMACEN"));
					System.out.println("iIdProductoBase: 	" + request.getParameter("ID_PRODUCTO_BASE"));
				}

				iIdAlmacen		= Integer.parseInt(request.getParameter("ID_ALMACEN"));
				iIdProductoBase	= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " + iIdEmpresa);
					System.out.println("iIdProductoBase: " + iIdProductoBase);
				}
				
				try {
					
					dao.abreConexion();
					sResultado = dao.sGetSeriesMinInv(iIdEmpresa, iIdAlmacen, iIdProductoBase, usuario.sCveUsuario, lbDebug);
					sResultado	= new Gson().toJson(sResultado);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break;
        	case "REPORTE_MINIMOS_INVENTARIO_PB": 
        		dao 		= new OpticsDAO();
				sResultado 	= "";
				
				if(lbDebug){
					System.out.println("sIdEmpresa: 		" + request.getParameter("ID_EMPRESA"));
					System.out.println("iIdAlmacen: 		" + request.getParameter("ID_ALMACEN"));
					System.out.println("iIdProductoBase: 	" + request.getParameter("ID_PRODUCTO_BASE"));
				}

				iIdAlmacen		= Integer.parseInt(request.getParameter("ID_ALMACEN"));
				iIdProductoBase	= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " + iIdEmpresa);
					System.out.println("iIdProductoBase: " + iIdProductoBase);
				}
				
				try {
					
					dao.abreConexion();
					sResultado = dao.sGetSeriesInvMenorMin(iIdEmpresa, iIdAlmacen, iIdProductoBase, usuario.sCveUsuario, lbDebug);
			        
					sResultado	= new Gson().toJson(sResultado);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "GET_MOVTOS_OTROS_PRODS_NOTA": 
        		dao 		= new OpticsDAO();
        		map 		= new LinkedHashMap<String, Object>();   
				sResultado 	= "";
				
				if(lbDebug){
					System.out.println("sIdEmpresa: 		" + request.getParameter("ID_EMPRESA"));
					System.out.println("iIdNota: 			" + request.getParameter("ID_NOTA"));
					System.out.println("iIdProductoBase: 	" + request.getParameter("ID_PRODUCTO_BASE"));
					System.out.println("iIdProductoBase: 	" + request.getParameter("ID_PRODUCTO_BASE"));
				}
				
				map.put("DYN_CTL_ID_EMPRESA", usuario.iIdEmpresa);
				map.put("DYN_CTL_BDEBUG", lbDebug);
				map.put("DYN_CTL_ID_NOTA", Integer.parseInt(request.getParameter("ID_NOTA")));
				map.put("DYN_CTL_ID_ROL", usuario.iIdRol);

				try {
					
					dao.abreConexion();
					sResultado = dao.sGetOtrosProdsNotaVta(map);
					//sResultado	= new Gson().toJson(sResultado);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "DAME_DATOS_MATRIZ":
        		System.out.println("DAME_DATOS_MATRIZ *********************");
				
				if(lbDebug){
					System.out.println("sIdEmpresa: 		" + request.getParameter("ID_EMPRESA"));
					System.out.println("iIdProductoBase: 	" + request.getParameter("ID_PRODUCTO_BASE"));
				}

				iIdProductoBase			= Integer.parseInt(request.getParameter("ID_PRODUCTO_BASE"));
				String sConfResultados 	= request.getParameter("CVE_CONFIGURACION");
				
				if(lbDebug){
					System.out.println("sIdEmpresa: " + iIdEmpresa);
					System.out.println("iIdProductoBase: " + iIdProductoBase);
				}

				try {
					
					dao = new OpticsDAO();
					dao.abreConexion();
					
					if(sConfResultados!=null&&!sConfResultados.equals("")){
						
						conf = dao.getMatrizResultadoConf(iIdEmpresa, sConfResultados, lbDebug);
						if(conf!=null&&conf.sQueryDatos!=null&&conf.sParamsDatos!=null){
							
							tokensDatos 	= new StringTokenizer(conf.sParamsDatos, ",");
							
							if(lbDebug)
								System.out.println("Cuenta los tokensDatos: " + tokensDatos.countTokens());
							
							mapDatos = new LinkedHashMap<String, String>();
							String[] parts;
							String sNomParametro 	= "";

							String sParametro = "";
							while(tokensDatos.hasMoreTokens()){
								sParametro = tokensDatos.nextToken();
								parts = sParametro.split(Pattern.quote("|"));
								sNomParametro 	= parts[0];
								
								if(lbDebug){
									System.out.println("En servlet ComboSrv agrega parametro: " + sNomParametro + " Con valor: " + request.getParameter(sNomParametro));
								}
								if(sNomParametro.equals("ID_EMPRESA")){
									mapDatos.put(sNomParametro, String.valueOf(usuario.iIdEmpresa));
								}else{
									mapDatos.put(sNomParametro, request.getParameter(sNomParametro));
								}
						    }
							tokensDatos 	= new StringTokenizer(conf.sParamsDatos, ",");
							
						}
						
						if(conf!=null&&conf.sQueryPlaceholder!=null&&conf.sParamsPlaceholder!=null){
							
							tokensPlaceholder 	= new StringTokenizer(conf.sParamsPlaceholder, ",");
							
							if(lbDebug)
								System.out.println("Cuenta los tokensDatos: " + tokensPlaceholder.countTokens());
							
							mapPlaceholder = new LinkedHashMap<String, String>();
							String[] parts;
							String sNomParametro 	= "";

							String sParametro = "";
							while(tokensPlaceholder.hasMoreTokens()){
								sParametro = tokensPlaceholder.nextToken();
								parts = sParametro.split(Pattern.quote("|"));
								sNomParametro 	= parts[0];
								
								if(lbDebug){
									System.out.println("En servlet agrega parametro: " + sNomParametro + " Con valor: " + request.getParameter(sNomParametro));
								}
								if(sNomParametro.equals("ID_EMPRESA")){
									mapPlaceholder.put(sNomParametro, String.valueOf(usuario.iIdEmpresa));
								}else{
									mapPlaceholder.put(sNomParametro, request.getParameter(sNomParametro));
								}
						    }
							tokensPlaceholder 	= new StringTokenizer(conf.sParamsPlaceholder, ",");
						}
						
					}

					
					if(lbDebug)
			            System.out.println("Elementos en el map: " + (mapDatos!=null?mapDatos.size():""));

					sResultado = dao.sGetDatosMatriz(iIdEmpresa, iIdProductoBase, conf, tokensDatos, mapDatos, 
							tokensPlaceholder, mapPlaceholder, usuario.sCveUsuario, lbDebug);

					sResultado	= new Gson().toJson(sResultado);   

				} catch (Exception exc) {
		            exc.printStackTrace();
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break; 
        	case "ADM_DATOS_MATRIZ":
        		System.out.println("ADM_DATOS_MATRIZ *********************");
				
				dao 		= new OpticsDAO();
				admConf		= new MatrizAdmDatosConf();
				sResultado 	= "";
				
				if(lbDebug){
					System.out.println("sIdEmpresa: 		" + request.getParameter("ID_EMPRESA"));
					System.out.println("sCveConfiguracion: 	" + request.getParameter("CVE_CONFIGURACION"));
				}

				String sCveConfiguracion 	= request.getParameter("CVE_CONFIGURACION");
				
				if(lbDebug){
					System.out.println("sIdEmpresa: 	" + iIdEmpresa);
					System.out.println("iIdProductoBase:" + sCveConfiguracion);
				}

				try {
					
					 
					dao.abreConexion();
					
					if(sCveConfiguracion!=null&&!sCveConfiguracion.equals("")){
						
						admConf = dao.getMatrizAdmDatosConf(iIdEmpresa, sCveConfiguracion, lbDebug);
						if(admConf!=null&&admConf.sParamsAdmDatos!=null){
							
							tokensDatos 	= new StringTokenizer(admConf.sParamsAdmDatos, ",");
							
							if(lbDebug)
								System.out.println("Cuenta los tokensAdmDatos: " + tokensDatos.countTokens());
							
							mapDatos = new LinkedHashMap<String, String>();
							String[] parts;
							String sNomParametro 	= "";

							String sParametro = "";
							while(tokensDatos.hasMoreTokens()){
								sParametro = tokensDatos.nextToken();
								parts = sParametro.split(Pattern.quote("|"));
								sNomParametro 	= parts[0];
								
								if(lbDebug)
						            System.out.println("En servlet ComboSrv agrega parametro: " + sNomParametro + " Con valor: " + request.getParameter(sNomParametro));
								mapDatos.put(sNomParametro, request.getParameter(sNomParametro));
						    }
							tokensDatos 	= new StringTokenizer(admConf.sParamsAdmDatos, ",");
						}
					}

					
					if(lbDebug)
			            System.out.println("Elementos en el map: " + (mapDatos!=null?mapDatos.size():"null"));

					sResultado = dao.sAdmDatosMatriz(iIdEmpresa, admConf, tokensDatos, mapDatos, usuario.sCveUsuario, lbDebug);

					sResultado	= new Gson().toJson(sResultado);   
				} catch (Exception exc) {
		            exc.printStackTrace();
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
        		break;
			}
			
			if(lbDebug){
				System.out.println("SResultadoJson: " + sResultado);
			}			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(sResultado);      
			return;
		}
	}
}
