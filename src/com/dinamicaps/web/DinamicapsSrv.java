package com.dinamicaps.web;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
//import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

// servlet
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;

// objetos
import com.dinamicaps.web.impl.*;
import com.dinamicaps.web.impl.domain.*;


/**
 * Servlet implementation class DinamicapsSrv
 */
public class DinamicapsSrv extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DinamicapsSrv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//response.setContentType("text/html");
        //PrintWriter out = response.getWriter();

		// LinkedLists
        LinkedList<NavegacionTemplate>	listaNavegacion = null;
        LinkedList<Remplazo>			listaRemplazos 	= null;
        LinkedList<Atributo>			listaParametros	= null;
        LinkedList<Detalle>				listaDetalle	= null;
        LinkedList<MasterDetail>		lMasterDetail	= null;

        // Iterators
        Iterator<Remplazo> 				listaEtiquetas	= null;
        Iterator<NavegacionTemplate> 	listaNavega 	= null;
        Iterator<Atributo> 				listaParam		= null;
        Iterator<Detalle> 				listaDet		= null;
        Iterator<MasterDetail> 			listMD			= null;

        // Objetos
        Navegacion navegacion					= null;
        NavegacionTemplate navegacionTemplate 	= null;
        Remplazo remplazo 						= null;
        Pantalla pantalla 						= null;
        Atributo atributo 						= null;
        Detalle detalle							= null;
        Usuario usuario							= null;
        Buttons buttons							= null;
        Pantalla permisosDetalle 				= null;
        BrowserType browserType					= new BrowserType(request.getHeader("user-agent"));
        String sPlantilla 						= "";
        String sPlantillaPopUpWin				= "";
        Ejecutable ejecutable					= null;
        MasterDetail masterDetail				= null;
        String sTipoOperacion					= "";
        String sError							= "";
        String sBodySkin						= "";
        String sJsTableTransform				= "";
        Map<String, Object> map 				= new HashMap<String, Object>();
        String[][] sSetttingsRep			    = new String[10000][2];

        // Variables
        int iIdEmpresa;
        int iIdPantalla;
        int iIdTabla;
        
        if(request.getAttribute("ID_TABLA")==null){
	        if(request.getParameter("PARAM_CTL_ID_TABLA") != null && !request.getParameter("PARAM_CTL_ID_TABLA").equals("")){
	        	iIdTabla = Integer.parseInt(request.getParameter("PARAM_CTL_ID_TABLA"));
	        }else{
	        	iIdTabla = -1;
	        }
        }else{
        	iIdTabla = (Integer)request.getAttribute("ID_TABLA");
        }

        //= Integer.parseInt(request.getParameter("PARAM_CTL_ID_TABLA") != null ? request.getParameter("PARAM_CTL_ID_TABLA") : "0");
        int i = 0;
        int iIdClob = 0;
        int iPageNumber = 0;
        
        String sCveNavegacion = request.getAttribute("CVE_NAVEGACION")==null ? (request.getParameter("PARAM_CTL_CVE_NAVEGACION") == null ? "NAVEGA_CAT_CTL_MENU" : request.getParameter("PARAM_CTL_CVE_NAVEGACION"))
        		: (String)request.getAttribute("CVE_NAVEGACION");
        String sInsertStmt 	= "";
        String sRemplazo	= "";
        int iIdTablaAnterior = (Integer)request.getAttribute("ID_TABLA_ANTERIOR")==null?0:(Integer)request.getAttribute("ID_TABLA_ANTERIOR");
        String[] sParametros= new String[1000];
        String sErrorHtml	= null;
        //String sMacAddress	= "00-27-0E-08-7E-41";
        String sJavaScriptCode	= 	"\n";
        String sJavaScriptCodeTmp	= null;
        String sEsPopUpWindow	= request.getParameter("PARAM_CTL_ES_POPUP_WINDOW") == null ? "NO" : "SI";
        
        boolean bExisteDetalle 		= false;
        boolean lbDebug				= false;
        Date date = null;
        
        // Se inicializa el sevicio de conexi�n
        Servicio servicio = new Servicio();
        
        //Obtain the session object, create a new session if doesn't exist
        HttpSession session = request.getSession(false);

        if(session == null){
	        response.setContentType("text/html");
			request.getRequestDispatcher("index.jsp").forward(request,response);
        	return;
        }else{
        	if(request.getParameter("CerrarSesion") != null){
                //System.out.println("*******************         Salir de la sesi�n      ************************************************** ");
        		session.invalidate();
        		//response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
        		//response.setDateHeader("Expires", 0);
    	        response.setContentType("text/html");
    			// request.getRequestDispatcher("index.jsp").forward(request,response);
    			response.sendRedirect("index.jsp");
    			System.out.println("*******************         Salir de la sesi�n      ************************************************** ");
    			return;
        	}else{
        		//System.out.println("*******************         Obtiene usuario de la sesi�n    ************************************************** ");
	        	usuario 			= (Usuario) session.getAttribute("Usuario");
	        	sPlantilla 			= (String)  session.getAttribute("Plantilla");
	        	sPlantillaPopUpWin	= (String)  session.getAttribute("PlantillaPopUpWin");
	        	buttons				= (Buttons) session.getAttribute("Buttons");
	        	//map 				= (Map<String, Object>) session.getAttribute("Settings");
	            
	        	if(usuario == null){
	    	        response.setContentType("text/html");
	    			request.getRequestDispatcher("index.jsp").forward(request,response);
	        		return;
	        	}
	        	iIdEmpresa 	= usuario.iIdEmpresa;
	        	lbDebug		= servicio.bDebug(iIdEmpresa, usuario.sCveUsuario);			
	            navegacion 	= servicio.getNavegacion(sCveNavegacion, lbDebug);
	        	
	        	if(lbDebug){
		        	System.out.println("*******************         entra al get      **************************************************: ");
		            date = new Date();
		            System.out.println("fecha de sistema: " + date.toString());
	        	}
	        	
	        	
	        	if(sPlantilla!=null){
	            	if(sPlantilla.equals("")){
	    		        request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(53, usuario.sCveUsuario, lbDebug));
	    		        response.setContentType("text/html");
	    				request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
	                	return;
	            	}
	            }else{
	            	request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(53, usuario.sCveUsuario, lbDebug));
    		        response.setContentType("text/html");
    				request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
                	return;
	            } 
	        	
        		// THE PAGE NUMBER IS CLACULATED
        		iPageNumber = (Integer)session.getAttribute("pageNumber")==null?1:(Integer)session.getAttribute("pageNumber");
        		iPageNumber = (iPageNumber + 1)>19?1:(iPageNumber + 1);
        		session.setAttribute("pageNumber",iPageNumber);//*/
        	}
        }
 
        // Se verifica que la interface est� compilada
        //System.out.println(" servicio.CompilaInterface 1 **** " + servicio.sCompilaInterface(lbDebug));     
    	iIdEmpresa = usuario.iIdEmpresa;
        
    	if(lbDebug){

	        System.out.println("***********************************************************************************************************: ");
	        date = new Date();
	        System.out.println("fecha de sistema: " + date.toString());
	        System.out.println("Usuario: " + usuario.sCveUsuario + " ejecuta GET " );
	        System.out.println("BROWSER: " + browserType.getName());
	        System.out.println("VERSION: " + browserType.getVersion());
	        System.out.println("PARAM_CTL_CVE_NAVEGACION: " + request.getParameter("PARAM_CTL_CVE_NAVEGACION"));
	        System.out.println("PARAM_CTL_PANTALLA: " + request.getParameter("PARAM_CTL_PANTALLA"));
	        System.out.println("PARAM_CTL_ID_EMPRESA: " + request.getParameter("PARAM_CTL_ID_EMPRESA"));
	        System.out.println("PARAM_CTL_ID_TABLA: " + request.getParameter("PARAM_CTL_ID_TABLA"));
	        System.out.println("request.getContextPath(): " + request.getContextPath());
        }



        if(request.getAttribute("ID_PANTALLA")==null){
	        if(request.getParameter("PARAM_CTL_PANTALLA") != null && !request.getParameter("PARAM_CTL_PANTALLA").equals("")){
	        	iIdPantalla = Integer.parseInt(request.getParameter("PARAM_CTL_PANTALLA"));
	        }else{
	        	request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(23, usuario.sCveUsuario, lbDebug));
		        response.setContentType("text/html");
				request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
            	return;
	        }
        }else{
        	iIdPantalla = (Integer)request.getAttribute("ID_PANTALLA");
        }
        
        // Se obtienen los datos de la pantalla
        pantalla = servicio.getPantalla(iIdEmpresa, iIdPantalla, iIdTabla, usuario.iIdRol, navegacion.sCVE_OPERACION, usuario.sCveUsuario, lbDebug);

        
        if(pantalla==null || pantalla.sSitPantalla.equals("IN")){
        	request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(23, usuario.sCveUsuario, lbDebug));
	        response.setContentType("text/html");
			request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
        	return;
        }

        if(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_BUSQUEDA")){
        		sJsTableTransform = pantalla.sJQTransformRSTable==null?"":pantalla.sJQTransformRSTable;
       	}

        if(sEsPopUpWindow.equals("NO")){
            /*if(lbDebug){
    	        System.out.println("*******************    NO ES POPUP WINDOW *******************************************************: ");
    	        System.out.println(sPlantilla);
    	        System.out.println("***********************************************************************************************************: ");
            }*/
	        pantalla.sHtmlPantalla = sPlantilla.replace("<!--PSC-->", pantalla.sPageSpecificPlugins==null?"":pantalla.sPageSpecificPlugins);	

        }else{
            /*if(lbDebug){
    	        System.out.println("*******************   SI ES POPUP WINDOW *******************************************************: ");
    	        System.out.println(sPlantillaPopUpWin);
    	        System.out.println("***********************************************************************************************************: ");
            }*/
        	pantalla.sHtmlPantalla = sPlantillaPopUpWin.replace("<!--PSC-->", pantalla.sPageSpecificPlugins==null?"":pantalla.sPageSpecificPlugins);
        }
        
		sTipoOperacion = request.getParameter("PARAM_CTL_OPERACION") ;
		

        /*******************   SETTINGS   *************************************************************/

    	
    	Cookie[] cookies = request.getCookies();
    	
    	String sCookies = "";
    	
    	for (int y = 0; y < cookies.length; y++) {
    		if(lbDebug){
    	        System.out.println("valor de y: >" + y + "<");
            }
    	  String sName = cookies[y].getName();
    	  String sValue = cookies[y].getValue();
    	  
    	  if(sName.startsWith("dyn.") && sValue!=null && !sValue.equals("false")){
    		  sCookies = sCookies + (!sCookies.equals("")?"|":"") + sName + sValue;
    	  }
    	  
    	  if(lbDebug){

    	        System.out.println("***************************  cookies en GET   **************************************************: ");
    	        System.out.println("sName: >" + sName + "<");
    	        System.out.println("sValue: >" + sValue + "<");
          }

    	  if(sName!=null&&sName.equals("ace.body.skin")){
    		  sBodySkin = sValue;
			  if(lbDebug){
				  System.out.println("Actualiza el valor de sBodySkin: " + sBodySkin);
			  }
    		  //break; 
    	  }
    	  
		if(lbDebug){
			System.out.println(sName+sValue);
			System.out.println(map.containsKey(sName+sValue));
		}
    	  
    	  if(map.containsKey(sName+sValue)){
    		  sSetttingsRep = (String[][])map.get(sName+sValue);
    		  if(lbDebug){
	      	        System.out.println(" Contiene el valor: " + sName+sValue);
	      	      System.out.println(" Elementos: " + sSetttingsRep.length);
	            }

    		  
    	  }
    	  
    	
    	}
    	
    	if(!sCookies.equals("")){
    		if(lbDebug){
      	        System.out.println(" sCookies: " + sCookies);
            }
    		
    		map = servicio.getSettings(usuario.sCveMasterTemplate, usuario.sCveMasterTemplateVersion, sCookies, lbDebug);

    		
       		if(lbDebug){
      	      System.out.println(" map.length: " + map.size());
            }
 
       		
       		//*********************************************************************
       		
       		int iSize = map.size(); 
       		
       	   
       		
       		
       		//*********************************************************************

       		for (int x=0; x<=iSize; x++){
				 
				 if(lbDebug){
					 System.out.println("Valor del token:" + x);
				 }
				 
				 if(map.containsKey(String.valueOf(x))){
					 System.out.println("Si contiene la llave:" + x);
				 }else{
					 System.out.println("No contiene la llave:  " + x);
				 }

				 sSetttingsRep = (String[][])map.get(String.valueOf(x));
				 if(sSetttingsRep!=null){
					 if(lbDebug){
						 System.out.println("sSetttingsRep.length:" + sSetttingsRep.length);
					 } 
					 
					 for (int z=0; z<1000; z++){
						 
						 if(sSetttingsRep[z][0]==null){
							 break;
						 }
						 
						 if(lbDebug){
				    		 System.out.println("sSetttingsRep[z][0]:" + sSetttingsRep[z][0]);
				    		 System.out.println("sSetttingsRep[z][1]:" + sSetttingsRep[z][1]);
				    	 }
						 pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace(sSetttingsRep[z][0], sSetttingsRep[z][1]);
					 
					 }
				 }
			 }
    	}
    	
    	
    	/*******************   TERMINA SETTINGS   *************************************************************/

		
		if(lbDebug){
			date = new Date();
	        System.out.println("fecha de sistema: " + date.toString());
	        System.out.println("sTipoOperacion:  *********>  " + sTipoOperacion);
        }
		
		if(sTipoOperacion==null){
			if(lbDebug){
		        System.out.println("sTipoOperacion es NULL ");
	        }
			request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(22, usuario.sCveUsuario, lbDebug));
	        response.setContentType("text/html");
			request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
        	return;
		}

		request.setAttribute("FROM_LOGIN", "YES");
		
		if(sTipoOperacion.equals("C") && !pantalla.bBConsulta && request.getAttribute("FROM_LOGIN")==null){
			
			if(lbDebug){
		        System.out.println("No tiene permisos de Consultar = " + pantalla.bBConsulta);
	        }
			request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(21, usuario.sCveUsuario, lbDebug));
	        response.setContentType("text/html");
			request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
        	return;
        }

	    pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("@TITULO_PAGINA", (pantalla.sTituloPantalla!=null?pantalla.sTituloPantalla:""));

        sInsertStmt	= "SELECT 'ID_EMPRESA',0,1,'CONTROL','" + iIdEmpresa + "',NULL,NULL FROM DUAL --1\n"
        		+ " UNION ALL SELECT 'CVE_USUARIO',0,1,'CONTROL','" + usuario.sCveUsuario + "',NULL,NULL FROM DUAL --1\n";

        
		if(navegacion.sB_OBTIENE_CAMPOS_PK.equals("V")){  		

			//Se recuperan los atributos de la llave primaria 
			listaParametros = servicio.getListaAtributosPk(iIdEmpresa, pantalla.iIdPantalla, iIdTabla, iIdTablaAnterior, "F", lbDebug);

			//  llave primaria 
			if(lbDebug){
				date = new Date();
		        System.out.println("fecha de sistema: " + date.toString());
				System.out.println("llena lista de atributos de llave primaria " + listaParametros.size());
			}

			listaParam = (Iterator<Atributo>) listaParametros.iterator();
			while (listaParam.hasNext()){
				atributo =  listaParam.next();

				if(iIdTabla==172){
					
					if(atributo.sNomAtributo.equals("ID_EMPRESA")){
						sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ atributo.sNomAtributo + "'," + iIdTabla + ",1,'PK','" + iIdEmpresa + "',NULL,NULL FROM DUAL  --2\n";
					}else{
						sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ atributo.sNomAtributo + "'," + iIdTabla + ",1,'PK','" + 
								request.getParameter("login") + "',NULL,NULL FROM DUAL  --2\n";
					}
					
				}else{
					
					if(atributo.sNomAtributo.equals("ID_EMPRESA")){
						sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ atributo.sNomAtributo + "'," + iIdTabla + ",1,'" + 
							(atributo.sIdPk.equals("0")? "NEEDED" : atributo.sCveParameter) + "'," +
			        			"'" + iIdEmpresa + "',NULL,NULL FROM DUAL --3\n";
					}else{
						
						if(atributo.sAtributoHijo!=null){
							if(request.getParameter(atributo.sAtributoHijo) != null && !request.getParameter(atributo.sAtributoHijo).equals("")){
								sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ atributo.sNomAtributo + "'," + iIdTabla + ",1,'" +
								(atributo.sIdPk.equals("0")? "NEEDED" : atributo.sCveParameter) + "','" +
					        			request.getParameter(atributo.sAtributoHijo) + "', NULL,NULL FROM DUAL --4\n";
							}
							
						}else{
							if(request.getParameter(atributo.sNomAtributo) != null && !request.getParameter(atributo.sNomAtributo).equals("")){
								sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ atributo.sNomAtributo + "'," + iIdTabla + ",1,'" +
								(atributo.sIdPk.equals("0")? "NEEDED" : atributo.sCveParameter) + "','" +
					        			request.getParameter(atributo.sNomAtributo) + "', NULL,NULL FROM DUAL --5\n";
							}else{
								if(request.getAttribute(atributo.sNomAtributo)!=null&&!Integer.toString((Integer)request.getAttribute(atributo.sNomAtributo)).equals("")
										&&atributo.sSequenceAutoInc!=null){
									sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ atributo.sNomAtributo + "'," + iIdTabla + ",1,'" +
											(atributo.sIdPk.equals("0")? "NEEDED" : atributo.sCveParameter) + "','" +
											Integer.toString((Integer)request.getAttribute(atributo.sNomAtributo)) + "', NULL,NULL FROM DUAL --5\n";
								}
							}
						}
					}
				}
			}
		}

		if(lbDebug){
			date = new Date();
	        System.out.println("fecha de sistema: " + date.toString());
			System.out.println("ANTES DE REGISTROS ENCONTRADOS  ");	
			System.out.println("navegacion.sB_OBTIENE_CAMPOS_FILTRO  >" + navegacion.sB_OBTIENE_CAMPOS_FILTRO + "<");	
			System.out.println("navegacion.sCVE_NAVEGACION  " + navegacion.sCVE_NAVEGACION);
			System.out.println("usuario.bCambioPassword  " + usuario.bCambioPassword);
		}
		
		
        if(navegacion.sB_OBTIENE_CAMPOS_FILTRO.equals("V") && !usuario.bCambioPassword){
        	
        	if(lbDebug){
        		System.out.println("entra a validaci�n ");	
        	}
        	
			//SE RECUPERAN LOS PAR�METROS DE B�SQUEDA
			listaParametros = servicio.getListaAtributosTabla(iIdEmpresa, pantalla.iIdPantalla, iIdTabla, "FILTRO", usuario.iIdRol, sTipoOperacion, lbDebug);
			listaParam = (Iterator<Atributo>) listaParametros.iterator();
			
			if(lbDebug){
				System.out.println("REGISTROS ENCONTRADOS: " + listaParametros.size());	
			}
			
			while (listaParam.hasNext()){
				atributo =  listaParam.next();

				if(atributo.sTipoTemplate.contains("CHECKBOX")){
					//if(lbDebug){
					//	System.out.println("el atributo " + atributo.sNomAtributo + " contiene checkbox");
					//	System.out.println("el tipo template es >" + atributo.sTipoTemplate + "<");
					//	System.out.println("el valor es >" + request.getParameter(atributo.sNomAtributo) + "<");
					//}		

					if(request.getParameter(atributo.sNomAtributo) != null){
						sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ atributo.sNomAtributo + "'," + iIdTabla + ",1,'FILTRO','V',NULL,NULL FROM DUAL --6\n";
					}

				}else{
					//if(lbDebug){
					//	System.out.println("el atributo " + atributo.sNomAtributo + " NO contiene checkbox");
					//	System.out.println("el tipo template es >" + atributo.sTipoTemplate + "<");
					//	System.out.println("el valor es >" + request.getParameter(atributo.sNomAtributo) + "<");
					//}		
				
					if(request.getParameter(atributo.sNomAtributo) != null){// && !request.getParameter(atributo.sNomAtributo).equals("")){
						sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ atributo.sNomAtributo + "'," + iIdTabla + ",1,'FILTRO','" +
			        			request.getParameter(atributo.sNomAtributo) + "','" +
			        			((atributo.sTipoTemplate.equals("LINK_POPUP") && request.getParameter("CTL_DESC_" + atributo.sNomAtributo) != null &&
			        					!request.getParameter("CTL_DESC_" + atributo.sNomAtributo).equals("")) ? 
			        					(request.getParameter("CTL_DESC_" + atributo.sNomAtributo).replace("'", "''")) : "NULL") +
			        			"',NULL FROM DUAL --7\n";
						
					}
					if((atributo.sTipoDato.equals("DATE") || atributo.sTipoDato.equals("NUMBER")) && 
							request.getParameter("PREOPER_" + atributo.sNomAtributo) != null && !request.getParameter("PREOPER_" + atributo.sNomAtributo).equals("")){
						sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ "PREOPER_" + atributo.sNomAtributo + "'," + iIdTabla + ",1,'FILTRO','" +
			        			request.getParameter("PREOPER_" + atributo.sNomAtributo) + "',NULL,NULL FROM DUAL --8\n";
					}
					if((atributo.sTipoDato.equals("DATE") || atributo.sTipoDato.equals("NUMBER")) && 
							request.getParameter("POSTVAL_" + atributo.sNomAtributo) != null && !request.getParameter("POSTVAL_" + atributo.sNomAtributo).equals("")){
						sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ "POSTVAL_" + atributo.sNomAtributo + "'," + iIdTabla + ",1,'FILTRO','" +
			        			request.getParameter("POSTVAL_" + atributo.sNomAtributo) + "', NULL,NULL FROM DUAL --9\n";
					}
				}
			}
    	}
        
      //if(sCveNavegacion.equals("PARAM_CTL_GET_REG")){
		if(navegacion.sB_OBTIENE_CONSULTA_MAESTRO_DET.equals("V")){

			lMasterDetail = servicio.getListMDAttributes(iIdEmpresa, pantalla.iIdPantalla, iIdTabla, iIdTablaAnterior, lbDebug);
			listMD				= (Iterator<MasterDetail>)lMasterDetail.iterator();
			int iOrder 			= 0;
			int iDetailTableTMp = 0;
	
			while(listMD.hasNext()){
				bExisteDetalle	= true;
				masterDetail 	= listMD.next();
				iOrder			= iDetailTableTMp!=masterDetail.iIdDetailTable?(iOrder + 1):iOrder;

				if(lbDebug){
					date = new Date();
			        System.out.println("fecha de sistema: " + date.toString());
					System.out.println("VALOR DE masterDetail.iIdDetailTable = " + masterDetail.iIdDetailTable);
					System.out.println("masterDetail.sDetailAttribute = " + masterDetail.sDetailAttribute);
				}
				
				if(!masterDetail.sDetailAttribute.equals("ID_EMPRESA")){
					
					if(masterDetail.sCveParametroEnvio==null){
						sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ masterDetail.sDetailAttribute + "'," + masterDetail.iIdDetailTable + ",1,'FILTRO','" +
			        			request.getParameter(masterDetail.sMasterAttribute) + "',NULL,NULL FROM DUAL --10\n";
						
						sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ masterDetail.sDetailAttribute + "'," + masterDetail.iIdDetailTable + ",1,'PK','" +
			        			request.getParameter(masterDetail.sMasterAttribute) + "',NULL,NULL FROM DUAL --11\n";
					}else{
						sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ masterDetail.sMasterAttribute + "',-1,1,'" + masterDetail.sCveParametroEnvio + "','" +
			        			request.getParameter(masterDetail.sMasterAttribute) + "',NULL,NULL FROM DUAL --12\n";
					}
				}
  
				// Se agregan los par�metros
				sParametros[iOrder] = (sParametros[iOrder] == null ? "" : sParametros[iOrder]) + 
						
						(masterDetail.sCveParametroEnvio!=null ? (

								"  <input name=\"" + masterDetail.sMasterAttribute + "\" value=\"\" id=\"" + 
										masterDetail.sMasterAttribute + masterDetail.iIdDetailTable + "\" type=\"hidden\"> \n" + 
										" <script>document.getElementById('"+ masterDetail.sMasterAttribute + masterDetail.iIdDetailTable + 
										"').value = document.getElementById('" + masterDetail.sMasterAttribute + "').value;</script> \n"
								) : (

						(masterDetail.sDetailAttribute.equals("ID_EMPRESA") ?  
						("  <input name=\"ID_EMPRESA\" value=\"" + iIdEmpresa + "\" type=\"hidden\"> \n")  :

							// Se agrega el valor del par�metro en caso de que no sea ID_EMPRESA
							(("  <input name=\"" + masterDetail.sDetailAttribute + "\" id=\"" + 
									masterDetail.sDetailAttribute + masterDetail.iIdDetailTable + "\" value=\"" + 
								(masterDetail.bIsFKParentPage ? request.getParameter(masterDetail.sMasterAttribute) : "") + // .sDetailAttribute
							 "\" type=\"hidden\"> \n") +

							 (masterDetail.bIsFKParentPage ? "" : (
								" <script>document.getElementById('"+ masterDetail.sDetailAttribute + masterDetail.iIdDetailTable + 
								"').value = document.getElementById('" + masterDetail.sMasterAttribute + "').value;</script> \n"

									 )) +

						((masterDetail.sMasterDescriptionField != null && !masterDetail.sMasterDescriptionField.equals("")) ? 
							(" <input name=\"CTL_DESC_" + masterDetail.sDetailAttribute + 
									"\" id=\"CTL_DESC_" + masterDetail.sDetailAttribute + iOrder + "\" value=\"\" type=\"hidden\"> \n" +
							"<script>document.getElementById('CTL_DESC_"+ masterDetail.sDetailAttribute + iOrder + 
							"').value = document.getElementById('" + masterDetail.sMasterDescriptionField + "').value;</script> \n") : "")))));
				
				iDetailTableTMp = masterDetail.iIdDetailTable;
				if(lbDebug){
					System.out.println("sParametros[" + iOrder + "] = " + sParametros[iOrder]);
				}
			}
		}
        
        
		// WE GET THE ATTRIBUTES FOREIGN KEY 
        if(navegacion.sB_OBTIENE_CAMPOS_FK.equals("V")){
        	if(lbDebug){
        		date = new Date();
                System.out.println("fecha de sistema: " + date.toString());
        		System.out.println("sB_OBTIENE_CAMPOS_FK --->>>>> ");
        	}

			//SE RECUPERAN LOS PAR�METROS DE LLAVE PRIMARIA DE LA TABLA 
			listaParametros = servicio.getListaAtributosFk(iIdEmpresa, pantalla.iIdPantalla, iIdTabla, lbDebug);
			listaParam = (Iterator<Atributo>) listaParametros.iterator();
			atributo = new Atributo();
			while (listaParam.hasNext()){
				atributo =  listaParam.next();

				if(request.getParameter(atributo.sNomAtributo) != null && !request.getParameter(atributo.sNomAtributo).equals("")){
					
					// Se agregan los valores a la variable
					sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ atributo.sNomAtributo + "'," + iIdTabla + ",1,'" + 
							(atributo.sCveParameter==null ? "FK" : atributo.sCveParameter) + "','" +
		        			request.getParameter(atributo.sNomAtributo) + "','" +
		        			((request.getParameter("CTL_DESC_" + atributo.sNomAtributo)!= null && !request.getParameter("CTL_DESC_" + atributo.sNomAtributo).equals("")) ?
		        			request.getParameter("CTL_DESC_" + atributo.sNomAtributo).replace("'", "''") : "") + "',NULL FROM DUAL --13\n";
				}
			}
		}
        
        // Se obtienen los templates a remplazar 
        listaNavegacion = servicio.getNavegacionTemplate(iIdEmpresa, pantalla.iIdPantalla, navegacion.sCVE_NAVEGACION, usuario.sCveMasterTemplate, usuario.sCveMasterTemplateVersion, sEsPopUpWindow, lbDebug);

        listaNavega = (Iterator<NavegacionTemplate>)listaNavegacion.iterator();
        while(listaNavega.hasNext()){
        	navegacionTemplate = listaNavega.next();

        	if(lbDebug){
        		date = new Date();
                System.out.println("fecha de sistema: " + date.toString());
	        	System.out.println("ENTRA A NAVEGACION ********************** ");
	    		System.out.println("sCveNavegacion: -------------------->>>> " + navegacion.sCVE_NAVEGACION);
	    		System.out.println("bExisteDetalle: -------------------->>>> " + bExisteDetalle);
	    		System.out.println("sTipoTemplate: -------------------->>>> " + navegacionTemplate.sTipoTemplate);
        	}
    		
    		//if((sCveNavegacion.equals("PARAM_CTL_GET_REG") && navegacionTemplate.sTipoTemplate.equals("RESULTADO_BUSQUEDA"))){
        	if(navegacionTemplate.sRemplazaConsultaDetalle.equals("V") && bExisteDetalle){
    			if(lbDebug){
    				System.out.println("ENTRA IF sRemplazaConsultaDetalle -------------------->>>> " );
    			}
    			
    			String sListExclude = "";
    			
    			if(pantalla.sQryIncludeDetail!=null){
    				sListExclude = servicio.sDameListaExcluye(iIdEmpresa, sInsertStmt, pantalla.sQryIncludeDetail, lbDebug);
    				if(lbDebug){
    					System.out.println("sListExclude -------------------->>>> " + sListExclude);
    				}
    			}

    			listaDetalle = servicio.getDistinctDetalle(iIdEmpresa, pantalla.iIdPantalla, iIdTabla, usuario.iIdRol, sListExclude, lbDebug);
				listaDet = (Iterator<Detalle>)listaDetalle.iterator();
				i = 0;
				while(listaDet.hasNext()){
					detalle = listaDet.next();

					sJsTableTransform = sJsTableTransform + "\n" + detalle.sJsTableTransform;
					if(lbDebug){
	    				System.out.println("sJsTableTransform navegacion.sCVE_NAVEGACION -------------------->>>>" + navegacion.sCVE_NAVEGACION + " \n" + sJsTableTransform );
	    			}
					
					// Obtiene los permisos para verificar si se debe de desplegar la informaci�n
					permisosDetalle = servicio.getPantalla(iIdEmpresa, pantalla.iIdPantalla, detalle.iDetIdTablaDetalle, usuario.iIdRol, "", usuario.sCveUsuario, lbDebug);

					if(permisosDetalle != null && permisosDetalle.bBConsulta){

						// Se recuperan los par�metros
			    		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace(navegacionTemplate.sEtiquetaTemplate, navegacionTemplate.sHtml +  "\n" +
			    				navegacionTemplate.sEtiquetaTemplate);
			    		
			    		// Se obtiene el listado de remplazos del template
			        	listaRemplazos = servicio.getListaRemplazoEtiquetas(iIdEmpresa, navegacion.sCVE_NAVEGACION, navegacionTemplate.sEtiquetaTemplate, usuario.sCveMasterTemplate, usuario.sCveMasterTemplateVersion, sEsPopUpWindow, lbDebug);
			        	listaEtiquetas = (Iterator<Remplazo>)listaRemplazos.iterator();
			        	while(listaEtiquetas.hasNext()){
			        		remplazo = listaEtiquetas.next();
			        		
			        		if(iIdClob<=0 && remplazo.sBNecesitaDatos.equals("V")){
			        			iIdClob = servicio.iGuardaCLOB(iIdEmpresa, 0, pantalla.iIdPantalla, usuario.sCveUsuario, sInsertStmt, "Servlet get remplazo", lbDebug);
			        		}

			        		// Remplazos que aplican para cada template
			        		remplazo.sProceso = remplazo.sProceso.replace("{ID_EMPRESA}", 	String.valueOf(iIdEmpresa)); 
			        		remplazo.sProceso = remplazo.sProceso.replace("{PANTALLA}", 	String.valueOf(pantalla.iIdPantalla ));
			        		remplazo.sProceso = remplazo.sProceso.replace("{CVE_USUARIO}", 	"'"  + usuario.sCveUsuario + "'");
			        		remplazo.sProceso = remplazo.sProceso.replace("{ID_ROL}", 		String.valueOf(usuario.iIdRol));
			        		remplazo.sProceso = remplazo.sProceso.replace("{ID_TABLA}", 	String.valueOf(detalle.iDetIdTablaDetalle));
			        		remplazo.sProceso = remplazo.sProceso.replace("{BROWSER_TYPE}", "'" + browserType.getName() + "'");
			        		remplazo.sProceso = remplazo.sProceso.replace("{ES_POPUP_WIN}", "'" + sEsPopUpWindow + "'");
			        		remplazo.sProceso = remplazo.sProceso.replace("{ID_DATOS}",  	String.valueOf(iIdClob));

			        		if(lbDebug){
			        			date = new Date();
			        	        System.out.println("fecha de sistema: " + date.toString());
			        			System.out.println("detalle.iDetIdTablaDetalle en GET 1   ????????????????????? :  " + detalle.iDetIdTablaDetalle );
			        			System.out.println("remplazo.sEtiqueta en GET 1   ????????????????????? :  >" + remplazo.sEtiqueta + "<");
			        			System.out.println("remplazo.sProceso : \n DECLARE \n  V CLOB;\nBEGIN \n    V:=  " + remplazo.sProceso + "; \n "
			        					+ "    DBMS_OUTPUT.PUT_LINE(V);  \n END; ");
			        		}
			        				
			        		if(remplazo.sBNecesitaDatos.equals("V") && lbDebug){
			        			System.out.println(" SE NECESITAN DATOS: \n INSERT INTO CONTROL.AOS_ARQ_PARAM_REQ\n" + sInsertStmt 
			        					// + " \n " + sInsertStmt.replace("'", "''")
			        					);
			        		}

			        		// SE OBTIENE EL REMPLAZO
			        		if(remplazo.sBEsTexto.equals("F") && remplazo.sEsJavaScript.equals("F")){
				        		sRemplazo = (servicio.sGetRemplazoEtiqueta(iIdEmpresa, remplazo.sProceso, iIdClob, usuario.sCveUsuario, lbDebug));
			        		}else if (remplazo.sEsJavaScript.equals("F")){
			        			sRemplazo = remplazo.sProceso;
			        		}else{
			        			sRemplazo = null;
			        			sJavaScriptCodeTmp = (servicio.sGetRemplazoEtiqueta(iIdEmpresa, remplazo.sProceso,iIdClob, usuario.sCveUsuario, lbDebug));
			        			if(sJavaScriptCodeTmp!=null&&!sJavaScriptCodeTmp.equals("null")){
			        				sJavaScriptCode = sJavaScriptCode + sJavaScriptCodeTmp;
			        			}
			        		}
			        		
			        		if(sRemplazo != null && sRemplazo.matches("SYS_DYNAPP_ERROR:(.*)")){
			        			sErrorHtml = sRemplazo.replace("SYS_DYNAPP_ERROR:", "");
			        			request.setAttribute("MSG_ERROR", sErrorHtml);
			    		        response.setContentType("text/html");
			    				request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
			                	return;
			        		}
			        		
			        		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace(remplazo.sEtiqueta, (sRemplazo == null ? "" : sRemplazo));
			        		
			        		if(remplazo.sEtiqueta.equals("<!--TABLE_RESULT_SET-->")){
			        			pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("@TableName", "TableRs" + (detalle == null ? "0" :
			        				String.valueOf(detalle.iDetIdTablaDetalle)));
			        		}

			        	}
			        	if(lbDebug){
			        		System.out.println("Entra a remplazar los par�metros ---------------------------------------------------------------------");
			        	}
		        		i = i + 1;
	    				pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--PARAMETROS-->", sParametros[i]);
	    				if(lbDebug){
	    					System.out.println("sParametros[" + i + "] = " + sParametros[i]);
	    				}
	    				
	    				if(permisosDetalle.bBAlta && !sEsPopUpWindow.equals("SI") && permisosDetalle.bBButtonCreateFromResult){
	    					pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON ALTA-->", buttons.sButtonGoToCreateRecord);
	    				}else{
	    					pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON ALTA-->", " ");
	    				}
					}else{
						i = i + 1;
					}
				}
    			
        	}
    		else{

    			if(!(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_GET_REG") && navegacionTemplate.sTipoTemplate.equals("SEARCH_RESULTS") && !bExisteDetalle)){
	    			if(lbDebug){
	    				date = new Date();
	    		        System.out.println("fecha de sistema: " + date.toString());
	    				System.out.println("ENTRA ELSE EN GET -------------------->>>> " );
	    			}
	    			
	    			
	
	    			pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace(navegacionTemplate.sEtiquetaTemplate, navegacionTemplate.sHtml);
		        	listaRemplazos = servicio.getListaRemplazoEtiquetas(iIdEmpresa, navegacion.sCVE_NAVEGACION, navegacionTemplate.sEtiquetaTemplate, usuario.sCveMasterTemplate, usuario.sCveMasterTemplateVersion, sEsPopUpWindow, lbDebug);
		        	listaEtiquetas = (Iterator<Remplazo>)listaRemplazos.iterator();
		        	
		        	if(lbDebug){
		        		date = new Date();
		                System.out.println("fecha de sistema: " + date.toString());
		        		System.out.println("ENTRAAAAA  ************************************************************************************* ");
		        		System.out.println("sCveNavegacion  *************************************** " + navegacion.sCVE_NAVEGACION);
		        		System.out.println("navegacionTemplate.sTipoTemplate  ********************* " + navegacionTemplate.sTipoTemplate);
			        	//System.out.println("navegacionTemplate.sTipoTemplate  ********************* " + navegacionTemplate.sTipoTemplate);
			        	//System.out.println("permisosMaestro.bBAlta  ******************************* " + permisosMaestro.bBAlta);
		        	}
		        	
		        	boolean bVerificaEjecutable = false;
		        	while(listaEtiquetas.hasNext()){
		        		remplazo = listaEtiquetas.next();
		        		
		        		remplazo.sProceso = remplazo.sProceso.replace("{ID_EMPRESA}", 	String.valueOf(iIdEmpresa)); 
		        		remplazo.sProceso = remplazo.sProceso.replace("{PANTALLA}",  	String.valueOf(pantalla.iIdPantalla) );
		        		remplazo.sProceso = remplazo.sProceso.replace("{CVE_USUARIO}", 	"'" + usuario.sCveUsuario + "'");
		        		remplazo.sProceso = remplazo.sProceso.replace("{ID_ROL}", 		String.valueOf(usuario.iIdRol));
		        		remplazo.sProceso = remplazo.sProceso.replace("{ID_TABLA}", 	String.valueOf(iIdTabla));
		        		remplazo.sProceso = remplazo.sProceso.replace("{BROWSER_TYPE}", "'" + browserType.getName() + "'");
		        		remplazo.sProceso = remplazo.sProceso.replace("{ID_LOG_ERROR}", request.getParameter("ID_LOG_ERROR_ALTA_ENTIDAD") == null ? "0" : request.getParameter("ID_LOG_ERROR_ALTA_ENTIDAD"));
		        		remplazo.sProceso = remplazo.sProceso.replace("{ES_POPUP_WIN}", "'" + sEsPopUpWindow + "'");
		        		
		        		if(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_EJECUTA") && !bVerificaEjecutable && navegacionTemplate.sTipoTemplate.equals("EXECUTABLE_FORM")){
		        			bVerificaEjecutable = true;
		        			ejecutable = servicio.getEjecutable(iIdEmpresa, Integer.parseInt(request.getParameter("ID_EJECUTABLE")), lbDebug);
		        			
		        			// Cuando viene el par�metro de Nom_Entidad se agrega a la sentencia insert statement
		        			if(request.getParameter("NOM_ENTIDAD") != null && !request.getParameter("NOM_ENTIDAD").equals("")){
		        				sInsertStmt = sInsertStmt + "UNION ALL SELECT 'NOM_ENTIDAD'," + iIdTabla + ",1,'CONTROL','" + 
		        						request.getParameter("NOM_ENTIDAD")	+ "',NULL,NULL FROM DUAL --14\n";
		        			}
		        			
			        		if(request.getParameter("ID_EJECUTABLE") != null){
			        			remplazo.sProceso = remplazo.sProceso.replace("{ID_EJECUCION}", request.getParameter("ID_EJECUTABLE"));
			        			
			        			if(request.getParameter("ID_EJECUTABLE").equals("-121")){
			        				sInsertStmt = sInsertStmt + " UNION ALL SELECT 'CONSTRAINT_REFERENCIA'," + iIdTabla + ",1,'EXEC','" +
			        				(request.getParameter("CONSTRAINT_REFERENCIA")==null?"":request.getParameter("CONSTRAINT_REFERENCIA")) + "',NULL,NULL FROM DUAL --15\n" +
			        				 "UNION ALL SELECT 'ENTIDAD_ORIGEN'," + iIdTabla + ",1,'EXEC','" +
					        			(request.getParameter("ENTIDAD_ORIGEN")==null?"":request.getParameter("ENTIDAD_ORIGEN")) + "',NULL,NULL FROM DUAL --16\n" +
			        				 "UNION ALL SELECT 'ENTIDAD_DESTINO'," + iIdTabla + ",1,'EXEC','" +
						        			(request.getParameter("ENTIDAD_DESTINO")==null?"":request.getParameter("ENTIDAD_DESTINO")) + "',NULL, NULL FROM DUAL --17\n";
			        			}
	
			        			if(ejecutable.sFuncJsAgregaAtributo!=null && 
			        					((request.getParameter("NOM_ENTIDAD") != null && !request.getParameter("NOM_ENTIDAD").equals(""))) 
			        					||(request.getParameter("ID_EJECUTABLE").equals("-101") || request.getParameter("ID_EJECUTABLE").equals("-102") )
			        				){
	
			        				pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON AGREGAR ATRIBUTO-->", 
			        						buttons.sButtonAddAttribute.replace("@FUNCTION_JS_ADD_ATTRIBUTE", ejecutable.sFuncJsAgregaAtributo));
	
	/*			        					"<input type=\"button\" id=\"ButtonAgrega\" name=\"ButtonAgrega\" class=\"roundedRight\" value=\"Agregar Atributo\" onClick=\"javascript:"+ ejecutable.sFuncJsAgregaAtributo + "\"> \n " +
							" <script type=\"text/javascript\"> $(function() { $(\"form.jqtransform\").jqTransform(); $(\"#ButtonAgrega\").click("+ ejecutable.sFuncJsAgregaAtributo + "); }); </script> \n");
							//" <script type=\"text/javascript\"> $(\"#ButtonAgrega\").click("+ ejecutable.sFuncJsAgregaAtributo + "); </script> \n");
	
			        				
			        			if(request.getParameter("ID_EJECUTABLE").equals("101"))
			        				pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON AGREGAR ATRIBUTO-->", 
			        					"<input type=\"button\" class=\"roundedRight\" value=\"Agregar Atributo\" onClick=\"javascript:cloneRow();\">");
			        			
			        			if((request.getParameter("ID_EJECUTABLE").equals("145") || request.getParameter("ID_EJECUTABLE").equals("149")) && ( 
			        					(request.getParameter("NOM_ENTIDAD") != null && !request.getParameter("NOM_ENTIDAD").equals(""))    
			        					|| request.getParameter("ID_LOG_ERROR_ALTA_ENTIDAD") != null)){
			        				
			        				if(request.getParameter("ID_EJECUTABLE").equals("145"))
			        					pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON AGREGAR ATRIBUTO-->", 
			        					"<input type=\"button\" class=\"roundedRight\" value=\"Agregar Atributo\" onClick=\"javascript:cloneRowBorraRestric();\">");
			        				
			        				if(request.getParameter("ID_EJECUTABLE").equals("149"))
				        				pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON AGREGAR ATRIBUTO-->", 
				        					"<input type=\"button\" class=\"roundedRight\" value=\"Agregar Atributo\" onClick=\"javascript:cloneRowAltaChk();\">");
			        			}
	*/
			        			}
			        		}
		        		}
		        		
		        		if(lbDebug){
		        			date = new Date();
		        	        System.out.println("fecha de sistema: " + date.toString());
		        			System.out.println("remplazo.sProceso : \n DECLARE \n  V CLOB;\nBEGIN \n    V:=  " + remplazo.sProceso + "; \n "
		        					+ "    DBMS_OUTPUT.PUT_LINE(V);  \n END; ");
		        		}
	
		        		if(remplazo.sBNecesitaDatos.equals("V") && lbDebug){
		        			System.out.println(" SE NECESITAN DATOS: \n INSERT INTO CONTROL.AOS_ARQ_PARAM_REQ\n" + sInsertStmt 
		        					//+ " \n " + sInsertStmt.replace("'", "''")
		        					);
		        		}
		        		
		        		if(iIdClob<=0 && remplazo.sBNecesitaDatos.equals("V")){
		        			iIdClob = servicio.iGuardaCLOB(iIdEmpresa, 0, pantalla.iIdPantalla, usuario.sCveUsuario, sInsertStmt, "Servlet get 2 remplazo", lbDebug);
		        		}
	
		        		if(remplazo.sBEsTexto.equals("F") && remplazo.sEsJavaScript.equals("F")){
			        		sRemplazo = (servicio.sGetRemplazoEtiqueta(iIdEmpresa, remplazo.sProceso,iIdClob, usuario.sCveUsuario, lbDebug));
		        		}
		        		else if (remplazo.sEsJavaScript.equals("F")){
		        				sRemplazo = remplazo.sProceso;
			        		}else{
			        			sRemplazo = null;
			        			sJavaScriptCodeTmp = (servicio.sGetRemplazoEtiqueta(iIdEmpresa, remplazo.sProceso, iIdClob, usuario.sCveUsuario, lbDebug));
			        			if(sJavaScriptCodeTmp!=null&&!sJavaScriptCodeTmp.equals("null")){
			        				sJavaScriptCode = sJavaScriptCode + sJavaScriptCodeTmp;
			        			}
			        	}
	
		        		if(sRemplazo != null && sRemplazo.matches("SYS_DYNAPP_ERROR:(.*)")){
		        			sErrorHtml = sRemplazo.replace("SYS_DYNAPP_ERROR:", "");
		        			request.setAttribute("MSG_ERROR", sErrorHtml);
		    		        response.setContentType("text/html");
		    				request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
		                	return;
		        		}
		        		
		        		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace(remplazo.sEtiqueta, (sRemplazo == null ? "" : sRemplazo));
		        		
		        		if(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_GET_REG")){
	
		        			if(pantalla.bBBaja){
		        				pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON BORRAR-->",buttons.sButtonDeleteRecord);
	
		        			}
		        			if((pantalla.bBModifica && pantalla.bBButtonUpdate)|| pantalla.iIdPantalla==186){
		        				pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON MODIFICAR-->", buttons.sButtonUpdateRecord);
		        			}
	    				}
		        		if(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_ALTA")){
	
		        			if(pantalla.bBAlta && pantalla.bBButtonCreate){
		        				pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON MODIFICAR-->", 
		        						//"<input class=\"roundedButton\" name=\"PARAM_CTL_ALTA\" value=\"Aceptar\" type=\"button\" onClick=\"submitForm('A');\">");
		        						buttons.sButtonCreateRecord);
		        			}
		        		}
		        	}
		        	
	        	
	        	
    		}
	        	
	        	
    		}
        }

        pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("@PANTALLA", String.valueOf(pantalla.iIdPantalla));
        pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("@ID_TABLA", String.valueOf(iIdTabla));
        pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("@ID_EMPRESA", String.valueOf(iIdEmpresa));

        
/********************************************************************          EJECUTABLES       *************************************************************************/
        
        if(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_EJECUTA")){
        	
        	pantalla.sHtmlPantalla 	= pantalla.sHtmlPantalla.replace("@ID_EJECUTABLE", request.getParameter("ID_EJECUTABLE"));
        	String sEntidadOrigen	= "";
        	String sAtributosLost  	= "";
        	ejecutable				= servicio.getEjecutable(iIdEmpresa, Integer.parseInt(request.getParameter("ID_EJECUTABLE")), lbDebug);
        	if(request.getParameter("NOM_ENTIDAD")!=null){
        		sEntidadOrigen	= "SELECT 'NOM_ENTIDAD',4,1,'CONTROL','" + 
        				request.getParameter("NOM_ENTIDAD") + "'NULL,NULL FROM DUAL\n";
        	}
        	 
        	if(request.getParameter("NOM_OBJETO") != null)
        		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("name=\"NOM_OBJETO\" value=\"\"",
					"name=\"NOM_OBJETO\" value=\"" + request.getParameter("NOM_OBJETO") + "\" ");
 
        	// Alta de constraint de referencia de entidad
    		if(request.getParameter("ID_EJECUTABLE").equals("-121")){

            	sEntidadOrigen	= "SELECT 'CONSTRAINT_REFERENCIA',4,1,'EXEC','" + 
            		(request.getParameter("CONSTRAINT_REFERENCIA") == null ? "" : request.getParameter("CONSTRAINT_REFERENCIA")) + 
            		"',NULL,NULL FROM DUAL\n" +
            	"UNION ALL SELECT 'ENTIDAD_ORIGEN',4,1,'EXEC','" + 
            		(request.getParameter("ENTIDAD_ORIGEN") == null ? "" : request.getParameter("ENTIDAD_ORIGEN")) + 
            		"',NULL,NULL FROM DUAL\n";
    			
    			//pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--OPCIONES_COMBO_DESTINO-->", servicio.sDameOpcionesCombo(iIdEmpresa, -29, request.getParameter("ENTIDAD_DESTINO"), "", "F", usuario.sCveUsuario, lbDebug));
            	//pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--OPCIONES-->", servicio.sDameOpcionesCombo(iIdEmpresa, -29, request.getParameter("NOM_ENTIDAD"), "", "F", usuario.sCveUsuario, lbDebug));
    			
    			if(request.getParameter("ENTIDAD_ORIGEN") != null && !request.getParameter("ENTIDAD_ORIGEN").equals("")
    					&& request.getParameter("ENTIDAD_DESTINO") != null && !request.getParameter("ENTIDAD_DESTINO").equals("")){

    				if(lbDebug){
    					System.out.println("iIdEmpresa      " + iIdEmpresa);
    					System.out.println("ENTIDAD_ORIGEN  " + request.getParameter("ENTIDAD_ORIGEN"));
		        		System.out.println("ENTIDAD_DESTINO " + request.getParameter("ENTIDAD_DESTINO"));
		        	}
    				
    				String sAtributosFK = servicio.sDameAtributosFK(iIdEmpresa, request.getParameter("ENTIDAD_DESTINO"), usuario.sCveUsuario, lbDebug);
    				pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--CTL_CAMPOS_TABLA-->", sAtributosFK == null ? "" : sAtributosFK);

    				if(sAtributosFK!=null){
        				String sAtributosOrigen = servicio.sDameOpcionesCombo(iIdEmpresa, -30, "", sEntidadOrigen, "V", usuario.sCveUsuario, lbDebug);
        				pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--OPCIONES-->",
        						sAtributosOrigen == null ? "" : sAtributosOrigen);
    				}
    			}
    		}else{
    			
    			boolean bRemplazaCamposTabla = false;
	        	if(ejecutable.sBObtieneElementos.equals("V")){ 
	        			
	        		if(ejecutable.sBAtribDependEntidad.equals("V")){
	        			if (request.getParameter("NOM_ENTIDAD") != null && !request.getParameter("NOM_ENTIDAD").equals("")){
		        			if(lbDebug){
		    	        		System.out.println("ACTUALIZA EL DATO PARA REMPLAZAR CAMPOS, LA NOM_ENTIDAD NO ES NULO");
		    	        	}
	        				bRemplazaCamposTabla = true;
	        			}
	        		}else{
	        			if(lbDebug){
	    	        		System.out.println("NO SON DEPENDIENTES DE LA ENTIDAD");
	    	        	}
	        			bRemplazaCamposTabla = true;
	        		}
	        		
	        		if(bRemplazaCamposTabla){
		        		sAtributosLost = servicio.sDameAtributosTablaRecupera(ejecutable.iIdEmpresa, ejecutable.iIdEjecutable, 
		        				(request.getParameter("ID_LOG_ERROR_ALTA_ENTIDAD") != null ? Integer.parseInt(request.getParameter("ID_LOG_ERROR_ALTA_ENTIDAD")) : 0), 
		        				sEntidadOrigen, usuario.sCveUsuario, lbDebug);
		        		if(lbDebug){
	    	        		System.out.println("ENTRA A REMPLAZAR CTL_CAMPOS_TABLA");
	    	        		System.out.println("sAtributosLost: \n" + sAtributosLost);
	    	        	}
		        		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--CTL_CAMPOS_TABLA-->", sAtributosLost);
	        		}
	        	}

	        	if(request.getParameter("B_IGNORA_ERROR_YA_EXISTE") != null)
	        		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("name=\"B_IGNORA_ERROR_YA_EXISTE\" >","name=\"B_IGNORA_ERROR_YA_EXISTE\" checked> ");

	        	if(request.getParameter("B_IGNORA_TODO_ERROR") != null)
	        		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("name=\"B_IGNORA_TODO_ERROR\" >","name=\"B_IGNORA_TODO_ERROR\" checked> ");

	        	if(ejecutable.sBRemplazaComboEnt.equals("V") && ejecutable.iListadoRmpzaEntidad != 0){
            		String sOpcionesEntidad = servicio.sDameOpcionesCombo(iIdEmpresa, ejecutable.iListadoRmpzaEntidad, request.getParameter("NOM_ENTIDAD"), "", "F", usuario.sCveUsuario, lbDebug);
            		if(lbDebug){
    	        		System.out.println("NOM_ENTIDAD " + request.getParameter("NOM_ENTIDAD"));
    	        		System.out.println("sOpcionesEntidad \n " + sOpcionesEntidad);
    	        		//System.out.println("pantalla.sHtmlPantalla \n " + pantalla.sHtmlPantalla);
    	        	}
            		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--OPCIONES-->", sOpcionesEntidad);
            	}
        		if(ejecutable.sBRemplazaComboAts.equals("V") && request.getParameter("ID_LOG_ERROR_ALTA_ENTIDAD") == null &&
        				ejecutable.iListadoRmpzaDetalle < 0 && ejecutable.sEtiquetaRmpzaDetalle!=null
        				&& (request.getParameter("NOM_ENTIDAD") != null && !request.getParameter("NOM_ENTIDAD").equals(""))){
	        		String sAtributosOrigen = servicio.sDameOpcionesCombo(iIdEmpresa, ejecutable.iListadoRmpzaDetalle, "", sEntidadOrigen, ejecutable.sBObligatorioComboDet, usuario.sCveUsuario, lbDebug);
        			if(lbDebug){
    	        		System.out.println("Esta entrando a la condici�n ***************************************");
    	        		System.out.println("sAtributosOrigen opciones del combo \n " + sAtributosOrigen);
    	        		System.out.println("sEntidadOrigen \n " + sEntidadOrigen);
    	        		
    	        		//System.out.println("pantalla.sHtmlPantalla \n " + pantalla.sHtmlPantalla);
    	        	}
					pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace(ejecutable.sEtiquetaRmpzaDetalle,
							sAtributosOrigen == null ? "" : sAtributosOrigen);
	        	}
    		}

            if(ejecutable.sBBotonRegresar.equals("V")){
    			pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON REGRESAR-->", buttons.sButtonBack);
            }
            pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("@TIPO_OPERACION","A");
        }


/***************************************************************************************************************************************************************************/
        
        String sSkinToApply = ((sBodySkin==null||sBodySkin.equals("")||sBodySkin.equals("undefined"))?pantalla.sDefaultSkin:sBodySkin);
    	pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<body>", "<body class=\"" + sSkinToApply + "\">");
    	pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("data-skin=\"" + sSkinToApply + "\">", "data-skin=\"" + sSkinToApply + "\" selected>");

        if(sEsPopUpWindow.equals("SI")){
        	pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--PARAM_CTL_ES_POPUP_WINDOW-->", 
        			"<input name=\"PARAM_CTL_ES_POPUP_WINDOW\" value=\"SI\" type=\"hidden\"> ");
        	pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--PARAM_CTL_REGRESA_VAL_NAME-->", 
        			"<input name=\"PARAM_CTL_REGRESA_VAL_NAME\" value=\"" + 
        					(request.getParameter("PARAM_CTL_REGRESA_VAL_NAME") == null ? "" : request.getParameter("PARAM_CTL_REGRESA_VAL_NAME")) + 
        					"\" type=\"hidden\"> ");
        }else{
	        if(pantalla.sNotifications!=null){
	        	pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--MESSAGE_REMINDER-->", pantalla.sNotifications);
	        }
        }
        
        if(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_ALTA")){
        	if(pantalla.bBButtonBackCreate){
        		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON REGRESAR-->", buttons.sButtonBack);
        	}
			pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("@TIPO_OPERACION","A");
        }
        
        if(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_GET_REG")){
			pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("@TIPO_OPERACION","M");
			if(pantalla.bBButtonBackUpdate){
				pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON REGRESAR-->", buttons.sButtonBack);
			}
        }
        
        if(lbDebug){
			System.out.println("Antes de remplazar sJsTableTransform -------------------->>>> \n" + navegacion.sCVE_NAVEGACION + "\n iIdTabla: " + 
					iIdTabla);
		}
        
        if((navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_MENU") || navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_BUSQUEDA") ||
        		 navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_GET_REG")) && iIdTabla > 0){
        	if(lbDebug){
				System.out.println("entra a remplazar sJsTableTransform-------------------->>>> \n" + sJsTableTransform);
			}
        	pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("//TABLES_RS", (sJsTableTransform==null?"":sJsTableTransform));
        	if(pantalla.bBButtonSearch){
        		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BUTTON_SEARCH-->", buttons.sButtonSearch);
        	}
        	if((navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_MENU") || navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_BUSQUEDA")) &&
           		pantalla.bBButtonClear){
        		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BUTTON_CLEAR-->", (buttons.sButtonClear==null?" ":buttons.sButtonClear));
        	}
        }
        
        
        if(lbDebug){
			System.out.println("MENU O BUSQUEDA 1  navegacion.sCVE_NAVEGACION ------>>>>" + navegacion.sCVE_NAVEGACION + 
					" pantalla.bBAlta: " + pantalla.bBAlta);
		}
        
        if((navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_MENU") || navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_BUSQUEDA")) && iIdTabla > 0 && pantalla.bBAlta
        		&& !sEsPopUpWindow.equals("SI")){
        	String sButtonId = buttons.sButtonGoToCreateRecordFromSearch.replace("@ID_TABLA@",  String.valueOf(iIdTabla));
        	
        	if(lbDebug){
				System.out.println("MENU O BUSQUEDA ------>>>> \n" + sButtonId + " bBButtonCreateSearch: " + pantalla.bBButtonCreateSearch);
			}
        	
        	if(pantalla.bBButtonCreateSearch){
				pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON ALTA DESDE BUSQUEDA-->", sButtonId);
        	}
        	
        	if(pantalla.bBButtonCreateFromResult){
        		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--BOTON ALTA-->", buttons.sButtonGoToCreateRecord);
        	}

		}

        String sTipoForm = ((navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_MENU") || navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_BUSQUEDA"))
    			?"SEARCH":(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_ALTA") ? "CREATE": (navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_GET_REG")? "UPDATE": 
    				(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_EJECUTA") ? "EXEC": ""))));

        String sJsFuncionesFiltro = "";
        //if(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_GET_REG") && iIdTabla > 0){
        	sJsFuncionesFiltro = servicio.sDameJsComboDependFiltro(iIdEmpresa, pantalla.iIdPantalla, iIdTabla, 
        			(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_GET_REG") || navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_ALTA")) ? 
        			"ALT_MOD" : "FILTRO" 
        			
        			, usuario.sCveUsuario, lbDebug);
        	if(lbDebug){
        		System.out.println("OBTIENE LAS FUNCIONES JAVASCRIPT \n" + sJsFuncionesFiltro);
        	}
        //}

    	if(iIdClob>0){
    		// Elimina el registro de clob utilizado para enviar par�metros
    		servicio.borraCLOB(iIdClob, lbDebug);
    	}

        String sValidationForm = servicio.sGetValidationForm(iIdEmpresa, pantalla.iIdPantalla, iIdTabla, sTipoForm, pantalla.sCveMasterTemplate, 
        		(navegacion.sCVE_NAVEGACION.equals("NAVEGA_CAT_CTL_EJECUTA") ? ejecutable.iIdEjecutable : 0 ),
        		usuario.sCveUsuario, lbDebug);
		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("//ACE_VALIDATION_FORM", "//ACE_VALIDATION_FORM \n" + sValidationForm);
		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("// js code here", sJavaScriptCode + "\n" + 
				((sJsFuncionesFiltro!=null&&!sJsFuncionesFiltro.equals("null"))?sJsFuncionesFiltro:"") + 
				((request.getParameter("ID_EJECUTABLE")!=null&&request.getParameter("ID_EJECUTABLE").equals("-121"))? ("\n" + "setOptions();"):"")
				);
		//pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--MENU-->", usuario.sMenu);
		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("@ID_EMPRESA", Integer.toString(usuario.iIdEmpresa));
		pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("@PARAM_CTL_ID_MENU", request.getParameter("PARAM_CTL_ID_MENU")==null?"menu_home":
			request.getParameter("PARAM_CTL_ID_MENU"));
		
		if(iIdTabla>0){
			pantalla.sPageContentHeader = pantalla.sPageContentHeader.replace("@SUBTITULO_PANTALLA",
					(request.getParameter("ID_EJECUTABLE")==null?(pantalla.sSubtituloPantalla!=null?pantalla.sSubtituloPantalla:"") : 
						ejecutable!=null?ejecutable.sTituloExecutable:(pantalla.sSubtituloPantalla!=null?pantalla.sSubtituloPantalla:"")));
			pantalla.sPageContentHeader = pantalla.sPageContentHeader.replace("@TITULO_PANTALLA", (pantalla.sTituloPantalla!=null?pantalla.sTituloPantalla:""));
	
			pantalla.sHtmlPantalla = pantalla.sHtmlPantalla.replace("<!--PAGE_HEADER-->", pantalla.sPageContentHeader);
		}
		
		if(lbDebug){
			date = new Date();
			System.out.println("fecha de sistema final get: " + date.toString());
		}
		
		response.setContentType("text/html");
		request.setAttribute("HTML_PAGE" + (sEsPopUpWindow.equals("SI") ? "_POPUPWIN" : ""), pantalla.sHtmlPantalla);
		request.getRequestDispatcher("/PageDisplay" + (sEsPopUpWindow.equals("SI")? "PopUpWin":"") + iPageNumber + ".jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        // LinkedLists
        LinkedList<Atributo> listaAtributos 	= null;
        LinkedList<Atributo> listaParametros	= null;

        // Iterators
        Iterator<Atributo> listaAtrib			= null;
        Iterator<Atributo> listaParam			= null;
        
        // Objetos
        NavegacionNext navegacionNext 			= null;
        Navegacion navegacion  					= null;
        Pantalla pantalla 						= null;
        Atributo atributo 						= null;
        Usuario usuario							= null;
        Buttons buttons 						= null;
        Ejecutable ejecutable 					= null;
        BrowserType browserType					= new BrowserType(request.getHeader("user-agent"));
 
        // Variables
        String sInsertStmt						= "";
        String sResultAltMod					= null;
        String sTmpCveNavegacion				= "";
        String sCveNavegacion					= request.getParameter("PARAM_CTL_CVE_NAVEGACION");
        String sTipoOperacion					= null;
        String sPlantilla 						= "";
        String sPlantillaPopUpWin				= "";

        int iIdEmpresa;
        int iIdTabla 							= Integer.parseInt(request.getParameter("PARAM_CTL_ID_TABLA"));
        int iIdClob 							= 0;
        int iPantallaTmp 						= Integer.parseInt(request.getParameter("PARAM_CTL_PANTALLA"));
        int iPageNumber 						= 0;
        int iNumRegistrosAltMod					= 0;
        
        boolean bIsChangePswd 					= false;
        boolean lbDebug							= false;
        Date date 								= null;

        // Se inicializa el sevicio de conexi�n
        Servicio servicio 						= new Servicio();

        //Obtain the session object
        HttpSession session = request.getSession(false);
        
        /*if(session == null){
        	System.out.println(" La sesi�n es nula en POST");
        	response.sendRedirect(request.getContextPath() + "/index.jsp");
        	return;
        }*/
        
        /*******************         CONTROL DE SESI�N DEL USUARIO        *****************************/
        
        //Check if our session variable is set, if so, get the session variable value
        //which is an Integer object, and add one to the value.
        //If the value is not set, create an Integer object with the default value 1.
        //Add the variable to the session overwriting any possible present values.
        if(session != null){
        	usuario 			= (Usuario) session.getAttribute("Usuario");
        	sPlantilla 			= (String)  session.getAttribute("Plantilla");
        	sPlantillaPopUpWin	= (String)  session.getAttribute("PlantillaPopUpWin");
        	buttons   			= (Buttons) session.getAttribute("Buttons");
        	
        	if(usuario != null){
        		
    	        // Se obtinenen los par�metros necesarios para ejecutar la respuesta  
    	        navegacion  = servicio.getNavegacion(sCveNavegacion, lbDebug);
    	        iIdEmpresa 	= usuario.iIdEmpresa;
    	        lbDebug 	= servicio.bDebug(iIdEmpresa, usuario.sCveUsuario);

    	        if(lbDebug){
    	        	System.out.println(" El usuario no es nulo -  Antes del control de sesi�n ");
    	        }
        	}
        }


        // Si no existe el usuario se redirecciona a la p�gina de login
        if (usuario == null) {
        	//System.out.println(" El usuario es nulo **** ");
        	// Se verifica que hayan enviado los par�metros para realizar el login
        	if(request.getParameter("login") == null || request.getParameter("password") == null){
        		//System.out.println(" No llegan los datos de login **** ");
        		response.sendRedirect(request.getContextPath() + "/index.jsp");
        		return;
        	}else{

        		// Se genera la sesi�n
	        	session = request.getSession(true);
	        	//System.out.println(" Se genera la sesi�n **** ");

	        	// Se verifica que la interface est� compilada
	        	//System.out.println(" servicio.CompilaInterface 3 **** " + servicio.sCompilaInterface(false));        
	        	// Se obtiene el usuario y se valida el password
	        	usuario = servicio.getUsuario(request.getParameter("login"), request.getParameter("password"), null, false);

	        	if(usuario != null && usuario.sCveUsuario.equals("ERROR_EN_SYSTEMA")){
	        		if(lbDebug){
	        			System.out.println(" Error al obtener el usuario ");
	        		}
        			request.setAttribute("MSG_ERROR", "El servicio no est� disponible, por favor comun�quese con el administrador");
    		        response.setContentType("text/html");
    				request.getRequestDispatcher("index.jsp").forward(request,response);
                	return;
	        	}

	        	//System.out.println(" Se obtiene el usuario  **** ");

	        	// Verifica si el usuario captur� el password correcto
	        	if(usuario != null && usuario.sPasswordValido.equals("V")){

        			if(usuario.sSettings.equals("V")){
            			request.setAttribute("MSG_ERROR", "Por favor actualice su versi�n del sistema");
        		        response.setContentType("text/html");
        				request.getRequestDispatcher("index.jsp").forward(request,response);
                    	return;
        			}

        	        // Se obtinenen los par�metros necesarios para ejecutar la respuesta  
        	        navegacion    	= servicio.getNavegacion(sCveNavegacion, lbDebug);
        	        iIdEmpresa 		= usuario.iIdEmpresa;
        	        lbDebug 		= servicio.bDebug(iIdEmpresa, usuario.sCveUsuario);
        	        
        			session.setAttribute("Usuario", usuario);
        			if(lbDebug){
        				System.out.println(" Password correcto, se agrega el usuario a la sesi�n  **** ");
        			}
        			// Se obtiene la plantilla de las pantallas y se agrega a la sesi�n
        			sPlantilla = servicio.sGetPlantilla(usuario.iIdEmpresa, usuario.sCveUsuario, "F", lbDebug);
        			if(lbDebug){
        				System.out.println(" Obtiene la plantilla ****  " + sPlantilla!=null?" Y NO ES NULA" : "PERO ES NULA");
        			}
        			session.setAttribute("Plantilla", sPlantilla);
        			if(lbDebug){
        				System.out.println(" SE AGREGA A LA SESION ");
        			}
        			sPlantillaPopUpWin = servicio.sGetPlantilla(usuario.iIdEmpresa, usuario.sCveUsuario, "V", lbDebug);
        			session.setAttribute("PlantillaPopUpWin", sPlantillaPopUpWin);
        			if(lbDebug){
        				System.out.println(" SE AGREGA A LA SESION LA POPUPWINDOW");
        			}
        			
        			buttons		= servicio.getButtons(usuario.iIdEmpresa, usuario.sCveUsuario, lbDebug);
        			session.setAttribute("Buttons", buttons);
        			if(lbDebug){
        				System.out.println(" SE AGREGA A LA SESION LA LOS BOTONES ");
        			}
        			
        			/*
        			map = servicio.getSettings(usuario.sCveMasterTemplate, usuario.sCveMasterTemplateVersion, lbDebug); 
        			session.setAttribute("Settings", map);
        			
        			if(lbDebug){
        				System.out.println(" SE AGREGA A LA SESION LOS SETTINGS " + map.size());
        			}
        			*/
        			// set another default locale
        		    Locale.setDefault(new Locale("en", "US"));
        		    
        			// Verifica si el usuario debe de cambiar su password por que ha expirado
	        		if(usuario.bCambioPassword){
	        			iPantallaTmp = 186;
	        			iIdTabla	 = 172;
	        			sCveNavegacion = "NAVEGA_CAT_CTL_GET_REG";
//	        			sEsPopUpWindow = "SI";
	        			if(lbDebug){
	        				System.out.println(" Password correcto, se manda al usuario a cambio de password  **** ");
	        			}
	        			//response.sendRedirect(request.getContextPath() + "/ChangePassword.jsp?CVE_USUARIO=" + request.getParameter("login"));
		        		//return;
	        			request.setAttribute("ID_PANTALLA", iPantallaTmp);
	        			request.setAttribute("CVE_NAVEGACION", sCveNavegacion);
	        			request.setAttribute("ID_TABLA", 172);
	        			request.setAttribute("ID_TABLA_ANTERIOR", 0);
	        			request.setAttribute("FROM_LOGIN", "YES");
	            		doGet(request, response); 
	            		return;
	        		} 
	        	}
	        	else{
	        		if(usuario != null && usuario.sPasswordValido.equals("F")){
	        			if(usuario.sSettings.equals("V")){
	            			request.setAttribute("MSG_ERROR", "Por favor actualice su versi�n del sistema");
	        		        response.setContentType("text/html");
	        				request.getRequestDispatcher("index.jsp").forward(request,response);
	                    	return;
	        			}
		        		// Si el password es incorrecto se redirecciona a la pantalla de login
            			request.setAttribute("MSG_ERROR", "Password Incorrecto");
        		        response.setContentType("text/html");
        				request.getRequestDispatcher("index.jsp").forward(request,response);
                    	return;
	        		}else{
	        			if(usuario==null){	
	            			request.setAttribute("MSG_ERROR", "El usuario no existe");
	        		        response.setContentType("text/html");
	        				request.getRequestDispatcher("index.jsp").forward(request,response);
	                    	return;
	        			}
	        		}
	        	}
        	}
        } 
        
		if(usuario.sSettings.equals("V")){
			request.setAttribute("MSG_ERROR", "Por favor actualice su versi�n del sistema");
	        response.setContentType("text/html");
			request.getRequestDispatcher("index.jsp").forward(request,response);
        	return;
		}
		
		iIdEmpresa 		= usuario.iIdEmpresa;
		
        /*******************  CALCULATING RESPONSE PAGE       *****************************/
		
		// THE PAGE NUMBER IS CALCULATED
		iPageNumber = (Integer)session.getAttribute("pageNumber")==null?1:(Integer)session.getAttribute("pageNumber");
		iPageNumber = (iPageNumber + 1)>19?1:(iPageNumber + 1);
		session.setAttribute("pageNumber",iPageNumber);//*/

       /**************************************************    PROCESO DE IDA HACIA EL SERVIDOR Y BASE DE DATOS    *************************************************/

        if(lbDebug){

            date = new Date();
            System.out.println("fecha de sistema: " + date.toString());
	        System.out.println("***********************************************************************************************************: ");
	        System.out.println("Usuario: " + usuario.sCveUsuario + " ejecuta POST " );
	        System.out.println("BROWSER: " + browserType.getName());
	        System.out.println("VERSION: " + browserType.getVersion());
	        System.out.println("PARAM_CTL_CVE_NAVEGACION: " + request.getParameter("PARAM_CTL_CVE_NAVEGACION"));
	        System.out.println("PARAM_CTL_PANTALLA: " + request.getParameter("PARAM_CTL_PANTALLA"));
	        System.out.println("PARAM_CTL_ID_EMPRESA: " + request.getParameter("PARAM_CTL_ID_EMPRESA"));
	        System.out.println("PARAM_CTL_ID_TABLA: " + request.getParameter("PARAM_CTL_ID_TABLA"));
	        System.out.println("request.getContextPath(): " + request.getContextPath());
	        System.out.println("valor del select :  " + request.getParameter("OPERADOR"));
	        System.out.println("valor del select2 :  " + request.getAttribute("data-test"));
	        System.out.println("valor del DELETE :  " + request.getParameter("DELETE"));
	        System.out.println("valor del UPDATE :  " + request.getParameter("UPDATE"));
        }

        /*******************      SE OBTINE LA NAVEGACI�N, LA PANTALLA Y LOS PERMISOS DE LA TABLA EN PROCESO     *****************************/

        // Se inicia la generaci�n del String que contiene los par�metros necesarios para realizar la operaci�n 
		sInsertStmt	= "SELECT 'ID_EMPRESA'," + iIdTabla + ",1,'CONTROL','" + iIdEmpresa + "',NULL,NULL FROM DUAL --18\n"
        		+ "UNION ALL SELECT 'CVE_USUARIO'," + iIdTabla + ",1,'CONTROL','" + usuario.sCveUsuario + "',NULL,NULL FROM DUAL --18\n";

		
		if(lbDebug){
	        date = new Date();
	        System.out.println("fecha de sistema: " + date.toString());
			System.out.println(" usuario.bCambioPassword:  " + usuario.bCambioPassword);
			System.out.println(" CTLNEW_PASSWORD:  >" + request.getParameter("CTLNEW_PASSWORD") + "<");
		}
		
		if(!usuario.bCambioPassword || request.getParameter("CTLNEW_PASSWORD")!=null){
			
		
		// Se obtiene la pantalla que se est� procesando
        pantalla = servicio.getPantalla(iIdEmpresa, iPantallaTmp, iIdTabla, usuario.iIdRol, navegacion.sCVE_OPERACION, usuario.sCveUsuario, lbDebug);

        if(pantalla==null || pantalla.sSitPantalla.equals("IN")){
        	// Si la pantalla est� inactiva se redirecciona a la pantalla de error
			request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(23, usuario.sCveUsuario, lbDebug));
	        response.setContentType("text/html");
			request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
        	return;
        }

		sTipoOperacion = request.getParameter("PARAM_CTL_OPERACION") ;
		
		if(sTipoOperacion==null){
			request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(22, usuario.sCveUsuario, lbDebug));
	        response.setContentType("text/html");
			request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
        	return;
		}

		if(sTipoOperacion.equals("B") && request.getParameter("UPDATE")!=null){
			sTipoOperacion = "M";
		}

		if(sTipoOperacion.equals("M")){
			if(!pantalla.bBModifica&&pantalla.iIdPantalla!=186){
				request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(21, usuario.sCveUsuario, lbDebug));
		        response.setContentType("text/html");
				request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
	        	return;

        	}
    	}else{ 
			if(sTipoOperacion.equals("B")){
	        	if(!pantalla.bBBaja){
	    			request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(21, usuario.sCveUsuario, lbDebug));
	    	        response.setContentType("text/html");
	    			request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
	            	return;

	        	}
	        }else{  
	        	if(sTipoOperacion.equals("A")){
		        	if(!pantalla.bBAlta){
		    			request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(21, usuario.sCveUsuario, lbDebug));
		    	        response.setContentType("text/html");
		    			request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
		            	return;

		        	}
	        	}
        	}
        }

		if(lbDebug){
			System.out.println("  TIPO DE OPERACION  ----->>>>>>>>>>>>>>>>>>>  " + sTipoOperacion);
		}


		if(navegacion.sB_OBTIENE_CAMPOS_EXEC.equals("V")){

    		// Se obtienen los atributos del ejecutable 
			listaAtributos = servicio.getListaParamExec(iIdEmpresa, Integer.parseInt(request.getParameter("ID_EJECUTABLE")), lbDebug);
			String sAtributosEntidad = "";
			sInsertStmt = sInsertStmt + "UNION ALL SELECT 'ID_EJECUTABLE'," + iIdTabla + ",1,'CONTROL','" + request.getParameter("ID_EJECUTABLE") + "',NULL,NULL FROM DUAL --19\n";
			ejecutable = servicio.getEjecutable(iIdEmpresa, Integer.parseInt(request.getParameter("ID_EJECUTABLE")), lbDebug);
			
	        if (listaAtributos != null){
	        	
	        	if(lbDebug){
		        	System.out.println(" LA LISTA DE PAR�METROS getListaParamExec TIENE REGISTROS  ----->>>>>>>>>>>>>>>>>>>  ");
		        	System.out.println(" listaAtributos.size()  ----->  " + listaAtributos.size());
	        	}
	        	listaAtrib = (Iterator<Atributo>)listaAtributos.iterator();
	    		while(listaAtrib.hasNext()){
	    			atributo = listaAtrib.next();
    				
    					if(request.getParameter("ID_EJECUTABLE").equals("-121") && atributo.sNomAtributo.equals("CONSTRAINT_REFERENCIA")){
    						
			    			sInsertStmt = sInsertStmt + "UNION ALL SELECT 'NOM_ENTIDAD'," + iIdTabla + ",1,'CONTROL','" +
				        			 request.getParameter("NOM_ENTIDAD") + "',NULL,NULL FROM DUAL --20\n" + 
				    					"UNION ALL SELECT 'ENTIDAD_DESTINO'," + iIdTabla + ",1,'CONTROL','" +
					        			 request.getParameter("ENTIDAD_DESTINO") + "',NULL,NULL FROM DUAL --20\n" +
			    					"UNION ALL SELECT 'CONSTRAINT_REFERENCIA'," + iIdTabla + ",1,'EXEC','" +
				        			 	request.getParameter("CONSTRAINT_REFERENCIA") + "',NULL,NULL FROM DUAL --20\n";
			    			
			    			String[] sColumnaOrigen		= request.getParameterValues("ATRIBUTO_ORIGEN");
			    			String[] sColumnaDestino	= request.getParameterValues("ATRIBUTO_DESTINO");
		    				if(lbDebug){
		    					System.out.println(" sColumnaOrigen.length: " + sColumnaOrigen.length);
		    					System.out.println(" sColumnaDestino.length: " + sColumnaDestino.length);
		    				}
			    			
			    			for (int y = 0; y < request.getParameterValues("ATRIBUTO_ORIGEN").length; y++) {
			    				if(lbDebug){
			    					System.out.println(" sColumnaOrigen[" + y + "] -----> " + sColumnaOrigen[y]==null?"NULO":sColumnaOrigen[y]);
			    					System.out.println(" sColumnaDestino[" + y + "] -----> " + (sColumnaDestino==null?"NULO":sColumnaDestino[y]==null?"DETNULL":sColumnaDestino[y]));
			    				}
		    					sInsertStmt = sInsertStmt + "UNION ALL SELECT 'ATRIBUTO_ORIGEN'," + iIdTabla + "," + (y + 1 ) + ",'EXEC','" + sColumnaOrigen[y] + "','" + 
		    							(request.getParameter("CONSTRAINT_REFERENCIA")==null?"":request.getParameter("CONSTRAINT_REFERENCIA")) + "',NULL FROM DUAL --21\n";
		    					sInsertStmt = sInsertStmt + "UNION ALL SELECT 'ATRIBUTO_DESTINO'" + iIdTabla + "," + (y + 1 ) + ",'EXEC','" + 
		    						(sColumnaDestino==null?"NULL":sColumnaDestino[y]==null?"DETNULL":sColumnaDestino[y]) + "','" + 
		    							(request.getParameter("CONSTRAINT_REFERENCIA")==null?"":request.getParameter("CONSTRAINT_REFERENCIA")) + "',NULL FROM DUAL --22\n";
		    				}
			    			
    					}else{

    						if(ejecutable.sBObtieneElementos.equals("V") && sAtributosEntidad.equals("")){
    							
    							LinkedList<ElementRow> listaElementos = servicio.getElementRow(iIdEmpresa, Integer.parseInt(request.getParameter("ID_EJECUTABLE")), lbDebug);

    							String[] sRows = request.getParameterValues("ROWNUM");
    							String[] sVector;
								int iNumRegs = sRows==null?0:sRows.length;
								if(lbDebug){
						        	System.out.println(" recupera el vector iNumRegs: " + iNumRegs);
					        	}

    							if(listaElementos.size()>0){

    								ElementRow elementRow = null;
    								Iterator<ElementRow> iteraElementos = listaElementos.iterator();

    								if(lbDebug){
							        	System.out.println(" listaElementos.size(): " + listaElementos.size());
						        	}

    								while(iteraElementos.hasNext()){

    									elementRow = iteraElementos.next();
	    								if(lbDebug){
								        	System.out.println(" Un nuevo elemento " + elementRow.sElemento);
							        	}
	    								
    									sVector = request.getParameterValues(elementRow.sElemento);

    									for (int y = 0; y < iNumRegs; y++) {

	    									if(lbDebug){
    								        	System.out.println(" elementRow.sElemento[" + y + "]  ----->>>>>>>>>>>>>>>>>>>  " + elementRow.sElemento);
    								        	System.out.println(" iNumRegs= " + iNumRegs);
    							        	}
	    									if(elementRow.sTipoElemento.contains("CHECKBOX")){

	    										String sNomParametro = elementRow.sElemento + "_" + sRows[y];
	    										if(lbDebug){
	    								        	System.out.println(" sNomParametro  ----->>>>>>>>>>>>>>>>>>>  " + sNomParametro);
	    							        	}
	    										
	    										sAtributosEntidad = sAtributosEntidad + "UNION ALL SELECT '" + elementRow.sElemento + "'," + iIdTabla + "," + (y + 1 ) + ",'RECOVERY','" + 
		    											(request.getParameter(sNomParametro) == null ? "F" : "V") + "',NULL,NULL FROM DUAL\n";
	    									}else{
	    										if(sVector[y]!=null)
	    											sAtributosEntidad = sAtributosEntidad + "UNION ALL SELECT '" + elementRow.sElemento + "'," + iIdTabla + "," + (y + 1 ) + ",'RECOVERY','" + 
			    											 (sVector[y]==null?"":sVector[y].replace("'", "''")) + "',NULL,NULL FROM DUAL\n";
	    									}
	    									if(lbDebug){
    								        	System.out.println(" Obtuvo el elemento" );
    							        	}
		    		    				}
    								}
    							}
    						}
    						
    						if(atributo.sTipoDato.equals("CLOB")){

    							iIdClob = servicio.iGuardaCLOB(iIdEmpresa, Integer.parseInt(request.getParameter("ID_EJECUTABLE")), 
    									pantalla.iIdPantalla, usuario.sCveUsuario, request.getParameter(atributo.sNomAtributo), "Servlet atributo", lbDebug);
    							
    							sInsertStmt = sInsertStmt + "UNION ALL SELECT '" + atributo.sNomAtributo + "'," + iIdTabla + ",1,'EXEC'," +
    									"'SELECT VALOR FROM @ESQUEMA@.AOS_TMP_CLOB@DBLINK@ WHERE ID = " + String.valueOf(iIdClob) + "',NULL,NULL FROM DUAL --23\n";
    							
    						}else{

				    			sInsertStmt = sInsertStmt + "UNION ALL SELECT '" + atributo.sNomAtributo + "'," + iIdTabla + ",1,'EXEC','" +
				    					(request.getParameter(atributo.sNomAtributo) == null ? (atributo.sTipoTemplate.contains("CHECKBOX") ? "F" : "")  : 
				    						(atributo.sTipoTemplate.contains("CHECKBOX") ? "V" : 
					        			 ((request.getParameter(atributo.sNomAtributo)==null?"":request.getParameter(atributo.sNomAtributo).replace("'", "''"))))) +
					        				 "',NULL,NULL FROM DUAL --24\n";
    						}
    					}
    				//}
	    		}
	    		sInsertStmt = sInsertStmt + sAtributosEntidad;
	        }else{
	        	if(lbDebug){
		        	System.out.println(" LA LISTA DE PAR�METROS getListaParamExec NO TIENE REGISTROS  ----->>>>>>>>>>>>>>>>>>>  ");
	        	}
	        }
		}


		if(navegacion.sB_EJECUTA_EXECUTABLE.equals("V")){
	        if(lbDebug){
	            date = new Date();
	            System.out.println("fecha de sistema: " + date.toString());
	        	System.out.println("ANTES DE LLAMAR A PROCESAR LA EJECUCI�N >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> \n");
	        	System.out.println(" SE NECESITAN DATOS: \n INSERT INTO CONTROL.AOS_ARQ_PARAM_REQ\n" + sInsertStmt 
    					//+ " \n '" + sInsertStmt.replace("'", "''") + "'"
    					);
	        }
	        // Se agrega el id del ejecutable
	        //sInsertStmt = sInsertStmt + "UNION ALL SELECT 'ID_EJECUTABLE' AS PARAMETRO, 'EXEC' AS TIPO, 1 AS RENGLON, '" + request.getParameter("ID_EJECUTABLE") + "' AS VALOR, " + iIdTabla + " AS ID_TABLA FROM DUAL \n";

	        // Se obtiene el ejecutable
	        ejecutable = servicio.getEjecutable(iIdEmpresa, Integer.parseInt(request.getParameter("ID_EJECUTABLE")), lbDebug);
	        //iIdLogEjecutable = servicio.iIdLogEjecutable();
	        
	        if(lbDebug){
	            date = new Date();
	            System.out.println("fecha de sistema: " + date.toString());
	        	System.out.println("iIdLogEjecutable: " + ejecutable.iIdLogError);
	        }		        
	        sResultAltMod = servicio.ejecutaProceso(iIdEmpresa, iIdTabla, ejecutable.iIdEjecutable, sInsertStmt, usuario.sCveUsuario, ejecutable.iIdLogError, lbDebug);
			
	        if(lbDebug){
	            date = new Date();
	            System.out.println("fecha de sistema: " + date.toString());
	        	System.out.println("DESPU�S DE LLAMAR A PROCESAR LA EJECUCI�N >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	        }
	        
	        int iIdMsgErrorExecutable = servicio.getMensajeError(ejecutable.iIdLogError, lbDebug);
	        
	        //Verifica si ocurri� alg�n error
			if(sResultAltMod != null || iIdMsgErrorExecutable > 0){
				if(lbDebug){
			        date = new Date();
			        System.out.println("fecha de sistema: " + date.toString());
		        	System.out.println("iIdMsgErrorExecutable ====== " + iIdMsgErrorExecutable);
		        }

				String sEntidadOrigen	= "UNION ALL SELECT 'NOM_ENTIDAD',4,1,'CONTROL','" + 
						(request.getParameter("NOM_ENTIDAD")==null?"":request.getParameter("NOM_ENTIDAD")) + "',NULL,NULL FROM DUAL\n";
				sInsertStmt = sInsertStmt + sEntidadOrigen;

				if(lbDebug){
		        	System.out.println("sInsertStmt en errorr indice  ====== \n" + sInsertStmt);
		        }

				if(ejecutable.sBGuardaLogErrorHtml.equals("V")){
					if(lbDebug){
				        date = new Date();
				        System.out.println("fecha de sistema: " + date.toString());
			        	System.out.println("Entra a guardar los Atributos **************** ");
			        }
					ejecutable.iIdLogErrorHtml = servicio.iIdLogErrorGuardaDatosExec(iIdEmpresa, sInsertStmt, usuario.sCveUsuario, lbDebug);
				}
				

				if(iIdMsgErrorExecutable > 1){

					if(ejecutable.sBGuardaLogErrorHtml.equals("F")){
						if(sResultAltMod != null){
							request.setAttribute("MSG_ERROR", sResultAltMod);
					        response.setContentType("text/html");
							request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
				        	return;
						}else{
							request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(iIdMsgErrorExecutable, usuario.sCveUsuario, lbDebug));
					        response.setContentType("text/html");
							request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
				        	return;
						}

					}else{
						if(lbDebug){
				        	System.out.println(usuario.sCveMasterTemplate + "ErrorPage.jsp?ErrorMessage=" + 
				        			(sResultAltMod != null ? sResultAltMod : servicio.sDameMensajeSistema(iIdMsgErrorExecutable, usuario.sCveUsuario, lbDebug)).replace("@ID_LOG", Integer.toString(ejecutable.iIdLogError)) +
									"&ID_LOG_ERROR_ALTA_ENTIDAD=" + ejecutable.iIdLogErrorHtml + "&PARAM_CTL_PANTALLA=" + pantalla.iIdPantalla + 
									"&PARAM_CTL_ID_EMPRESA=" + iIdEmpresa + "&NOM_ENTIDAD=" + (request.getParameter("NOM_ENTIDAD")==null?"":request.getParameter("NOM_ENTIDAD")) + "&PARAM_CTL_ID_TABLA=" + iIdTabla + "&ID_EJECUTABLE=" + request.getParameter("ID_EJECUTABLE") +
									"&PARAM_CTL_CVE_NAVEGACION=NAVEGA_CAT_CTL_EJECUTA      1  ");
						} 
						request.setAttribute("MSG_ERROR", (sResultAltMod != null ? sResultAltMod : servicio.sDameMensajeSistema(iIdMsgErrorExecutable, usuario.sCveUsuario, lbDebug)).replace("@ID_LOG", Integer.toString(ejecutable.iIdLogError)) +
								"&ID_LOG_ERROR_ALTA_ENTIDAD=" + ejecutable.iIdLogErrorHtml + "&PARAM_CTL_PANTALLA=" + pantalla.iIdPantalla + 
								"&PARAM_CTL_ID_EMPRESA=" + iIdEmpresa + "&NOM_ENTIDAD=" + (request.getParameter("NOM_ENTIDAD")==null?"":request.getParameter("NOM_ENTIDAD")) + "&PARAM_CTL_ID_TABLA=" + iIdTabla + "&ID_EJECUTABLE=" + request.getParameter("ID_EJECUTABLE") +
								"&PARAM_CTL_CVE_NAVEGACION=NAVEGA_CAT_CTL_EJECUTA");
				        response.setContentType("text/html");
						request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
			        	return;
					}

				}else{
					if(ejecutable.sBGuardaLogErrorHtml.equals("F")){
						if(lbDebug){
					        date = new Date();
					        System.out.println("fecha de sistema: " + date.toString());
							System.out.println("ejecutable.sBGuardaLogErrorHtml >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + ejecutable.sBGuardaLogErrorHtml);
					    } 
						request.setAttribute("MSG_ERROR", sResultAltMod);
				        response.setContentType("text/html");
						request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
			        	return;
					}else{
						if(lbDebug){
				        	System.out.println(usuario.sCveMasterTemplate + "ErrorPage.jsp?ErrorMessage=" + sResultAltMod +
									"&ID_LOG_ERROR_ALTA_ENTIDAD=" + ejecutable.iIdLogErrorHtml + "&PARAM_CTL_PANTALLA=" + pantalla.iIdPantalla + 
									"&PARAM_CTL_ID_EMPRESA=" + iIdEmpresa + "&NOM_ENTIDAD=" + (request.getParameter("NOM_ENTIDAD")==null?"":request.getParameter("NOM_ENTIDAD")) + "&PARAM_CTL_ID_TABLA=" + iIdTabla + "&ID_EJECUTABLE=" + request.getParameter("ID_EJECUTABLE") +
									"&PARAM_CTL_CVE_NAVEGACION=NAVEGA_CAT_CTL_EJECUTA");
				        }
						request.setAttribute("MSG_ERROR", sResultAltMod +
								"&ID_LOG_ERROR_ALTA_ENTIDAD=" + ejecutable.iIdLogErrorHtml + "&PARAM_CTL_PANTALLA=" + pantalla.iIdPantalla + 
								"&PARAM_CTL_ID_EMPRESA=" + iIdEmpresa + "&NOM_ENTIDAD=" + (request.getParameter("NOM_ENTIDAD")==null?"":request.getParameter("NOM_ENTIDAD")) + "&PARAM_CTL_ID_TABLA=" + iIdTabla + "&ID_EJECUTABLE=" + request.getParameter("ID_EJECUTABLE") +
								"&PARAM_CTL_CVE_NAVEGACION=NAVEGA_CAT_CTL_EJECUTA");
				        response.setContentType("text/html");
						request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
			        	return;
					}
				}
			}
		}
    		
		if(navegacion.sB_OBTIENE_CAMPOS_PK.equals("V")){  		

			String sGetSequence = sTipoOperacion.equals("A")?"V":"F";
			//Se recuperan los atributos de la llave primaria 
			listaParametros = servicio.getListaAtributosPk(iIdEmpresa, pantalla.iIdPantalla, iIdTabla, 0, sGetSequence, lbDebug);

			//  llave primaria 
			if(lbDebug){
		        date = new Date();
		        System.out.println("fecha de sistema: " + date.toString());
				System.out.println("llena lista de atributos de llave primaria " + listaParametros.size());
			}
			
			listaParam = (Iterator<Atributo>) listaParametros.iterator();
			while (listaParam.hasNext()){
				atributo =  listaParam.next();
				
				if(atributo.sNomAtributo.equals("ID_EMPRESA")){
					sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ atributo.sNomAtributo + "'," + iIdTabla + ",1,'" +
						(atributo.sIdPk.equals("0")? "NEEDED" : atributo.sCveParameter) + "','" +
		        			request.getParameter("PARAM_CTL_ID_EMPRESA") + "',NULL,NULL FROM DUAL --25\n";
				}else{
					if(request.getParameter(atributo.sNomAtributo) != null && !request.getParameter(atributo.sNomAtributo).equals("")){
						sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ atributo.sNomAtributo + "'," + iIdTabla + ",1,'" +
						(atributo.sIdPk.equals("0")? "NEEDED" : atributo.sCveParameter) + "','" +
			        			request.getParameter(atributo.sNomAtributo) + "',NULL,NULL FROM DUAL --26\n";
					}else{
						if(lbDebug){
			                date = new Date();
			                System.out.println("ELSE PARAMETRO NULO ");
			        	}
						if(atributo.iSeqVal>0&&atributo.sIdPk!=null&&!atributo.sIdPk.equals("")){
							sInsertStmt = sInsertStmt + "UNION ALL SELECT '"+ atributo.sNomAtributo + "'," + iIdTabla + ",1,'PK','" +
									atributo.iSeqVal + "',NULL,NULL FROM DUAL --26-2\n";
							request.setAttribute(atributo.sNomAtributo, atributo.iSeqVal);
						}
					}
				}
			}
		}
    		
		if(navegacion.sB_OBTIENE_CAMPOS_ALT_MOD.equals("V")){
		
			// Se obtienen los atributos de la tabla a modificar 
			listaAtributos = servicio.getListaAtributosTabla(iIdEmpresa, pantalla.iIdPantalla, iIdTabla,"ALT_MOD", usuario.iIdRol, sTipoOperacion, lbDebug);
	
	        if (listaAtributos != null){
	        	
	        	if(lbDebug){
	                date = new Date();
	                System.out.println("iNumRegistrosAltMod: " + iNumRegistrosAltMod);
		        	System.out.println(" LA LISTA getListaAtributosTabla TIENE REGISTROS  ----->>>>>>>>>>>>>>>>>>>  ");
		        	System.out.println(" listaAtributos.size()  ----->  " + listaAtributos.size());
	        	}

	        	listaAtrib = (Iterator<Atributo>)listaAtributos.iterator();
	    		while(listaAtrib.hasNext()){
	    			atributo = listaAtrib.next();
		        	if(lbDebug){
		                date = new Date();
		                System.out.println("fecha de sistema: " + date.toString());
		    			System.out.println(" ATRIBUTO EN ALT_MOD ----->     " + atributo.sNomAtributo);
		        	}
		        	
		        	if(atributo.bIsMultiInsert && iNumRegistrosAltMod==0){
    								
						String[] sRegistros = request.getParameterValues(atributo.sNomAtributo);
						iNumRegistrosAltMod = sRegistros==null?1:sRegistros.length;
						if(lbDebug){
							System.out.println("iNumRegistrosAltMod NUEVOOO ---->>>>>>" + iNumRegistrosAltMod);
						}
		        	}else{
		        		if(iNumRegistrosAltMod==0){
		        			iNumRegistrosAltMod = 1;
		        		}
		        	}
		        	
		        	if(atributo.sNomAtributo.equals("ID_EMPRESA")){

		        		for (int x = 0; x < iNumRegistrosAltMod; x++) {
		        		
			        		sInsertStmt = sInsertStmt + "UNION ALL SELECT 'ID_EMPRESA'," + iIdTabla + "," + (x + 1) + ",'ALTA_MOD','" +
				        			 iIdEmpresa + "',NULL,NULL FROM DUAL --iemp \n";
		        		}
		        	}else{

			        	String[] sCampo = request.getParameterValues(atributo.sNomAtributo);
			        	
			        	if(lbDebug){
			        		System.out.println("iNumRegistrosAltMod : " + iNumRegistrosAltMod);
			        		int iLength = sCampo==null?0:sCampo.length;
			                System.out.println("Longitud del areglo : " + atributo.sNomAtributo + " > " + iLength);
			    			System.out.println(" ATRIBUTO EN ALT_MOD ----->     " + atributo.sNomAtributo);
			        	}
			        	
			        	// Verifica si el tipo de par�metro es checkbox, actualmente solo soporta un parametro y no un arreglo
	    				if(atributo.sTipoTemplate.contains("CHECKBOX")){
	    					
	    					sInsertStmt = sInsertStmt + "UNION ALL SELECT '" + atributo.sNomAtributo + "'," + iIdTabla + ",1,'ALTA_MOD','" +
	    							(request.getParameter(atributo.sNomAtributo)!=null?
						        	(request.getParameter(atributo.sNomAtributo).equals("on") ? "V" : "F"):"F") +
						        	"',NULL,NULL FROM DUAL --29\n";
	    					
	    				}else{
	    					if(sCampo!=null){
				        		for (int x = 0; x < iNumRegistrosAltMod; x++) {
				        			
				        			if(lbDebug){
						                date = new Date();
						                System.out.println("x: " + x);
						    			System.out.println(" itera el arreglo ----->     ");
						        	}
				        			
				        			// Verifica si el tipo de atributo es password
						        	if(atributo.sTipoTemplate.equals("INPUT_PASSWORD_CHANG")){
						        		
						        		bIsChangePswd = true;		        		
					    			
					    				if (sCampo[x]!= null){
						    				
							    			sInsertStmt = sInsertStmt + "UNION ALL SELECT '" + atributo.sNomAtributo + "'," + iIdTabla + "," + (x + 1) + ",'ALTA_MOD','" +
								        			 (atributo.sNomAtributo.equals("ID_EMPRESA") ? iIdEmpresa : (sCampo[x] == null ? "" : 
								        				 sCampo[x].replace("'", "''"))) + 
								        				 "',NULL,NULL FROM DUAL --27\n";
						    				
					    					
						    				if(servicio.bVerificaPassword(iIdEmpresa, pantalla.iIdPantalla, iIdTabla, sInsertStmt, atributo.sNomAtributo, request.getParameter(atributo.sNomAtributo), usuario.sCveUsuario, lbDebug)){
		
						    					sInsertStmt = sInsertStmt + "UNION ALL SELECT 'CTLNEW_" + atributo.sNomAtributo + "'," + iIdTabla + "," + (x + 1) + ",'ALTA_MOD','" +
									        			 (request.getParameter("CTLNEW_" + atributo.sNomAtributo) == null ? "" : 
									        				 request.getParameter("CTLNEW_" + atributo.sNomAtributo).replace("'", "''")) + 
									        				 "',NULL,NULL FROM DUAL --28\n";
						    				}else{
						    					// si la confirmaci�n no coincide se env�a el mensaje de error
						    					request.setAttribute("MSG_ERROR", servicio.sDameMensajeSistema(26, usuario.sCveUsuario, lbDebug));
										        response.setContentType("text/html");
												request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
									        	return;
						    				}
					    				}		    				
		
					    			}else{

			    						if(atributo.sTipoDato.equals("CLOB")){
	
			    							if(atributo.sCveMultiInsert.equals("V")){
				    							iIdClob = servicio.iGuardaCLOB(iIdEmpresa, 0, 
				    									pantalla.iIdPantalla, usuario.sCveUsuario, sCampo[x], "Servlet get atts altmod", lbDebug);
				    							
				    							sInsertStmt = sInsertStmt + "UNION ALL SELECT '" + atributo.sNomAtributo + "'," + iIdTabla + "," + (x + 1) + ",'ALTA_MOD'," +
				    									"'SELECT VALOR FROM @ESQUEMA@.AOS_TMP_CLOB@DBLINK@ WHERE ID = " + String.valueOf(iIdClob) + "',NULL,NULL FROM DUAL --31\n";
			    							}else{
			    								iIdClob = servicio.iGuardaCLOB(iIdEmpresa, 0, 
				    									pantalla.iIdPantalla, usuario.sCveUsuario, request.getParameter(atributo.sNomAtributo), "Servlet get atts altmod", lbDebug);
				    							
				    							sInsertStmt = sInsertStmt + "UNION ALL SELECT '" + atributo.sNomAtributo + "'," + iIdTabla + "," + (x + 1) + ",'ALTA_MOD'," +
				    									"'SELECT VALOR FROM @ESQUEMA@.AOS_TMP_CLOB@DBLINK@ WHERE ID = " + String.valueOf(iIdClob) + "',NULL,NULL FROM DUAL --31\n";
			    							}
			    						}else{
	
			    							if(atributo.sCveMultiInsert.equals("V")){
			    								sInsertStmt = sInsertStmt + "UNION ALL SELECT '" + atributo.sNomAtributo + "'," + iIdTabla + "," + (x + 1) + ",'ALTA_MOD','" +
									        			 (atributo.sNomAtributo.equals("ID_EMPRESA") ? iIdEmpresa : (sCampo[x] == null ? "" : 
									        				 sCampo[x].replace("'", "''"))) + "',NULL,NULL FROM DUAL --32\n";
			    								
			    							}else{
			    								sInsertStmt = sInsertStmt + "UNION ALL SELECT '" + atributo.sNomAtributo + "'," + iIdTabla + "," + (x + 1) + ",'ALTA_MOD','" +
							        			 (request.getParameter(atributo.sNomAtributo).replace("'", "''")) + "',NULL,NULL FROM DUAL --32\n";
			    							}
			    						}
					    			}
				        		}
				        	}else{
				        		if(atributo.sIdPk!=null&&atributo.sSequenceAutoInc!=null){
    								sInsertStmt = sInsertStmt + "UNION ALL SELECT '" + atributo.sNomAtributo + "'," + iIdTabla + ",1,'ALTA_MOD','" +
						        			 (request.getAttribute(atributo.sNomAtributo)) + "',NULL,NULL FROM DUAL --32-2\n";
				        		}
				        	}
	    				}
		        	}
	    		}
	        }
		}

    		// Verifica si debe realizar una alta modificaci�n o baja de registros
		if(navegacion.sB_EJECUTA_ALT_MOD.equals("V")){
		
			if(lbDebug){
		        date = new Date();
		        System.out.println("fecha de sistema: " + date.toString());
				System.out.println("ANTES DE LLAMAR A EJECUTAR LA ACTUALIZACION >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> \n" +
						"INSERT INTO CONTROL.AOS_ARQ_PARAM_REQ\n" + sInsertStmt);
			}

			// Ejecuta la actualizaci�n de datos
			sResultAltMod = servicio.altaModDatos(iIdEmpresa, pantalla.iIdPantalla, iIdTabla, sInsertStmt, sTipoOperacion, 
								usuario.sCveUsuario, usuario.iIdRol, lbDebug);
			
			if(lbDebug){
		        date = new Date();
		        System.out.println("fecha de sistema: " + date.toString());
				System.out.println("TERMINA DE EJECUTAR LA ACTUALIZACION >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> \n");
			}

			if(sResultAltMod != null){
				sResultAltMod = sResultAltMod.replace("SYS_DYNAPP_ERROR:", "").replace("\"", "").replace("'", "");
				if(lbDebug){
			        System.out.println("sResultAltMod >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> \n" + sResultAltMod);
				}
				
				if(iIdClob>0){
	        		servicio.borraCLOB(iIdClob, lbDebug);
	        	}
				
		        request.setAttribute("MSG_ERROR", sResultAltMod);
		        response.setContentType("text/html");
				request.getRequestDispatcher(usuario.sCveMasterTemplate + "ErrorPage.jsp").forward(request,response);
            	return;
			}
			
			if(bIsChangePswd){
				usuario.bCambioPassword = false;
				session.setAttribute("Usuario", usuario);
			}
		}
	}
/*********************** TERMINA BLOQUE DE IDA AL SERVIDOR Y A LA BASE DE DATOS Y COMIENZA EL REGRESO O RESPUESTA   ********************************************************/

			if(lbDebug){
		        date = new Date();
		        System.out.println("fecha de sistema: " + date.toString());
				System.out.println("INICIA RESPUESTA    ************************************************************************************* ");
			}

			// Verifica si se obtiene la pr�xima navegaci�n 
			if(navegacion.sB_OBTIENE_NAVEGA_NEXT.equals("V") && !usuario.bCambioPassword && !bIsChangePswd){
				navegacionNext = servicio.getNavegacionNext(iIdEmpresa, pantalla.iIdPantalla, iIdTabla, usuario.sCveUsuario, lbDebug);
				if(lbDebug){
		    		System.out.println("navegacionNext.iIdTabla " + navegacionNext.iIdTabla);
		    		System.out.println("iIdTabla " + iIdTabla);
	    		}
	    		
				if(navegacionNext.iIdTabla != iIdTabla){
					if(lbDebug){
						System.out.println("VA A GET REGISTROOOOOOOO  ---->>>>>>>>>>>>>>>>>>>>>>>>>>" );
					}
					/***************   SE VA A GET REGISTRO           **************************************************************/
					sTmpCveNavegacion 	= "NAVEGA_CAT_CTL_GET_REG";
					if(!(pantalla.bBRedirectAltToMod&&(sTipoOperacion.equals("A")||sTipoOperacion.equals("M"))))
					request.setAttribute("ID_TABLA_ANTERIOR", iIdTabla);
				}
				else{
					if(lbDebug){
						System.out.println("VA A BUSQUEDA       ---->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" );
					}
					/***************   SE VA A BUSQUEDA DE SU MISMA TABLA  **************************************************************/
					sTmpCveNavegacion 	= "NAVEGA_CAT_CTL_MENU";
				}
				// Se obtiene la nueva navegaci�n en caso de que haya cambiado
				if(!navegacion.sCVE_NAVEGACION.equals(sTmpCveNavegacion)){
					if(pantalla.bBRedirectAltToMod&&(sTipoOperacion.equals("A")||sTipoOperacion.equals("M"))){
						navegacion = servicio.getNavegacion("NAVEGA_CAT_CTL_GET_REG", lbDebug);
					}else{
						navegacion = servicio.getNavegacion(sTmpCveNavegacion, lbDebug);
					}
				}
			}
			
    		if(navegacion.sB_REDIRECCIONA_ID_TABLA.equals("V") && !usuario.bCambioPassword && !bIsChangePswd){
				if(navegacionNext.iIdTabla != iIdTabla){
					if(lbDebug){
						System.out.println("VA A REDIRECCIONAR LA TABLA  ---->>>>>>>>>>>>>>>>>>>>>>>>>>" );
					}
					/***************   SE VA A GET REGISTRO           **************************************************************/
					if(!(pantalla.bBRedirectAltToMod&&(sTipoOperacion.equals("A")||sTipoOperacion.equals("M"))))
					iIdTabla 			= navegacionNext.iIdTabla;
				}
    		}
    		
    		if(!usuario.bCambioPassword){
    			if(lbDebug){
    		        date = new Date();
    		        System.out.println("fecha de sistema: " + date.toString());
					System.out.println("YA NO ES CAMBIO PASSWORD ---->>>>>>>>>>>>>>>>>>>>>>>>>>" );
				}
	    		if(bIsChangePswd){
	    			if(lbDebug){
						System.out.println("ACABA DE CAMBIAR PASSWORD ---->>>>>>>>>>>>>>>>>>>>>>>>>>" );
					}
	    			iIdTabla = 0;
	    			iPantallaTmp = 0;
	    			navegacion = servicio.getNavegacion("NAVEGA_CAT_CTL_MENU", lbDebug);
	    		}
    		}

/*********************** TERMINA BLOQUE DE IDA AL SERVIDOR Y A LA BASE DE DATOS Y COMIENZA EL REGRESO O RESPUESTA   ********************************************************/    		
    		
    		if(lbDebug){
				System.out.println("agrega atributos cve_navegacion  ---->>>>>>>>>>>>>>>>>>>>>>>>>> " + navegacion.sCVE_NAVEGACION);
				System.out.println("agrega atributos iIdTabla  ---->>>>>>>>>>>>>>>>>>>>>>>>>> " + iIdTabla);
			}
    		request.setAttribute("CVE_NAVEGACION", navegacion.sCVE_NAVEGACION);
    		request.setAttribute("ID_TABLA", iIdTabla);
    		doGet(request, response); 
    		
	}
	
}