package com.dinamicaps.web;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//import javax.servlet.annotation.WebServlet;

import java.io.File;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperRunManager;

import com.dinamicaps.web.impl.domain.media.ReportVO;
import com.dinamicaps.web.impl.media.reports.ReportsMediaDAO;
import com.google.gson.Gson;
import com.dinamicaps.web.impl.domain.media.OrdenInsercionDet;
import com.dinamicaps.web.impl.domain.media.ServicioAjax;
import com.dinamicaps.web.impl.domain.media.ServicioCalendario;

//import org.apache.commons.lang3.StringUtils;
/**
 * Servlet implementation class ReportsMerdiaSrv
 */
//@WebServlet("/ReportsMerdiaSrv")
public class ReportsMerdiaSrv extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReportsMerdiaSrv() {
        super();
        // TODO Auto-generated constructor stub
    }

    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //PrintWriter out = response.getWriter();
        ServletOutputStream outputstream = response.getOutputStream();
        try {
            //ORDEN_PUBLICITARIA_FACTURA_V
            //ENCABEZADO_FORMATO_TVSA_V
        	
        	System.out.println("en metodo ID_PLAN: " + request.getParameter("ID_BUSINESS"));
        	System.out.println("en metodo ID_EMPRESA: " + request.getParameter("ID_COMPANY"));

            if (request.getParameter("ID_BUSINESS") != null & request.getParameter("ID_COMPANY") != null) {
                if (!request.getParameter("ID_BUSINESS").equals("") & !request.getParameter("ID_COMPANY").equals("")) {
                	
                	System.out.println("entra a la condicion" );
                	
                    ReportsMediaDAO conn = new ReportsMediaDAO();
                    conn.abreConexion();
                    ReportVO encabezado = conn.getEncabezadoReporte(request.getParameter("ID_COMPANY"), request.getParameter("ID_BUSINESS"));
                    ResultSet ds = conn.getDatosReporte(request.getParameter("ID_BUSINESS"));

                    //String jrxmlFileName = "WEB-INF\\reportes\\ReporteMedios.jasper";
                    
                    String sRealPath = request.getSession().getServletContext().getRealPath("/");
                    System.out.println("sRealPath antes: " + sRealPath);
                    //sRealPath = sRealPath.substring(0, sRealPath.lastIndexOf("\\")) + "\\WEB-INF\\reportes\\ReporteMedios.jasper";
                    sRealPath = sRealPath + "WEB-INF\\reportes\\ReporteMedios.jasper";
                    
                    System.out.println("sRealPath: " + sRealPath);
                    //File archivoReporte = new File(request.getSession().getServletContext().getRealPath(jrxmlFileName));
                    File archivoReporte = new File(sRealPath);
                    HashMap<String, Object> parametros = new HashMap<String, Object>();
                    parametros.put("IMG_LOGO", request.getSession().getServletContext().getRealPath("WEB-iNF\\reportes\\img\\televisa.jpg"));
                    parametros.put("cveCliente", encabezado.getCveCliente()==null?"":encabezado.getCveCliente());
                    parametros.put("cveEncCliente", encabezado.getCveEncClienteAg()==null?"":encabezado.getCveEncClienteAg());
                    parametros.put("canalOrdPlat", encabezado.getCanalOrden()==null?"":encabezado.getCanalOrden());
                    parametros.put("tipoFact", encabezado.getTipoFacturacion()==null?"":encabezado.getTipoFacturacion());
                    parametros.put("comentOrdServ", encabezado.getComentarios()==null?"":encabezado.getComentarios());
                    parametros.put("cpsMasContct", encabezado.getCpsMasterContact()==null?"":encabezado.getCpsMasterContact());
                    parametros.put("nomTarfia", encabezado.getNombreTarifa()==null?"":encabezado.getNombreTarifa());
                    parametros.put("catProducto", encabezado.getCategoriaProducto()==null?"":encabezado.getCategoriaProducto());
                    parametros.put("masterCntctIng", encabezado.getMasterContact()==null?"":encabezado.getMasterContact());
                    parametros.put("totalSpots", encabezado.getTotalSpots()==null?"":encabezado.getTotalSpots());
                    parametros.put("procXLinea", encabezado.getProcesarPorLinea()==null?"":encabezado.getProcesarPorLinea());
                    parametros.put("garantizado", encabezado.getGarantizado()==null?"":encabezado.getGarantizado());
                    parametros.put("nomEmailResp", encabezado.getNomEmailResponsable()==null?"":encabezado.getNomEmailResponsable());
                    parametros.put("refFolio", encabezado.getReferenciaFolio()==null?"":encabezado.getReferenciaFolio());
                    parametros.put("totOrdSinDesc", encabezado.getTotalOrdenSinDesc()==null?"":encabezado.getTotalOrdenSinDesc());
                    parametros.put("totOrdenConDesc", encabezado.getTotalOrdenConDesc()==null?"":encabezado.getTotalOrdenConDesc());
                    parametros.put("descUsoInt", encabezado.getDescUsoInterno()==null?"":encabezado.getDescUsoInterno());
                    parametros.put("tarifaCutIn", encabezado.getTarifaCutIn()==null?"":encabezado.getTarifaCutIn());                    
                    parametros.put("refFolio", encabezado.getReferenciaFolio()==null?"":encabezado.getReferenciaFolio());                    

                    System.out.println("agrega datos ------------" );
                    JRResultSetDataSource dataSource = new JRResultSetDataSource(ds);
                    System.out.println("va a concertir a pdf " );
                    byte[] bytes = JasperRunManager.runReportToPdf(archivoReporte.getPath(), parametros, dataSource);
                    System.out.println("obtiene bytes " );
                    
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    
                    System.out.println("va a escribir a pdf " );
                    outputstream.write(bytes, 0, bytes.length);
                    
                    System.out.println("va a flush " );
                    outputstream.flush();
                    System.out.println("va a close " );
                    outputstream.close();
                    System.out.println("ya cerro" );
                }
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }    
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Entra al servlet -------------------------------");
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Obtain the session object, create a new session if doesn't exist
        HttpSession session = request.getSession(false);
        String SResultado = "";

		if(session == null){
            System.out.println(" La sesi�n es nula ");
            SResultado = "Su sesi�n ha caducado";
	        SResultado	= new Gson().toJson(SResultado);   
	        System.out.println("SResultadoJson: " + SResultado);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(SResultado);    
			return;
        }
		
		
		String sCveOperacion   	= request.getParameter("CTL_CVE_OPERACION");
		System.out.println("sCveOperacion -------------------------------" + sCveOperacion);
		
		if(sCveOperacion==null){
			
			return;
		}else{
			
			if(sCveOperacion.equals("ALTA_MOD_CANTIDAD")){
			
				ReportsMediaDAO dao 	= new ReportsMediaDAO();
				SResultado 				= "";
				
				String sIdEmpresa		= request.getParameter("ID_EMPRESA");
				String sIdOrdenInsercion= request.getParameter("ID_ORDEN_INSERCION");
				String sTarifaCliente	= request.getParameter("TARIFA_CLIENTE");
				String sFInsercion		= request.getParameter("F_INSERCION");
				String sIdTarifa		= request.getParameter("ID_TARIFA");
				String sCantidad		= request.getParameter("CANTIDAD");
				String sCveEstatus		= request.getParameter("CVE_ESTATUS");
				String sCveMedida		= request.getParameter("CVE_MEDIDA")==null?"":request.getParameter("CVE_MEDIDA");
				String sHorizontal		= request.getParameter("HORIZONTAL")==null?"":request.getParameter("HORIZONTAL");
				String sVertical		= request.getParameter("VERTICAL")==null?"":request.getParameter("VERTICAL");
				String sImpHorizVert	= request.getParameter("IMPORTE_HORIZONTAL_VERTICAL")==null?"":request.getParameter("IMPORTE_HORIZONTAL_VERTICAL");
				
				System.out.println("sIdEmpresa: " + sIdEmpresa);
				System.out.println("sIdOrdenInsercion: " + sIdOrdenInsercion);
				System.out.println("sTarifaCliente: " + sTarifaCliente);
				System.out.println("sFInsercion: " + sFInsercion);
				System.out.println("sIdTarifa: " + sIdTarifa);
				System.out.println("sCantidad: " + sCantidad);
				System.out.println("sCveMedida: " + sCveMedida);
				System.out.println("sHorizontal: " + sHorizontal);
				System.out.println("sVertical: " + sVertical);
				System.out.println("sImpHorizVert: " + sImpHorizVert);
				System.out.println("sCveEstatus: " + sCveEstatus);
				
				try {
					
					dao.abreConexion();
			        SResultado = dao.sInsertaCantidad(sIdEmpresa, sIdOrdenInsercion, sFInsercion, sCantidad, 
			        		sIdTarifa, sTarifaCliente, sCveMedida, sHorizontal, sVertical, sImpHorizVert, sCveEstatus);
			        
			        System.out.println("SResultado: " + SResultado);
			        SResultado	= new Gson().toJson(SResultado);   
			        System.out.println("SResultadoJson: " + SResultado);
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write(SResultado);      
					return;
				} catch (Exception exc) {
		            exc.printStackTrace();
		        }
				 finally {
					try {
						if( dao != null ){
							if(dao.conn != null){
								dao.conn.close();	
							}							
						}							
					} catch(Exception exc){
						exc.printStackTrace();
					}			
				}
			}else{
				
				if(sCveOperacion.equals("ALTA_MOD_INSER_DET")){
					ServicioAjax servicioAjax = new ServicioAjax();
					ServicioCalendario servicioCalendario = new ServicioCalendario();
					// accion a realizar en el servlet
					String accion = request.getParameter("ACCION");
					System.out.println("ALTA_MOD_INSER_DET accion: " + accion);
					// mapa que contiene los valores que se envian por Json
					Map<String, Object> map = new HashMap<String, Object>();
					
					
					if (accion.equals("obtenDetalles")) {
						System.out.println("ID_EMPRESA: " + request.getParameter("ID_EMPRESA"));
						System.out.println("ID_INSERCION_CANTIDAD: " + request.getParameter("ID_INSERCION_CANTIDAD"));
						System.out.println("ID_TARIFA: " + request.getParameter("ID_TARIFA"));
						int idEmpresa = Integer.parseInt(request.getParameter("ID_EMPRESA"));
						int idInsercion = Integer.parseInt(request.getParameter("ID_INSERCION_CANTIDAD"));
						String sFecha   = request.getParameter("F_INSERCION");
						int iIdTarifa	= Integer.parseInt(request.getParameter("ID_TARIFA"));
						ArrayList<OrdenInsercionDet> listaOrdenInsercionDet = new ArrayList<OrdenInsercionDet>();
						
						try {
							listaOrdenInsercionDet = servicioCalendario.obtenListaOrdenInsercionBD(idEmpresa, idInsercion, sFecha, iIdTarifa);
						} catch(SQLException e) {
							e.printStackTrace();
						} catch(Exception e) {
							e.printStackTrace();
						}
						servicioAjax.mandaJsonRespuesta(response, listaOrdenInsercionDet);
						return;
					} else if (accion.equals("actualizaEstatusDet")) {
						int iId = Integer.parseInt(request.getParameter("ID"));
						String cveEstatus = request.getParameter("CVE_ESTATUS");
						try {
							servicioCalendario.actualizaOrdenPubliDet(iId, cveEstatus);
							map.put("estado", "correcto");
						} catch (SQLException e) {
							e.printStackTrace();
							map.put("estado", "errorSQL");
						} catch (Exception e) {
							e.printStackTrace();
							map.put("estado", "error");				
						}
						servicioAjax.mandaJsonRespuesta(response, map);
						return;
					} else if (accion.equals("eliminaEstatusDet")) {
						int iId = Integer.parseInt(request.getParameter("ID"));
						try {
							servicioCalendario.eliminaOrdenPubliDet(iId);
							map.put("estado", "correcto");
						} catch (SQLException e) {
							e.printStackTrace();
							map.put("estado", "errorSQL");
						} catch (Exception e) {
							e.printStackTrace();
							map.put("estado", "error");				
						}
						servicioAjax.mandaJsonRespuesta(response, map);
						return;			
					} 
					/*
					else if (accion.equals("crear")) {
						int idInsercion = Integer.parseInt(request.getParameter("idInsercion").substring(1));
						int idEmpresa = Integer.parseInt(request.getParameter("idEmpresa"));
						int idOrden = Integer.parseInt(request.getParameter("idOrden"));
						int idTarifa = Integer.parseInt(request.getParameter("idTarifa"));
						int cantidad = Integer.parseInt(request.getParameter("cantidad"));
						try {
							servicioCalendario.altaOrdenPubli(idInsercion, idEmpresa, idOrden, idTarifa, cantidad);
							map.put("estado", "correcto");
						} catch (SQLException e) {
							e.printStackTrace();
							map.put("estado", "errorSQL");
						} catch (Exception e) {
							e.printStackTrace();
							map.put("estado", "error");				
						}
						servicioAjax.mandaJsonRespuesta(response, map);
						return;
					} else if (accion.equals("actualizar")) {
						int idInsercion = Integer.parseInt(request.getParameter("idInsercion").substring(1));
						int cantidad = Integer.parseInt(request.getParameter("cantidad"));
						try {
							servicioCalendario.actualizaOrdenPubli(idInsercion, cantidad);
							map.put("estado", "correcto");
						} catch (SQLException e) {
							e.printStackTrace();
							map.put("estado", "errorSQL");
						} catch (Exception e) {
							e.printStackTrace();
							map.put("estado", "error");				
						}
						servicioAjax.mandaJsonRespuesta(response, map);
						return;
					}*/
				}
			}
		}
	}
}
