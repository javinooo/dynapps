package com.dinamicaps.web;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.File;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dinamicaps.web.impl.Servicio;
import com.dinamicaps.web.impl.ServicioDAO;
import com.dinamicaps.web.impl.domain.Usuario;
import com.dinamicaps.web.impl.domain.Archivo;
import com.dinamicaps.web.impl.domain.BrowserType;
import com.dinamicaps.web.impl.domain.vstream.VStreamDAO;
import com.google.gson.Gson;

import org.apache.commons.digester.RegexRules;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;


/**
 * Servlet implementation class DinamicapsSrv
 */
public class AdmContenidosSrv extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdmContenidosSrv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ResourceBundle rb = ResourceBundle.getBundle("LocalStrings",request.getLocale());

        boolean lbDebug			= true;
        Archivo archivo = new Archivo();
        Servicio servicio = new Servicio();
        Usuario usuario = null;
        
        HttpSession session = request.getSession(false);

        if(session != null){
        	usuario = (Usuario) session.getAttribute("Usuario");
        }

        if (usuario == null) {
			System.out.println(" Entra al send redirect");
			response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp?SinSesion=V&ErrorMessage=" + servicio.sDameMensajeControl(34, usuario.sCveUsuario));
			return;
        }else{        		
        		
        		
			if(request.getParameter("ID_FILE") != null && !request.getParameter("ID_FILE").equals("")){
				
	    		if(lbDebug){
	    			System.out.println("ID_FILE no es nulo -----------------");
				}
				// Obtiene el archivo para su descarga
				archivo = servicio.getArchivoBytes(Integer.parseInt(request.getParameter("ID_FILE")), usuario.sCveUsuario, lbDebug);
				
				// get the image from the database
			       byte[] imgData = archivo.bDatos;   
			       // display the image
			       response.setContentType(archivo.sTipoContenido);
			       OutputStream o = response.getOutputStream();
			       o.write(imgData);
			       o.flush(); 
			       o.close();
			}
			
        
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        Usuario usuario			= null;
        Archivo archivo			= null;
        BrowserType browserType	= new BrowserType(request.getHeader("user-agent"));
        boolean isMultipart		= false;
        boolean lbDebug			= true;
        List<FileItem> items 	= null;
        String[] sParametros= new String[2];
        String sResultado		= "";

        // Se inicializa el sevicio de conexi�n
        Servicio servicio = new Servicio();

        //Obtain the session object
        HttpSession session = request.getSession(false);

        //Check if our session variable is set, if so, get the session variable value
        //which is an Integer object, and add one to the value.
        //If the value is not set, create an Integer object with the default value 1.
        //Add the variable to the session overwriting any possible present values.
        if(session != null){
        	usuario = (Usuario) session.getAttribute("Usuario");
        }

        if (usuario == null) {
			System.out.println(" Entra al send redirect");
			response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp?SinSesion=V&ErrorMessage=" + servicio.sDameMensajeControl(34, usuario.sCveUsuario));
			return;
        }else{

            if(lbDebug){
            	
    	        System.out.println("***********************************************************************************************************: ");
    	        System.out.println("Usuario: " + usuario.sCveUsuario + " ejecuta POST AdmContenidosSrv " );
    	        System.out.println("BROWSER: " + browserType.getName());
    	        System.out.println("VERSION: " + browserType.getVersion());
    	        System.out.println("PARAM_CTL_CVE_NAVEGACION: " + request.getParameter("PARAM_CTL_CVE_NAVEGACION"));
    	        System.out.println("PARAM_CTL_PANTALLA: " + request.getParameter("PARAM_CTL_PANTALLA"));
    	        System.out.println("PARAM_CTL_ID_EMPRESA: " + request.getParameter("PARAM_CTL_ID_EMPRESA"));
    	        System.out.println("PARAM_CTL_ID_TABLA: " + request.getParameter("PARAM_CTL_ID_TABLA"));
    	        System.out.println("request.getContextPath(): " + request.getContextPath());
            }

        	isMultipart = ServletFileUpload.isMultipartContent(request);

        	if(isMultipart){
        		
        		if(lbDebug){
        			System.out.println("Es Multipart  ****************************** ");
        		}

        		// Para subir archivos a una carpeta 
        		FileItemFactory factory = new DiskFileItemFactory();
                ServletFileUpload upload = new ServletFileUpload(factory);
                try {
                	
                	items = upload.parseRequest(request);
                    if(lbDebug){
                    	System.out.println("items: " + items);
                    }
                } catch (FileUploadException e) {
                    e.printStackTrace();
                }
                
                FileItem file 			= items.get(0);
                int iIdFile 			= servicio.iIdFile();
                String sFileName 		= file.getName();
                long lSize 				= file.getSize();
                sFileName				= sFileName.substring(0, sFileName.lastIndexOf(".")) +
                							"-" + iIdFile + sFileName.substring(sFileName.lastIndexOf("."));
                
                //System.out.println(sFileName.replaceAll("[^a-zA-Z0-9\\.-]", ""));
                
                sFileName				= sFileName.replaceAll("[^a-zA-Z0-9\\.-]", ""); 
                
                //Regex rgx = new RegexRules("[^a-zA-Z0-9-]");
                //str = rgx.Replace(str, "");
                //sFileName				= sFileName.replaceAll("[^A-Za-z0-9]", "");
                String sContentType		= file.getContentType();
                String sTipoCarga 		= "bfile";	

                if(lbDebug){
        			System.out.println("Es Multipart sContentType: " + sContentType);
        			System.out.println("iIdFile: " + iIdFile);
        			System.out.println("sFileName: " + sFileName);
        		}
                
                if(sTipoCarga.equals("EN BASE DE DATOS")){

                    /*String sResultInsertArchivo = servicio.sFileInsert(iIdFile, usuario.iIdEmpresa, usuario.sCveUsuario, sFileName, sContentType, file, lbDebug);

                    if(sResultInsertArchivo != null){

                    	response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp?ErrorMessage=" + sResultInsertArchivo);
                    	
                    }else{
                    	response.sendRedirect(request.getContextPath() + "/LoadFile.jsp?LoadFileResult=" + 
                    			(sResultInsertArchivo == null ? "El archivo se carg� exitosamente" : "") +
                    			"&IdFile=" + iIdFile + "&PARAM_CTL_FIELD_NAME=" + sNomParametro 
                    			);
                    	
                    	if(lbDebug){
	                    	System.out.println("parametro de nomre de campo: " + sNomParametro);
	                    	System.out.println("parametro de nomre de campo: " + sNomParametro);
                    	}
                    }*/

                }else{

/*					String root = getServletContext().getRealPath("/");
					File path = new File(root + "/uploads");
					if (!path.exists()) {
					    boolean status = path.mkdirs();
					}
*/
                	
                	try{
	                	if(sContentType.contains("image/")){
	                		
		                		int iMaxBytes = Integer.parseInt(servicio.sDameValorParametro(usuario.iIdEmpresa, "VARIABLES_CONFIGURACION", 
		                    			"IMG_PRODUCTO", usuario.sCveUsuario, lbDebug));

		                		if(lbDebug){
			                		System.out.println("iMaxBytes: " + iMaxBytes);
			                	}
		                		if(lSize <= iMaxBytes){
									//File uploadedFile = new File( "C:\\app\\Tecnofin\\product\\11.2.0\\dbhome_1\\files\\schema\\doctos\\" + sFileName);
				                	File uploadedFile = new File( servicio.sDameUrlDirectorio(usuario.sUserSchema, lbDebug) + sFileName);
				
				                	if(lbDebug){
				                		System.out.println("uploadedFile.getAbsolutePath(): " + uploadedFile.getAbsolutePath());
				                	}
			                	
									try{
										file.write(uploadedFile);
									}catch (Exception e) {
										e.printStackTrace();
									}
				
									if(lbDebug){
										System.out.println("NOMBRE DEL ARCHIVO ---->>>> " + sFileName);
										System.out.println("file.getContentType() ---->>>> " + file.getContentType());
									}
									
									String sResultInsertArchivo = servicio.sRefFileInsert(iIdFile, usuario.iIdEmpresa, usuario.sCveUsuario, sFileName, sContentType, usuario.sUserSchema, lbDebug);
				
									if(lbDebug){
										System.out.println("sResultInsertArchivo  ---->>>> " + sResultInsertArchivo);
									}
									
									if(request.getParameter("DYN_CTL_ID_TABLA")!=null&&request.getParameter("ATRIBUTO")!=null){
										if(lbDebug){
											System.out.println("SI VIENEN DYN_CTL_ID_TABLA Y ATRIBUTO  ---->>>> ");
										}
										
										String sCveOperacion = request.getParameter("DYN_CTL_ID_TABLA") + request.getParameter("ATRIBUTO"); 
										int iIdEvento;
										VStreamDAO dao = null;
										ServicioDAO daoMain = null;
										int iIdUsuario = 0; 
										int iIdOrdenServicio = 0;
										
										switch (sCveOperacion){
										case "498FOTO":
											if(lbDebug){
												System.out.println("ENTRA A 498FOTO  ---->>>> ");
											}
											iIdEvento = Integer.parseInt(request.getParameter("ID_EVENTO"));
											dao = new VStreamDAO();
											dao.abreConexion(); 
											dao.sUpdateFotoEvento(usuario.iIdEmpresa, iIdEvento, sFileName, lbDebug); 
											dao.conn.commit();
											dao.conn.close();	
										break;
										case "496ID_IMG_ALTA_TICKET":
											if(lbDebug){
												System.out.println("ENTRA A 496ID_IMG_ALTA_TICKET  ---->>>> ");
											}
											if(request.getParameter("ID_ORDEN_SERVICIO")!=null){
												iIdOrdenServicio = Integer.parseInt(request.getParameter("ID_ORDEN_SERVICIO"));
											}
											
											daoMain = new ServicioDAO();
											daoMain.abreConexion(); 
											daoMain.sUpdateOrdenServicio(usuario.iIdEmpresa, iIdOrdenServicio, "ID_IMG_ALTA_TICKET", 
													iIdFile, lbDebug); 
											daoMain.conn.commit();
											daoMain.conn.close();	
										break;
										case "496ID_IMG_CIERRE_TICKET":
											if(lbDebug){
												System.out.println("ENTRA A 496ID_IMG_CIERRE_TICKET  ---->>>> ");
											}
											if(request.getParameter("ID_ORDEN_SERVICIO")!=null){
												iIdOrdenServicio = Integer.parseInt(request.getParameter("ID_ORDEN_SERVICIO"));
											}
											daoMain = new ServicioDAO();
											daoMain.abreConexion(); 
											daoMain.sUpdateOrdenServicio(usuario.iIdEmpresa, iIdOrdenServicio, "ID_IMG_CIERRE_TICKET", 
													iIdFile, lbDebug); 
											daoMain.conn.commit();
											daoMain.conn.close();	
										break;
										case "499FOTO":
											if(lbDebug){
												System.out.println("ENTRA A 499FOTO  ---->>>> ");
											}
											iIdUsuario = Integer.parseInt(request.getParameter("ID_USUARIO"));
											dao = new VStreamDAO();
											dao.abreConexion(); 
											dao.sUpdateFotoUsuario(usuario.iIdEmpresa, iIdUsuario, sFileName, lbDebug); 
											dao.conn.commit();
											dao.conn.close();	
										break;
										}
									}
									
				                    if(sResultInsertArchivo != null){
				                    	if(lbDebug){
				    						System.out.println("ERROR: sResultInsertArchivo ---->>>> " + sResultInsertArchivo);
				    					}
				                    	//response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp?ErrorMessage=" + sResultInsertArchivo);
				
				                    }else{
				                    	
				                    	sParametros[0] = "" + iIdFile;
				            			sParametros[1] = null;
				            			sResultado	= new Gson().toJson(sParametros);   
				            	        //System.out.println("SResultadoJson: " + SResultado);
				            			response.setContentType("application/json");
				            			response.setCharacterEncoding("UTF-8");
				            			response.getWriter().write(sResultado);   
				                		return;	
				                    }
		                		}else{
		                			sParametros[0] = null;
		                			sParametros[1] = servicio.sDameMensajeSistema(65, usuario.sCveUsuario, lbDebug);
		                			sResultado	= new Gson().toJson(sParametros);   
		                			response.setContentType("application/json");
		                			response.setCharacterEncoding("UTF-8");
		                			response.getWriter().write(sResultado);   
		                    		return;	
		                		}
	                		} // si contiene image/
	                		else{
		                		sParametros[0] = null;
		            			sParametros[1] = servicio.sDameMensajeSistema(66, usuario.sCveUsuario, lbDebug);
		            			sResultado	= new Gson().toJson(sParametros);   
		            			response.setContentType("application/json");
		            			response.setCharacterEncoding("UTF-8");
		            			response.getWriter().write(sResultado);   
		                		return;	
	                	}
                	}catch (Exception e) {
                		e.printStackTrace();
                	}
                }
        	}
        }
	}
}